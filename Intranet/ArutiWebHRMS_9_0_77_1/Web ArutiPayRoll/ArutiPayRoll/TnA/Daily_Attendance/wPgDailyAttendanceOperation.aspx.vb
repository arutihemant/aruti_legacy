﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing
#End Region

Partial Class TnA_GlobalTimesheetAddEditDelete_wPgDailyAttendanceOperation
    Inherits Basepage

#Region " Private Variables "

    Private clsuser As New User
    Private cmsg As New CommonCodes
    Private dtEmployee As DataTable
    Private mintEmployeeUnkId As Integer = -1
    Private mintShiftID As Integer = -1
    Private ReadOnly mstrModuleName As String = "frmDailyAttendanceOperation"
    Dim objLogin As New clslogin_Tran

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                cmsg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            SetLanguage()

            If IsPostBack = False Then
                Call FillCombo()
                Call Fill_Employee_List()
                Call CheckButtonVisibility()
            Else
                mintEmployeeUnkId = Me.ViewState("mintEmployeeUnkId")
            End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg.Message, Me)
            'Pinkal (13-Aug-2020) -- End
        Catch ex As Exception
            cmsg.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintEmployeeUnkId", mintEmployeeUnkId)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'cmsg.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            cmsg.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Dim dsList As New DataSet
        Try

            dsList = objShift.getListForCombo("Shift", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Shift")
                .DataBind()
                .SelectedValue = "0"
            End With

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then


                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
                'cboEmployee_SelectedIndexChanged(Nothing, Nothing)
                cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
            End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg.Message, Me)
            'Pinkal (13-Aug-2020) -- End
        Catch ex As Exception
            cmsg.DisplayError("FillCombo : " & ex.Message, Me)
        Finally
            objEmployee = Nothing : objShift = Nothing : objPolicy = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_Employee_List()

        Dim dsEmployee As New DataSet
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId
            objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date

            dsEmployee = objLogin.GetList("List", True)

            If Not dsEmployee.Tables("List").Columns.Contains("workedDuration") Then
                dsEmployee.Tables("List").Columns.Add("workedDuration", GetType(String))
            End If


            Dim TotalSec As Integer = 0
            For Each dtRow As DataRow In dsEmployee.Tables("List").Rows
                dtRow("workedDuration") = CalculateTime(True, CInt(dtRow("workhour"))).ToString("#00.00").Replace(".", ":")
                TotalSec += CInt(dtRow("workhour"))
            Next


            txtTotalWorkedHour.Text = CalculateTime(True, TotalSec).ToString("#00.00").Replace(".", ":")


            dsEmployee.Tables("List").AcceptChanges()
            dtEmployee = New DataView(dsEmployee.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'If Me.ViewState("dtEmployee") Is Nothing Then
            'Me.ViewState.Add("dtEmployee", dtEmployee)
            'Else
            'Me.ViewState("dtEmployee") = dtEmployee
            'End If
            'Pinkal (13-Aug-2020) -- End


            dgvEmp.AutoGenerateColumns = False
            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()


        Catch ex As Exception
            cmsg.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'dgvEmp.DataSource = dtEmployee
            'dgvEmp.DataBind()
            'Pinkal (13-Aug-2020) -- End
            dsEmployee.Dispose()
        End Try
    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim objEmpShift As New clsEmployee_Shift_Tran
            mintEmployeeUnkId = cboEmployee.SelectedValue
            mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(ConfigParameter._Object._CurrentDateAndTime.Date, mintEmployeeUnkId)
            cboShift.SelectedValue = mintShiftID
            objEmpShift = Nothing

        Catch exArg As ArgumentException
            cmsg.DisplayError(exArg.Message, Me)
        Catch ex As Exception
            cmsg.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    Private Sub CheckButtonVisibility()
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId
            objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date

            If objLogin.GetLoginType(mintEmployeeUnkId, ConfigParameter._Object._CurrentDateAndTime.Date) = 1 Then
                btnCheckIn.Visible = False
                btnCheckOut.Visible = True
            Else
                btnCheckIn.Visible = True
                btnCheckOut.Visible = False
            End If
        Catch ex As Exception
            cmsg.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Pinkal (13-Aug-2020) -- End

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLogin._Userunkid = CInt(Session("UserId"))
            End If
            'objLogin._WebClientIP = CStr(Session("IP_ADD"))

            'objLogin._WebHostName = CStr(Session("HOST_NAME"))
            'objLogin._WebFormName = mstrModuleName

            With objLogin
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
        Catch ex As Exception
            cmsg.DisplayError("SetAtValue():" + ex.Message, Me)
        End Try

    End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnCheckIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckIn.Click
        Try
            objLogin._Employeeunkid = mintEmployeeUnkId
            objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            objLogin._checkintime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If cboShift.SelectedValue IsNot Nothing AndAlso CInt(cboShift.SelectedValue) > 0 Then
                objLogin._Shiftunkid = CInt(cboShift.SelectedValue)
            Else
                objLogin._Shiftunkid = 0
            End If
            'Pinkal (27-Aug-2020) -- End

            objLogin._Original_InTime = objLogin._checkintime
            objLogin._Checkouttime = Nothing
            objLogin._Original_OutTime = Nothing
            objLogin._SourceType = enInOutSource.Manual
            objLogin._InOutType = 0
            objLogin._Workhour = 0
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId
            SetAtValue()

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'objLogin.Insert(Session("Database_Name") _
            '                                                      , CInt(Session("UserId")) _
            '                                                      , Session("Fin_year") _
            '                                                      , Session("CompanyUnkId") _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , Session("UserAccessModeSetting") _
            '                                                      , True, Session("IsIncludeInactiveEmp") _
            '                                                      , Session("PolicyManagementTNA") _
            '                                                      , Session("DonotAttendanceinSeconds") _
            '                                                      , Session("FirstCheckInLastCheckOut") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , Session("IsDayOffConsiderOnWeekend") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , False, -1, Nothing, "", "")

            objLogin.Insert(Session("Database_Name") _
                                                                  , CInt(Session("UserId")) _
                                                                  , Session("Fin_year") _
                                                                  , Session("CompanyUnkId") _
                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , Session("UserAccessModeSetting") _
                                                                  , True, Session("IsIncludeInactiveEmp") _
                                                                  , Session("PolicyManagementTNA") _
                                                                  , Session("DonotAttendanceinSeconds") _
                                                                  , Session("FirstCheckInLastCheckOut") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , False, -1, Nothing, "", "")

            'Pinkal (20-Aug-2020) -- End

            CheckButtonVisibility()
            Fill_Employee_List()
        Catch ex As Exception
            cmsg.DisplayError("btnCheckIn_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
        Try
            Dim dsList As DataSet
            objLogin._Employeeunkid = mintEmployeeUnkId
            objLogin._Logindate = ConfigParameter._Object._CurrentDateAndTime.Date
            dsList = objLogin.GetList("List", True)
            Dim drCheckIn() As DataRow = dsList.Tables(0).Select("checkouttime IS NULL AND inouttype = 0 ")
            For Each dr In drCheckIn
                objLogin._Loginunkid = CInt(dr("loginunkid").ToString)
                Exit For
            Next

            objLogin._Checkouttime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))
            objLogin._Original_OutTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & ConfigParameter._Object._CurrentDateAndTime.ToString("HH:mm"))
            objLogin._SourceType = enInOutSource.Manual
            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
            objLogin._Workhour = CInt(wkmins)
            objLogin._InOutType = 1
            objLogin._Holdunkid = 0
            objLogin._Voiddatetime = Nothing
            objLogin._Isvoid = False
            objLogin._Voidreason = ""
            objLogin._LoginEmployeeUnkid = mintEmployeeUnkId

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If cboShift.SelectedValue IsNot Nothing AndAlso CInt(cboShift.SelectedValue) > 0 Then
                objLogin._Shiftunkid = CInt(cboShift.SelectedValue)
            Else
                objLogin._Shiftunkid = 0
            End If
            'Pinkal (27-Aug-2020) -- End

            SetAtValue()


            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'objLogin.Update(Session("Database_Name") _
            '                                                      , CInt(Session("UserId")) _
            '                                                      , Session("Fin_year") _
            '                                                      , Session("CompanyUnkId") _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , eZeeDate.convertDate(Session("EmployeeAsOnDate")) _
            '                                                      , Session("UserAccessModeSetting") _
            '                                                      , True, Session("IsIncludeInactiveEmp") _
            '                                                      , Session("PolicyManagementTNA") _
            '                                                      , Session("DonotAttendanceinSeconds") _
            '                                                      , Session("FirstCheckInLastCheckOut") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , Session("IsDayOffConsiderOnWeekend") _
            '                                                      , Session("IsHolidayConsiderOnWeekend") _
            '                                                      , False, -1, Nothing, "", "")

            objLogin.Update(Session("Database_Name") _
                                                                  , CInt(Session("UserId")) _
                                                                  , Session("Fin_year") _
                                                                  , Session("CompanyUnkId") _
                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()) _
                                                                  , Session("UserAccessModeSetting") _
                                                                  , True, Session("IsIncludeInactiveEmp") _
                                                                  , Session("PolicyManagementTNA") _
                                                                  , Session("DonotAttendanceinSeconds") _
                                                                  , Session("FirstCheckInLastCheckOut") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , Session("IsDayOffConsiderOnWeekend") _
                                                                  , Session("IsHolidayConsiderOnWeekend") _
                                                                  , False, -1, Nothing, "", "")

            'Pinkal (20-Aug-2020) -- End

            CheckButtonVisibility()
            Fill_Employee_List()
        Catch ex As Exception
            cmsg.DisplayError("btnCheckOut_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridView's Events "
    Protected Sub dgvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmp.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(0).Text <> "&nbsp;" Then
                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    'SetDateFormat()
                    'Pinkal (13-Aug-2020) -- End
                    e.Row.Cells(0).Text = CDate(e.Row.Cells(0).Text).ToShortDateString()
                End If
            End If
        Catch ex As Exception
            cmsg.DisplayError("dgvEmp_RowDataBound Event : " & ex.Message, Me)
        End Try
    End Sub
#End Region


    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblTotalWorkedHour.Text = Language._Object.getCaption(Me.lblTotalWorkedHour.ID, Me.lblTotalWorkedHour.Text)
            Me.txtTotalWorkedHour.Text = Language._Object.getCaption(Me.txtTotalWorkedHour.ID, Me.txtTotalWorkedHour.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.ID, Me.lblShift.Text)

            Me.dgvEmp.Columns(0).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(0).FooterText, Me.dgvEmp.Columns(0).HeaderText)
            Me.dgvEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
            Me.dgvEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)
            Me.dgvEmp.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(3).FooterText, Me.dgvEmp.Columns(3).HeaderText)


            Me.btnCheckIn.Text = Language._Object.getCaption(Me.btnCheckIn.ID, Me.btnCheckIn.Text).Replace("&", "")
            Me.btnCheckOut.Text = Language._Object.getCaption(Me.btnCheckOut.ID, Me.btnCheckOut.Text).Replace("&", "")
        Catch ex As Exception
            cmsg.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (13-Aug-2020) -- End
   
   
End Class

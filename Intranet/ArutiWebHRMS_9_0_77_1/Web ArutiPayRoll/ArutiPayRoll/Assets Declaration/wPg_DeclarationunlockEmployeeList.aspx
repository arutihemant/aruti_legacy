﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_DeclarationunlockEmployeeList.aspx.vb" 
Inherits="Assets_Declaration_wPg_AssetDecunlockEmployee" Title="Unlock Employee"  %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblPageHeader2" runat="server" Text="Unlock Employee"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div2" class="panel-body-default">
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="300px" ScrollBars="Auto">
                                        <asp:GridView ID="GvLockEmployeeList" DataKeyNames="adlockunkid" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" >
                                            <Columns>
                                             <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                  </asp:TemplateField>
                                                <asp:BoundField DataField="Employee" HeaderText="Employee" HeaderStyle-Width="70%" FooterText = "colhEmployee"></asp:BoundField>
                                                <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock DateTime" HeaderStyle-Width="25%" FooterText ="colhLockDateTime"> </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnUnlock" runat="server" CssClass="btndefault" Text="Unlock" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  <uc2:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message=""/>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_PayrollReport.aspx.vb" Inherits="Payroll_wPg_PayrollReport" 
    Title="Payroll Report" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

   <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
   </script>
            
    

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%; vertical-align:top" >
                                            <td style="width: 50%; padding-right:10px">
                                                <table width="100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="ddlReportType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%;" colspan="2">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%;" colspan="2">
                                                            <asp:DropDownList ID="cboBranch" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="drpPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblToPeriod" runat="server" Text="To Period" Visible="False"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%;" colspan="2">
                                                            <asp:DropDownList ID="drpPeriodTo" runat="server" Visible="false" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:Label ID="lblExRate" runat="server" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkIgnorezeroHead" runat="server" Text="Ignore Zero Value Heads" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkIncludeEmployerContribution" runat="server" Text="Include Employer Contribution" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkShowPaymentDetails" runat="server" Text="Show Payment Details" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkShowLoansInSeparateColumns" runat="server" Text="Show Loans In Separate Columns" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkShowSavingsInSeparateColumns" runat="server" Text="Show Savings In Separate Columns" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkShowCompanyLogoOnReport" runat="server" Text="Show Company Logo" />
                                                        </td>
                                                    </tr>
                                                     <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblCostCenterCode" runat="server" Text="Cost Center Code" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:TextBox ID="txtCostCenterCode" runat="server" Visible="false"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblMembership" runat="server" Text="Membership" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="cboMembership" runat="server" Visible="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkShowEmpNameinSeperateColumn" runat="server" Text="Show Emp Name in Seperate Columns" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%; vertical-align:text-top; padding-top:10px">
                                                            <asp:Label ID="lblIncludeHeads" runat="server" Text="Include Allowance, Other Earning and Other Deduction Heads"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%; overflow:auto; margin-top:10px" colspan="2">
                                                            <div id="scrollable-container" style="margin-top: 10px; height: 680px; overflow: auto"
                                                                onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                <%--<asp:Panel ID="pnl_lvTranHead" Style="margin-top: 10px" Height="680px" ScrollBars="Auto"
                                                                    runat="server">--%>
                                                                <asp:GridView ID="lvTranHead" Style="overflow: auto; margin: auto" runat="server"
                                                                    AutoGenerateColumns="False" ShowFooter="False" Width="99%" CssClass="gridview"
                                                                    HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false">
                                                                <Columns>
                                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="tranheadunkid" HeaderText="tranheadunkid" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                        FooterText="colhCode" />
                                                                    <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                        FooterText="colhName" />
                                                                </Columns>
                                                            </asp:GridView>
                                                                <%--</asp:Panel>--%>
                                                            </div>
                                                            <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%; border-left: 2px solid #DDD; padding-left:10px">
                                                <asp:UpdatePanel ID="uppnlCustomReport" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div id="Div1" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblCustomSettings" runat="server" Text="Custom Settings"></asp:Label>
                                                            </div>
                                                            <div style="text-align: right">
                                                                <asp:LinkButton ID="lnkCustomSettings" runat="server" Text="Save Settings" CssClass="lnkhover"></asp:LinkButton>
                                                        </div>
                                                        </div>
                                                        <%-- <asp:Label ID="" Text="" runat="server" class="heading1"
                                                            Style="width: 100%; text-align: left" />--%>
                                                        <%--<asp:LinkButton ID="" runat="server" Text="" Style="float: right;
                                                            padding-right: 10px; padding-bottom: 6px" />--%>
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%; vertical-align:top">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container1" style="margin-top: 10px; margin-bottom: 30px; height: 400px;
                                                                        overflow: auto" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                        <%--<asp:Panel ID="pnl_lvAllocation_hierarchy" Style="margin-top: 10px; margin-bottom: 30px"
                                                                            Height="400px" ScrollBars="Auto" runat="server">--%>
                                                                        <asp:GridView ID="lvAllocation_hierarchy" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                        DataKeyNames="Id">
                                                                        <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkAllocationSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllocationSelectAll_OnCheckedChanged" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkAllcationSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                        AutoPostBack="true" OnCheckedChanged="chkAllocationSelect_OnCheckedChanged" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Name" HeaderText="Allocation/other Details" ReadOnly="true"
                                                                                FooterText="colhAllocation" />
                                                                            <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" />
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                                        Text="&#x25B2;" OnClick="ChangeAllocationLocation" />
                                                                                    <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                                        Text="&#x25BC;" OnClick="ChangeAllocationLocation" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                        <%-- </asp:Panel>--%>
                                                                    </div>
                                                                    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100%">
                                                                    <table style="width: 100%">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 30%">
                                                                                <asp:Label ID="lblTrnHeadType" runat="server" Text="Transaction Head Type"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%; vertical-align:top">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container2" style="height: 400px; overflow: auto" onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                        <%--<asp:Panel ID="pnl_lvCustomTranHead" Height="400px" ScrollBars="Auto" runat="server">--%>
                                                                        <asp:GridView ID="lvCustomTranHead" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                            DataKeyNames="tranheadunkid">
                                                                        <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                        OnCheckedChanged="chkHeadSelect_OnCheckedChanged" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                FooterText="colhAllocation" />
                                                                            <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                FooterText="colhAllocation" />
                                                                            <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                                        Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                                                    <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                                        Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                        <%--</asp:Panel>--%>
                                                                    </div>
                                                                    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btnDefault" ValidationGroup="Payslip" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" ValidationGroup="Payslip"
                                            CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        <uc9:Export runat="server" ID="Export" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="ddlReportType" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="drpPeriod" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="drpPeriodTo" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
</asp:Content>

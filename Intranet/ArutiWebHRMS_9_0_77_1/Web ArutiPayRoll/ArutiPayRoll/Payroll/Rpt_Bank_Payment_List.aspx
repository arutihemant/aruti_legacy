﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Bank_Payment_List.aspx.vb"
    Inherits="Payroll_Rpt_Bank_Payment_List" Title="Bank Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 75%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Bank Payment List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="4">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployeeName" runat="server" Text="Emp.Name"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="4">
                                                <asp:DropDownList ID="cboEmployeeName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%-- <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 25%">
                                            </td>
                                        </tr>--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCompanyBankName" runat="server" Text="Company Bank Name"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboCompanyBankName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCompanyBranchName" runat="server" Text="Company Bank Branch"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCompanyBranchName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCompanyAccountNo" runat="server" Text="Company Account No."></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboCompanyAccountNo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportMode" runat="server" Text="Report Mode"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboReportMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No."></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboChequeNo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBankName" runat="server" Text="Emp.Bank Name"></asp:Label>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:DropDownList ID="cboCondition" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 27%">
                                                <asp:DropDownList ID="cboBankName" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBranchName" runat="server" Text="Branch Name"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboBranchName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCountryName" runat="server" Text="Country Name"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboCountry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Paid Currency"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCurrency" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <asp:Panel ID="pnlOtherSetting" runat="server" Visible="false">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPresentDays" runat="server" Text="Present Days"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboPresentDays" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBasicSalary" runat="server" Text="Basic Salary"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboBasicSalary" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblSocialSecurity" runat="server" Text="Social Security"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboSocialSecurity" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblExtraIncome" runat="server" Text="Extra Income"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboExtraIncome" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAbsentDays" runat="server" Text="Absent Deduction"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboAbsentDays" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblIdentityType" runat="server" Text="Identity Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="txtIdentityType" runat="server">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="txtPaymentType" runat="server">
                                                </asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCustomCurrFormat" runat="server" Text="Custom Curr. Format"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="txtCustomCurrFormat" runat="server">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        </asp:Panel>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmountTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCutOffAmount" runat="server" Text="Cut Off Amount"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:TextBox ID="txtCutOffAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPostingDate" runat="server" Text="Posting Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl ID="dtpPostingDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblMembershipRepo" runat="server" Text="Membership"></asp:Label>
                                            </td>
                                            <td style="width: 35%" colspan="2">
                                                <asp:DropDownList ID="cboMembershipRepo" runat="server">
                                                </asp:DropDownList>
                                            </td>                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkSignatory1" runat="server" Text="Show Signatory 1"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkDefinedSignatory" runat="server" Text="Show Defined Signatory">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkSignatory2" runat="server" Text="Show Signatory 2"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkDefinedSignatory2" runat="server" Text="Show Defined Signatory 2">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkSignatory3" runat="server" Text="Show Signatory 3"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkDefinedSignatory3" runat="server" Text="Show Defined Signatory 3">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkShowGroupByBankBranch" runat="server" Text="Show Group By Bank/Branch"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowBankCode" runat="server" Text="Show Bank Code"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkEmployeeSign" runat="server" Text="Show Employee Sign"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowBranchCode" runat="server" Text="Show Branch Code"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="5">
                                                <asp:CheckBox ID="chkShowFNameSeparately" runat="server" Text="Show First Name,Other Name and Surname in Separate Columns">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkShowPayrollPeriod" runat="server" Text="Show Payroll Period"
                                                    Checked="true"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkSaveAsTXT_WPS" runat="server" Text="Save As TXT"
                                                    Checked="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkShowSortCode" runat="server" Text="Show Sort Code" Checked="true">
                                                </asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowAccountType" runat="server" Text="Show Account Type" Checked="false">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkShowEmployeeCode" runat="server" Text="Show Employee Code" Checked="true"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowReportHeader" runat="server" Text="Show Report Header" Checked="true">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkLetterhead" runat="server" Text="Letterhead" Checked="false"
                                                    AutoPostBack="false"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkAddresstoEmployeeBank" runat="server" Text="Address to Employee Bank" Checked="false">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <%--'Hemant (20 Jul 2019) -- Start--%>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:CheckBox ID="chkShowSelectedBankInfo" runat="server" Text="Show Selected Bank Info" Checked="false"
                                                    AutoPostBack="false"></asp:CheckBox>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowCompanyGrpInfo" runat="server" Text="Show Company Group Info" Checked="false">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <%--'Hemant (20 Jul 2019) -- End--%>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="3">
                                                <asp:LinkButton ID="lnkEFTCityBankExport" runat="server" Text="EFT City Direct Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_CBA_Export" runat="server" Text="EFT CBA Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_EXIM_Export" runat="server" Text="EFT EXIM Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_Custom_CSV_Export" runat="server" Text="EFT Custom CSV Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_Custom_XLS_Export" runat="server" Text="EFT Custom XLS Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_FlexCubeRetailGEFU_Export" runat="server" Text="EFT Flex Cube Retail - GEFU Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_ECO_Bank" runat="server" Text="EFT ECO Bank..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkBankPaymentLetter" runat="server" Text="Bank Payment Letter..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTBarclaysBankExport" runat="server" Text="Barclays Bank Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTNationalBankMalawi" runat="server" Text="EFT National Bank Malawi..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_FNB_Bank_Export" runat="server" Text="EFT FNB Bank Export..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTStandardCharteredBank_S2B" runat="server" Text="EFT Standard Chartered Bank(S2B)..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFT_ABSA_Bank" runat="server" Text="EFT ABSA Bank..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTNationalBankMalawiXLSX" runat="server" Text="EFT National Bank Malawi XLSX..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTEquityBankKenya" runat="server" Text="EFT Equity Bank Kenya..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTNationalBankKenya" runat="server" Text="EFT National Bank Kenya..."></asp:LinkButton>
                                                <asp:LinkButton ID="lnkEFTCitiBankKenya" runat="server" Text="EFT City Bank Kenya..."></asp:LinkButton>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:LinkButton ID="lnkMobileMoneyEFTMPesaExport" runat="server" Text="EFT MPesa Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_EFTCustom" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnEFTClose" PopupControlID="pnl_EFTCustom" TargetControlID="HiddenField1">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_EFTCustom" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="EFT Custom Columns Export" />
                            </div>
                            <div class="panel-body">
                                <div id="Div10" class="panel-default">
                                    <div id="Div11" class="panel-heading-default">
                                        <div style="float: left;">
                                        </div>
                                    </div>
                                    <div id="Div12" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td colspan="2" style="width: 100%">
                                                    <div style="max-height: 400px; overflow: auto">
                                                        <asp:GridView ID="lvEFTCustomColumns" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="False" Width="99%" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            CssClass="gridview" DataKeyNames="ID">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                    FooterText="objcolhNCheck">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" ToolTip="All"
                                                                            OnCheckedChanged="lvEFTCustomColumns_ItemChecked" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                            AutoPostBack="true" ToolTip="Checked" OnCheckedChanged="lvEFTCustomColumns_ItemChecked" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Name" HeaderText="EFT Custom Columns" ReadOnly="true"
                                                                    FooterText="colhEFTCustomColumns" />
                                                                <asp:BoundField DataField="ID" ReadOnly="true" Visible="false" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                            Text="&#x25B2;" OnClick="ChangeLocation" />
                                                                        <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                            Text="&#x25BC;" OnClick="ChangeLocation" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="2" style="width: 100%">
                                                    <asp:CheckBox ID="chkShowColumnHeader" runat="server" Text="Show Column Header on Report" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblMembership" runat="server" Text="Membership"></asp:Label>
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:DropDownList ID="cboMembership" runat="server" AutoPostBack="false" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <%--'Hemant (02 Jul 2020) -- Start--%>
                                            <tr style="width: 100%">
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblDateFormat" runat="server" Text="Date Format"></asp:Label>
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:TextBox ID="txtDateFormat" runat="server" >
                                                    </asp:TextBox>
                                                </td>
                                            </tr>             
                                           <%--'Hemant (02 Jul 2020) -- End--%>
                                           <%-- 'Hemant (27 June 2019) -- Start--%>
                                           <tr style="width: 100%">
                                                <td  style="width: 35%">
                                                    <asp:CheckBox ID="chkSaveAsTXT" runat="server" Text="Save As TXT" />
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:CheckBox ID="chkTABDelimiter" runat="server" Text="TAB Delimiter" />
                                                </td>
                                            </tr>
                                           <%-- 'Hemant (27 June 2019) -- End--%>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnEFTSaveSelection" runat="server" Text="Save Selection" CssClass="btnDefault" />
                                            <asp:Button ID="btnEFTOK" runat="server" Text="OK" CssClass="btnDefault" />
                                            <asp:Button ID="btnEFTClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

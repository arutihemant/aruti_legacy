﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_BatchPostingList.aspx.vb" Inherits="Payroll_wPg_BatchPostingList"
    Title="Batch Posting List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="Delete" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
//    function pageLoad(sender, args) {
//        $("select").searchable();

//        alert('hi');
//        $('#imgclose').click(function(evt) {
//            $find('<%= popupAddEdit.ClientID %>').hide();
//        });
//    }

    function closePopup() {
        $('#imgclose').click(function(evt) {
            $find('<%= popupAddEdit.ClientID %>').hide();        
        });
    }

    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
    
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Batch Posting List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 8%;">
                                                <%--<asp:Label ID="lblBatchPostingNo" runat="server" Text="Batch #"></asp:Label>--%>
                                                <asp:Label ID="lblBatch" runat="server" Text="Batch"></asp:Label>
                                            </td>
                                            <td style="width: 24%">
                                                <%--<asp:TextBox ID="txtlstBatchNo" runat="server"></asp:TextBox>--%>
                                                <asp:DropDownList ID="cboBatch" runat="server"></asp:DropDownList>
                                            </td>
                                            <td style="width: 8%;">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Pay Period" Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 24%">
                                                <asp:DropDownList ID="cboPayPeriod" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%;">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 24%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnlstReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <asp:Panel ID="pbl_dgBatchPostingList" runat="server" ScrollBars="Auto" Height="500px">
                                    <asp:GridView ID="dgBatchPostingList" runat="server" AutoGenerateColumns="false"
                                        AllowPaging="false" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                            CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                                    </span>
                                                    <%--  <asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPostedToED" runat="server" Text="Post To ED" Font-Underline="false"
                                                        CommandName="PostED" CommandArgument="<%# Container.DataItemIndex %>" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkVoidBatchPosting" runat="server" Text="Void Batch Posting" Font-Underline="false"
                                                        CommandName="VoidBatchPosting" CommandArgument="<%# Container.DataItemIndex %>" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="batchno" HeaderText="Batch No" ReadOnly="True" FooterText="colhBatchNo" />
                                            <asp:BoundField DataField="batchcode" HeaderText="Batch Code" ReadOnly="True" FooterText="colhBatchCode" />
                                            <asp:BoundField DataField="batchname" HeaderText="Batch Name" ReadOnly="True" FooterText="colhBatchName" />
                                            <asp:BoundField DataField="PeriodName" HeaderText="Pay Period" ReadOnly="True" FooterText="colhPeriod" />
                                            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" ReadOnly="True"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                            <asp:BoundField DataField="IsPosted" HeaderText="Is Posted" ReadOnly="True" FooterText="colhPosted" />
                                            <asp:BoundField DataField="user_name" HeaderText="User Name" ReadOnly="True" FooterText="colhUser" />
                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True" FooterText="colhDesc" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                        PopupControlID="pnlAddEdit" BackgroundCssClass="ModalPopupBG" DropShadow="false"
                        CancelControlID="btnClose">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlAddEdit" Width="850px" Height="650px" runat="server" Style="display: none;"
                        CssClass="newpopup">
                        <div class="panel-primary">
                            <div class="panel-heading">
                                <asp:Label ID="gbBatchPosting" runat="server" Text="Batch Posting Information"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label2" runat="server" Text="Batch Posting Information Add/Edit"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblBatchPostingNo" runat="server" Text="Batch Posting #"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:TextBox ID="txtBatchPostingNo" runat="server"></asp:TextBox>
                                                </td>
                                                
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblBatchCode" runat="server" Text="Batch Code" Visible="false"></asp:Label>
                                                     <asp:Label ID="lblBatchName" runat="server" Text="Batch Name"></asp:Label>
                                                </td>
                                                <td style="width: 20%" colspan="3">
                                                    <asp:TextBox ID="txtBatchCode" runat="server" Visible="false"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtBatchCode" ErrorMessage="Please enter Batch Code. "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit"
                                                                SetFocusOnError="True" Visible="False"></asp:RequiredFieldValidator>
                                                     <asp:TextBox ID="txtBatchName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtBatchName" ErrorMessage="Please enter Batch Name. "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit"
                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </td>
                                                
                                                <%--<td style="width: 13%;">
                                                    <asp:Label ID="lblBatchName" runat="server" Text="Batch Name"></asp:Label>
                                                </td>--%>
                                                <%--<td style="width: 20%">
                                                    <asp:TextBox ID="txtBatchName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtBatchName" ErrorMessage="Please enter Batch Name. "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit"
                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </td>--%>
                                              </tr>
                                                
                                              <tr>
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" Width="134px">
                                                    </asp:DropDownList>
                                                </td>
                                                
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:UpdatePanel ID="UpdateEmployeeCode" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <%--<asp:TextBox ID="txtEmployeeCode" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtEmployeeCode" ErrorMessage="Please enter Employee Code. "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit"
                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="cboEmployee" runat="server" Width="134px"></asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                            <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblTranHead" runat="server" Text="Tran. Head"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:UpdatePanel ID="UpdateTranhead" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <%--<asp:TextBox ID="txtTranHeadCode" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtTranHeadCode" ErrorMessage="Please enter Transaction Head Code. "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit"
                                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                                            <asp:DropDownList ID="cboTranhead" runat="server" Width="134px"></asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                            <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 9%;">
                                                    <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:UpdatePanel ID="UpdateAmount" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                                ControlToValidate="txtAmount" ErrorMessage="Please enter Amount. " CssClass="ErrorControl"
                                                                ForeColor="White" Style="z-index: 1000" ValidationGroup="AddEdit" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                            <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                                                                                
                                                <td style="width: 13%;">
                                                    <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="AddEdit" />
                                            <asp:Button ID="btnEdit" runat="server" Text="Update" CssClass="btnDefault" ValidationGroup="AddEdit" />
                                            <asp:Button ID="btnDelete" runat="server" Text="Reset" CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="Server" />
                                        </div>
                                    </div>
                                    <div class="panel-body-default">
                                        <asp:UpdatePanel ID="UpdateBatchPosting" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="pnl_dgBatchPosting" runat="server" ScrollBars="Auto" Height="323px">
                                                    <asp:GridView ID="dgBatchPosting" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                        Width="99%" CellPadding="3" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText=""
                                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                                            CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText=""
                                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                            CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="periodname" HeaderText="Period" ReadOnly="True" FooterText="colhPeriod" />
                                                            <%--<asp:BoundField DataField="employeecode" HeaderText="Employee Code" ReadOnly="True" FooterText="colhEmpCode" />--%>
                                                            <asp:BoundField DataField="EmpName" HeaderText="Employee Name" ReadOnly="True" FooterText="colhEmpName" />
                                                            <%--<asp:BoundField DataField="trnheadcode" HeaderText="Head Code" ReadOnly="True" FooterText="colhTranHeadCode" />--%>
                                                            <asp:BoundField DataField="TranHeadName" HeaderText="Transaction Head Name" ReadOnly="True" FooterText="colhTranHeadName" />
                                                            <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnEdit" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="dgBatchPosting" EventName="RowCommand" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupPostED" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnCancel" PopupControlID="pnlPostED" TargetControlID="HiddenField2">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlPostED" runat="server" Style="display: none; padding: 10px; width: 300px;
                        border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                        -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                        -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                        BackColor="#5377A9" DefaultButton="btnPost">
                        <div>
                            <asp:Label ID="lblTitle" runat="server" Text="Post To ED" Width="270px" ForeColor="White"
                                Style="margin-left: 5px;" />
                        </div>
                        <hr style="display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em;
                            -webkit-margin-start: auto; -webkit-margin-end: auto; border-style: inset; border-width: 1px;" />
                        <div style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                            margin-bottom: 10px;">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdateOverWriteIfExist" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkOverwriteIfExist" runat="server" Text="Overwrite if selected head exist"
                                                    ForeColor="White" />
                                                <br />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkCopyPreviousEDSlab" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous ED Slab"
                                            AutoPostBack="true" ForeColor="White" /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:CheckBox ID="chkOverwritePrevEDSlabHeads" runat="server" Text="Overwrite Previous ED Slab heads"
                                                    ForeColor="White" />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkCopyPreviousEDSlab" EventName="CheckedChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="btnbar">
                            <asp:Button ID="btnPost" runat="server" Text="Post" Width="70px" CssClass="btnDefault" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="70px" CssClass="btnDefault" />
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                        </div>
                    </asp:Panel>
                    <uc4:Delete ID="popupDelete" runat="server" Title="Are you sure you want to delete selected transactions ?" />
                    <uc5:Confirmation ID="popupCopyPrevious" runat="server" Message="You have ticked Copy Previous ED Slab. This will copy Previous Period Slab.Do you want to continue?"
                        Title="Confirmation" />
                    <uc5:Confirmation ID="popupPost" runat="server" Message="Are you sure you want to Post selected transactions to earning deduction?"
                        Title="Confirmation" />
                    <uc5:Confirmation ID="popupVoidBatchConfirm" runat="server" Message="Are you sure you want to Void Posting of selected batch to ED?"
                        Title="Confirmation" />
                    <uc4:Delete ID="popupVoidBatchPosting" runat="server" Title="Are you sure you want to Void Posting of selected batch to ED?" ValidationGroup="vbp"  />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

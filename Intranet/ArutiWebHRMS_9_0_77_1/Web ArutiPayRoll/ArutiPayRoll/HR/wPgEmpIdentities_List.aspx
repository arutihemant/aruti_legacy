﻿<%@ Page Title="Employee Identity List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmpIdentities_List.aspx.vb" Inherits="wPgEmpIdentities_List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Identity List"></asp:Label>
                        </div>
                        <div class="panel-body" style="text-align: left">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%" style="text-align: left">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <%--<cc1:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="drpEmployee"
                                                    IsSorted="true" QueryTimeout="5" QueryPattern="Contains" PromptCssClass="ListSearchExtenderPrompt" />--%>
                                            </td>
                                            <td style="width: 85%" colspan="4">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 34%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 2%">
                                            </td>
                                            <td style="width: 15%; padding-left: 50px">
                                                <asp:Label ID="lblIdType" runat="server" Text="Id. types"></asp:Label>
                                            </td>
                                            <td style="width: 34%">
                                                <asp:DropDownList ID="cboIdType" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                         <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        
                                        
                                        <%--'Gajanan [22-Feb-2019] -- Start--%>
                                        <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                            <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                        <%--'Gajanan [22-Feb-2019] -- End--%>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="PnlGrid" runat="server" Width="99%">
                                <asp:DataGrid ID="dgvIdentity" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                    Width="100%">
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Select"
                                                        CommandName="Select"></asp:LinkButton>
                                                    <%--'Gajanan [22-Feb-2019] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                        Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                    <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                    <%--'Gajanan [22-Feb-2019] -- End--%>
                                                </span>
                                                <%-- <asp:ImageButton ID="ImgSelect" runat="server" ImageUrl="~/images/edit.png" ToolTip="Select"
                                                        CommandName="Select" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--0--%>
                                        <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" oolTip="Delete"
                                                        CommandName="Delete"></asp:LinkButton>
                                                </span>
                                                <%--  <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--1--%>
                                        <asp:BoundColumn DataField="identities" HeaderText="Identity Type" ReadOnly="True"
                                            FooterText="colhIdType"></asp:BoundColumn>
                                        <%--2--%>
                                        <asp:BoundColumn DataField="serial_no" HeaderText="Serial No" ReadOnly="True" FooterText="colhIdSerialNo">
                                        </asp:BoundColumn>
                                        <%--3--%>
                                        <asp:BoundColumn DataField="identity_no" HeaderText="Identity No" ReadOnly="True"
                                            FooterText="colhIdNo"></asp:BoundColumn>
                                        <%--4--%>
                                        <asp:BoundColumn DataField="country" HeaderText="Country" ReadOnly="True" FooterText="colhIssueCountry">
                                        </asp:BoundColumn>
                                        <%--5--%>
                                        <asp:BoundColumn DataField="issued_place" HeaderText="Place of Issue" ReadOnly="True"
                                            FooterText="colhPlaceOfIssue"></asp:BoundColumn>
                                        <%--6--%>
                                        <asp:BoundColumn DataField="issue_date" HeaderText="IssueDate" ReadOnly="True" FooterText="colhIssueDate">
                                        </asp:BoundColumn>
                                        <%--7--%>
                                        <asp:BoundColumn DataField="expiry_date" HeaderText="ExpiryDate" ReadOnly="True"
                                            FooterText="colhExpiryDate"></asp:BoundColumn>
                                        <%--8--%>
                                        <asp:BoundColumn DataField="isdefault" HeaderText="isdefault" ReadOnly="True" Visible="false">
                                        </asp:BoundColumn>
                                        <%--9--%>
                                        <%--'Gajanan [22-Feb-2019] -- Start--%>
                                        <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                        <asp:BoundColumn DataField="operationtypeid" HeaderText="operationtypeid" ReadOnly="true"
                                            Visible="false"></asp:BoundColumn>
                                        <%--10--%>
                                        <asp:BoundColumn DataField="operationtype" HeaderText="Operation Type" ReadOnly="true">
                                        </asp:BoundColumn>
                                        <%--11--%>
                                        <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" ReadOnly="true" Visible="false">
                                        </asp:BoundColumn>
                                        <%--12--%>
                                        <asp:BoundColumn DataField="identitytranunkid" HeaderText="identitytranunkid" Visible="false">
                                        </asp:BoundColumn>
                                        <%--<13>--%>
                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false">
                                        </asp:BoundColumn>
                                        <%--<14>--%>
                                        <%--'Gajanan [22-Feb-2019] -- End--%>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                    <%--'Gajanan [22-Feb-2019] -- Start--%>
                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <%--'Gajanan [22-Feb-2019] -- End--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="OrganizationChart.aspx.vb"
    Inherits="OrgChart_OrganizationChart" Title="View Organization Chart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script>
       function openwin() {
          debugger;

            var popUp = window.open('../OrganizationChart/Chart.aspx', '_blank');
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please disable your pop-up blocker and click on link again.');
            }
            else {
                popUp.focus();
            }
            }
    </script>

    <asp:UpdatePanel ID="uppnl_mianMSS" runat="server">
        <ContentTemplate>
            <div class="panel-primary">
                <div class="panel-heading">
                    <asp:Label ID="lblPageHeader" runat="server" Text="Organization Chart"></asp:Label>
                </div>
                <div class="panel-body">
                    <div id="Div2" class="panel-default">
                        <div id="Div3" class="panel-heading-default">
                            <div style="float: left;">
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </div>
                            <div style="text-align: right;">
                                <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" Visible="false"
                                    CssClass="lnkhover" Style="color: Blue; vertical-align: top;"></asp:LinkButton>
                            </div>
                        </div>
                        <div id="Div4" class="panel-body-default" style="position: relative">
                            <div class="row2">
                                <div class="ibwm" style="width: 15%">
                                    <asp:Label ID="lblcharttype" runat="server" Text="View Type"></asp:Label>
                                </div>
                                <div class="ibwm" style="width: 30%">
                                    <asp:DropDownList ID="cbocharttype" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="ibwm" style="width: 15%">
                                    <asp:CheckBox ID="chkViewFilters" runat="server" Text="View Filters On Chart?" Visible="false" />
                                </div>
                                <div class="ibwm" style="width: 35%">
                                    <asp:CheckBox ID="chkViewJobdetail" runat="server" Text="View Planned Position, Head Count, Position Variance?"
                                        Visible="false" />
                                </div>
                            </div>
							
							<asp:Panel ID="pnlOrganizationChartFilter" runat="server" Visible="false">
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblOrganizationChartDeptGroup" runat="server" Text="Branch"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="drpOrganizationChartDeptGroup" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                               
                            </asp:Panel>	
							
							
                            <asp:Panel ID="pnljobFilter" runat="server" Visible="false">
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblbranch" runat="server" Text="Branch"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbobranch" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblunitgroup" runat="server" Text="Unit Group"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbounitgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lbldeptgroup" runat="server" Text="Dept. Group"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbodeptgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblunit" runat="server" Text="Units"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbounit" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lbldepartment" runat="server" Text="Department"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbodepartment" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblteam" runat="server" Text="Team"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboteam" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblsectiongroup" runat="server" Text="Sec. Group"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbosectiongroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblclassgroup" runat="server" Text="Class Group"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboclassgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblsection" runat="server" Text="Sections"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cbosection" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblclass" runat="server" Text="Class"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboclass" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblJobGroup" runat="server" Text="Job Group"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboJobGroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblGrade" runat="server" Text="Grade"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboGradeLevel" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ibwm" style="width: 15%">
                                        <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                    </div>
                                    <div class="ibwm" style="width: 30%">
                                        <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div id="btnfixedbottom" class="btn-default">
                                <asp:CheckBox ID="chkExpandAll" runat="server" Text="Expand All Level?" Style="float: left" />
                                <asp:Button ID="btnViewchart" runat="server" CssClass="btndefault" Text="View" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

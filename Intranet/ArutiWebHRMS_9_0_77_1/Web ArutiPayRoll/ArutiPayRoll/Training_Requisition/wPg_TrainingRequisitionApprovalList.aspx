﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_TrainingRequisitionApprovalList.aspx.vb"
    Inherits="Training_Requisition_wpg_TrainingRequisitionApprovalList" Title="Training Requisition Approval List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Requisition Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblUser" runat="server" Text="User"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtUser" runat="server" Enabled="False"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lbllevel" runat="server" Text="Level"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtlevel" runat="server" Enabled="False"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblemp" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpemp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Checked="true"
                                                Font-Bold="true" />
                                        </div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="300px">
                                            <asp:GridView ID="gvTrainingApprovalList" runat="server" AutoGenerateColumns="false"
                                                AllowPaging="false" Width="100%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" DataKeyNames="mapuserunkid,priority,mappingunkid,iStatusId,iStatus,employeeunkid,linkedmasterid,uempid,ecompid,crmasterunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText=""
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Change Status"
                                                                    OnClick="lnkChangeStatus_Click" CommandArgument='<%#Eval("masterguid")%>'></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:BoundField DataField="ecode" HeaderText="Code" ReadOnly="true" FooterText="colhecode" />--%>
                                                    <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                    <asp:BoundField DataField="CourseMaster" HeaderText="Training Name" ReadOnly="True"
                                                        FooterText="colhtrainingname" />
                                                    <asp:BoundField DataField="institute_name" HeaderText="Vendor" ReadOnly="True" FooterText="colhvendor" />
                                                    <asp:BoundField DataField="TrainingMode" HeaderText="Training Mode" ReadOnly="True"
                                                        FooterText="colhRefNo" />
                                                    <asp:BoundField DataField="training_startdate" HeaderText="Start Date" ReadOnly="True"
                                                        FooterText="colhstartdate" />
                                                    <asp:BoundField DataField="training_enddate" HeaderText="End Date" ReadOnly="True"
                                                        FooterText="colhenddate" />
                                                    <asp:BoundField DataField="Duration" HeaderText="Duration" ReadOnly="True" FooterText="colhduration" />
                                                    <asp:BoundField DataField="training_venue" HeaderText="Venue" ReadOnly="True" FooterText="colhvenue" />
                                                    <asp:BoundField DataField="additonal_comments" HeaderText="Comments" ReadOnly="True"
                                                        FooterText="colhcomments" />
                                                    <asp:BoundField DataField="iStatus" HeaderText="Status" ReadOnly="True" FooterText="colhstatus" />
                                                    <asp:BoundField DataField="username" HeaderText="Approver" ReadOnly="true" FooterText="colhApprover" />
                                                    <asp:BoundField DataField="levelname" HeaderText="Level" ReadOnly="true" FooterText="colhApprovelevel" />
                                                    <%--'S.SANDEEP [07-NOV-2018] -- START--%>
                                                    <asp:BoundField HeaderText="Training Type" DataField="trainingtype" ItemStyle-VerticalAlign="Top" FooterText="colhtrainingtype" />
                                                    <%--'S.SANDEEP [07-NOV-2018] -- END--%>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" Visible="False" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

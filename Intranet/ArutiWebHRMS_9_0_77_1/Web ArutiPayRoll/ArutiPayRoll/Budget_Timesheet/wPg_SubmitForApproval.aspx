﻿<%@ Page Title="Submit For Approval" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_SubmitForApproval.aspx.vb" Inherits="Budget_Timesheet_wPg_SubmitForApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Submit For Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 7%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 5%">
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 17%">
                                                <uc1:DateCtrl ID="dtpDate" runat="server" />
                                            </td>
                                            <td style="width: 9%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 7%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 5%">
                                            </td>
                                            <td style="width: 7%">
                                            </td>
                                            <td style="width: 17%">
                                            </td>
                                            <td style="width: 9%">
                                            </td>
                                            <td style="width: 35%">
                                            </td>
                                        </tr>
                                        
                                        <%--   'Pinkal (28-Jul-2018) -- Start
                                          'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                         <tr style="width: 100%">
                                           <td style="width: 7%">
                                                 <asp:Label ID = "LblSubmissionRemark" runat = "server" Text = "Remark"></asp:Label>
                                            </td>
                                            <td  colspan = "4">
                                                  <asp:TextBox ID = "txtSubmissionRemark" runat ="server" TextMode="MultiLine" Rows = "2"></asp:TextBox>
                                            </td>
                                         </tr>
                                           <%--  'Pinkal (28-Jul-2018) -- End--%>
                                         
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div id="Div1" class="panel-default">
                                    <div id="Div3" class="panel-body-default">
                                        <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                            max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                            <asp:DataGrid ID="dgvEmpTimesheetList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhSelect" 
                                                        HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" 
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" 
                                                                oncheckedchanged="chkSelectAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" 
                                                                oncheckedchanged="chkSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="IsChecked" FooterText="dgcolhSelect" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Particular" FooterText="dgcolhEmployee" 
                                                        HeaderText="Employee" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activitydate" FooterText="dgcolhDate" 
                                                        HeaderText="Activity Date" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" 
                                                        HeaderText="Donor/Grant" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" 
                                                        HeaderText="Project Code" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" 
                                                        HeaderText="Activity Name" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" 
                                                        HeaderText="Activity Hours" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="approvedactivity_hrs" 
                                                        FooterText="dgcolhApprovedActHrs" HeaderText="Approved Activity Hours" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="description" FooterText="dgcolhDescription" 
                                                        HeaderText="Description" ReadOnly="true"></asp:BoundColumn>
                                                        
                                                        <%--   'Pinkal (28-Jul-2018) -- Start
                                                                   'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                                       <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark" 
                                                        HeaderText="Submission Remark" ReadOnly="true"></asp:BoundColumn>
                                                        
                                                         <%--  'Pinkal (28-Jul-2018) -- End--%>
                                                        
                                                    <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" 
                                                        HeaderText="Status" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="emptimesheetunkid" 
                                                        FooterText="objdgcolhEmpTimesheetID" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectcodeunkid" 
                                                        FooterText="objdgcolhProjectID" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundactivityunkid" FooterText="objdgcolhActivityID" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusId" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isholiday" FooterText="objdgcolhIsHoliday" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isDayOff" FooterText="objdgcolhIsDayOFF" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isLeave" FooterText="objdgcolhIsLeave" 
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ApprovedActivityHoursInMin" 
                                                        FooterText="objdgcolhApprovedActivityHoursInMin" ReadOnly="true" 
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="issubmit_approval" 
                                                        FooterText="objdgcolhIsSubmitForApproval" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ADate" FooterText="objdgcolhADate" ReadOnly="true" 
                                                        Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                        <div id="btnfixedbottom" class="btn-default">
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btndefault" />
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                            <asp:Button ID="btnSubmitForApproval" runat="server" Text="Submit For Approval" CssClass="btndefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:DeleteReason ID="popupDeleteReason" runat="server" />
                    <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Title="Custom Item View" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_CustomeItemView.aspx.vb" Inherits="Assessment_New_Common_wPg_CustomeItemView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;
                
            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;
                
            if (charCode == 13)
                return false;

            if (charCode > 31  && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

<script type="text/javascript">
    var prm;
    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(endRequestHandler);
    function endRequestHandler(sender, args) {
                SetGeidScrolls();
    }
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "none"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Custom Item View"></asp:Label>
                            <div style="display: none">
                                <uc1:DateCtrl ID="dtpdate" runat="server" />
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%;">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:2%"></td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:18%"></td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblCustomHeader" runat="server" Text="Header"></asp:Label></td>                                            
                                            <td colspan="4">
                                                <asp:DropDownList ID="cboHeader" runat="server" AutoPostBack="true">
                                                </asp:DropDownList></td>
                                            <td style="width:18%">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float:left">
                                            <asp:Button ID="btnReport" runat="server" Text="View Report" CssClass="btndefault" />
                                        </div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnclose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top: 10px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_dgvdata" ScrollBars="Auto" Height="400px" Width="100%" runat="server" CssClass="gridscroll">
                                                <asp:GridView ID="dgvData" runat="server" Style="margin: auto" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                                CancelControlID="hdf_cItem" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                PopupControlID="pnl_CItemAddEdit" Drag="false">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="newpopup" Style="display: none;
                                width: 750px">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblCCustomeItem" runat="server" Text="Custome Items"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div10" class="panel-default">
                                            <div id="Div11" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div12" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:Panel ID="pnl_dgv_Citems" runat="server" Style="height: 360px !important" ScrollBars="Auto">
                                                                <asp:DataGrid ID="dgv_Citems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="clmItem"
                                                                            HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                                                        <asp:TemplateColumn FooterText="clmValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                                            ItemStyle-Width="250px"></asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                                                        </asp:BoundColumn>
                                                                        <asp:TemplateColumn FooterText="clmValue" Visible="false"></asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="customitemunkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="isdefaultentry" Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_cItem" runat="server" />
                                                    <asp:Button ID="btnIAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                    <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <cc1:ModalPopupExtender ID="popup_CItemReason" runat="server" BackgroundCssClass="ModalPopupBG"
                                CancelControlID="btnNo" PopupControlID="pnl_cItemReason" TargetControlID="hdf_cItemReason">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_cItemReason" runat="server" CssClass="newpopup" Style="display: none;
                                width: 300px">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblTitle" runat="server" Text="Title" />
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div13" class="panel-default">
                                            <div id="Div15" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:Label ID="lblMessage" runat="server" Text="Message :" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                                    <asp:HiddenField ID="hdf_cItemReason" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>                            
                        </div>
                    </div>
                    <uc2:DeleteReason ID="popupDelete" runat="server" Title="Aruti" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>


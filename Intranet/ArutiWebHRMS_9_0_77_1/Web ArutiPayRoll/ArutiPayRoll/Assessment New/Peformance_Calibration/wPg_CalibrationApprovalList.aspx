﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_CalibrationApprovalList.aspx.vb" Inherits="Assessment_New_Peformance_Calibration_wPg_CalibrationApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <style>
        .vl
        {
            border-left: 1px solid black;
            height: 15px;
        }
    </style>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        function GetSelected() {
            var ischkboxfound = false;
            var grid = document.getElementById("<%=gvFilterRating.ClientID%>");
            var checkBoxes = grid.getElementsByTagName("INPUT");
            $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
            $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i] != undefined && checkBoxes[i].checked) {
                    var row = $(checkBoxes[i]).closest("tr").find("td:eq(1)").attr('title');
                    ischkboxfound = true;
                    var res = row.split(" - ");
                    RangeSearch(parseFloat(res[0]), parseFloat(res[1]));
                }
            }
            if (ischkboxfound == false) {
                $('#<%= gvApplyCalibration.ClientID %> tr').show();
            }
        }

        function RangeSearch(val1, val2) {
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "pScore") {
                        if (parseFloat($(col).html()) >= val1 && parseFloat($(col).html()) <= val2) {
                            $(row).show();
                        }
                    }
                }
            }
        }

        function ListSeaching() {
            if ($('#<%= txtClistSearch.ClientID %>').val().length > 0) {
                $('#<%= gvCalibrateList.ClientID %> tbody tr').hide();
                $('#<%= gvCalibrateList.ClientID %> tbody tr:first').show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtClistSearch.ClientID %>').val() + '\')').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.MainGroupHeaderStyleLeft').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.GroupHeaderStylecompLeft').parent().show();
            }
            else if ($('#<%= txtClistSearch.ClientID %>').val().length == 0) {
                resetListSeaching();
            }
            if ($('#<%= gvCalibrateList.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetListSeaching();
            }
        }
        function resetListSeaching() {
            $('#<%= txtClistSearch.ClientID %>').val('');
            $('#<%= gvCalibrateList.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtClistSearch.ClientID %>').focus();
        }

        function FromSearching() {
            if ($('#<%= txtListSearch.ClientID %>').val().length > 0) {
                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtListSearch.ClientID %>').val() + '\')').parent().show();
            }
            else if ($('#<%= txtListSearch.ClientID %>').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= gvApplyCalibration.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#<%= txtListSearch.ClientID %>').val('');
            $('#<%= gvApplyCalibration.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtListSearch.ClientID %>').focus();
        }
        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {

                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });

        function RatingOperation() {
            var ratingid = 0;
            var rid = document.getElementById("<%=cbocRating.ClientID%>");
            ratingid = parseInt($(rid).val());
            if (ratingid <= 0) {

                swal({ title: '', text: "Sorry, Rating is mandatory information. Please select rating to continue." });
                return;
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }
            var counter = 0;
            var txt = document.getElementById("<%=txtIRemark.ClientID%>").value;
            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    counter++;
                }
            });
            if (counter == 0) {
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }

            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    $(this).closest("tr").find("td[Id='cRem']").html(txt);
                    $(this).closest("tr").find("td[Id='cRate']").html(rid.options[rid.selectedIndex].innerHTML);
                    $(this).closest("tr").find("td[Id='iChng']").html("1");
                }
            });
            document.getElementById("<%=txtIRemark.ClientID%>").value = "";
            rid.selectedIndex = 0;
            swal({ title: '', type: "success", text: "Selected rating applied successfully." });
        }

        function ApproveReject(ival) {
            var flg = false;
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");

            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRate") {
                        if ($(col).html() == '&nbsp;') {
                            flg = true;
                            break;
                        }
                    }
                }
                if (flg == true) { break; }
            }
            if (flg == true) {
                swal({ title: '', text: "Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation." });
                return;
            }

            var amark = document.getElementById("<%=txtApprRemark.ClientID%>").value;

            if (amark.trim().length <= 0 && ival == 0) {
                swal({ title: '', text: "Sorry, remark is mandatory information. Please enter remark to continue." });
                return;
            }
            var txt = "";
            var eOpr = 0;
            switch (ival) {
                case 0: // REJECT
                    txt = "Are you sure you want to reject selected score calibration number?";
                    eOpr = 3;
                    break;
                case 1: // APPROVE
                    txt = "Are you sure you want to approve selected score calibration number?";
                    eOpr = 2;
                    break;
            }
            var blnYes = false;
            swal({
                title: '',
                text: txt,
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {
                    blnYes = true;
                    if (blnYes == true) {
                        var dictRate = []; var dictRem = [];
                        //var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                        for (var i = 1, row; row = table.rows[i]; i++) {
                            for (var j = 1, col; col = row.cells[j]; j++) {
                                if ($(col).attr('Id') != undefined && $(col).attr('Id') == "trgid") {
                                    dictRate.push(
                                        $(row).closest("tr").find("td[Id='trgid']").text() + "|" + $(row).closest("tr").find("td[Id='cRate']").text()
                                    );
                                    dictRem.push(
                                        $(row).closest("tr").find("td[Id='trgid']").text() + "|" + $(row).closest("tr").find("td[Id='cRem']").text()
                                    );
                                }
                            }
                        }

                        var coyid = '<%= Session("CompanyUnkId") %>';
                        var ipadd = '<%= Session("IP_ADD") %>';
                        var hstname = '<%= Session("HOST_NAME") %>';
                        var sndradd = '<%= Session("Senderaddress") %>';
                        var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                        var usrid = '<%= Session("UserId") %>';
                        var yrid = '<%= Session("Fin_year") %>';
                        var empdate = '<%= Session("EmployeeAsOnDate") %>';
                        var cprip = document.getElementById('<%= hdfpriority.ClientID %>').value;
                        var db = '<%= Session("Database_Name") %>';

                        swal({ title: '', type: "info", text: "Sending Notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                        PageMethods.PerfApprRejOperation(eOpr, $(pid).val(), pid.options[pid.selectedIndex].text, db, usrid, yrid, coyid, empdate.toString(), empdate.toString(), dictRate, dictRem, amark, cprip, ipadd, hstname, "frmApproveRejectCalibrationAddEdit", sndradd, onSuccess, onFailure);
                        //PageMethods.PerfApprRejOperation(eOpr, $(pid).val(), pid.options[pid.selectedIndex].text, db, usrid, yrid, coyid, empdate.toString(), empdate.toString(), amark, cprip, ipadd, hstname, "frmApproveRejectCalibrationAddEdit", sndradd, onSuccess, onFailure);

                        function onSuccess(str) {
                            if (str == "1") {
                                if (ival == 0)
                                { swal({ title: '', type: "success", text: "Batch has been rejected successfully." }); }
                                else
                                { swal({ title: '', type: "success", text: "Batch has beed approved successfully." }); }
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                document.getElementById('<%=btnIApprove.ClientID %>').style.display = 'none';
                                document.getElementById('<%=btnIReject.ClientID %>').style.display = 'none';
                                document.getElementById("<%= hdfEditedcNum.ClientID %>").value = "";
                                document.getElementById("<%=txtApprRemark.ClientID%>").value = "";
                                //                                var calno = document.getElementById('<%= hdfEditedcNum.ClientID %>').value;
                                //                                document.getElementById("<%= hdfEditedcNum.ClientID %>").value = "";                                
                                return;
                            }
                        }

                        function onFailure(err) {
                            //alert(err.get_message());
                            swal({ title: '', type: "warning", text: err.get_message() });
                            return;
                        }
                    }
                }
            });
            return;
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria/Approver Info."></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                            Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnklstInfo" runat="server" ToolTip="View Rating Information"
                                            Text="Show Ratings" Font-Underline="false" Visible="false"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 33%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 6%" class="ib">
                                            <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                        </div>
                                        <div style="width: 23%" class="ib">
                                            <asp:TextBox ID="txtApprover" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                        </div>
                                        <div style="width: 15%" class="ib">
                                            <asp:TextBox ID="txtLevel" runat="server" ReadOnly="true"></asp:TextBox>
                                            <asp:HiddenField ID="hdfpriority" runat="server" />
                                        </div>
                                    </div>
                                    <hr style="margin: 9px; margin-left: -1px;" />
                                    <asp:HiddenField ID="hdfEditedcNum" runat="server" />
                                    <div class="row2">
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div style="width: 20%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 6%" class="ib">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div style="width: 20%" class="ib">
                                            <asp:DropDownList ID="cboStatus" runat="server" Width="234px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblRemark" runat="server" Text="Calib. Remark"></asp:Label>
                                        </div>
                                        <div style="width: 28%" class="ib">
                                            <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblPScoreFrom" runat="server" Text="Prov. Rating From"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbopRatingFrm" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 2%" class="ib">
                                            <asp:Label ID="lblPScTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbopRatingTo" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 11%" class="ib">
                                            <asp:Label ID="lblCScoreForm" runat="server" Text="Calb. Rating From"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbocRatingFrm" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 3%" class="ib">
                                            <asp:Label ID="lblCScTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbocRatingTo" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No"></asp:Label>
                                        </div>
                                        <div style="width: 28%" class="ib">
                                            <asp:TextBox ID="txtCalibrationNo" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Font-Bold="true"
                                                Checked="true" AutoPostBack="true" Visible="false" />
                                            <asp:Button ID="btnShowAll" runat="server" Text="My Report" CssClass="btndefault" />
                                            <asp:Button ID="btnDisplayCurve" runat="server" Text="Display HPO Curve" CssClass="btnDefault" />
                                        </div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="dvClistSearch" style="width: 100%">
                            <asp:TextBox ID="txtClistSearch" runat="server" AutoPostBack="false" Width="98%"
                                Style="margin-left: 5px" onkeyup="ListSeaching();"></asp:TextBox>
                        </div>
                        <div id="scrollable-container" style="width: 100%; height: 250px; overflow: auto">
                            <asp:GridView ID="gvCalibrateList" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,lusername,allocation,calibuser,ispgrp,pgrpid,total"
                                runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                ToolTip="Approve/Reject" OnClick="lnkedit_Click"><i class="fa fa-tasks" style="font-size:14px; color:Red;"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgEmployee" />
                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhlstpRating" />
                                    <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhsacrating" />
                                    <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhsacremark" />
                                    <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhlstcRating" />
                                    <asp:BoundField DataField="apcalibremark" HeaderText="Calib. Remark" FooterText="dgcolheRemark" />
                                    <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhsApprovalRemark" />
                                    <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgiStatus" />
                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgLevelName" />
                                    <asp:BoundField DataField="calibration_no" HeaderText="" FooterText="objdgcolhcbno" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="btnfixedbottom" class="btn-default">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        BehaviorID="”mpe”" TargetControlID="lblCalibrateHeading" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblFilter" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblCalibrateHeading" runat="server" Text="BSC Calibration"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblFilter" runat="server" Text="Search Criteria" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblSPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 89%" class="ib">
                                                <asp:DropDownList ID="cboSPeriod" runat="server" Width="762px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row2">
                                            <div style="width: 13%" class="ib">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calibration Rating"></asp:Label>
                                            </div>
                                            <div style="width: 23%" class="ib">
                                                <asp:DropDownList ID="cbocRating" runat="server" Width="200px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 14%;" class="ib">
                                                <asp:Label ID="lblIRemark" runat="server" Text="Calibration Remark"></asp:Label>
                                            </div>
                                            <div style="width: 30%; vertical-align: bottom;" class="ib">
                                                <asp:TextBox ID="txtIRemark" runat="server" Width="101%"></asp:TextBox>
                                            </div>
                                            <div style="width: 11%; margin-left: 5px;" class="ib">
                                                <asp:Button ID="btnIApply" runat="server" Text="Apply" CssClass="btndefault" OnClientClick="RatingOperation(); return false;" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblFilterData" runat="server" Text="Search"></asp:Label>
                                    </div>
                                    <div style="width: 90%;" class="ib">
                                        <asp:TextBox ID="txtListSearch" runat="server" Width="99%" onkeyup="FromSearching();"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="float: left; width: 19%; height: 200px;">
                                        <div>
                                            <div style="width: 100%; height: 170px;">
                                                <asp:Panel ID="pnlRatingF" runat="server" Height="200px" Width="100%" ScrollBars="Auto">
                                                    <asp:GridView ID="gvFilterRating" runat="server" AutoGenerateColumns="False" Width="95%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="scrf,scrt,id">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkFRating" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="name" HeaderText="Filter By Calib. Rating" FooterText="dgcolhFRating" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </div>
                                            <div>
                                                <asp:Button ID="btnFilter" OnClientClick="GetSelected(); return false;" runat="server"
                                                    Text="Filter" CssClass="btndefault" Style="margin-left: 4px; width: 97%" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Div4" style="width: 80%; height: 204px; overflow: auto;">
                                        <asp:GridView ID="gvApplyCalibration" DataKeyNames="employeeunkid,periodunkid,calibratnounkid,calibratescore,code,lusername,tranguid"
                                            runat="server" AutoGenerateColumns="False" Width="120%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="25">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhSEmployee" />
                                                <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhSJob" />
                                                <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhprovRating" />
                                                <asp:BoundField DataField="lrating" HeaderText="" FooterText="dgcolhlastlvlrating" />
                                                <asp:BoundField DataField="lcalibremark" HeaderText="" FooterText="dgcolhlastlvlremark" />
                                                <asp:BoundField DataField="acrating" HeaderText="Calib. Rating" FooterText="dgcolhcalibRating" />
                                                <asp:BoundField DataField="acremark" HeaderText="Calibration Remark" FooterText="dgcolhcalibrationremark"
                                                    ItemStyle-Wrap="true" />
                                                <asp:BoundField DataField="calibratescore" HeaderText="" FooterText="objdgcalibratescore"
                                                    ItemStyle-Wrap="true" />
                                                <asp:BoundField DataField="ichanged" HeaderText="" FooterText="objdgichanged" />
                                                <asp:BoundField DataField="tranguid" HeaderText="" FooterText="objdgtranguid" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="row2">
                                <div style="width: 6%; margin-left: 10px;" class="ib">
                                    <asp:Label ID="lblApprRemark" runat="server" Text="Remark"></asp:Label>
                                </div>
                                <div style="width: 90%;" class="ib">
                                    <asp:TextBox ID="txtApprRemark" runat="server" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnIApprove" runat="server" Text="Approve" CssClass="btndefault"
                                    OnClientClick="ApproveReject(1); return false;" />
                                <asp:Button ID="btnIReject" runat="server" Text="Reject" CssClass="btndefault" OnClientClick="ApproveReject(0); return false;" />
                                <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupMyReport" BackgroundCssClass="modalBackground" TargetControlID="lblMyReport"
                        runat="server" PopupControlID="pnlMyReport" CancelControlID="lblMyReport" />
                    <asp:Panel ID="pnlMyReport" runat="server" CssClass="newpopup" Style="display: none;
                        width: 1024px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblMyReport" runat="server" Text="My Report"></asp:Label>
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkRptAdvFilter" runat="server" Text="Advance Filter" Font-Underline="false"
                                        ForeColor="White" Visible="false"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto; height: 440px">
                                <br />
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptPeriod" runat="server" Text="Period"></asp:Label>
                                    </div>
                                    <div style="width: 35%;" class="ib">
                                        <asp:DropDownList ID="cboRptPeriod" runat="server" Width="350px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblRptCalibNo" runat="server" Text="Calibration No"></asp:Label>
                                    </div>
                                    <div style="width: 30%;" class="ib">
                                        <asp:DropDownList ID="cboRptCalibNo" runat="server" Width="286px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptShow" runat="server" Text="Show" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptEmployee" runat="server" Text="Employee"></asp:Label>
                                    </div>
                                    <div style="width: 78%;" class="ib">
                                        <asp:DropDownList ID="cboRptEmployee" runat="server" Width="776px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                    <hr />
                                </div>
                                <div class="row2">
                                    <div id="Div6" style="width: 100%; height: 325px; overflow: auto;">
                                        <asp:GridView ID="gvMyReport" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,allocation,calibuser,total"
                                            runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhMEmployee" />
                                                <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhMprovRating" />
                                                <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhMacrating" />
                                                <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhMacremark" />
                                                <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhMcalibRating" />
                                                <asp:BoundField DataField="apcalibremark" HeaderText="Calibration Remark" FooterText="dgcolhMcalibrationremark"
                                                    ItemStyle-Wrap="true" />
                                                <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhMApprovalRemark" />
                                                <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgcolhMiStatus" />
                                                <asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgcolhMusername" />
                                                <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgcolhMlevelname" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div style="float: left">
                                    <asp:Button ID="btnExportMyReport" runat="server" Text="Export" CssClass="btndefault" />
                                </div>
                                <asp:Button ID="btnCloseMyReport" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="lblHPOCurver"
                        runat="server" PopupControlID="pnlHPOChart" CancelControlID="lblHPOCurver" />
                    <asp:Panel ID="pnlHPOChart" runat="server" CssClass="newpopup" Style="display: none;
                        width: 950px; top: 30px;" Height="590px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblHPOCurver" runat="server" Text="HPO Chart"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 495px; overflow: auto">
                                <div id="Div8" class="panel-default">
                                    <div id="Div9" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblChGenerate" runat="server" Text="Generate Criteria" />
                                        </div>
                                        <div style="float: right;">
                                            <asp:LinkButton ID="lnkChAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                                Visible="false"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="Div10" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblchPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 28%" class="ib">
                                                <asp:DropDownList ID="cboChPeriod" runat="server" Width="240px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 10%" class="ib">
                                                <asp:Label ID="lblChDisplayType" runat="server" Text="Display Type"></asp:Label>
                                            </div>
                                            <div style="width: 22%" class="ib">
                                                <asp:DropDownList ID="cbochDisplay" runat="server" Width="192px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 25%;" class="ib">
                                                <asp:Button ID="btnChGenerate" runat="server" Text="Generate" CssClass="btndefault" />
                                                <asp:Button ID="btnChReset" runat="server" Text="Reset" CssClass="btndefault" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <asp:Panel ID="Panel4" runat="server" Height="165px" ScrollBars="Auto">
                                        <asp:DataGrid ID="dgvCalibData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            ShowHeader="false">
                                        </asp:DataGrid>
                                    </asp:Panel>
                                </div>
                                <div class="row2">
                                    <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                        BorderlineWidth="2" Width="917px" Style="margin-left: 7px" Height="300px" AntiAliasing="All">
                                        <Titles>
                                            <asp:Title Name="Title1">
                                            </asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                            </asp:Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnChClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportMyReport" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Title="Employee Feedback" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmployeeFeedback.aspx.vb" Inherits="wPgEmployeeFeedback" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Feedback"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="FilterCriteria" class="panel-default">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Feedback Add/Edit"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblCourseTitle" runat="server" Text="Course Title"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboCourseTitle" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblFeedbackItems" runat="server" Text="Feedback Item"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboFeedbackItem" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 33%">
                                                    <asp:Label ID="lblRemark" runat="server" Text="Text Result"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblFeedbackSubItems" runat="server" Text="Sub Item"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboSubItem" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 33%" rowspan="2" valign="top">
                                                    <asp:TextBox ID="txtTextResult" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblFeedbackGroup" runat="server" Text="Feedback Group"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboFeedbackGrp" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 13%">
                                                    <asp:Label ID="lblResult" runat="server" Text="Result"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboResult" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAddFeedback" runat="server" CssClass="btndefault" Text="Add" />
                                            <asp:Button ID="btnEditFeedback" runat="server" CssClass="btndefault" Text="Edit" />
                                        </div>
                                    </div>
                                    <div id="Div2" class="panel-body-default">
                                        <asp:Panel ID="pnlView" runat="server" Width="100%" Height="200px" ScrollBars="Auto">
                                            <asp:GridView ID="dgvFeedback" runat="server" AutoGenerateColumns="False" Width="99%"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CssClass="gridedit" CommandName="Change"
                                                                    ToolTip="Edit"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:ButtonField ButtonType="Image" CommandName="" HeaderText="Edit" ItemStyle-CssClass=""
                                                        ShowHeader="True" Text="Edit">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:ButtonField>--%>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CssClass="griddelete" CommandName="Remove"
                                                                    ToolTip="Delete"></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:ButtonField ButtonType="Image" CommandName="" HeaderText="" ImageUrl="~/images/remove.png"
                                                        ShowHeader="True" Text="Delete">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:ButtonField>--%>
                                                    <asp:BoundField DataField="feedback_group" HeaderText="Feedback Group" ReadOnly="true"
                                                        FooterText="lblFeedbackGroup" />
                                                    <asp:BoundField DataField="feedback_item" HeaderText="Feedback Item" ReadOnly="true"
                                                        FooterText="colhFeedbackItem" />
                                                    <asp:BoundField DataField="feedback_subitem" HeaderText="Feedback SubItem" ReadOnly="true"
                                                        FooterText="colhFeedbackSubItems" />
                                                    <asp:BoundField DataField="feedback_result" HeaderText="Result" ReadOnly="true" FooterText="colhResult" />
                                                    <asp:BoundField DataField="other_result" HeaderText="Text Result" ReadOnly="true"
                                                        FooterText="colhTextResult" />
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSaveComplete" runat="server" CssClass="btndefault" Text="Save & Complete" />
                                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- <asp:Panel ID="Panel3" Width="500px" Height="80px" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="pnl3" runat="server" Width="500px" Height="80px" Style="background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                            <table style="width: 500px">
                                <tr>
                                    <td>
                                        <asp:Label ID="LblDelete" runat="server" Text="" Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnYes" runat="server" Text="Yes" Width="77px" CssClass="btndefault" />
                                        <asp:Button ID="btnNo" runat="server" Text="No" Width="77px" CssClass="btndefault" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btnNo" DropShadow="true" PopupControlID="Panel3" TargetControlID="LblDelete" />--%>
                    <ucCfnYesno:Confirmation ID="popupConfirm" runat="server" Message="" Title="Confirmation" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

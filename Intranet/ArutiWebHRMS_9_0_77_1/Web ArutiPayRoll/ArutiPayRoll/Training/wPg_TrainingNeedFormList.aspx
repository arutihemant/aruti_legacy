﻿<%@ Page Title="Training Need Form List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_TrainingNeedFormList.aspx.vb" Inherits="Training_wPg_TrainingNeedFormList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    
    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }
    </script>
    
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Need Form List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetailHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">                                                                             
                                        <div class="ib" style="width: 5%">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 8%">
                                            <asp:Label ID="lblDatefrom" runat="server" Text="Date From"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <uc3:DateCtrl ID="dtpDatefrom" runat="server" />
                                        </div>
                                        <div class="ib" style="width: 5%">
                                            <asp:Label ID="lblDateTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 13%">
                                            <uc3:DateCtrl ID="dtpDateTo" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 5%">
                                            <asp:Label ID="lblFormNo" runat="server" Text="Form No."></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <asp:TextBox ID="txtFormNo" runat="server"></asp:TextBox>
                                        </div>                                           
                                        <div class="ib" style="width: 8%">
                                            <asp:Label ID="lblDevRequire" runat="server" Text="Dev. Required"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <asp:DropDownList ID="cboDevRequire" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 5%">
                                            <asp:Label ID="lblPriority" runat="server" Text="Priority"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 13%">
                                            <asp:DropDownList ID="cboPriority" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </div>                                        
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <div id="Grid-view" style="height: 235px; overflow: auto">
                                    <asp:GridView ID="dgvTrainingNeed" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="trainingneedformunkid, IsGrp">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                FooterText="colhEdit">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>"
                                                            CssClass="gridedit" CommandName="Change" ToolTip="Edit">
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                FooterText="colhDelete">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CausesValidation="false" CssClass="griddelete"
                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" ToolTip="Delete">
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                                                                                                    
                                            <%--<asp:BoundField DataField="formno" HeaderText="Form No" FooterText="colhFormNo" />--%>
                                            <asp:BoundField DataField="period_name" HeaderText="Period" FooterText="colhPeriodName" />
                                            <asp:BoundField DataField="start_date" HeaderText="Start Date" FooterText="colhStartdate">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="end_date" HeaderText="End Date" FooterText="colhEnddate">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="trainingcoursename" HeaderText="Dev. Required" FooterText="colhTrainingCourseName" />
                                            <asp:BoundField DataField="priorityname" HeaderText="Priority" FooterText="colhPriority" />                                            
                                            <%--<asp:BoundField DataField="form_status" HeaderText="Status" FooterText="colhStatus" />--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc4:Confirmation ID="popupYesNo" runat="server" Title="Confirmation" Message="Are you sure you want to delete this Training Need Form?" />
                    <uc5:DeleteReason ID="popupDelReason" runat="server" Title="Void Reason" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>    
</asp:Content>


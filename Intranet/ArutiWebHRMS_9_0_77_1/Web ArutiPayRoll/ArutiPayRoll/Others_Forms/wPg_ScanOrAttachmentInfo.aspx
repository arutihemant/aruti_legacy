﻿<%@ Page Title="Scan / Attachment Info" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_ScanOrAttachmentInfo.aspx.vb" Inherits="Others_Forms_wPg_ScanOrAttachmentInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
            $('#<%= lblError.ClientID %>').css("position","static");
        }
        
    </script>
    
     <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>_drpCombo');
            var cboEmp = $('#<%= cboEmployee.ClientID %>_drpCombo');
            var cboDocument = $('#<%= cboDocument.ClientID %>_drpCombo');
           
            var lblError = $('#<%= lblError.ClientID %>');
            
             if (parseInt(cboEmp.val()) <= 0) {
                lblError.html("Employee is compulsory information. Please select Employee to continue.");
                cboEmp.focus();
                return false;
            }           
            if (parseInt(cbodoctype.val()) <= 0) {
                lblError.html("Document Type is mandatory information. Please select Document Type to continue.");
                cbodoctype.focus();
                return false;
            }
           if (parseInt(cboDocument.val()) <= 0) {
                lblError.html("Document is mandatory information. Please select Document to continue.");
                cboDocument.focus();
                return false;
            }
            
            if (document.referrer.indexOf("wPg_LeaveFormAddEdit.aspx") <= 0){
            var cboAssocateVal = $('#<%= cboAssocatedValue.ClientID %>_drpCombo');
            if (typeof cboAssocateVal.val() != "undefined") {
                if (parseInt(cboAssocateVal.val()) <= 0 && parseInt(cbodoctype.val()) != 3) {
                    lblError.html("is mandatory information. Please select Assocate Level.");
                    cboAssocateVal.focus();
                    return false;
                }
            }
            }
            return true;
        }    
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Scan / Attachment Info"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 33%">
                                                <asp:Label ID="objlblCaption" runat="server" Text="#Value"></asp:Label>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:Label ID="lblDocType" runat="server" Text="Document Type"></asp:Label>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:Label ID="lblUploadDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True" />
                                            </td>
                                            <td style="width: 33%">
                                                <asp:DropDownList ID="cboDocumentType" runat="server" AutoPostBack="True"></asp:DropDownList>
                                            </td>
                                            <td style="width: 33%">
                                                <uc2:DateCtrl ID="dtpUploadDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%">
                                                <asp:Label ID="lblDocument" runat="server" Text="Document"></asp:Label>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:Label ID="lblSelection" runat="server" Text="#Value"></asp:Label>
                                            </td>
                                            <td style="width: 33%">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%">
                                                <asp:DropDownList ID="cboDocument" runat="server"></asp:DropDownList>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:DropDownList ID="cboAssocatedValue" runat="server"></asp:DropDownList>
                                            </td>
                                            <td style="width: 33%">
                                                <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <asp:FileUpload ID="docUpload" runat="server" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAdd" />
                                                    </Triggers>
                                                </asp:UpdatePanel>--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 78%">
                                        <asp:Button ID="btnScan" runat="server" Text="Scan" CssClass="btnDefault" ValidationGroup="ScanAttach"
                                            Visible="false" />
                                        <asp:Label ID="lblError" runat="server" Text="" CssClass="ErrorControl" Style="text-align: right;
                                            position: relative; float: left;"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server" Width="47%" Style="float: left">
                                                        <div id="fileuploader">
                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Add"
                                                                onclick="return IsValidAttach()" validationgroup="ScanAttach" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnAdd" runat="server" Text="" ValidationGroup="ScanAttach" Style="display: none"
                                                        OnClick="btnAdd_Click" />
                                                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                    <%--<asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="btnDefault" ValidationGroup="ScanAttach" />--%>
                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault"
                                                        ValidationGroup="ScanAttach" />
                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="pnl_gvScanAttachment" runat="server" Height="350px" Width="100%" ScrollBars="Auto">
                                        <asp:GridView ID="gvScanAttachment" runat="server" AutoGenerateColumns="False" Width="99%"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="GUID,scanattachtranunkid,orgfilepath">
                                             <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                             <%--DataKeyNames="scanattachtranunkid"--%>
                                             <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                        </span>
                                                        <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName="Remove"
                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--0--%>
                                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                <asp:TemplateField HeaderText="Download">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="btnDownload" runat="server" CausesValidation="false" CommandArgument="<%# Container.DataItemIndex %>"
                                                                CommandName="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--1--%>
                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" FooterText="colhCode">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--2--%>
                                                <asp:BoundField DataField="names" HeaderText="Employee/Applicant" ReadOnly="True"
                                                    FooterText="colhName">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--3--%>
                                                <asp:BoundField DataField="document" HeaderText="Document" ReadOnly="True" FooterText="colhDocument">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--4--%>
                                                <asp:BoundField DataField="filename" HeaderText="File Name" ReadOnly="True" FooterText="colhFileName">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--5--%>
                                                <asp:BoundField DataField="valuename" HeaderText="Associated Value" ReadOnly="True"
                                                    FooterText="colhValue">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--6--%>
                                                <asp:BoundField DataField="employeeunkid" HeaderText="empid" ReadOnly="True" Visible="false"
                                                    FooterText="colhemployeeunkid">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--7--%>
                                                <asp:BoundField DataField="applicantid" HeaderText="applicantid" ReadOnly="True"
                                                    Visible="false" FooterText="colhapplicantid">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <%--8--%>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" ValidationGroup="Save" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" ValidationGroup="Save" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvScanAttachment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
				$('#<%= lblError.ClientID %>').css("position","static");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_ScanOrAttachmentInfo.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAdd.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			$('input[type=file]').live("click",function(){
			    return IsValidAttach();
			});
    </script>

</asp:Content>

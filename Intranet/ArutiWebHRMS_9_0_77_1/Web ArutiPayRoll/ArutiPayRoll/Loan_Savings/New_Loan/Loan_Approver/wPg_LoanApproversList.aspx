﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanApproversList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Approver_wPg_LoanApproversList" Title="Employee Approvers List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>
    
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Approvers List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboLoanLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblLoanApprover" Style="margin-left: 10px" runat="server" Text="Loan Approver"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboLoanApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblStatus" Style="margin-left: 10px" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvApproversList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="brnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                            CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="100px" Visible="true" ItemStyle-Width="100px"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="lnkSetInActive" Font-Underline="false" style="font-weight:bold; font-size:12px" CommandName="InActive"
                                                                            Text="Set InActive" runat="server"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkSetActive" CommandName="Active" Font-Underline="false" style="font-weight:bold; font-size:12px" Text="Set Active"
                                                                            runat="server"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="lnlevelname" HeaderText="Level" Visible="true" ReadOnly="true"
                                                                FooterText="colhLevel" />
                                                            <asp:BoundColumn DataField="lnapproverunkid" HeaderText="Approverunkid" Visible="false"
                                                                FooterText="colhlnapproverunkid"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Loan Approver" ReadOnly="true" FooterText="colhLoanApprover" />
                                                            <asp:BoundColumn DataField="departmentname" HeaderText="Department" ReadOnly="true"
                                                                FooterText="colhDepartment" />
                                                            <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                            <asp:BoundColumn DataField="mappeduser" HeaderText="Mapped User" ReadOnly="true"
                                                                FooterText="colhMappedUser" />
                                                            <asp:BoundColumn DataField="isactive" HeaderText="IsActive" Visible="false" ReadOnly="true"
                                                                FooterText="colhIsActive" />
                                                            <asp:BoundColumn DataField="extapprover" HeaderText="External Approver" ReadOnly="true"
                                                                FooterText="colhIsExternalApprover" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </tr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

</asp:Content>

﻿
Partial Class Reports_Report
    Inherits System.Web.UI.Page

    Dim DisplayMessage As New CommonCodes

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            If Session("objRpt") Is Nothing Then
                Response.Redirect("../UserHome.aspx", False)
                Exit Sub
            End If

            crViewer.ReportSource = Session("objRpt")
            crViewer.PageZoomFactor = 125
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Init :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                If Request.UrlReferrer IsNot Nothing Then
                    ViewState("referrer") = Request.UrlReferrer.ToString
                Else
                    ViewState("referrer") = "../Index.aspx"
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Page.Theme = Session("Theme")
    End Sub
End Class

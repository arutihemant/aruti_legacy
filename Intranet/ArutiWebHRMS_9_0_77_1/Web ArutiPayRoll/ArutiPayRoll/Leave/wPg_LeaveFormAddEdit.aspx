﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LeaveFormAddEdit.aspx.vb"
    Inherits="Leave_wPg_LeaveFormAddEdit" Title="Leave Form Add/Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                      
                }
            }
    </script>

    <script>
        function IsValidAttach() {
            var cboEmployee = $('#<%= cboEmployee.ClientID %>');

            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }

            if (parseInt(cboEmployee.val()) <= 0) {
                alert('Employee Name is compulsory information. Please select Employeee to continue.');
                cboEmployee.focus();
                return false;
            }
        }    
        
    </script>

    
        <%--'Pinkal (20-Nov-2018) -- Start
        'Enhancement - Working on P2P Integration for NMB.--%>
        <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>
    
<%--       'Pinkal (20-Nov-2018) -- End--%>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_YesNo.ClientID %>_Panel1").css("z-index", "100005");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Leave Form"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Add / Edit Leave Form"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                             <%-- 'Pinkal (25-May-2019) -- Start
                                                      'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                            <asp:Label ID="lblEmployee" runat="server" CssClass="mandatory_field" Text="Employee:"></asp:Label>
                                             <%-- 'Pinkal (25-May-2019) -- End--%>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 13%">
                                         <%-- 'Pinkal (25-May-2019) -- Start
                                                  'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                            <asp:Label ID="lblLeaveType" runat="server" CssClass="mandatory_field" Text="Leave Type:"></asp:Label>
                                             <%-- 'Pinkal (25-May-2019) -- End--%>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 13%">
                                         <%-- 'Pinkal (25-May-2019) -- Start
                                                  'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                            <asp:Label ID="lblStartDate" runat="server"  CssClass="mandatory_field" Text="Start Date:"></asp:Label>
                                             <%-- 'Pinkal (25-May-2019) -- End--%>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblFormNo" runat="server" Text="Application No:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:TextBox ID="txtFormNo" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtpApplyDate" runat="server" AutoPostBack="True" />
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblReturnDate" runat="server" Text="End Date:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    
                                    <%--'Pinkal (01-Oct-2018) -- Start Enhancement - Leave Enhancement for NMB.--%>
                                    <tr style="width: 100%">
                                      <td style="width: 13%">
                                            <%-- 'Pinkal (25-May-2019) -- Start
                                                      'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                            <asp:Label ID="LblReliever" runat="server" CssClass="mandatory_field" Text="Reliever"></asp:Label>
                                            <%-- 'Pinkal (25-May-2019) -- End--%>
                                        </td>
                                        <td style="width: 20%">
                                              <asp:DropDownList ID="cboReliever" runat="server"/>
                                        </td>
                                        <td colspan ="4">
                                        </td>
                                    </tr>
                                     <%--Pinkal (01-Oct-2018) -- End--%>
                                     
                                    <tr style="width: 100%">
                                        <td style="width: 13%; padding-top: 5px;">
                                            <asp:Label ID="lblDays" runat="server" Text="Days to Apply:"></asp:Label>
                                        </td>
                                        <td style="width: 87%; padding-top: 5px;" colspan="5">
                                            <asp:Label ID="objNoofDays" runat="server" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblLeaveAccrue" runat="server" Text="Total Leave Accrue:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:Label ID="objLeaveAccrue" runat="server" Text="0.00"></asp:Label>
                                        </td>
                                        <td style="width: 66%" colspan="4">
                                            <asp:Label ID="LblAsonDateAccrue" runat="server" Text="As On Date Leave Accrue:"
                                                Style="padding-right: 10px;"></asp:Label>
                                            <asp:Label ID="objAsonDateAccrue" runat="server" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblBalance" runat="server" Text="Total Leave Balance:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:Label ID="objBalance" runat="server" Text="0.00"></asp:Label>
                                        </td>
                                        <td style="width: 66%" colspan="4">
                                            <asp:Label ID="lblAsonDateBalance" runat="server" Text="As On Date Leave Balance:"
                                                Style="padding-right: 10px;"></asp:Label>
                                            <asp:Label ID="objAsonDateBalance" runat="server" Text="0.00"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%" colspan="6">
                                            <asp:Label ID="LblELCStart" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: right; padding-right: 40px;" colspan="6">
                                            <asp:LinkButton ID="lnkScanDocuements" runat="server" Visible="false" Font-Underline="false"
                                                Text="Browse" Style="padding-right: 20px;" />
                                            <asp:LinkButton ID="lnkLeaveDayCount" runat="server" Font-Underline="false" Text="Leave Day Fraction"
                                                Style="padding-right: 20px;" />
                                            <asp:LinkButton ID="lvLeaveExpense" runat="server" Font-Underline="false" Text="Leave Expense" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 46%;" colspan="3">
                                            <asp:Label ID="lblLeaveAddress" runat="server" Text="Address and Telephone Number While On leave"></asp:Label>
                                        </td>
                                        <td style="width: 46%;" colspan="3">
                                            <asp:Label ID="lblRemarks" runat="server" Text="Reason"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 46%;" colspan="3">
                                            <asp:TextBox ID="txtLeaveAddress" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td style="width: 46%;" colspan="3">
                                            <asp:TextBox ID="txtLeaveReason" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="btnSave" runat="server" CssClass=" btnDefault" Text="Save" />
                                    <asp:Button ID="btnClose" runat="server" CssClass=" btnDefault" Text="Close" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <table>
                        <tr id="LeaveExpense">
                            <td>
                                <cc1:ModalPopupExtender ID="popupExpense" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="hdf_Claim" PopupControlID="pnlExpensePopup" DropShadow="true"
                                    CancelControlID="hdf_Claim">
                                </cc1:ModalPopupExtender>
<%--'Pinkal (20-Nov-2018) -- Start
                                                    'Enhancement - Working on P2P Integration for NMB.--%>
                                <asp:Panel ID="pnlExpensePopup" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 990px">
<%--'Pinkal (20-Nov-2018) -- End--%>
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div4" class="panel-default">
                                                <div id="Div5" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="Label2" runat="server" Text="Claim & Request Add/Edit"></asp:Label>
                                                    </div>
                                                    <%--'Pinkal (20-Nov-2018) -- Start
                                                    'Enhancement - Working on P2P Integration for NMB.--%>
                                                    <div style="text-align: right">
                                                        <asp:LinkButton ID="lnkViewDependants" runat="server" Text="View Depedents List"
                                                            CssClass="lnkhover" Font-Bold="true"></asp:LinkButton>
                                                    </div>
                                                    <%--'Pinkal (20-Nov-2018) -- End--%>
                                                </div>
                                                <div id="Div6" class="panel-body-default">
                                                    <table style="width: 100%;">
                                                        <tr style="width: 100%">
                                                            <td style="border-right: solid 1px #DDD; width: 40%;">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                    <%--'Pinkal (20-Nov-2018) -- Start
                                                    'Enhancement - Working on P2P Integration for NMB.--%>
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                                        </td>
                                                                        <td style="width: 80%" colspan="3">
                                                                            <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" Width="270px">
                                                                            </asp:DropDownList>
                                                                            <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period"></asp:Label>
                                                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 20%">
                                                                            <asp:Label ID="lblClaimNo" runat="server" Text="Claim No"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%;">
                                                                            <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="lblExpEmployee" runat="server" Text="Employee"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" style="width: 75%">
                                                                            <asp:DropDownList ID="cboExpEmployee" runat="server" AutoPostBack="true" Width="270px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="lblExpLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" style="width: 75%">
                                                                            <asp:DropDownList ID="cboExpLeaveType" runat="server" AutoPostBack="true" Enabled="False"
                                                                                Width="270px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" style="width: 75%">
                                                                            <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False"
                                                                                Width="270px">

<%--'Pinkal (20-Nov-2018) -- End--%>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                      <%--'Pinkal (20-Nov-2018) -- Start
                                                                     'Enhancement - Working on P2P Integration for NMB.--%>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%">
                                                                            <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                                        </td>
                                                                        <td colspan="3" style="width: 75%">
                                                                            <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                               ReadOnly="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                  
                                                                    <%--<tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3" style="font-weight: bold">
                                                                            <asp:LinkButton ID="lnkViewDependants" runat="server" ForeColor="#006699" Text="View Depedents List"
                                                                                CssClass="lnkhover"></asp:LinkButton>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <%--'Pinkal (20-Nov-2018) -- End--%>
                                                                </table>
                                                            </td>
                                                            <td style="width: 60">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 15%">
                                                                            <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                                        </td>
                                                                        
                                                                           <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                        <td colspan="3" style="width: 90%">
                                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" Width="270px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                           <td style="width: 10%">
                                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                          <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                                    </tr>
                                                                    <%--'Pinkal (20-Nov-2018) -- Start
                                                                    'Enhancement - Working on P2P Integration for NMB.--%>
                                                                   <%--   'Pinkal (30-Apr-2018) - Start
                                                                      'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                                    <%--<tr style="width: 100%">
                                                                        <td style="width: 13%">
                                                                            <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 23%">
                                                                            <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                                Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 19%">
                                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 45%">
                                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 26%">
                                                                            <asp:TextBox ID="txtQty" runat="server" CssClass="txttextalignright" Text="0" AutoPostBack="true"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                                Enabled="false" Visible="false"></asp:TextBox>
                                                                            <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 13%">
                                                                            <asp:Label ID="lblExpBalance" runat="server" Text="Balance"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 23%">
                                                                            <asp:TextBox ID="txtBalance" runat="server" CssClass="txttextalignright" Enabled="false"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 19%">
                                                                            <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 45%">
                                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="107px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 18%">
                                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 26%">
                                                                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 50%" colspan="3">
                                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="270px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        
                                                                     <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                                 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                     <%--   <td style="width: 10%">
                                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                                        </td>--%>
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                                            <asp:TextBox ID="txtQty" runat="server" CssClass="txttextalignright" Text="0" AutoPostBack="true"
                                                                                Style="text-align: right"  onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                        </td>
                                                                         <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 10%">
                                                                            <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 50%" colspan="3">
                                                                            <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" Width="270px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        
                                                                    <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                                 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                      <%--  <td style="width: 10%">
                                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                                            <asp:TextBox ID="txtQty" runat="server" CssClass="txttextalignright" Text="0" AutoPostBack="true"
                                                                                Style="text-align: right"  onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                        </td>--%>
                                                                          <td style="width: 10%">
                                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                          <%--'Pinkal (07-Feb-2020) -- Start
                                                                                  'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB 
                                                                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this,event)"></asp:TextBox>--%>
                                                                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="1.00" Style="text-align: right" onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                            <%--  'Pinkal (07-Feb-2019) -- End --%>
                                                                            <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00" Enabled="false" Visible="false"></asp:TextBox>
                                                                            <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                                        </td>
                                                                        <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 14%">
                                                                            <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                                Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 11%">
                                                                            <asp:Label ID="lblExpBalance" runat="server" Text="Balance"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtBalance" runat="server" CssClass="txttextalignright" Enabled="false"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                           
                                                                     <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                                 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                        
                                                                      <%--  <td style="width: 13%">
                                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 20%">
                                                                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"
                                                                                Style="text-align: right" onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                            <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                                Enabled="false" Visible="false"></asp:TextBox>
                                                                            <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                                        </td>--%>
                                                                   <td style="width: 10%">
                                                                          <asp:Label ID="LblCurrency" runat="server" Text="Currency"></asp:Label>
                                                                        </td>
                                                                     <td style="width: 20%">
                                                                          <asp:DropDownList ID="cboCurrency" runat="server" Width= "115px"></asp:DropDownList>
                                                                    </td>
                                                                    <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                                    </tr>
                                                               <%--  'Pinkal (30-Apr-2018) - End--%>
                                                                    <%--'Pinkal (20-Nov-2018) -- End--%>
                                                                    <tr style="width: 100%">
                                                                        <td colspan="5">
                                                                            <cc1:TabContainer ID="tabmain" runat="server" ActiveTabIndex="0">
                                                                                <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                                    <HeaderTemplate>
                                                                                        Expense Remark
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                                        <%--<td colspan="3" style="width: 20%" valign="top">
                                                                            <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                                            <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                                Style="height: 56px;" ReadOnly="true"></asp:TextBox>
                                                                        </td>--%>
                                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                                        <td style="width: 15%;vertical-align: bottom;" align="right">
                                                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btnDefault" />
                                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btnDefault" Visible="false" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="Div7" class="panel-default">
                                                <div id="Div8" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                              <%--'Pinkal (20-Nov-2018) -- Start
                                                               'Enhancement - Working on P2P Integration for NMB.--%>
                                                                <asp:Panel ID="pnl_dgvdata" runat="server" Height="170px" ScrollBars="Auto">
                                                                <%--'Pinkal (20-Nov-2018) -- End--%>
                                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                                FooterText="brnEdit">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="imgEdit" runat="server" CssClass="gridedit" ToolTip="Edit" CommandName="Edit"></asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                                FooterText="btnDelete">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                                            CommandName="Delete"></asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                                        <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                                FooterText="dgcolhExpense" />
                                                                            <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                                            <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                                ItemStyle-CssClass="txttextalignright" />
                                                                            <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                                ItemStyle-CssClass="txttextalignright" />
                                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                                ItemStyle-CssClass="txttextalignright" />
                                                                            <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                                                Visible="true" FooterText="dgcolhExpenseRemark" />
                                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                                Visible="false" />
                                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                                Visible="false" />
                                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                                        </Columns>
                                                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                                    </asp:DataGrid>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="text-align: right">
                                                                <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                                <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="txttextalignright"
                                                                    Width="20%"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_Claim" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Button ID="btnScanAttchment" runat="server" Visible="false" Text="Scan/Attchment"
                                                                    CssClass="btndefault" />
                                                                <asp:Button ID="btnSaveAddEdit" runat="server" Text="Save" CssClass="btnDefault"
                                                                    ValidationGroup="Step" />
                                                                <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btnDefault"
                                                                    CausesValidation="false" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                <%--  'Pinkal (22-Oct-2018) -- Start
                                 'Enhancement - Implementing Claim & Request changes For NMB .--%>
                                <uc7:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" />
                                 <%--  'Pinkal (22-Oct-2018) -- End --%>
                                 
                                <%--   'Pinkal (04-Feb-2019) -- Start
                                             'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                <uc7:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Title="Confirmation" />
                                 <%--  'Pinkal (04-Feb-2019) -- End --%></asp:Panel>
                                
                                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="btnEmppnlEmpDepedentsClose" PopupControlID="pnlEmpDepedents"
                                    DropShadow="true" CancelControlID="btnEmppnlEmpDepedentsClose">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 450px">
                                    <div class="panel-primary" style="margin-bottom: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List" />
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div9" class="panel-default">
                                                <div id="Div10" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="Label3" runat="server" Text="Dependents List" />
                                                    </div>
                                                </div>
                                                <div id="Div11" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" Width="99%"
                                                                    DataKeyField="" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                                                        <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                                                        <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                                                        <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                                                        <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" Width="70px"
                                                            CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdnEmpDepedents" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <cc1:ModalPopupExtender ID="popupClaim" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="hdf_ClaimDelete" PopupControlID="pnl_popupClaimDelete" DropShadow="true"
                                    CancelControlID="btnDelReasonNo">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnl_popupClaimDelete" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 450px;">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblClaimDelete" runat="server" Text="Aruti"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div21" class="panel-default">
                                                <div id="Div22" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblClaimTitle" runat="server" Text="Are you sure you want to delete selected transaction?"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:TextBox ID="txtClaimDelReason" runat="server" TextMode="MultiLine" Width="97%"
                                                                    Height="50px" Style="margin-bottom: 5px;" />
                                                                <asp:RequiredFieldValidator ID="rqfv_txtClaimDelReason" runat="server" Display="None"
                                                                    ControlToValidate="txtClaimDelReason" ErrorMessage="Delete Reason can not be blank. "
                                                                    CssClass="ErrorControl" ForeColor="White" Style="" SetFocusOnError="True" ValidationGroup="ClaimDelete"></asp:RequiredFieldValidator>
                                                                <cc1:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="rqfv_txtClaimDelReason"
                                                                    Width="300px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" Width="70px" Style="margin-right: 10px"
                                                            CssClass="btnDefault" ValidationGroup="ClaimDelete" />
                                                        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" Width="70px" Style="margin-right: 10px"
                                                            CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_ClaimDelete" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                    CancelControlID="hdf_ScanAttchment">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                    Style="display: none;">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div2" class="panel-default">
                                                <div id="Div1" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 30%">
                                                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 40%">
                                                                <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                    Width="200px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                    <div id="fileuploader">
                                                                        <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Add" />
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click" Text="Browse" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td colspan="3" style="width: 100%">
                                                                <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                        ToolTip="Delete"></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--0--%>
                                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                        <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--1--%>
                                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                        <%--2--%>
                                                                        <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                                        <%--3--%>
                                                                        <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                                        <%--4--%>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                        <div style="float: left">
                                                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                        </div>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" />
                                                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--  'Pinkal (18-Mar-2021) -- Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.--%>
                                    <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                                    <%--  'Pinkal (18-Mar-2021) -- End--%></asp:Panel>
                                
                                <%--  'Pinkal (18-Mar-2021) -- Start
                                      'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                                     'Pinkal (18-Mar-2021) -- End--%>
                              
                                <cc1:ModalPopupExtender ID="popupFraction" BackgroundCssClass="modalBackground" TargetControlID="LblFraction"
                                    runat="server" PopupControlID="pnlFractionPopup" DropShadow="true" CancelControlID="btnFractionCancel">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlFractionPopup" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 450px">
                                    <div class="panel-primary">
                                        <div class="panel-heading">
                                            <asp:Label ID="LblFraction" Text="Leave Day Count" runat="server"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div12" class="panel-default">
                                                <div id="Div13" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="Label4" Text="Leave Day Count" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="Div14" class="panel-body-default">
                                                    <asp:Panel ID="pnl_" runat="server" ScrollBars="Auto" Height="350px">
                                                        <asp:GridView ID="dgFraction" runat="server" AutoGenerateColumns="False" Width="99%"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:CommandField ShowEditButton="true" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Underline="false" />
                                                                <asp:BoundField DataField="leavedate" HeaderText="Date" ReadOnly="True" FooterText="dgColhDate" />
                                                                <asp:BoundField DataField="dayfraction" HeaderText="Fractions" ItemStyle-HorizontalAlign="Right"
                                                                    HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhFraction" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnFractionOK" runat="server" CssClass="btndefault" Text="Ok" />
                                                        <asp:Button ID="btnFractionCancel" runat="server" CssClass="btndefault" Text="Cancel" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <uc7:Confirmation ID="ExpenseConfirmation" Title="Confirmation" runat="server" />
                                <uc7:Confirmation ID="LeaveFormConfirmation" runat="server" Title="Confirmation" />
                                <uc7:Confirmation ID="FormDatesConfirmation" Title="Confirmation" runat="server" />
                                <%--'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.--%>
                                <uc7:Confirmation ID="P2PLvFormDeleteConfirmation" Title="Confirmation" runat="server" IsFireButtonNoClick="true"/>
                                <%--'Pinkal (20-Nov-2018) -- End--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_LeaveFormAddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>

<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="LeaveIssue.aspx.vb" Inherits="LeaveIssue" Title="Leave Issue" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="YesNobutton" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/Controls/ConfirmYesNo.ascx" tagname="Confirmation" tagprefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Issue"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbLeaveIssue" runat="server" Text="Leave Issue Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%; margin-bottom: 20px">
                                        <tr style="width: 100%;">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFromYear" runat="server" Text="From Year" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 45%" colspan="2">
                                                <asp:DropDownList ID="cboYear" runat="server" Visible="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 40%" rowspan="5" valign="top">
                                                <table width="100%" border="1" style="border-color: #DDD">
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%; background: Steelblue;">
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description" Font-Bold="true"
                                                                Font-Size="Small" ForeColor="White"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%; background: Steelblue;" align="right">
                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount" Font-Bold="true" Font-Size="Small"
                                                                ForeColor="White"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%; display:none">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="lblLastYearAccrued" runat="server" Text="Total Accrued Upto Last Year"
                                                                Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="lblCaption1Value" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="objlblLeaveBFvalue" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="lblCaption2Value" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%; display:none">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="lblLastYearIssued" runat="server" Text="Total Issued Upto Last Year"
                                                                Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="lblCaption3Value" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued Upto Date"
                                                                Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="lblCaption4Value" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="objlblTotalAdjustment" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 70%;">
                                                            <asp:Label ID="lblBalance" runat="server" Text="Balance" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%;" align="right">
                                                            <asp:Label ID="lblCaption5Value" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 45%" colspan="2">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFormNo" runat="server" Text="Form No"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtFormNo" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFDateVal" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtLeaveType" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:TextBox ID="txtColor" runat="server" Enabled="false" BorderStyle="None"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblLeaveEndDate" runat="server" Text="As On Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtAsOnDate" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblTotalLeaveDays" runat="server" Text="Total Days : "></asp:Label><asp:Label
                                                    ID="lblDays" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:UpdatePanel ID="Upanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel1" Width="100%" ScrollBars="Auto" runat="server" Style="border: 2pz solid #DDD">
                                                <asp:DataGrid ID="dgViewLeave" runat="server" Width="200%" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <ItemStyle Wrap="True" Width="50px" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="CalendarTableID" HeaderText="CalendarTableID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MonthYear" HeaderText="Month" FooterText=""></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="startday" HeaderText="startday"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MonthDays" HeaderText="MonthDays"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MonthId" HeaderText="MonthId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="YearId" HeaderText="YearId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_1" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_1" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_1" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_1" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_1" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_1" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_1" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_2" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_2" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_2" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_2" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_2" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_2" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_2" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_3" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_3" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_3" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_3" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_3" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_3" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_3" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_4" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_4" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_4" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_4" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_4" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_4" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_4" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_5" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_5" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_5" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_5" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_5" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_5" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_5" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sun_6" HeaderText="Sun"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Mon_6" HeaderText="Mon"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Tue_6" HeaderText="Tue"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Wed_6" HeaderText="Wed"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Thu_6" HeaderText="Thu"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Fri_6" HeaderText="Fri"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Sat_6" HeaderText="Sat"></asp:BoundColumn>
                                                        <asp:ButtonColumn CommandName="ColumnClick" Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="dgViewLeave" EventName="ItemCommand" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass=" btnDefault" Width="77px" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass=" btnDefault" Width="77px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:YesNobutton ID="radYesNo" runat="server" Title="Leave Issue" Message="Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?" />
                      <%-- 'Pinkal (01-Jan-2019) -- Start
                       'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.--%>
                    <uc4:confirmation ID ="LeaveIssueConfirmation" runat="server" Title ="Confirmation"  />
                      <%-- 'Pinkal (01-Jan-2019) -- End--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

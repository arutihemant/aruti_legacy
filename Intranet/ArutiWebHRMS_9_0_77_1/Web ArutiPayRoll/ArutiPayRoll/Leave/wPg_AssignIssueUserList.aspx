﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AssignIssueUserList.aspx.vb" Inherits="Leave_wPg_AssignIssueUserList"
    Title="Assign Issue User To Employee List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assign Issue User To Employee List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Assign Issue User To Employee List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 80%;">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblIssueUser" runat="server" Text="Issue User" Width="100%" />
                                            </td>
                                            <td style="width: 35%">
                                                <uc4:ComboList ID="drpIssueUser" runat="server" Width="280" Height="25" />
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee" Width="100%" />
                                            </td>
                                            <td style="width: 35%">
                                                <uc4:ComboList ID="drpEmployee" runat="server" Width="280" Height="25" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnl_gvView" runat="server" Height="400px" ScrollBars="Auto">
                                <asp:DataGrid ID="GvIssueUserList" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                        ToolTip="Delete"></asp:LinkButton>
                                                </span>
                                                <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                    CommandName="Delete" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMigration" runat="server" Text="Migration" Font-Underline="false"
                                                    CommandName="Migration" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="issueuser" HeaderText="Issue User" ReadOnly="true" FooterText="colhIssueUser" />
                                        <asp:BoundColumn DataField="employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployeeName" />
                                        <asp:BoundColumn DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment" />
                                        <asp:BoundColumn DataField="section" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                        <asp:BoundColumn DataField="job" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="true"
                                            Visible="false" />
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
                    <%--<asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="Panel2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                            <div>
                                <p>
                                    Are ou Sure You Want To delete?:</p>
                            </div>
                        </asp:Panel>
                        <table style="width: 180px;">
                            <tr>
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtreasondel" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="BtnDelete" runat="server" Text="Delete" />
                                    <asp:Button ID="BtnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="BtnCancel" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreasondel">
                    </cc1:ModalPopupExtender>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

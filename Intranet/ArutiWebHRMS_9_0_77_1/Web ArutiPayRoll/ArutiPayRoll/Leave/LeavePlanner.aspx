﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="LeavePlanner.aspx.vb" Inherits="LeavePlanner" Title="Leave Planner" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/LeaveType.ascx" TagName="LeaveType" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/PaidLeaveTYpe.ascx" TagName="PaidLeaveType" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                        $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                      
                }
            }
    </script>

    <center>
        <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Planner"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:Panel ID="pnldispGrid" Width="100%" runat="server">
                                        <div id="Div1" class="panel-default">
                                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div2" class="panel-body-default" style="text-align: left">
                                                <table style="width: 70%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee:"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                            <uc5:EmployeeList ID="ddlfilemloyee" runat="server" />
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblstartdate0" runat="server" Text="Start Date:"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <uc2:DateCtrl ID="dtfilstdate" runat="server" AutoPostBack="False" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblfilleavetype" runat="server" Text="Leave Type:"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                            <uc6:PaidLeaveType ID="ddlfilleavetype" runat="server" />
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblStopdate0" runat="server" Text="Stop Date:"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <uc2:DateCtrl ID="dtfilenddate" runat="server" AutoPostBack="False" />
                                                            <asp:TextBox ID="txtleaveplannerunkid" runat="server" Width="5px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="New" />
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnl_dgView" Width="100%" runat="server" Height="350px" ScrollBars="Auto">
                                            <asp:DataGrid ID="dgView" runat="server" Width="99%" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="btnSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                                    ToolTip="Edit"></asp:LinkButton>
                                                            </span>
                                                            <%-- <asp:ImageButton ID="" runat="server"  ImageUrl="~/images/edit.png"
                                                                />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                    ToolTip="Delete"></asp:LinkButton>
                                                            </span>
                                                            <%--<asp:ImageButton ID="" runat="server" " " ImageUrl="~/images/remove.png" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="leavename" HeaderText="Leave Type" ReadOnly="True" FooterText="colhLeaveType">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="startdate" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartdate">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="stopdate" HeaderText="Stop Date" ReadOnly="True" FooterText="colhStopdate">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlLeaveEntry" runat="server">
                                        <div id="Div3" class="panel-default">
                                            <div id="Div4" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div5" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%; padding-left: 15px;">
                                                            <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                                        </td>
                                                        <td style="width: 90%">
                                                            <asp:DropDownList ID="drpGender" runat="server" AutoPostBack="true" Width="15%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%">
                                                    <asp:Panel ID="pnlFirstTable" runat="server" Style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 33%; vertical-align: top">
                                                                <div id="Div9" class="panel-default" style="height: 370px">
                                                                    <div id="Div7" class="panel-default">
                                                                        <div id="Div6" class="panel-heading-default">
                                                                            <div style="float: left;">
                                                                                <asp:Label ID="lblfiltercriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Div8" class="panel-body-default">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <asp:RadioButtonList ID="rbtfiltertype" runat="server" AutoPostBack="True">
                                                                                        <asp:ListItem Value="1">Employee</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Section</asp:ListItem>
                                                                                        <asp:ListItem Value="3">Department</asp:ListItem>
                                                                                        <asp:ListItem Value="4">Job</asp:ListItem>
                                                                                    </asp:RadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="width: 33%">
                                                                <div id="Div10" class="panel-default">
                                                                    <div id="Div12" class="panel-body-default">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <asp:CheckBox ID="chkgrp" runat="server" Text="Group" AutoPostBack="True" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <cc1:TabContainer ID="TabContainer2" Width="100%" runat="server" ActiveTabIndex="0"
                                                                                        ScrollBars="Vertical" Height="280px">
                                                                                        <cc1:TabPanel ID="tabpan" runat="server" Width="100%" HeaderText="Group">
                                                                                            <ContentTemplate>
                                                                                                <asp:CheckBoxList ID="chklistgrp" runat="server" AutoPostBack="True" Height="220px"
                                                                                                    Width="100%" RepeatLayout="Flow">
                                                                                                </asp:CheckBoxList>
                                                                                            </ContentTemplate>
                                                                                        </cc1:TabPanel>
                                                                                    </cc1:TabContainer>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="width: 33%">
                                                                <div id="Div11" class="panel-default">
                                                                    <div id="Div13" class="panel-body-default">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <asp:CheckBox ID="chkEmp" runat="server" Text="Employee" AutoPostBack="True" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <cc1:TabContainer ID="TabContainer1" Width="100%" Height="280px" runat="server" ActiveTabIndex="0"
                                                                                        ScrollBars="Vertical">
                                                                                        <cc1:TabPanel ID="TabPanel1" runat="server" Width="100%" HeaderText="Employee">
                                                                                            <ContentTemplate>
                                                                                                <asp:CheckBoxList ID="chkbxlistemlployee" runat="server" AppendDataBoundItems="True"
                                                                                                    RepeatLayout="Flow" Height="250px" RepeatColumns="1" Width="100%" AutoPostBack="True">
                                                                                                </asp:CheckBoxList>
                                                                                            </ContentTemplate>
                                                                                        </cc1:TabPanel>
                                                                                    </cc1:TabContainer>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 100%;" colspan="3">
                                                            <div id="Div14" class="panel-default">
                                                                <div id="Div15" class="panel-heading-default">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="lblleaveinf" runat="server" Text="Leave Information"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div id="Div16" class="panel-body-default">
                                                                    <table style="width: 100%;">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblleavetype" runat="server" Text="Leave Type:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <uc6:PaidLeaveType ID="leavetype" runat="server" />
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblstartdate" runat="server" Text="Start Date:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <uc2:DateCtrl ID="dtStartDate" runat="server" AutoPostBack="False" />
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblStopdate" runat="server" Text="Stop Date:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <uc2:DateCtrl ID="dtEnddate" runat="server" AutoPostBack="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblremarks" runat="server" Text="Remark:"></asp:Label>
                                                                            </td>
                                                                            <td colspan="5" style="width: 86%">
                                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3" Width="99%"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass=" btnDefault" Text="Save" />
                                        <asp:Button ID="Btnclose" runat="server" CssClass=" btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="pnl2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                            <div>
                                <p>
                                    Are ou Sure You Want To delete?:</p>
                            </div>
                        </asp:Panel>
                        <table style="width: 180px;">
                            <tr>
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtreason" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="ButDel" runat="server" Text="Delete" />
                                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="CancelButton" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreason">
                    </cc1:ModalPopupExtender>--%>
                    <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

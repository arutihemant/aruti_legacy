﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgEmployeeUpdate.aspx.vb"
    Inherits="Recruitment_wPgEmployeeUpdate" Title="Update Detail Links" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>


    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Update Detail Links"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Update Detail Links"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:Panel ID="pnlEntry" Width="100%" runat="server">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:LinkButton ID="lnkMyQualification" runat="server" Text="Qualifications" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkMySkills" runat="server" Text="Skills" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkMyExperiences" runat="server" Text="Experiences" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkMyReferences" runat="server" Text="References" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:LinkButton ID="lnkMyProfile" runat="server" Text="View Profile" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkAddress" runat="server" Text="Address" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkPersonal" runat="server" Text="Personal" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                                <td style="width: 30%;">
                                                    <asp:LinkButton ID="lnkAttachment" runat="server" Text="Attachment" Font-Underline="false"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

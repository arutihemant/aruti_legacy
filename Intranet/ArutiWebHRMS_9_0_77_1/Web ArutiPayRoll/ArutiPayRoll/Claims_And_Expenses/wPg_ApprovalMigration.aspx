<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ApprovalMigration.aspx.vb" Inherits="Claims_And_Expenses_wPg_ApprovalMigration"
    Title="Expense Approval Migration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <script type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching(ctrl) {
        
            var txtSearch = $(ctrl);
            
            switch(txtSearch[0].id) {
              case 'txtOldSearchEmployee':
              if ($(txtSearch).val().length > 0) {
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgOldApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtOldSearchEmployee').val() + '\')').parent().show();
               }
               
               else if ($('#<%=dgOldApproverEmp.ClientID %>').val().length == 0) {
                    $('#dgOldApproverEmp').val('');
                    $('#<%=dgOldApproverEmp.ClientID %> tr').show();
                    $('#dgOldApproverEmp').focus();
              }
              break;
              
              case 'txtNewApproverMigratedEmp':
                if ($(txtSearch).val().length > 0) {
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtNewApproverMigratedEmp').val() + '\')').parent().show();
               }
               
               else if ($('#<%=dgNewApproverMigratedEmp.ClientID %>').val().length == 0) {
                    $('#dgNewApproverMigratedEmp').val('');
                    $('#<%=dgNewApproverMigratedEmp.ClientID %> tr').show();
                    $('#dgNewApproverMigratedEmp').focus();
              }
              break;
              
              case 'txtNewApproverAssignEmp':
                if ($(txtSearch).val().length > 0) {
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr').hide();
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr:first').show();
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtNewApproverAssignEmp').val() + '\')').parent().show();
               }
               
               else if ($('#<%=dgNewApproverAssignEmp.ClientID %>').val().length == 0) {
                    $('#dgNewApproverAssignEmp').val('');
                    $('#<%=dgNewApproverAssignEmp.ClientID %> tr').show();
                    $('#dgNewApproverAssignEmp').focus();
              }
              break;
              default:
                // code block
            } 
        }
        
        function gridCheckBoxAll(ctrl)
        {
              var chkHeader = $(ctrl);
              var grid = $(ctrl).closest("table");
              $("input[type=checkbox]", grid).each(function() {
                  if (chkHeader.is(":checked")) {
                     if ($(this).is(":visible")) {
                         $(this).attr("checked", "checked");
                     }
                  } 
                  else {
                     $(this).removeAttr("checked");
                 }
             });
        }  

        function gridSelectCheckBox(ctrl,checkboxall,checkboxselect)
        {
            var grid = $(ctrl).closest("table");
            var chkHeader = $("[id*="+checkboxall+"]", grid);
            var row = $(ctrl).closest("tr")[0];

            if (!$(ctrl).is(":checked")) {
                chkHeader.removeAttr("checked");
                
            } else {
                if ($("[id*="+checkboxselect+"]", grid).length == $("[id*="+checkboxselect+"]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        }  
    </script>

    <center>
        <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Approver Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                                            <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Category"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true" Width="200px">
                                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row2">
                                        <div class="ib" style="width: 42%; vertical-align: top">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                    <asp:CheckBox ID="chkShowInactiveApprovers" runat="server" AutoPostBack="true" Text="Inactive approver" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                            <asp:Label ID="lblOldApprover" runat="server" Text="From Approver"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                            <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                            <asp:Label ID="lblOldLevel" runat="server" Text="Level"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                            <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                    <input type="text" id="txtOldSearchEmployee" name="txtOldSearchEmployee" placeholder="type search text"
                                                        maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching(this);" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                            <div id="scrollable-container" style="vertical-align: top; overflow: auto; width: 100%;
                                                                height: 400px; margin-top: 5px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                <asp:GridView ID="dgOldApproverEmp" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames = "employeeunkid">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                        <asp:CheckBox ID="ChkOldAll" runat="server" onclick="gridCheckBoxAll(this)" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                        <asp:CheckBox ID="ChkgvOldSelect" runat="server" onclick="gridSelectCheckBox(this,'ChkOldAll','ChkgvOldSelect')" />
                                                                        <%--<asp:HiddenField ID="hfEmployeeid" runat="server" Value='<%# Eval("employeeunkid") %>' />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="true" FooterText="colhdgEmployeecode" />
                                                                        <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee" />
                                                                        <%--<asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true" Visible="false" />--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 12%; vertical-align: top; margin-top: 20%">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%; text-align: center">
                                                            <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btndefault" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%; text-align: center">
                                                            <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 42%; vertical-align: top">
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                    <asp:CheckBox ID="chkShowInActiveEmployees" runat="server" AutoPostBack="true" Text="Inactive employee" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                    <asp:Label ID="lblNewApprover" runat="server" Text="To Approver"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                    <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 20%">
                                                    <asp:Label ID="lblNewLevel" runat="server" Text="Level"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 72%">
                                                    <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                            <cc1:TabContainer ID="tabMain" runat="server" ActiveTabIndex="0">
                                                                <cc1:TabPanel ID="tbMigrationEmp" runat="server" HeaderText="Migrated Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                            <input type="text" id="txtNewApproverMigratedEmp" name="txtNewApproverMigratedEmp"
                                                                                placeholder="type search text" maxlength="50" style="height: 25px; font: 100"
                                                                                onkeyup="FromSearching(this);" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container1" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                                        <asp:GridView ID="dgNewApproverMigratedEmp" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="employeeunkid">
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                                    <HeaderTemplate>
                                                                                                <asp:CheckBox ID="ChkNewAll" runat="server" onclick="gridCheckBoxAll(this)" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                <asp:CheckBox ID="ChkgvNewSelect" runat="server" onclick="gridSelectCheckBox(this,'ChkNewAll','ChkgvNewSelect')" />
                                                                                              <%--  <asp:HiddenField ID="hfEmployeeid" runat="server" Value='<%# Eval("employeeunkid") %>' />--%>
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                </asp:TemplateField>
                                                                                                <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="True" FooterText="colhdgMigratedEmpCode">
                                                                                                    <HeaderStyle Width="30%" />
                                                                                                </asp:BoundField>
                                                                                                <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="True" FooterText="colhdgMigratedEmp">
                                                                                                    <HeaderStyle Width="70%" />
                                                                                                </asp:BoundField>
                                                                                                <%--<asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"  Visible="False" />--%>
                                                                                            </Columns>
                                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                                            <RowStyle CssClass="griviewitem" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                                <cc1:TabPanel ID="tbAssignedEmp" runat="server" HeaderText="Assigned Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                            <input type="text" id="txtNewApproverAssignEmp" name="txtNewApproverAssignEmp" placeholder="type search text"
                                                                                maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching(this);" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container2" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                                        <asp:GridView ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False"
                                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="ecode" HeaderStyle-Width="30%" HeaderText="Employee Code"
                                                                                                    ReadOnly="true" FooterText="colhdgAssignEmpCode" />
                                                                                                <asp:BoundField DataField="ename" HeaderStyle-Width="70%" HeaderText="Employee" ReadOnly="true"
                                                                                                    FooterText="colhdgAssignEmployee" />
                                                                                                <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                                    Visible="false" />
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

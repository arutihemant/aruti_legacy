﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Emp_QualificationRange.aspx.vb" Inherits="Reports_Rpt_Emp_QualificationRange"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/Controls/Closebutton.ascx" tagname="Closebutton" tagprefix="uc1" %>
<%@ Register src="~/Controls/DateCtrl.ascx" tagname="DateCtrl" tagprefix="uc2" %>
<%@ Register src="~/Controls/EmployeeList.ascx" tagname="EmployeeList" tagprefix="uc4" %>
<%@ Register src="~/Controls/GetComboList.ascx" tagname="DropDownList" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

<script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
 </script> 
<center>
    <asp:Panel ID="Panel1" runat="server" Style="width: 50%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Qualification Range Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                               <asp:Label ID = "lblEmployee" runat="server"  
                                                    Text = "Employee"></asp:Label>
                                            </td>
                                            <td style="width: 45%">
                                                <asp:DropDownList ID="drpEmployee" runat="server" ></asp:DropDownList>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID = "lblQualifLevel" 
                                                        runat="server"  Text = "Qualification Level"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:TextBox ID = "TxtLevel" runat="server" Enabled="false" style="text-align:right; background-color:White"></asp:TextBox>
                                                     <cc1:NumericUpDownExtender ID = "nudLevel" runat="server" Width="70" Minimum="0" Maximum = "999" TargetControlID="TxtLevel"></cc1:NumericUpDownExtender>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID = "lblCondition" runat="server"  
                                                    Text = "Condition"></asp:Label>
                                            </td>
                                            <td style="width: 45%">
                                                 <asp:DropDownList ID="drpCondition"  runat="server"></asp:DropDownList>
                                            </td>
                                            <td style="width:40%" colspan="2"></td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 85%" colspan="3">
                                                <asp:CheckBox ID="chkInActiveEmp" runat="server" Text = "Include Inactive Employee"  Checked = "false" CssClass="chkbx"></asp:CheckBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                         <asp:Button ID = "BtnReset" runat ="server" Text = "Reset" CssClass="btndefault" />
                            <asp:Button ID = "BtnReport" runat ="server" Text = "Report" CssClass="btndefault" />
                            <asp:Button ID = "BtnClose" runat ="server" Text = "Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</center>
              
</asp:Content>

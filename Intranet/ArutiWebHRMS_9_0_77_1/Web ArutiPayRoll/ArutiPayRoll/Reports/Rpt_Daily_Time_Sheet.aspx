<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Daily_Time_Sheet.aspx.vb"
    Inherits="Reports_Rpt_Daily_Time_Sheet" Title="Daily Time Sheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Daily Time Sheet Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtDate" runat="server" />
                                            </td>
                                            <td style="width: 40%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |29-MAR-2019| -- START--%>
                                        <%--'ENHANCEMENT : 0003630 {PAPAYE}.--%>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblShiftType" runat="server" Text="Shift Type"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="cboShiftType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |29-MAR-2019| -- END--%>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblShift" runat="server" Text="Shift"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="cboShift" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlColumns" runat="server" Width="100%">
                                        <div id="Div2" class="panel-default">
                                            <div id="Div1" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="gbShowColumns" runat="server" Text="Column(s) Display"></asp:Label>
                                                </div>
                                                <div style="text-align: right">
                                                    <asp:LinkButton ID="lnkSaveChanges" runat="server" Text="Save Changes" CssClass="lnkhover"></asp:LinkButton>
                                                </div>
                                            </div>
                                            <table width="100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 29%">
                                                        <asp:Label ID="lblAllocation" runat="server" Text="Display On Report"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="cboReportColumn" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 25%">
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkShowEmployeeStatus" runat="server" Text="Show Employee Status" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_PerformanceEvaluationReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_PerformanceEvaluationReport" %>

<%--'S.SANDEEP |27-JUL-2019| -- START--%>
<%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--'S.SANDEEP |27-JUL-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Performance Evaluation Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAllocation" runat="server" Text="Allocation"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboAllocations" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |27-MAY-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboScoreOption" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |27-MAY-2019| -- END--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" Width="99%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:Panel ID="Panel2" runat="server" Width="100%" Height="180px" ScrollBars="Auto">
                                                    <asp:DataGrid ID="lvAllocation" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="Name" HeaderText="" />
                                                            <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%">
                                                            <asp:LinkButton ID="lnkDisplayChart" Text="Display Analysis Chart" runat="server"></asp:LinkButton>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:LinkButton ID="lnkDisplayHPOCurve" runat="server" Text="Display HPO Curve"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                    <cc1:ModalPopupExtender ID="popupAnalysisChart" BackgroundCssClass="modalBackground"
                        TargetControlID="Panel3" runat="server" PopupControlID="pnlAnalysisChart" CancelControlID="btnClAnalysisChart" />
                    <asp:Panel ID="pnlAnalysisChart" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px; top: 30px;" Height="535px">
                        <table style="width: 100%">
                            <tr style="width: 100%">
                                <td style="width: 20%" valign="top">
                                    <asp:Panel ID="Panel3" runat="server" Height="475px" ScrollBars="Auto">
                                        <asp:DataGrid ID="lvDisplayAllocation" runat="server" AutoGenerateColumns="false"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundColumn DataField="Name" HeaderText="" />
                                            </Columns>
                                        </asp:DataGrid>
                                    </asp:Panel>
                                </td>
                                <td style="width: 70%" valign="top">
                                    <asp:Panel ID="achart" runat="server" Height="100%" Width="100%" ScrollBars="Auto">
                                        <asp:Chart ID="chAnalysis_Chart" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                            BorderlineWidth="2" AntiAliasing="All" Width="720px" Height="480px">
                                            <%--Height="475px" Style="width: 100%"--%>
                                            <Titles>
                                                <asp:Title Name="Title1">
                                                </asp:Title>
                                            </Titles>
                                            <Series>
                                                <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column">
                                                </asp:Series>
                                                <asp:Series BorderWidth="3" ChartArea="ChartArea1" ChartType="Spline" LabelBorderWidth="3"
                                                    MarkerSize="10" MarkerStyle="Diamond" Name="Series2">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1" Area3DStyle-WallWidth="5">
                                                    <AxisY2 Enabled="True">
                                                    </AxisY2>
                                                    <Area3DStyle WallWidth="5" />
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr style="width: 100%">
                                <td style="width: 100%" align="right" colspan="2">
                                    <asp:Button ID="btnClAnalysisChart" runat="server" Text="Close" CssClass="btndefault" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="Panel4"
                        runat="server" PopupControlID="pnlHPOChart" CancelControlID="btnClHpoClose" />
                    <asp:Panel ID="pnlHPOChart" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px; top: 30px;" Height="535px">
                        <table style="width: 100%">
                            <tr style="width: 100%;">
                                <td style="width: 100%" valign="top">
                                    <asp:Panel ID="Panel4" runat="server" Height="165px" ScrollBars="Auto">
                                        <asp:DataGrid ID="dgvCalibData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            ShowHeader="false">
                                        </asp:DataGrid>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr style="width: 100%">
                                <td style="width: 100%" valign="top">
                                    <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                        BorderlineWidth="2" Width="890px" Height="300px" AntiAliasing="All">
                                        <Titles>
                                            <asp:Title Name="Title1">
                                            </asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                            </asp:Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </td>
                            </tr>
                            <tr style="width: 100%">
                                <td style="width: 100%" align="right">
                                    <asp:Button ID="btnClHpoClose" runat="server" Text="Close" CssClass="btndefault" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

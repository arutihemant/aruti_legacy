﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_EmpAssetDeclarationCategoryWiseT2.aspx.vb"
    Inherits="Reports_Rpt_EmpAssetDeclarationCategoryWiseT2" Title="Employee Declaration Categorywise Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Declaration Categorywise Report "></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                      
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblAsOnDate" runat="server" Text="As On Date"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <uc2:DateCtrl ID="dtpAsOnDate" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="row2" style="display: none">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblreporttype" runat="server" Text="Report Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboReporttype" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblAppointment" runat="server" Text="Appointment"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboAppointment" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="objlblCaption" runat="server" Text="From"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 33%">
                                            <uc2:DateCtrl ID="dtpDate1" runat="server" AutoPostBack="false" />
                                        </div>
                                        <asp:Panel class="ibwm" ID="pnlappdate" runat="server" Width="41%">
                                        <div class="ib" style="width: 3%">
                                            <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 84%">
                                            <uc2:DateCtrl ID="dtpDate2" runat="server" AutoPostBack="false" />
                                        </div>
                                        </asp:Panel>
                                        
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblcategory" runat="server" Text="Asset Category"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboADCategory" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="lnkAdvanceFilter" runat="server" CssClass="btndefault" Text="Advance Filter" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                            <uc3:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Employee_Listing.aspx.vb" Inherits="Reports_Rpt_Employee_Listing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Listing Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <%--'Hemant (28 Apr 2020) -- Start--%>
                                    <%--'Enhancement - Analysis By on Employee Listing Report--%>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                    <%--'Hemant (28 Apr 2020) -- End--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative;">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="drpemployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployeementType" runat="server" Text="Employement Type"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="drpEmployeementType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |15-JUL-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkGeneratePeriod" runat="server" Text="Generate Based on Period"
                                                    AutoPostBack="true" OnCheckedChanged="chkGeneratePeriod_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |15-JUL-2019| -- END--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblBirthdateFrom" runat="server" Text="Birthdate From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblBirthdateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpBirthdateTo" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblAnniversaryFrom" runat="server" Text="Anniversary Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAnniversaryDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblAnniversaryDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAnniversaryDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblFirstAppointDateFrom" runat="server" Text="First Appointment Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpFAppointDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblFirstAppointDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpFAppointDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblAppointDateFrom" runat="server" Text="Appointment Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAppointDateFrom" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblAppointDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAppointDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblConfirmationDateFrom" runat="server" Text="Confirmation Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpConfirmationDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblConfirmationDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpConfirmationDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblProbationStartDateFrom" runat="server" Text="Probation Start Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationStartDateFrom" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblProbationStartDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationStartDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblProbationEndDateFrom" runat="server" Text="Probation End Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationEndDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblProbationEndDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationEndDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblSuspendedStartDateFrom" runat="server" Text="Suspended Start Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedStartDateFrom" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblSuspendedStartDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedStartDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblSuspendedEndDateFrom" runat="server" Text="Suspended End Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedEndDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblSuspendedEndDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedEndDateTo" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblReinstatementDateFrom" runat="server" Text="Reinstatement Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpReinstatementFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblReinstatementDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpReinstatementTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblEOCDateFrom" runat="server" Text="End Of Contract Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpEOCDateFrom" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblEOCDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpEOCDateTo" runat="server"  AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="Label2" runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" Checked="false"
                                                    CssClass="chkbx" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--'Hemant (28 Apr 2020) -- Start--%>
                        <%--'Enhancement - Analysis By on Employee Listing Report--%>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        <%--'Hemant (28 Apr 2020) -- End--%>
                    </div>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB"  MasterPageFile="~/home.master"  AutoEventWireup="false" CodeFile="Rpt_LeaveStatementReport.aspx.vb" Inherits="Reports_Rpt_LeaveStatementReport" title="Leave Statement Report" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/Controls/Closebutton.ascx" tagname="Closebutton" tagprefix="uc1" %>
<%@ Register src="~/Controls/DateCtrl.ascx" tagname="DateCtrl" tagprefix="uc2" %>
<%@ Register src="~/Controls/EmployeeList.ascx" tagname="EmployeeList" tagprefix="uc4" %>
<%@ Register src="~/Controls/GetComboList.ascx" tagname="DropDownList" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

<script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
 </script> 
 
   <center>
    <asp:Panel ID="Panel1" runat="server" Style="width: 35%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Statement Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               <asp:Label ID = "LblAsOnDate" runat = "server" Text = "As On Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID = "dtpAsonDate" Width="75" runat = "server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID = "LblEmployee" runat = "server" Text = "Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID = "cboEmployee" runat = "server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               <asp:Label ID = "lblLeaveName" runat = "server" Text = "Leave"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                               <asp:DropDownList ID = "cboLeave" runat = "server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                               <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                               <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
   </center>
 
 </asp:Content>
 
 

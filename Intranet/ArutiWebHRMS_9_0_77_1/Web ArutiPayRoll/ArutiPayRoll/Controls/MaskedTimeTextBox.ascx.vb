﻿Imports System.Data
Imports System.Globalization
Imports Aruti.Data

Partial Class Controls_MaskedTimeTextBox
    Inherits System.Web.UI.UserControl

    'Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Property "
    Public Property AutoPostBack() As Boolean
        Get
            Return txtHours.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtHours.AutoPostBack = value
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Return txtHours.Width.Value
        End Get
        Set(ByVal value As Integer)
            txtHours.Width = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return txtHours.Text
        End Get
        Set(ByVal value As String)
            txtHours.Text = value
        End Set
    End Property

    Public Property ValidationGroup() As String
        Get
            Return txtHours.ValidationGroup
        End Get
        Set(ByVal value As String)
            txtHours.ValidationGroup = value
            MaskedEditValidatorHours.ValidationGroup = value
        End Set
    End Property

    Public WriteOnly Property InvalidValueMessage() As String
        Set(ByVal value As String)
            MaskedEditValidatorHours.InvalidValueMessage = value
        End Set
    End Property

    Public WriteOnly Property EmptyValueMessage() As String
        Set(ByVal value As String)
            MaskedEditValidatorHours.EmptyValueMessage = value
        End Set
    End Property

    Public WriteOnly Property MaximumValueMessage() As String
        Set(ByVal value As String)
            MaskedEditValidatorHours.MaximumValueMessage = value
        End Set
    End Property

    Public WriteOnly Property MinimumValueMessage() As String
        Set(ByVal value As String)
            MaskedEditValidatorHours.MinimumValueMessage = value
        End Set
    End Property

    Public WriteOnly Property IsErrorMessageDynamic() As Boolean
        Set(ByVal value As Boolean)
            If value = True Then
                MaskedEditValidatorHours.Display = ValidatorDisplay.Dynamic
            Else
                MaskedEditValidatorHours.Display = ValidatorDisplay.None
            End If
        End Set
    End Property

#End Region

    'Protected Sub txtHours_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHours.TextChanged
    '    Try
    '        RaiseEvent TextChanged(sender, e)
    '    Catch ex As Exception
    '        Throw New Exception("txtHours_TextChanged:- " & ex.Message, ex)
    '    End Try
    'End Sub

End Class

﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Delete_Reason.ascx.vb"
    Inherits="Controls_Delete_Reason" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG2"
    CancelControlID="btnDelReasonNo" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 450px;
    z-index: 100002!important;" DefaultButton="btnDelReasonYes">
    <div class="panel-primary" style="margin-bottom: 0px;">
        <div class="panel-heading">
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <div style="height:160px;overflow:auto">
                        <table style="width: 100%">
                            <tr style="width: 100%">
                                <td style="width: 100%">
                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                </td>
                            </tr>
                            <tr style="width: 100%">
                                <td style="width: 100%;">
                                    <asp:RadioButtonList ID="rdbReason" runat="server" Width="97%" Height="200px" >
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="btn-default">
                        <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" Width="70px" 
                            CssClass="btnDefault" ValidationGroup="Reason" />
                        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" Width="70px"
                            CssClass="btnDefault" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

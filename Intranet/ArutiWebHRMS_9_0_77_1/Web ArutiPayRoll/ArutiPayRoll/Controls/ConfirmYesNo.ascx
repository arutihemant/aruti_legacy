﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfirmYesNo.ascx.vb"
    Inherits="Controls_ConfirmYesNo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG"
    CancelControlID="btnNo" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 400px;
    z-index: 100099!important;" DefaultButton="btnYes">
    <div class="panel-primary" style="margin-bottom: 0px">
        <div class="panel-heading">
            <asp:Label ID="lblTitle" runat="server" Text="Title" />
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Label ID="lblMessage" runat="server" Text="Message :"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" ValidationGroup="ConfirmYes" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

﻿
Partial Class Controls_OperationButton
    Inherits System.Web.UI.UserControl

    Public Property Text() As String
        Get
            Return btnOperation.Text
        End Get
        Set(ByVal value As String)
            btnOperation.Text = value
        End Set
    End Property

    Public WriteOnly Property TragateControlId() As String
        Set(ByVal value As String)
            btnOperation.Attributes.Add("targetcontrolid", value)
        End Set
    End Property

End Class

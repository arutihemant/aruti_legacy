﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NumericTextBox.ascx.vb"
    Inherits="Controls_NumericTextBox" %>

<script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
  
</script>

<asp:TextBox ID="txtNumeric" runat="server" Text="" onKeypress="return onlyNumbers(this, event);"
    Style="text-align: right;">
</asp:TextBox>

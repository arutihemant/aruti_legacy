﻿
Partial Class Closebutton
    Inherits System.Web.UI.UserControl

    Private _pagehead As String

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private _IsEmp As Boolean = False
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

    Public Event CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    Public Property PageHeading() As String
        Get
            Return _pagehead
        End Get
        Set(ByVal value As String)
            _pagehead = value
	    'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            If Me.IsEmployee = True Then
                LblformName.Text = Me.Page.Title
            Else
                LblformName.Text = Me.PageHeading
            End If
            'Anjan [04 June 2014] -- End
        End Set
    End Property

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property IsEmployee() As Boolean
        Get
            Return _IsEmp
        End Get
        Set(ByVal value As Boolean)
            _IsEmp = value
        End Set
    End Property
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'LblformName.Text = Me.PageHeading
        If Me.IsEmployee = True Then
            LblformName.Text = Me.Page.Title
        Else
        LblformName.Text = Me.PageHeading
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
    End Sub



    'Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
    '    RaiseEvent CloseButton_click(sender, e)
    'End Sub
End Class

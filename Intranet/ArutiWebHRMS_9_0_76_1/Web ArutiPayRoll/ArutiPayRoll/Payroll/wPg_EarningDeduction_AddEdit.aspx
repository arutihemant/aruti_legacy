﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EarningDeduction_AddEdit.aspx.vb" Inherits="Payroll_wPg_EarningDeduction_AddEdit"
    Title="Add / Edit Employee Earning and Deduction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">

     function onlyNumbers(txtBox, e) {
         //        var e = event || evt; // for trans-browser compatibility
         //        var charCode = e.which || e.keyCode;
         if (window.event)
             var charCode = window.event.keyCode;       // IE
         else
             var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

         if (charCode == 13)
             return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Employee Earning and Deduction"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Add / Edit Employee Earning and Deduction"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Effective Period" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblTrnHeadType" runat="server" Text="Transaction Head Type" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblTypeOf" runat="server" Text="Transaction Type Of" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboTypeOf" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblTrnHead" runat="server" Text="Transaction Head" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboTrnHead" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblAmount" runat="server" Text="Amount" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:TextBox ID="txtAmount" runat="server" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this, event);" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboCostCenter" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblMedicalRefNo" runat="server" Text="Reference No." />
                                            </td>
                                            <td style="width: 65%">
                                                <asp:TextBox ID="txtMedicalRefNo" runat="server" Text="" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblCumulativeStartDate" runat="server" Text="Cumulative Start Date" />
                                            </td>
                                            <td style="width: 65%">
                                                <uc2:DateCtrl ID="dtpCumulativeStartDate" runat="server"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblEDStartDate" runat="server" Text="E. D. Start Date" />
                                            </td>
                                            <td style="width: 65%">
                                                <uc2:DateCtrl ID="dtpEDStartDate" runat="server"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblStopDate" runat="server" Text="Stop Date" />
                                            </td>
                                            <td style="width: 65%">
                                                <uc2:DateCtrl ID="dtpStopDate" runat="server"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                            </td>
                                            <td style="width: 65%">
                                                <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous ED Slab"
                                                    AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                            </td>
                                            <td style="width: 65%">
                                                <asp:CheckBox ID="chkOverwritePrevEDSlabHeads" runat="server" Text="Overwrite Previous ED Slab heads" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <asp:Label ID="lblDefCC" runat="server" Text="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSaveEDHistory" runat="server" Text="Save" Width="75px" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="75px" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:ConfirmYesNo ID="popupFormulaAssign" runat="server" Title="" />
                    <uc9:ConfirmYesNo ID="popupPreviousED" runat="server" Title="" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

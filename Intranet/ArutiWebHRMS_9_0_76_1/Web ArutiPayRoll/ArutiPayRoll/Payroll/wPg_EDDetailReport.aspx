﻿<%@ Page Title="E&D Detail Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EDDetailReport.aspx.vb" Inherits="Payroll_wPg_EDDetailReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="E&D Detail Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblSelectMode" runat="server" Text="Mode Selection"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="ddlMode" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblHeads" runat="server" Text="Heads"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="ddlHeads" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblMembership" runat="server" Text="Membership"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="drpMembership" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="From Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="drpPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblTo" runat="server" Text="To Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="drpPeriodTo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkIncludeNegativeHeads" runat="server" Text="Include Negative Heads"
                                                    Checked="true" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btnDefault" ValidationGroup="Payslip" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" ValidationGroup="Payslip"
                                            CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>


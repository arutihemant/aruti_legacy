﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Payment_List.aspx.vb"
    Inherits="Payroll_wPg_Payment_List" Title="Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());

            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Payment List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%;">
                                            <td style="width: 8%;">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 35%;" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%;">
                                                <asp:Label ID="lblVoucherno" runat="server" Text="Voucher No."></asp:Label>
                                            </td>
                                            <td style="width: 13%;">
                                                <asp:TextBox ID="txtVoucherNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 5%;">
                                                <asp:Label ID="lblPaymentDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 13%;">
                                                <uc2:DateCtrl ID="dtpPaymentDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblPayYear" runat="server" Text="Pay Year"></asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:DropDownList ID="cboPayYear" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:DropDownList ID="cboPayPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblPaidAmount" runat="server" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <uc9:txtNumeric ID="txtPaidAmount" runat="server" Style="text-align: right;" />
                                            </td>
                                            <td style="width: 5%">
                                                <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <uc9:txtNumeric ID="txtPaidAmountTo" runat="server" Style="text-align: right;" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                    overflow: auto; max-height: 300px; margin-top: 5px; margin-bottom: 5px">
                                    <asp:DataGrid ID="dgvPayment" runat="server" Width="99%" AutoGenerateColumns="False"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="EditImg" runat="server" CssClass="gridedit" CommandName="Edit"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Date" HeaderText="Date" ReadOnly="True" FooterText="colhDate">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Voc" HeaderText="Voucher#" ReadOnly="True" FooterText="colhVoucher">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="employeecode" HeaderText="Emp.Code" Visible="True" FooterText="colhEmpCode">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" Visible="True" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PeriodName" HeaderText="Pay Period" Visible="True" FooterText="colhPayPeriod">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="expaidamt" HeaderText="Paid Amount" Visible="True" FooterText="colhPaidAmount"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="currency_sign" HeaderText="Currency" Visible="True" FooterText="colhCurrency">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="paidcurrencyid" HeaderText="paidcurrencyid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="periodunkid" HeaderText="periodunkid" Visible="false"
                                                FooterText="colhPayPeriod"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="paymentdate" HeaderText="paymentdate" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="paymenttranunkid" HeaderText="paymenttranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="paymentdate_periodunkid" HeaderText="paymentdate_periodunkid"
                                                Visible="false" FooterText="colhPaymentDatePeriod"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false"
                                                FooterText="colhEmployee"></asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc8:DeleteReason ID="popupDelReason" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_CalibrateList.aspx.vb" Inherits="Assessment_New_Peformance_Calibration_wPg_CalibrateList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="Date" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CV" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <style>
        .vl
        {
            border-left: 1px solid black;
            height: 15px;
        }
        .swal-wide
        {
            width: 600px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };


        function ListSeaching() {
            if ($('#<%= txtClistSearch.ClientID %>').val().length > 0) {
                $('#<%= gvCalibrateList.ClientID %> tbody tr').hide();
                $('#<%= gvCalibrateList.ClientID %> tbody tr:first').show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtClistSearch.ClientID %>').val() + '\')').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.MainGroupHeaderStyleLeft').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.GroupHeaderStylecompLeft').parent().show();
            }
            else if ($('#<%= txtClistSearch.ClientID %>').val().length == 0) {
                resetListSeaching();
            }
            if ($('#<%= gvCalibrateList.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetListSeaching();
            }
        }
        function resetListSeaching() {
            $('#<%= txtClistSearch.ClientID %>').val('');
            $('#<%= gvCalibrateList.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtClistSearch.ClientID %>').focus();
        }


        function FromSearching() {
            if ($('#<%= txtListSearch.ClientID %>').val().length > 0) {
                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtListSearch.ClientID %>').val() + '\')').parent().show();
            }
            else if ($('#<%= txtListSearch.ClientID %>').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= gvApplyCalibration.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#<%= txtListSearch.ClientID %>').val('');
            $('#<%= gvApplyCalibration.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtListSearch.ClientID %>').focus();
        }
        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {

                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });

        function iSubmitOpr() {
            var txtvalue = document.getElementById("<%=txtICalibrationNo.ClientID%>");
            if (txtvalue.disabled == false) {
                if (txtvalue.value == '') {
                    //alert('Sorry, Calibration number is mandatory information. Please provide calibration number to continue.');                   
                    swal({ title: '', text: "Sorry, Calibration number is mandatory information. Please provide calibration number to continue." });
                    return;
                }
                if (txtvalue.value != '') {
                    PageMethods.IsNumberExists(txtvalue.value, onSuccess, onFailure);
                    function onSuccess(str) {
                        if (str == "1") {
                            //alert('Sorry, This calibration number is already exists. Please provide another calibration number to continue.');                            
                            swal({ title: '', text: "Sorry, This calibration number is already exists. Please provide another calibration number to continue." });
                            return;
                        }
                    }

                    function onFailure(err) {
                        //alert(err.get_message());
                        swal({ title: '', text: err.get_message() });
                        return;
                    }
                }
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                //alert('Sorry, No records found in order to perform selected operation.');
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }

            var msrv = '<%= Session("Mailserverip") %>';
            if (msrv == "") {
                swal({ title: "", type: "info", text: "Sorry, you have not configured, Email account setup under company registration. Please go to aruti configuration in oder to configure email setup." });
                return;
            }

            var flg = false;
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");

            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRate") {
                        if ($(col).html() == '&nbsp;') {
                            flg = true;
                            break;
                        }
                    }
                }
                if (flg == true) { break; }
            }
            if (flg == true) {
                //alert('Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation?');
                swal({ title: '', text: "Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation." });
                return;
            }

            var iMsg = '';
            if (flg == false) {
                var icode = 0;
                iMsg = '<div style="height:200px; width:100%;overflow-y: scroll;">';
                for (var i = 1, row; row = table.rows[i]; i++) {
                    iMsg += '<div class="row2">';
                    for (var j = 1, col; col = row.cells[j]; j++) {
                        if ($(col).attr('Id') != undefined && $(col).attr('Id') == "mCode") {
                            if (parseInt($(col).html()) != 0) {
                                flg = true;
                                iMsg += '<div style="width: 46%" class="ib">' + row.cells[1].innerHTML + ' - ' + row.cells[2].innerHTML + '</div>';
                                switch (parseInt($(col).html())) {
                                    case 1:
                                        iMsg += '<div style="width: 50%" class="ib">Approver Not Assigned</div>';
                                        break;
                                    case 2:
                                        iMsg += '<div style="width: 50%" class="ib">Email not Assigned to Approver</div>';
                                        break;
                                }
                            }
                        }
                    }
                    iMsg += '</div>';
                }
            }
            if (flg == true) {
                swal({ html: true, type: "error", title: 'Information Missing', text: iMsg, customClass: 'swal-wide' });
                return;
            }

            if (flg == false) {
                for (var i = 1, row; row = table.rows[i]; i++) {
                    for (var j = 1, col; col = row.cells[j]; j++) {
                        if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRem") {
                            if ($(col).html() == '&nbsp;') {
                                flg = true;
                                break;
                            }
                        }
                    }
                    if (flg == true) { break; }
                }
                if (flg == true) {
                    //                    var yes = confirm('You have not set calibration remark for some of employee. Do you wish to continue with submit operation.');
                    //                    if (yes == true) {

                    //                    }
                    //                    else {
                    //                        return;
                    //                    }

                    swal({
                        title: '',
                        text: "You have not set calibration remark for some of employee. Do you wish to continue with submit operation?",
                        //type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function(isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: '',
                                text: "Enter Submission Remark",
                                type: "input",
                                showCancelButton: false,
                                closeOnConfirm: false,
                                animation: "slide-from-top",
                                inputPlaceholder: "Submit Remark"
                            }, function(inputValue) {
                                if (inputValue === false) return false;

                                if (inputValue === "") {
                                    swal.showInputError("Please provide remark in order to submit.");
                                    return false;
                                }
                                var remark = inputValue;
                                var arr = [];
                                var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                                $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                                    var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                                    arr.push(guid);
                                });
                                var coyid = '<%= Session("CompanyUnkId") %>';
                                var ipadd = '<%= Session("IP_ADD") %>';
                                var hstname = '<%= Session("HOST_NAME") %>';
                                var calusrid = '<%= Session("UserId") %>';
                                var sndradd = '<%= Session("Senderaddress") %>';

                                swal({ title: '', type: "info", text: "Performing selected operation and sending notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                                PageMethods.CalibrationSubmission(coyid, $(pid).val(), calusrid, arr, txtvalue.value, pid.options[pid.selectedIndex].text, remark, "frmScoreCalibrationAddEdit", sndradd, ipadd, hstname, onSuccess, onFailure);
                                
                                function onSuccess(str) {
                                    if (str == "1") {
                                        $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                        $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                        swal({ title: '', type: "success", text: "Information submitted successfully." });
                                    }
                                    else if (str == "0") {
                                        swal({ title: '', type: "warning", text: "Problem in submitting calibration." });
                                    }
                                }

                                function onFailure(err) {

                                    swal({ title: '', text: err.get_message() });
                                    return;
                                }
                                //swal("Nice!", "You wrote: " + inputValue, "success");
                            });
                        }
                    });
                    return;
                }
                else {
                    swal({
                        title: '',
                        text: "Enter Submission Remark",
                        type: "input",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Submit Remark"
                    }, function(inputValue) {
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Please provide remark in order to submit.");
                            return false;
                        }
                        var remark = inputValue;
                        var arr = [];
                        var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                            arr.push(guid);
                        });
                        var coyid = '<%= Session("CompanyUnkId") %>';
                        var ipadd = '<%= Session("IP_ADD") %>';
                        var hstname = '<%= Session("HOST_NAME") %>';
                        var calusrid = '<%= Session("UserId") %>';
                        var sndradd = '<%= Session("Senderaddress") %>';

                        swal({ title: '', type: "info", text: "Sending Notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                        PageMethods.CalibrationSubmission(coyid, $(pid).val(), calusrid, arr, txtvalue.value, pid.options[pid.selectedIndex].text, remark, "frmScoreCalibrationAddEdit", sndradd, ipadd, hstname, onSuccess, onFailure);
                        function onSuccess(str) {
                            if (str == "1") {
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                swal({ title: '', type: "success", text: "Information submitted successfully." });
                            }
                            else if (str == "0") {
                                swal({ title: '', type: "warning", text: "Problem in submitting calibration." });
                            }
                        }

                        function onFailure(err) {

                            swal({ title: '', text: err.get_message() });
                            return;
                        }
                        //swal("Nice!", "You wrote: " + inputValue, "success");
                    });
                }
            }
        }

        function RatingOperation(ival) {
            var arr = [];
            var ratingid = 0;
            var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
            var rid = document.getElementById("<%=cbocRating.ClientID %>");
            if (ival == 1) {
                var rid = document.getElementById("<%=cbocRating.ClientID%>");
                ratingid = parseInt($(rid).val());
                if (ratingid <= 0) {
                    //alert('Sorry, Please select rating in order to perform calibrate operation.');
                    swal({ title: '', text: "Sorry, Please select rating in order to perform calibrate operation." });
                    return;
                }
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                //alert('Sorry, No records found in order to perform selected operation.');
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }
            var txt = document.getElementById("<%=txtIRemark.ClientID%>").value;
            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                    arr.push(guid);
                }
            });
            if (arr == null) {
                //alert('Sorry, Please check atleast one information in order to calibrate score.');
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }
            if (arr != null && arr.length <= 0) {
                //alert('Sorry, Please check atleast one information in order to calibrate score.');
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }
            if (arr != null && arr.length > 0) {

                PageMethods.RatingUpdate(parseInt($(pid).val()), arr, txt, ratingid, onSuccess, onFailure);

                function onSuccess(str) {
                    if (str == "1") {
                        for (var k = 0; k < arr.length; k++) {
                            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                            for (var i = 1, row; row = table.rows[i]; i++) {
                                for (var j = 1, col; col = row.cells[j]; j++) {
                                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "trgid") {
                                        if ($(col).html() == arr[k]) {
                                            $(row).closest("tr").find("td[Id='cRem']").html(txt);
                                            if (ival == 1) { $(row).closest("tr").find("td[Id='cRate']").html(rid.options[rid.selectedIndex].innerHTML); }
                                            else { $(row).closest("tr").find("td[Id='cRate']").html($(row).closest("tr.griviewitem").find("td[Id='pRate']").text()); }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            if ($(this).is(':checked')) {
                                var grd = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                                var tbod = grd.rows[0].parentNode;
                                var row = $(this).closest("tr");
                                var newRow = row[0].cloneNode(true);
                                tbod.append(newRow);
                                $(newRow).closest("tr")[0].style.color = "#0000FF"
                                $(this).closest("tr").remove();
                            }
                        });
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            if ($(this).is(':checked')) {
                                $(this).removeAttr("checked");
                            }
                        });
                        //alert('Selected Operation performed successfully.');
                        swal({ title: '', text: "Selected Operation performed successfully." });
                    }
                }

                function onFailure(err) {
                    //alert(err.get_message());
                    swal({ title: '', text: err.get_message() });
                    return;
                }

            }
        }

        function GetSelected() {
            var ischkboxfound = false;
            var grid = document.getElementById("<%=gvFilterRating.ClientID%>");
            var checkBoxes = grid.getElementsByTagName("INPUT");
            $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
            $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i] != undefined && checkBoxes[i].checked) {
                    var row = $(checkBoxes[i]).closest("tr").find("td:eq(1)").attr('title');
                    ischkboxfound = true;
                    var res = row.split(" - ");
                    RangeSearch(parseFloat(res[0]), parseFloat(res[1]));
                }
            }
            if (ischkboxfound == false) {
                $('#<%= gvApplyCalibration.ClientID %> tr').show();
            }
        }

        function RangeSearch(val1, val2) {
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "pScore") {
                        if (parseFloat($(col).html()) >= val1 && parseFloat($(col).html()) <= val2) {
                            $(row).show();
                        }
                    }
                }
            }
        }

        //        function FillCalibNumn(drp) {
        //            debugger;
        //            var myindex = drp.selectedIndex;
        //            var SelValue = drp.options[myindex].value;
        //            cbno = document.getElementById("<%=cboRptCalibNo.ClientID %>");
        //            //cbno.empty();
        //            var calusrid = '<%= Session("UserId") %>';
        //            PageMethods.GetCalibrationNo(SelValue, calusrid, onSuccess);
        //            function onSuccess(response) {
        //                for (var i in response) {
        //                    if (response[i].Value == 0) {
        //                        cbno.append('<option selected="selected" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                        cbno.nextSibling.append('<option title="' + response[i].Text + '" lang="' + i.toString() + '" selected="selected" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                    }
        //                    else {
        //                        cbno.append('<option value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                        cbno.nextSibling.append('<option title="' + response[i].Text + '" lang="' + i.toString() + '" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                    }
        //                    //cbno.append('<Option value=' + response[i].Value + '>' + response[i].Text + '</Option>')
        //                    //                    var option = document.createElement('<option value="' + response[i].Value + '">');
        //                    //                    countries.options.add(option);
        //                    //                    option.innerText = response[i].Text;
        //                }
        //            }

        //        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblBatchNo" runat="server" Text="Calibration No" Width="100%" />
                                        </div>
                                        <div style="width: 15%" class="ib">
                                            <asp:TextBox ID="txtBatchNo" runat="server" Width="90%"></asp:TextBox>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period" Width="100%" />
                                        </div>
                                        <div style="width: 23%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Width="91%">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" Width="100%" />
                                        </div>
                                        <div style="width: 23%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Width="91%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status" Width="100%" />
                                        </div>
                                        <div style="width: 25%" class="ib">
                                            <asp:DropDownList ID="cboStatus" runat="server" Width="90%">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblSubmitDateFrom" runat="server" Text="Submit From" Width="100%" />
                                        </div>
                                        <div style="width: 14%" class="ib">
                                            <uc2:Date ID="dtDateFrom" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div style="width: 9%" class="ib">
                                            <asp:Label ID="lblSubmitDateTo" runat="server" Text="To" Width="100%" />
                                        </div>
                                        <div style="width: 23%" class="ib">
                                            <uc2:Date ID="dtDateTo" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Button ID="btnShowAll" runat="server" Text="View Report" CssClass="btndefault" />
                                            <asp:Button ID="btnDisplayCurve" runat="server" Text="Display HPO Curve" CssClass="btnDefault" />
                                            <asp:Button ID="btnImportCalibScore" runat="server" Text="Import Calib. Score" CssClass="btnDefault"
                                                Visible="false" />
                                        </div>
                                        <asp:Button ID="btnNew" runat="server" Text="Calibrate" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="dvClistSearch" style="width: 100%">
                                <asp:TextBox ID="txtClistSearch" runat="server" AutoPostBack="false" Width="98%"
                                    Style="margin-left: 5px" onkeyup="ListSeaching();"></asp:TextBox>
                            </div>
                            <div id="scrollable-container" style="width: 100%; height: 250px; overflow: auto">
                                <asp:GridView ID="gvCalibrateList" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,submitdate,calibratescore,calibuser,allocation,ispgrp,pgrpid"
                                    runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                    ToolTip="Edit" OnClick="lnkedit_Click"><i class="fa fa-pencil" style="font-size:15px;color:black"></i>                                                   
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                    ToolTip="Delete" OnClick="lnkdelete_Click"><i class="fa fa-remove" style="font-size:14px;color:Red"></i>                                                    
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgEmployee" />
                                        <asp:BoundField DataField="povisionscore" HeaderText="Provision Score (%)" ItemStyle-HorizontalAlign="Right"
                                            FooterText="dgpovision" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                        <asp:BoundField DataField="pRating" HeaderText="Prov. Rating" ItemStyle-HorizontalAlign="Right"
                                            FooterText="dgpRating" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                        <asp:BoundField DataField="calibratescore" HeaderText="Calibrated Score" ItemStyle-HorizontalAlign="Right"
                                            FooterText="dgcalibratescore" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px"
                                            Visible="false" />
                                        <asp:BoundField DataField="cRating" HeaderText="Calib. Rating" ItemStyle-HorizontalAlign="Right"
                                            FooterText="dgcRating" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                        <asp:BoundField DataField="c_remark" HeaderText="Calibration Remark" FooterText="dgcRemark" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="btnfixedbottom" class="btn-default">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="lblCalibrateHeading" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblFilter" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblCalibrateHeading" runat="server" Text="BSC Calibration"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblFilter" runat="server" Text="Search Criteria" />
                                        </div>
                                        <div style="float: right; display: none">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" Font-Underline="false"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblSPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 63%" class="ib">
                                                <asp:DropDownList ID="cboSPeriod" runat="server" Width="544">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 11%;" class="ib">
                                                <asp:Button ID="btnISearch" runat="server" Text="Search" CssClass="btndefault" />
                                            </div>
                                            <div style="width: 12%;" class="ib">
                                                <asp:Button ID="btnSReset" runat="server" Text="Reset" CssClass="btndefault" />
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No."></asp:Label>
                                            </div>
                                            <div style="width: 18%" class="ib">
                                                <asp:TextBox ID="txtICalibrationNo" runat="server"></asp:TextBox>
                                            </div>
                                            <div style="width: 14%" class="ib">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calibration Rating"></asp:Label>
                                            </div>
                                            <div style="width: 32%" class="ib">
                                                <asp:DropDownList ID="cbocRating" runat="server" Width="282px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 11%" class="ib">
                                                <asp:LinkButton ID="lnkCopyData" runat="server" Font-Underline="false" Font-Bold="true"
                                                    Text="Copy Rating" OnClientClick="RatingOperation(0); return false;"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 15%;" class="ib">
                                                <asp:Label ID="lblIRemark" runat="server" Text="Calibration Remark"></asp:Label>
                                            </div>
                                            <div style="width: 67%; vertical-align: bottom;" class="ib">
                                                <asp:TextBox ID="txtIRemark" runat="server" Width="101%"></asp:TextBox>
                                            </div>
                                            <div style="width: 11%; margin-left: 5px;" class="ib">
                                                <asp:Button ID="btnIApply" runat="server" Text="Apply" CssClass="btndefault" OnClientClick="RatingOperation(1); return false;" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblFilterData" runat="server" Text="Search"></asp:Label>
                                    </div>
                                    <div style="width: 90%;" class="ib">
                                        <asp:TextBox ID="txtListSearch" runat="server" Width="99%" onkeyup="FromSearching();"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="float: left; width: 19%; height: 200px;">
                                        <div>
                                            <div style="width: 100%; height: 170px;">
                                                <asp:Panel ID="pnlRatingF" runat="server" Height="180px" Width="100%" ScrollBars="Auto">
                                                    <asp:GridView ID="gvFilterRating" runat="server" AutoGenerateColumns="False" Width="95%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="scrf,scrt,id">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20" FooterText="dgcolhchkbox">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkFRating" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="name" HeaderText="Filter By Prov. Rating" FooterText="dgcolhFRating" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </div>
                                            <div>
                                                <asp:Button ID="btnFilter" OnClientClick="GetSelected(); return false;" runat="server"
                                                    Text="Filter" CssClass="btndefault" Style="margin-left: 4px; width: 97%" />
                                            </div>
                                        </div>
                                    </div>
                                    <div id="Div4" style="width: 80%; height: 200px; overflow: auto;">
                                        <asp:GridView ID="gvApplyCalibration" DataKeyNames="employeeunkid,periodunkid,calibratnounkid,calibrated_score,tranguid"
                                            runat="server" AutoGenerateColumns="False" Width="130%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="25">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Code" HeaderText="Code" FooterText="dgcolhSCode" />
                                                <asp:BoundField DataField="EmployeeName" HeaderText="Employee" FooterText="dgcolhSEmployee" />
                                                <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhSJob" />
                                                <asp:BoundField DataField="Provision_Score" HeaderText="Provision Score (%)" ItemStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhSProvision" />
                                                <asp:BoundField DataField="provRating" HeaderText="Prov. Rating" FooterText="dgcolhprovRating" />
                                                <asp:BoundField DataField="calibrated_score" HeaderText="Calibrated Score" ItemStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhScalibratescore" Visible="false" />
                                                <asp:BoundField DataField="calibRating" HeaderText="Calib. Rating" FooterText="dgcolhcalibRating" />
                                                <asp:BoundField DataField="calibration_remark" HeaderText="Calibration Remark" FooterText="dgcolhcalibrationremark"
                                                    ItemStyle-Wrap="true" />
                                                <asp:BoundField DataField="tranguid" HeaderText="" FooterText="objdgcolhtranguid" />
                                                <asp:BoundField DataField="MsgId" HeaderText="" FooterText="objMsgId" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnISubmit" runat="server" Text="Submit" CssClass="btndefault" OnClientClick="iSubmitOpr();return false;" />
                                <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <uc3:CV ID="valid" runat="server" ShowYesNo="false" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc9:ConfirmYesNo ID="cnfRemark" runat="server" />
                    <uc2:ConfirmYesNo ID="cnfDelete" runat="server" />
                    <ucDel:DeleteReason ID="delReason" runat="server" />
                    <cc1:ModalPopupExtender ID="popupMyReport" BackgroundCssClass="modalBackground" TargetControlID="lblMyReport"
                        runat="server" PopupControlID="pnlMyReport" CancelControlID="lblMyReport" />
                    <asp:Panel ID="pnlMyReport" runat="server" CssClass="newpopup" Style="display: none;
                        width: 1024px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblMyReport" runat="server" Text="My Report"></asp:Label>
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkRptAdvFilter" runat="server" Text="Advance Filter" Font-Underline="false"
                                        ForeColor="White" Visible="false"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto; height: 440px">
                                <br />
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptPeriod" runat="server" Text="Period"></asp:Label>
                                    </div>
                                    <div style="width: 35%;" class="ib">
                                        <asp:DropDownList ID="cboRptPeriod" runat="server" Width="350px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblRptCalibNo" runat="server" Text="Calibration No"></asp:Label>
                                    </div>
                                    <div style="width: 30%;" class="ib">
                                        <asp:DropDownList ID="cboRptCalibNo" runat="server" Width="286px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptShow" runat="server" Text="Show" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptEmployee" runat="server" Text="Employee"></asp:Label>
                                    </div>
                                    <div style="width: 78%;" class="ib">
                                        <asp:DropDownList ID="cboRptEmployee" runat="server" Width="776px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                    <hr />
                                </div>
                                <div class="row2">
                                    <div id="Div11" style="width: 100%; height: 325px; overflow: auto;">
                                        <asp:GridView ID="gvMyReport" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,allocation,calibuser,total"
                                            runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhMEmployee" />
                                                <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhMprovRating" />
                                                <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhsacrating" />
                                                <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhsacremark" />
                                                <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhMcalibRating" />
                                                <asp:BoundField DataField="apcalibremark" HeaderText="Calib. Remark" FooterText="dgcolhMcalibrationremark"
                                                    ItemStyle-Wrap="true" />
                                                <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhsApprovalRemark" />
                                                <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgcolhMiStatus" />
                                                <asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgcolhMusername" />
                                                <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgcolhMlevelname" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div style="float: left">
                                    <asp:Button ID="btnExportMyReport" runat="server" Text="Export" CssClass="btndefault" />
                                </div>
                                <asp:Button ID="btnCloseMyReport" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="lblHPOCurver"
                        runat="server" PopupControlID="pnlHPOChart" CancelControlID="lblChGenerate" />
                    <asp:Panel ID="pnlHPOChart" runat="server" CssClass="newpopup" Style="display: none;
                        width: 950px; top: 30px;" Height="590px">
                        <div class="panel-primary" style="margin: 0; height: 100%">
                            <div class="panel-heading">
                                <asp:Label ID="lblHPOCurver" runat="server" Text="HPO Chart"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 495px; overflow: auto">
                                <div id="Div8" class="panel-default">
                                    <div id="Div9" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblChGenerate" runat="server" Text="Generate Criteria" />
                                        </div>
                                        <div style="float: right;">
                                            <asp:LinkButton ID="lnkChAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                                Visible="false"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="Div10" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblchPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 28%" class="ib">
                                                <asp:DropDownList ID="cboChPeriod" runat="server" Width="240px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 10%" class="ib">
                                                <asp:Label ID="lblChDisplayType" runat="server" Text="Display Type"></asp:Label>
                                            </div>
                                            <div style="width: 22%" class="ib">
                                                <asp:DropDownList ID="cbochDisplay" runat="server" Width="192px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 25%;" class="ib">
                                                <asp:Button ID="btnChGenerate" runat="server" Text="Generate" CssClass="btndefault" />
                                                <asp:Button ID="btnChReset" runat="server" Text="Reset" CssClass="btndefault" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <asp:Panel ID="Panel4" runat="server" Height="165px" ScrollBars="Auto">
                                        <asp:DataGrid ID="dgvCalibData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            ShowHeader="false">
                                        </asp:DataGrid>
                                    </asp:Panel>
                                </div>
                                <div class="row2">
                                    <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                        BorderlineWidth="2" Width="917px" Style="margin-left: 7px" Height="300px" AntiAliasing="All">
                                        <Titles>
                                            <asp:Title Name="Title1">
                                            </asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                            </asp:Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnChClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportMyReport" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

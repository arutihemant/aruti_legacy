﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wpg_AssignedCompetenciesList.aspx.vb"
    Inherits="wpg_AssignedCompetenciesList" Title="Assigned Competencies List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="EmployeeList" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
     var prm;
    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(endRequestHandler);
    function endRequestHandler(sender, args) {
                SetGeidScrolls();
    }
        function SetGeidScrolls() {
        var arrPnl=$('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "none"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assigned Competencies List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%;">
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="90%">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="lblCompetenceCategory" runat="server" Text="Competence Category"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboCompetenceCategory" runat="server" Width="90%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="90%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblAssessGroup" runat="server" Text="Assess Group"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboAssessGroup" runat="server" Width="90%">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="lblCompetencies" runat="server" Text="Competencies"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboCompetencies" runat="server" Width="90%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnl_lvData" runat="server" ScrollBars="Auto" Height="350px" Width="100%"
                                CssClass="gridscroll">
                                <asp:DataGrid ID="lvData" runat="server" AutoGenerateColumns="false" Width="99%"
                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="imgEdit" runat="server" CssClass="gridedit" CausesValidation="false"
                                                        CommandName="objEdit" ToolTip="Edit"></asp:LinkButton>
                                                </span>
                                                <%-- <asp:ImageButton ID="" runat="server" CommandName="" ImageUrl="~/images/edit.png"
                                                    ImageAlign="Middle" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="imgDelete" runat="server" CssClass="griddelete" CausesValidation="false"
                                                        CommandName="objDelete" ToolTip="Delete"></asp:LinkButton>
                                                </span>
                                                <%--<asp:ImageButton ID="" runat="server" CommandName="" ImageUrl="~/images/remove.png"
                                                    ImageAlign="Middle" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ccategory" HeaderText="Competence Category" Visible="false"
                                            FooterText="objcolhCGroup"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="competencies" HeaderText="Competencies" FooterText="colhCompetencies">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="assigned_weight" HeaderStyle-Width="100px" ItemStyle-Width="100px"
                                            HeaderText="Weight" FooterText="colhWeight" ItemStyle-HorizontalAlign="Right">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="assigncompetenceunkid" HeaderText="" Visible="false" />
                                        <asp:BoundColumn DataField="statusid" HeaderText="" Visible="false" />
                                        <asp:BoundColumn DataField="isfinal" HeaderText="" Visible="false" />
                                        <asp:BoundColumn DataField="opstatusid" HeaderText="" Visible="false" />
                                        <asp:BoundColumn DataField="assigncompetencetranunkid" HeaderText="" Visible="false" />
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            HeaderText="Info." ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                    Font-Underline="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="iscategory" Visible="false" />
                                        <asp:BoundColumn DataField="masterunkid" Visible="false" />
                                        <asp:BoundColumn DataField="competenciesunkid" Visible="false" />
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </div>
                    </div>
                    <uc3:DeleteReason ID="delReason" runat="server" Title="Aruti" />
                    <ajaxToolkit:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                        CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px; z-index: 100002!important;">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div4" class="panel-default">
                                    <div id="Div5" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblTitle" runat="server" Text="Description"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" Width="97%" Height="100px"
                                                        Style="margin-bottom: 5px;" ReadOnly="true" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

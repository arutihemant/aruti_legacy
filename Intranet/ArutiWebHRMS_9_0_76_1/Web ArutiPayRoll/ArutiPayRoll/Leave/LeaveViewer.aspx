﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="LeaveViewer.aspx.vb" Inherits="Leave_Viewer" Title="Leave Viewer Form" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
	<asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
	<asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                        $("#scrollable-container1").scrollLeft($(scroll1.Y).val());
                      
                }
            }
    </script>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Viewer"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Leave Viewer"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 60%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:TextBox ID="TxtSearch" runat="server" AutoPostBack="true" Width="99%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                                height: 350px; overflow: auto">
                                                                <asp:DataGrid ID="dgViewEmployeelist" runat="server" Width="99%" AutoGenerateColumns="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Style="margin: 0px">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="lnkEdit" runat="server" CssClass="gridedit" CommandName="Select"
                                                                                        ToolTip="Select"></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--<asp:ButtonColumn CommandName="Select" Text="Select"></asp:ButtonColumn>--%>
                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="lblEmployee">
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                    <PagerStyle Mode="NumericPages" />
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 40%; vertical-align: top">
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblFromDate" runat="server" Text="FromDate:" valign="top"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <uc2:DateCtrl ID="dtfromdate" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblToDate" runat="server" Text="ToDate:"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <uc2:DateCtrl ID="dttodate" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td colspan="2" style="width: 100%">
                                                            <asp:Panel ID="pnl_dgViewLeaveType" ScrollBars="Auto" runat="server" Width="100%"
                                                                Height="150px">
                                                                <asp:DataGrid ID="dgViewLeaveType" runat="server" GridLines="None" Width="98%" AutoGenerateColumns="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="leavename" HeaderText="Leave Name" ReadOnly="True" FooterText="colhLeavetype">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="color" HeaderText="Leave Color" Visible="False"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass=" btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div style="border: 2px solid #DDD">
                                <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollLeft);" style="width: 100%;
                                    max-height: 350px; overflow: auto">
                                    <asp:DataGrid ID="dgViewLeave" runat="server" SkinIDWidth="200%" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundColumn DataField="CalendarTableID" HeaderText="CalendarTableID"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MonthYear" HeaderText="Month"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="startday" HeaderText="startday"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MonthDays" HeaderText="MonthDays"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MonthId" HeaderText="MonthId"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="YearId" HeaderText="YearId"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_1" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_1" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_1" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_1" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_1" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_1" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_1" HeaderText="Sat"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_2" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_2" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_2" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_2" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_2" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_2" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_2" HeaderText="Sat"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_3" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_3" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_3" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_3" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_3" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_3" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_3" HeaderText="Sat"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_4" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_4" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_4" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_4" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_4" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_4" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_4" HeaderText="Sat"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_5" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_5" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_5" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_5" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_5" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_5" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_5" HeaderText="Sat"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sun_6" HeaderText="Sun"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Mon_6" HeaderText="Mon"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Tue_6" HeaderText="Tue"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Wed_6" HeaderText="Wed"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Thu_6" HeaderText="Thu"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Fri_6" HeaderText="Fri"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Sat_6" HeaderText="Sat"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

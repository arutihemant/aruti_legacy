﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeLeaveBalanceReport.aspx.vb"
    Inherits="Reports_Rpt_EmployeeLeaveBalanceReport" Title="Employee Leave Balance Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 35%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Leave Balance List Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <%--'S.SANDEEP |17-MAR-2020| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : PM ERROR--%>
                                    <div style="float: right;">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" Font-Underline="false"></asp:LinkButton>
                                    </div>
                                    <%--'S.SANDEEP |17-MAR-2020| -- END--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblDate" runat="server" Text="As On Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboReportType" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkIncludeCloseELC" runat="server" Text="Include Close ELC" Width="100%"
                                                    Visible="False" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%; vertical-align:top; padding-top:10px">
                                                <asp:Label ID="LblLeaveType" runat="server" Text="Leave Type" Style="vertical-align: top"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <div style="margin-top: 5px; margin-bottom: 5px; border: 1px solid #DDD; height: auto;
                                                    overflow: auto">
                                                    <asp:CheckBoxList ID="chkLeaveType" runat="server" RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_OmanAppraisalSummaryReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_OmanAppraisalSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Appraisal Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblAssessmentPeriod" runat="server" Text="Assessment Period"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblTnAPeiod" runat="server" Text="TnA Peiod"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboOtherPeriod" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblPeriodDuration" runat="server" Text="Period Duration"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:Label ID="objlblDuration" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkExcludeInactive" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkExcludeZeroRating" runat="server" Text="Exclude Zero Appraisal Rating %" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

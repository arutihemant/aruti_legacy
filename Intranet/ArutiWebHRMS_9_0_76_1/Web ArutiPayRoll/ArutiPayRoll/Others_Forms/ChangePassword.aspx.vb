﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib.clsDataOperation

#End Region

Partial Class ChangePassword
    Inherits Basepage

#Region " Private Variables "
    Dim msg As New CommonCodes
    Dim objDataOperation As New eZeeCommonLib.clsDataOperation(True)
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmChangePassword"
    'Anjan [04 June 2014 ] -- End
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                Dim objOption As New clsPassowdOptions
                'Sohail (11 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End
                    RequiredFieldValidator4.ValidationGroup = "novalidation" 'Old Password
                    RequiredFieldValidator1.ValidationGroup = "novalidation" 'New Password
                    RequiredFieldValidator2.ValidationGroup = "novalidation" 'Confirm Password    
                    txtNewPassword.ValidationGroup = "novalidation"
                    'RegExp1.ValidationGroup = "novalidation"
                    'RegExp1.Enabled = False


                    If objOption._ProhibitPasswordChange = True AndAlso CInt(Me.ViewState("Empunkid")) <> 1 Then
                        btnSave.Enabled = False
                    End If
                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                    RequiredFieldValidator4.ValidationGroup = "password" 'Old Password
                    RequiredFieldValidator1.ValidationGroup = "password" 'New Password
                    RequiredFieldValidator2.ValidationGroup = "password" 'Confirm Password
                    txtNewPassword.ValidationGroup = "password"
                    'RegExp1.ValidationGroup = "password"
                    'PNReqE.TargetControlID = "RegExp1"
                    'RegExp1.Enabled = True

                    If objOption._ProhibitPasswordChange = True Then
                        btnSave.Enabled = False
                    End If
                End If
                'Sohail (11 May 2012) -- End


            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsDataValid() = False Then Exit Sub
            Dim objData As New clsDataOperation(True)
            'Sohail (11 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objEmp As New clsEmployee_Master
            'objEmp._Employeeunkid = Me.ViewState("Empunkid")
            'objEmp._Password = txtConfirmPassword.Text
            'Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
            'objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)
            'objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = Me.ViewState("Empunkid")
                objUser._Password = txtConfirmPassword.Text
                Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
                objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)
                objData.ExecNonQuery("UPDATE hrmsconfiguration..cfuser_master SET password = @Passwd WHERE userunkid = " & objUser._Userunkid)
                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'objUser = Nothing
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = '" & objUser._EmployeeUnkid & "'")
                End If
                objUser = Nothing
                'S.SANDEEP [ 14 DEC 2012 ] -- END
            Else
                Dim objEmp As New clsEmployee_Master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = Me.ViewState("Empunkid")
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = Me.ViewState("Empunkid")
                'Shani(20-Nov-2015) -- End

                objEmp._Password = txtConfirmPassword.Text
                Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
                objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid)
                objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))))
                'Shani(20-Nov-2015) -- End

                objEmp = Nothing

                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    objData.ExecNonQuery("UPDATE hrmsconfiguration..cfuser_master SET password = @Passwd WHERE employeeunkid = '" & Me.ViewState("Empunkid") & "' AND companyunkid = '" & Session("CompanyUnkId") & "'")
                End If
                'S.SANDEEP [ 14 DEC 2012 ] -- END




            End If
            'Sohail (11 May 2012) -- End

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                SetUserTracingInfo(False, enUserMode.Password, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), True, Session("mdbname"))
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                SetUserTracingInfo(False, enUserMode.Password, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), True, Session("mdbname"))
            End If
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            Dim objPswd As New clsPassowdOptions
            Dim objPasswordHistory As New clsPasswordHistory
            If objPswd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPswd._IsPasswordHistory AndAlso _
               (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
            If Session("IsEmployeeAsUser") = True OrElse Session("LoginBy") = Global.User.en_loginby.Employee Then
                Dim intEmployeeId As Integer = CInt(Session("Employeeunkid"))
                If Session("LoginBy") = Global.User.en_loginby.User Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = Me.ViewState("Empunkid")
                    intEmployeeId = objUser._EmployeeUnkid
                End If
                With objPasswordHistory
                    ._Tranguid = Guid.NewGuid().ToString
                    ._Userunkid = -1
                    ._Employeeunkid = intEmployeeId
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = txtConfirmPassword.Text
                    ._OldPassword = txtOldPassword.Text
                    ._Companyunkid = CInt(Session("CompanyUnkId"))
                    ._ChangedUserunkid = Session("UserId")
                    ._Ip = CStr(Session("IP_ADD"))
                    ._Hostname = CStr(Session("HOST_NAME"))
                    ._Isweb = True
                        If .Insert() = False Then
                            msg.DisplayMessage(._Message, Me)
                            Exit Sub
                        End If
                End With
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                With objPasswordHistory
                    ._Tranguid = Guid.NewGuid().ToString
                    ._Userunkid = Session("UserId")
                    ._Employeeunkid = -1
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = txtConfirmPassword.Text
                    ._OldPassword = txtOldPassword.Text
                    ._Companyunkid = CInt(Session("CompanyUnkId"))
                    ._ChangedUserunkid = Session("UserId")
                    ._Ip = CStr(Session("IP_ADD"))
                    ._Hostname = CStr(Session("HOST_NAME"))
                    ._Isweb = True
                        If .Insert() = False Then
                            msg.DisplayMessage(._Message, Me)
                            Exit Sub
                        End If
                End With
            End If
            End If
            objPasswordHistory = Nothing
            objPswd = Nothing
            'Hemant (25 May 2021) -- End

            'objEmp.Update()
            If objData.ErrorMessage = "" Then
                msg.DisplayMessage("Password Changed Successfully.", Me, "../Index.aspx")
            Else
                msg.DisplayMessage(objData.ErrorMessage, Me)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Controls "
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebuton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region " Private Functions & Methods "

    Private Function IsDataValid() As Boolean
        Try

            'Sohail (11 May 2012) -- Start
            'TRA - ENHANCEMENT
            'If txtOldPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("Old Password is compulsory information. Please enter Old Password to continue.", Me)
            '    txtOldPassword.Focus()
            '    Return False
            'End If

            'If txtNewPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("New Password is compulsory information. Please enter New Password to continue.", Me)
            '    txtNewPassword.Focus()
            '    Return False
            'End If

            'If txtConfirmPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("Confirmation Password is compulsory information. Please enter Confirmation Password to continue.", Me)
            '    txtConfirmPassword.Focus()
            '    Return False
            'End If

            'If txtNewPassword.Text <> txtConfirmPassword.Text Then
            '    msg.DisplayMessage("Password does not match. Please enter correct password", Me)
            '    txtConfirmPassword.Focus()
            '    Return False
            'End If

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = ViewState("Empunkid")
                If txtOldPassword.Text <> objUser._Password Then
                    msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                    'txtOldPassword.Focus()
                    Return False
                End If

                'S.SANDEEP [ 06 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim objPswd As New clsPassowdOptions
                'If objPswd._IsPasswordLenghtSet Then
                '    If txtNewPassword.Text.Trim.Length < objPswd._PasswordLength Then
                '        msg.DisplayMessage("Password length cannot be less than " & objPswd._PasswordLength & " character(s)", Me)
                '        'txtNewPassword.Focus()
                '        Return False
                '    End If
                'End If
                'objPswd = Nothing
                'S.SANDEEP [ 06 NOV 2012 ] -- END
            Else
                Dim objEmployee As New clsEmployee_Master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = Session("Employeeunkid")
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = Session("Employeeunkid")
                'Shani(20-Nov-2015) -- End

                If txtOldPassword.Text <> objEmployee._Password Then
                    msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                    'txtOldPassword.Focus()
                    Return False
                End If
            End If
            'Sohail (11 May 2012) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End


            'S.SANDEEP [ 06 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPswd As New clsPassowdOptions
            If objPswd._IsPasswordLenghtSet Then
                If txtNewPassword.Text.Trim.Length < objPswd._PasswordLength Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Password length cannot be less than ") & objPswd._PasswordLength & Language.getMessage(mstrModuleName, 8, " character(s)"), Me)
                    'txtNewPassword.Focus()
                    Return False
                End If
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            If txtNewPassword.Text.Trim.Length > 0 Then
                If objPswd._IsPasswdPolicySet Then
                    If objPswd._IsUpperCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Password must contain atleast one upper case character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsLowerCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Password must contain atleast one lower case character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsNumeric_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Password must contain atleast one numeric digit."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsSpecalChars_Mandatory Then
                        'S.SANDEEP [ 07 MAY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                        'S.SANDEEP [ 07 MAY 2013 ] -- END
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Password must contain atleast one special character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                End If
            End If
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If objPswd._IsPasswordHistory AndAlso (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
                Dim objPasswordHistory As New clsPasswordHistory
                Dim intTopCount As Integer
                Dim strFilter As String = String.Empty
                Dim strOrderBy As String = String.Empty
                If objPswd._PasswdLastUsedNumber > 0 Then
                    intTopCount = objPswd._PasswdLastUsedNumber
                End If

                If objPswd._PasswdLastUsedDays > 0 Then
                    Dim dtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                    Dim dtStartDate As Date = dtEndDate.AddDays(-1 * objPswd._PasswdLastUsedDays)
                    strFilter &= "AND Convert(char(8),transactiondate,112) >= '" & eZeeDate.convertDate(dtStartDate) & "' AND Convert(char(8),transactiondate,112) <= '" & eZeeDate.convertDate(dtEndDate) & "' "
                End If

                strOrderBy = " order by transactiondate desc "
                Dim dsList As DataSet = Nothing

                If Session("IsEmployeeAsUser") = True OrElse Session("LoginBy") = Global.User.en_loginby.Employee Then
                    Dim intEmployeeId As Integer = CInt(Session("Employeeunkid"))
                    If Session("LoginBy") = Global.User.en_loginby.User Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = Me.ViewState("Empunkid")
                        intEmployeeId = objUser._EmployeeUnkid
                    End If
                    dsList = objPasswordHistory.GetFilterDataList("List", -1, intEmployeeId, CInt(Session("CompanyUnkId")), intTopCount, strFilter, strOrderBy)
                ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                    dsList = objPasswordHistory.GetFilterDataList("List", CInt(Session("UserId")), -1, -1, intTopCount, strFilter, strOrderBy)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim drPassword() As DataRow = dsList.Tables(0).Select("new_password = '" & clsSecurity.Encrypt(txtNewPassword.Text, "ezee").ToString & "' ")
                    If drPassword.Length > 0 Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, Entered New Password is already Used. Please Enter another Password!!!"), Me)
                        txtNewPassword.Focus()
                        Exit Function
                    End If
                End If
                objPasswordHistory = Nothing
            End If
            'Hemant (25 May 2021) -- End
            objPswd = Nothing
            'S.SANDEEP [ 06 NOV 2012 ] -- END

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblTitle.Text = Language._Object.getCaption(mstrModuleName, Me.Title) 'SHANI [01 FEB 2015]-START 'Enhancement - REDESIGN SELF SERVICE.

            Me.lblOldPwd.Text = Language._Object.getCaption(Me.lblOldPwd.ID, Me.lblOldPwd.Text)
            Me.lblConfirmPwd.Text = Language._Object.getCaption(Me.lblConfirmPwd.ID, Me.lblConfirmPwd.Text)
            Me.lblNewPwd.Text = Language._Object.getCaption(Me.lblNewPwd.ID, Me.lblNewPwd.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
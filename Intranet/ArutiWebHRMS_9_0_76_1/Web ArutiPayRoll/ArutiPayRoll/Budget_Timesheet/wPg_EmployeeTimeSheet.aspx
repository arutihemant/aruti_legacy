<%@ Page Title="Employee Timesheet" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheet.aspx.vb" Inherits="Budget_Timesheet_wPg_EmployeeTimeSheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)

                if (charCode == 1)
                return false;

            if (charCode > 31 && (charCode <= 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                var hdf = '#<%= hdf_SetScroll.ClientID %>'
                if ($(hdf).val() == "1") {
                    $(hdf).val("0");
                } else {
                    $(window).scrollTop(nbodyY);
                }
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
        function SethiddenVal() {
            var hdf = '#<%= hdf_SetScroll.ClientID %>'
            $(hdf).val("1");
            return true
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Timesheet"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 5%">
                                                <asp:Label ID="LblPeriod" runat="server" Style="margin-left: 7px" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 6%; text-align: right">
                                                <asp:Label ID="LblDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%" align="right">
                                                <uc3:DateCtrl ID="dtpDate" runat="server" />
                                            </td>
                                           <td style="width: 6%; text-align: right">
                                                <asp:Label ID="LblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                             <td style="width: 15%" align="right">
                                                <uc3:DateCtrl ID="dtpToDate" runat="server" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 41%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="Div2" class="panel-default">
                                        <div id="Div4" class="panel-body-default">
                                            <div id="Div5" style="max-height: 400px; overflow: auto; vertical-align: top">
                                                <asp:DataGrid ID="dgvTimesheet" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <%--    Pinkal (28-Mar-2018) -- Start
                                                       Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details. --%>
                                                        <asp:TemplateColumn>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                    CommandName="Show Details" ToolTip="Show Project Hour Details" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <%--    Pinkal (28-Mar-2018) -- End--%>
                                                        <asp:BoundColumn DataField="Donor" HeaderText="Donor/Grant" ReadOnly="true" FooterText="dgcolhEmpDonor">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Project" HeaderText="Project" ReadOnly="true" FooterText="dgcolhEmpProject">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Activity" HeaderText="Activity" ReadOnly="true" FooterText="dgcolhEmpActivity">
                                                        </asp:BoundColumn>
                                                        <%--    Pinkal (28-Mar-2018) -- Start
                                                            Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..--%>
                                                        <asp:BoundColumn DataField="percentage" HeaderText="Assigned Activity(%)" ReadOnly="true"
                                                            FooterText="dgcolhEmpPercentage" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-Width="150px" ItemStyle-Width="150px" />
                                                        <%--    Pinkal (28-Mar-2018) -- End--%>
                                                        <asp:TemplateColumn FooterText="dgcolhEmpHours" HeaderText="Hours : Mins">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:TextBox ID="TxtHours" runat="server" AutoPostBack="true" Width="30px" onKeypress="return onlyNumbers(this, event);"
                                                                        MaxLength="2" Style="text-align: right;" OnTextChanged="TxtHours_TextChanged1" />
                                                                    <asp:Label ID="LblColon" runat="server" Text=":" Style="margin-left: 2px" />
                                                                    <asp:TextBox ID="TxtMinutes" runat="server" AutoPostBack="true" Width="30px" onKeypress="return onlyNumbers(this, event);"
                                                                        MaxLength="2" Style="text-align: right" OnTextChanged="TxtMinutes_TextChanged" />
                                                                    <%--<asp:Label ID="LblHours" runat="server" Text='<%# Eval("ActivityHours") %>' Visible="false"
                                                                        Style="display: none" />--%>
                                                                </span>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="105px" />
                                                            <ItemStyle Width="105px" HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn FooterText="dgcolhEmpDescription" HeaderText="Description" HeaderStyle-Width="275px"
                                                            ItemStyle-Width="200px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:TextBox ID="TxtDescription" runat="server" Text='<%# Eval("description") %>'
                                                                        AutoPostBack="true" Width="98%" OnTextChanged="TxtDescription_TextChanged"></asp:TextBox>
                                                                </span>
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="275px" />
                                                            <ItemStyle Width="200px" HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="ActivityHoursInMin" HeaderText="HourInSec" ReadOnly="true"
                                                            Visible="false" FooterText="objdgcolhEmpHourInMin" />
                                                        <asp:BoundColumn DataField="AssignedActivityHrsInMin" HeaderText="AssignedActivityHrsInMin"
                                                            ReadOnly="true" Visible="false" FooterText="objdgcolhEmpAssignedActivityHrsInMin" />
                                                        <asp:BoundColumn DataField="periodunkid" HeaderText="Periodunkid" ReadOnly="true"
                                                            Visible="false" FooterText="objdgcolhEmpPeriodunkid" />
                                                        <asp:BoundColumn DataField="fundsourceunkid" HeaderText="DonorID" ReadOnly="true"
                                                            Visible="false" FooterText="objdgcolhEmpDonorID" />
                                                        <asp:BoundColumn DataField="fundprojectcodeunkid" HeaderText="ProjectID" ReadOnly="true"
                                                            Visible="false" FooterText="objdgcolhEmpProjectunkid" />
                                                        <asp:BoundColumn DataField="fundactivityunkid" HeaderText="ActivityID" ReadOnly="true"
                                                            Visible="false" FooterText="objdgcolhEmpActivityunkid" />
                                                        <%--    Pinkal (28-Mar-2018) -- Start
                                                            Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..--%>
                                                        <%--  <asp:BoundColumn DataField="percentage" HeaderText="Percentage" ReadOnly="true" Visible="false"
                                                            FooterText="objdgcolhEmpPercentage" />--%>
                                                        <%--    Pinkal (28-Mar-2018) -- End--%>
                                                        <asp:BoundColumn DataField="budgetunkid" HeaderText="BudgetID" ReadOnly="true" Visible="false"
                                                            FooterText="objdgcolhEmpBudgetID" />
                                                        <asp:BoundColumn DataField="ActivityHours" ReadOnly="true" Visible="false" FooterText="dgcolhActivityHours">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divValidation" style="text-align: left; padding: 5px; margin-left: 115px">
                                        <asp:ValidationSummary ID="ValidationSummary" HeaderText="Fields Required!" DisplayMode="BulletList"
                                            runat="server" ValidationGroup="MKE" />
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <%--<asp:Button ID="btnSubmitForAproval" runat="server" ValidationGroup="MKE" Text="Submit For Approval"
                                                CssClass="btnDefault" />--%>
                                            <uc5:OperationButton ID="btnOperation" runat="server" Text="Operation" TragateControlId="data" />
                                            <div id="data" style="display: none;">
                                                <asp:LinkButton ID="lnkViewPendingSubmitApproval" runat="server" Text="View Pending Submit For Approval"
                                                    Width="250px"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkViewCompletedSubmitApproval" runat="server" Text="View Completed Submit For Approval"
                                                    Width="250px"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkGlobalCancelTimesheet" runat="server" Text="Gloabl Cancel Timesheet"
                                                    Width="250px"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkGlobalDeleteTimesheet" runat="server" Text="Gloabl Delete Timesheet"
                                                    Width="250px"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <asp:Button ID="btnSave" runat="server" ValidationGroup="MKE" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnEdit" runat="server" ValidationGroup="MKE" Text="Edit" Visible="false"
                                            CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <input type="hidden" id="hdf_SetScroll" value="0" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="max-height: 400px;
                                        overflow: auto; vertical-align: top">
                                        <asp:DataGrid ID="dgvEmpTimesheet" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="125%">
                                            <ItemStyle CssClass="griviewitem" />
                                            <Columns>
                                                <%--<asp:TemplateColumn FooterText="dgcolhSelect" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="30px" Visible="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn FooterText="btnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                ToolTip="Edit" OnClientClick="return SethiddenVal()"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                Style="text-align: center;" ToolTip="Delete"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn FooterText="btnCancel" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgCancel" runat="server" CommandName="Cancel" CssClass="gridicon"
                                                                ToolTip="Cancel"><i class="fa fa-ban" 
                                                            aria-hidden="true" style="color:Red; font-size: 18px"></i></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="Particular" FooterText="dgcolhEmployee" HeaderText="Employee"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activitydate" FooterText="dgcolhDate" HeaderText="Activity Date"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" HeaderText="Donor/Grant"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" HeaderText="Project Code"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" HeaderText="Activity Code"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" HeaderStyle-Width="100px"
                                                    HeaderText="Activity Hours" ItemStyle-Width="100px" ReadOnly="true">
                                                    <HeaderStyle Width="100px" />
                                                    <ItemStyle Width="100px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="approvedactivity_hrs" FooterText="dgcolhApprovedActHrs"
                                                    HeaderStyle-Width="175px" HeaderText="Approved Activity Hrs." ItemStyle-Width="175px"
                                                    ReadOnly="true">
                                                    <HeaderStyle Width="175px" />
                                                    <ItemStyle Width="175px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="description" FooterText="dgcolhDescription" HeaderText="Description"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" HeaderText="Status"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="emptimesheetunkid" FooterText="objdgcolhEmpTimesheetID"
                                                    HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundprojectcodeunkid" FooterText="objdgcolhProjectID"
                                                    HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundactivityunkid" FooterText="objdgcolhActivityID" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusId" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" HeaderText="" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isholiday" FooterText="objdgcolhIsHoliday" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isDayOff" FooterText="objdgcolhIsDayOFF" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isLeave" FooterText="objdgcolhIsLeave" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="issubmit_approval" FooterText="objdgcolhIsSubmitForApproval"
                                                    HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsChecked" FooterText="objdgcolhSelect" HeaderText=""
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ApprovedActivityHoursInMin" FooterText="objdgcolhApprovedActivityHoursInMin"
                                                    HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    
                                                  <%--  'Pinkal (28-Jul-2018) -- Start
                                                   'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                                <asp:BoundColumn DataField="ActivityHoursInMin" FooterText="objdgcolhActivityHoursInMin"
                                                    HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                 <%--'Pinkal (28-Jul-2018) -- End--%>
                                                    
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" DropShadow="true"
                                    CancelControlID="hdf_Details">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlShowDetails" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 550px">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                                        </div>
                                        <div id="Div6" class="panel-body-default">
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" Width="20%" />
                                                <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" Width="20%" />
                                                <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblProject" runat="server" Text="Project" Width="20%" />
                                                <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" Width="20%" />
                                                <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblActivity" runat="server" Text="Activity" Width="20%" />
                                                <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" Width="79%" />
                                            </div>
                                        </div>
                                        
                                        <div id="Div8" class="panel-body-default">
                                            <div id="Div9" style="max-height: 400px; overflow: auto; vertical-align: top">
                                                <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" a HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                            FooterText="dgcolhParticulars"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                            <div class="btn-default">
                                                <asp:Button ID="btnShowDetailsClose" runat="server" CssClass=" btnDefault" Text="Close" />
                                                <asp:HiddenField ID="hdf_Details" runat="server" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                    <uc2:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Add_Edit_Employee_Saving.aspx.vb"
    Inherits="Loan_Savings_wPg_Add_Edit_Employee_Saving" Title="Add/Edit Employee Savings" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function onlyNumbers(txtBox, evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            //var cval = document.getElementById(txtBoxId).value;
            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Employee Savings"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <%--<div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>--%>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%">
                                                            <div class="panel-body">
                                                                <div id="Div1" class="panel-default" style="height: 317px">
                                                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                                        <div style="float: left;">
                                                                            <asp:Label ID="lblCaption" runat="server" Text="Employee Saving Information"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <div id="Div2" class="panel-body-default">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No."></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <asp:TextBox ID="txtVoucherNo" runat="server"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblDate" runat="server" Text="Date" Enabled="false"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <uc2:DateCtrl ID="dtpDate" runat="server" Enabled="false" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblStopDate" runat="server" Text="Stop Date"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <uc2:DateCtrl ID="dtpStopDate" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <asp:DropDownList ID="cboEmpName" runat="server">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%;">
                                                                                    <asp:Label ID="lblSavingScheme" runat="server" Text="Saving Scheme"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%;">
                                                                                    <asp:DropDownList ID="cboSavingScheme" runat="server" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%" align="left">
                                                                                    <asp:Label ID="lblCurrency" runat="server" Text="Contribution Currency"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%" align="left">
                                                                                    <div style="float: left; width: 20%">
                                                                                        <asp:DropDownList ID="cboCurrency" runat="server" Height="23px" Width="100%" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                    <div style="vertical-align: middle; float: left; padding-top: 3px; padding-left: 10px">
                                                                                        <asp:Label ID="objlblExRate" runat="server" Text="1"></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 30%" align="left">
                                                                                    <asp:Label ID="lblOpeningContribution" runat="server" Text="Opening Total Contrib."></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%" align="left">
                                                                                    <div style="float: left; width: 20%">
                                                                                        <asp:TextBox ID="txtOpeningContribution" runat="server" Text="0" onKeypress="return onlyNumbers(this);"></asp:TextBox>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%; display: none">
                                                                                <td style="width: 30%" align="left">
                                                                                    <asp:Label ID="lblOpeningBalance" runat="server" Text="Opening Balance."></asp:Label>
                                                                                </td>
                                                                                <td style="width: 70%" align="left">
                                                                                    <div style="float: left; width: 20%">
                                                                                        <asp:TextBox ID="txtOpeningBalance" runat="server" Text="0" onKeypress="return onlyNumbers(this);"></asp:TextBox>
                                                                                    </div>
                                                                                    <div style="float: left; width: 70%">
                                                                                        <asp:Label ID="lblWithOpeningInterest" runat="server" Text="(With Opening Interest)"></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%; vertical-align: top">
                                                <div class="panel-body">
                                                    <div id="Div6" class="panel-default">
                                                        <div id="Div7" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblContributionCaption" runat="server" Text="Employee Contribution Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div8" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%;">
                                                                        <asp:Label ID="lblContPeriod" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%;">
                                                                        <asp:DropDownList ID="cboContPeriod" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%;">
                                                                        <asp:Label ID="lblContribution" runat="server" Text="Contribution"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%;">
                                                                        <asp:TextBox ID="txtMonthlyContribution" runat="server" Style="text-align: right"
                                                                            Text="0" onKeypress="return onlyNumbers(this);"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnContributionAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                                <asp:Button ID="btnContributionEdit" runat="server" Text="Edit" CssClass="btndefault"
                                                                    Visible="false" />
                                                            </div>
                                                        </div>
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnl_lvContributionData" runat="server" Width="100%" Height="150px"
                                                                        ScrollBars="Auto">
                                                                        <asp:DataGrid ID="lvContributionData" runat="server" CssClass="gridview" AutoGenerateColumns="false"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                                                    HeaderStyle-HorizontalAlign="Center" FooterText="colhContEdit">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="imgContEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                                                                                CssClass="gridedit"></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                                                    HeaderStyle-HorizontalAlign="Center" FooterText="colhContDelete">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="ImgContDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                                                CssClass="griddelete"></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="period_name" HeaderStyle-Width="40%" ItemStyle-Width="40%"
                                                                                    HeaderText="Period" ReadOnly="true" FooterText="colhContPeriod" />
                                                                                <asp:BoundColumn DataField="contribution" HeaderStyle-Width="40%" ItemStyle-Width="40%"
                                                                                    HeaderText="Contribution" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                                                    ReadOnly="true" FooterText="colhContribution" />
                                                                                <asp:BoundColumn DataField="savingcontributiontranunkid" Visible="false" FooterText="objcolhContMasterId" />
                                                                                <asp:BoundColumn DataField="GUID" Visible="false" FooterText="objcolhGuid" />
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 100%" colspan="2">
                                                <div class="panel-body">
                                                    <div id="Div3" class="panel-default">
                                                        <div id="Div4" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblRateCaption" runat="server" Text="Employee Interest Rate Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div style="width: 100%">
                                                            <div id="Div5" class="panel-body-default" style="width: 49%; float: left">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="lblRatePeriod" runat="server" Text="Period"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 75%;">
                                                                            <asp:DropDownList ID="cboRatePeriod" runat="server" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="lblRateDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 75%;">
                                                                            <uc2:DateCtrl ID="dtpRateDate" runat="server" Enabled="false" AutoPostBack="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 25%;">
                                                                            <asp:Label ID="lblRate" runat="server" Text="Interest Rate"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 75%;">
                                                                            <asp:TextBox ID="txtRate" runat="server" Text="0" onKeypress="return onlyNumbers(this);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div class="btn-default">
                                                                    <asp:Button ID="btnRateAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                                    <asp:Button ID="btnRateEdit" runat="server" Text="Edit" CssClass="btndefault" Visible="false" />
                                                                </div>
                                                            </div>
                                                            <div style="width: 49%; float: left">
                                                                <table style="width: 100%; margin-top: 10px">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 100%">
                                                                            <asp:Panel ID="pnl_lvInterestRateData" runat="server" Height="180px" ScrollBars="Auto">
                                                                                <asp:DataGrid ID="lvInterestRateData" runat="server" AutoGenerateColumns="false"
                                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                                                            HeaderStyle-HorizontalAlign="Center" FooterText="colhRateEdit">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="imgRateEdit" runat="server" ToolTip="Edit" CommandName="Edit"
                                                                                                        CssClass="gridedit"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                                                            HeaderStyle-HorizontalAlign="Center" FooterText="colhRateDelete">
                                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                                            <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                                    <asp:LinkButton ID="ImgRateDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                                                        CssClass="griddelete"></asp:LinkButton>
                                                                                                </span>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="period_name" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                                                                            HeaderText="Period" ReadOnly="true" FooterText="colhRatePeriod" />
                                                                                        <asp:BoundColumn DataField="effectivedate" HeaderStyle-Width="25%" ItemStyle-Width="25%"
                                                                                            HeaderText="Effective Date" ReadOnly="true" FooterText="colhRateEffectiveDate" />
                                                                                        <asp:BoundColumn DataField="interest_rate" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                                                                                            HeaderText="Rate %" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                                                            ReadOnly="true" FooterText="colhInterestRate" />
                                                                                        <asp:BoundColumn DataField="savinginterestratetranunkid" Visible="false" FooterText="objcolhRateMasterId" />
                                                                                        <asp:BoundColumn DataField="GUID" Visible="false" FooterText="objcolhRateGuid" />
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc8:DeleteReason ID="popup_Contribution" runat="server" Title="Are you Sure Delete this Record"
                        ValidationGroup="Contribution" />
                    <uc8:DeleteReason ID="popup_InterestRate" runat="server" Title="Are you Sure Delete this Record"
                        ValidationGroup="InterestRate" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_GlobalAssignLoanAdvance.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_GlobalAssignLoanAdvance"
    MaintainScrollPositionOnPostback="true" Title="Global Assigned Loan / Advance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
        
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
            RateOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);
        prm.add_endRequest(RateOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }

        function RateOnly() {

            $(".percentage").keyup(function(e) {
                if (($(this).val() < 0 || $(this).val() > 100)) {
                    $(this).val(0);
                    return false;
                }
            });
            $(".percentage").keypress(function(e) {
                if (($(this).val().indexOf('.') != -1) && $(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2) {
                    return false;
                }
                if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
                $("#scrollable-container").scrollTop($(scroll1.Y).val());
            }
            $("a[title='Collapse']").children().children().attr('class', 'fa fa-chevron-circle-down');
            $("a[title='Expand']").children().children().attr('class', 'fa fa-chevron-circle-up');
        }
    </script>

    <%--<script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Assigned Loan/Advance"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%; margin-top: -10px; margin-bottom: -10px">
                                        <tr style="width: 100%">
                                            <td style="width: 47%" valign="top">
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default" style="margin-left: -2px; margin-right: -2px;
                                                        padding-bottom: 7px">
                                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:LinkButton ID="lnkAllocation" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                                    Font-Underline="false" runat="server" Text="Allocation"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div id="Div2" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblMode" runat="server" Text="Mode"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="cboMode" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblCurrency" runat="server" Style="margin-left: 15px" Text="Currency"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboCurrency" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 85%" colspan="3">
                                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblLoan" runat="server" Text="Loan"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 85%" colspan="3">
                                                                        <asp:DropDownList ID="cboLoan" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 85%" colspan="3">
                                                                        <asp:DropDownList ID="cboApprover" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 53%" valign="top">
                                                <div class="panel-body">
                                                    <div id="Div3" class="panel-default" style="margin-right: -2px">
                                                        <div id="Div4" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblBasicInfo" runat="server" Text="Basic Info"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div5" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="lblPeriod" runat="server" Text="Assigned Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboPayPeriod" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblVoucherNo" runat="server" Style="margin-left: 5px" Text="Voc #"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 16%">
                                                                        <asp:TextBox ID="txtPrefix" runat="server" Text="ISGLA_" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 13%">
                                                                        <asp:TextBox ID="txtManualNo" runat="server" Text="1" Style="text-align: right"></asp:TextBox>
                                                                    </td>
                                                                    <%-- <td style="width: 4%">
                                                                        <cc1:NumericUpDownExtender ID="nudManualNo" TargetControlID="txtManualNo" runat="server"
                                                                            Width="90" Minimum="0" Maximum="107">
                                                                        </cc1:NumericUpDownExtender>
                                                                    </td>--%>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="lblGLoanCalcType" runat="server" Text="Loan Calculation"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboGLoanCalcType" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblPurpose" runat="server" Style="margin-left: 5px" Text="Purpose"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 29%" colspan="2" rowspan="3" valign="top">
                                                                        <asp:TextBox ID="txtPurpose" TextMode="MultiLine" runat="server" Rows="4"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="lblInterestCalcType" runat="server" Text="Interest Calculation"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboGInterestCalcType" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 43%" colspan="3" valign="top">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="lblMappedHead" runat="server" Text="Mapped Head"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboMappedHead" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 43%" colspan="3" valign="top">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 22%">
                                                                        <asp:Label ID="lblGRate" runat="server" Text="Rate %"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 78%" colspan="4">
                                                                        <table style="width: 100%; margin: -3px">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 14%">
                                                                                    <asp:TextBox ID="txtGRate" runat="server" CssClass="percentage" Text="0" Style="text-align: right"></asp:TextBox>
                                                                                </td>
                                                                                <td style="width: 64%">
                                                                                    <asp:RadioButton ID="radApplyToChecked" runat="server" Style="margin-left: 10px"
                                                                                        Text="Apply To Checked" GroupName="APPLY" />
                                                                                    <asp:RadioButton ID="radApplyToAll" runat="server" Style="margin-left: 10px" Text="Apply To All"
                                                                                        GroupName="APPLY" />
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btndefault" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div6" class="panel-default" style="position: relative">
                                <div id="Div8" class="panel-body-default">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollLeft);$(scroll1.Y).val(this.scrollTop);"
                                        style="margin-bottom: 10px; overflow: auto; z-index: 10">
                                        <asp:DataGrid ID="dgvDataList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            HeaderStyle-Font-Bold="false" Width="175%">
                                            <ItemStyle CssClass="griviewitem" />
                                            <Columns>
                                                <%--0--%>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" FooterText="objdgcolhCollapse">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgCollapse" runat="server" ToolTip="Collapse" CommandName="ExpCol">
                                                            <div style="font-size: 20px; color:#004474; text-align:center"><i id="expcol" class="fa fa-chevron-circle-up"></i></div>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="30px" />
                                                    <ItemStyle Width="30px" />
                                                </asp:TemplateColumn>
                                                <%--1--%>
                                                <asp:BoundColumn DataField="IsCheck" HeaderText="IsCheck" Visible="false"></asp:BoundColumn>
                                                <%--2--%>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" FooterText="objdgcolhSelect">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <%--3--%>
                                                <asp:BoundColumn DataField="AppNo" HeaderText="Application No." FooterText="dgcolhAppNo"
                                                    HeaderStyle-Width="125px" ItemStyle-Width="125px">
                                                    <HeaderStyle Width="125px" />
                                                    <ItemStyle Width="125px" />
                                                </asp:BoundColumn>
                                                <%--4--%>
                                                <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee"
                                                    HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Width="20%" />
                                                </asp:BoundColumn>
                                                <%--5--%>
                                                <asp:BoundColumn DataField="Approver" HeaderText="Approver" FooterText="dgcolhApprover" />
                                                <%--6--%>
                                                <asp:BoundColumn DataField="Amount" HeaderText="Amount" ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAmount" HeaderStyle-Width="150px"
                                                    ItemStyle-Width="150px">
                                                    <HeaderStyle HorizontalAlign="Right" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="150px" />
                                                </asp:BoundColumn>
                                                <%--7--%>
                                                <asp:TemplateColumn HeaderText="Deduction Period" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="dgcolhDeductionPeriod">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbodgcolhDeductionPeriod" runat="server" AutoPostBack="true"
                                                            OnSelectedIndexChanged="cbodgcolhDeductionPeriod_SelectedIndexChanged" CssClass="combo">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="150px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                </asp:TemplateColumn>
                                                <%--8--%>
                                                <asp:BoundColumn DataField="dstartdate" HeaderText="Effective Date" FooterText="dgcolhEffDate"
                                                    HeaderStyle-Width="115px" ItemStyle-Width="115px">
                                                    <HeaderStyle Width="115px" />
                                                    <ItemStyle Width="115px" />
                                                </asp:BoundColumn>
                                                <%--9--%>
                                                <asp:TemplateColumn HeaderText="Loan Calculation" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="dgcolhLoanType">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbodgcolhLoanType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbodgcolhLoanType_SelectedIndexChanged"
                                                            CssClass="combo">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="150px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                </asp:TemplateColumn>
                                                <%--10--%>
                                                <asp:TemplateColumn HeaderText="Interest Calculation" HeaderStyle-Width="125px" ItemStyle-Width="125px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="dgcolhInterestType">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbodgcolhInterestCalcType" runat="server" AutoPostBack="true"
                                                            CssClass="combo" OnSelectedIndexChanged="cbodgcolhInterestCalcType_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="125px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="125px" />
                                                </asp:TemplateColumn>
                                                <%--11--%>
                                                <asp:TemplateColumn HeaderText="Mapped Head" HeaderStyle-Width="125px" ItemStyle-Width="125px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="dgcolhMappedHead">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbodgcolhMappedHead" runat="server" AutoPostBack="true"
                                                            CssClass="combo" OnSelectedIndexChanged="cbodgcolhMappedHead_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="125px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="125px" />
                                                </asp:TemplateColumn>
                                                 <%--12--%>
                                                <asp:BoundColumn DataField="rate" HeaderText="Rate" Visible="false"></asp:BoundColumn>
                                                <%--13--%>
                                                <asp:TemplateColumn HeaderText="Rate %" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="90px"
                                                    ItemStyle-Width="90px" FooterText="dgcolhRate">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhRate" CssClass="percentage" Style="text-align: right" runat="server"
                                                            AutoPostBack="True" Text='<%# Eval("rate") %>' OnTextChanged="txtdgcolhRate_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle Width="90px" />
                                                </asp:TemplateColumn>
                                                <%--14--%>
                                                <asp:BoundColumn DataField="noofinstallment" HeaderText="No. Of Instl." Visible="false">
                                                </asp:BoundColumn>
                                                <%--15--%>
                                                <asp:TemplateColumn HeaderText="No. Of Instl." HeaderStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Width="90px" ItemStyle-Width="90px" FooterText="dgcolhNoOfEMI">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhNoOfEMI" CssClass="numberonly" Style="text-align: right"
                                                            runat="server" AutoPostBack="True" Text='<%# Eval("noofinstallment") %>' OnTextChanged="txtdgcolhNoOfEMI_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle Width="90px" />
                                                </asp:TemplateColumn>
                                                <%--16--%>
                                                <asp:BoundColumn DataField="installmentamt" HeaderText="Installment Amount" Visible="false">
                                                </asp:BoundColumn>
                                                <%--17--%>
                                                <asp:TemplateColumn HeaderText="Installment Amount" HeaderStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Width="150px" ItemStyle-Width="150px" FooterText="dgcolhInstallmentAmount">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhInstallmentAmount" Style="text-align: right" runat="server"
                                                            AutoPostBack="True" Text='<%# Eval("installmentamt") %>' OnTextChanged="txtdgcolhInstallmentAmount_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle Width="150px" />
                                                </asp:TemplateColumn>
                                                <%--18--%>
                                                <asp:BoundColumn DataField="IsGrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp">
                                                </asp:BoundColumn>
                                                <%--19--%>
                                                <asp:BoundColumn DataField="IsEx" HeaderText="objdgcolhIsEx" Visible="false" FooterText="objdgcolhIsEx">
                                                </asp:BoundColumn>
                                                <%--20--%>
                                                <asp:BoundColumn DataField="Pendingunkid" HeaderText="objdgcolhPendingunkid" Visible="false"
                                                    FooterText="objdgcolhPendingunkid"></asp:BoundColumn>
                                                <%--21--%>
                                                <asp:BoundColumn DataField="GrpId" HeaderText="objdgcolhGrpId" Visible="false" FooterText="objdgcolhGrpId">
                                                </asp:BoundColumn>
                                                <%--22--%>
                                                <asp:BoundColumn DataField="ApprId" HeaderText="objdgcolhApprId" Visible="false"
                                                    FooterText="objdgcolhApprId"></asp:BoundColumn>
                                                <%--23--%>
                                                <asp:BoundColumn DataField="EmpId" HeaderText="objdgcolhEmpId" Visible="false" FooterText="objdgcolhEmpId">
                                                </asp:BoundColumn>
                                                <%--24--%>
                                                <asp:BoundColumn DataField="deductionperiodunkid" HeaderText="objdgcolhDeductionPeriodId"
                                                    Visible="false" FooterText="objdgcolhDeductionPeriodId"></asp:BoundColumn>
                                                <%--25--%>
                                                <asp:BoundColumn DataField="calctypeId" HeaderText="objdgcolhCalcTypeId" Visible="false"
                                                    FooterText="objdgcolhCalcTypeId"></asp:BoundColumn>
                                                <%--26--%>
                                                <asp:BoundColumn DataField="instlamt" HeaderText="objdgMatchInstallmentAmt" Visible="false"
                                                    FooterText="objdgMatchInstallmentAmt"></asp:BoundColumn>
                                                <%--27--%>
                                                <asp:BoundColumn DataField="intrsamt" HeaderText="objdgcolhInterestAmt" Visible="false"
                                                    FooterText="objdgcolhInterestAmt"></asp:BoundColumn>
                                                <%--28--%>
                                                <asp:BoundColumn DataField="netamt" HeaderText="objdgcolhNetAmount" Visible="false"
                                                    FooterText="objdgcolhNetAmount"></asp:BoundColumn>
                                                <%--29--%>
                                                <asp:BoundColumn DataField="interest_calctype_id" HeaderText="objdgcolhIntCalcTypeId"
                                                    Visible="false" FooterText="objdgcolhIntCalcTypeId"></asp:BoundColumn>
                                                 <%--30--%>
                                                <asp:BoundColumn DataField="mapped_tranheadunkid" HeaderText="objdgcolhMappedHead"
                                                    Visible="false" FooterText="objdgcolhMappedHead"></asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnAssign" runat="server" Text="Assign" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

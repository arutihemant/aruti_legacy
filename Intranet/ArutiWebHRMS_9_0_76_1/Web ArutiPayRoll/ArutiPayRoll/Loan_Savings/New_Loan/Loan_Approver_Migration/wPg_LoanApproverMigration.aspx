﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanApproverMigration.aspx.vb"
    Inherits="Loan_Savings_New_Loan_wPg_LoanApproverMigration" Title="Loan Approver Migration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <center>
        <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan Approver Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblOldApprover" runat="server" Text="From Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblNewApprover" runat="server" Text="To Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblOldLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblNewLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 45%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="height: 5px; width: 100%" colspan="3">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 70%; margin-bottom: 5px">
                                                            <asp:TextBox ID="txtOldSearchEmployee" runat="server" AutoPostBack="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%; text-align: right; font-weight: bold">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"></asp:LinkButton>
                                                        </td>
                                                        <td style="width: 10%; text-align: right; font-weight: bold">
                                                            <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="3">
                                                            <div id="scrollable-container" style="vertical-align: top; overflow: auto; max-height: 400px;
                                                                margin-top: 5px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="dgOldApproverEmp" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <ItemStyle CssClass="griviewitem" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="ChkOldAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkOldAll_CheckedChanged" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="ChkgvOldSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvOldSelect_CheckedChanged" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                            <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="true"
                                                                            FooterText="colhdgEmployeecode"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                            Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10%;" valign="middle">
                                                <table id="tblOperation" style="width: 100%;">
                                                    <tr style="width: 100%">
                                                        <td align="center">
                                                            <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btndefault" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td align="center">
                                                            <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btndefault" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 45%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="height: 5px; width: 100%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <cc1:TabContainer ID="tabMain" runat="server" ActiveTabIndex="0" Style="overflow: auto">
                                                                <cc1:TabPanel ID="tbMigrationEmp" runat="server" HeaderText="Migrated Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                                    <asp:TextBox ID="txtNewApproverMigratedEmp" runat="server" Style="margin-left: 5px;"
                                                                                        AutoPostBack="True"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container1" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                                        <asp:DataGrid ID="dgNewApproverMigratedEmp" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                            <ItemStyle CssClass="griviewitem" />
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn>
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="ChkNewAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkNewAll_CheckedChanged" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:CheckBox ID="ChkgvNewSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvNewSelect_CheckedChanged" />
                                                                                                    </ItemTemplate>
                                                                                                    <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                                                    <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn DataField="employeecode" FooterText="colhdgMigratedEmpCode" HeaderText="Employee Code"
                                                                                                    ReadOnly="True">
                                                                                                    <HeaderStyle Width="30%" />
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="employeename" FooterText="colhdgMigratedEmp" HeaderText="Employee"
                                                                                                    ReadOnly="True">
                                                                                                    <HeaderStyle Width="70%" />
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"
                                                                                                    Visible="False" />
                                                                                            </Columns>
                                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                                        </asp:DataGrid></div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                                <cc1:TabPanel ID="tbAssignedEmp" runat="server" HeaderText="Assigned Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                                    <asp:TextBox ID="txtNewApproverAssignEmp" runat="server" Style="margin-left: 5px"
                                                                                        AutoPostBack="True"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container2" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                                        <asp:DataGrid ID="dgNewApproverAssignEmp" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                            <Columns>
                                                                                                <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" ReadOnly="True"
                                                                                                    FooterText="colhdgAssignEmpCode">
                                                                                                    <HeaderStyle Width="30%" />
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhdgAssignEmployee">
                                                                                                    <HeaderStyle Width="70%" />
                                                                                                </asp:BoundColumn>
                                                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="True"
                                                                                                    Visible="False" />
                                                                                            </Columns>
                                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                                            <ItemStyle CssClass="griviewitem" />
                                                                                        </asp:DataGrid></div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

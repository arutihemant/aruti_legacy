﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeExperienceList.aspx.vb"
    Inherits="HR_wPg_EmployeeExperienceList" MasterPageFile="~/home.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Job History And Experience List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%" id="Table1">
                                       <%-- <tr style="width: 100%;">
                                            <td colspan="6">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>--%>
                                        <tr style="width: 100%;">
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="drpJob" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:TextBox ID="TxtSupervisor" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblInstitution" runat="server" Text="Company"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:TextBox ID="TxtCompany" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <uc2:DateCtrl ID="dtpStartdate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 13%;">
                                                <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" CssClass="btndefault" runat="server" Text="New" Visible="False" />
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                        <asp:Button ID="BtnReset" CssClass="btndefault" runat="server" Text="Reset" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />

                               <%--'Gajanan [17-DEC-2018] -- Start--%>
                               <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    
                                        <%--<asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail"
                                            Style="float: left" />
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                            Style="padding: 2px; font-weight: bold; float: left; margin-top: 5px; margin-left: 10px;"></asp:Label>--%>
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                            <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                        <%--'Gajanan [17-DEC-2018] -- End--%>
                                    </div>
                                </div>
                                <asp:Panel ID="pbl_GvExpList" runat="server" ScrollBars="Auto" Height="350px">
                                    <asp:DataGrid ID="GvExpList" runat="server" Width="99%" AutoGenerateColumns="False"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                        Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                        <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<0>--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<1>--%>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <%--<2>--%>
                                            <asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True" FooterText="colhCompany">
                                            </asp:BoundColumn>
                                            <%--<3>--%>
                                            <asp:BoundColumn DataField="Jobs" HeaderText="Job" ReadOnly="True" FooterText="colhJob">
                                            </asp:BoundColumn>
                                            <%--<4>--%>
                                            <asp:BoundColumn DataField="StartDate" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartDate">
                                            </asp:BoundColumn>
                                            <%--<5>--%>
                                            <asp:BoundColumn DataField="EndDate" HeaderText="End Date" ReadOnly="True" FooterText="colhEndDate">
                                            </asp:BoundColumn>
                                            <%--<6>--%>
                                            <asp:BoundColumn DataField="Supervisor" HeaderText="Supervisor" ReadOnly="True" FooterText="colhSupervisor">
                                            </asp:BoundColumn>
                                            <%--<7>--%>
                                            <asp:BoundColumn DataField="Remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark">
                                            </asp:BoundColumn>
                                            <%--<8>--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%--<9>--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                            </asp:BoundColumn>
                                            <%--<10>--%>
                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="ExpId" HeaderText="ExpId" Visible="false" FooterText="objdgcolhRefreeTranId">
                                            </asp:BoundColumn>
                                            <%--<11>--%>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%--<12>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                        </Columns>
                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

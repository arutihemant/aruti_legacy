﻿<%@ Page Title="Employee Identity List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmpMembership_List.aspx.vb" Inherits="wPgEmpMembership_List" %>

<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="ucYesNo" %>
<%--'S.SANDEEP |15-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%--'S.SANDEEP |15-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Membership List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="4">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="LblEmployee" runat="server" Style="margin-left: 5px" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblMembershipType" runat="server" Text="Membership"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboMembership" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                         <asp:Button ID="btnCloseForm" runat="server" CssClass="btndefault" Text="Close" />
                                        
                                        <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                            <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                        <%--'S.SANDEEP |15-APR-2019| -- END--%>
                                    </div>
                                </div>
                                <asp:Panel ID="PnlGrid" runat="server" Width="100%">
                                    <asp:DataGrid ID="dgvMembership" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:BoundColumn DataField="ccategory"></asp:BoundColumn>
                                            <%--0--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                            CommandName="Edit"></asp:LinkButton>
                                                        <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                            Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                        <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                        <%--'S.SANDEEP |15-APR-2019| -- END--%>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" 
                                                         />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--1--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--2--%>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkActiveMem" runat="server" ToolTip="Active Membership" Text="Active"
                                                        CommandName="Active"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--3--%>
                                            <asp:BoundColumn DataField="catagory" HeaderText="Membership Category" ReadOnly="True"
                                                FooterText="colhMembershipCategory"></asp:BoundColumn>
                                            <%--4--%>
                                            <asp:BoundColumn DataField="membership" HeaderText="Membership" ReadOnly="True" FooterText="colhMembershipType">
                                            </asp:BoundColumn>
                                            <%--5--%>
                                            <asp:BoundColumn DataField="membershipno" HeaderText="Membership No." ReadOnly="True"
                                                FooterText="colhMembershipNo"></asp:BoundColumn>
                                            <%--6--%>
                                            <asp:BoundColumn DataField="issue_date" HeaderText="Issue Date" ReadOnly="True" FooterText="colhMemIssueDate">
                                            </asp:BoundColumn>
                                            <%--7--%>
                                            <asp:BoundColumn DataField="start_date" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartDate">
                                            </asp:BoundColumn>
                                            <%--8--%>
                                            <asp:BoundColumn DataField="expiry_date" HeaderText="End Date" ReadOnly="True" FooterText="colhEndDate">
                                            </asp:BoundColumn>
                                            <%--9--%>
                                            <asp:BoundColumn DataField="remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark">
                                            </asp:BoundColumn>
                                            <%--10--%>
                                            <asp:BoundColumn DataField="membershiptranunkid" HeaderText="membershiptranunkid"
                                                Visible="false" FooterText="colhmembershiptranunkid"></asp:BoundColumn>
                                            <%--11--%>
                                            <asp:BoundColumn DataField="IsGrp" Visible="false"></asp:BoundColumn>
                                            <%--12--%>
                                            <asp:BoundColumn DataField="ccategoryid" Visible="false"></asp:BoundColumn>
                                            <%--13--%>
                                            <asp:BoundColumn DataField="membership_categoryunkid" Visible="false"></asp:BoundColumn>
                                            <%--14--%>
                                            <asp:BoundColumn DataField="membershipunkid" Visible="false"></asp:BoundColumn>
                                            <%--15--%>
                                            <asp:BoundColumn DataField="emptrnheadid" Visible="false"></asp:BoundColumn>
                                            <%--16--%>
                                            <asp:BoundColumn DataField="cotrnheadid" Visible="false"></asp:BoundColumn>
                                            <%--17--%>
                                            <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="operationtypeid" ReadOnly="true"
                                                Visible="false"></asp:BoundColumn>
                                            <%--18--%>
                                            <asp:BoundColumn DataField="operationtype" HeaderText="Operation Type" ReadOnly="true">
                                            </asp:BoundColumn>
                                            <%--19--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" ReadOnly="true" Visible="false">
                                            </asp:BoundColumn>
                                            <%--20--%>
                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <%--21--%>
                                            <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                    <cc1:ModalPopupExtender ID="popup_active" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnClose" PopupControlID="pnlActiveMembership" TargetControlID="HiddenField1">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlActiveMembership" runat="server" CssClass="newpopup" Style="display: none;
                        width: 430px; z-index: 100099!important;" DefaultButton="btnOk">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Assign Head" />
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-body-default">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblActMembership" runat="server" Text="Membership"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAMembership" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEmpHead" runat="server" Text="Emp. Head"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmpHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCoHead" runat="server" Text="Co. Head"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCoHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEffctivePeriod" runat="server" Text="Effective Period"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboEffectivePeriod" runat="server" ReadOnly="true" Width="252px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous Slab" />
                                                    <asp:CheckBox ID="chkOverwriteHeads" runat="server" Text="Overwrite Heads" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="btnDefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucYesNo:ConfirmYesNo ID="popup_yesno" runat="server" />
                    <%--'S.SANDEEP |15-APR-2019| -- START--%>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <%--'S.SANDEEP |15-APR-2019| -- END--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wpg_AppRejEmployeeData.aspx.vb"
    Inherits="HR_wpg_AppRejEmployeeData" Title="Appove/Reject Employee Data" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}

    </script>

    <style>
        .dropbtn
        {
            font-size: 16px;
            border: none;
        }
        .dropdown
        {
            position: relative;
            display: inline-block;
            width: 15px;
            margin: 0;
        }
        .dropdown-content
        {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            margin-top: 0;
        }
        .dropdown-content a
        {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }
        .dropdown:hover .dropdown-content
        {
            display: block;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Appove/Reject Employee Data"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtApprover" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hfApprover" runat="server" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtLevel" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:HiddenField ID="hfLevel" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblEmployeeData" runat="server" Text="Select Data"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpEmployeeData" runat="server">
                                            </asp:DropDownList>
                                            <%--AutoPostBack="true"--%>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblSelectOperationType" runat="server" Text="Opration Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpOprationType" runat="server">
                                            </asp:DropDownList>
                                            <%--AutoPostBack="true"--%>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Panel ID="pnlextopr" runat="server" Width="80%" Style="float: left" Visible="false">
                                            <div class="row2">
                                                <div class="ib" style="width: 7%">
                                                    <asp:Label ID="lblEffectivePeriod" runat="server" Text="Period" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 16%">
                                                    <asp:DropDownList ID="cboEffectivePeriod" runat="server" Width="100%">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="ib" style="width: 31%">
                                                    <asp:RadioButton ID="radAll" Text="Apply To All" runat="server" GroupName="extopr"
                                                        Font-Bold="true" />
                                                    <asp:RadioButton ID="radChecked" Text="Apply To Checked" runat="server" GroupName="extopr"
                                                        Font-Bold="true" />
                                                </div>
                                                <div class="ib" style="width: 6%">
                                                    <asp:LinkButton ID="lnkSet" runat="server" Text="[SET]" Font-Bold="true"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div class="ib" style="width: 100%">
                                            <asp:TextBox ID="txtsearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 100%; height: 230px; overflow: auto">
                                            <asp:GridView ID="gvApproveRejectEmployeeData" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="iCheck,isgrp,employeeunkid,qualificationtranunkid,qualificationgroupunkid,qualificationunkid,dpndtbeneficetranunkid,addresstypeid,isbirthinfo">
                                                <%--'S.SANDEEP |26-APR-2019| -- START--%>
                                                <%--addresstypeid,isbirthinfo--%>
                                                <%--'S.SANDEEP |26-APR-2019| -- END--%>
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="5px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <%--<asp:LinkButton ID="lnkdownload" runat="server" ToolTip="Download Attached Document(s)"
                                                                Visible="false" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-download"></i></asp:LinkButton>--%>
                                                            <div class="dropdown">
                                                                <asp:LinkButton ID="lnkdownload" runat="server" class="dropbtn"><i class="fa fa-download"></i></asp:LinkButton>
                                                                <div class="dropdown-content">
                                                                    <asp:LinkButton runat="server" ID="lnkdownloadall" OnClick="btndownloadAttachment_Click">All Download</asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="lnkdownloadnew" OnClick="btndownloadAttachment_Click">New</asp:LinkButton>
                                                                    <asp:LinkButton runat="server" ID="lnkdownloaddeleted" OnClick="btndownloadAttachment_Click">Delete</asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <asp:HiddenField ID="hfqualificationtranunkid" runat="server" Value='<%#Eval("qualificationtranunkid")%>' />
                                                            <%--'S.SANDEEP |26-APR-2019| -- START--%>
                                                            <%--<asp:HiddenField ID="hfnewattachdocumnetid" runat="server" Value='<%#Eval("newattachdocumnetid")%>' />
                                                            <asp:HiddenField ID="hfdeleteattachdocumnetid" runat="server" Value='<%#Eval("deleteattachdocumnetid")%>' />--%>
                                                            <asp:HiddenField ID="hfnewattachdocumentid" runat="server" Value='<%#Eval("newattachdocumentid")%>' />
                                                            <asp:HiddenField ID="hfdeleteattachdocumentid" runat="server" Value='<%#Eval("deleteattachdocumentid")%>' />
                                                            <%--'S.SANDEEP |26-APR-2019| -- END--%>
                                                            <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#Eval("employeeunkid")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Skill Category" DataField="scategory" FooterText="dgcolhSkillCategory" />
                                                    <asp:BoundField HeaderText="Skill" DataField="skill" FooterText="dgcolhSkill" />
                                                    <asp:BoundField HeaderText="Description" DataField="description" FooterText="dgcolhDescription" />
                                                    <asp:BoundField HeaderText="Qualification Group" DataField="qualificationgrpname"
                                                        FooterText="dgcolhQualifyGroup" />
                                                    <asp:BoundField HeaderText="Qualification" DataField="qualificationname" FooterText="dgcolhQualification" />
                                                    <asp:BoundField HeaderText="Award From Date" DataField="award_start_date" FooterText="dgcolhAwardDate" />
                                                    <asp:BoundField HeaderText="Award To Date" DataField="award_end_date" FooterText="dgcolhAwardToDate" />
                                                    <asp:BoundField HeaderText="Institute" DataField="institute_name" FooterText="dgcolhInstitute" />
                                                    <asp:BoundField HeaderText="Ref. No." DataField="reference_no" FooterText="dgcolhRefNo" />
                                                    <asp:BoundField HeaderText="Company" DataField="cname" FooterText="dgcolhCompany" />
                                                    <asp:BoundField HeaderText="Job" DataField="old_job" FooterText="dgcolhJob" />
                                                    <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhStartDate" />
                                                    <asp:BoundField HeaderText="End Date" DataField="end_date" FooterText="dgcolhEndDate" />
                                                    <asp:BoundField HeaderText="Supervisor" DataField="supervisor" FooterText="dgcolhSupervisor" />
                                                    <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhRemark" />
                                                    <asp:BoundField HeaderText="Refree Name" DataField="rname" FooterText="dgcolhRefreeName" />
                                                    <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                                                    <asp:BoundField HeaderText="Id. No." DataField="Company" FooterText="dgcolhIdNo" />
                                                    <asp:BoundField HeaderText="Email" DataField="Email" FooterText="dgcolhEmail" />
                                                    <asp:BoundField HeaderText="Tel. No." DataField="telephone_no" FooterText="dgcolhTelNo" />
                                                    <asp:BoundField HeaderText="Mobile No." DataField="mobile_no" FooterText="dgcolhMobile" />
                                                    <asp:BoundField HeaderText="Form Name" DataField="form_name" FooterText="objcolhformname" />
                                                    <%--'Gajanan [22-Feb-2019] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:BoundField HeaderText="Identity Type" DataField="IdType" FooterText="dgcolhIdType" />
                                                    <asp:BoundField HeaderText="Serial No." DataField="serial_no" FooterText="dgcolhSrNo" />
                                                    <asp:BoundField HeaderText="Identity No." DataField="identity_no" FooterText="dgcolhIdentityNo" />
                                                    <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhIdCountry" />
                                                    <asp:BoundField HeaderText="Issue Place" DataField="issued_place" FooterText="dgcolhIdIssuePlace" />
                                                    <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhIdIssueDate" />
                                                    <asp:BoundField HeaderText="Expiry Date" DataField="expiry_date" FooterText="dgcolhIdExpiryDate" />
                                                    <asp:BoundField HeaderText="DL Class" DataField="dl_class" FooterText="dgcolhIdDLClass" />
                                                    <asp:BoundField HeaderText="Name" DataField="dependantname" FooterText="dgcolhDBName" />
                                                    <asp:BoundField HeaderText="Relation" DataField="relation" FooterText="dgcolhDBRelation" />
                                                    <asp:BoundField HeaderText="Birth Date" DataField="birthdate" FooterText="dgcolhDBbirthdate" />
                                                    <asp:BoundField HeaderText="Identify Number" DataField="identify_no" FooterText="dgcolhDBIdentifyNo" />
                                                    <asp:BoundField HeaderText="Gender" DataField="gender" FooterText="dgcolhDBGender" />
                                                    <%--'Gajanan [22-Feb-2019] -- End--%>
                                                    <%--'Gajanan [18-Mar-2019] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:BoundField HeaderText="Address Type" DataField="addresstype" FooterText="dgcolhAddressType" />
                                                    <asp:BoundField HeaderText="Address1" DataField="address1" FooterText="dgcolhAddress1" />
                                                    <asp:BoundField HeaderText="Address2" DataField="address2" FooterText="dgcolhAddress2" />
                                                    <asp:BoundField HeaderText="Firstname" DataField="firstname" FooterText="dgcolhFirstname" />
                                                    <asp:BoundField HeaderText="Lastname" DataField="lastname" FooterText="dgcolhLastname" />
                                                    <asp:BoundField HeaderText="Country" DataField="country" FooterText="dgcolhAddressCountry" />
                                                    <asp:BoundField HeaderText="State" DataField="state" FooterText="dgcolhAddressState" />
                                                    <asp:BoundField HeaderText="City" DataField="city" FooterText="dgcolhAddressCity" />
                                                    <asp:BoundField HeaderText="ZipCode" DataField="zipcode_code" FooterText="dgcolhAddressZipCode" />
                                                    <asp:BoundField HeaderText="Province" DataField="provicnce" FooterText="dgcolhAddressProvince" />
                                                    <asp:BoundField HeaderText="Road" DataField="road" FooterText="dgcolhAddressRoad" />
                                                    <asp:BoundField HeaderText="Estate" DataField="estate" FooterText="dgcolhAddressEstate" />
                                                    <asp:BoundField HeaderText="Province1" DataField="provicnce1" FooterText="dgcolhAddressProvince1" />
                                                    <asp:BoundField HeaderText="Road1" DataField="Road1" FooterText="dgcolhAddressRoad1" />
                                                    <asp:BoundField HeaderText="Chiefdom" DataField="chiefdom" FooterText="dgcolhAddressChiefdom" />
                                                    <asp:BoundField HeaderText="Village" DataField="village" FooterText="dgcolhAddressVillage" />
                                                    <asp:BoundField HeaderText="Town" DataField="town" FooterText="dgcolhAddressTown" />
                                                    <asp:BoundField HeaderText="Mobile" DataField="mobile" FooterText="dgcolhAddressMobile" />
                                                    <asp:BoundField HeaderText="Telephone Number" DataField="tel_no" FooterText="dgcolhAddressTel_no" />
                                                    <asp:BoundField HeaderText="Plot Number" DataField="plotNo" FooterText="dgcolhAddressPlotNo" />
                                                    <asp:BoundField HeaderText="Alternate Number" DataField="alternateno" FooterText="dgcolhAddressAltNo" />
                                                    <asp:BoundField HeaderText="Email" DataField="email" FooterText="dgcolhAddressEmail" />
                                                    <asp:BoundField HeaderText="Fax" DataField="fax" FooterText="dgcolhAddressFax" />
                                                    <%--'Gajanan [18-Mar-2019] -- End--%>
                                                    <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                    <asp:BoundField HeaderText="Membership Category" DataField="Category" FooterText="dgcolhCategory" />
                                                    <asp:BoundField HeaderText="Membership" DataField="membershipname" FooterText="dgcolhmembershipname" />
                                                    <asp:BoundField HeaderText="Membership No" DataField="membershipno" FooterText="dgcolhmembershipno" />
                                                    <asp:BoundField HeaderText="Eff. Period" DataField="period_name" FooterText="dgcolhperiod_name" />
                                                    <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhissue_date" />
                                                    <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhstart_date" />
                                                    <asp:BoundField HeaderText="End Date" DataField="expiry_date" FooterText="dgcolhexpiry_date" />
                                                    <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhMemRemark" />
                                                    <%--'S.SANDEEP |15-APR-2019| -- END
                                                    
                                                    <%--'Gajanan [17-April-2019]-- START--%>
                                                    <asp:BoundField HeaderText="Birth Country" DataField="country" FooterText="dgcolhBirthCountry" />
                                                    <asp:BoundField HeaderText="Birth State" DataField="state" FooterText="dgcolhBirthState" />
                                                    <asp:BoundField HeaderText="Birth City" DataField="city" FooterText="dgcolhBirthCity" />
                                                    <asp:BoundField HeaderText="Birth Certificate No." DataField="birthcertificateno"
                                                        FooterText="dgcolhBirthCertificateNo" />
                                                    <asp:BoundField HeaderText="Birth Town1" DataField="town" FooterText="dgcolhBirthTown1" />
                                                    <asp:BoundField HeaderText="Birth Village1" DataField="birth_village" FooterText="dgcolhBirthVillage1" />
                                                    <asp:BoundField HeaderText="Birth Ward" DataField="birth_ward" FooterText="dgcolhBirthWard" />
                                                    <asp:BoundField HeaderText="Birth Village" DataField="birth_village" FooterText="dgcolhBirthVillage" />
                                                    <asp:BoundField HeaderText="Birth Chiefdom" DataField="chiefdom" FooterText="dgcolhBirthChiefdom" />
                                                    <asp:BoundField HeaderText="Complexion" DataField="Complexion" FooterText="dgcolhComplexion" />
                                                    <asp:BoundField HeaderText="Blood Group" DataField="BloodGroup" FooterText="dgcolhBloodGroup" />
                                                    <asp:BoundField HeaderText="Eye Color" DataField="EyeColor" FooterText="dgcolhEyeColor" />
                                                    <asp:BoundField HeaderText="Nationality" DataField="Nationality" FooterText="dgcolhNationality" />
                                                    <asp:BoundField HeaderText="EthinCity" DataField="Ethnicity" FooterText="dgcolhEthinCity" />
                                                    <asp:BoundField HeaderText="Religion" DataField="Religion" FooterText="dgcolhReligion" />
                                                    <asp:BoundField HeaderText="Hair" DataField="HairColor" FooterText="dgcolhHair" />
                                                    <asp:BoundField HeaderText="Marital Status" DataField="Maritalstatus" FooterText="dgcolhMaritalStatus" />
                                                    <asp:BoundField HeaderText="Extra Telephone" DataField="ExtTelephoneno" FooterText="dgcolhExtraTel" />
                                                    <asp:BoundField HeaderText="Language1" DataField="language1" FooterText="dgcolhLanguage1" />
                                                    <asp:BoundField HeaderText="Language2" DataField="language2" FooterText="dgcolhLanguage2" />
                                                    <asp:BoundField HeaderText="Language3" DataField="language3" FooterText="dgcolhLanguage3" />
                                                    <asp:BoundField HeaderText="Language4" DataField="language4" FooterText="dgcolhLanguage4" />
                                                    <asp:BoundField HeaderText="Height" DataField="height" FooterText="dgcolhHeight" />
                                                    <asp:BoundField HeaderText="Weight" DataField="weight" FooterText="dgcolhWeight" />
                                                    <asp:BoundField HeaderText="Marital Date" DataField="anniversary_date" FooterText="dgcolhMaritalDate" />
                                                    <asp:BoundField HeaderText="Allergies" DataField="Allergies" FooterText="dgcolhAllergies" />
                                                    <asp:BoundField HeaderText="Sports/Hobbies" DataField="sports_hobbies" FooterText="dgcolhSportsHobbies" />
                                                    <%--'Gajanan [17-April-2019]-- End--%>
                                                    <asp:BoundField HeaderText="Inactive Reason" DataField="reason" FooterText="dgcolhInactiveReason" />
                                                    <asp:BoundField HeaderText="Void Reason" DataField="voidreason" FooterText="dgcolhVoidReason" />
                                                    
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 75%">
                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btndefault" />
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btndefault" />
                                        <asp:Button ID="btnShowMyReport" runat="server" Text="My Report" CssClass="btndefault" />
                                        <asp:Label ID="lblotherqualificationnote" runat="server" ForeColor="Blue" Text="Note: This color indicates other qualification"
                                            Style="font-weight: bold; float: left;"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc2:Cnf_YesNo ID="Cnf_WithoutRemark" runat="server" Title="Aruti" />
                        <uc3:Pop_report ID="Pop_report" runat="server" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvApproveRejectEmployeeData" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

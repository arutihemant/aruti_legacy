﻿<%@ Page Title="Membership Info" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgEmpMemberships.aspx.vb" Inherits="wPgEmpMemberships" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="ucYesNo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Membership Info"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Membership Info Add/Edit"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 17%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 83%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 17%">
                                                <asp:Label ID="lblMembershipName" runat="server" Text="Mem. Catagory"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboMemCategory" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 17%">
                                                <asp:Label ID="lblMemIssueDate" runat="server" Text="Issue Date"></asp:Label>
                                            </td>
                                            <td style="width: 31%">
                                                <uc2:DateCtrl ID="dtpIssueDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 17%">
                                                <asp:Label ID="lblMembershipType" runat="server" Text="Membership"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboMembership" runat="server" AutoPostBack="true" >
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 17%">
                                                <asp:Label ID="lblActivationDate" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 31%">
                                                <uc2:DateCtrl ID="dtpStartDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 17%">
                                                <asp:Label ID="lblMembershipNo" runat="server" Text="Membership No."></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtMembershipNo" runat="server" ></asp:TextBox>
                                            </td>
                                            <td style="width: 17%">
                                                <asp:Label ID="lblMemExpiryDate" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 31%">
                                                <uc2:DateCtrl ID="dtpEndDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 17%" valign="top">
                                                <asp:Label ID="lblMembershipRemark" runat="server" Text="Remark"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 83%">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" Wrap="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_active" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnmemClose" PopupControlID="pnlActiveMembership" TargetControlID="HiddenField1">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlActiveMembership" runat="server" CssClass="newpopup" Style="display: none;
                        width: 430px; z-index: 100099!important;" DefaultButton="btnOk">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Assign Head" />
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-body-default">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblActMembership" runat="server" Text="Membership"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAMembership" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEmpHead" runat="server" Text="Emp. Head"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmpHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblCoHead" runat="server" Text="Co. Head"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCoHead" runat="server" ReadOnly="true" Width="250px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblEffctivePeriod" runat="server" Text="Effective Period"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="cboEffectivePeriod" runat="server" ReadOnly="true" Width="252px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous Slab" />
                                                    <asp:CheckBox ID="chkOverwriteHeads" runat="server" Text="Overwrite Heads" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="btnDefault" />
                                            <asp:Button ID="btnmemClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucYesNo:ConfirmYesNo id="popup_yesno" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

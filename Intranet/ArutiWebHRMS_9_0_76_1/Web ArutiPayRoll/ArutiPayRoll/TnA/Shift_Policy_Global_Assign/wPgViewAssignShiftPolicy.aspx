﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/home.master"
    CodeFile="wPgViewAssignShiftPolicy.aspx.vb" Inherits="TnA_Shift_Policy_Global_Assign_wPgViewAssignShiftPolicy"
    Title="View Assigned Shift/Policy List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/TimeCtrl.ascx" TagName="TimeCtrl" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="Allocation" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assigned Shift/Policy List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblViewAssign" runat="server" Text="View Assign"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:RadioButtonList ID="RdShiftPolicy" runat="server" RepeatDirection="Horizontal"
                                                    AutoPostBack="true" Width="30%">
                                                    <asp:ListItem Value="1" Text="Shift" Selected="True" />
                                                    <asp:ListItem Value="2" Text="Policy" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblShiftType" runat="server" Text="Shift Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboShiftType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="LblShift" runat="server" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboShift" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl ID="dtpDate1" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPolicy" runat="server" Text="Policy"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboPolicy" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblTo" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <uc2:DateCtrl ID="dtpDate2" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btnDefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top: 5px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnlGrid" ScrollBars="Auto" style="max-height:300px; margin-top:5px; margin-bottom:10px" runat="server">
                                                <asp:UpdatePanel ID="UpdateViewShiftPolicyList" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="dgvData" runat="server" Style="margin: auto" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="ecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhCode" />
                                                                <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEmployee" />
                                                                <asp:BoundField DataField="effectivedate" HeaderText="Effective Date" ReadOnly="True"
                                                                    FooterText="dgcolhEffectiveDate" />
                                                                <asp:BoundField DataField="shiftname" HeaderText="Shift" ReadOnly="True" FooterText="dgcolhShift" />
                                                                <asp:BoundField DataField="policyname" HeaderText="Policy" ReadOnly="True" />
                                                                <asp:BoundField DataField="shifttranunkid" HeaderText="Shifttranunkid" ReadOnly="True"
                                                                    Visible="false" />
                                                                <asp:BoundField DataField="emppolicytranunkid" HeaderText="Emppolicytranunkid" ReadOnly="True"
                                                                    Visible="false" />
                                                                <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <uc9:Confirmation ID="DeleteConfirmation" runat="server" Title="Confirmation" Message="Are you sure you want to delete checked transaction(s)?" />
                    <uc8:DeleteReason ID="DeleteReason" runat="server" Title="Delete Reason" />
                    <uc6:Allocation ID = "popupAdvanceFilter" runat ="server" Title= "Advance Filter" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

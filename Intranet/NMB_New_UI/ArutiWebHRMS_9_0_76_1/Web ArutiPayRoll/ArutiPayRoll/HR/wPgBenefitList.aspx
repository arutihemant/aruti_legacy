﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgBenefitList.aspx.vb"
    Inherits="wPgBenefitList" Title="Employee Benefits" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Benefits"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        <asp:DropDownList ID="drpEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBenefitType" runat="server" Text="Benefit Group"></asp:Label>
                                        <asp:DropDownList ID="drpBenefitGroup" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBenefitPlan" runat="server" Text="Plan"></asp:Label>
                                        <asp:DropDownList ID="drpPlan" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTransactionHead" runat="server" Text="Tran.Head"></asp:Label>
                                        <asp:DropDownList ID="DrpTranHead" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:RadioButtonList ID="radiolist1" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0">Show Void</asp:ListItem>
                                            <asp:ListItem Selected="true" Value="1">Show Active</asp:ListItem>
                                            <asp:ListItem Value="2">Show All</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" Text="New" CssClass="btn btn-primary" Visible="False" />
                                <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table table-responsive" style="height: 300px">
                                            <asp:DataGrid ID="dgvBenefit" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Select" CommandName="Select">
                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="BGroupName" HeaderText="Benefit Group" ReadOnly="True"
                                                        FooterText="colhBenefitGroup"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="BPlanName" HeaderText="Benefit Plan" ReadOnly="True"
                                                        FooterText="colhBenefitPlan"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TranHead" HeaderText="Transaction Head" ReadOnly="True"
                                                        FooterText="colhTransactionHead"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isvoid" HeaderText="isvoid" ReadOnly="True" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

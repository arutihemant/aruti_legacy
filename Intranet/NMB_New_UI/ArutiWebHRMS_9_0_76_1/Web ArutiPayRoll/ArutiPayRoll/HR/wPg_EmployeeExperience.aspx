﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeExperience.aspx.vb"
    Inherits="HR_wPg_EmployeeExperience" MasterPageFile="~/Home1.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }    
    
     function IsValidAttach() {
         return true;
     }
    
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Job History And Experience Form Add/Edit"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0 p-l-0">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblInstitution" runat="server" Text="Company" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAddress" runat="server" Text="Address" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtAdress" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpStartdate" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOldJob" runat="server" ValidationGroup="Save" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOldJob"
                                                CssClass="ErrorControl" Display="Dynamic" ValidationGroup="Save" ErrorMessage="Job is compulsory information. Please give Job to continue. "
                                                ForeColor="White" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSupervisor" runat="server" Text="Supervisors" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtSuperVisors" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtRemark" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblMemo" runat="server" Text="Memo" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtMemo" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  p-r-0 p-l-0">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                <li role="presentation" class="active"><a href="#Benefits" data-toggle="tab">
                                                    <asp:Label ID="tabpBenefit" runat="server" Text="Benefits" CssClass="form-label"></asp:Label></a></li>
                                                <li role="presentation"><a href="#OtherBenefits" data-toggle="tab">
                                                    <asp:Label ID="tabpOtherBenefit" runat="server" Text="Other Benefits" CssClass="form-label"></asp:Label>
                                                </a></li>
                                            </ul>
                                            <div class="tab-content" style="height: 190px">
                                                <div role="tabpanel" class="tab-pane fade in active" id="Benefits">
                                                    <asp:CheckBoxList ID="ChkBenefits" runat="server" />
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="OtherBenefits">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtOtherBenefits" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCurrency" runat="server" Text="Currency Sign" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtSign" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblLastGrossPay" runat="server" Text="Last Gross Pay" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtGrossPay" runat="server" MaxLength="28" Style="text-align: right;"
                                                        onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkCanContatEmployee" runat="server" Text="Can Contact Previous Employer" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblContacPerson" runat="server" Text="Contact Person" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtContactPerson" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblContactNo" runat="server" Text="Contact Address" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtContactAddress" runat="server" TextMode="MultiLine" Rows="4"
                                                        CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblLeaveReason" runat="server" Text="Leave Reason" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtLeaveReason" runat="server" TextMode="MultiLine" Rows="4" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix d--f ai--c">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-b-0 d--f">
                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                            <div id="fileuploader">
                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                    value="Browse" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"
                                            OnClick="btnSaveAttachment_Click" />
                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                    </div>
                                </div>
                                <div class="row clearfix d--f ai--c">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px">
                                            <asp:DataGrid ID="dgvExperienceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objcohDelete">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                    <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                    <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-Font-Size="22px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                        <i class="fas fa-download text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                        Visible="false" />
                                                    <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" ValidationGroup="Save" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_EmployeeExperience.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			//$('input[type=file]').live("click",function(){
			$("body").on("click", 'input[type=file]', function() {
			    return IsValidAttach();
			});
    </script>

</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_AppRejEmployeeData.aspx.vb"
    Inherits="HR_wpg_AppRejEmployeeData" Title="Appove/Reject Employee Data" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script>
$(function() {
  $('.table-responsive').on('shown.bs.dropdown', function(e) {
    var t = $(this),
      m = $(e.target).find('.dropdown-menu'),
      tb = t.offset().top + t.height(),
      mb = m.offset().top + m.outerHeight(true),
      d = 20; // Space for shadow + scrollbar.   
    if (t[0].scrollWidth > t.innerWidth()) {
      if (mb + d > tb) {
        t.css('padding-bottom', ((mb + d) - tb));
      }
    } else {
      t.css('overflow', 'visible');
    }
  }).on('hidden.bs.dropdown', function() {
    $(this).css({
      'padding-bottom': '',
      'overflow': ''
    });
  });
});

</script>


    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Appove/Reject Employee Data"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtApprover" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hfApprover" runat="server" />
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtLevel" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox></div>
                                    </div>
                                    <asp:HiddenField ID="hfLevel" runat="server" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployeeData" runat="server" Text="Select Data" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpEmployeeData" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblSelectOperationType" runat="server" Text="Opration Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpOprationType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlextopr" runat="server" Visible="false" CssClassclass="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEffectivePeriod" runat="server" Text="Period" Font-Bold="true"
                                        CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboEffectivePeriod" runat="server" Width="100%">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:RadioButton ID="radAll" Text="Apply To All" runat="server" GroupName="extopr"
                                        Font-Bold="true" />
                                    <asp:RadioButton ID="radChecked" Text="Apply To Checked" runat="server" GroupName="extopr"
                                        Font-Bold="true" />
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:LinkButton ID="lnkSet" runat="server" Text="[SET]" Font-Bold="true"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtsearch" runat="server" PlaceHolder="Type To Search" CssClass="form-control"
                                        AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 300px">
                                        <asp:GridView ID="gvApproveRejectEmployeeData" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="iCheck,isgrp,employeeunkid,qualificationtranunkid,qualificationgroupunkid,qualificationunkid,dpndtbeneficetranunkid,addresstypeid,isbirthinfo">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="ChkAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ChkSelect" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="ChkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="5px" HeaderStyle-CssClass="headerstyle"
                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <div class="dropdown">
                                                            <asp:LinkButton ID="lnkdownload" runat="server" class="dropbtn"><i class="fa fa-download"></i></asp:LinkButton>
                                                            <div class="dropdown-content">
                                                                <asp:LinkButton runat="server" ID="lnkdownloadall" OnClick="btndownloadAttachment_Click">All Download</asp:LinkButton>
                                                                <asp:LinkButton runat="server" ID="lnkdownloadnew" OnClick="btndownloadAttachment_Click">New</asp:LinkButton>
                                                                <asp:LinkButton runat="server" ID="lnkdownloaddeleted" OnClick="btndownloadAttachment_Click">Delete</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                                                aria-haspopup="true" aria-expanded="true">
                                                                INFO <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                                                <li role="separator" class="divider"></li>
                                                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Separated link</a></li>
                                                            </ul>
                                                        </div>
                                                        <asp:HiddenField ID="hfqualificationtranunkid" runat="server" Value='<%#Eval("qualificationtranunkid")%>' />
                                                        <asp:HiddenField ID="hfnewattachdocumentid" runat="server" Value='<%#Eval("newattachdocumentid")%>' />
                                                        <asp:HiddenField ID="hfdeleteattachdocumentid" runat="server" Value='<%#Eval("deleteattachdocumentid")%>' />
                                                        <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#Eval("employeeunkid")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Skill Category" DataField="scategory" FooterText="dgcolhSkillCategory" />
                                                <asp:BoundField HeaderText="Skill" DataField="skill" FooterText="dgcolhSkill" />
                                                <asp:BoundField HeaderText="Description" DataField="description" FooterText="dgcolhDescription" />
                                                <asp:BoundField HeaderText="Qualification Group" DataField="qualificationgrpname"
                                                    FooterText="dgcolhQualifyGroup" />
                                                <asp:BoundField HeaderText="Qualification" DataField="qualificationname" FooterText="dgcolhQualification" />
                                                <asp:BoundField HeaderText="Award From Date" DataField="award_start_date" FooterText="dgcolhAwardDate" />
                                                <asp:BoundField HeaderText="Award To Date" DataField="award_end_date" FooterText="dgcolhAwardToDate" />
                                                <asp:BoundField HeaderText="Institute" DataField="institute_name" FooterText="dgcolhInstitute" />
                                                <asp:BoundField HeaderText="Ref. No." DataField="reference_no" FooterText="dgcolhRefNo" />
                                                <asp:BoundField HeaderText="Company" DataField="cname" FooterText="dgcolhCompany" />
                                                <asp:BoundField HeaderText="Job" DataField="old_job" FooterText="dgcolhJob" />
                                                <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhStartDate" />
                                                <asp:BoundField HeaderText="End Date" DataField="end_date" FooterText="dgcolhEndDate" />
                                                <asp:BoundField HeaderText="Supervisor" DataField="supervisor" FooterText="dgcolhSupervisor" />
                                                <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhRemark" />
                                                <asp:BoundField HeaderText="Refree Name" DataField="rname" FooterText="dgcolhRefreeName" />
                                                <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                                                <asp:BoundField HeaderText="Id. No." DataField="Company" FooterText="dgcolhIdNo" />
                                                <asp:BoundField HeaderText="Email" DataField="Email" FooterText="dgcolhEmail" />
                                                <asp:BoundField HeaderText="Tel. No." DataField="telephone_no" FooterText="dgcolhTelNo" />
                                                <asp:BoundField HeaderText="Mobile No." DataField="mobile_no" FooterText="dgcolhMobile" />
                                                <asp:BoundField HeaderText="Form Name" DataField="form_name" FooterText="objcolhformname" />
                                                <%--'Gajanan [22-Feb-2019] -- Start--%>
                                                <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                <asp:BoundField HeaderText="Identity Type" DataField="IdType" FooterText="dgcolhIdType" />
                                                <asp:BoundField HeaderText="Serial No." DataField="serial_no" FooterText="dgcolhSrNo" />
                                                <asp:BoundField HeaderText="Identity No." DataField="identity_no" FooterText="dgcolhIdentityNo" />
                                                <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhIdCountry" />
                                                <asp:BoundField HeaderText="Issue Place" DataField="issued_place" FooterText="dgcolhIdIssuePlace" />
                                                <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhIdIssueDate" />
                                                <asp:BoundField HeaderText="Expiry Date" DataField="expiry_date" FooterText="dgcolhIdExpiryDate" />
                                                <asp:BoundField HeaderText="DL Class" DataField="dl_class" FooterText="dgcolhIdDLClass" />
                                                <asp:BoundField HeaderText="Name" DataField="dependantname" FooterText="dgcolhDBName" />
                                                <asp:BoundField HeaderText="Relation" DataField="relation" FooterText="dgcolhDBRelation" />
                                                <asp:BoundField HeaderText="Birth Date" DataField="birthdate" FooterText="dgcolhDBbirthdate" />
                                                <asp:BoundField HeaderText="Identify Number" DataField="identify_no" FooterText="dgcolhDBIdentifyNo" />
                                                <asp:BoundField HeaderText="Gender" DataField="gender" FooterText="dgcolhDBGender" />
                                                <%--'Gajanan [22-Feb-2019] -- End--%>
                                                <%--'Gajanan [18-Mar-2019] -- Start--%>
                                                <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                <asp:BoundField HeaderText="Address Type" DataField="addresstype" FooterText="dgcolhAddressType" />
                                                <asp:BoundField HeaderText="Address1" DataField="address1" FooterText="dgcolhAddress1" />
                                                <asp:BoundField HeaderText="Address2" DataField="address2" FooterText="dgcolhAddress2" />
                                                <asp:BoundField HeaderText="Firstname" DataField="firstname" FooterText="dgcolhFirstname" />
                                                <asp:BoundField HeaderText="Lastname" DataField="lastname" FooterText="dgcolhLastname" />
                                                <asp:BoundField HeaderText="Country" DataField="country" FooterText="dgcolhAddressCountry" />
                                                <asp:BoundField HeaderText="State" DataField="state" FooterText="dgcolhAddressState" />
                                                <asp:BoundField HeaderText="City" DataField="city" FooterText="dgcolhAddressCity" />
                                                <asp:BoundField HeaderText="ZipCode" DataField="zipcode_code" FooterText="dgcolhAddressZipCode" />
                                                <asp:BoundField HeaderText="Province" DataField="provicnce" FooterText="dgcolhAddressProvince" />
                                                <asp:BoundField HeaderText="Road" DataField="road" FooterText="dgcolhAddressRoad" />
                                                <asp:BoundField HeaderText="Estate" DataField="estate" FooterText="dgcolhAddressEstate" />
                                                <asp:BoundField HeaderText="Province1" DataField="provicnce1" FooterText="dgcolhAddressProvince1" />
                                                <asp:BoundField HeaderText="Road1" DataField="Road1" FooterText="dgcolhAddressRoad1" />
                                                <asp:BoundField HeaderText="Chiefdom" DataField="chiefdom" FooterText="dgcolhAddressChiefdom" />
                                                <asp:BoundField HeaderText="Village" DataField="village" FooterText="dgcolhAddressVillage" />
                                                <asp:BoundField HeaderText="Town" DataField="town" FooterText="dgcolhAddressTown" />
                                                <asp:BoundField HeaderText="Mobile" DataField="mobile" FooterText="dgcolhAddressMobile" />
                                                <asp:BoundField HeaderText="Telephone Number" DataField="tel_no" FooterText="dgcolhAddressTel_no" />
                                                <asp:BoundField HeaderText="Plot Number" DataField="plotNo" FooterText="dgcolhAddressPlotNo" />
                                                <asp:BoundField HeaderText="Alternate Number" DataField="alternateno" FooterText="dgcolhAddressAltNo" />
                                                <asp:BoundField HeaderText="Email" DataField="email" FooterText="dgcolhAddressEmail" />
                                                <asp:BoundField HeaderText="Fax" DataField="fax" FooterText="dgcolhAddressFax" />
                                                <%--'Gajanan [18-Mar-2019] -- End--%>
                                                <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                <asp:BoundField HeaderText="Membership Category" DataField="Category" FooterText="dgcolhCategory" />
                                                <asp:BoundField HeaderText="Membership" DataField="membershipname" FooterText="dgcolhmembershipname" />
                                                <asp:BoundField HeaderText="Membership No" DataField="membershipno" FooterText="dgcolhmembershipno" />
                                                <asp:BoundField HeaderText="Eff. Period" DataField="period_name" FooterText="dgcolhperiod_name" />
                                                <asp:BoundField HeaderText="Issue Date" DataField="issue_date" FooterText="dgcolhissue_date" />
                                                <asp:BoundField HeaderText="Start Date" DataField="start_date" FooterText="dgcolhstart_date" />
                                                <asp:BoundField HeaderText="End Date" DataField="expiry_date" FooterText="dgcolhexpiry_date" />
                                                <asp:BoundField HeaderText="Remark" DataField="remark" FooterText="dgcolhMemRemark" />
                                                <%--'S.SANDEEP |15-APR-2019| -- END
                                                    
                                                    <%--'Gajanan [17-April-2019]-- START--%>
                                                <asp:BoundField HeaderText="Birth Country" DataField="country" FooterText="dgcolhBirthCountry" />
                                                <asp:BoundField HeaderText="Birth State" DataField="state" FooterText="dgcolhBirthState" />
                                                <asp:BoundField HeaderText="Birth City" DataField="city" FooterText="dgcolhBirthCity" />
                                                <asp:BoundField HeaderText="Birth Certificate No." DataField="birthcertificateno"
                                                    FooterText="dgcolhBirthCertificateNo" />
                                                <asp:BoundField HeaderText="Birth Town1" DataField="town" FooterText="dgcolhBirthTown1" />
                                                <asp:BoundField HeaderText="Birth Village1" DataField="birth_village" FooterText="dgcolhBirthVillage1" />
                                                <asp:BoundField HeaderText="Birth Ward" DataField="birth_ward" FooterText="dgcolhBirthWard" />
                                                <asp:BoundField HeaderText="Birth Village" DataField="birth_village" FooterText="dgcolhBirthVillage" />
                                                <asp:BoundField HeaderText="Birth Chiefdom" DataField="chiefdom" FooterText="dgcolhBirthChiefdom" />
                                                <asp:BoundField HeaderText="Complexion" DataField="Complexion" FooterText="dgcolhComplexion" />
                                                <asp:BoundField HeaderText="Blood Group" DataField="BloodGroup" FooterText="dgcolhBloodGroup" />
                                                <asp:BoundField HeaderText="Eye Color" DataField="EyeColor" FooterText="dgcolhEyeColor" />
                                                <asp:BoundField HeaderText="Nationality" DataField="Nationality" FooterText="dgcolhNationality" />
                                                <asp:BoundField HeaderText="EthinCity" DataField="Ethnicity" FooterText="dgcolhEthinCity" />
                                                <asp:BoundField HeaderText="Religion" DataField="Religion" FooterText="dgcolhReligion" />
                                                <asp:BoundField HeaderText="Hair" DataField="HairColor" FooterText="dgcolhHair" />
                                                <asp:BoundField HeaderText="Marital Status" DataField="Maritalstatus" FooterText="dgcolhMaritalStatus" />
                                                <asp:BoundField HeaderText="Extra Telephone" DataField="ExtTelephoneno" FooterText="dgcolhExtraTel" />
                                                <asp:BoundField HeaderText="Language1" DataField="language1" FooterText="dgcolhLanguage1" />
                                                <asp:BoundField HeaderText="Language2" DataField="language2" FooterText="dgcolhLanguage2" />
                                                <asp:BoundField HeaderText="Language3" DataField="language3" FooterText="dgcolhLanguage3" />
                                                <asp:BoundField HeaderText="Language4" DataField="language4" FooterText="dgcolhLanguage4" />
                                                <asp:BoundField HeaderText="Height" DataField="height" FooterText="dgcolhHeight" />
                                                <asp:BoundField HeaderText="Weight" DataField="weight" FooterText="dgcolhWeight" />
                                                <asp:BoundField HeaderText="Marital Date" DataField="anniversary_date" FooterText="dgcolhMaritalDate" />
                                                <asp:BoundField HeaderText="Allergies" DataField="Allergies" FooterText="dgcolhAllergies" />
                                                <asp:BoundField HeaderText="Sports/Hobbies" DataField="sports_hobbies" FooterText="dgcolhSportsHobbies" />
                                                <%--'Gajanan [17-April-2019]-- End--%>
                                                <asp:BoundField HeaderText="Inactive Reason" DataField="reason" FooterText="dgcolhInactiveReason" />
                                                <asp:BoundField HeaderText="Void Reason" DataField="voidreason" FooterText="dgcolhVoidReason" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-default" />
                            <asp:Button ID="btnShowMyReport" runat="server" Text="My Report" CssClass="btn btn-default" />
                            <asp:Label ID="lblotherqualificationnote" runat="server" CssClass="text-primary pull-left"
                                Text="Note: This color indicates other qualification"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <uc2:Cnf_YesNo ID="Cnf_WithoutRemark" runat="server" Title="Aruti" />
            <uc3:Pop_report ID="Pop_report" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="gvApproveRejectEmployeeData" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

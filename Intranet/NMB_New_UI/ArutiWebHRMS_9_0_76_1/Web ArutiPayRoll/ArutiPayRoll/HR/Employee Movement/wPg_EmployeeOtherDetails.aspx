﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Home1.master" CodeFile="wPg_EmployeeOtherDetails.aspx.vb"
    Inherits="HR_Employee_Movement_wPg_EmployeeOtherDetails" Title="Cost Center Information" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>
--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Cost Center Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Cost Center Information"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblCaption" runat="server" Text="" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboCCTrnHead" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboChangeReason" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAppointDate" runat="server" Visible="false" CssClass="form-label"
                                            Text="Appoint Date"></asp:Label>
                                        <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 350px">
                                                <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                    Width="99%" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fas fa-exclamation-circle"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="brnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                                        Visible="false"><i class="fas fa-eye text-primary"></i> </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                    </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="EffDate" HeaderText="Effective Date" ReadOnly="true"
                                                            FooterText="dgcolhChangeDate"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DispValue" HeaderText="" ReadOnly="true" FooterText="objdgcolhValue">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CReason" HeaderText="Reason" ReadOnly="true" FooterText="dgcolhReason">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="isfromemployee" HeaderText="objdgcolhFromEmp" ReadOnly="true"
                                                            FooterText="objdgcolhFromEmp" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="cctranheadunkid" HeaderText="objdgcolhdetailtranunkid"
                                                            ReadOnly="true" FooterText="objdgcolhdetailtranunkid" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="adate" HeaderText="objdgcolhAppointDate" ReadOnly="true"
                                                            FooterText="objdgcolhAppointDate" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                            FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                            FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                        <%--12--%>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                        <asp:Label ID="lblRehireEmployeeData" runat="server" Text="Pending Approval" CssClass="label label-warning"></asp:Label>
                                        <asp:Label ID="lblPendingData" runat="server" Text="Parent Detail" CssClass="label label-primary"></asp:Label>
                                    </asp:Panel>
                                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmpWorkPermit.aspx.vb"
    Inherits="HR_wPg_EmpWorkPermit" Title="Work Permit" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="block-header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Work Permit Information"></asp:Label>
                            </h2>
                        </div>
                        <div class="row clearfix">
                            <!-- Task Info -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Work Permit Information"></asp:Label>
                                        </h2>
                                        <ul class="header-dropdown m-r--5">
                                            <li class="dropdown">
                                                <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                                <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblWorkPermitNo" runat="server" Text="Work Permit No." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtWorkPermitNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblIssueCountry" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboIssueCountry" runat="server" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date" CssClass="form-label"></asp:Label>
                                                <uc1:DateCtrl ID="dtIssueDate" runat="server" AutoPostBack="false"></uc1:DateCtrl>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblExpiryDate" runat="server" Text="Expiry Date" CssClass="form-label"></asp:Label>
                                                <uc1:DateCtrl ID="dtExpiryDate" runat="server" AutoPostBack="false"></uc1:DateCtrl>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPlaceOfIssue" runat="server" Text="Issue Place" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtIssuePlace" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboChangeReason" runat="server" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Panel ID="pnlAppointDate" runat="server" Visible="false">
                                                    <asp:Label ID="lblAppointDate" runat="server" Visible="false" Text="Appoint Date"
                                                        CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="height: 350px;">
                                    <asp:DataGrid ID="dgvHistory" runat="server" AllowPaging="false" CssClass="table table-hover table-bordered"
                                        Width="99%">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                    <i class="fas fa-eye text-info"></i> 
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                FooterText="brnEdit">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit"><i class="fas fa-pencil-alt text-primary"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                        CommandName="Delete">
                                                            <i class="fas fa-trash text-danger"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="EffDate" HeaderText="Effective Date" ReadOnly="true"
                                                FooterText="colhChangeDate"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="work_permit_no" HeaderText="Permit No." ReadOnly="true"
                                                FooterText="colhPermitNo"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Country" HeaderText="Country" ReadOnly="true" FooterText="colhCountry">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="issue_place" HeaderText="Issue Place" ReadOnly="true"
                                                FooterText="colhIssuePlace"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="IDate" HeaderText="Issue Date" ReadOnly="true" FooterText="colhIssueDate">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ExDate" HeaderText="Expiry Date" ReadOnly="true" FooterText="colhExpiryDate">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CReason" HeaderText="Reason" ReadOnly="true" FooterText="colhReason">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="workpermittranunkid" Visible="false" HeaderText="PermitTranunkid"
                                                ReadOnly="true" FooterText="colhworkpermittranunkid"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isfromemployee" Visible="false" HeaderText="From Employee"
                                                ReadOnly="true" FooterText="colhfromemployee"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="adate" Visible="false" HeaderText="Appoint Date" ReadOnly="true"
                                                FooterText="colhadate"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                            <%--16--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                    <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" CssClass="label label-warning"></asp:Label>
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary"></asp:Label>
                                </asp:Panel>
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="Personal Info" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgEmployeePersonal.aspx.vb" Inherits="wPgEmployeePersonal" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }
    
   function onlyNum(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cWeight = document.getElementById('<%=txtWeight.ClientID%>').value;
        if (cWeight.length > 0)
            if (charCode == 46)
                if (cWeight.indexOf(".") > 0)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }    
    </script>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblpnl_Header" runat="server" Text="Employee Personal Detail"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <asp:Panel ID="pnl_Employee" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlBirthInfo" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbBirthInfo" runat="server" Text="Birth Info." CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkScanAttachBirthInfo" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblbirthinfoapproval" Visible="false" runat="server" Text="Original Details"
                                                    CssClass="pull-right label label-primary"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBirthCounty" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblState" runat="server" Text="State" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBirthState" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCity" runat="server" Text="City" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBirthCity" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblWard" runat="server" Text="Ward" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBirthWard" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCertificateNo" runat="server" Text="Cert. No." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBirthCertNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblVillage" runat="server" Text="Village" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBirthVillage" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBirthTown" runat="server" Text="Town1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBrithTown" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBirthChiefdom" runat="server" Text="Chiefdom" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBrithChiefdom" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBirthVillage" runat="server" Text="Village1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBrithVillage" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                            <li role="presentation" class="active"><a href="#WorkPermit" data-toggle="tab">
                                                <asp:Label ID="gbWorkPermit" runat="server" Text="Work Permit" CssClass="form-label"></asp:Label>
                                            </a></li>
                                            <li role="presentation"><a href="#ResidentPermit" data-toggle="tab">
                                                <asp:Label ID="lblResidentPermitNo" runat="server" Text="Resident Permit" CssClass="form-label"></asp:Label>
                                            </a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="WorkPermit">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblWorkPermitNo" runat="server" Text="Permit No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtWorkPermitNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtIssueDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblIssueCountry" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboIssueCountry" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpiryDate" runat="server" Text="Expiry Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtExpiryDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPlaceOfIssue" runat="server" Text="Issue Place" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtIssuePlace" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="ResidentPermit">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="Label2" runat="server" Text="Permit No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtResidentPermitNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblResidentIssueDate" runat="server" Text="Issue Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpResidentIssueDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblResidentIssueCountry" runat="server" Text="Country" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboResidentIssueCountry" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblResidentExpiryDate" runat="server" Text="Expiry Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpResidentExpiryDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblResidentIssuePlace" runat="server" Text="Issue Place" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtResidentIssuePlace" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlOtherinfo" runat="server" CssClass="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbOtherInfo" runat="server" Text="Other Info." CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAttachOtherInfo" runat="server" ToolTip="Attach Document">
                                                    <i class="fas fa-paperclip"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                                <asp:Label ID="lblotherinfoapproval" Visible="false" runat="server" Text="Pending Approval"
                                                    CssClass="pull-right label label-primary"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblComplexion" runat="server" Text="Complexion" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboComplexion" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblExtraTel" runat="server" Text="Tel. Ext." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtExTel" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblBloodGroup" runat="server" Text="Blood Group" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboBloodGrp" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblEyeColor" runat="server" Text="Eye Color" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEyeColor" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblLanguage1" runat="server" Text="Language1" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLang1" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblLanguage2" runat="server" Text="Language2" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLang2" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblLanguage3" runat="server" Text="Language3" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLang3" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblLanguage4" runat="server" Text="Language4" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLang4" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblEthinCity" runat="server" Text="Ethnicity" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEthnicity" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblNationality" runat="server" Text="Nationality" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboNationality" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblReligion" runat="server" Text="Religion" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboReligion" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblHair" runat="server" Text="Hair" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboHair" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblHeight" runat="server" Text="Height" CssClass="form-label"></asp:Label>
                                                                <uc9:NumericText id="txtHeight" runat="server" cssclass="form-control" />
                                                            </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblWeight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                                                <uc9:NumericText id="txtWeight" runat="server" cssclass="form-control" />
                                                            </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblMaritalDate" runat="server" Text="Marriage Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtMarriedDate" runat="server" ReadonlyDate="False" />
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblMaritalStatus" runat="server" Text="Marital Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboMaritalStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblAllergies" runat="server" Text="Allergies" CssClass="form-label"></asp:Label>
                                                        <asp:Panel ID="pnlallergies" runat="server" Height="111px" ScrollBars="Auto" CssClass="border">
                                                            <asp:CheckBoxList ID="chkLstAllergies" runat="server" RepeatLayout="Flow" BorderStyle="None">
                                                            </asp:CheckBoxList>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblDisabilities" runat="server" Text="Disabilities" CssClass="form-label"></asp:Label>
                                                        <asp:Panel ID="pnlDisabilities" runat="server" Height="111px" ScrollBars="Auto" CssClass="border">
                                                            <asp:CheckBoxList ID="chkLstDisabilities" runat="server" RepeatLayout="Flow">
                                                            </asp:CheckBoxList>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <asp:Label ID="lblSportsHobbies" runat="server" Text="Sport/Hobbies" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSpHob" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Update" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ScanAttachment">
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                            </h2>
                            </div>
                        <div class="body">
                            <div class="row clearfix d--f ai--c">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-b-0 d--f">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                        <div id="fileuploader">
                                            <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse"
                                                                onclick="return IsValidAttach();" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                        Text="Browse" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="max-height: 350px">
                                        <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-hover"
                                            AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--0--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--1--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <%--2--%>
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                            <%--3--%>
                                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                            <%--4--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="footer">
                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary pull-left" />
                            <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btn btn-default" />
                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                        </div>
                    </asp:Panel>
                    <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                        IsFireButtonNoClick="false" />
                </div>
                <%--S.SANDEEP |26-APR-2019| -- END--%>
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmployeePersonal.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        //$('input[type=file]').live("click", function() {
        $("body").on("click", 'input[type=file]', function() {            
            return IsValidAttach();
        });
        
    </script>

</asp:Content>

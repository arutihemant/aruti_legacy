﻿<%@ Page Title="Employee Membership List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmpMembership_List.aspx.vb" Inherits="wPgEmpMembership_List" %>

<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="ucYesNo" %>
<%--'S.SANDEEP |15-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<%--'S.SANDEEP |15-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Membership List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true"
                                            CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Style="margin-left: 5px" Text="Employee"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipType" runat="server" Text="Membership"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboMembership" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" runat="server" CssClass="btn btn-primary" Text="New" />
                                <asp:Button ID="BtnSearch" runat="server" CssClass="btn btn-default" Text="Search" />
                                <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnCloseForm" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" CssClass="pull-left">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-warning bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 300px">
                                            <asp:DataGrid ID="dgvMembership" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundColumn DataField="ccategory"></asp:BoundColumn>
                                                    <%--0--%>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                    <i class="fas fa-pencil-alt text-info"></i> 
                                                                </asp:LinkButton>
                                                                <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                                <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                                    CommandName="View" Visible="false">
                                                                    <i class="fas fa-eye text-info"></i> 
                                                                </asp:LinkButton>
                                                                <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                                <%--'S.SANDEEP |15-APR-2019| -- END--%>
                                                            </span>
                                                            <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" 
                                                         />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                    <i class="fas fa-trash text-danger"></i> 
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--2--%>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkActiveMem" runat="server" ToolTip="Active Membership" CommandName="Active">
                                                                <i class="fas fa-check-double text-success"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="catagory" HeaderText="Membership Category" ReadOnly="True"
                                                        FooterText="colhMembershipCategory"></asp:BoundColumn>
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="membership" HeaderText="Membership" ReadOnly="True" FooterText="colhMembershipType">
                                                    </asp:BoundColumn>
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="membershipno" HeaderText="Membership No." ReadOnly="True"
                                                        FooterText="colhMembershipNo"></asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="issue_date" HeaderText="Issue Date" ReadOnly="True" FooterText="colhMemIssueDate">
                                                    </asp:BoundColumn>
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="start_date" HeaderText="Start Date" ReadOnly="True" FooterText="colhStartDate">
                                                    </asp:BoundColumn>
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="expiry_date" HeaderText="End Date" ReadOnly="True" FooterText="colhEndDate">
                                                    </asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark">
                                                    </asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="membershiptranunkid" HeaderText="membershiptranunkid"
                                                        Visible="false" FooterText="colhmembershiptranunkid"></asp:BoundColumn>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false"></asp:BoundColumn>
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="ccategoryid" Visible="false"></asp:BoundColumn>
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="membership_categoryunkid" Visible="false"></asp:BoundColumn>
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="membershipunkid" Visible="false"></asp:BoundColumn>
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="emptrnheadid" Visible="false"></asp:BoundColumn>
                                                    <%--16--%>
                                                    <asp:BoundColumn DataField="cotrnheadid" Visible="false"></asp:BoundColumn>
                                                    <%--17--%>
                                                    <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                    <asp:BoundColumn DataField="operationtypeid" HeaderText="operationtypeid" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <%--18--%>
                                                    <asp:BoundColumn DataField="operationtype" HeaderText="Operation Type" ReadOnly="true">
                                                    </asp:BoundColumn>
                                                    <%--19--%>
                                                    <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <%--20--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false">
                                                    </asp:BoundColumn>
                                                    <%--21--%>
                                                    <%--'S.SANDEEP |15-APR-2019| -- START--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                <cc1:ModalPopupExtender ID="popup_active" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnClose" PopupControlID="pnlActiveMembership" TargetControlID="HiddenField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlActiveMembership" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btnOk">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Assign Head" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <asp:Label ID="lblActMembership" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtAMembership" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <asp:Label ID="lblEmpHead" runat="server" Text="Emp. Head" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtEmpHead" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <asp:Label ID="lblCoHead" runat="server" Text="Co. Head" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <div class="form-line">
                                    <asp:TextBox ID="txtCoHead" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <asp:Label ID="lblEffctivePeriod" runat="server" Text="Effective Period" CssClass="form-label"></asp:Label>
                            <div class="form-group">
                                <asp:DropDownList ID="cboEffectivePeriod" runat="server" ReadOnly="true" data-live-search="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:CheckBox ID="chkCopyPreviousEDSlab" runat="server" Text="Copy Previous Slab"
                                    CssClass="filled-in" />
                                <asp:CheckBox ID="chkOverwriteHeads" runat="server" Text="Overwrite Heads" CssClass="filled-in" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnOk" runat="server" Text="Ok" CssClass="btn btn-primary" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-Default" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </div>
                </asp:Panel>
                <ucYesNo:ConfirmYesNo ID="popup_yesno" runat="server" />
                <%--'S.SANDEEP |15-APR-2019| -- START--%>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <%--'S.SANDEEP |15-APR-2019| -- END--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

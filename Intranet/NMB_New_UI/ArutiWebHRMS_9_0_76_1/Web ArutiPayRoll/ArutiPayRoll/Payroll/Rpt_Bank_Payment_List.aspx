﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Bank_Payment_List.aspx.vb"
    Inherits="Payroll_Rpt_Bank_Payment_List" Title="Bank Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Bank Payment List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboReportType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeeName" runat="server" Text="Emp.Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployeeName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyBankName" runat="server" Text="Company Bank Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyBankName" runat="server"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyBranchName" runat="server" Text="Company Bank Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyBranchName" runat="server"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCompanyAccountNo" runat="server" Text="Company Account No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCompanyAccountNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportMode" runat="server" Text="Report Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboReportMode" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChequeNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBankName" runat="server" Text="Emp.Bank Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboBankName" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranchName" runat="server" Text="Branch Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboBranchName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCountryName" runat="server" Text="Country Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCountry" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Paid Currency" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboCurrency" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlOtherSetting" runat="server" Visible="false">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPresentDays" runat="server" Text="Present Days" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboPresentDays" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblBasicSalary" runat="server" Text="Basic Salary" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboBasicSalary" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblSocialSecurity" runat="server" Text="Social Security" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboSocialSecurity" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblExtraIncome" runat="server" Text="Extra Income" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboExtraIncome" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAbsentDays" runat="server" Text="Absent Deduction" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboAbsentDays" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblIdentityType" runat="server" Text="Identity Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtIdentityType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPaymentType" runat="server" Text="Payment Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCustomCurrFormat" runat="server" Text="Custom Curr. Format" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtCustomCurrFormat" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAmountTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCutOffAmount" runat="server" Text="Cut Off Amount" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCutOffAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPostingDate" runat="server" Text="Posting Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpPostingDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMembershipRepo" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboMembershipRepo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory1" runat="server" Text="Show Signatory 1" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory" runat="server" Text="Show Defined Signatory"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory2" runat="server" Text="Show Signatory 2" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory2" runat="server" Text="Show Defined Signatory 2"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSignatory3" runat="server" Text="Show Signatory 3" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkDefinedSignatory3" runat="server" Text="Show Defined Signatory 3"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowGroupByBankBranch" runat="server" Text="Show Group By Bank/Branch"
                                            AutoPostBack="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowBankCode" runat="server" Text="Show Bank Code" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkEmployeeSign" runat="server" Text="Show Employee Sign" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowBranchCode" runat="server" Text="Show Branch Code" CssClass="filled-in">
                                        </asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowFNameSeparately" runat="server" Text="Show First Name,Other Name and Surname in Separate Columns"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPayrollPeriod" runat="server" Text="Show Payroll Period"
                                            Checked="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkSaveAsTXT_WPS" runat="server" Text="Save As TXT" CssClass="filled-in"
                                            Checked="true"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSortCode" runat="server" Text="Show Sort Code" Checked="true"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowAccountType" runat="server" Text="Show Account Type" Checked="false"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmployeeCode" runat="server" Text="Show Employee Code" Checked="true"
                                            AutoPostBack="true" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowReportHeader" runat="server" Text="Show Report Header" Checked="true"
                                            CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkLetterhead" runat="server" Text="Letterhead" Checked="false"
                                            CssClass="filled-in" AutoPostBack="false"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkAddresstoEmployeeBank" runat="server" Text="Address to Employee Bank"
                                            Checked="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSelectedBankInfo" runat="server" Text="Show Selected Bank Info"
                                            Checked="false" AutoPostBack="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowCompanyGrpInfo" runat="server" Text="Show Company Group Info"
                                            Checked="false" CssClass="filled-in"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkEFTCityBankExport" runat="server" Text="EFT City Direct Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_CBA_Export" runat="server" Text="EFT CBA Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_EXIM_Export" runat="server" Text="EFT EXIM Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_Custom_CSV_Export" runat="server" Text="EFT Custom CSV Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_Custom_XLS_Export" runat="server" Text="EFT Custom XLS Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_FlexCubeRetailGEFU_Export" runat="server" Text="EFT Flex Cube Retail - GEFU Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_ECO_Bank" runat="server" Text="EFT ECO Bank..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkBankPaymentLetter" runat="server" Text="Bank Payment Letter..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTBarclaysBankExport" runat="server" Text="Barclays Bank Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankMalawi" runat="server" Text="EFT National Bank Malawi..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_FNB_Bank_Export" runat="server" Text="EFT FNB Bank Export..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTStandardCharteredBank_S2B" runat="server" Text="EFT Standard Chartered Bank(S2B)..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFT_ABSA_Bank" runat="server" Text="EFT ABSA Bank..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankMalawiXLSX" runat="server" Text="EFT National Bank Malawi XLSX..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTEquityBankKenya" runat="server" Text="EFT Equity Bank Kenya..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTNationalBankKenya" runat="server" Text="EFT National Bank Kenya..."></asp:LinkButton>
                                        <asp:LinkButton ID="lnkEFTCitiBankKenya" runat="server" Text="EFT City Bank Kenya..."></asp:LinkButton>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:LinkButton ID="lnkMobileMoneyEFTMPesaExport" runat="server" Text="EFT MPesa Export..."></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_EFTCustom" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnEFTClose" PopupControlID="pnl_EFTCustom" TargetControlID="HiddenField1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_EFTCustom" runat="server" Style="display: none;
                    width: 450px">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblTitle" runat="server" Text="EFT Custom Columns Export" />
                                    </h2>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height:200px; overflow:auto;" >
                                                <asp:GridView ID="lvEFTCustomColumns" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="False" Width="99%" CssClass="table table-hover table-bordered" DataKeyNames="ID">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="objcolhNCheck">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" ToolTip="All"
                                                                    OnCheckedChanged="lvEFTCustomColumns_ItemChecked" Text=" " CssClass="filled-in" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    AutoPostBack="true" ToolTip="Checked" OnCheckedChanged="lvEFTCustomColumns_ItemChecked"
                                                                    CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="EFT Custom Columns" ReadOnly="true"
                                                            FooterText="colhEFTCustomColumns" />
                                                        <asp:BoundField DataField="ID" ReadOnly="true" Visible="false" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                    Text="&#x25B2;" OnClick="ChangeLocation" />
                                                                <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                    Text="&#x25BC;" OnClick="ChangeLocation" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkShowColumnHeader" runat="server" Text="Show Column Header on Report"
                                                CssClass="filled-in" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblMembership" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboMembership" runat="server" AutoPostBack="false"
                                                    Width="250px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDateFormat" runat="server" Text="Date Format" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDateFormat" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkSaveAsTXT" runat="server" Text="Save As TXT" CssClass="filled-in" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkTABDelimiter" runat="server" Text="TAB Delimiter" CssClass="filled-in" />
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnEFTSaveSelection" runat="server" Text="Save Selection" CssClass="btn btn-default" />
                                    <asp:Button ID="btnEFTOK" runat="server" Text="OK" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnEFTClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

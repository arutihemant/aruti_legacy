﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Cash_Payment_List.aspx.vb"
    Inherits="Payroll_Rpt_Cash_Payment_List" Title="Cash Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="block-header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Cash Payment List" CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployeeName" runat="server" Text="Emp.Name" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboEmployeeName" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmpCode" runat="server" Text="Emp.Code" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtEmpcode" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                            Text="0" CssClass="form-control"></asp:TextBox></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblAmountTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                            Text="0" CssClass="form-control"></asp:TextBox></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblType" runat="server" Text="NetPay/Adv." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboPayAdvance" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboBranch" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Paid Currency" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboCurrency" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkShowSign" runat="server" Text="Show Signature"></asp:CheckBox>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblReportMode" runat="server" Text="Report Mode" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList data-live-search="true" ID="cboReportMode" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee">
                                                </asp:CheckBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

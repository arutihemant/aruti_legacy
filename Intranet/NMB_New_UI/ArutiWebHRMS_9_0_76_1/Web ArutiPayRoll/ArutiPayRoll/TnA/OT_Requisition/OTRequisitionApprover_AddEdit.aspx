﻿<%@ Page Title="Add/ Edit OT Approver" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="OTRequisitionApprover_AddEdit.aspx.vb" Inherits="TnA_OT_Requisition_OTRequisitionApprover_AddEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">



        $("body").on("click", "[id*=ChkAllAddEditEmployee]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectAddEditEmployee]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkSelectAddEditEmployee]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllAddEditEmployee]", grid);
            debugger;
            if ($("[id*=ChkSelectAddEditEmployee]", grid).length == $("[id*=ChkSelectAddEditEmployee]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $("body").on("click", "[id*=ChkAllSelectedEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkSelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            debugger;
            if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        function FromSearching() {
            if ($('#txtSearchEmployee').val().length > 0) {
                $('#<%= gvAddEditEmployee.ClientID %> tbody tr').hide();
                $('#<%= gvAddEditEmployee.ClientID %> tbody tr:first').show();
                $('#<%= gvAddEditEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmployee').val() + '\')').parent().show();
            }
            else if ($('#txtSearchEmployee').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= gvAddEditEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearchEmployee').val('');
            $('#<%= gvAddEditEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchEmployee').focus();
        }


        function ToSearching() {
            if ($('#txtAssignedEmpSearch').val().length > 0) {
                $('#<%= GvSelectedEmployee.ClientID %> tbody tr').hide();
                $('#<%= GvSelectedEmployee.ClientID %> tbody tr:first').show();
                $('#<%= GvSelectedEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtAssignedEmpSearch').val() + '\')').parent().show();
            }
            else if ($('#txtAssignedEmpSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= GvSelectedEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtAssignedEmpSearch').val('');
            $('#<%= GvSelectedEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtAssignedEmpSearch').focus();
        }
        
    
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <asp:Label ID="lblPageHeader1" runat="server" Text="Add/ Edit OT Approver"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblApproverInfo" runat="server" Text="Approvers Info" />
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkAddEditExtApprover" runat="server" AutoPostBack="true" CssClass="filled-in"
                                                            Text="Make External Approver" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproverName" runat="server" Text="Name" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="drpAddEditApprover" runat="server"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApproverlevel" runat="server" Text="Level" CssClass="form-label" />
                                                        <div class="form-group">
                                                            <asp:DropDownList data-live-search="true" ID="drpAddEditApproverLevel" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <asp:Panel ID="PnlApproverUser" runat="server">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblApproverUser" runat="server" Text="User" CssClass="form-label" />
                                                            <div class="form-group">
                                                                <asp:DropDownList data-live-search="true" ID="drpAddEditUser" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkHodCapForOT" runat="server" AutoPostBack="False" CssClass="filled-in"
                                                            Text="HOD Cap for OT" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 p-l-0">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <%-- <asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>--%>
                                                                    <input type="text" id="txtSearchEmployee" name="txtSearch" placeholder="Type To Search Text"
                                                                        maxlength="50" onkeyup="FromSearching();" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15 p-l-0">
                                                            <ul class="header-dropdown">
                                                                    <asp:LinkButton ID="lnkAllocation" runat="server"  ToolTip = "Allocation">
                                                                                  <i class="fas fa-sliders-h"></i>
                                                                    </asp:LinkButton>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15  p-l-0">
                                                            <ul class="header-dropdown">
                                                                    <asp:LinkButton ID="lnkReset" runat="server"  ToolTip = "Reset">
                                                                                   <i class="fas fa-sync-alt"></i>
                                                                    </asp:LinkButton>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 300px">
                                                            <asp:GridView ID="gvAddEditEmployee" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                DataKeyNames="employeeunkid,employeecode" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Employee Code" DataField="employeecode" FooterText="dgcolhEcode" />
                                                                    <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEname" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAddEmployee" runat="server" Text="Add" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="LblOTAddEditAssignedEmp" runat="server" Text="Selected Employee" Font-Bold="true" />
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <%--<asp:TextBox ID="txtAssignedEmpSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>--%>
                                                                <input type="text" id="txtAssignedEmpSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                    maxlength="50" onkeyup="ToSearching();" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 605px">
                                                            <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="false"
                                                                CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="employeeunkid,tnamappingunkid">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Employee" DataField="ename" FooterText="dgcolhEmpName" />
                                                                    <asp:BoundField HeaderText="Department" DataField="edept" FooterText="dgcolhEDept" />
                                                                    <asp:BoundField HeaderText="Job" DataField="ejob" FooterText="dgcolhEJob" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnDeleteEmployee" runat="server" Text="Delete" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnpopupsave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnpopupclose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                <uc9:ConfirmYesNo ID="confirmationSelectedEmp" runat="server" />
                <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

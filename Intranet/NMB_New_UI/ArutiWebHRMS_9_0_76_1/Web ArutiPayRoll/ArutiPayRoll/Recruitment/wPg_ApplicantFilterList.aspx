﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_ApplicantFilterList.aspx.vb"
    Inherits="wPg_ApplicantFilterList" MasterPageFile="~/Home1.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">

    function closePopup() {
        $('#imgclose').click(function(evt) 
        {
            $find('<%= popupApproveDisapprove.ClientID %>').hide();        
        });
    }

function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }


//        var cnt = 1;
//        while (cnt <= 3) 
//        {
//            var imgIdRec = "imgAcc" + cnt;

//            if (document.getElementById(imgIdRec) != null && imgIdRec != imgID) 
//            {
//                document.getElementById(imgIdRec).src = URL + "images/plus.png";
//            }
//            cnt = cnt + 1;
//        }        
        
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
	 if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
          
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupApproveDisapprove" runat="server" TargetControlID="HiddenField1"
                    PopupControlID="pnlApproveDisapprove" BackgroundCssClass="modal-backdrop" DropShadow="false"
                    CancelControlID="btnApproveDisapproveClose" Drag="true">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlApproveDisapprove" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <uc4:Confirmation ID="popConfirm" runat="server" />
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="frmApplicantFilter_Approval" runat="server" Text="Approve/Disapprove Applicant Filter"
                                CssClass="form-label"></asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body" style="max-height:525px;">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="inner-card">
                                            <div class="header">
                                                <div style="float: left;" onclick="ChangeApplicantFilterImage('img1','dvApplicantFilter');">
                                                    <img id="img1" src="../images/plus.png" alt="" />
                                                </div>
                                                <asp:Label ID="gbCriteria" runat="server" Text="Shortlisting Criteria" CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="dvApplicantFilter" class="table-responsive" style="max-height: 200px;">
                                                            <asp:GridView ID="dgvShortListCriteria" runat="server" AutoGenerateColumns="False"
                                                                ShowFooter="false" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%"
                                                                CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="colhRefNo" />
                                                                    <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhSCVacancy" />
                                                                    <asp:BoundField DataField="qlevel" HeaderText="Ql.Grp Level" FooterText="colhQlGrpLevel" />
                                                                    <asp:BoundField DataField="qualificationgroup" HeaderText="Qul. Group" FooterText="colhQlGrp" />
                                                                    <asp:BoundField DataField="qualification" HeaderText="Qualification" FooterText="colhdgQualification" />
                                                                    <asp:BoundField DataField="resultname" HeaderText="Result Code" FooterText="colhResultCode" />
                                                                    <asp:BoundField DataField="resultlevel" HeaderText="Result Level" FooterText="colhRLevel" />
                                                                    <asp:BoundField DataField="ResultLvl_Condition" HeaderText="Result Level Condition"
                                                                        FooterText="colhRCondition" />
                                                                    <asp:BoundField DataField="gpacode" HeaderText="GPA" FooterText="colhGPA" />
                                                                    <asp:BoundField DataField="gpacode_condition" HeaderText="GPA Condition" FooterText="colhGPACondition" />
                                                                    <asp:BoundField DataField="age" HeaderText="Age" FooterText="colhAge" HeaderStyle-Width="50px"
                                                                        ItemStyle-Width="50px" />
                                                                    <asp:BoundField DataField="agecondition" HeaderText="Age Condition" FooterText="colhAgeCondition" />
                                                                    <asp:BoundField DataField="award_year" HeaderText="Awarded Year" FooterText="colhAwardYear" />
                                                                    <asp:BoundField DataField="year_condition" HeaderText="Year Condition" FooterText="colhYearCondition" />
                                                                    <asp:BoundField DataField="gender_name" HeaderText="Gender" FooterText="colhGender" />
                                                                    <asp:BoundField DataField="skill_category" HeaderText="SKill Category" FooterText="colhFilterSkillCategory" />
                                                                    <asp:BoundField DataField="skill" HeaderText="Skill" FooterText="colhFilterSkill" />
                                                                    <asp:BoundField DataField="other_qualificationgrp" HeaderText="Other Qul. Group"
                                                                        FooterText="colhFilterOtherQuliGrp" />
                                                                    <asp:BoundField DataField="other_qualification" HeaderText="Other Qualification"
                                                                        FooterText="colhFilterOtherQualification" />
                                                                    <asp:BoundField DataField="other_resultcode" HeaderText="Other Result Code" FooterText="colhFilterOtherResultCode" />
                                                                    <asp:BoundField DataField="other_skillcategory" HeaderText="Other Skill Category"
                                                                        FooterText="colhFilterOtherSkillCategory" />
                                                                    <asp:BoundField DataField="other_skill" HeaderText="Other Skill" FooterText="colhFilterOtherSkill" />
                                                                    <asp:BoundField DataField="nationality" HeaderText="Nationality" FooterText="colhNationality" />
                                                                    <asp:BoundField DataField="experience_days" HeaderText="Experience (Days)" FooterText="colhExperience" />
                                                                    <asp:BoundField DataField="experiencecondition" HeaderText="Experience Condition"
                                                                        FooterText="colhExperienceCond" />
                                                                    <asp:BoundField DataField="logicalcondition" HeaderText="Logical Condition" FooterText="colhLogicalCondition" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="inner-card">
                                            <div class="header">
                                                <div style="float: left;" onclick="ChangeApplicantFilterImage('img2','dvData');">
                                                    <img id="img2" src="../images/plus.png" alt="" />
                                                </div>
                                                <asp:Label ID="gbApplicantInfo" runat="server" Text="Applicant Info" CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="dvData" class="table-responsive" style="max-height: 285px;">
                                                            <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="100%" CssClass="table table-hover table-bordered"
                                                                RowStyle-Wrap="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="dgcolhRefNo" />
                                                                    <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhVacancy" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="inner-card">
                                            <div class="header">
                                                <div style="float: left;" onclick="ChangeApplicantFilterImage('img3','dvRemark');">
                                                    <img id="img3" src="../images/plus.png" alt="" />
                                                </div>
                                                <asp:Label ID="gbRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="dvRemark">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtRemark" runat="server" Rows="4" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReject" runat="server" Text="Disapprove" CssClass="btn btn-default" />
                                <asp:Button ID="btnApproveDisapproveClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="HiddenField1" runat="Server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Applicant Filter List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblVacancyType" runat="server" Text="Vacancy Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacancyType" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefNo" runat="server" Text="Reference No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboRefNo" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEnddate" runat="server" Text="Vac. End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateControl ID="dtEndDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVacancy" runat="server" Text="Vacancy" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboVacancy" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartdate" runat="server" Text="Vac. Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateControl ID="dtpStartdate" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnOperation" runat="server" CssClass="btn btn-default" Text="Operation" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 235px;">
                                            <asp:GridView ID="dgvApplicant" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                RowStyle-Wrap="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged"
                                                                Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                                Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="dgcolhRefNo" />
                                                    <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhVacancy" />
                                                    <asp:BoundField DataField="Status" HeaderText="Status" FooterText="dgcolhStatus" />
                                                    <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

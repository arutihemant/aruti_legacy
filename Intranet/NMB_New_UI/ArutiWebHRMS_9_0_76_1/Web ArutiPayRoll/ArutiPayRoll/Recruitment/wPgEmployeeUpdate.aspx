﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmployeeUpdate.aspx.vb"
    Inherits="Recruitment_wPgEmployeeUpdate" Title="Update Detail Links" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f ai--c jc--c">
                    <div class="col-xs-12 col-sm-9">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblpnl_Header" runat="server" Text="Update Detail Links"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <asp:LinkButton ID="lnkMyQualification" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Qualifications</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-graduate col-green"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMySkills" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Skills</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-cog col-pink"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMyExperiences" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Experiences</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-tie col-blue"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkMyReferences" runat="server" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">References</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-users col-teal"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="row clearfix">
                                    <asp:LinkButton ID="lnkMyProfile" runat="server" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">View Profile</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-address-card col-purple"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAddress" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Address</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-id-card-alt col-orange"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkPersonal" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Personal</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-clipboard-list col-blue-grey"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkAttachment" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">Attachment</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-paperclip col-green"></i>
                                        </div>
                                    </div>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

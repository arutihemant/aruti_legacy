﻿<%@ Page Title="Talent Setups" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_TalentSettings.aspx.vb" Inherits="wPg_TalentSettings" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<%@ Register Src="~/Controls/ColorPickerTextbox.ascx" TagName="ColorPickerTextbox"
    TagPrefix="colt" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
        }
    </script>

    <script type="text/javascript">

    $("body").on("click", "[id*=chkAllSelect]", function() {
        var chkHeader = $(this);
        debugger;
        var grid = $(this).closest("table");
        $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
    });

    $("body").on("click", "[id*=chkSelect]", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkAllSelect]", grid);
        debugger;
        if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Talent Setups"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <ul class="nav nav-tabs tab-nav-right" role="tablist" id="Tabs">
                                    <li role="presentation" class="active"><a href="#CycleSetup" data-toggle="tab">
                                        <asp:Label ID="lblTalentCycle" runat="server" Text="Cycle Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#StagingSetup" data-toggle="tab">
                                        <asp:Label ID="lblStaging" runat="server" Text="Staging Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#QualifyingCriteriaSetup" data-toggle="tab">
                                        <asp:Label ID="lblQualifyingCriteria" runat="server" Text="Qualifying Criteria Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#ScreenersSetup" data-toggle="tab">
                                        <asp:Label ID="lblScreeners" runat="server" Text="Screeners Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#QuestionnaireSetup" data-toggle="tab">
                                        <asp:Label ID="lblQuestinnaire" runat="server" Text="Questionnaire Setup"></asp:Label>
                                    </a></li>
                                    <li role="presentation"><a href="#RatingsSetup" data-toggle="tab">
                                        <asp:Label ID="lblRatings" runat="server" Text="Ratings Setup"></asp:Label>
                                    </a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content p-b-0">
                                    <div role="tabpanel" class="tab-pane fade in active" id="CycleSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblAddEditCycle" runat="server" Text="Add/Edit Talent Cycle"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblFinYear" Text="Financial Year" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpFinYear" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCode" Text="Code" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtCode" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblName" Text="Name" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblStartDate" Text="Start Date" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <uc2:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="false" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEndDate" Text="End Date" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <uc2:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnCycleAdd" CssClass="btn btn-primary" runat="server" Text="Add" />
                                                        <asp:Button ID="btnCycleReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="height: 350px">
                                                                    <asp:GridView ID="gvCycle" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="cycleunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                        <asp:LinkButton ID="t1Edit" runat="server" ToolTip="Select" OnClick="lnkCEdit_Click">
                                                                                                    <i class="fas fa-pencil-alt text-primary" ></i>
                                                                                        </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="t1Delete" runat="server" ToolTip="Delete" OnClick="lnkCDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="Year" HeaderText="Year" ReadOnly="True" FooterText="colhYear">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" FooterText="colhCode">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" FooterText="colhName">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="sdate" HeaderText="Start Date" ReadOnly="True" FooterText="colhSDate">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="edate" HeaderText="End Date" ReadOnly="True" FooterText="colhEDate">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="status" HeaderText="Status" ReadOnly="True" FooterText="colhStatus">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="StagingSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblStages" runat="server" Text="Add/Edit Stages"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSPeriod" Text="Period" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpSPeriod" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblStageName" Text="Stage Name" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtStageName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="display: none">
                                                                <asp:Label ID="lblFlowOrder" Text="Order" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpFlow" runat="server" Enabled="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblPStage" Text="Stage" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpPStage" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnSAdd" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                        <asp:Button ID="btnSReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvobjStaging" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="stageunkid,cycleunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                        <asp:LinkButton ID="t1Edit" runat="server" ToolTip="Select" OnClick="lnkSEdit_Click">
                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="t1Delete" runat="server" ToolTip="Delete" OnClick="lnkSDelete_Click">
                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="period" HeaderText="Period" ReadOnly="True" FooterText="colhSPeriod">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="stagename" HeaderText="Stage Name" ReadOnly="True" FooterText="colhStageName">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="floworder" HeaderText="Order" ReadOnly="True" FooterText="colhFlowOrder">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="QualifyingCriteriaSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblQCriteria" runat="server" Text="Add/Edit Qualifying Criteria Setup"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="header">
                                                                                <h2>
                                                                                    <asp:Label ID="lblQPeriodMapped" Text="Period" runat="server" />
                                                                                </h2>
                                                                            </div>
                                                                            <div class="body">
                                                                                <div class="form-group">
                                                                                    <asp:DropDownList ID="drpQMPeriod" runat="server" AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="header">
                                                                                <h2>
                                                                                    <asp:Label ID="lblInstruction" Text="Instruction" runat="server" />
                                                                                </h2>
                                                                            </div>
                                                                            <div class="body">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="txtInstruction" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                                            Rows="3"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="header">
                                                                                <h2>
                                                                                    <asp:CheckBox ID="chkPerformanceScore" runat="server" Text="Performace Score" AutoPostBack="true" />
                                                                                </h2>
                                                                            </div>
                                                                            <asp:Panel ID="pnlPerformanceScore" runat="server" CssClass="body" Enabled="false">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:Label ID="lblminscore" runat="server" CssClass="form-label" Text="Minumum Score"></asp:Label>
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtMinPSValue" runat="server" CssClass="form-control" Enabled="false" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <asp:Label ID="Label2" runat="server" CssClass="form-label" Text="Apply the Minimum Score in"></asp:Label>
                                                                                        <asp:RadioButton ID="rdbanyPeriod" runat="server" Text="Any of the Performance Period"
                                                                                            AutoPostBack="true" GroupName="PerformancePeriod" />
                                                                                        <br />
                                                                                        <asp:RadioButton ID="rdballPeriod" runat="server" Text="All of the Performance Period"
                                                                                            AutoPostBack="true" GroupName="PerformancePeriod" />
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card">
                                                                            <div class="header">
                                                                                <h2>
                                                                                    <asp:CheckBox ID="chkAllocationBy" runat="server" Text="Allocation By" AutoPostBack="true" />
                                                                                </h2>
                                                                            </div>
                                                                            <asp:Panel ID="pnlAllocation" runat="server" CssClass="body" Enabled="false">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <div class="form-line">
                                                                                                <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control" placeholder="Search Here"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <asp:DropDownList ID="drpQAllocBy" runat="server" AutoPostBack="true">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                        <div class="table-responsive" style="max-height: 400px">
                                                                                            <asp:GridView ID="gvQAllocBy" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                                                DataKeyNames="Id" AllowPaging="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                                        ItemStyle-Width="25px" HeaderStyle-Width="25px">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                                                                                        FooterText="objdgcolhId" />
                                                                                                    <asp:BoundField DataField="Name" HeaderText="Report By" FooterText="dgcolhReportBy"
                                                                                                        ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <div class="card inner-card m-b-0">
                                                                            <div class="body">
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:Label ID="lblQPeriodNo" runat="server" Text="Number of Performance Periods to Consider"></asp:Label>
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQPrdNo" runat="server" CssClass="form-control" Min="0" Text="0"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:CheckBox ID="chkQMaxAge" runat="server" Text="Max Age as of start of Talent Period"
                                                                                                AutoPostBack="true" />
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQMaxAgeNo" runat="server" Enabled="false" Min="0" Max="120" Text="0"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:CheckBox ID="chkOrgYearNo" runat="server" Text="Number of Years with Organization"
                                                                                                AutoPostBack="true" />
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQOrgYrNo" runat="server" Text="0" Min="0" Max="100" CssClass="form-control" Enabled="false" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:CheckBox ID="chkQMaxScreeners" runat="server" Text="Maximum Number Of Screeners"
                                                                                                AutoPostBack="true" />
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtMaxQScreener" runat="server" CssClass="form-control" Min="0" Enabled="false" Text="0"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:CheckBox ID="chkQMinScrReq" runat="server" Text="Minimum Number Of Screening Required"
                                                                                                AutoPostBack="true" />
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQMinScrReq" runat="server" Min="0" Text="0" Enabled="false" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:CheckBox ID="chkQTotalQueWeight" runat="server" Text="Total Question Weight"
                                                                                                AutoPostBack="true" />
                                                                                        </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQTotalQueWeight" Min="0" Text="0" runat="server" Enabled="false" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                <div class="row clearfix">
                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 d--f ai--c">
                                                                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 p-l-0">
                                                                                            <asp:Label ID="lblQTotalDataDisplayPerStage" runat="server" Text="Max count display per stage"></asp:Label>
                                                                            </div>
                                                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-r-0">
                                                                                            <nut:NumericText ID="txtQTotalDataDisplayPerStage" Min="1" Max="300" Text="50" runat="server" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                    </div>
                                                                                </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <asp:Panel ID="pnlQualifyFooter" CssClass="footer" runat="server">
                                                        <asp:Button ID="btnQualifySave" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="ScreenersSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblScsSetupTitle" runat="server" Text="Add/Edit Screener"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblScreenerPeriod" Text="Period" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <%--'Pinkal (12-Dec-2020) -- Start
                                                                    'Enhancement  -  Working on Talent Issue which is given by Andrew.--%>
                                                                    <asp:DropDownList ID="drpScreenerPeriod" runat="server" AutoPostBack="true">
                                                                   <%--  'Pinkal (12-Dec-2020) -- End--%>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblscreenerUser" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                                                <asp:DropDownList ID="drpscreenerUser" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 300px">
                                                                    <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="UserAccess" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnAddScreener" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblScsSetupTitle2" runat="server" Text="Screener List"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvScreener" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="screenermstunkid,isactive,cycleunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkt1ScreenerDelete" runat="server" ToolTip="Delete" OnClick="lnkt1ScreenerDelete_Click">
                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="t1ScreenerActive" runat="server" ToolTip="Active" OnClick="lnkt1ScreenerActive_Click">
                                                        <i class="fas fa-user-check text-success"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="t1ScreenerInActive" runat="server" ToolTip="Inactive" OnClick="lnkt1ScreenerInActive_Click">
                                                        <i class="fas fa-user-times text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" FooterText="colhName">
                                                                            </asp:BoundField>
                                                                        <asp:BoundField DataField="period" HeaderText="Period" ReadOnly="True" FooterText="colhScreenerPeriod">
                                                                        </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="QuestionnaireSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblDefineQuestions" runat="server" Text="Add/Edit Questionnaire"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQPeriod" Text="Period" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                 <%--'Pinkal (12-Dec-2020) -- Start
                                                                    'Enhancement  -  Working on Talent Issue which is given by Andrew.--%>
                                                                    <asp:DropDownList ID="drpQPeriod" runat="server" AutoPostBack="true">
                                                                      <%--'Pinkal (12-Dec-2020) -- End --%>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQWeight" Text="Weight" runat="server" CssClass="form-lable" />
                                                                <nut:NumericText ID="txtQWeight" runat="server" Type="Point" Max="9999" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQuestion" Text="Question" runat="server" CssClass="form-lable" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtQuestion" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="1"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnQuestionAdd" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    <asp:Button ID="btnQuestionReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvobjQuestionnaire" runat="server" AutoGenerateColumns="false"
                                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="questionnaireunkid,cycleunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="t1Edit" runat="server" ToolTip="Select" OnClick="lnkQEdit_Click">
                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="t1Delete" runat="server" ToolTip="Delete" OnClick="lnkQDelete_Click">
                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="period" HeaderText="Period" ReadOnly="True" FooterText="colhQPeriod">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="question" HeaderText="Question" ReadOnly="True" FooterText="colhQuestion">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="weight" HeaderText="weight" ReadOnly="True" FooterText="colhQWgt">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="RatingsSetup">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblRSetups" runat="server" Text="Add/Edit Ratings"></asp:Label></b>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblRPeriod" Text="Period" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpRPeriod" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblScrFrom" Text="Score From" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtScrF" runat="server" CssClass="form-control" Type="Point" />
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblScrTo" Text="Score To" runat="server" CssClass="form-label" />
                                                                <nut:NumericText ID="txtScrT" runat="server" CssClass="form-control" Type="Point" />
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblRActionColor" Text="Color" runat="server" CssClass="form-label" />
                                                                <colt:ColorPickerTextbox ID="txtRActionColor" runat="server" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblRAction" Text="Action" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="drpRStage" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                                <asp:Label ID="lblRDescription" Text="Description" runat="server" CssClass="form-label" />
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtRDescription" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnRAdd" CssClass="btn btn-primary" runat="server" Text="Save" />
                                                    <asp:Button ID="btnRReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="table-responsive" style="max-height: 350px;">
                                                                    <asp:GridView ID="gvobjRatings" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                                        AllowPaging="false" DataKeyNames="ratingunkid,cycleunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="t1Edit" runat="server" ToolTip="Select" OnClick="lnkREdit_Click">
                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                                        </asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="t1Delete" runat="server" ToolTip="Delete" OnClick="lnkRDelete_Click">
                                                        <i class="fas fa-trash text-danger"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="period" HeaderText="Period" ReadOnly="True" FooterText="colhRPeriod">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="scorefrom" HeaderText="Score From" ReadOnly="True" FooterText="colhScoreFrom">
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="scoreto" HeaderText="Score To" ReadOnly="True" FooterText="colhScoreTo">
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Color"
                                                                                FooterText="colhColor">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="objLblcolor" runat="server" Height="20px" Width="20px" CssClass="colorbox">
                                                                                    </asp:Label>
                                                                                    <asp:HiddenField ID="hfColor" runat="server" Value='<%# Eval("color") %>'></asp:HiddenField>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="description" HeaderText="Description" ReadOnly="True"
                                                                                FooterText="colhdescription"></asp:BoundField>
                                                                            <asp:BoundField DataField="action" HeaderText="Action" ReadOnly="True" FooterText="colhaction">
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cnf:Confirmation ID="cnfConfirm" runat="server" Title="Aruti" />
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
                <asp:HiddenField ID="TabName" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
    
	    $(document).ready(function()
		{
		    RetriveTab();
            
        });
         function RetriveTab() {
 
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "CycleSetup";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        debugger;
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
}

    </script>

</asp:Content>

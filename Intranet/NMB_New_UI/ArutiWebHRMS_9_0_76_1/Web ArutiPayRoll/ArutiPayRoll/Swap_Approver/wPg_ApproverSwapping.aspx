﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ApproverSwapping.aspx.vb"
    Inherits="Swap_Approver_wPg_ApproverSwapping" Title="Approver Swapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) { $("select").searchable(); } </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
    }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Swap Approver" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix  d--f ai--c jc--c">
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblCategory" runat="server" Text="Expense Category" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpenseCat" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFromApprover" runat="server" Text="From Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboFromApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFromLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboFromLevel" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 350px;">
                                                            <asp:GridView ID="dgFromApprover" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:BoundField DataField="employeecode" FooterText="dgcolhFromEmpcode" HeaderStyle-Width="30%"
                                                                        HeaderText="Employee Code" ReadOnly="true" />
                                                                    <asp:BoundField DataField="employeename" FooterText="dgcolhFromEmployee" HeaderStyle-Width="70%"
                                                                        HeaderText="Employee" ReadOnly="true" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeID" ReadOnly="true"
                                                                        Visible="false" />
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                                <RowStyle />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center">
                                        <asp:Button ID="btnSwap" runat="server" Text="Swap" CssClass="btn btn-primary"></asp:Button>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 as--fs">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblToApprover" runat="server" Text="To Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboToApprover" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblToLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboToLevel" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 350px;">
                                                            <asp:GridView ID="dgToApprover" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Employee Code" HeaderStyle-Width="30%"
                                                                        ReadOnly="true" FooterText="dgcolhToEmpCode" />
                                                                    <asp:BoundField DataField="employeename" HeaderText="Employee" HeaderStyle-Width="70%"
                                                                        ReadOnly="true" FooterText="dgcolhToEmployee" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeID" ReadOnly="true"
                                                                        Visible="false" />
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                                <RowStyle />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

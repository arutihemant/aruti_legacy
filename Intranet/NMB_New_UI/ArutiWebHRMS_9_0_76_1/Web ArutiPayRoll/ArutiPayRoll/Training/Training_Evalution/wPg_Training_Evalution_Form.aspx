﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Training_Evalution_Form.aspx.vb"
    Inherits="Training_Training_Evalution_wPg_Training_Evalution_Form" Title="Training Evaluation Form" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc6" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        
        $(document).ready(function () {
           maintainCommentScrollPosition();
        });
    
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            RetriveTab();
			RetriveCollapse();
            maintainCommentScrollPosition();
        }
       
         function maintainCommentScrollPosition() {
			$("#dvCommentScroll").scrollTop = parseInt($('#<%=hfCommentScrollPosition.ClientID%>').val());
        }
        function setCommentScrollPosition(scrollValue) {
            $('#<%=hfCommentScrollPosition.ClientID%>').val(scrollValue);
        }    
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblMainHeader" runat="server" Text="Training Evaluation Form"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <asp:Panel ID="pnlInstruction" runat="server" Visible="false">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 200px;">
                                                <asp:Label ID="lblInstructions" runat="server" Text="Instructions :" CssClass="form-label"></asp:Label>
                                                <asp:Label ID="txtInstruction" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dgvCategory" runat="server" Width="100%">
                                    <ItemTemplate>
                                        <div class="row clearfix">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <asp:Panel ID="pnlActionPlanList" runat="server">
                                                    <div class="panel-group" id="pnlActionPlan" role="tablist" aria-multiselectable="true">
                                                        <div class="panel" style="border: 1px solid #ddd">
                                                            <div class="panel-heading" role="tab" id="ActionCategoryheadingOne_<%# Eval("categoryid") %>">
                                                                <h4 class="panel-title d--f ai--c jc--sb">
                                                                    <a role="button" data-toggle="collapse" class="ActionCategoryCollapse" href="#ActionCategorycollapseOne_<%# Eval("categoryid") %>"
                                                                        aria-expanded="false" aria-controls="ActionCategorycollapseOne_<%# Eval("categoryid") %>"
                                                                        style="flex: 1">
                                                                        <asp:Label ID="lblCategoryName" Text='<%# Eval("categoryname") %>' runat="server" />
                                                                    </a>
                                                                    <asp:HiddenField ID="hfcategoryunkid" runat="server" Value='<%# Eval("categoryid") %>' />
                                                                </h4>
                                                            </div>
                                                            <div id="ActionCategorycollapseOne_<%# Eval("categoryid") %>" class="panel-collapse collapse"
                                                                role="tabpanel" aria-labelledby="ActionCategoryheadingOne_<%# Eval("categoryid") %>">
                                                                <div class="panel-body">
                                                                    <div class="row clearfix">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <div class="table-responsive" style="max-height: 500px">
                                                                                <asp:DataList ID="dgvQuestion" runat="server" Width="100%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Panel ID="pnlActionPlanItem" runat="server" CssClass="panel detail-box m-b-20">
                                                                                            <div class="body">
                                                                                                <asp:HiddenField ID="hfquestionnaireId" runat="server" Value='<%# Eval("questionnaireId") %>' />
                                                                                                <asp:HiddenField ID="hfAnswerType" Value='<%# Eval("answertype") %>' runat="server" />
                                                                                                <asp:HiddenField ID="hfCategoryId" Value='<%# Eval("categoryid") %>' runat="server" />
                                                                                                <asp:HiddenField ID="hfAskForJustification" Value='<%# Eval("isaskforjustification") %>'
                                                                                                    runat="server" />
                                                                                                <asp:HiddenField ID="hfForLineManagerFeedback" Value='<%# Eval("isforlinemanager") %>'
                                                                                                    runat="server" />
                                                                                                <div class="row clearfix">
                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                        <div class="title">
                                                                                                            <asp:Label ID="lblQuestion" runat="server" Text="Question" Visible="false"></asp:Label>
                                                                                                        </div>
                                                                                                        <div class="content">
                                                                                                            <asp:Label ID="txtActionPlanGoalName" Text='<%# Eval("Question") %>' runat="server"
                                                                                                                Font-Bold="true" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row clearfix">
                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                        <div class="title">
                                                                                                            <asp:Label ID="lblActionPlanGoalDescription" runat="server" Text="Answer"></asp:Label>
                                                                                                        </div>
                                                                                                        <div class="content">
                                                                                                            <asp:Panel ID="pnlAnswerFreeText" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                    <div class="form-group">
                                                                                                                        <div class="form-line">
                                                                                                                            <asp:TextBox ID="txtItemNameFreeText" runat="server" TextMode="MultiLine" Rows="3"
                                                                                                                                CssClass="form-control"></asp:TextBox>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </asp:Panel>
                                                                                                            <asp:Panel ID="pnlAnswerRating" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                    <div class="form-group">
                                                                                                                        <asp:DropDownList ID="cboItemNameRating" runat="server" AutoPostBack="false">
                                                                                                                        </asp:DropDownList>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </asp:Panel>
                                                                                                            <asp:Panel ID="pnlAnswerSelection" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                <div class="form-group">
                                                                                                                    <asp:DropDownList ID="cboItemNameSelection" runat="server" AutoPostBack="false">
                                                                                                                    </asp:DropDownList>
                                                                                                                </div>
                                                                                                            </asp:Panel>
                                                                                                            <asp:Panel ID="pnlAnswerDtp" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                <div class="form-group">
                                                                                                                    <uc1:DateCtrl ID="dtpItemName" runat="server" AutoPostBack="false" />
                                                                                                                </div>
                                                                                                            </asp:Panel>
                                                                                                            <asp:Panel ID="pnlAnswerNameNum" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                <uc4:NumericText ID="txtItemNameNUM" runat="server" Width="100%" AutoPostBack="false" />
                                                                                                            </asp:Panel>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row clearfix">
                                                                                                    <asp:Panel ID="pnlJustification" runat="server" Visible="false">
                                                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                            <div class="title">
                                                                                                                <asp:Label ID="lblJustify" runat="server" Text="Justify"></asp:Label>
                                                                                                            </div>
                                                                                                            <div class="content">
                                                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                    <div class="form-group">
                                                                                                                        <div class="form-line">
                                                                                                                            <asp:TextBox ID="txtJustify" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                </div>
                                                                                                <asp:Panel ID="pnlSubQuestion" runat="server" Visible="false">
                                                                                                    <div class="row clearfix">
                                                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                                                            <asp:Panel ID="pnlActionPlanList" runat="server">
                                                                                                                <div class="panel-group" id="pnlActionPlan" role="tablist" aria-multiselectable="true">
                                                                                                                    <div class="panel" style="border: 1px solid #ddd">
                                                                                                                        <div class="panel-heading" role="tab" id="SubQuestionCollection">
                                                                                                                            <h4 class="panel-title d--f ai--c jc--sb">
                                                                                                                                <a role="button" data-toggle="collapse" class="SubQuestionCollapse" href="#SubQuestionCollection_<%# Eval("questionnaireId") %>"
                                                                                                                                    aria-expanded="false" aria-controls="SubQuestionCollection_<%# Eval("questionnaireId") %>"
                                                                                                                                    style="flex: 1">
                                                                                                                                    <asp:Label ID="lblCategoryName" Text="Sub-Questions" runat="server" />
                                                                                                                                </a>
                                                                                                                            </h4>
                                                                                                                        </div>
                                                                                                                        <div id="SubQuestionCollection_<%# Eval("questionnaireId") %>" class="panel-collapse collapse"
                                                                                                                            role="tabpanel" aria-labelledby="SubQuestionCollection_<%# Eval("questionnaireId") %>">
                                                                                                                            <div class="panel-body">
                                                                                                                                <div class="row clearfix">
                                                                                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                                                                                        <div class="table-responsive" style="max-height: 500px">
                                                                                                                                            <asp:DataList ID="dgvSubQuestion" runat="server" Width="100%">
                                                                                                                                                <ItemTemplate>
                                                                                                                                                    <asp:HiddenField ID="hfSubQuestionId" runat="server" Value='<%# Eval("subquestionid") %>' />
                                                                                                                                                    <asp:HiddenField ID="hfQuestionnaireId" runat="server" Value='<%# Eval("questionid") %>' />
                                                                                                                                                    <asp:HiddenField ID="hfAnswerType" Value='<%# Eval("AnsType") %>' runat="server" />
                                                                                                                                                    <asp:HiddenField ID="hfAskForJustification" Value='<%# Eval("isaskforjustification") %>'
                                                                                                                                                        runat="server" />
                                                                                                                                                    <asp:HiddenField ID="hfForLineManagerFeedback" Value='<%# Eval("isforlinemanager") %>'
                                                                                                                                                        runat="server" />
                                                                                                                                                    <asp:Panel ID="pnlActionPlanItem" runat="server" CssClass="panel detail-box m-b-20">
                                                                                                                                                        <div class="body">
                                                                                                                                                            <div class="row clearfix">
                                                                                                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                                                                    <div class="title">
                                                                                                                                                                        <asp:Label ID="lblActionPlanGoalName" runat="server" Text="Question" Visible="false"></asp:Label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="content">
                                                                                                                                                                        <asp:Label ID="txtActionPlanGoalName" Text='<%# Eval("subquestion") %>' runat="server"
                                                                                                                                                                            Font-Bold="true" />
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                            <div class="row clearfix">
                                                                                                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                                                                    <div class="title">
                                                                                                                                                                        <asp:Label ID="lblActionPlanGoalDescription" runat="server" Text="Answer"></asp:Label>
                                                                                                                                                                    </div>
                                                                                                                                                                    <div class="content">
                                                                                                                                                                        <asp:Panel ID="pnlAnswerFreeText" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                                                                                <div class="form-group">
                                                                                                                                                                                    <div class="form-line">
                                                                                                                                                                                        <asp:TextBox ID="txtItemNameFreeText" runat="server" TextMode="MultiLine" Rows="3"
                                                                                                                                                                                            CssClass="form-control"></asp:TextBox>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </asp:Panel>
                                                                                                                                                                        <asp:Panel ID="pnlAnswerRating" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                                                                                <div class="form-group">
                                                                                                                                                                                    <asp:DropDownList ID="cboItemNameRating" runat="server" AutoPostBack="false">
                                                                                                                                                                                    </asp:DropDownList>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </asp:Panel>
                                                                                                                                                                        <asp:Panel ID="pnlAnswerSelection" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                <asp:DropDownList ID="cboItemNameSelection" runat="server" AutoPostBack="false">
                                                                                                                                                                                </asp:DropDownList>
                                                                                                                                                                            </div>
                                                                                                                                                                        </asp:Panel>
                                                                                                                                                                        <asp:Panel ID="pnlAnswerDtp" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                                                                            <div class="form-group">
                                                                                                                                                                                <uc1:DateCtrl ID="dtpItemName" runat="server" AutoPostBack="false" />
                                                                                                                                                                            </div>
                                                                                                                                                                        </asp:Panel>
                                                                                                                                                                        <asp:Panel ID="pnlAnswerNameNum" runat="server" Visible="false" CssClass="d--f ai--c">
                                                                                                                                                                            <uc4:NumericText ID="txtItemNameNUM" runat="server" Width="100%" AutoPostBack="false" />
                                                                                                                                                                        </asp:Panel>
                                                                                                                                                                    </div>
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                            <div class="row clearfix">
                                                                                                                                                                <asp:Panel ID="pnlJustification" runat="server" Visible="false">
                                                                                                                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                                                                                                        <div class="title">
                                                                                                                                                                            <asp:Label ID="lblJustify" runat="server" Text="Justify"></asp:Label>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div class="content">
                                                                                                                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                                                                                                                                                <div class="form-group">
                                                                                                                                                                                    <div class="form-line">
                                                                                                                                                                                        <asp:TextBox ID="txtJustify" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                                                                                                                                                    </div>
                                                                                                                                                                                </div>
                                                                                                                                                                            </div>
                                                                                                                                                                        </div>
                                                                                                                                                                    </div>
                                                                                                                                                                </asp:Panel>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </asp:Panel>
                                                                                                                                                </ItemTemplate>
                                                                                                                                            </asp:DataList>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </asp:Panel>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </asp:Panel>
                                                                                            </div>
                                                                                        </asp:Panel>
                                                                                    </ItemTemplate>
                                                                                </asp:DataList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="TabActionPlanName" runat="server" />
                <asp:HiddenField ID="TabName" runat="server" />
                <asp:HiddenField ID="hfCommentScrollPosition" Value="0" runat="server" />
                <asp:HiddenField ID="CollapseActionPlanName" runat="server" />
                <asp:HiddenField ID="CollapsePersonalDevlopmentGoalName" runat="server" />
                <asp:HiddenField ID="CollapsePersonalDevlopmentGoalCategory" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>

        $(document).ready(function() {
            RetriveTab();
			RetriveCollapse();
        });
        
        
        function RetriveTab() {

            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "Employee_Detail";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function() {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
            
            
           var actionPlanTabName = $("[id*=TabActionPlanName]").val() != "" ? $("[id*=TabActionPlanName]").val() : "ActionPlanDetail";
            $('#ActionPlanTabs a[href="#' + actionPlanTabName + '"]').tab('show');
            $("#ActionPlanTabs a").click(function() {
                $("[id*=TabActionPlanName]").val($(this).attr("href").replace("#", ""));
            });
        }
        
        function RetriveCollapse() {
			var actionPlanCollapseName = $("[id*=CollapseActionPlanName]").val();
			if (actionPlanCollapseName != "")
			{
				$("#" + actionPlanCollapseName + "").collapse('show');
			}
			
			$(".ActionCategoryCollapse").click(function() {
				$("[id*=CollapseActionPlanName]").val($(this).attr("href").replace("#", ""));
			});				
			

			var collapsePersonalDevlopmentGoalName = $("[id*=CollapsePersonalDevlopmentGoalName]").val();
			if (collapsePersonalDevlopmentGoalName != "")
			{
				$("#" + collapsePersonalDevlopmentGoalName + "").collapse('show');
			}
			
			$(".PersonalDevlopmentGoalCollapse").click(function() {
				$("[id*=CollapsePersonalDevlopmentGoalName]").val($(this).attr("href").replace("#", ""));
			});				

			
            var CollapsePersonalDevlopmentGoalCategory = $("[id*=CollapsePersonalDevlopmentGoalCategory]").val();
			if (CollapsePersonalDevlopmentGoalCategory != "")
			{
				$("#" + CollapsePersonalDevlopmentGoalCategory + "").collapse('show');
        }
			
			$(".PersonalDevlopmentGoalCollapseCategory").click(function() {
				$("[id*=CollapsePersonalDevlopmentGoalCategory]").val($(this).attr("href").replace("#", ""));
			});				
        }
    </script>

</asp:Content>

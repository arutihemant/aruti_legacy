﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

#End Region
Partial Class Training_Training_Approver_Mapping_wPg_TrainingApproverMapping
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApproverMapping"
    Private blnpopupTrainingApprover As Boolean = False
    Private mintApproverMatrixUnkid As Integer = -1
    Private mdtEmployee As DataTable = Nothing
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = String.Empty
    Private dtEmployee As DataTable = Nothing
    Private mintapproverunkid As Integer = -1
    Private currentId As String = ""
    Private mintMappingUnkid As Integer
    Private mblActiveDeactiveApprStatus As Boolean
    Private mdtApproverList As DataTable
    Private mstrDeleteAction As String = ""
    Private mintLevelmstid As Integer
    Private objTrainingApprLvlMaster As New clstraining_approverlevel_master
    Private blnpopupApproverLevel As Boolean = False
#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillComboApprovalLevel()
                If CBool(Session("AllowToViewTrainingApproverLevel")) Then
                    FillListApprovalLevel(False)
                Else
                    FillListApprovalLevel(True)

                End If
                SetVisibilityApprovalLevel()
                ListFillCombo()
                FillApprovalMatrix()
                FillTrainingApproverCombo()
                If CBool(Session("AllowToViewTrainingApprover")) Then
                    FillTrainingApproverList(False)
                Else
                    FillTrainingApproverList(True)
                End If
            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                blnpopupApproverLevel = Convert.ToBoolean(ViewState("blnpopupApproverLevel").ToString())

                mintApproverMatrixUnkid = CInt(Me.ViewState("mintApproverMatrixUnkid"))

                If ViewState("mdtApproverList") IsNot Nothing Then
                    mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                    If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("mappingunkid").ToString) > 0 Then
                        gvApproverList.DataSource = mdtApproverList
                        gvApproverList.DataBind()
                    End If
                End If
                mstrDeleteAction = CStr(Me.ViewState("mstrDeleteAction"))
                mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                If blnpopupTrainingApprover Then
                    popupTrainingApprover.Show()
                Else
                    popupTrainingApprover.Hide()
                End If

                If blnpopupApproverLevel Then
                    popupApproverLevel.Show()
                Else
                    popupApproverLevel.Hide()
                End If


            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupApproverLevel") = blnpopupApproverLevel
            ViewState("mintApproverMatrixUnkid") = mintApproverMatrixUnkid
            ViewState("mintMappingUnkid") = mintMappingUnkid
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus
            ViewState("mdtApproverList") = mdtApproverList
            Me.ViewState("mstrDeleteAction") = mstrDeleteAction
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Approval Levels"

#Region " Private Methods "

    Private Sub FillComboApprovalLevel()
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As DataSet
        Try
            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsList.Tables(0).Rows.Count > 0 Then
                With drpTrainingCalendarAppLevel
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Private Sub FillListApprovalLevel(ByVal isblank As Boolean)
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As New DataSet
        Dim dsCalendarList As New DataSet
        Dim strfilter As String = ""
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            dsCalendarList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsCalendarList.Tables(0).Rows.Count > 0 Then
                If strfilter.Trim.Length > 0 Then
                    strfilter &= " AND hrtraining_approverlevel_master.calendarunkid = " & dsCalendarList.Tables(0).Rows(0).Item("calendarunkid").ToString
                Else
                    strfilter &= " hrtraining_approverlevel_master.calendarunkid = " & dsCalendarList.Tables(0).Rows(0).Item("calendarunkid").ToString
                End If

                dsList = objTrainingApprLvlMaster.GetList("List", True, False, strfilter)
                If dsList.Tables(0).Rows.Count <= 0 Then
                    dsList = objTrainingApprLvlMaster.GetList("List", True, True, "1 = 2")
                    isblank = True
                End If
                GvApprLevelList.DataSource = dsList.Tables("List")
                GvApprLevelList.DataBind()
            End If

            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Private Sub SetVisibilityApprovalLevel()
        Try
            btnnew.Visible = CBool(Session("AllowToAddTrainingApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValueApprovalLevel()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If

            objTrainingApprLvlMaster._Levelcode = txtlevelcode.Text.Trim
            objTrainingApprLvlMaster._Levelname = txtlevelname.Text.Trim
            objTrainingApprLvlMaster._Priority = Convert.ToInt32(txtlevelpriority.Text)
            objTrainingApprLvlMaster._Calendarunkid = CInt(drpTrainingCalendarAppLevel.SelectedValue)
            Call SetAtValueApprovalLevel()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValueApprovalLevel()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApprLvlMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApprLvlMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApprLvlMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApprLvlMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApprLvlMaster._FormName = mstrModuleName
            objTrainingApprLvlMaster._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValueApprovalLevel()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objTrainingApprLvlMaster._Levelcode
            txtlevelname.Text = objTrainingApprLvlMaster._Levelname
            txtlevelpriority.Text = objTrainingApprLvlMaster._Priority.ToString()
            If CInt(objTrainingApprLvlMaster._Calendarunkid) > 0 Then
                drpTrainingCalendarAppLevel.SelectedValue = CStr(objTrainingApprLvlMaster._Calendarunkid)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearDataApprovalLevel()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
            drpTrainingCalendarAppLevel.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "
    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprover.Click
        Dim blnFlag As Boolean = False
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
                Exit Sub
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Approver Level Name cannot be blank. Approver Level Name is required information"), Me)
                Exit Sub
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Approver Level Priority cannot be less than Zero. Approver Level Priority is required information"), Me)
                Exit Sub
            ElseIf CInt(drpTrainingCalendarAppLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue"), Me)
                Exit Sub
            End If
            Call SetValueApprovalLevel()
            If mintLevelmstid > 0 Then
                blnFlag = objTrainingApprLvlMaster.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objTrainingApprLvlMaster.Insert()
            End If

            If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
            Else
                ClearDataApprovalLevel()
                FillListApprovalLevel(False)
                ListFillCombo()
                FillTrainingApproverCombo()
                mintLevelmstid = 0
                blnpopupApproverLevel = False
                popupApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValueApprovalLevel()
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())

            If objTrainingApprLvlMaster.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use"), Me)
                Exit Sub
            End If
            mstrDeleteAction = "dellevel"
            cnfConfirm.Show()
            cnfConfirm.Title = "Confirmation"
            cnfConfirm.Message = "Are You Sure You Want To Delete This Approver Level ?"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

  
    Protected Sub btnCloseGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClosApprover.Click
        Try
            ClearDataApprovalLevel()
            blnpopupApproverLevel = False
            popupApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("levelunkid").ToString) > 0 Then
                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                        lnkedit.Visible = CBool(Session("AllowToEditTrainingApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteTrainingApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Approval Matrix"

#Region " Private Methods "

    Private Sub ListFillCombo()
        Dim objLevel As New clstraining_approverlevel_master
        Dim objMaster As New clsMasterData
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try

            dsList = objCalendar.getListForCombo("List", , StatusType.Open)
            If dsList.Tables(0).Rows.Count > 0 Then
                With drpTrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

                With drpALTrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0).Copy()
                    .DataBind()
                End With

                With drpATrainingCalendar
                    .DataTextField = "name"
                    .DataValueField = "calendarunkid"
                    .DataSource = dsList.Tables(0).Copy()
                    .DataBind()
                End With

                dsList = objLevel.getListForCombo(CInt(drpTrainingCalendar.SelectedValue), "List", True)
                drpLevel.DataTextField = "name"
                drpLevel.DataValueField = "levelunkid"
                drpLevel.DataSource = dsList.Tables(0)
                drpLevel.DataBind()

                drpALevel.DataTextField = "name"
                drpALevel.DataValueField = "levelunkid"
                drpALevel.DataSource = dsList.Tables(0).Copy()
                drpALevel.DataBind()

            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevel = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Dim dsApprovalMatrix As DataSet
        Try
            If CInt(drpTrainingCalendar.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue"), Me)
                Return False
            End If

            If CInt(drpLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Level is mandatory information. Please select Level to continue"), Me)
                Return False
            End If

            'Hemant (25 Oct 2021) -- Start
            If CDbl(txtCostAmountFrom.Text) > 1000000000000000000 OrElse CDbl(txtCostAmountTo.Text) > 1000000000000000000 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, Defined Cost Amount is too large"), Me)
                Return False
            End If
            'Hemant (25 Oct 2021) -- End

            dsApprovalMatrix = objApprovalMatrix.GetList("List", CInt(drpTrainingCalendar.SelectedValue))
            Dim drApprovalMatrix As DataRow() = dsApprovalMatrix.Tables(0).Select("levelunkid = " & CInt(drpLevel.SelectedValue) & " AND trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid & " ")
            If drApprovalMatrix.Length > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, approval matrix for the selected level is already defined for this calendar. Please select another Level to continue"), Me)
                Return False
            End If

            For Each drRow As DataRow In dsApprovalMatrix.Tables(0).Select("trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid)
                If (CDbl(txtCostAmountFrom.Text) >= CDbl(drRow.Item("costamountfrom")) AndAlso CDbl(txtCostAmountFrom.Text) <= CDbl(drRow.Item("costamountto"))) OrElse _
                (CDbl(txtCostAmountTo.Text) >= CDbl(drRow.Item("costamountfrom")) AndAlso CDbl(txtCostAmountTo.Text) <= CDbl(drRow.Item("costamountto"))) Then

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, this cost range is already defined to another approver level"), Me)
                    Return False

                End If
            Next

            Dim drExistRow() As DataRow = dsApprovalMatrix.Tables(0).Select("trainingapprovalmatrixunkid <> " & mintApproverMatrixUnkid & " AND costamountto > " & CDbl(txtCostAmountFrom.Text) & "  AND costamountto < " & CDbl(txtCostAmountTo.Text))
            If drExistRow.Length > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, this cost range is not valid"), Me)
                Return False
            End If


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Function

    Private Sub FillApprovalMatrix()
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Dim dsList As New DataSet
        Try

            dsList = objApprovalMatrix.GetList("List", CInt(drpTrainingCalendar.SelectedValue))
            gvApprovalMatrix.DataSource = dsList.Tables(0)
            gvApprovalMatrix.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovalMatrix = Nothing
        End Try
    End Sub

    Private Sub ClearCtrls()
        Try
            mintApproverMatrixUnkid = 0
            drpTrainingCalendar.SelectedIndex = 0
            drpLevel.SelectedValue = CStr(0)
            txtCostAmountFrom.Text = "0"
            txtCostAmountTo.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue(ByVal objApproverMatrix As clsTraining_Approval_Matrix)
        Try
            objApproverMatrix._TrainingApprovalMatrixunkid = mintApproverMatrixUnkid
            objApproverMatrix._Calendarunkid = CInt(drpTrainingCalendar.SelectedValue)
            objApproverMatrix._Levelunkid = CInt(drpLevel.SelectedValue)
            objApproverMatrix._CostAmountFrom = CDec(txtCostAmountFrom.Text)
            objApproverMatrix._CostAmountTo = CDec(txtCostAmountTo.Text)
            objApproverMatrix._Isvoid = False

            objApproverMatrix._AuditUserId = CInt(Session("UserId"))
            objApproverMatrix._ClientIP = CStr(Session("IP_ADD"))
            objApproverMatrix._FormName = mstrModuleName
            objApproverMatrix._FromWeb = True
            objApproverMatrix._HostName = CStr(Session("HOST_NAME"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetApprovalMatrixValue(ByVal intApproverMatrixId As Integer)
        Dim objApprovalMatrix As New clsTraining_Approval_Matrix
        Try
            objApprovalMatrix._TrainingApprovalMatrixunkid = intApproverMatrixId
            drpTrainingCalendar.SelectedValue = CStr(objApprovalMatrix._Calendarunkid)
            drpLevel.SelectedValue = CStr(objApprovalMatrix._Levelunkid)
            txtCostAmountFrom.Text = CStr(objApprovalMatrix._CostAmountFrom)
            txtCostAmountTo.Text = CStr(objApprovalMatrix._CostAmountTo)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprovalMatrix = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objApproverMatrix As New clsTraining_Approval_Matrix
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then
                Exit Sub
            End If

            SetValue(objApproverMatrix)
            If mintApproverMatrixUnkid > 0 Then
                blnFlag = objApproverMatrix.Update()
            Else
                blnFlag = objApproverMatrix.Insert()
            End If

            If blnFlag = False AndAlso objApproverMatrix._Message.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(objApproverMatrix._Message, Me)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Approval Matrix Saved successfully"), Me)
                ClearCtrls()
                FillApprovalMatrix()
                drpTrainingCalendar.Enabled = True
                drpLevel.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApproverMatrix = Nothing
        End Try

    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ClearCtrls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView's Event"

    Protected Sub gvApprovalMatrix_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApprovalMatrix.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(gvApprovalMatrix.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkApprovalMatrixEdit_Click(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintApproverMatrixUnkid = CInt(gvApprovalMatrix.DataKeys(row.RowIndex)("trainingapprovalmatrixunkid"))
            GetApprovalMatrixValue(mintApproverMatrixUnkid)
            drpTrainingCalendar.Enabled = False
            drpLevel.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub lnkApprovalMatrixDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintApproverMatrixUnkid = CInt(gvApprovalMatrix.DataKeys(row.RowIndex)("trainingapprovalmatrixunkid"))
            mstrDeleteAction = "delmatrix"
            cnfConfirm.Message = Language.getMessage(mstrModuleName, 10, "You are about to delete this Approval Matrix Entry. Are you sure you want to delete?")
            cnfConfirm.Show()
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

#End Region

#End Region

#Region "Training Approver"

#Region "Private Methods"

    Private Function IsTrainingApproverValid() As Boolean
        Try
            If CInt(drpARole.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Role cannot be blank. Role is required information"), Me)
                drpARole.Focus()
                Return False

            ElseIf CInt(drpALevel.SelectedValue) = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Approver Level is compulsory information. Please Select Approver Level"), Me)
                drpLevel.Focus()
                Return False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillTrainingApproverList(ByVal isblank As Boolean)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try
            If CInt(drpALTrainingCalendar.SelectedValue) > 0 Then
                strSearching &= "AND TAM.calendarunkid = " & CInt(drpALTrainingCalendar.SelectedValue) & " "
            End If

            If CInt(drpALLevel.SelectedValue) > 0 Then
                strSearching &= "AND TAM.levelunkid = " & CInt(drpALLevel.SelectedValue) & " "
            End If

            If CInt(drpALRole.SelectedValue) > 0 Then
                strSearching &= "AND TAM.roleunkid = " & CInt(drpALRole.SelectedValue) & " "
            End If

            If isblank Or CBool(Session("AllowToViewTrainingApprover")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            If strSearching.Trim.Length > 0 Then strSearching = strSearching.Substring(3)


            dsApproverList = objTrainingApproverMaster.GetList("List", True, strSearching)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objTrainingApproverMaster.GetList("List", True, "1 = 2", 0, Nothing, True)
                isblank = True
            End If


            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "Level", DataViewRowState.CurrentRows).ToTable()
                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("Level")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLeaveName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                mdtApproverList = dtTable
                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

    Private Sub FillTrainingApproverCombo()
        Dim objLevel As New clstraining_approverlevel_master
        Dim objApprover As New clstraining_approver_master
        Dim objUserRole As New clsUserRole_Master
        Dim dsList As DataSet
        Try

            dsList = objUserRole.getComboList("UserRole", True)
            With drpARole
                .DataSource = dsList.Tables("UserRole")
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataBind()
            End With
            Me.ViewState.Add("UserList", dsList.Tables("UserRole").Copy())

            With drpALRole
                .DataSource = dsList.Tables("UserRole").Copy()
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataBind()
            End With

            If drpALTrainingCalendar.DataSource IsNot Nothing Then
                dsList = objLevel.getListForCombo(CInt(drpALTrainingCalendar.SelectedValue), "List", True)
                With drpALLevel
                    .DataTextField = "name"
                    .DataValueField = "levelunkid"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLevel = Nothing
            objApprover = Nothing
            objUserRole = Nothing
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetAtValue(ByVal objTrainingApproverMaster As clstraining_approver_master)

        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApproverMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApproverMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApproverMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApproverMaster._FormName = mstrModuleName
            objTrainingApproverMaster._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try

    End Sub

    Protected Sub SetValueToPopup(ByVal objTrainingApproverMaster As clstraining_approver_master)
        Dim objTrainingApproverTran As New clsTraining_Approver_Tran
        Dim dtAssignEmployeeList As New DataTable
        Try
            drpALevel.SelectedValue = objTrainingApproverMaster._Levelunkid
            drpARole.SelectedValue = objTrainingApproverMaster._Roleunkid
            drpARole_SelectedIndexChanged(Nothing, Nothing)
            drpARole.Enabled = False
            drpATrainingCalendar.Enabled = False

            objTrainingApproverTran._TrainingApproverunkid = objTrainingApproverMaster._Mappingunkid
            objTrainingApproverTran.GetData()

            dtAssignEmployeeList = objTrainingApproverTran._TranDataTable

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverTran = Nothing
        End Try
    End Sub

#End Region

#Region "Button Methods"

    Protected Sub btnALNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALNew.Click
        Try
            FillTrainingApproverCombo()
            mintMappingUnkid = 0
            drpARole.Enabled = True
            drpATrainingCalendar.Enabled = False

            blnpopupTrainingApprover = True
            popupTrainingApprover.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnASave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnASave.Click
        Dim objTrainingApprover As New clstraining_approver_master
        Dim strMessage As String = ""

        Dim blnFlag As Boolean = False
        Try
            If IsTrainingApproverValid() = False Then Exit Sub

            objTrainingApprover._Mappingunkid = mintMappingUnkid
            objTrainingApprover._Calendarunkid = CInt(drpATrainingCalendar.SelectedValue)
            objTrainingApprover._Levelunkid = CInt(drpALevel.SelectedValue)
            objTrainingApprover._Roleunkid = CInt(drpARole.SelectedValue)
            objTrainingApprover._Isvoid() = False
            objTrainingApprover._Voiddatetime() = Nothing
            objTrainingApprover._Voiduserunkid() = -1
            objTrainingApprover._Voidreason() = ""
            objTrainingApprover._IsFromWeb = True
            objTrainingApprover._FormName = mstrModuleName
            objTrainingApprover._AuditUserId = CInt(Session("UserId"))
            objTrainingApprover._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApprover._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApprover._HostName = CStr(Session("HOST_NAME"))


            If mintMappingUnkid > 0 Then
                blnFlag = objTrainingApprover.Update()
            Else
                blnFlag = objTrainingApprover.Insert()
            End If


            If blnFlag = False And objTrainingApprover._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprover._Message, Me)
                blnpopupTrainingApprover = True
                popupTrainingApprover.Show()

            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Training Approver Saved successfully"), Me)

                drpARole.SelectedIndex = 0
                drpALevel.SelectedIndex = 0

                mintMappingUnkid = 0

                blnpopupTrainingApprover = False
                popupTrainingApprover.Hide()
                Call FillTrainingApproverList(False)
            End If


           
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApprover = Nothing
        End Try

    End Sub

    Protected Sub btnALReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALReset.Click
        Try
            drpALLevel.SelectedIndex = 0
            drpALRole.SelectedIndex = 0
            Call FillTrainingApproverList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub drpARole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpARole.SelectedIndexChanged
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    

#End Region

#Region " Gridview Event "

    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("mappingunkid").ToString) > 0 Then
                    oldid = dt.Rows(e.Row.RowIndex)("Level").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("Level").ToString(), gvApproverList)
                        currentId = oldid
                    Else

                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkALdelete As LinkButton = TryCast(e.Row.FindControl("lnkALdelete"), LinkButton)

                        If CBool(Session("AllowToDeleteTrainingApprover")) Then
                            lnkALdelete.Visible = True
                        Else
                            lnkALdelete.Visible = False
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            blnpopupTrainingApprover = False
            popupTrainingApprover.Hide()
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnALSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnALSearch.Click
        Try
            FillTrainingApproverList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkALEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Try
            Dim lnkALEdit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkALEdit).NamingContainer, GridViewRow)

            objTrainingApproverMaster._Mappingunkid = CInt(lnkALEdit.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkALEdit.CommandArgument.ToString())

            objTrainingApproverMaster._Mappingunkid = mintMappingUnkid
            SetValueToPopup(objTrainingApproverMaster)
            popupTrainingApprover.Show()
            blnpopupTrainingApprover = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

    Protected Sub lnkALdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objTrainingApproverMaster As New clstraining_approver_master
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objTrainingApproverMaster._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkdelete.CommandArgument.ToString())

            mstrDeleteAction = "delapproval"
            cnfConfirm.Title = "Confirmation"
            cnfConfirm.Message = Language.getMessage(mstrModuleName, 21, "Are You Sure You Want To Delete This Approver?")
            cnfConfirm.Show()

            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingApproverMaster = Nothing
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                Case "DELLEVEL"

                    SetAtValueApprovalLevel()
                    blnFlag = objTrainingApprLvlMaster.Delete(mintLevelmstid)

                    If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                        DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
                    Else
                        FillListApprovalLevel(False)
                        ListFillCombo()
                        FillTrainingApproverCombo()
                        mintLevelmstid = 0
                    End If

                Case "DELMATRIX"
                    delReason.Title = Language.getMessage(mstrModuleName, 12, "Enter reason for deleting this Approval Matrix")
                    delReason.Show()
                Case "DELAPPROVAL"
                    delReason.Title = Language.getMessage(mstrModuleName, 13, "Enter reason for deleting this Training Approver")
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Delete Reason"

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mstrDeleteAction.ToUpper()
                
                Case "DELMATRIX"
                    Dim objApprovalMatrix As New clsTraining_Approval_Matrix
                    objApprovalMatrix.pblnIsvoid = True
                    objApprovalMatrix.pintVoiduserunkid = CInt(Session("UserId"))
                    objApprovalMatrix.pstrVoidreason = delReason.Reason
                    objApprovalMatrix.pdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objApprovalMatrix.pstrFormName = mstrModuleName
                    objApprovalMatrix.pstrClientIP = CStr(Session("IP_ADD"))
                    objApprovalMatrix.pstrHostName = CStr(Session("HOST_NAME"))
                    objApprovalMatrix.pintAuditUserId = CInt(Session("UserId"))
                    objApprovalMatrix.pblnIsWeb = True

                    If objApprovalMatrix.Delete(mintApproverMatrixUnkid) = False Then
                        DisplayMessage.DisplayMessage(objApprovalMatrix._Message, Me)
                        Exit Sub
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Approval Matrix deleted Successfully"), Me)
                        FillApprovalMatrix()
                        ClearCtrls()
                    End If
                    objApprovalMatrix = Nothing

                Case "DELAPPROVAL"
                    Dim objTrainingApproverMaster As New clstraining_approver_master
                    SetAtValue(objTrainingApproverMaster)
                    objTrainingApproverMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objTrainingApproverMaster._Voidreason = delReason.Reason
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        objTrainingApproverMaster._Voiduserunkid = CInt(Session("UserId"))
                    End If

                    If objTrainingApproverMaster.Delete(CInt(mintMappingUnkid)) = False Then
                        DisplayMessage.DisplayMessage(objTrainingApproverMaster._Message, Me)
                        Exit Sub
                    End If
                    FillTrainingApproverList(False)

                    objTrainingApproverMaster = Nothing
                Case Else
                    delReason.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblPageHeader.ID, Me.Title)

            Language._Object.setCaption(lblApprovalLevels.ID, lblApprovalLevels.Text)
            Language._Object.setCaption(lblApprovalMatrix.ID, lblApprovalMatrix.Text)
            Language._Object.setCaption(lblTrainingApprover.ID, lblTrainingApprover.Text)

            'Approval Levels            
            Language._Object.setCaption(lblApproverLevelsCaption.ID, lblApproverLevelsCaption.Text)
            Language._Object.setCaption(lblCancelText1.ID, lblCancelText1.Text)
            Language._Object.setCaption(lblTrainingCalendarAppLevel.ID, lblTrainingCalendarAppLevel.Text)
            Language._Object.setCaption(lbllevelcode.ID, lbllevelcode.Text)
            Language._Object.setCaption(lbllevelname.ID, lbllevelname.Text)
            Language._Object.setCaption(lbllevelpriority.ID, lbllevelpriority.Text)

            Language._Object.setCaption(btnnew.ID, btnnew.Text)
            Language._Object.setCaption(btnSaveApprover.ID, btnSaveApprover.Text)
            Language._Object.setCaption(btnClosApprover.ID, btnClosApprover.Text)

            Language._Object.setCaption(GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            Language._Object.setCaption(GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            Language._Object.setCaption(GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            Language._Object.setCaption(GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)

            'Approval Matrix
            Language._Object.setCaption(lblAddEditApprovalMatrix.ID, lblAddEditApprovalMatrix.Text)
            Language._Object.setCaption(lblTrainingCalendar.ID, lblTrainingCalendar.Text)
            Language._Object.setCaption(lblLevel.ID, lblLevel.Text)
            Language._Object.setCaption(lblCostAmountFrom.ID, lblCostAmountFrom.Text)
            Language._Object.setCaption(lblCostAmountTo.ID, lblCostAmountTo.Text)

            Language._Object.setCaption(btnSave.ID, btnSave.Text)
            Language._Object.setCaption(btnReset.ID, btnReset.Text)

            Language._Object.setCaption(gvApprovalMatrix.Columns(2).FooterText, gvApprovalMatrix.Columns(2).HeaderText)
            Language._Object.setCaption(gvApprovalMatrix.Columns(3).FooterText, gvApprovalMatrix.Columns(3).HeaderText)
            Language._Object.setCaption(gvApprovalMatrix.Columns(4).FooterText, gvApprovalMatrix.Columns(4).HeaderText)
            Language._Object.setCaption(gvApprovalMatrix.Columns(5).FooterText, gvApprovalMatrix.Columns(5).HeaderText)

            'TrainingApprover
            Language._Object.setCaption(lblALCaption.ID, lblALCaption.Text)
            Language._Object.setCaption(lblALTrainingCalendar.ID, lblALTrainingCalendar.Text)
            Language._Object.setCaption(lblALLevel.ID, lblALLevel.Text)
            Language._Object.setCaption(lblALRole.ID, lblALRole.Text)
            Language._Object.setCaption(Label3.ID, Label3.Text)
            Language._Object.setCaption(lblATrainingCalendar.ID, lblATrainingCalendar.Text)
            Language._Object.setCaption(lblARole.ID, lblARole.Text)
            Language._Object.setCaption(lblALevel.ID, lblALevel.Text)
            Language._Object.setCaption(lblPageHeader.ID, lblPageHeader.Text)
            
            Language._Object.setCaption(btnALNew.ID, btnALNew.Text)
            Language._Object.setCaption(btnALSearch.ID, btnALSearch.Text)
            Language._Object.setCaption(btnALReset.ID, btnALReset.Text)
            Language._Object.setCaption(btnASave.ID, btnASave.Text)
            Language._Object.setCaption(btnApproverUseraccessClose.ID, btnApproverUseraccessClose.Text)

            Language._Object.setCaption(gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)
            Language._Object.setCaption(gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)


            Language._Object.setCaption(btnClose.ID, btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, Me.Title)

            Me.lblApprovalLevels.Text = Language._Object.getCaption(lblApprovalLevels.ID, lblApprovalLevels.Text)
            Me.lblApprovalMatrix.Text = Language._Object.getCaption(lblApprovalMatrix.ID, lblApprovalMatrix.Text)
            Me.lblTrainingApprover.Text = Language._Object.getCaption(lblTrainingApprover.ID, lblTrainingApprover.Text)

            'Approval Levels            
            Me.lblApproverLevelsCaption.Text = Language._Object.getCaption(lblApproverLevelsCaption.ID, lblApproverLevelsCaption.Text)
            Me.lblCancelText1.Text = Language._Object.getCaption(lblCancelText1.ID, lblCancelText1.Text)
            Me.lblTrainingCalendarAppLevel.Text = Language._Object.getCaption(lblTrainingCalendarAppLevel.ID, lblTrainingCalendarAppLevel.Text)
            Me.lbllevelcode.Text = Language._Object.getCaption(lbllevelcode.ID, lbllevelcode.Text)
            Me.lbllevelname.Text = Language._Object.getCaption(lbllevelname.ID, lbllevelname.Text)
            Me.lbllevelpriority.Text = Language._Object.getCaption(lbllevelpriority.ID, lbllevelpriority.Text)

            Me.btnnew.Text = Language._Object.getCaption(btnnew.ID, btnnew.Text).Replace("&", "")
            Me.btnSaveApprover.Text = Language._Object.getCaption(btnSaveApprover.ID, btnSaveApprover.Text).Replace("&", "")
            Me.btnClosApprover.Text = Language._Object.getCaption(btnClosApprover.ID, btnClosApprover.Text).Replace("&", "")

            GvApprLevelList.Columns(2).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(2).FooterText, GvApprLevelList.Columns(2).HeaderText)
            GvApprLevelList.Columns(3).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(3).FooterText, GvApprLevelList.Columns(3).HeaderText)
            GvApprLevelList.Columns(4).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(4).FooterText, GvApprLevelList.Columns(4).HeaderText)
            GvApprLevelList.Columns(5).HeaderText = Language._Object.getCaption(GvApprLevelList.Columns(5).FooterText, GvApprLevelList.Columns(5).HeaderText)

            'Approval Matrix
            Me.lblAddEditApprovalMatrix.Text = Language._Object.getCaption(lblAddEditApprovalMatrix.ID, lblAddEditApprovalMatrix.Text)
            Me.lblTrainingCalendar.Text = Language._Object.getCaption(lblTrainingCalendar.ID, lblTrainingCalendar.Text)
            Me.lblLevel.Text = Language._Object.getCaption(lblLevel.ID, lblLevel.Text)
            Me.lblCostAmountFrom.Text = Language._Object.getCaption(lblCostAmountFrom.ID, lblCostAmountFrom.Text)
            Me.lblCostAmountTo.Text = Language._Object.getCaption(lblCostAmountTo.ID, lblCostAmountTo.Text)

            Me.btnSave.Text = Language._Object.getCaption(btnSave.ID, btnSave.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(btnReset.ID, btnReset.Text).Replace("&", "")

            gvApprovalMatrix.Columns(2).HeaderText = Language._Object.getCaption(gvApprovalMatrix.Columns(2).FooterText, gvApprovalMatrix.Columns(2).HeaderText)
            gvApprovalMatrix.Columns(3).HeaderText = Language._Object.getCaption(gvApprovalMatrix.Columns(3).FooterText, gvApprovalMatrix.Columns(3).HeaderText)
            gvApprovalMatrix.Columns(4).HeaderText = Language._Object.getCaption(gvApprovalMatrix.Columns(4).FooterText, gvApprovalMatrix.Columns(4).HeaderText)
            gvApprovalMatrix.Columns(5).HeaderText = Language._Object.getCaption(gvApprovalMatrix.Columns(5).FooterText, gvApprovalMatrix.Columns(5).HeaderText)

            'TrainingApprover
            Me.lblALCaption.Text = Language._Object.getCaption(lblALCaption.ID, lblALCaption.Text)
            Me.lblALTrainingCalendar.Text = Language._Object.getCaption(lblALTrainingCalendar.ID, lblALTrainingCalendar.Text)
            Me.lblALLevel.Text = Language._Object.getCaption(lblALLevel.ID, lblALLevel.Text)
            Me.lblALRole.Text = Language._Object.getCaption(lblALRole.ID, lblALRole.Text)
            Me.Label3.Text = Language._Object.getCaption(Label3.ID, Label3.Text)
            Me.lblATrainingCalendar.Text = Language._Object.getCaption(lblATrainingCalendar.ID, lblATrainingCalendar.Text)
            Me.lblARole.Text = Language._Object.getCaption(lblARole.ID, lblARole.Text)
            Me.lblALevel.Text = Language._Object.getCaption(lblALevel.ID, lblALevel.Text)
            Me.lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, lblPageHeader.Text)

            Me.btnALNew.Text = Language._Object.getCaption(btnALNew.ID, btnALNew.Text).Replace("&", "")
            Me.btnALSearch.Text = Language._Object.getCaption(btnALSearch.ID, btnALSearch.Text).Replace("&", "")
            Me.btnALReset.Text = Language._Object.getCaption(btnALReset.ID, btnALReset.Text).Replace("&", "")
            Me.btnASave.Text = Language._Object.getCaption(btnASave.ID, btnASave.Text).Replace("&", "")
            Me.btnApproverUseraccessClose.Text = Language._Object.getCaption(btnApproverUseraccessClose.ID, btnApproverUseraccessClose.Text).Replace("&", "")

            gvApproverList.Columns(2).HeaderText = Language._Object.getCaption(gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)
            gvApproverList.Columns(3).HeaderText = Language._Object.getCaption(gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)


            Me.btnClose.Text = Language._Object.getCaption(btnClose.ID, btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
	
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Level is mandatory information. Please select Level to continue")
            Language.setMessage(mstrModuleName, 2, "Approval Matrix Saved successfully")
            Language.setMessage(mstrModuleName, 3, "Approver Level is compulsory information. Please Select Approver Level")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use")
            Language.setMessage(mstrModuleName, 5, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue")
            Language.setMessage(mstrModuleName, 6, "Role cannot be blank. Role is required information")
            Language.setMessage(mstrModuleName, 7, "Sorry, this cost range is already defined to another approver level")
            Language.setMessage(mstrModuleName, 8, "Sorry, this cost range is not valid")
            Language.setMessage(mstrModuleName, 10, "You are about to delete this Approval Matrix Entry. Are you sure you want to delete?")
            Language.setMessage(mstrModuleName, 11, "Sorry, approval matrix for the selected level is already defined for this calendar. Please select another Level to continue")
            Language.setMessage(mstrModuleName, 12, "Enter reason for deleting this Approval Matrix")
            Language.setMessage(mstrModuleName, 13, "Enter reason for deleting this Training Approver")
            Language.setMessage(mstrModuleName, 14, "Training Approver Saved successfully")
            Language.setMessage(mstrModuleName, 16, "Approval Matrix deleted Successfully")
            Language.setMessage(mstrModuleName, 17, "Sorry, Training Calendar is mandatory information. Please select Training Calendar to continue")
            Language.setMessage(mstrModuleName, 18, "Approver Level Priority cannot be less than Zero. Approver Level Priority is required information")
            Language.setMessage(mstrModuleName, 19, "Approver Level Name cannot be blank. Approver Level Name is required information")
            Language.setMessage(mstrModuleName, 20, "Approver Level Code cannot be blank. Approver Level Code is required information")
            Language.setMessage(mstrModuleName, 21, "Are You Sure You Want To Delete This Approver?")
            Language.setMessage(mstrModuleName, 22, "Sorry, Defined Cost Amount is too large")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

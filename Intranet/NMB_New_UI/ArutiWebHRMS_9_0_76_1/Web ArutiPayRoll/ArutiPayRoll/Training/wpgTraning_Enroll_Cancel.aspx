<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpgTraning_Enroll_Cancel.aspx.vb"
    Inherits="wpgTraning_Enroll_Cancel" Title="Enrollment And Cancellation List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Enrollment And Cancellation List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBranch" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEnrollmentdateFrom" runat="server" Text="Enroll Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtApplyDate" runat="server" AutoPostBack="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEnrollmentDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtApplyDateTo" runat="server" AutoPostBack="False" />
                                    </div>
                                </div>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label Text="Course Title" runat="server" ID="lblCourse" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList runat="server" ID="cboCourseTitle" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancellationDateFrom" runat="server" CssClass="form-label" Text="Cancel Date"></asp:Label>
                                        <uc2:DateCtrl ID="dtCancelDate" runat="server" AutoPostBack="False" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCancellationDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtCancelDateTo" runat="server" AutoPostBack="False" />
                                    </div>
                                </div>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:RadioButtonList ID="radlstOperation" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">Cancelled</asp:ListItem>
                                            <asp:ListItem Value="2">Void</asp:ListItem>
                                            <asp:ListItem Selected="True" Value="3">Show All</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" CssClass=" btn btn-primary" Text="Search" Width="85px" />
                                <asp:Button ID="btnReset" runat="server" CssClass=" btn btn-default" Text="Reset" Width="85px" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px;">
                                            <asp:Panel ID="pbl_dgView" runat="server" ScrollBars="Auto">
                                                <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" Width="99%"
                                                  CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Title" HeaderText="Course Title" ReadOnly="True" />
                                                        <asp:BoundColumn DataField="Provider" HeaderText="Institute Provider" ReadOnly="True" />
                                                        <asp:BoundColumn DataField="Venue" HeaderText="Venue" ReadOnly="True" />
                                                        <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee" />
                                                        <asp:BoundColumn DataField="EnrollDate" HeaderText="Enroll Date" ReadOnly="True"
                                                            FooterText="colhEnrollDate" />
                                                        <asp:BoundColumn DataField="CancelDate" HeaderText="Cancel Date" ReadOnly="True"
                                                            FooterText="colhCancelDate" />
                                                        <asp:BoundColumn DataField="STATUS" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                                        <asp:BoundColumn HeaderText="Award Status" ReadOnly="true" FooterText="colhAwardStatus" />
                                                        <asp:BoundColumn DataField="enroll_remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark" />
                                                        <asp:BoundColumn DataField="isqualificaionupdated" HeaderText="Qua. Updated" ReadOnly="True"
                                                            FooterText="colhUpdateQuali" />
                                                        <asp:BoundColumn DataField="operationmodeid" HeaderText="operationmodeid" ReadOnly="True"
                                                            Visible="false" />
                                                    </Columns>
                                                    <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                        Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.IO
Imports System.Text
Imports System.Data

#End Region


Partial Class Training_Training_Settings_wPg_TrainingSettings
    Inherits Basepage

#Region " Private Variable(s) "
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingSettings"
    Dim DisplayMessage As New CommonCodes
    Dim mintTrainingCalendarId As Integer = 0
    Dim mintTrainingpriorityId As Integer = 0
    Dim mintTrainingCategoryId As Integer = 0
    Dim mintTrainingCostId As Integer = 0
    Dim mintTrainingLearningId As Integer = 0
    Dim mintTrainingVenueId As Integer = 0
    Dim mblnATAllowToAddTrainingwithoutTNAProcess As Boolean = False
    Dim mblnATAllowToApplyRequestTrainingNotinTrainingPlan As Boolean = False
    Dim mblnATTrainingRequireForForeignTravelling As Boolean = False
    'Sohail (08 Apr 2021) -- Start
    'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
    Private mstrATTrainingNeedAllocationID As String = ""
    'Sohail (08 Apr 2021) -- End
    'Hemant (28 Jul 2021) -- Start             
    'ENHANCEMENT : OLD-293 - Training Evaluation
    Dim mblnATPreTrainingEvaluationSubmitted As Boolean = False
    'Hemant (28 Jul 2021) -- End
    'Hemant (23 Sep 2021) -- Start
    'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
    Dim mblnATSkipTrainingRequisitionAndApproval As Boolean = False
    'Hemant (23 Sep 2021) -- End
#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                FillCombo()
                FillTrainingCalendar()
                FillTrainingPriority()
                FillTrainingCategory()
                FillTrainingCost()
                FillTrainingLearning()
                FillTrainingVenue()
                chkCopyAddressAlreadyProvided_CheckedChanged(chkCopyAddressAlreadyProvided, New EventArgs())
                GetOtherSettings()
            Else
                mintTrainingCalendarId = CInt(Me.ViewState("TrainingCalendarId"))
                mintTrainingpriorityId = CInt(Me.ViewState("TrainingpriorityId"))
                mintTrainingCategoryId = CInt(Me.ViewState("TrainingCategoryId"))
                mintTrainingCostId = CInt(Me.ViewState("TrainingCostId"))
                mintTrainingLearningId = CInt(Me.ViewState("TrainingLearningId"))
                mintTrainingVenueId = CInt(Me.ViewState("TrainingVenueId"))
                mblnATAllowToAddTrainingwithoutTNAProcess = CBool(Me.ViewState("ATAllowToAddTrainingwithoutTNAProcess"))
                mblnATAllowToApplyRequestTrainingNotinTrainingPlan = CBool(Me.ViewState("ATAllowToApplyRequestTrainingNotinTrainingPlan"))
                mblnATTrainingRequireForForeignTravelling = CBool(Me.ViewState("ATTrainingRequireForForeignTravelling"))
                'Sohail (08 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                mstrATTrainingNeedAllocationID = ViewState("mstrATTrainingNeedAllocationID")
                'Sohail (08 Apr 2021) -- End
                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation
                mblnATPreTrainingEvaluationSubmitted = ViewState("ATPreTrainingEvaluationSubmitted")
                'Hemant (28 Jul 2021) -- End
                'Hemant (23 Sep 2021) -- Start
                'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                mblnATSkipTrainingRequisitionAndApproval = ViewState("ATSkipTrainingRequisitionAndApproval")
                'Hemant (23 Sep 2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("TrainingCalendarId") = mintTrainingCalendarId
            Me.ViewState("TrainingpriorityId") = mintTrainingpriorityId
            Me.ViewState("TrainingCategoryId") = mintTrainingCategoryId
            Me.ViewState("TrainingCostId") = mintTrainingCostId
            Me.ViewState("TrainingLearningId") = mintTrainingLearningId
            Me.ViewState("TrainingVenueId") = mintTrainingVenueId
            Me.ViewState("ATAllowToAddTrainingwithoutTNAProcess") = mblnATAllowToAddTrainingwithoutTNAProcess
            Me.ViewState("ATAllowToApplyRequestTrainingNotinTrainingPlan") = mblnATAllowToApplyRequestTrainingNotinTrainingPlan
            Me.ViewState("ATTrainingRequireForForeignTravelling") = mblnATTrainingRequireForForeignTravelling
            'Sohail (08 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            Me.ViewState("mstrATTrainingNeedAllocationID") = mstrATTrainingNeedAllocationID
            'Sohail (08 Apr 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            Me.ViewState("ATPreTrainingEvaluationSubmitted") = mblnATPreTrainingEvaluationSubmitted
            'Hemant (28 Jul 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            Me.ViewState("ATSkipTrainingRequisitionAndApproval") = mblnATSkipTrainingRequisitionAndApproval
            'Hemant (23 Sep 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

#Region "Training Calendar"

    Private Sub FillTrainingCalendar()
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim mblnblank As Boolean = False
        Try
            Dim dsList As DataSet = objCalendar.GetList("List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("calendar_name") = ""
                drRow("sdate") = ""
                drRow("edate") = ""
                drRow("description") = ""
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvTrainingCalendar.DataSource = dsList.Tables(0)
            gvTrainingCalendar.DataBind()

            If mblnblank Then gvTrainingCalendar.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

#End Region

#Region "Training Priority"

    Private Sub FillTrainingPriority()
        Dim objPriority As New clsTraining_Priority_Master
        Dim mblnblank As Boolean = False
        Try
            Dim dsList As DataSet = objPriority.GetList("List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("trpriority_name") = ""
                drRow("trpriority") = 0
                drRow("trdefaultypeid") = clsTraining_Priority_Master.enPriorityDefaultTypeId.Custom
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvTrainingPriorities.DataSource = dsList.Tables(0)
            gvTrainingPriorities.DataBind()

            If mblnblank Then gvTrainingPriorities.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPriority = Nothing
        End Try
    End Sub

#End Region

#Region "Training Category"

    Private Sub FillTrainingCategory()
        Dim objCategory As New clsTraining_Category_Master
        Dim mblnblank As Boolean = False
        Try

            Dim dsList As DataSet = objCategory.GetList("List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("categorycode") = ""
                drRow("categoryname") = ""
                drRow("description") = ""
                drRow("trpriorityunkid") = 0
                drRow("categorydefaultypeid") = 0
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvTrainingCategories.DataSource = dsList.Tables(0)
            gvTrainingCategories.DataBind()

            If mblnblank Then gvTrainingCategories.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub

#End Region

#Region "Training Cost"

    Private Sub FillTrainingCost()
        Dim objTrainingCost As New clstrainingitemsInfo_master
        Dim mblnblank As Boolean = False
        Try

            Dim dsList As DataSet = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("infotypeid") = "0"
                drRow("info_code") = ""
                drRow("info_name") = ""
                drRow("description") = ""
                drRow("defaultitemtypeid") = "0"
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvTrainingCosts.DataSource = dsList.Tables(0)
            gvTrainingCosts.DataBind()

            If mblnblank Then gvTrainingCosts.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingCost = Nothing
        End Try
    End Sub

#End Region

#Region "Training Learing Method"

    Private Sub FillTrainingLearning()
        Dim objTrainingLearning As New clstrainingitemsInfo_master
        Dim mblnblank As Boolean = False
        Try
            Dim dsList As DataSet = objTrainingLearning.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Learning_Method, True)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("infotypeid") = "0"
                drRow("info_code") = ""
                drRow("info_name") = ""
                drRow("description") = ""
                drRow("defaultitemtypeid") = "0"
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvLearningMethods.DataSource = dsList.Tables(0)
            gvLearningMethods.DataBind()

            If mblnblank Then gvLearningMethods.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingLearning = Nothing
        End Try
    End Sub

#End Region

#Region "Training Venue"

    Private Sub FillTrainingVenue()
        Dim objTrainingVenue As New clstrtrainingvenue_master
        Dim mblnblank As Boolean = False
        Try
            Dim dsList As DataSet = objTrainingVenue.GetList("List", True)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("venuename") = ""
                drRow("address") = ""
                drRow("country") = ""
                drRow("state") = ""
                drRow("city") = ""
                dsList.Tables(0).Rows.Add(drRow)
                mblnblank = True
            End If

            gvTrainingVenues.DataSource = dsList.Tables(0)
            gvTrainingVenues.DataBind()

            If mblnblank Then gvTrainingVenues.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingVenue = Nothing
        End Try
    End Sub

#End Region

#Region "Other Setting"

    Private Sub GetOtherSettings()
        Dim objConfig As New clsConfigOptions
        Try
            Dim mstrValue As String = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "AllowToAddTrainingwithoutTNAProcess", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = "False"
            mblnATAllowToAddTrainingwithoutTNAProcess = CBool(mstrValue)
            chkTrainingAddedDirectly.Checked = mblnATAllowToAddTrainingwithoutTNAProcess


            'Pinkal (08-Apr-2021)-- Start
            'Enhancement  -  Working on Employee Claim Form Report.
            chkTrainingAddedDirectly.Visible = False
            'Pinkal (08-Apr-2021) -- End


            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "AllowToApplyRequestTrainingNotinTrainingPlan", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = "False"
            mblnATAllowToApplyRequestTrainingNotinTrainingPlan = CBool(mstrValue)
            chkRequestTraining.Checked = mblnATAllowToApplyRequestTrainingNotinTrainingPlan

            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "TrainingRequireForForeignTravelling", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = "False"
            mblnATTrainingRequireForForeignTravelling = CBool(mstrValue)
            chkIsForeignTravelling.Checked = mblnATTrainingRequireForForeignTravelling

            'Sohail (08 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "TrainingNeedAllocationID", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = CInt(enAllocation.DEPARTMENT).ToString
            cboTrainingNeedAllocation.SelectedValue = CInt(mstrValue)
            mstrATTrainingNeedAllocationID = cboTrainingNeedAllocation.SelectedItem.Text
            'Sohail (08 Apr 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "PreTrainingEvaluationSubmitted", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = "False"
            mblnATPreTrainingEvaluationSubmitted = CBool(mstrValue)
            chkIsPreTrainingEvaluationSubmitted.Checked = mblnATPreTrainingEvaluationSubmitted
            'Hemant (28 Jul 2021) -- End

            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            mstrValue = ""
            mstrValue = objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "SkipTrainingRequisitionAndApproval", Nothing)
            If mstrValue.Trim.Length <= 0 Then mstrValue = "False"
            mblnATSkipTrainingRequisitionAndApproval = CBool(mstrValue)
            chkIsSkipTrainingRequisitionAndApproval.Checked = mblnATSkipTrainingRequisitionAndApproval
            'Hemant (23 Sep 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
        End Try
    End Sub

#End Region


    Private Sub FillCombo()
        Try
            Dim objPriority As New clsTraining_Priority_Master
            Dim dsList As DataSet = objPriority.getListForCombo("List", True, Nothing)
            cboPriority.DataTextField = "priority"
            cboPriority.DataValueField = "trpriorityunkid"
            cboPriority.DataSource = dsList.Tables(0)
            cboPriority.DataBind()
            objPriority = Nothing

            dsList = Nothing
            Dim objCountry As New clsMasterData
            dsList = objCountry.getCountryList("List", True, -1, Nothing)
            cboCountry.DataValueField = "countryunkid"
            cboCountry.DataTextField = "country_name"
            cboCountry.DataSource = dsList.Tables(0)
            cboCountry.DataBind()
            'objCountry = Nothing 'Sohail (08 Apr 2021)
            cboCountry_SelectedIndexChanged(cboCountry, New EventArgs())

            dsList = Nothing
            Dim objProvider As New clsinstitute_master
            dsList = objProvider.getListForCombo(False, "List", True, -1, False, 0, False)
            cboTrainingProvider.DataTextField = "name"
            cboTrainingProvider.DataValueField = "instituteunkid"
            cboTrainingProvider.DataSource = dsList.Tables(0)
            cboTrainingProvider.DataBind()
            objProvider = Nothing

            'Sohail (08 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            dsList = objCountry.GetEAllocation_Notification("AList", "", False, False)
            With cboTrainingNeedAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                'Hemant (25 May 2021) -- Start
                'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                '.SelectedValue = (enAllocation.DEPARTMENT).ToString
                .SelectedValue = CStr(enAllocation.DEPARTMENT)
                'Hemant (25 May 2021) -- End
            End With
            'Sohail (07 Apr 2021) -- End


            objCountry = Nothing 'Sohail (08 Apr 2021)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

#Region "Training Calendar "

    Protected Sub btnCSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCSave.Click
        Dim objCalendar As New clsTraining_Calendar_Master
        Try

            If txtCName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Calendar Name is required information. Please add the Calendar Name"), Me)
                txtCName.Focus()
                Exit Sub
            ElseIf dtpCStartDate.IsNull OrElse dtpCEndDate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Start Date and End Date are required. Please set Start Date and End Date"), Me)
                dtpCStartDate.Focus()
                Exit Sub
            End If

            If mintTrainingCalendarId > 0 Then
                objCalendar._Calendarunkid = mintTrainingCalendarId
            Else
                Dim dsList As DataSet = objCalendar.GetList("List", "trtraining_calendar_master.statusunkid = " & enStatusType.OPEN)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot add another calendar. Only one open calendar is allowed at a time"), Me)
                    Exit Sub
                End If
            End If

            objCalendar._CalendarName = txtCName.Text.Trim
            objCalendar._StartDate = dtpCStartDate.GetDate.Date
            objCalendar._EndDate = dtpCEndDate.GetDate.Date
            objCalendar._Description = txtCDescription.Text.Trim
            objCalendar._Isactive = True
            objCalendar._Statusunkid = enStatusType.OPEN
            objCalendar._AuditUserId = CInt(Session("UserId"))
            objCalendar._FormName = mstrModuleName
            objCalendar._ClientIP = CStr(Session("IP_ADD"))
            objCalendar._HostName = CStr(Session("HOST_NAME"))
            objCalendar._FromWeb = True

            Dim mblnFlag As Boolean = True

            If mintTrainingCalendarId > 0 Then
                mblnFlag = objCalendar.Update()
            Else
                mblnFlag = objCalendar.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objCalendar._Message, Me)
                Exit Sub
            Else
                If mintTrainingCalendarId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Training Calendar updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Training Calendar saved successfully"), Me)
                End If
                btnCReset_Click(btnCReset, New EventArgs())
                FillTrainingCalendar()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Protected Sub btnCReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCReset.Click
        Try
            txtCName.Text = ""
            dtpCStartDate.SetDate = Nothing
            dtpCEndDate.SetDate = Nothing
            txtCDescription.Text = ""
            mintTrainingCalendarId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfCalendar_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfCalendar.buttonYes_Click
        Try
            If mintTrainingCalendarId > 0 Then   'Training Calendar
                Dim objCalendar As New clsTraining_Calendar_Master
                objCalendar._Calendarunkid = mintTrainingCalendarId
                objCalendar._AuditUserId = CInt(Session("UserId"))
                objCalendar._FormName = mstrModuleName
                objCalendar._ClientIP = CStr(Session("IP_ADD"))
                objCalendar._HostName = CStr(Session("HOST_NAME"))
                objCalendar._FromWeb = True

                If objCalendar.Delete(mintTrainingCalendarId) = False Then
                    'Hemant (07 Jun 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-393 - Training module bug fixes and enhancements
                    btnCReset_Click(btnCReset, New EventArgs())
                    'Hemant (07 Jun 2021) -- End
                    DisplayMessage.DisplayMessage(objCalendar._Message, Me)
                    Exit Sub
                End If
                objCalendar = Nothing
                btnCReset_Click(btnCReset, New EventArgs())
                FillTrainingCalendar()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Priority"

    Protected Sub btnPSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Dim objPriority As New clsTraining_Priority_Master
        Try

            If txtPPriorityName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Priority Name cannot be blank. Please add Priority Name to Save"), Me)
                txtPPriorityName.Focus()
                Exit Sub
            ElseIf txtPPriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Priority cannot be blank. Please select Priority to Save"), Me)
                txtPPriority.Focus()
                Exit Sub
            End If

            If mintTrainingpriorityId > 0 Then objPriority._Priorityunkid = mintTrainingpriorityId

            objPriority._PriorityName = txtPPriorityName.Text.Trim
            objPriority._Priority = IIf(txtPPriority.Text.Trim.Length <= 0, 0, CInt(txtPPriority.Text))
            objPriority._Isactive = True
            objPriority._AuditUserid = CInt(Session("UserId"))
            objPriority._FormName = mstrModuleName
            objPriority._ClientIp = CStr(Session("IP_ADD"))
            objPriority._HostName = CStr(Session("HOST_NAME"))
            objPriority._IsFromWeb = True

            Dim mblnFlag As Boolean = True

            If mintTrainingpriorityId > 0 Then
                mblnFlag = objPriority.Update()
            Else
                objPriority._DefaultTypeId = clsTraining_Priority_Master.enPriorityDefaultTypeId.Custom
                mblnFlag = objPriority.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objPriority._Message, Me)
                Exit Sub
            Else
                If mintTrainingpriorityId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Training Priority updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Training Priority saved successfully"), Me)
                End If
                btnPReset_Click(btnPReset, New EventArgs())
                FillTrainingPriority()
                FillCombo()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPriority = Nothing
        End Try
    End Sub

    Protected Sub btnPReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPReset.Click
        Try
            txtPPriorityName.Text = ""
            txtPPriority.Text = ""
            mintTrainingpriorityId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfPriority_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfPriority.buttonYes_Click
        Try
            If mintTrainingpriorityId > 0 Then   'Training Priority
                Dim objPriority As New clsTraining_Priority_Master
                objPriority._Priorityunkid = mintTrainingpriorityId
                objPriority._AuditUserid = CInt(Session("UserId"))
                objPriority._FormName = mstrModuleName
                objPriority._ClientIp = CStr(Session("IP_ADD"))
                objPriority._HostName = CStr(Session("HOST_NAME"))
                objPriority._IsFromWeb = True

                If objPriority.Delete(mintTrainingpriorityId) = False Then
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                    btnPReset_Click(btnPReset, New EventArgs())
                    'Hemant (25 May 2021) -- End
                    DisplayMessage.DisplayMessage(objPriority._Message, Me)
                    Exit Sub
                End If
                objPriority = Nothing
                btnPReset_Click(btnPReset, New EventArgs())
                FillTrainingPriority()
                FillCombo()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Category"

    Protected Sub btnCtSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCtSave.Click
        Dim objCategory As New clsTraining_Category_Master
        Try

            If txtCtCategoryCode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Training Category Code cannot be blank. Please add Category Code"), Me)
                txtCtCategoryCode.Focus()
                Exit Sub
            ElseIf txtCtCategoryName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Training Category Name cannot be blank. Please add Category Name"), Me)
                txtCtCategoryName.Focus()
                Exit Sub
            ElseIf CInt(cboPriority.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Priority is required information. Please Select Priority"), Me)
                cboPriority.Focus()
                Exit Sub
            End If

            If mintTrainingCategoryId > 0 Then objCategory._Categoryunkid = mintTrainingCategoryId

            objCategory._CategoryCode = txtCtCategoryCode.Text.Trim
            objCategory._CategoryName = txtCtCategoryName.Text.Trim
            objCategory._Priorityunkid = CInt(cboPriority.SelectedValue)
            objCategory._Description = txtCtDescription.Text.Trim
            objCategory._Isactive = True
            objCategory._AuditUserId = CInt(Session("UserId"))
            objCategory._FormName = mstrModuleName
            objCategory._ClientIP = CStr(Session("IP_ADD"))
            objCategory._HostName = CStr(Session("HOST_NAME"))
            objCategory._FromWeb = True

            Dim mblnFlag As Boolean = True

            If mintTrainingCategoryId > 0 Then
                mblnFlag = objCategory.Update()
            Else
                objCategory._CategoryDefaultypeid = clsTraining_Category_Master.enTrainingCategoryDefaultId.Custom
                mblnFlag = objCategory.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objCategory._Message, Me)
                Exit Sub
            Else
                If mintTrainingCategoryId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Training Category updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Training Category saved successfully"), Me)
                End If
                btnCtReset_Click(btnCtReset, New EventArgs())
                FillTrainingCategory()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategory = Nothing
        End Try
    End Sub

    Protected Sub btnCtReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCtReset.Click
        Try
            txtCtCategoryCode.Text = ""
            txtCtCategoryName.Text = ""
            cboPriority.SelectedValue = 0
            txtCtDescription.Text = ""
            mintTrainingCategoryId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfCategory_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfCategory.buttonYes_Click
        Try
            If mintTrainingCategoryId > 0 Then   'Training Category
                Dim objCategory As New clsTraining_Category_Master
                objCategory._Categoryunkid = mintTrainingCategoryId
                objCategory._AuditUserId = CInt(Session("UserId"))
                objCategory._FormName = mstrModuleName
                objCategory._ClientIP = CStr(Session("IP_ADD"))
                objCategory._HostName = CStr(Session("HOST_NAME"))
                objCategory._FromWeb = True

                If objCategory.Delete(mintTrainingCategoryId) = False Then
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                    btnCtReset_Click(btnCtReset, New EventArgs())
                    'Hemant (25 May 2021) -- End
                    DisplayMessage.DisplayMessage(objCategory._Message, Me)
                    Exit Sub
                End If
                objCategory = Nothing
                btnCtReset_Click(btnCtReset, New EventArgs())
                FillTrainingCategory()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Training Cost"

    Protected Sub btnCoSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoSave.Click
        Dim objTrainingCost As New clstrainingitemsInfo_master
        Try

            If txtCoItemCode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Training Cost Code cannot be blank. Please add Training Cost Code"), Me)
                txtCoItemCode.Focus()
                Exit Sub
            ElseIf txtCoItemName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Training Cost Name cannot be blank. Please add Training Cost Name"), Me)
                txtCoItemName.Focus()
                Exit Sub
            End If

            If mintTrainingCostId > 0 Then objTrainingCost._Infounkid = mintTrainingCostId

            objTrainingCost._Info_Code = txtCoItemCode.Text.Trim
            objTrainingCost._Info_Name = txtCoItemName.Text.Trim
            objTrainingCost._Infotypeid = clstrainingitemsInfo_master.enTrainingItem.Training_Cost
            objTrainingCost._Description = txtCoDescription.Text.Trim
            objTrainingCost._Isactive = True
            objTrainingCost._Userunkid = CInt(Session("UserId"))
            objTrainingCost._WebFormName = mstrModuleName
            objTrainingCost._WebClientIP = CStr(Session("IP_ADD"))
            objTrainingCost._WebHostName = CStr(Session("HOST_NAME"))
            objTrainingCost._IsWeb = True

            Dim mblnFlag As Boolean = True

            If mintTrainingCostId > 0 Then
                mblnFlag = objTrainingCost.Update()
            Else
                mblnFlag = objTrainingCost.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objTrainingCost._Message, Me)
                Exit Sub
            Else
                If mintTrainingCostId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Training Cost updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Training Cost saved successfully"), Me)
                End If
                btnCoReset_Click(btnCoReset, New EventArgs())
                FillTrainingCost()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingCost = Nothing
        End Try
    End Sub

    Protected Sub btnCoReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoReset.Click
        Try
            txtCoItemCode.Text = ""
            txtCoItemName.Text = ""
            txtCoDescription.Text = ""
            mintTrainingCostId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfTrainingCost_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfTrainingCost.buttonYes_Click
        Try
            If mintTrainingCostId > 0 Then   'Training Cost

                Dim objTrainingCost As New clstrainingitemsInfo_master
                objTrainingCost._Infounkid = mintTrainingCostId
                objTrainingCost._Userunkid = CInt(Session("UserId"))
                objTrainingCost._WebFormName = mstrModuleName
                objTrainingCost._WebClientIP = CStr(Session("IP_ADD"))
                objTrainingCost._WebHostName = CStr(Session("HOST_NAME"))
                objTrainingCost._IsWeb = True

                If objTrainingCost.Delete(mintTrainingCostId) = False Then
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                    btnCoReset_Click(btnCoReset, New EventArgs())
                    'Hemant (25 May 2021) -- End
                    DisplayMessage.DisplayMessage(objTrainingCost._Message, Me)
                    Exit Sub
                End If
                objTrainingCost = Nothing
                btnCoReset_Click(btnCoReset, New EventArgs())
                FillTrainingCost()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Learing Method"

    Protected Sub btnLSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLSave.Click
        Dim objLearningMethod As New clstrainingitemsInfo_master
        Try

            If txtLItemCode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Item Code cannot be blank. Please add Item Code"), Me)
                txtLItemCode.Focus()
                Exit Sub
            ElseIf txtLItemName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Item Name cannot be blank. Please add Item Name"), Me)
                txtLItemName.Focus()
                Exit Sub
            End If

            If mintTrainingLearningId > 0 Then objLearningMethod._Infounkid = mintTrainingLearningId

            objLearningMethod._Info_Code = txtLItemCode.Text.Trim
            objLearningMethod._Info_Name = txtLItemName.Text.Trim
            objLearningMethod._Infotypeid = clstrainingitemsInfo_master.enTrainingItem.Learning_Method
            objLearningMethod._Description = txtLDescription.Text.Trim
            objLearningMethod._Isactive = True
            objLearningMethod._Userunkid = CInt(Session("UserId"))
            objLearningMethod._WebFormName = mstrModuleName
            objLearningMethod._WebClientIP = CStr(Session("IP_ADD"))
            objLearningMethod._WebHostName = CStr(Session("HOST_NAME"))
            objLearningMethod._IsWeb = True

            Dim mblnFlag As Boolean = True

            If mintTrainingLearningId > 0 Then
                mblnFlag = objLearningMethod.Update()
            Else
                mblnFlag = objLearningMethod.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objLearningMethod._Message, Me)
                Exit Sub
            Else
                If mintTrainingLearningId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Learning method updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Learning method saved successfully"), Me)
                End If
                btnLReset_Click(btnLReset, New EventArgs())
                FillTrainingLearning()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLearningMethod = Nothing
        End Try
    End Sub

    Protected Sub btnLReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLReset.Click
        Try
            txtLItemCode.Text = ""
            txtLItemName.Text = ""
            txtLDescription.Text = ""
            mintTrainingLearningId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfLearningMethod_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfLearningMethod.buttonYes_Click
        Try
            If mintTrainingLearningId > 0 Then   'Learning Method

                Dim objLearningMethod As New clstrainingitemsInfo_master
                objLearningMethod._Infounkid = mintTrainingLearningId
                objLearningMethod._Userunkid = CInt(Session("UserId"))
                objLearningMethod._WebFormName = mstrModuleName
                objLearningMethod._WebClientIP = CStr(Session("IP_ADD"))
                objLearningMethod._WebHostName = CStr(Session("HOST_NAME"))
                objLearningMethod._IsWeb = True

                If objLearningMethod.Delete(mintTrainingLearningId) = False Then
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                    btnLReset_Click(btnLReset, New EventArgs())
                    'Hemant (25 May 2021) -- End
                    DisplayMessage.DisplayMessage(objLearningMethod._Message, Me)
                    Exit Sub
                End If
                objLearningMethod = Nothing
                btnLReset_Click(btnLReset, New EventArgs())
                FillTrainingLearning()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Venue"

    Protected Sub btnVSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVSave.Click
        Dim objTrainingVenue As New clstrtrainingvenue_master
        Try

            If txtVVenueName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Training Venue Name cannot be blank. Please add Training Venue Name"), Me)
                txtVVenueName.Focus()
                Exit Sub
            ElseIf chkCopyAddressAlreadyProvided.Checked AndAlso CInt(cboTrainingProvider.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Training Provider is compulsory information.Please select Training Provider"), Me)
                cboTrainingProvider.Focus()
                Exit Sub
            End If

            If mintTrainingVenueId > 0 Then objTrainingVenue._Venueunkid = mintTrainingVenueId

            If chkCopyAddressAlreadyProvided.Checked = False AndAlso CInt(cboTrainingProvider.SelectedValue) <= 0 Then
                objTrainingVenue._Venuename = txtVVenueName.Text.Trim
                objTrainingVenue._Address = txtVAddress.Text.Trim
                objTrainingVenue._Countryunkid = CInt(cboCountry.SelectedValue)
                objTrainingVenue._Stateunkid = CInt(cboState.SelectedValue)
                objTrainingVenue._Cityunkid = CInt(cboCity.SelectedValue)
                objTrainingVenue._Contact_Person = txtVContactPerson.Text.Trim
                objTrainingVenue._Fax = txtVFax.Text.Trim
                objTrainingVenue._Telephoneno = txtVTelNo.Text.Trim
                objTrainingVenue._Email = txtVEmail.Text.Trim
            Else
                objTrainingVenue._Venuename = ""
                objTrainingVenue._Address = ""
                objTrainingVenue._Countryunkid = 0
                objTrainingVenue._Stateunkid = 0
                objTrainingVenue._Cityunkid = 0
                objTrainingVenue._Contact_Person = ""
                objTrainingVenue._Fax = ""
                objTrainingVenue._Telephoneno = ""
                objTrainingVenue._Email = ""
            End If

            objTrainingVenue._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
            objTrainingVenue._Isactive = True
            objTrainingVenue._Userunkid = CInt(Session("UserId"))
            objTrainingVenue._WebFormName = mstrModuleName
            objTrainingVenue._WebClientIP = CStr(Session("IP_ADD"))
            objTrainingVenue._WebHostName = CStr(Session("HOST_NAME"))
            objTrainingVenue._IsWeb = True
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            objTrainingVenue._Capacity = txtVCapacity.Text
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            objTrainingVenue._IsLocked = CBool(chkIsLocked.Checked)
            'Hemant (03 Jun 2021) -- End

            Dim mblnFlag As Boolean = True

            If mintTrainingVenueId > 0 Then
                mblnFlag = objTrainingVenue.Update()
            Else
                mblnFlag = objTrainingVenue.Insert()
            End If

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objTrainingVenue._Message, Me)
                Exit Sub
            Else
                If mintTrainingVenueId > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Training Venue updated successfully"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Training Venue saved successfully"), Me)
                End If
                btnVReset_Click(btnVReset, New EventArgs())
                FillTrainingVenue()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingVenue = Nothing
        End Try
    End Sub

    Protected Sub btnVReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVReset.Click
        Try
            chkCopyAddressAlreadyProvided.Checked = False
            chkCopyAddressAlreadyProvided_CheckedChanged(chkCopyAddressAlreadyProvided, New EventArgs())
            cboTrainingProvider.SelectedValue = 0
            cboTrainingProvider_SelectedIndexChanged(cboTrainingProvider, New EventArgs())
            txtVVenueName.Text = ""
            txtVAddress.Text = ""
            txtVContactPerson.Text = ""
            txtVFax.Text = ""
            txtVTelNo.Text = ""
            txtVEmail.Text = ""
            mintTrainingVenueId = 0
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            txtVCapacity.Text = "0"
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            chkIsLocked.Checked = False
            'Hemant (03 Jun 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfVenues_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfVenues.buttonYes_Click
        Try
            If mintTrainingVenueId > 0 Then   'Training Venue
                Dim objVenue As New clstrtrainingvenue_master
                objVenue._Venueunkid = mintTrainingVenueId
                objVenue._Userunkid = CInt(Session("UserId"))
                objVenue._WebFormName = mstrModuleName
                objVenue._WebClientIP = CStr(Session("IP_ADD"))
                objVenue._WebHostName = CStr(Session("HOST_NAME"))
                objVenue._IsWeb = True

                If objVenue.Delete(mintTrainingVenueId) = False Then
                    'Hemant (25 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : Training module bug fixes and enhancements
                    btnVReset_Click(btnVReset, New EventArgs())
                    'Hemant (25 May 2021) -- End
                    DisplayMessage.DisplayMessage(objVenue._Message, Me)
                    Exit Sub
                End If
                objVenue = Nothing
                btnVReset_Click(btnVReset, New EventArgs())
                FillTrainingVenue()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Other Settings"

    Protected Sub btnOSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOSave.Click
        Dim objConfig As New clsConfigOptions
        Try
            objConfig._Companyunkid = CInt(Session("CompanyUnkId"))

            objConfig._AllowToAddTrainingwithoutTNAProcess = chkTrainingAddedDirectly.Checked
            objConfig._AllowToApplyRequestTrainingNotinTrainingPlan = chkRequestTraining.Checked
            objConfig._TrainingRequireForForeignTravelling = chkIsForeignTravelling.Checked
            'Sohail (08 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            objConfig._TrainingNeedAllocationID = CInt(cboTrainingNeedAllocation.SelectedValue)
            'Sohail (08 Apr 2021) -- End
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            objConfig._PreTrainingEvaluationSubmitted = chkIsPreTrainingEvaluationSubmitted.Checked
            'Hemant (28 Jul 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            objConfig._SkipTrainingRequisitionAndApproval = chkIsSkipTrainingRequisitionAndApproval.Checked
            'Hemant (23 Sep 2021) -- End

            Dim mblnFlag As Boolean = objConfig.updateParam()

            'Sohail (08 Apr 2021) -- Start
            'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
            Session("TrainingNeedAllocationID") = CInt(cboTrainingNeedAllocation.SelectedValue)
            'Sohail (08 Apr 2021) -- End


            If mblnFlag Then
                Dim objatConfigOption As New clsAtconfigoption
                objatConfigOption._Companyunkid = CInt(Session("CompanyUnkId"))
                objatConfigOption._Audituserunkid = CInt(Session("UserId"))

                If chkTrainingAddedDirectly.Checked <> mblnATAllowToAddTrainingwithoutTNAProcess Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "AllowToAddTrainingwithoutTNAProcess")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & chkTrainingAddedDirectly.Text & " --> Old Value :- " & mblnATAllowToAddTrainingwithoutTNAProcess.ToString() & " | New Value :- " & chkTrainingAddedDirectly.Checked
                    objatConfigOption.Insert()
                End If

                If chkRequestTraining.Checked <> mblnATAllowToApplyRequestTrainingNotinTrainingPlan Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "AllowToAddTrainingwithoutTNAProcess")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & chkRequestTraining.Text & " --> Old Value :- " & mblnATAllowToApplyRequestTrainingNotinTrainingPlan.ToString() & " | New Value :- " & chkRequestTraining.Checked
                    objatConfigOption.Insert()
                End If

                If chkIsForeignTravelling.Checked <> mblnATTrainingRequireForForeignTravelling Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "TrainingRequireForForeignTravelling")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & chkIsForeignTravelling.Text & " --> Old Value :- " & mblnATTrainingRequireForForeignTravelling.ToString() & " | New Value :- " & chkIsForeignTravelling.Checked
                    objatConfigOption.Insert()
                End If

                'Sohail (08 Apr 2021) -- Start
                'NMB Enhancement : : Changes in Departmental Training Need as per comments in online doc in New UI.
                If cboTrainingNeedAllocation.SelectedItem.Text <> mstrATTrainingNeedAllocationID Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "TrainingNeedAllocationID")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblTrainingNeedAllocation.Text & " -->  Old Value :- " & mstrATTrainingNeedAllocationID & " | New Value :- " & cboTrainingNeedAllocation.SelectedItem.Text
                    objatConfigOption.Insert()
                End If
                'Sohail (08 Apr 2021) -- End

                'Hemant (28 Jul 2021) -- Start             
                'ENHANCEMENT : OLD-293 - Training Evaluation
                If chkIsPreTrainingEvaluationSubmitted.Checked <> mblnATPreTrainingEvaluationSubmitted Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "PreTrainingEvaluationSubmitted")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & chkIsPreTrainingEvaluationSubmitted.Text & " --> Old Value :- " & mblnATPreTrainingEvaluationSubmitted.ToString() & " | New Value :- " & chkIsPreTrainingEvaluationSubmitted.Checked
                    objatConfigOption.Insert()
                    mblnATPreTrainingEvaluationSubmitted = chkIsPreTrainingEvaluationSubmitted.Checked
                End If
                'Hemant (28 Jul 2021) -- End

                'Hemant (23 Sep 2021) -- Start
                'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
                If chkIsSkipTrainingRequisitionAndApproval.Checked <> mblnATSkipTrainingRequisitionAndApproval Then
                    objConfig.GetData(CInt(Session("CompanyUnkId")), "SkipTrainingRequisitionAndApproval")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = "Screen Name : " & Me.Title & " --> " & lblOOtherSettings.Text & " --> " & chkIsSkipTrainingRequisitionAndApproval.Text & " --> Old Value :- " & mblnATSkipTrainingRequisitionAndApproval.ToString() & " | New Value :- " & chkIsSkipTrainingRequisitionAndApproval.Checked
                    objatConfigOption.Insert()
                    mblnATSkipTrainingRequisitionAndApproval = chkIsSkipTrainingRequisitionAndApproval.Checked
                End If
                'Hemant (23 Sep 2021) -- End

                objatConfigOption = Nothing

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Other Settings saved successfully"), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
        End Try
    End Sub

#End Region

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

#Region "Training Calendar "

    Protected Sub gvTrainingCalendar_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTrainingCalendar.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCalendarId = CInt(gvTrainingCalendar.DataKeys(gridRow.RowIndex).Values("calendarunkid").ToString())
                    If mintTrainingCalendarId > 0 Then
                        Dim objCalendar As New clsTraining_Calendar_Master
                        objCalendar._Calendarunkid = mintTrainingCalendarId
                        txtCName.Text = objCalendar._CalendarName.Trim()
                        dtpCStartDate.SetDate = objCalendar._StartDate.Date
                        dtpCEndDate.SetDate = objCalendar._EndDate.Date
                        txtCDescription.Text = objCalendar._Description.Trim()
                        objCalendar = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCalendarId = CInt(gvTrainingCalendar.DataKeys(gridRow.RowIndex).Values("calendarunkid").ToString())
                    cnfCalendar.Message = Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this training calendar? ")
                    cnfCalendar.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvTrainingCalendar_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingCalendar.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhStartDate", False, True)).Text.Trim.Length > 0 AndAlso e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhStartDate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhStartDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhStartDate", False, True)).Text)
                End If
                If e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhCEndDate", False, True)).Text.Trim.Length > 0 AndAlso e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhCEndDate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhCEndDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(getColumnID_Griview(gvTrainingCalendar, "colhCEndDate", False, True)).Text)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Priority"

    Protected Sub gvTrainingPriorities_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingPriorities.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then

                If CInt(gvTrainingPriorities.DataKeys(e.Row.RowIndex)("trdefaultypeid").ToString) <> clsTraining_Priority_Master.enPriorityDefaultTypeId.Custom Then
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("CPDelete"), LinkButton)
                    lnkdelete.Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    
    Protected Sub gvTrainingPriorities_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTrainingPriorities.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingpriorityId = CInt(gvTrainingPriorities.DataKeys(gridRow.RowIndex).Values("trpriorityunkid").ToString())
                    If mintTrainingpriorityId > 0 Then
                        Dim objPriority As New clsTraining_Priority_Master
                        objPriority._Priorityunkid = mintTrainingpriorityId
                        txtPPriorityName.Text = objPriority._PriorityName.Trim()
                        txtPPriority.Text = CInt(objPriority._Priority)
                        objPriority = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingpriorityId = CInt(gvTrainingPriorities.DataKeys(gridRow.RowIndex).Values("trpriorityunkid").ToString())
                    cnfPriority.Message = Language.getMessage(mstrModuleName, 11, "Are you sure you want to delete this training priority ? ")
                    cnfPriority.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Category"

    Protected Sub gvTrainingCategories_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingCategories.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then

                If CInt(gvTrainingCategories.DataKeys(e.Row.RowIndex)("categorydefaultypeid").ToString) <> clsTraining_Category_Master.enTrainingCategoryDefaultId.Custom Then
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("CtDelete"), LinkButton)
                    lnkdelete.Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvTrainingCategories_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTrainingCategories.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCategoryId = CInt(gvTrainingCategories.DataKeys(gridRow.RowIndex).Values("categoryunkid").ToString())
                    If mintTrainingCategoryId > 0 Then
                        Dim objCategory As New clsTraining_Category_Master
                        objCategory._Categoryunkid = mintTrainingCategoryId
                        txtCtCategoryCode.Text = objCategory._CategoryCode.Trim
                        txtCtCategoryName.Text = objCategory._CategoryName.Trim
                        cboPriority.SelectedValue = objCategory._Priorityunkid
                        txtCtDescription.Text = objCategory._Description.Trim
                        objCategory = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCategoryId = CInt(gvTrainingCategories.DataKeys(gridRow.RowIndex).Values("categoryunkid").ToString())
                    cnfCategory.Message = Language.getMessage(mstrModuleName, 17, "Are you sure you want to delete this training category? ")
                    cnfCategory.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Cost"

    Protected Sub gvTrainingCosts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingCosts.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then

                If CInt(gvTrainingCosts.DataKeys(e.Row.RowIndex)("infotypeid").ToString) = clstrainingitemsInfo_master.enTrainingItem.Training_Cost Then
                    If CInt(gvTrainingCosts.DataKeys(e.Row.RowIndex)("defaultitemtypeid").ToString) <> clstrainingitemsInfo_master.enTrainingCost.Custom Then
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("CoDelete"), LinkButton)
                        lnkdelete.Visible = False
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvTrainingCosts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTrainingCosts.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCostId = CInt(gvTrainingCosts.DataKeys(gridRow.RowIndex).Values("infounkid").ToString())
                    If mintTrainingCostId > 0 Then
                        Dim objTrainingCost As New clstrainingitemsInfo_master
                        objTrainingCost._Infounkid = mintTrainingCostId
                        txtCoItemCode.Text = objTrainingCost._Info_Code.Trim
                        txtCoItemName.Text = objTrainingCost._Info_Name.Trim
                        txtCoDescription.Text = objTrainingCost._Description.Trim
                        objTrainingCost = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingCostId = CInt(gvTrainingCosts.DataKeys(gridRow.RowIndex).Values("infounkid").ToString())
                    cnfTrainingCost.Message = Language.getMessage(mstrModuleName, 22, "Are you sure you want to delete this training cost ? ")
                    cnfTrainingCost.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Learing Method"

    Protected Sub gvLearningMethods_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLearningMethods.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then

                If CInt(gvLearningMethods.DataKeys(e.Row.RowIndex)("infotypeid").ToString) = clstrainingitemsInfo_master.enTrainingItem.Learning_Method Then
                    If CInt(gvLearningMethods.DataKeys(e.Row.RowIndex)("defaultitemtypeid").ToString) <> clstrainingitemsInfo_master.enTrainingLearningMethod.Custom Then
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("LDelete"), LinkButton)
                        lnkdelete.Visible = False
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvLearningMethods_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLearningMethods.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingLearningId = CInt(gvLearningMethods.DataKeys(gridRow.RowIndex).Values("infounkid").ToString())
                    If mintTrainingLearningId > 0 Then
                        Dim objLearning As New clstrainingitemsInfo_master
                        objLearning._Infounkid = mintTrainingLearningId
                        txtLItemCode.Text = objLearning._Info_Code.Trim
                        txtLItemName.Text = objLearning._Info_Name.Trim
                        txtLDescription.Text = objLearning._Description.Trim
                        objLearning = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingLearningId = CInt(gvLearningMethods.DataKeys(gridRow.RowIndex).Values("infounkid").ToString())
                    cnfLearningMethod.Message = Language.getMessage(mstrModuleName, 27, "Are you sure you want to delete this learning method ? ")
                    cnfLearningMethod.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Training Venue"

    Protected Sub gvTrainingVenues_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTrainingVenues.RowCommand
        Try
            If e.CommandName = "EditRow" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingVenueId = CInt(gvTrainingVenues.DataKeys(gridRow.RowIndex).Values("venueunkid").ToString())
                    If mintTrainingVenueId > 0 Then
                        Dim objVenue As New clstrtrainingvenue_master
                        objVenue._Venueunkid = mintTrainingVenueId
                        If objVenue._Trainingproviderunkid <= 0 Then
                            txtVVenueName.Text = objVenue._Venuename.Trim
                            txtVAddress.Text = objVenue._Address.Trim
                            cboCountry.SelectedValue = objVenue._Countryunkid
                            cboState.SelectedValue = objVenue._Stateunkid
                            cboCity.SelectedValue = objVenue._Cityunkid
                            txtVTelNo.Text = objVenue._Telephoneno.Trim
                            txtVFax.Text = objVenue._Fax.Trim
                            txtVContactPerson.Text = objVenue._Contact_Person.Trim
                            txtVEmail.Text = objVenue._Email.Trim
                        Else
                            chkCopyAddressAlreadyProvided.Checked = True
                            chkCopyAddressAlreadyProvided_CheckedChanged(chkCopyAddressAlreadyProvided, New EventArgs())
                            cboTrainingProvider.SelectedValue = objVenue._Trainingproviderunkid
                            cboTrainingProvider_SelectedIndexChanged(cboTrainingProvider, New EventArgs())
                        End If
                        'Hemant (03 Jun 2021) -- Start
                        'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
                        txtVCapacity.Text = objVenue._Capacity
                        'Hemant (03 Jun 2021) -- End
                        'Hemant (03 Jun 2021) -- Start
                        'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
                        chkIsLocked.Checked = objVenue._IsLocked
                        'Hemant (03 Jun 2021) -- End
                        objVenue = Nothing
                    End If
                End If
                gridRow = Nothing
            ElseIf e.CommandName = "DeleteRow" Then
                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If gridRow IsNot Nothing Then
                    mintTrainingVenueId = CInt(gvTrainingVenues.DataKeys(gridRow.RowIndex).Values("venueunkid").ToString())
                    cnfVenues.Message = Language.getMessage(mstrModuleName, 32, "Are you sure you want to delete this venue ? ")
                    cnfVenues.Show()
                End If
                gridRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "DropDown Event"

    Protected Sub cboTrainingProvider_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingProvider.SelectedIndexChanged
        Try
            If CInt(cboTrainingProvider.SelectedValue) > 0 Then
                Dim objInstitute As New clsinstitute_master
                objInstitute._Instituteunkid = CInt(cboTrainingProvider.SelectedValue)
                txtVVenueName.Text = objInstitute._Institute_Name.Trim()
                txtVAddress.Text = objInstitute._Institute_Address.Trim()
                cboCountry.SelectedValue = objInstitute._Countryunkid
                cboState.SelectedValue = objInstitute._Stateunkid
                cboCity.SelectedValue = objInstitute._Cityunkid
                txtVTelNo.Text = objInstitute._Telephoneno.Trim()
                txtVFax.Text = objInstitute._Institute_Fax.Trim()
                txtVContactPerson.Text = objInstitute._Contact_Person.Trim()
                txtVEmail.Text = objInstitute._Institute_Email.Trim()
                objInstitute = Nothing

                txtVVenueName.Enabled = False
                txtVAddress.Enabled = False
                cboCountry.Enabled = False
                cboState.Enabled = False
                cboCity.Enabled = False
                txtVTelNo.Enabled = False
                txtVFax.Enabled = False
                txtVContactPerson.Enabled = False
                txtVEmail.Enabled = False
            Else
                txtVVenueName.Enabled = True
                txtVAddress.Enabled = True
                cboCountry.Enabled = True
                cboState.Enabled = True
                cboCity.Enabled = True
                txtVTelNo.Enabled = True
                txtVFax.Enabled = True
                txtVContactPerson.Enabled = True
                txtVEmail.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Dim objState As New clsstate_master
        Try
            Dim dsList As DataSet = objState.GetList("List", True, True, CInt(cboCountry.SelectedValue))
            cboState.DataTextField = "name"
            cboState.DataValueField = "stateunkid"
            cboState.DataSource = dsList.Tables(0)
            cboState.DataBind()
            cboState_SelectedIndexChanged(cboState, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objState = Nothing
        End Try
    End Sub

    Protected Sub cboState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Dim objCity As New clscity_master
        Try
            Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboState.SelectedValue))
            cboCity.DataTextField = "name"
            cboCity.DataValueField = "cityunkid"
            cboCity.DataSource = dsList.Tables(0)
            cboCity.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCity = Nothing
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkCopyAddressAlreadyProvided_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyAddressAlreadyProvided.CheckedChanged
        Try
            cboTrainingProvider.Enabled = chkCopyAddressAlreadyProvided.Checked
            If chkCopyAddressAlreadyProvided.Checked = False Then
                cboTrainingProvider.SelectedValue = 0
                txtVVenueName.Text = ""
                txtVAddress.Text = ""
                txtVContactPerson.Text = ""
                txtVFax.Text = ""
                txtVTelNo.Text = ""
                txtVEmail.Text = ""
                cboCountry.SelectedValue = 0
                cboState.SelectedValue = 0
                cboCity.SelectedValue = 0
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
                txtVCapacity.Text = "0"
                'Hemant (03 Jun 2021) -- End
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
                chkIsLocked.Checked = False
                'Hemant (03 Jun 2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Language._Object.setCaption(Me.lblTrainingPriorities.ID, Me.lblTrainingPriorities.Text)
            Language._Object.setCaption(Me.lblTrainingCategories.ID, Me.lblTrainingCategories.Text)
            Language._Object.setCaption(Me.lblTrainingCosts.ID, Me.lblTrainingCosts.Text)
            Language._Object.setCaption(Me.lblLearningMethods.ID, Me.lblLearningMethods.Text)
            Language._Object.setCaption(Me.lblTrainingVenues.ID, Me.lblTrainingVenues.Text)
            Language._Object.setCaption(Me.lblOtherSettings.ID, Me.lblOtherSettings.Text)

            ' Start Training Calendar
            Language._Object.setCaption(Me.lblCCalendar.ID, Me.lblCCalendar.Text)
            Language._Object.setCaption(Me.lblCName.ID, Me.lblCName.Text)
            Language._Object.setCaption(Me.lblCStartDate.ID, Me.lblCStartDate.Text)
            Language._Object.setCaption(Me.lblCEndDate.ID, Me.lblCEndDate.Text)
            Language._Object.setCaption(Me.lblCDescription.ID, Me.lblCDescription.Text)
            Language._Object.setCaption(Me.btnCSave.ID, Me.btnCSave.Text)
            Language._Object.setCaption(Me.btnCReset.ID, Me.btnCReset.Text)

            Language._Object.setCaption(Me.gvTrainingCalendar.Columns(2).FooterText, Me.gvTrainingCalendar.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCalendar.Columns(3).FooterText, Me.gvTrainingCalendar.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCalendar.Columns(4).FooterText, Me.gvTrainingCalendar.Columns(4).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCalendar.Columns(5).FooterText, Me.gvTrainingCalendar.Columns(5).HeaderText)
            ' End Training Calendar

            ' Start Training Priority
            Language._Object.setCaption(Me.lblPPriorities.ID, Me.lblPPriorities.Text)
            Language._Object.setCaption(Me.lblPPriorityName.ID, Me.lblPPriorityName.Text)
            Language._Object.setCaption(Me.lblPPriority.ID, Me.lblPPriority.Text)
            Language._Object.setCaption(Me.btnPSave.ID, Me.btnPSave.Text)
            Language._Object.setCaption(Me.btnPReset.ID, Me.btnPReset.Text)

            Language._Object.setCaption(Me.gvTrainingPriorities.Columns(2).FooterText, Me.gvTrainingPriorities.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvTrainingPriorities.Columns(3).FooterText, Me.gvTrainingPriorities.Columns(3).HeaderText)
            ' ENd Training Priority
            

            ' Start Training Category
            Language._Object.setCaption(Me.lblCtCategoryCode.ID, Me.lblCtCategoryCode.Text)
            Language._Object.setCaption(Me.lblCtCategoryName.ID, Me.lblCtCategoryName.Text)
            Language._Object.setCaption(Me.lblCtPriority.ID, Me.lblCtPriority.Text)
            Language._Object.setCaption(Me.lblCtDescription.ID, Me.lblCtDescription.Text)
            Language._Object.setCaption(Me.btnCtSave.ID, Me.btnCtSave.Text)
            Language._Object.setCaption(Me.btnCtSave.ID, Me.btnCtSave.Text)

            Language._Object.setCaption(Me.gvTrainingCategories.Columns(2).FooterText, Me.gvTrainingCategories.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCategories.Columns(3).FooterText, Me.gvTrainingCategories.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCategories.Columns(4).FooterText, Me.gvTrainingCategories.Columns(4).HeaderText)
            ' ENd Training Category

            ' Start Training Cost
            Language._Object.setCaption(Me.lblCoTrainingCosts.ID, Me.lblCoTrainingCosts.Text)
            Language._Object.setCaption(Me.lblCoItemCode.ID, Me.lblCoItemCode.Text)
            Language._Object.setCaption(Me.lblCoItemName.ID, Me.lblCoItemName.Text)
            Language._Object.setCaption(Me.lblCoDescription.ID, Me.lblCoDescription.Text)
            Language._Object.setCaption(Me.btnCoSave.ID, Me.btnCoSave.Text)
            Language._Object.setCaption(Me.btnCoReset.ID, Me.btnCoReset.Text)

            Language._Object.setCaption(Me.gvTrainingCosts.Columns(2).FooterText, Me.gvTrainingCosts.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCosts.Columns(3).FooterText, Me.gvTrainingCosts.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvTrainingCosts.Columns(4).FooterText, Me.gvTrainingCosts.Columns(4).HeaderText)
            ' ENd Training Cost

            ' Start Training Learning Method
            Language._Object.setCaption(Me.lblLLearningMethods.ID, Me.lblLLearningMethods.Text)
            Language._Object.setCaption(Me.lblLItemCode.ID, Me.lblLItemCode.Text)
            Language._Object.setCaption(Me.lblLItemName.ID, Me.lblLItemName.Text)
            Language._Object.setCaption(Me.lblLDescription.ID, Me.lblLDescription.Text)
            Language._Object.setCaption(Me.btnLSave.ID, Me.btnLSave.Text)
            Language._Object.setCaption(Me.btnLReset.ID, Me.btnLReset.Text)

            Language._Object.setCaption(Me.gvLearningMethods.Columns(2).FooterText, Me.gvLearningMethods.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvLearningMethods.Columns(3).FooterText, Me.gvLearningMethods.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvLearningMethods.Columns(4).FooterText, Me.gvLearningMethods.Columns(4).HeaderText)
            ' ENd Training Learning Method

            ' Start Training Venue
            Language._Object.setCaption(Me.lblVTrainingVenues.ID, Me.lblVTrainingVenues.Text)
            Language._Object.setCaption(Me.chkCopyAddressAlreadyProvided.ID, Me.chkCopyAddressAlreadyProvided.Text)
            Language._Object.setCaption(Me.lblVTrainingProvider.ID, Me.lblVTrainingProvider.Text)
            Language._Object.setCaption(Me.lblVVenueName.ID, Me.lblVVenueName.Text)
            Language._Object.setCaption(Me.lblVAddress.ID, Me.lblVAddress.Text)
            Language._Object.setCaption(Me.lblVCountry.ID, Me.lblVCountry.Text)
            Language._Object.setCaption(Me.lblVState.ID, Me.lblVState.Text)
            Language._Object.setCaption(Me.lblVCity.ID, Me.lblVCity.Text)
            Language._Object.setCaption(Me.lblVContactPerson.ID, Me.lblVContactPerson.Text)
            Language._Object.setCaption(Me.lblVFax.ID, Me.lblVFax.Text)
            Language._Object.setCaption(Me.lblVTelNo.ID, Me.lblVTelNo.Text)
            Language._Object.setCaption(Me.lblVEmail.ID, Me.lblVEmail.Text)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            Language._Object.setCaption(Me.lblVCapacity.ID, Me.lblVCapacity.Text)
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            Language._Object.setCaption(Me.chkIsLocked.ID, Me.chkIsLocked.Text)
            'Hemant (03 Jun 2021) -- End
            Language._Object.setCaption(Me.btnVSave.ID, Me.btnVSave.Text)
            Language._Object.setCaption(Me.btnVReset.ID, Me.btnVReset.Text)

            Language._Object.setCaption(Me.gvTrainingVenues.Columns(2).FooterText, Me.gvTrainingVenues.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvTrainingVenues.Columns(3).FooterText, Me.gvTrainingVenues.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvTrainingVenues.Columns(4).FooterText, Me.gvTrainingVenues.Columns(4).HeaderText)
            Language._Object.setCaption(Me.gvTrainingVenues.Columns(5).FooterText, Me.gvTrainingVenues.Columns(5).HeaderText)
            Language._Object.setCaption(Me.gvTrainingVenues.Columns(6).FooterText, Me.gvTrainingVenues.Columns(6).HeaderText)
            ' ENd Training Venue


            ' Start Other Settings
            Language._Object.setCaption(Me.lblOOtherSettings.ID, Me.lblOOtherSettings.Text)
            Language._Object.setCaption(Me.chkTrainingAddedDirectly.ID, Me.chkTrainingAddedDirectly.Text)
            Language._Object.setCaption(Me.chkRequestTraining.ID, Me.chkRequestTraining.Text)
            Language._Object.setCaption(chkIsForeignTravelling.ID, Me.chkIsForeignTravelling.Text)
            Language._Object.setCaption(Me.lblTrainingNeedAllocation.ID, Me.lblTrainingNeedAllocation.Text)
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            Language._Object.setCaption(chkIsPreTrainingEvaluationSubmitted.ID, Me.chkIsPreTrainingEvaluationSubmitted.Text)
            'Hemant (28 Jul 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            Language._Object.setCaption(chkIsSkipTrainingRequisitionAndApproval.ID, Me.chkIsSkipTrainingRequisitionAndApproval.Text)
            'Hemant (23 Sep 2021) -- End
            Language._Object.setCaption(Me.btnOSave.ID, Me.btnOSave.Text)
            Language._Object.setCaption(Me.BtnClose.ID, Me.BtnClose.Text)
            ' ENd Other Settings

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
           
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblTrainingPriorities.Text = Language._Object.getCaption(Me.lblTrainingPriorities.ID, Me.lblTrainingPriorities.Text)
            Me.lblTrainingCategories.Text = Language._Object.getCaption(Me.lblTrainingCategories.ID, Me.lblTrainingCategories.Text)
            Me.lblTrainingCosts.Text = Language._Object.getCaption(Me.lblTrainingCosts.ID, Me.lblTrainingCosts.Text)
            Me.lblLearningMethods.Text = Language._Object.getCaption(Me.lblLearningMethods.ID, Me.lblLearningMethods.Text)
            Me.lblTrainingVenues.Text = Language._Object.getCaption(Me.lblTrainingVenues.ID, Me.lblTrainingVenues.Text)
            Me.lblOtherSettings.Text = Language._Object.getCaption(Me.lblOtherSettings.ID, Me.lblOtherSettings.Text)

            ' Start Training Calendar
            Me.lblCCalendar.Text = Language._Object.getCaption(Me.lblCCalendar.ID, Me.lblCCalendar.Text)
            Me.lblCName.Text = Language._Object.getCaption(Me.lblCName.ID, Me.lblCName.Text)
            Me.lblCStartDate.Text = Language._Object.getCaption(Me.lblCStartDate.ID, Me.lblCStartDate.Text)
            Me.lblCEndDate.Text = Language._Object.getCaption(Me.lblCEndDate.ID, Me.lblCEndDate.Text)
            Me.lblCDescription.Text = Language._Object.getCaption(Me.lblCDescription.ID, Me.lblCDescription.Text)
            Me.btnCSave.Text = Language._Object.getCaption(Me.btnCSave.ID, Me.btnCSave.Text)
            Me.btnCReset.Text = Language._Object.getCaption(Me.btnCReset.ID, Me.btnCReset.Text)

            Me.gvTrainingCalendar.Columns(2).HeaderText = Language._Object.getCaption(Me.gvTrainingCalendar.Columns(2).FooterText, Me.gvTrainingCalendar.Columns(2).HeaderText)
            Me.gvTrainingCalendar.Columns(3).HeaderText = Language._Object.getCaption(Me.gvTrainingCalendar.Columns(3).FooterText, Me.gvTrainingCalendar.Columns(3).HeaderText)
            Me.gvTrainingCalendar.Columns(4).HeaderText = Language._Object.getCaption(Me.gvTrainingCalendar.Columns(4).FooterText, Me.gvTrainingCalendar.Columns(4).HeaderText)
            Me.gvTrainingCalendar.Columns(5).HeaderText = Language._Object.getCaption(Me.gvTrainingCalendar.Columns(5).FooterText, Me.gvTrainingCalendar.Columns(5).HeaderText)
            ' End Training Calendar

            ' Start Training Priority
            Me.lblPPriorities.Text = Language._Object.getCaption(Me.lblPPriorities.ID, Me.lblPPriorities.Text)
            Me.lblPPriorityName.Text = Language._Object.getCaption(Me.lblPPriorityName.ID, Me.lblPPriorityName.Text)
            Me.lblPPriority.Text = Language._Object.getCaption(Me.lblPPriority.ID, Me.lblPPriority.Text)
            Me.btnPSave.Text = Language._Object.getCaption(Me.btnPSave.ID, Me.btnPSave.Text)
            Me.btnPReset.Text = Language._Object.getCaption(Me.btnPReset.ID, Me.btnPReset.Text)

            Me.gvTrainingPriorities.Columns(2).HeaderText = Language._Object.getCaption(Me.gvTrainingPriorities.Columns(2).FooterText, Me.gvTrainingPriorities.Columns(2).HeaderText)
            Me.gvTrainingPriorities.Columns(3).HeaderText = Language._Object.getCaption(Me.gvTrainingPriorities.Columns(3).FooterText, Me.gvTrainingPriorities.Columns(3).HeaderText)
            ' ENd Training Priority


            ' Start Training Category
            Me.lblCtCategoryCode.Text = Language._Object.getCaption(Me.lblCtCategoryCode.ID, Me.lblCtCategoryCode.Text)
            Me.lblCtCategoryName.Text = Language._Object.getCaption(Me.lblCtCategoryName.ID, Me.lblCtCategoryName.Text)
            Me.lblCtPriority.Text = Language._Object.getCaption(Me.lblCtPriority.ID, Me.lblCtPriority.Text)
            Me.lblCtDescription.Text = Language._Object.getCaption(Me.lblCtDescription.ID, Me.lblCtDescription.Text)
            Me.btnCtSave.Text = Language._Object.getCaption(Me.btnCtSave.ID, Me.btnCtSave.Text)
            Me.btnCtSave.Text = Language._Object.getCaption(Me.btnCtSave.ID, Me.btnCtSave.Text)

            Me.gvTrainingCategories.Columns(2).HeaderText = Language._Object.getCaption(Me.gvTrainingCategories.Columns(2).FooterText, Me.gvTrainingCategories.Columns(2).HeaderText)
            Me.gvTrainingCategories.Columns(3).HeaderText = Language._Object.getCaption(Me.gvTrainingCategories.Columns(3).FooterText, Me.gvTrainingCategories.Columns(3).HeaderText)
            Me.gvTrainingCategories.Columns(4).HeaderText = Language._Object.getCaption(Me.gvTrainingCategories.Columns(4).FooterText, Me.gvTrainingCategories.Columns(4).HeaderText)
            ' ENd Training Category

            ' Start Training Cost
            Me.lblCoTrainingCosts.Text = Language._Object.getCaption(Me.lblCoTrainingCosts.ID, Me.lblCoTrainingCosts.Text)
            Me.lblCoItemCode.Text = Language._Object.getCaption(Me.lblCoItemCode.ID, Me.lblCoItemCode.Text)
            Me.lblCoItemName.Text = Language._Object.getCaption(Me.lblCoItemName.ID, Me.lblCoItemName.Text)
            Me.lblCoDescription.Text = Language._Object.getCaption(Me.lblCoDescription.ID, Me.lblCoDescription.Text)
            Me.btnCoSave.Text = Language._Object.getCaption(Me.btnCoSave.ID, Me.btnCoSave.Text)
            Me.btnCoReset.Text = Language._Object.getCaption(Me.btnCoReset.ID, Me.btnCoReset.Text)

            Me.gvTrainingCosts.Columns(2).HeaderText = Language._Object.getCaption(Me.gvTrainingCosts.Columns(2).FooterText, Me.gvTrainingCosts.Columns(2).HeaderText)
            Me.gvTrainingCosts.Columns(3).HeaderText = Language._Object.getCaption(Me.gvTrainingCosts.Columns(3).FooterText, Me.gvTrainingCosts.Columns(3).HeaderText)
            Me.gvTrainingCosts.Columns(4).HeaderText = Language._Object.getCaption(Me.gvTrainingCosts.Columns(4).FooterText, Me.gvTrainingCosts.Columns(4).HeaderText)
            ' ENd Training Cost

            ' Start Training Learning Method
            Me.lblLLearningMethods.Text = Language._Object.getCaption(Me.lblLLearningMethods.ID, Me.lblLLearningMethods.Text)
            Me.lblLItemCode.Text = Language._Object.getCaption(Me.lblLItemCode.ID, Me.lblLItemCode.Text)
            Me.lblLItemName.Text = Language._Object.getCaption(Me.lblLItemName.ID, Me.lblLItemName.Text)
            Me.lblLDescription.Text = Language._Object.getCaption(Me.lblLDescription.ID, Me.lblLDescription.Text)
            Me.btnLSave.Text = Language._Object.getCaption(Me.btnLSave.ID, Me.btnLSave.Text)
            Me.btnLReset.Text = Language._Object.getCaption(Me.btnLReset.ID, Me.btnLReset.Text)

            Me.gvLearningMethods.Columns(2).HeaderText = Language._Object.getCaption(Me.gvLearningMethods.Columns(2).FooterText, Me.gvLearningMethods.Columns(2).HeaderText)
            Me.gvLearningMethods.Columns(3).HeaderText = Language._Object.getCaption(Me.gvLearningMethods.Columns(3).FooterText, Me.gvLearningMethods.Columns(3).HeaderText)
            Me.gvLearningMethods.Columns(4).HeaderText = Language._Object.getCaption(Me.gvLearningMethods.Columns(4).FooterText, Me.gvLearningMethods.Columns(4).HeaderText)
            ' ENd Training Learning Method

            ' Start Training Venue
            Me.lblVTrainingVenues.Text = Language._Object.getCaption(Me.lblVTrainingVenues.ID, Me.lblVTrainingVenues.Text)
            Me.chkCopyAddressAlreadyProvided.Text = Language._Object.getCaption(Me.chkCopyAddressAlreadyProvided.ID, Me.chkCopyAddressAlreadyProvided.Text)
            Me.lblVTrainingProvider.Text = Language._Object.getCaption(Me.lblVTrainingProvider.ID, Me.lblVTrainingProvider.Text)
            Me.lblVVenueName.Text = Language._Object.getCaption(Me.lblVVenueName.ID, Me.lblVVenueName.Text)
            Me.lblVAddress.Text = Language._Object.getCaption(Me.lblVAddress.ID, Me.lblVAddress.Text)
            Me.lblVCountry.Text = Language._Object.getCaption(Me.lblVCountry.ID, Me.lblVCountry.Text)
            Me.lblVState.Text = Language._Object.getCaption(Me.lblVState.ID, Me.lblVState.Text)
            Me.lblVCity.Text = Language._Object.getCaption(Me.lblVCity.ID, Me.lblVCity.Text)
            Me.lblVContactPerson.Text = Language._Object.getCaption(Me.lblVContactPerson.ID, Me.lblVContactPerson.Text)
            Me.lblVFax.Text = Language._Object.getCaption(Me.lblVFax.ID, Me.lblVFax.Text)
            Me.lblVTelNo.Text = Language._Object.getCaption(Me.lblVTelNo.ID, Me.lblVTelNo.Text)
            Me.lblVEmail.Text = Language._Object.getCaption(Me.lblVEmail.ID, Me.lblVEmail.Text)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            Me.lblVCapacity.Text = Language._Object.getCaption(Me.lblVCapacity.ID, Me.lblVCapacity.Text)
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            Me.chkIsLocked.Text = Language._Object.getCaption(Me.chkIsLocked.ID, Me.chkIsLocked.Text)
            'Hemant (03 Jun 2021) -- End
            Me.btnVSave.Text = Language._Object.getCaption(Me.btnVSave.ID, Me.btnVSave.Text)
            Me.btnVReset.Text = Language._Object.getCaption(Me.btnVReset.ID, Me.btnVReset.Text)

            Me.gvTrainingVenues.Columns(2).HeaderText = Language._Object.getCaption(Me.gvTrainingVenues.Columns(2).FooterText, Me.gvTrainingVenues.Columns(2).HeaderText)
            Me.gvTrainingVenues.Columns(3).HeaderText = Language._Object.getCaption(Me.gvTrainingVenues.Columns(3).FooterText, Me.gvTrainingVenues.Columns(3).HeaderText)
            Me.gvTrainingVenues.Columns(4).HeaderText = Language._Object.getCaption(Me.gvTrainingVenues.Columns(4).FooterText, Me.gvTrainingVenues.Columns(4).HeaderText)
            Me.gvTrainingVenues.Columns(5).HeaderText = Language._Object.getCaption(Me.gvTrainingVenues.Columns(5).FooterText, Me.gvTrainingVenues.Columns(5).HeaderText)
            Me.gvTrainingVenues.Columns(6).HeaderText = Language._Object.getCaption(Me.gvTrainingVenues.Columns(6).FooterText, Me.gvTrainingVenues.Columns(6).HeaderText)
            ' ENd Training Venue


            ' Start Other Settings
            Me.lblOOtherSettings.Text = Language._Object.getCaption(Me.lblOOtherSettings.ID, Me.lblOOtherSettings.Text)
            Me.chkTrainingAddedDirectly.Text = Language._Object.getCaption(Me.chkTrainingAddedDirectly.ID, Me.chkTrainingAddedDirectly.Text)
            Me.chkRequestTraining.Text = Language._Object.getCaption(Me.chkRequestTraining.ID, Me.chkRequestTraining.Text)
            Me.chkIsForeignTravelling.Text = Language._Object.getCaption(Me.chkIsForeignTravelling.ID, Me.chkIsForeignTravelling.Text)
            Me.lblTrainingNeedAllocation.Text = Language._Object.getCaption(Me.lblTrainingNeedAllocation.ID, Me.lblTrainingNeedAllocation.Text)
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            Me.chkIsPreTrainingEvaluationSubmitted.Text = Language._Object.getCaption(Me.chkIsPreTrainingEvaluationSubmitted.ID, Me.chkIsPreTrainingEvaluationSubmitted.Text)
            'Hemant (28 Jul 2021) -- End
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            Me.chkIsSkipTrainingRequisitionAndApproval.Text = Language._Object.getCaption(Me.chkIsSkipTrainingRequisitionAndApproval.ID, Me.chkIsSkipTrainingRequisitionAndApproval.Text)
            'Hemant (23 Sep 2021) -- End
            Me.btnOSave.Text = Language._Object.getCaption(Me.btnOSave.ID, Me.btnOSave.Text)
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text)
            ' ENd Other Settings

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


   

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Calendar Name is required information. Please add the Calendar Name")
            Language.setMessage(mstrModuleName, 2, "Start Date and End Date are required. Please set Start Date and End Date")
            Language.setMessage(mstrModuleName, 3, "Training Calendar updated successfully")
            Language.setMessage(mstrModuleName, 4, "Training Calendar saved successfully")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot add another calendar. Only one open calendar is allowed at a time")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete this training calendar?")
            Language.setMessage(mstrModuleName, 7, "Priority Name cannot be blank. Please add Priority Name to Save")
            Language.setMessage(mstrModuleName, 8, "Priority cannot be blank. Please select Priority to Save")
            Language.setMessage(mstrModuleName, 9, "Training Priority updated successfully")
            Language.setMessage(mstrModuleName, 10, "Training Priority saved successfully")
            Language.setMessage(mstrModuleName, 11, "Are you sure you want to delete this training priority ?")
            Language.setMessage(mstrModuleName, 12, "Training Category Code cannot be blank. Please add Category Code")
            Language.setMessage(mstrModuleName, 13, "Training Category Name cannot be blank. Please add Category Name")
            Language.setMessage(mstrModuleName, 14, "Priority is required information. Please Select Priority")
            Language.setMessage(mstrModuleName, 15, "Training Category updated successfully")
            Language.setMessage(mstrModuleName, 16, "Training Category saved successfully")
            Language.setMessage(mstrModuleName, 17, "Are you sure you want to delete this training category?")
            Language.setMessage(mstrModuleName, 18, "Training Cost Code cannot be blank. Please add Training Cost Code")
            Language.setMessage(mstrModuleName, 19, "Training Cost Name cannot be blank. Please add Training Cost Name")
            Language.setMessage(mstrModuleName, 20, "Training Cost updated successfully")
            Language.setMessage(mstrModuleName, 21, "Training Cost saved successfully")
            Language.setMessage(mstrModuleName, 22, "Are you sure you want to delete this training cost ?")
            Language.setMessage(mstrModuleName, 23, "Item Code cannot be blank. Please add Item Code")
            Language.setMessage(mstrModuleName, 24, "Item Name cannot be blank. Please add Item Name")
            Language.setMessage(mstrModuleName, 25, "Learning method updated successfully")
            Language.setMessage(mstrModuleName, 26, "Learning method saved successfully")
            Language.setMessage(mstrModuleName, 27, "Are you sure you want to delete this learning method ?")
            Language.setMessage(mstrModuleName, 28, "Training Venue Name cannot be blank. Please add Training Venue Name")
            Language.setMessage(mstrModuleName, 29, "Training Provider is compulsory information.Please select Training Provider")
            Language.setMessage(mstrModuleName, 30, "Training Venue updated successfully")
            Language.setMessage(mstrModuleName, 31, "Training Venue saved successfully")
            Language.setMessage(mstrModuleName, 32, "Are you sure you want to delete this venue ?")
            Language.setMessage(mstrModuleName, 33, "Other Settings saved successfully")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

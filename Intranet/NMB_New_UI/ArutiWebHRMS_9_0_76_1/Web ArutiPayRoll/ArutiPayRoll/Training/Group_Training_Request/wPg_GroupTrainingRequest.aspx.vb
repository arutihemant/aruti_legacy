﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Training_Group_Training_Request_wPg_GroupTrainingRequest
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmGroupTrainingRequest"
    Private mintTrainingRequestunkid As Integer
    Private mintCourseMasterunkid As Integer
    Private mintDepartTrainingNeedId As Integer = -1
    Private mintTrainingNeedAllocationID As Integer = -1
    Private mblnShowTrainingNamePopup As Boolean = False
    Private mintDepartmentListId As Integer = -1
#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                dtpApplicationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mintTrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
                FillCombo()
                FillTrainingCostItem()
                FillFinancingSource()
                Clear_Controls()
            Else
                mintTrainingRequestunkid = CInt(ViewState("mintTrainingRequestunkid"))
                mintCourseMasterunkid = CInt(ViewState("mintCourseMasterunkid"))
                mblnShowTrainingNamePopup = CBool(ViewState("mblnShowTrainingNamePopup"))
                mintDepartmentListId = CInt(ViewState("mintDepartmentListId"))
                mintTrainingNeedAllocationID = CInt(Me.ViewState("mintTrainingNeedAllocationID"))
                mintDepartTrainingNeedId = CInt(Me.ViewState("mintDepartTrainingNeedId"))
            End If
            If mblnShowTrainingNamePopup = True Then
                popupTrainingName.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintTrainingRequestunkid") = mintTrainingRequestunkid
            Me.ViewState("mintCourseMasterunkid") = mintCourseMasterunkid
            Me.ViewState("mblnShowTrainingCostItemPopup") = mblnShowTrainingNamePopup
            Me.ViewState("mintDepartmentListId") = mintDepartmentListId
            Me.ViewState("mintTrainingNeedAllocationID") = mintTrainingNeedAllocationID
            Me.ViewState("mintDepartTrainingNeedId") = mintDepartTrainingNeedId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objTPeriod As New clsTraining_Calendar_Master
        Dim objInstitute As New clsinstitute_master
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Try
            Dim intFirstOpen As Integer = 0
            dsCombo = objTPeriod.getListForCombo("List", False, 1)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                intFirstOpen = CInt(dsCombo.Tables(0).Rows(0).Item("calendarunkid"))
            End If

            dsCombo = objTPeriod.getListForCombo("List", True, 1)
            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "calendarunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpen.ToString
            End With

            dsCombo = objInstitute.getListForCombo(False, "List", True)
            With cboTrainingProvider
                .DataTextField = "name"
                .DataValueField = "instituteunkid"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Call FillTrainingVenueCombo(0)

            Dim strIDs As String = "0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Trim <> "" Then
                Dim arr() As String = Session("Allocation_Hierarchy").ToString.Split(CChar("|"))
                Dim sIDs As String = String.Join(",", (From p In arr.AsEnumerable Where (CInt(p) >= mintTrainingNeedAllocationID) Select (p.ToString)).ToArray)
                If sIDs.Trim <> "" Then
                    strIDs = "0, 9, 10, " & sIDs
                End If
            End If
            dsCombo = objMaster.GetTrainingTargetedGroup("List", strIDs, False, False)

            With cboTargetedGroup
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                If dsCombo.Tables(0).Select("ID = " & mintTrainingNeedAllocationID & " ").Length > 0 Then
                    .SelectedValue = mintTrainingNeedAllocationID.ToString
                Else
                    .SelectedValue = "0"
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
            objInstitute = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillDepartmentTrainingInfo()
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim objDepartTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objCommon As New clsCommon_Master
        Dim objRequestMaster As New clstraining_request_master
        Try
            If mintDepartTrainingNeedId > 0 Then

                objDepartTrainingNeed._Departmentaltrainingneedunkid = mintDepartTrainingNeedId
                FillTrainingVenueCombo(objDepartTrainingNeed._Trainingvenueunkid)
                mintCourseMasterunkid = CInt(objDepartTrainingNeed._Trainingcourseunkid)
                objCommon._Masterunkid = mintCourseMasterunkid
                txtTrainingName.Text = objCommon._Name
                dtpStartDate.SetDate = CDate(objDepartTrainingNeed._Startdate)
                dtpEndDate.SetDate = CDate(objDepartTrainingNeed._Enddate)
                cboTrainingProvider.SelectedValue = CStr(objDepartTrainingNeed._Trainingproviderunkid)
                cboTrainingVenue.SelectedValue = CStr(objDepartTrainingNeed._Trainingvenueunkid)
                
                FillTrainingCostItem()
                txtTotalTrainingCost.Text = Format(CDec(objDepartTrainingNeed._Totalcost), CStr(Session("fmtCurrency")))

                Call FillFinancingSource()
                mintDepartmentListId = CInt(objDepartTrainingNeed._Departmentunkid)
                cboTargetedGroup.SelectedValue = CStr(objDepartTrainingNeed._Targetedgroupunkid)
                txtNoOfStaff.Decimal_ = objDepartTrainingNeed._Noofstaff
                Call cboTargetedGroup_SelectedIndexChanged(cboTargetedGroup, New System.EventArgs)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDFsource = Nothing
            objDepartTrainingNeed = Nothing
            objTCostItem = Nothing
            objRequestMaster = Nothing
        End Try
    End Sub

    Private Sub FillTrainingVenueCombo(ByVal intVenueId As Integer)
        Dim objTVenue As New clstrtrainingvenue_master
        Dim dsCombo As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsCombo = objTVenue.getListForCombo("List", True)
            If intVenueId <= 0 Then
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombo.Tables(0), " islocked = 0 OR (islocked = 1 AND venueunkid = " & intVenueId & " ) ", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboTrainingVenue
                .DataTextField = "name"
                .DataValueField = "venueunkid"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTVenue = Nothing
        End Try
    End Sub

    Private Sub FillTrainingCostItem()
        Dim objTItem As New clstrainingitemsInfo_master
        Dim objTCostItem As New clsDepttrainingneed_costitem_Tran
        Dim objTrainingCost As New clstraining_request_cost_tran
        Dim dsTCostItem As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim dsList As DataSet
        Dim mblnblank As Boolean = False
        Try

            dsList = objTItem.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, True)

            Dim dtCol As New DataColumn
            dtCol = New DataColumn
            dtCol.ColumnName = "trainingrequestcosttranunkid"
            dtCol.Caption = "trainingrequestcosttranunkid"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = -1
            dsList.Tables(0).Columns.Add(dtCol)

            dtCol = New DataColumn
            dtCol.ColumnName = "Amount"
            dtCol.Caption = "Amount"
            dtCol.DataType = System.Type.GetType("System.Decimal")
            dtCol.DefaultValue = 0
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvTrainingCostItem.DataSource = dsList
                dgvTrainingCostItem.DataBind()

                Dim intCellCount As Integer = dgvTrainingCostItem.Rows(0).Cells.Count

                dgvTrainingCostItem.Rows(0).Cells(0).Visible = False
                dgvTrainingCostItem.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvTrainingCostItem.Rows(0).Cells(i).Visible = False
                Next
                dgvTrainingCostItem.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                    For Each drRow As DataRow In dsTCostItem.Tables(0).Rows
                        drRow("Amount") = 0
                    Next
                    dsTCostItem.AcceptChanges()
                    Dim dsDeptCostItem As DataSet = Nothing
                    dsDeptCostItem = objTCostItem.GetList("List", mintDepartTrainingNeedId)
                    dsDeptCostItem.Tables(0).Columns("costitemunkid").ColumnName = "infounkid"
                    Dim strDeptCostIDs As String = String.Join(",", (From p In dsDeptCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                    If strDeptCostIDs.Trim <> "" Then
                        Dim row As List(Of DataRow) = (From p In dsTCostItem.Tables(0) Where (strDeptCostIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                        For Each dtRow As DataRow In row
                            Dim r() As DataRow = dsDeptCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                            If r.Length > 0 Then
                                dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), GUI.fmtCurrency)
                            Else
                                dtRow.Item("Amount") = 0
                            End If
                        Next
                    Else

                    End If
                Else
                    dsTCostItem = objTrainingCost.GetList("List", clstrainingitemsInfo_master.enTrainingItem.Training_Cost, mintTrainingRequestunkid)
                End If

                Dim strIDs As String = String.Join(",", (From p In dsTCostItem.Tables(0) Select (p.Item("infounkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("infounkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        Dim r() As DataRow = dsTCostItem.Tables(0).Select("infounkid = " & CInt(dtRow.Item("infounkid")) & " ")
                        If r.Length > 0 Then
                            dtRow.Item("Amount") = Format(CDec(r(0).Item("Amount")), GUI.fmtCurrency)
                            dtRow.Item("trainingrequestcosttranunkid") = CInt(r(0).Item("trainingrequestcosttranunkid"))
                        Else
                            dtRow.Item("Amount") = 0
                        End If
                    Next
                Else

                End If

                dtTable = New DataView(dsList.Tables(0), "", "Amount DESC, info_name", DataViewRowState.CurrentRows).ToTable

                dgvTrainingCostItem.DataSource = dtTable
                dgvTrainingCostItem.DataBind()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingCost = Nothing
            objTCostItem = Nothing
        End Try
    End Sub

    Private Sub FillFinancingSource()
        Dim objCommon As New clsCommon_Master
        Dim objTFsource As New clstraining_request_financing_sources_tran
        Dim objDFsource As New clsDepttrainingneed_financingsources_Tran
        Dim dsList As DataSet = Nothing
        Dim dsFSource As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try

            dsList = objCommon.GetList(clsCommon_Master.enCommonMaster.SOURCES_FUNDINGS, "List", , True)

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dsList.Tables(0).Columns.Add(dtCol)

            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList.Tables(0).Rows.Add(dsList.Tables(0).NewRow)
                dgvFinancingSource.DataSource = dsList
                dgvFinancingSource.DataBind()

                Dim intCellCount As Integer = dgvFinancingSource.Rows(0).Cells.Count

                dgvFinancingSource.Rows(0).Cells(0).Visible = False
                dgvFinancingSource.Rows(0).Cells(1).ColumnSpan = intCellCount
                For i As Integer = 2 To intCellCount - 1
                    dgvFinancingSource.Rows(0).Cells(i).Visible = False
                Next
                dgvFinancingSource.Rows(0).Cells(1).Text = "No Records Found"
            Else
                If mintDepartTrainingNeedId > 0 Then
                    dsFSource = objDFsource.GetList("List", mintDepartTrainingNeedId)
                    If dsFSource.Tables(0).Columns.Contains("trainingrequestfinancingsourcestranunkid") = False Then
                        dsFSource.Tables(0).Columns.Add("trainingrequestfinancingsourcestranunkid", GetType(System.Int32)).DefaultValue = -1
                    End If
                Else
                    dsFSource = objTFsource.GetList("List", mintTrainingRequestunkid)
                End If

                Dim strIDs As String = String.Join(",", (From p In dsFSource.Tables(0) Select (p.Item("financingsourceunkid").ToString)).ToArray)
                If strIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dsList.Tables(0) Where (strIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                Else
                    'For Each dtRow As DataRow In dsList.Tables(0).Rows
                    '    dtRow.Item("IsChecked") = True
                    'Next
                End If

                dtTable = New DataView(dsList.Tables(0), "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable

                dgvFinancingSource.DataSource = dtTable
                dgvFinancingSource.DataBind()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTFsource = Nothing
            objDFsource = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub FillTargetedGroup()
        Dim objDTAllocation As New clsDepttrainingneed_allocation_Tran
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As New DataSet
        Dim dtTable As DataTable = Nothing
        Dim dsTargetGroup As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim intColType As Integer = 0
        Try

            If dtpStartDate.GetDate = Nothing OrElse dtpEndDate.GetDate = Nothing Then
                dgvEmployee.DataSource = Nothing
                dgvEmployee.DataBind()
                Exit Try
            End If


            Select Case CInt(cboTargetedGroup.SelectedValue)

                Case 0 'Employee Names
                    Dim objEmp As New clsEmployee_Master
                    dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , mintDepartmentListId)

                    intColType = 5

                    dtTable = dsList.Tables(0)
                    objEmp = Nothing

                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " stationunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " deptgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.DEPARTMENT

                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " departmentunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION_GROUP

                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectiongroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.SECTION

                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " sectionunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT_GROUP

                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.UNIT

                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " unitunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.TEAM

                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " teamunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOB_GROUP

                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.JOBS

                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " jobunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable


                Case enAllocation.CLASS_GROUP

                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classgroupunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

                Case enAllocation.CLASSES

                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing


                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " classesunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable



                Case enAllocation.COST_CENTER

                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2

                    Dim sFilter As String = ""
                    If mintTrainingNeedAllocationID = CInt(cboTargetedGroup.SelectedValue) AndAlso mintDepartmentListId > 0 Then
                        sFilter = " costcenterunkid = " & mintDepartmentListId & " "
                    End If
                    dtTable = New DataView(dsList.Tables(0), sFilter, "", DataViewRowState.CurrentRows).ToTable

            End Select

            If intColType = 0 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("code").ColumnName = "Code"
                dtTable.Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                dtTable.Columns("jobunkid").ColumnName = "Id"
                dtTable.Columns("Code").ColumnName = "Code"
                dtTable.Columns("jobname").ColumnName = "Name"
            ElseIf intColType = 2 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("costcentercode").ColumnName = "Code"
                dtTable.Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                dtTable.Columns(1).ColumnName = "Id"
                dtTable.Columns(2).ColumnName = "Code"
                dtTable.Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                dtTable.Columns(0).ColumnName = "Id"
                dtTable.Columns("country_code").ColumnName = "Code"
                dtTable.Columns("country_name").ColumnName = "Name"
            ElseIf intColType = 5 Then
                dtTable.Columns("employeeunkid").ColumnName = "Id"
                dtTable.Columns("employeecode").ColumnName = "Code"
                dtTable.Columns("employeename").ColumnName = "Name"
            End If

            dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()

                Dim intCellCount As Integer = dgvEmployee.Rows(0).Cells.Count

                dgvEmployee.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvEmployee.Rows(0).Cells(i).Visible = False
                Next
                dgvEmployee.Rows(0).Cells(0).Text = "No Records Found"
            Else

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case 0 'Employee Names
                        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
                        dsTargetGroup = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True, "", mintDepartTrainingNeedId)


                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id IN (" & strIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            End If
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                        End If

                        txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                        dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                                  eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                                  True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                                  " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                                  CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                        Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                        If strRequestedEmpIDs.Trim <> "" Then
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id NOT IN (" & strRequestedEmpIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            Else
                                dtTable = Nothing
                            End If
                            txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                        End If

                    Case Else
                        dsTargetGroup = objDTAllocation.GetList("List", mintDepartTrainingNeedId)

                        Dim strIDs As String = String.Join(",", (From p In dsTargetGroup.Tables(0) Select (p.Item("allocationtranunkid").ToString)).ToArray)
                        If strIDs.Trim <> "" Then
                            Dim row As List(Of DataRow) = (From p In dtTable Where (strIDs.Split(CChar(",")).Contains(p.Item("id").ToString) = True) Select (p)).ToList

                            For Each dtRow As DataRow In row
                                dtRow.Item("IsChecked") = True
                            Next
                            Dim xTable As DataTable
                            Dim xRow() As DataRow = Nothing
                            xRow = dtTable.Select("id  IN (" & strIDs & ")")
                            If xRow.Length > 0 Then
                                xTable = xRow.CopyToDataTable()
                                dtTable = xTable
                            Else
                                dtTable = Nothing
                            End If
                        End If
                End Select

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, Name", DataViewRowState.CurrentRows).ToTable
                End If
                dgvEmployee.DataSource = dtTable
                dgvEmployee.DataBind()
                If dtTable IsNot Nothing AndAlso dtTable.Select("IsChecked = 1 ").Length = dgvEmployee.Rows.Count Then
                    CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
                End If

                Call FillAllocEmployee()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDTAllocation = Nothing
            objTrainingRequest = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub FillAllocEmployee()
        Dim objDTEmp As New clsDepttrainingneed_employee_Tran
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As DataSet = Nothing
        Dim dsAllocEmp As DataSet = Nothing
        Dim dsRequestedEmp As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim intColType As Integer = 0
        Dim strFilter As String = ""
        Dim strIDs As String = ""
        Try
            'chkChooseEmployee.Checked = False
            'Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)

            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If

            If dtpStartDate.GetDate.Date = Nothing OrElse dtpEndDate.GetDate.Date = Nothing Then
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                dgvAllocEmp.DataSource = Nothing
                dgvAllocEmp.DataBind()
                Exit Try
            End If


            strIDs = String.Join(",", dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True).Select(Function(x) dgvEmployee.DataKeys(x.RowIndex).Item("id").ToString).ToArray)

            If strIDs.Trim <> "" Then

                Select Case CInt(cboTargetedGroup.SelectedValue)

                    Case enAllocation.BRANCH
                        strFilter &= " AND T.stationunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT_GROUP
                        strFilter &= " AND T.deptgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.DEPARTMENT
                        strFilter &= " AND T.departmentunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION_GROUP
                        strFilter &= " AND T.sectiongroupunkid IN (" & strIDs & ") "

                    Case enAllocation.SECTION
                        strFilter &= " AND T.sectionunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT_GROUP
                        strFilter &= " AND T.unitgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.UNIT
                        strFilter &= " AND T.unitunkid IN (" & strIDs & ") "

                    Case enAllocation.TEAM
                        strFilter &= " AND T.teamunkid IN (" & strIDs & ") "

                    Case enAllocation.JOB_GROUP
                        strFilter &= " AND J.jobgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.JOBS
                        strFilter &= " AND J.jobunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASS_GROUP
                        strFilter &= " AND T.classgroupunkid IN (" & strIDs & ") "

                    Case enAllocation.CLASSES
                        strFilter &= " AND T.classunkid IN (" & strIDs & ") "

                    Case enAllocation.COST_CENTER
                        strFilter &= " AND C.costcenterunkid IN (" & strIDs & ") "

                End Select

            Else
                strFilter &= " AND 1 = 2 "
            End If

            If strFilter.Trim <> "" Then
                strFilter = strFilter.Substring(4)
            End If

            Dim objEmp As New clsEmployee_Master
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , 0, strAdvanceFilterQuery:=strFilter)


            dtTable = dsList.Tables(0)
            objEmp = Nothing

            'dtTable = New DataView(dtTable, "", "employeename", DataViewRowState.CurrentRows).ToTable

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsChecked"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            dtTable.Columns.Add(dtCol)

            If dtTable.Rows.Count <= 0 Then
                dtTable.Rows.Add(dtTable.NewRow)
                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()

                Dim intCellCount As Integer = dgvAllocEmp.Rows(0).Cells.Count

                dgvAllocEmp.Rows(0).Cells(0).ColumnSpan = intCellCount
                For i As Integer = 1 To intCellCount - 1
                    dgvAllocEmp.Rows(0).Cells(i).Visible = False
                Next
                dgvAllocEmp.Rows(0).Cells(0).Text = "No Records Found"
            Else
                If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                    strFilter &= " AND 1 = 2 "
                End If
                dsAllocEmp = objDTEmp.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True, "", mintDepartTrainingNeedId, "")

                Dim strExistIDs As String = String.Join(",", (From p In dsAllocEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strExistIDs.Trim <> "" Then
                    Dim row As List(Of DataRow) = (From p In dtTable Where (strExistIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) = True) Select (p)).ToList

                    For Each dtRow As DataRow In row
                        dtRow.Item("IsChecked") = True
                    Next
                    If row.Count > 0 Then
                        'chkChooseEmployee.Checked = True
                        'Call chkChooseEmployee_CheckedChanged(chkChooseEmployee, New System.EventArgs)
                    End If
                End If

                txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text))
                dsRequestedEmp = objTrainingRequest.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                          eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                          True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                          " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " ", , _
                                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                Dim strRequestedEmpIDs As String = String.Join(",", (From p In dsRequestedEmp.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)
                If strRequestedEmpIDs.Trim <> "" Then
                    Dim xTable As DataTable
                    Dim xRow() As DataRow = Nothing
                    xRow = dtTable.Select("employeeunkid NOT IN (" & strRequestedEmpIDs & ")")
                    If xRow.Length > 0 Then
                        xTable = xRow.CopyToDataTable()
                        dtTable = xTable
                    Else
                        dtTable = Nothing
                    End If
                    txtNoOfVacantStaff.Text = CStr(CInt(txtNoOfStaff.Text) - CInt(dsRequestedEmp.Tables(0).Rows.Count))
                End If

                If dtTable IsNot Nothing Then
                    dtTable = New DataView(dtTable, "", "IsChecked DESC, employeename", DataViewRowState.CurrentRows).ToTable
                End If

                dgvAllocEmp.DataSource = dtTable
                dgvAllocEmp.DataBind()
            End If

            'chkAllocEmpOnlyTicked.Checked = False

            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dtTable = Nothing
            objDTEmp = Nothing
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            mintTrainingRequestunkid = 0
            mintDepartTrainingNeedId = 0
            txtTrainingName.Text = ""
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboTrainingProvider.SelectedValue = "0"
            cboTrainingVenue.SelectedValue = "0"
            'FillTrainingCostItem()
            txtTotalTrainingCost.Text = "0.00"
            FillFinancingSource()
            rdbForeignTravellingYes.Checked = False
            rdbForeignTravellingNo.Checked = True
            txtExpectedReturn.Text = ""
            txtRemarks.Text = ""

            cboTargetedGroup.SelectedValue = "0"
            txtNoOfStaff.Decimal_ = 0
            dgvEmployee.DataSource = Nothing
            dgvEmployee.DataBind()

            dgvAllocEmp.DataSource = Nothing
            dgvAllocEmp.DataBind()
            txtNoOfVacantStaff.Decimal_ = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objTPeriod As New clsTraining_Calendar_Master
        Try
            objTPeriod._Calendarunkid = CInt(cboPeriod.SelectedValue)
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue"), Me)
                Return False
            End If

            If CInt(txtTrainingName.Text.Trim.Length) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue"), Me)
                Return False
            End If

            If dtpApplicationDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue"), Me)
                Return False
            End If

            If dtpStartDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, the Start date is mandatory information. Please select the start date to continue"), Me)
                Return False
            End If

            If dtpEndDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue"), Me)
                Return False
            End If

            If dtpStartDate.GetDate.Date < objTPeriod._StartDate OrElse dtpStartDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Start date should be in between selected period start date and end date"), Me)
                dtpStartDate.Focus()
                Return False
            End If

            If dtpEndDate.GetDate.Date < objTPeriod._StartDate OrElse dtpEndDate.GetDate.Date > objTPeriod._EndDate Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, End date should be in between selected period start date and end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If

            If dtpStartDate.GetDate.Date > dtpEndDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Start date should not be greater than end date"), Me)
                dtpEndDate.Focus()
                Return False
            End If

            If CDec(txtTotalTrainingCost.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Total Training Cost should be greater than Zero"), Me)
                Return False
            End If

            If (rdbForeignTravellingYes.Checked Or rdbForeignTravellingNo.Checked) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, ""Does this Training Require Travelling to Foreign Country?"" is mandatory information. Please select Yes/No to continue"), Me)
                Return False
            End If
            
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTPeriod = Nothing
        End Try
    End Function

    Private Sub SetValue(ByRef objRequestMaster As clstraining_request_master)
        Try
            objRequestMaster._TrainingRequestunkid = mintTrainingRequestunkid
            objRequestMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objRequestMaster._Application_Date = dtpApplicationDate.GetDate
            'objRequestMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objRequestMaster._Coursemasterunkid = mintCourseMasterunkid

            objRequestMaster._IsScheduled = True
            
            objRequestMaster._Start_Date = dtpStartDate.GetDate
            objRequestMaster._End_Date = dtpEndDate.GetDate
            objRequestMaster._Trainingproviderunkid = CInt(cboTrainingProvider.SelectedValue)
            objRequestMaster._Trainingvenueunkid = CInt(cboTrainingVenue.SelectedValue)
            objRequestMaster._TotalTrainingCost = CDec(txtTotalTrainingCost.Text)
            
            objRequestMaster._IsAlignedCurrentRole = False
            objRequestMaster._IsPartofPDP = False

            If rdbForeignTravellingYes.Checked Then
                objRequestMaster._IsForeignTravelling = True
            Else
                objRequestMaster._IsForeignTravelling = False
            End If

            objRequestMaster._ExpectedReturn = CStr(txtExpectedReturn.Text)
            objRequestMaster._Remarks = CStr(txtRemarks.Text)
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
                (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                objRequestMaster._IsSubmitApproval = True
                objRequestMaster._IsSkipTrainingRequestAndApproval = True
            Else
                'Hemant (23 Sep 2021) -- End
            objRequestMaster._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            objRequestMaster._IsSubmitApproval = False
            End If 'Hemant (23 Sep 2021)
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._Userunkid = CInt(Session("UserId"))
                objRequestMaster._LoginEmployeeunkid = -1
            Else
                objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                objRequestMaster._Userunkid = -1
            End If
            objRequestMaster._IsWeb = True
            objRequestMaster._Isvoid = False
            objRequestMaster._Voiddatetime = Nothing
            objRequestMaster._Voidreason = ""
            objRequestMaster._Voiduserunkid = -1
            objRequestMaster._ClientIP = CStr(Session("IP_ADD"))
            objRequestMaster._FormName = mstrModuleName
            objRequestMaster._HostName = CStr(Session("HOST_NAME"))
            If mintDepartTrainingNeedId > 0 Then
                objRequestMaster._DepartmentalTrainingNeedunkid = mintDepartTrainingNeedId
            End If
            'Hemant (23 Sep 2021) -- Start
            'ENHANCEMENT : OLD-470 - Silverlands - New Training Setting to skip Training Requests Process.
            If CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso Me.ViewState("Sender").ToString().ToUpper = "BTNSUBMIT" AndAlso _
               (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRequestMaster._IsEnrollConfirm = True
            Else
                'Hemant (23 Sep 2021) -- End
            objRequestMaster._IsEnrollConfirm = False
            End If 'Hemant (23 Sep 2021)
            objRequestMaster._IsEnrollReject = False
            objRequestMaster._EnrollAmount = 0
         
            objRequestMaster._CompletedStatusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Submit_Click()
        Dim objRequestMaster As New clstraining_request_master
        Dim objRequestCost As New clstraining_request_cost_tran
        Dim dtCostTran As DataTable

        Dim strEmployeeList As String = String.Empty
        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            Dim allEmp As List(Of String)

            If cboTargetedGroup.SelectedValue = "0" Then 'employee names
                gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvEmployee.DataKeys(x.RowIndex)("id").ToString()).ToList
            Else
                gRow = dgvAllocEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAllocEmp"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee."), Me)
                    Exit Sub
                ElseIf gRow.Count > CInt(txtNoOfVacantStaff.Text) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s)."), Me)
                    Exit Sub
                End If

                allEmp = gRow.AsEnumerable().Select(Function(x) dgvAllocEmp.DataKeys(x.RowIndex)("employeeunkid").ToString()).ToList
            End If
            
            strEmployeeList = String.Join(",", allEmp.ToArray)

            SetValue(objRequestMaster)

            If Me.ViewState("Sender").ToString().ToUpper() = "BTNSUBMIT" Then
                objRequestMaster._IsSubmitApproval = True
            Else
                objRequestMaster._IsSubmitApproval = False
            End If

            '*** Training Cost Item ***
            Dim lstTrainingCost As New List(Of clstraining_request_cost_tran)
            Dim objTrainingCost As New clstraining_request_cost_tran

            dtCostTran = objRequestCost._TranDataTable

            gRow = dgvTrainingCostItem.Rows.Cast(Of GridViewRow).Where(Function(x) CDec(CType(x.FindControl("txtamount"), Controls_NumericTextBox).Text) > 0).ToList

            For Each dr As GridViewRow In grow
                Dim drRow As DataRow = dtCostTran.NewRow
                drRow("trainingrequestcosttranunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("trainingrequestcosttranunkid").ToString())
                drRow("trainingcostitemunkid") = CInt(dgvTrainingCostItem.DataKeys(dr.RowIndex)("infounkid").ToString())
                Dim txtamount As Controls_NumericTextBox = CType(dr.FindControl("txtamount"), Controls_NumericTextBox)
                drRow("amount") = CDec(txtamount.Text)

                drRow("Isvoid") = False
                drRow("Voiddatetime") = DBNull.Value
                drRow("Voidreason") = ""
                drRow("Voiduserunkid") = -1
                drRow("voidloginemployeeunkid") = -1
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objRequestMaster._Userunkid = CInt(Session("UserId"))
                Else
                    objRequestMaster._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                End If

                drRow("AUD") = "A"
                dtCostTran.Rows.Add(drRow)

            Next
            dtCostTran.AcceptChanges()

            '*** Financial Sources ***
            Dim lstFinancingSources As New List(Of clstraining_request_financing_sources_tran)
            Dim objFinancingSources As New clstraining_request_financing_sources_tran

            Dim gNewRow As List(Of GridViewRow) = Nothing
            Dim strIDs As String

            grow = dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).ToList
            strIDs = String.Join(",", dgvFinancingSource.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectFSource"), CheckBox).Checked = True).Select(Function(x) dgvFinancingSource.DataKeys(x.RowIndex).Item("masterunkid").ToString).ToArray)

            For Each dgRow As GridViewRow In grow
                objFinancingSources = New clstraining_request_financing_sources_tran

                With objFinancingSources
                    .pintTrainingRequestfinancingsourcestranunkid = -1
                    .pintTrainingRequestunkid = mintTrainingRequestunkid
                    .pintFinancingsourceunkid = CInt(dgvFinancingSource.DataKeys(dgRow.RowIndex)("masterunkid").ToString())

                    .pblnIsweb = True
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        .pintUserunkid = CInt(Session("UserId"))
                        .pintLoginemployeeunkid = 0
                        .pintAuditUserId = CInt(Session("UserId"))
                    Else
                        .pintUserunkid = 0
                        .pintLoginemployeeunkid = CInt(Session("Employeeunkid"))
                        .pintAuditUserId = 0
                    End If
                    .pdtAuditDate = ConfigParameter._Object._CurrentDateAndTime
                    .pstrClientIp = Session("IP_ADD").ToString()
                    .pstrHostName = Session("HOST_NAME").ToString()
                    .pstrFormName = mstrModuleName

                End With

                lstFinancingSources.Add(objFinancingSources)
            Next

            objRequestMaster._lstFinancingSourceNew = lstFinancingSources

            objRequestMaster._Datasource_TrainingCostItem = dtCostTran

            If objRequestMaster.InsertAllByEmployeeList(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CStr(Session("UserAccessModeSetting")), _
                                                       Session("EmployeeAsOnDate").ToString, _
                                                       strEmployeeList, Nothing) = False Then
                DisplayMessage.DisplayMessage(objRequestMaster._Message, Me)
            Else
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                If objRequestMaster._IsSubmitApproval = True AndAlso _
                 ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse _
                   (CBool(Session("SkipTrainingRequisitionAndApproval")) = False)) Then
                    'Hemant (23 Sep 2021) -- [AndAlso ((CBool(Session("SkipTrainingRequisitionAndApproval")) = True AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) OrElse (CBool(Session("SkipTrainingRequisitionAndApproval")) = False) ]
                    Dim dicTrainingRequest As New Dictionary(Of Integer, Integer)
                    Dim dsRequestedEmp As DataSet = objRequestMaster.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                      eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), _
                                                       True, CBool(Session("IsIncludeInactiveEmp")), "Training", , _
                                                       " trtraining_request_master.departmentaltrainingneedunkid = " & mintDepartTrainingNeedId & " AND trtraining_request_master.employeeunkid in ( " & strEmployeeList & " ) ", , _
                                                       CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                    dicTrainingRequest = (From p In dsRequestedEmp.Tables("Training") Select New With {Key .TrainingRequestunkid = CInt(p.Item("trainingrequestunkid")), Key .Employeeunkid = CInt(p.Item("employeeunkid"))}).ToDictionary(Function(x) x.TrainingRequestunkid, Function(y) y.Employeeunkid)

                    objRequestMaster.SendNotificationApproverByEmployeeList(dicTrainingRequest, _
                                                                CInt(IIf(objRequestMaster._MinApprovedPriority > 0, objRequestMaster._MinApprovedPriority, -1)), _
                                                                clstraining_request_master.enEmailType.Training_Approver, _
                                                                CInt(Session("CompanyUnkId")), _
                                                                 txtTrainingName.Text, dtpApplicationDate.GetDate.Date, CInt(Session("Fin_year")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("ArutiSelfServiceURL")), _
                                                                enLoginMode, intLoginByEmployeeId, intUserId)
                End If
                Call Clear_Controls()
                FillTrainingCostItem()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRequestMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Protected Sub btnTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTrainingName.Click

        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAddTrainingName.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Training is compulsory information.Please Check atleast One Training.", Me)
                Exit Sub
            End If
            If gRow Is Nothing OrElse gRow.Count > 1 Then
                DisplayMessage.DisplayMessage("Please Check Only One Training.", Me)
                Exit Sub
            End If
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For Each dgRow As GridViewRow In gRow
                    mintCourseMasterunkid = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("masterunkid").ToString())
                    txtTrainingName.Text = dgvAddTrainingName.DataKeys(dgRow.RowIndex)("name").ToString()
                    mintDepartTrainingNeedId = CInt(dgvAddTrainingName.DataKeys(dgRow.RowIndex)("departmentaltrainingneedunkid").ToString())
                    FillDepartmentTrainingInfo()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally


        End Try

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            cnfConfirm.Message = Language.getMessage(mstrModuleName, 13, "Are you sure you want to submit for approval for this Training Request?")
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Protected Sub cboTrainingCalender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrainingCalender.SelectedIndexChanged
        Dim objDeptTrainingNeed As New clsDepartmentaltrainingneed_master
        Dim objTCategory As New clsTraining_Category_Master
        Dim dsList As DataSet = Nothing
        Try
            Dim strFilter As String = ""

            strFilter = " AND trdepartmentaltrainingneed_master.periodunkid = '" & CInt(cboTrainingCalender.SelectedValue) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.statusunkid, 0)  = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                        " AND ISNULL(trdepartmentaltrainingneed_master.request_statusunkid, 0)  = 0 "
            dsList = objDeptTrainingNeed.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                        , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), CInt(Session("TrainingNeedAllocationID")), "Tranining" _
                                                        , False, strFilter, 0, 0)

           
            If dsList.Tables.Count > 0 Then
                Dim xTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "departmentaltrainingneedunkid", "trainingcategoryunkid", "trainingcategoryname", "trainingcourseunkid", "trainingcoursename", "startdate", "enddate", "allocationtranname", "allocationtranunkid")

                If xTable.Columns.Contains("IsGrp") = False Then
                    xTable.Columns.Add("IsGrp", GetType(System.Boolean)).DefaultValue = False
                End If

                For Each r As DataRow In xTable.Rows
                    r("trainingcoursename") = r("trainingcoursename").ToString() + " - (" + eZeeDate.convertDate(r("startdate").ToString).ToShortDateString + " - " + eZeeDate.convertDate(r("enddate").ToString).ToShortDateString + ") "
                    r("IsGrp") = False
                Next

                xTable.Columns("trainingcourseunkid").ColumnName = "masterunkid"
                xTable.Columns("trainingcoursename").ColumnName = "name"

                Dim dsTCategoryList As DataSet = objTCategory.getListForCombo("List", False)
                Dim dRow As DataRow = Nothing
                For Each drTCategory As DataRow In dsTCategoryList.Tables(0).Rows
                    Dim drRow() As DataRow = xTable.Select("trainingcategoryunkid = " & CInt(drTCategory.Item("categoryunkid")) & " ")
                    If drRow.Length > 0 Then
                        dRow = xTable.NewRow()
                        dRow.Item("departmentaltrainingneedunkid") = -1
                        dRow.Item("trainingcategoryunkid") = CInt(drTCategory.Item("categoryunkid"))
                        dRow.Item("trainingcategoryname") = CStr(drTCategory.Item("categoryname"))
                        dRow.Item("masterunkid") = -1
                        dRow.Item("name") = ""
                        dRow.Item("startdate") = ""
                        dRow.Item("enddate") = ""
                        dRow.Item("allocationtranname") = ""
                        dRow.Item("allocationtranunkid") = -1
                        dRow.Item("IsGrp") = 1
                        xTable.Rows.Add(dRow)
                    End If
                Next

                xTable = New DataView(xTable, "", "trainingcategoryunkid, masterunkid ", DataViewRowState.CurrentRows).ToTable.Copy

                dgvAddTrainingName.DataSource = xTable
                dgvAddTrainingName.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDeptTrainingNeed = Nothing
            objTCategory = Nothing
        End Try
    End Sub

    Protected Sub cboTargetedGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTargetedGroup.SelectedIndexChanged
        Try
            If CInt(cboTargetedGroup.SelectedValue) <= 0 Then 'Employee Names
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = False
                txtNoOfVacantStaff.Visible = False
                pnlAllocEmp.Visible = False
            Else
                dgvEmployee.Enabled = True
                lblNoOfVacantStaff.Visible = True
                txtNoOfVacantStaff.Visible = True
                pnlAllocEmp.Visible = True
            End If
            Call FillTargetedGroup()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Protected Sub ChkAllTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            For Each gvRow As GridViewRow In dgvEmployee.Rows
                CType(gvRow.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = chkSelectAll.Checked
            Next

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkgvSelectTargetedGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'If IsValidData(False, False) = False Then Exit Try

            Dim chkSelect As CheckBox = CType(sender, CheckBox)

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectTargetedGroup"), CheckBox).Checked = True)

            If gRow IsNot Nothing AndAlso dgvEmployee.Rows.Count = gRow.Count Then
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = True
            Else
                CType(dgvEmployee.HeaderRow.FindControl("ChkAllTargetedGroup"), CheckBox).Checked = False
            End If

            Call FillAllocEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event "

    Protected Sub lnkAddTrainingName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTrainingName.Click
        Try
            dgvAddTrainingName.DataSource = Nothing
            dgvAddTrainingName.DataBind()

            Dim objCalender As New clsTraining_Calendar_Master
            Dim dsList As DataSet = objCalender.getListForCombo("List", False, enStatusType.OPEN)
            With cboTrainingCalender
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
            End With
            dsList.Clear()
            dsList = Nothing
            cboTrainingCalender_SelectedIndexChanged(New Object, New EventArgs)


            mblnShowTrainingNamePopup = True
            popupTrainingName.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView "

    Protected Sub dgvAddTrainingName_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAddTrainingName.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(dgvAddTrainingName.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "trainingcategoryname").ToString
                    e.Row.Cells(1).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = 2 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim ChkgvSelect As CheckBox = TryCast(e.Row.FindControl("ChkgvSelect"), CheckBox)
                    ChkgvSelect.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Confirmation"

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnSubmit.ID.ToUpper
                    Call Submit_Click()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblPageHeader.ID, Me.Title)

            Language._Object.setCaption(lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Language._Object.setCaption(lblEmployeeDetails.ID, lblEmployeeDetails.Text)
            Language._Object.setCaption(lblTrainingRequestInfoCaption.ID, lblTrainingRequestInfoCaption.Text)
            Language._Object.setCaption(lblPeriod.ID, lblPeriod.Text)
            Language._Object.setCaption(lblApplicationDate.ID, lblApplicationDate.Text)
            Language._Object.setCaption(lblTrainingName.ID, lblTrainingName.Text)
            Language._Object.setCaption(lblStartDate.ID, lblStartDate.Text)
            Language._Object.setCaption(lblEndDate.ID, lblEndDate.Text)
            Language._Object.setCaption(lblProviderName.ID, lblProviderName.Text)
            Language._Object.setCaption(lblProviderAddress.ID, lblProviderAddress.Text)
            Language._Object.setCaption(lblForeignTravelling.ID, lblForeignTravelling.Text)
            Language._Object.setCaption(lblExpectedReturn.ID, lblExpectedReturn.Text)
            Language._Object.setCaption(lblRemarks.ID, lblRemarks.Text)
            Language._Object.setCaption(lblTrainingCostList.ID, lblTrainingCostList.Text)
            Language._Object.setCaption(lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Language._Object.setCaption(lblFinancingSourceList.ID, lblFinancingSourceList.Text)
            Language._Object.setCaption(lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Language._Object.setCaption(lblTargetedGroup.ID, lblTargetedGroup.Text)
            Language._Object.setCaption(lblNoOfStaff.ID, lblNoOfStaff.Text)
            Language._Object.setCaption(lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Language._Object.setCaption(lblTTrainingName.ID, lblTTrainingName.Text)
            Language._Object.setCaption(lblTrainingCalender.ID, lblTrainingCalender.Text)

            Language._Object.setCaption(btnSubmit.ID, Me.btnSubmit.Text)
            Language._Object.setCaption(btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(btnTrainingName.ID, Me.btnTrainingName.Text)
            Language._Object.setCaption(btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text)

            Language._Object.setCaption(dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            Language._Object.setCaption(dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            Language._Object.setCaption(dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            Language._Object.setCaption(dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            Language._Object.setCaption(dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            Language._Object.setCaption(dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            Language._Object.setCaption(dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)
            

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, Me.Title)

            Me.lblTrainingRequestDetails.Text = Language._Object.getCaption(lblTrainingRequestDetails.ID, lblTrainingRequestDetails.Text)
            Me.lblEmployeeDetails.Text = Language._Object.getCaption(lblEmployeeDetails.ID, lblEmployeeDetails.Text)
            Me.lblTrainingRequestInfoCaption.Text = Language._Object.getCaption(lblTrainingRequestInfoCaption.ID, lblTrainingRequestInfoCaption.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(lblPeriod.ID, lblPeriod.Text)
            Me.lblApplicationDate.Text = Language._Object.getCaption(lblApplicationDate.ID, lblApplicationDate.Text)
            Me.lblTrainingName.Text = Language._Object.getCaption(lblTrainingName.ID, lblTrainingName.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(lblStartDate.ID, lblStartDate.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(lblEndDate.ID, lblEndDate.Text)
            Me.lblProviderName.Text = Language._Object.getCaption(lblProviderName.ID, lblProviderName.Text)
            Me.lblProviderAddress.Text = Language._Object.getCaption(lblProviderAddress.ID, lblProviderAddress.Text)
            Me.lblForeignTravelling.Text = Language._Object.getCaption(lblForeignTravelling.ID, lblForeignTravelling.Text)
            Me.lblExpectedReturn.Text = Language._Object.getCaption(lblExpectedReturn.ID, lblExpectedReturn.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(lblRemarks.ID, lblRemarks.Text)
            Me.lblTrainingCostList.Text = Language._Object.getCaption(lblTrainingCostList.ID, lblTrainingCostList.Text)
            Me.lblTotalTrainingCost.Text = Language._Object.getCaption(lblTotalTrainingCost.ID, lblTotalTrainingCost.Text)
            Me.lblFinancingSourceList.Text = Language._Object.getCaption(lblFinancingSourceList.ID, lblFinancingSourceList.Text)
            Me.lblEmployeeDetailsCaption.Text = Language._Object.getCaption(lblEmployeeDetailsCaption.ID, lblEmployeeDetailsCaption.Text)
            Me.lblTargetedGroup.Text = Language._Object.getCaption(lblTargetedGroup.ID, lblTargetedGroup.Text)
            Me.lblNoOfStaff.Text = Language._Object.getCaption(lblNoOfStaff.ID, lblNoOfStaff.Text)
            Me.lblNoOfVacantStaff.Text = Language._Object.getCaption(lblNoOfVacantStaff.ID, lblNoOfVacantStaff.Text)
            Me.lblTTrainingName.Text = Language._Object.getCaption(lblTTrainingName.ID, lblTTrainingName.Text)
            Me.lblTrainingCalender.Text = Language._Object.getCaption(lblTrainingCalender.ID, lblTrainingCalender.Text)

            Me.btnSubmit.Text = Language._Object.getCaption(btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnTrainingName.Text = Language._Object.getCaption(btnTrainingName.ID, Me.btnTrainingName.Text).Replace("&", "")
            Me.btnCloseTCostItem.Text = Language._Object.getCaption(btnCloseTCostItem.ID, Me.btnCloseTCostItem.Text).Replace("&", "")

            dgvTrainingCostItem.Columns(0).HeaderText = Language._Object.getCaption(dgvTrainingCostItem.Columns(0).FooterText, dgvTrainingCostItem.Columns(0).HeaderText)
            dgvTrainingCostItem.Columns(1).HeaderText = Language._Object.getCaption(dgvTrainingCostItem.Columns(1).FooterText, dgvTrainingCostItem.Columns(1).HeaderText)

            dgvFinancingSource.Columns(1).HeaderText = Language._Object.getCaption(dgvFinancingSource.Columns(1).FooterText, dgvFinancingSource.Columns(1).HeaderText)

            dgvEmployee.Columns(1).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            dgvEmployee.Columns(2).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)

            dgvAllocEmp.Columns(1).HeaderText = Language._Object.getCaption(dgvAllocEmp.Columns(1).FooterText, dgvAllocEmp.Columns(1).HeaderText)
            dgvAllocEmp.Columns(2).HeaderText = Language._Object.getCaption(dgvAllocEmp.Columns(2).FooterText, dgvAllocEmp.Columns(2).HeaderText)

            dgvAddTrainingName.Columns(1).HeaderText = Language._Object.getCaption(dgvAddTrainingName.Columns(1).FooterText, dgvAddTrainingName.Columns(1).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Calendar is mandatory information. Please select Calendar to continue")
            Language.setMessage(mstrModuleName, 2, "Sorry, Training Name is mandatory information. Please select Training Name to continue")
            Language.setMessage(mstrModuleName, 3, "Sorry, Application date is mandatory information. Please select Application date to continue")
            Language.setMessage(mstrModuleName, 4, "Sorry, the Start date is mandatory information. Please select the start date to continue")
            Language.setMessage(mstrModuleName, 5, "Sorry, the End date is mandatory information. Please select the end date to continue")
            Language.setMessage(mstrModuleName, 6, "Sorry, Start date should be in between selected period start date and end date")
            Language.setMessage(mstrModuleName, 7, "Sorry, End date should be in between selected period start date and end date")
            Language.setMessage(mstrModuleName, 8, "Sorry, Start date should not be greater than end date")
            Language.setMessage(mstrModuleName, 9, "Sorry, Total Training Cost should be greater than Zero")
            Language.setMessage(mstrModuleName, 10, "Sorry, ""Does this Training Require Travelling to Foreign Country?"" is mandatory information. Please select Yes/No to continue")
            Language.setMessage(mstrModuleName, 11, "Employee is compulsory information.Please check atleast one employee.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Selected Employees should not be greater than Number of Vacant Staff(s).")
            Language.setMessage(mstrModuleName, 13, "Are you sure you want to submit for approval for this Training Request?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

﻿<%@ Page Title="Job Family Competencies" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_Family_Competencies_Report.aspx.vb" Inherits="Reports_Competencies_Reports_Rpt_Family_Competencies_Report" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function AllocSearching() {
            if ($('#txtSearchAlloc').val().length > 0) {
                $('#<%=lvAlloc.ClientID %> tbody tr').hide();
                $('#<%=lvAlloc.ClientID %> tbody tr:first').show();
                $('#<%=lvAlloc.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchAlloc').val() + '\')').parent().show();
            }
            else if ($('#txtSearchAlloc').val().length == 0) {
                resetAllocValue();
            }

            if ($('#<%=lvAlloc.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetAllocValue();
            }
        }

        function resetAllocValue() {
            $('#lvAlloc').val('');
            $('#<%= lvAlloc.ClientID %> tr').show();
            $('.norecords').remove();
            $('#lvAlloc').focus();
        }

        $("body").on("click", "[id*=chkHeader]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        function FromSearching() {
            if ($('#txtEmployeeSearch').val().length > 0) {
                $('#<%=lvCmpts.ClientID %> tbody tr').hide();
                $('#<%=lvCmpts.ClientID %> tbody tr:first').show();
                $('#<%=lvCmpts.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtEmployeeSearch').val() + '\')').parent().show();
            }
            else if ($('#txtEmployeeSearch').val().length == 0) {
                resetFromSearchValue();
            }

            if ($('#<%=lvCmpts.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#lvCmpts').val('');
            $('#<%= lvCmpts.ClientID %> tr').show();
            $('.norecords').remove();
            $('#lvCmpts').focus();
        }


        $("body").on("click", "[id*=ChkAllSelectedEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkSelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            debugger;
            if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
    </script>

    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Job Family Competencies"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAllocRef" runat="server" Text="Allocation"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboAllocation" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtSearchAlloc" name="txtSearch" placeholder="type search text"
                                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="AllocSearching();" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 400px;">
                                                <asp:GridView ID="lvAlloc" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="Id,name">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeader" runat="server" CssClass="filled-in" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="name" HeaderText="" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblCompetencyCategory" runat="server" Text="Competency Category"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCmptCategory" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtEmployeeSearch" name="txtSearch" placeholder="type search text"
                                                        maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="max-height: 400px;">
                                                <asp:GridView ID="lvCmpts" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false" DataKeyNames="competenciesunkid,name">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="name" HeaderText="Competencies" FooterText="objdgCmpts" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

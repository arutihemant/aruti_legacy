﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_IssuedLeave_Status_Report.aspx.vb" Inherits="Reports_Rpt_IssuedLeave_Status_Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:Panel ID="Panel1" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                 <div class="row clearfix d--f fd--c ai--c">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                       <asp:Label ID="lblPageHeader" runat="server" Text="Leave Issued Report"></asp:Label>
                                    </h2>
                                </div>
                             <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                              <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                               <uc2:DateCtrl ID="dtpStartdate" runat="server"  AutoPostBack="false" />
                                    </div>
                                     <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                     </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                              <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                              </div>
                                    </div>
                                </div>   
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <asp:Label ID="lblLeaveName" runat="server" Text="Leave" CssClass="form-label"></asp:Label>
                                             <div class="form-group">
                                                    <asp:DropDownList ID="cboLeave" runat="server" />
                                             </div>
                                    </div>
                                </div>  
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblLeaveFormNo" runat="server" Text="Leave Form No" CssClass="form-label"></asp:Label>
                                             <div class="form-group">
                                                    <asp:DropDownList ID="cboLeaveFormNo" runat="server" />
                                             </div>
                                    </div>
                                </div>      
                            </div>  
                            <div class="footer">
                                   <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                   <asp:Button ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" />
                                   <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>     
                        </div>
                 </div>           
                
                
                  <%--  <div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                              
                                            </td>
                                            <td style="width: 75%">
                                             
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                               
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>


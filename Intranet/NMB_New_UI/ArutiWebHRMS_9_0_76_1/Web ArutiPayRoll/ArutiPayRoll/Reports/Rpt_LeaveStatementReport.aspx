﻿<%@ Page Language="VB"  MasterPageFile="~/Home1.master"  AutoEventWireup="false" CodeFile="Rpt_LeaveStatementReport.aspx.vb" Inherits="Reports_Rpt_LeaveStatementReport" title="Leave Statement Report" %>
<%@ Register src="~/Controls/DateCtrl.ascx" tagname="DateCtrl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Panel ID="Panel1" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Statement Report"></asp:Label>
                                    </h2>
                                </div>
                                <div class="body">
                                           <div class="row clearfix">
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                      <asp:Label ID = "LblAsOnDate" runat = "server" Text = "As On Date" CssClass="form-label"></asp:Label>
                                                       <uc2:DateCtrl ID = "dtpAsonDate" Width="75" runat = "server" AutoPostBack="false" />
                                               </div>
                                         </div>    
                                          <div class="row clearfix">
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                                                       <asp:Label ID = "LblEmployee" runat = "server" Text = "Employee"  CssClass="form-label"></asp:Label>
                                                       <div class="form-group">
                                                             <asp:DropDownList ID = "cboEmployee" runat = "server" />
                                                       </div>
                                               </div>
                                          </div>
                                          <div class="row clearfix">
                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">     
                                                       <asp:Label ID = "lblLeaveName" runat = "server" Text = "Leave"  CssClass="form-label"></asp:Label> 
                                                       <div class="form-group">
                                                                <asp:DropDownList ID = "cboLeave" runat = "server" />
                                                       </div>
                                               </div>
                                         </div>      
                                </div>
                                <div class="footer">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                </div>
                            </div>
                        </div>
                  </div>          
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
 </asp:Content>
 
 

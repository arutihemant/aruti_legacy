﻿<%@ Page Title="Assessment Score Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_Assessment_Score_Report.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_Assessment_Score_Report" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        function checkHeader(control) {
            var chkHeader = control;
            var grid = control.closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        }

        function checkRowCheckbox(control, chkboxID, headerchkboxID) {
            var grid = control.closest("table");
            var chkHeader = $("[id*=" + headerchkboxID + "]", grid);
            var row = control.closest("tr")[0];

            debugger;
            if (!control.is(":checked")) {
                var row = control.closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {
                if ($("[id*=" + chkboxID + "]", grid).length == $("[id*=" + chkboxID + "]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        }

        //$("[id*=chkpHeder]").live("click", function () {
        $("body").on("click", '[id*=chkpHeder]', function() {
            checkHeader($(this));
        });

        //$("[id*=chkpbox]").live("click", function() {
        $("body").on("click", '[id*=chkpbox]', function() {
            checkRowCheckbox($(this), 'chkpbox', 'chkpHeder');
        });


        //$("[id*=chkaHeder]").live("click", function () {
        $("body").on("click", '[id*=chkaHeder]', function() {
            checkHeader($(this));
        });

        //$("[id*=chkabox]").live("click", function () {
        $("body").on("click", '[id*=chkabox]', function() {
            checkRowCheckbox($(this), 'chkabox', 'chkaHeder');
        });

        //$("[id*=chkHeder]").live("click", function() {
        $("body").on("click", '[id*=chkHeder]', function() {
            checkHeader($(this));
        });

        //$("[id*=chkbox]").live("click", function () {
        $("body").on("click", '[id*=chkbox]', function() {
            checkRowCheckbox($(this), 'chkbox', 'chkHeder');
        });

        function pageLoad(sender, args) {
            $("select").searchable();
        }
   
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Score Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" TextAlign="Right"
                                            Text="Consider Employee Active Status On Period Date" />
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblRType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblSubRType" runat="server" Text="Sub Report Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboSubReportType" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboScoreOption" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblAllocation" runat="server" Text="Allocations" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboAllocations" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlAllocation" runat="server" Width="100%">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 220px">
                                                        <asp:DataGrid ID="lvAllocation" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkaHeder" runat="server" Enabled="true" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkabox" runat="server" Enabled="true" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Name" />
                                                                <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="objsp1" runat="server" Width="100%">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblYear" runat="server" Text="Financial Year" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="drpFinancialYear" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 430px">
                                                        <asp:DataGrid ID="dgvPeriod" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="25px">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkpHeder" runat="server" Enabled="true" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkpbox" runat="server" Enabled="true" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Name" HeaderText="Period" FooterText="dgcolhPeriod" />
                                                                <asp:BoundColumn DataField="Periodunkid" HeaderText="objdgcolhPId" Visible="false"
                                                                    FooterText="objdgcolhPId" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlCondition" runat="server" Width="100%">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:CheckBox ID="chkOption" runat="server" Text="Consider on Probation if App. Date is"
                                                        AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOverallCondition" runat="server" Text="Condition"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboCondition" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-20">
                                                    <datectrl:uc ID="dtpAppDate" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 650px">
                                                        <asp:DataGrid ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkHeder" runat="server" Enabled="true" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkbox" runat="server" Enabled="true" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Name" HeaderText="Display Column On Report" />
                                                                <asp:BoundColumn DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                                <asp:BoundColumn DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                                <asp:BoundColumn DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                                <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-l-10">
                                                    <asp:CheckBox ID="chkShowEmpSalary" runat="server" Text="Show Employee Scale On Report" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

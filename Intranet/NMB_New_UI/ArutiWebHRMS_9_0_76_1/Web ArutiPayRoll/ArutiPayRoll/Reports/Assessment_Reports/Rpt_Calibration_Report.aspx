﻿<%@ Page Title="Assessemt Calibration Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_Calibration_Report.aspx.vb" Inherits="Reports_Rpt_Calibration_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">


        //$("[id*=chkHeder1]").live("click", function() {
        $("body").on("click", '[id*=chkHeder1]', function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        //$("[id*=chkbox1]").live("click", function() {
        $("body").on("click", '[id*=chkbox1]', function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        //$("[id*=chkaHeder1]").live("click", function() {
        $("body").on("click", '[id*=chkaHeder1]', function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        //$("[id*=chkabox1]").live("click", function() {
        $("body").on("click", '[id*=chkabox1]', function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
    </script>

        <asp:Panel ID="Panel1" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="row clearfix d--f fd--c ai--c">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        <asp:Label ID="lblPageHeader" runat="server" Text="Assessemt Calibration Report"></asp:Label>
                                    </h2>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                 <i class="fas fa-filter"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboPeriod" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblReportPlanning" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboEmployee" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblProvisionalRating" runat="server" Text="Provisional Rating" CssClass="form-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblCalibrationRating" runat="server" Text="Calibration Rating" CssClass="form-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No" CssClass="form-label"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Panel ID="Panel2" runat="server" Width="100%" Height="245px" ScrollBars="Auto">
                                                        <asp:GridView ID="dgvRating" runat="server" AutoGenerateColumns="False" 
                                                            CssClass="table table-hover table-bordered" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="scrf,scrt,id,calibratnounkid">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkaHeder1" runat="server" Enabled="true" Text = " " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkabox1" runat="server" Enabled="true" Text = " " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating"></asp:BoundField>
                                                                <asp:BoundField DataField="calibration_no" HeaderText="Calibration No" FooterText="dgcolhCalibrationNo">
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Panel ID="Panel3" runat="server" Width="100%" Height="515px" ScrollBars="Auto">
                                                <asp:GridView ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                     AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="objcolhSelectCol,objcolhJoin,objcolhDisplay,Id,Name">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="25">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" Text = " " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" Text = " " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="Display Column On Report" />
                                                        <asp:BoundField DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                        <asp:BoundField DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                        <asp:BoundField DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btn btn-primary" />
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                    <asp:Button ID="BtnReport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    </div>
                     <uc1:Export runat="server" ID="Export" />
                </ContentTemplate>
                  <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="RptGrievanceDetailReport.aspx.vb"
    Inherits="Reports_Grievance_RptGrievanceDetailReport" Title="Grievance Detail Report" %>

<%--<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>--%>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Detail Report"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                      <i class="fas fa-filter"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblfromdate" runat="server" Text="From Date"></asp:Label>
                                        <uc3:DateCtrl ID="dtfromdate" runat="server" AutoPostBack="false" />
                                    </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lbltodate" runat="server" Text="To"></asp:Label>
                                        <uc3:DateCtrl ID="dttodate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblfromemployee" runat="server" Text="From Employee"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpfromemployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                    </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblaginstemployee" runat="server" Text="Against Employee"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpaginstemployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                    </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblrefno" runat="server" Text="Ref. No"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtrefno" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkshowcommitee" runat="server" Text="Show Commitee Members" />
                                </div>
                                    </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblgrestatus" runat="server" Text="Status"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpgrestatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <div class="footer">
                            <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                            <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-default" />
                            <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeRelation.aspx.vb"
    Inherits="Reports_Rpt_EmployeeRelation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Relation Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpReportType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblAsonDate" runat="server" Text="As on Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAsonDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblCondition" runat="server" Text="Condition" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCondition" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRelation" runat="server" Text="Relation" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpRelation" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeeCode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtCode" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlname" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtName" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlfname" runat="server">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFirstname" runat="server" Text="First Name" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLastname" runat="server" Text="Last Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                            Checked="false"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkshowdependantswithoutphotos" runat="server" Text="Show Dependants Without Photos/Picture"
                                            Checked="false"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowExemptedDependents" runat="server" Text="Show Only Exempted Dependents"
                                            AutoPostBack="true" Checked="false"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowSubGrandTotal" runat="server" Text="Show Sub And Grand Total Relation Wise"
                                            Checked="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lnInActiveDependentOption" runat="server" Text="InActive Dependent Options"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkIncludeInactiveDependents" runat="server" AutoPostBack="true"
                                                            Checked="false" Text="Include Inactive Dependents" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowInactiveDependents" runat="server" AutoPostBack="true" Checked="false"
                                                            Text="Show Inactive Dependent Only" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:RadioButtonList ID="rdInActiveOption" runat="server" RepeatDirection="Vertical"
                                                            AutoPostBack="true">
                                                            <asp:ListItem Text="Age Limit Wise" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Dependant Exception Wise" Value="2" Selected="false"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Home1.master" CodeFile="wPg_ExpenseAdjustment.aspx.vb"
    Title="Employee Expense Adjustment" Inherits="Claims_And_Expenses_wPg_ExpenseAdjustment" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="Allocation" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkselect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkselect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkselect]", grid).length == $("[id*=chkselect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
            if (cval.indexOf("-") > -1)
                   return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 44 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Expense Adjustment"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown p-l-0">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocation" Font-Underline="false">
                                                 <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblExpenseCategory" runat="server" Text="Category"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboExpense" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:RadioButtonList ID="rdOption" runat="server" RepeatDirection="Vertical">
                                                            <asp:ListItem Value="1" Text="Apply to All"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Apply to checked"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDate" runat="server" Text="Date"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpDate" runat="server" Width="95" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblAdjustmentAmount" runat="server" Text="Adjustment Amount"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtAdjustmentAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this,event);"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSet" runat="server" Text="Set" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 300px">
                                            <asp:GridView ID="dgEmployee" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false"  DataKeyNames="employeeunkid,crexpbalanceunkid,Isopenelc,Iselc,Isclose_Fy">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkselect" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgColhEmployee" />
                                                    <asp:BoundField DataField="appointeddate" HeaderText="Appointment Date" FooterText="dgColhAppointmentDt" />
                                                    <asp:BoundField DataField="Job" HeaderText="Job Title" FooterText="dgColhJobtitle" />
                                                    <asp:BoundField DataField="Accrue_amount" HeaderText="Accrue Amount" FooterText="dgColhAccrueAmt"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign = "Right" />
                                                    <asp:BoundField DataField="Issue_amount" HeaderText="Issue Amount" FooterText="dgcolhIssueAmt"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign = "Right" />
                                                    <asp:BoundField DataField="Remaining_Bal" HeaderText="Remaining Balance" FooterText="dgcolhRemaining_Bal"
                                                         HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign = "Right" />
                                                    <asp:BoundField DataField="Adjustment_Amt" HeaderText="Adjustment Amount" FooterText="dgcolhAdjustmentAmt"
                                                         HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign = "Right"  />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" Text="" Rows="2" TextMode="MultiLine"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--  <div class="panel-primary" style="width: 95%;">
                    <div class="panel-heading">
                       
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    
                                </div>
                                <div style="text-align: right">
                                  
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div class="ib" style="width: 50%; vertical-align: top; border-right: 1px solid #ccc;
                                        height: 100%">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                             
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                               
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                               
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                               
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                                
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibwm" style="width: 48%;">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 100%">
                                               
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 10%;">
                                              
                                            </div>
                                            <div class="ibwm" style="width: 30%">
                                             
                                            </div>
                                            <div class="ibwm" style="width: 30%;">
                                              
                                            </div>
                                            <div class="ibwm" style="width: 27%;">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-default">
                                  
                                </div>
                            </div>
                        </div>
                       <%-- <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-body-default">
                                <asp:Panel ID="pnlEmpDetails" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                   
                                </asp:Panel>
                                <div class="row2">
                                    <div class="ibwm" style="width: 15%;">
                                       
                                    </div>
                                    <div class="ibwm" style="width: 80%;">
                                       
                                    </div>
                                </div>
                                <div class="btn-default">
                                  
                                </div>
                            </div>
                        </div>--%>
                <uc2:Allocation ID="popupAdvanceFilter" runat="server" Title="Advance Filter" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

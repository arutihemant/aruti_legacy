﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_SickSheetList.aspx.vb"
    Inherits="Medical_wPg_SickSheetList" MasterPageFile="~/Home1.master" Title="Sick Sheet List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Sick Sheet List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSickSheetNo" runat="server" Text="Sick Sheet No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSickSheet" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpJob" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblSickSheetDate" runat="server" Text="Sick Sheet Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFromdate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblToSickSheetDate" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" />
                                        </div>
                                    </div>
                                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMedicalCover" runat="server" Text="Medical Cover" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpMedicalCover" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblProvider" runat="server" Text="Provider" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpProvider" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" CssClass="btn btn-primary" runat="server" Text="New" />
                                <asp:Button ID="BtnSearch" CssClass="btn btn-default" runat="server" Text="Search" />
                                <asp:Button ID="BtnReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="gvSickSheet" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" ToolTip="Edit" CommandName="Select" runat="server"
                                                                    CssClass="gridedit">
                                                                              <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn  HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" ToolTip="Delete" CommandName="Delete" runat="server"
                                                                    CssClass="griddelete">
                                                                             <i class="fas fa-trash text-danger"></i>    
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn  HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="mnuPreviewSickSheet">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgPrint" runat="server" ImageUrl="~/images/PrintSummary_16.png"
                                                                ToolTip="Print" CommandName="Print" />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="sicksheetno" HeaderText="Sick Sheet No" ReadOnly="True"
                                                        HeaderStyle-HorizontalAlign="Left" FooterText="colhSickSheetNo" />
                                                    <asp:BoundColumn DataField="sicksheetdate" HeaderText="Sick Sheet Date" ReadOnly="True"
                                                        HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" FooterText="colhSicksheetDate" />
                                                    <asp:BoundColumn DataField="institute_name" HeaderText="Provider" ReadOnly="True"
                                                        HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" FooterText="colhProvider" />
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundColumn DataField="job" HeaderText="Job" ReadOnly="True" FooterText="colhJob"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundColumn DataField="cover" HeaderText="Medical Cover" ReadOnly="True" FooterText="colhMedicalCover"
                                                        HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                        HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                     
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            
                                                        </td>
                                                        <td style="width: 10%">
                                                           
                                                        </td>
                                                        <td style="width: 45%">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                              
                                            </td>
                                            <td style="width: 35%">
                                               
                                            </td>
                                            <td style="width: 15%">
                                               
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_gvSickSheet" ScrollBars="Auto" style="margin-top:5px; margin-bottom:10px" Height="300px" runat="server">
                                               
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>--%>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="Global Change Status" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_GlobalChangeStatus.aspx.vb" Inherits="Loan_Savings_New_Loan_Loan_Status_wPg_GlobalChangeStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-ui.js"></script>--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {

            $(".percentage").keyup(function(e) {
                if (($(this).val() < 0 || $(this).val() > 100)) {
                    $(this).val(0);
                    return false;
                }
            });
            $(".percentage").keypress(function(e) {
                if (($(this).val().indexOf('.') != -1) && $(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2) {
                    return false;
                }
                if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Change Status" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" ToolTip="Allocation">
                                                    <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAssignedPeriod" runat="server" Text="Assig. Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAssignedPeriod" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblCalcType" runat="server" Text="Loan Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCalcType" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduct. Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInterestCalcType" runat="server" Text="Intr Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMode" runat="server" Text="Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboMode" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass=" btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblBasicInfo" runat="server" Text="Basic Information" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblExchangeRate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPaymentBy" runat="server" Text="Payment By" CssClass="form-label"></asp:Label>
                                        <asp:RadioButton ID="radValue" runat="server" Text="Value" GroupName="Payment" AutoPostBack="true"
                                            CssClass="with-gap" />
                                        <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" GroupName="Payment"
                                            AutoPostBack="true" CssClass="with-gap" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkCalculateInterest" runat="server" Text="Calculate Interest"
                                            CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                        <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPaymentMode" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 p-l-0">
                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAccountNo" runat="server" Text="Account No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboAccountNo" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPaymentDate" runat="server" Text="Pmt Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpPaymentDate" runat="server" AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBankGroup" runat="server" Text="Bank Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBankGroup" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher#" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtVoucherNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPercentage" runat="server" Text="Percent(%)" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPercentage" runat="server" AutoPostBack="true" Style="text-align: right"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChangeStatus" runat="server" Text="Change Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboChangeStatus" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSearch" runat="server" placeholder="Type to Search" AutoPostBack="true"
                                                    CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 m-t-10">
                                        <asp:Label ID="objlblCount" runat="server" Style="" Text="( 0 / 0 )" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" Width="135%" CssClass="table table-hover table-bordered">
                                                <ItemStyle />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhSelectAll" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                Text=" " CssClass="filled-in" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("IsChecked") %>'
                                                                OnCheckedChanged="chkSelect_CheckedChanged" Text=" " CssClass="filled-in" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="IsChecked" FooterText="objdgcolhSelect" Visible="false" />
                                                    <asp:BoundColumn DataField="VoucherNo" FooterText="dgcolhVocNo" HeaderText="Voucher No." />
                                                    <asp:BoundColumn DataField="Employee" FooterText="dgcolhEmployee" HeaderText="Employee" />
                                                    <asp:BoundColumn DataField="LoanScheme" FooterText="dgcolhLoanScheme" HeaderText="Loan Scheme" />
                                                    <asp:BoundColumn DataField="Mode" FooterText="dgcolhLoanAdvance" HeaderText="Mode" />
                                                    <asp:BoundColumn DataField="LoanCalcType" FooterText="dgcolhCalcType" HeaderText="Calculation Type" />
                                                    <asp:BoundColumn DataField="LoanInterestCalcType" FooterText="dgcolhInterestCalcType"
                                                        HeaderText="Interest Type" />
                                                    <asp:BoundColumn DataField="LoanAdvanceAmount" FooterText="dgcolhLoanAdvanceAmount"
                                                        HeaderText="Ln/Adv Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="balance_amount" FooterText="dgcolhBalanceAmount" HeaderText="Balance Amount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <%--<asp:BoundColumn DataField="settelment_amount" FooterText="dgcolhSettelmentAmount"
                                                        HeaderText="Settlement Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                                                    <asp:TemplateColumn HeaderText="Settlement Amount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Width="160px" ItemStyle-Width="160px" FooterText="dgcolhSettelmentAmount">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtdgcolhSettelmentAmount" onkeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right" runat="server" AutoPostBack="True" Text='<%# Eval("settelment_amount") %>'
                                                                        OnTextChanged="txtdgcolhSettelmentAmount_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle Width="160px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="loan_status" FooterText="dgcolhStatus" HeaderText="Status" />
                                                    <asp:BoundColumn DataField="ChangedStatus" FooterText="dgcolhChangedStatus" HeaderText="Change Status" />
                                                    <asp:BoundColumn DataField="loanadvancetranunkid" FooterText="objdgcolhLoanAdvanceUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeUnkid" Visible="false" />
                                                    <asp:BoundColumn DataField="loanschemeunkid" FooterText="objdgcolhLoanSchemeUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhAssignedPeriodUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="deductionperiodunkid" FooterText="objdgcolhDeductionPeriodUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="calctype_id" FooterText="objdgcolhCalcTypeId" Visible="false" />
                                                    <asp:BoundColumn DataField="interest_calctype_id" FooterText="objdgcolhInterestCalcTypeId"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusUnkid" Visible="false" />
                                                    <asp:BoundColumn DataField="changestatusunkid" FooterText="objdgcolhChangeStatusUnkid"
                                                        Visible="false" />
                                                </Columns>
                                                <HeaderStyle Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass=" btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass=" btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

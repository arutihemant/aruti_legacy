﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LoanStatus_AddEdit.aspx.vb"
    Inherits="Loan_Savings_New_Loan_wPg_LoanStatus_AddEdit" Title="Change Loan Status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>


    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
    --%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                <ucDel:DeleteReason ID="popupDeleteReason" runat="server" />
                <asp:HiddenField ID="hd1" runat="server" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%-- <asp:Label ID="lblDetialHeader" runat="server" Text="Loan Status Information" CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Change Loan Status" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkCalInterest" runat="server" Text="Calculate Interest" CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLoanScheme" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSettlementAmt" runat="server" Style="margin-left: 10px" Text="Settlement Amount"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSettlementAmt" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" Style="text-align: right" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBalance" runat="server" Style="margin-left: 10px" Text="Balance"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtBalance" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Loan Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                        <asp:LinkButton ID="lnkAddSettlement" runat="server" Enabled="false" Text="Add Settlement"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemarks" runat="server" AutoPostBack="true" TextMode="MultiLine"
                                                    Rows="3" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanAdvanceStatushistory" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered" Width="100%">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="username" FooterText="dgcolhUser" HeaderStyle-Width="40%"
                                                        HeaderText="User" ItemStyle-Width="40%">
                                                        <HeaderStyle Width="40%" />
                                                        <ItemStyle Width="40%" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="status_date" FooterText="dgcolhTransactionDate" HeaderText="Transaction Date">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" HeaderText="Status">
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="20%" HeaderText="Calculate Interest" ItemStyle-HorizontalAlign="Center"
                                                        ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkdgcolhCalculateInterest" runat="server" Checked='<%# Eval("iscalculateinterest") %>'
                                                                Enabled="False" Text=" " CssClass="filled-in" />
                                                        </ItemTemplate>
                                                        <HeaderStyle Width="20%" />
                                                        <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="grp" FooterText="objdgcolhIsGroup" HeaderText="Is Group"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loanadvancetranunkid" FooterText="objLoanAdvancestatustranunkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="statusid" FooterText="objdgcolhPeriodStatusId" Visible="false" />
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objcolhPeriodunkid" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="end_date" FooterText="objcolhEndDate" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="start_date" FooterText="objcolhStartDate" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="loanstatustranunkid" FooterText="objcolhLoanStatusTranunkid"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="iscalculateinterest" FooterText="dgcolhCalculateInterest"
                                                        Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" Title="Global Cancel Approved Application"
    CodeFile="wPg_GlobalCancelApprovedApplication.aspx.vb" Inherits="Loan_Savings_New_Loan_Loan_Approval_Process_wPg_GlobalCancelApprovedApplication" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").not(".combo").searchable();
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Cancel Approved Application"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocations">
                                                              <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApplicationNo" runat="server" Text="Application No." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApplicationNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprovedAmount" runat="server" Text="Approved Amt" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0 ">
                                                            <div class="form-group m-t-0">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtApprovedAmount" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                                        runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                            <div class="form-group m-t-0">
                                                                <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                                            <asp:Label ID="lblApplicationDate" runat="server" Text="Application Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0">
                                                            <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanAdvance" runat="server" Text="Loan/advance" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <asp:Label ID="gbInfo" runat="server" Text="Information" CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <asp:Label ID="gbApproved" runat="server" Text="Approved Loan/Advance Application(s)."
                                    CssClass="form-label"></asp:Label>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanApproved" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered"
                                                RowStyle-Wrap="false">
                                                <ItemStyle />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhchkSelect" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                Text=" " />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Application_No" FooterText="dgcolhApplicationNo" HeaderText="Application No." />
                                                    <asp:BoundColumn DataField="applicationdate" FooterText="dgcolhAppDate" HeaderText="Application Date" />
                                                    <asp:BoundColumn DataField="EmpName" FooterText="dgcolhEmployee" HeaderText="Employee" />
                                                    <asp:BoundColumn DataField="LoanScheme" FooterText="dgcolhLoanScheme" HeaderText="Loan Scheme" />
                                                    <asp:BoundColumn DataField="Loan_Advance" FooterText="dgcolhLoan_Advance" HeaderText="Loan/Advance" />
                                                    <asp:BoundColumn DataField="Amount" FooterText="dgcolhAmount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Approved_Amount" FooterText="dgcolhApp_Amount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Approved Amount" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="LoanStatus" FooterText="dgcolhStatus" HeaderText="Loan Status" />
                                                    <asp:BoundColumn DataField="isloan" FooterText="objdgcolhIsLoan" Visible="false" />
                                                    <asp:BoundColumn DataField="loan_statusunkid" FooterText="objdgcolhStatusunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeunkid" Visible="false" />
                                                    <asp:BoundColumn DataField="loanschemeunkid" FooterText="objdgcolhSchemeID" Visible="false" />
                                                    <asp:BoundColumn DataField="processpendingloanunkid" FooterText="objdgcolhprocesspendingloanunkid"
                                                        Visible="false" />
                                                </Columns>
                                                <HeaderStyle Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

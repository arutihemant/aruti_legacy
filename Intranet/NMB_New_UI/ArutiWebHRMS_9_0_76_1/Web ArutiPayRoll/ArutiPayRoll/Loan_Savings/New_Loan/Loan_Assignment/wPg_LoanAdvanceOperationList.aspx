﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LoanAdvanceOperationList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_LoanAdvanceOperationList"
    Title="Loan/Advance Operation List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupLoanParameter" runat="server" PopupControlID="pnl_LoanParameter"
                    TargetControlID="hdf_LoanParameter" CancelControlID="hdf_LoanParameter" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_LoanParameter" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPopupDetailHeader" runat="server" Text="Loan Parameters" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 525px;">
                                <asp:Panel ID="pnlradOpr" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="elOperation" runat="server" Text="Operation Type" CssClass="form-label"></asp:Label>
                                            <asp:RadioButton ID="radInterestRate" runat="server" AutoPostBack="true" Text="Loan Rate(%)"
                                                GroupName="OperationType" CssClass="with-gap" />
                                            <asp:RadioButton ID="radInstallment" runat="server" AutoPostBack="true" Text="Loan Installment Amount/No."
                                                GroupName="OperationType" CssClass="with-gap" />
                                            <asp:RadioButton ID="radTopup" runat="server" AutoPostBack="true" Text="Loan Topup"
                                                GroupName="OperationType" CssClass="with-gap" />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlControls" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Effective Period" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateControl ID="dtpEffectiveDate" runat="server" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30 ">
                                            <asp:Label ID="objlblExRate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  ">
                                            <asp:Label ID="lblLoanInterest" runat="server" Text="Interest (%)" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtLoanInterest" runat="server" Text="0" AutoPostBack="true" Style="text-align: right;"
                                                        onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDuration" runat="server" Text="Installment No. (In Months)" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDuration" runat="server" AutoPostBack="true" Text="1" Style="text-align: right;"
                                                        CssClass="form-control" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPrincipalAmt" runat="server" Text="0" AutoPostBack="true" Style="text-align: right;"
                                                        onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblInterestAmt" runat="server" Style="margin-left: 5px;" Text="Interest Amt."
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtInterestAmt" runat="server" ReadOnly="true" Text="1" Style="text-align: right;"
                                                        onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amount" CssClass="form-label"></asp:Label>
                                            <asp:Label ID="lblTopupAmt" runat="server" Text="Topup Amount" Visible="false" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtInstallmentAmt" runat="server" Style="text-align: right" Text="0"
                                                        AutoPostBack="true" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                    <asp:TextBox ID="txtTopupAmt" runat="server" Style="text-align: right" Text="0" Visible="false"
                                                        onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnl_ChangeStatus" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboStatus" runat="server" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Label ID="objlblEndDate" runat="server" Style="font-size: 12px" Text="" CssClass="form-label"></asp:Label>
                                </div>
                                <asp:HiddenField ID="hdf_LoanParameter" runat="server" />
                                <asp:Button ID="btnSavelnPara" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnCloselnPara" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ucDel:DeleteReason ID="popupDeleteReason" Title="Are you sure you want to Delete?"
                    runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan/Advance Operation List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Loan/Advance Operation List"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployeeName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLoanScheme" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVocNo" runat="server" Text="Voucher No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtVocNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPendingOp" runat="server" Text="Pending Operation(s)" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatusSearch" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatusSearch" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-30">
                                        <asp:CheckBox ID="chkMyApproval" runat="server" AutoPostBack="true" Text="My Approvals"
                                            Checked="true" CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:Panel ID="pnl_PendingOp" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                <asp:DataGrid ID="dgPendingOp" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                    CssClass="table table-hover table-bordered" Width="125%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkChangeStatus" Font-Underline="false" CommandName="ChangeStatus"
                                                                        Text="Change Status" runat="server">
                                                                        <i class="fas fa-tasks"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="PeriodName" HeaderText="Period" Visible="false" FooterText="dgcolhPeriodName">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="effectivedate" HeaderText="Effective Date" FooterText="dgcolhEffectiveDate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Operation" HeaderText="Operation" FooterText="dgcolhOperation">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="interest_rate" HeaderText="Rate(%)" FooterText="dgcolhRate"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="emi_tenure" HeaderText="Installment No." FooterText="dgcolhInstlNo"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="emi_amount" HeaderText="Installment Amount" FooterText="dgcolhInstlAmount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="topup_amount" HeaderText="Topup Amount" FooterText="dgcolhTopupAmount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Approver" HeaderText="Approver" FooterText="dgcolhApprover">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Status" HeaderText="Status" FooterText="dgcolhStatus">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="isgrp" HeaderText="isgrp" Visible="false" FooterText="objdgcolhisgrp">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="lnoperationtypeid" HeaderText="lnoperationtypeid" Visible="false"
                                                            FooterText="objdgcolhloanOpTypeId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MappedUserId" HeaderText="MappedUserId" Visible="false"
                                                            FooterText="objdgcolhmappeduserid"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false"
                                                            FooterText="objdgcolhEmployeeId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="approvertranunkid" HeaderText="approvertranunkid" Visible="false"
                                                            FooterText="objdgcolhlnapproverunkid"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="approverempunkid" HeaderText="approverempunkid" Visible="false"
                                                            FooterText="objdgcolhApproverEmpId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="priority" HeaderText="priority" Visible="false" FooterText="objdgcolhPriority">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="lnotheroptranunkid" HeaderText="lnotheroptranunkid" Visible="false"
                                                            FooterText="objdgcolhlnotheroptranunkid"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="identify_guid" HeaderText="identify_guid" Visible="false"
                                                            FooterText="objdgcolhIdentifyGuid"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="grpRow" HeaderText="grpRow" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="statusunkid" HeaderText="statusunkid" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="loanadvancetranunkid" HeaderText="loanadvancetranunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblApprovedOp" runat="server" Text="Approved Operation(s)" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" ">
                                            <asp:Panel ID="pnl_ApprovedOp" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                <asp:DataGrid ID="lvData" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                    CssClass="table table-hover table-bordered" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                        CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="EffDate" HeaderText="Effective Date" FooterText="colhEffectiveDate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Rate" HeaderText="Rate(%)" FooterText="colhRate" ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NoOfInstallment" HeaderText="Installment No." FooterText="colhINSTLNo"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="InstallmentAmount" HeaderText="Installment Amount" FooterText="colhINSTLAmt"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="TopupAmount" HeaderText="Topup Amount" FooterText="colhTopup"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="OprType" HeaderText="Operation" FooterText="colhChangeType">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="PeriodName" HeaderText="Period Name" Visible="false"
                                                            FooterText="objcolhPeriodName"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="periodunkid" HeaderText="objcolhPeriod" Visible="false"
                                                            FooterText="objcolhPeriod"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IdType" HeaderText="objcolhIdType" Visible="false" FooterText="objcolhIdType">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="statusid" HeaderText="objcolhStatusId" Visible="false"
                                                            FooterText="objcolhStatusId"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="oDate" HeaderText="oDate" Visible="false" FooterText="objcolhoDate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="TabUnkid" HeaderText="TabUnkid" Visible="false" FooterText="objcolhTabUnkid">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="isdefault" HeaderText="isdefault" Visible="false" FooterText="objcolhIsdefault">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="approverunkid" HeaderText="approverunkid" Visible="false"
                                                            FooterText="objcolhApproverunkid"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="approver" HeaderText="Approver" FooterText="colhApprover">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:Label ID="objlblEffDate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                </div>
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Add_Edit_Employee_Saving.aspx.vb"
    Inherits="Loan_Savings_wPg_Add_Edit_Employee_Saving" Title="Add/Edit Employee Savings" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">

        function onlyNumbers(txtBox, evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            //var cval = document.getElementById(txtBoxId).value;
            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc8:DeleteReason ID="popup_Contribution" runat="server" Title="Are you Sure Delete this Record"
                    ValidationGroup="Contribution" />
                <uc8:DeleteReason ID="popup_InterestRate" runat="server" Title="Are you Sure Delete this Record"
                    ValidationGroup="InterestRate" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Employee Savings" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblCaption" runat="server" Text="Employee Saving Information" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtVoucherNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDate" runat="server" Text="Date" Enabled="false" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpDate" runat="server" Enabled="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStopDate" runat="server" Text="Stop Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpStopDate" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmpName" runat="server" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblSavingScheme" runat="server" Text="Saving Scheme" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboSavingScheme" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Contribution Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:Label ID="objlblExRate" runat="server" Text="1" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOpeningContribution" runat="server" Text="Opening Total Contrib."
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtOpeningContribution" runat="server" Text="0" onKeypress="return onlyNumbers(this);"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOpeningBalance" runat="server" Text="Opening Balance." CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtOpeningBalance" runat="server" Text="0" onKeypress="return onlyNumbers(this);"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <asp:Label ID="lblWithOpeningInterest" runat="server" Text="(With Opening Interest)"
                                                            CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblContributionCaption" runat="server" Text="Employee Contribution Information"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblContPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboContPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblContribution" runat="server" Text="Contribution" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtMonthlyContribution" runat="server" Style="text-align: right"
                                                                    Text="0" onKeypress="return onlyNumbers(this);" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnContributionAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnContributionEdit" runat="server" Text="Edit" CssClass="btn btn-default"
                                                    Visible="false" />
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="max-height: 150px;">
                                                        <asp:Panel ID="pnl_lvContributionData" runat="server" ScrollBars="Auto">
                                                            <asp:DataGrid ID="lvContributionData" runat="server" AutoGenerateColumns="false"
                                                                AllowPaging="false" CssClass="table table-hover table-bordered" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="colhContEdit">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="imgContEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                    <i class="fas fa-pencil-alt"></i></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="colhContDelete">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="ImgContDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                <i class="fas fa-trash text-danger"></i> 
                                                                                </asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="period_name" HeaderStyle-Width="40%" ItemStyle-Width="40%"
                                                                        HeaderText="Period" ReadOnly="true" FooterText="colhContPeriod" />
                                                                    <asp:BoundColumn DataField="contribution" HeaderStyle-Width="40%" ItemStyle-Width="40%"
                                                                        HeaderText="Contribution" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                                        ReadOnly="true" FooterText="colhContribution" />
                                                                    <asp:BoundColumn DataField="savingcontributiontranunkid" Visible="false" FooterText="objcolhContMasterId" />
                                                                    <asp:BoundColumn DataField="GUID" Visible="false" FooterText="objcolhGuid" />
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblRateCaption" runat="server" Text="Employee Interest Rate Information"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRatePeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboRatePeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRateDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpRateDate" runat="server" Enabled="false" AutoPostBack="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRate" runat="server" Text="Interest Rate" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRate" runat="server" Text="0" onKeypress="return onlyNumbers(this);"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnRateAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnRateEdit" runat="server" Text="Edit" CssClass="btn btn-default"
                                                    Visible="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 400px;">
                                                            <asp:Panel ID="pnl_lvInterestRateData" runat="server" ScrollBars="Auto">
                                                                <asp:DataGrid ID="lvInterestRateData" runat="server" AutoGenerateColumns="false"
                                                                    AllowPaging="false" Width="99%" CssClass="table table-hover table-bordered">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="colhRateEdit">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="imgRateEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                                        <i class="fas fa-pencil-alt"></i>
                                                                                    </asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Delete"
                                                                            HeaderStyle-HorizontalAlign="Center" FooterText="colhRateDelete">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="ImgRateDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                                        <i class="fas fa-trash text-danger"></i> 
                                                                                    </asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="period_name" HeaderStyle-Width="35%" ItemStyle-Width="35%"
                                                                            HeaderText="Period" ReadOnly="true" FooterText="colhRatePeriod" />
                                                                        <asp:BoundColumn DataField="effectivedate" HeaderStyle-Width="25%" ItemStyle-Width="25%"
                                                                            HeaderText="Effective Date" ReadOnly="true" FooterText="colhRateEffectiveDate" />
                                                                        <asp:BoundColumn DataField="interest_rate" HeaderStyle-Width="20%" ItemStyle-Width="20%"
                                                                            HeaderText="Rate %" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                                            ReadOnly="true" FooterText="colhInterestRate" />
                                                                        <asp:BoundColumn DataField="savinginterestratetranunkid" Visible="false" FooterText="objcolhRateMasterId" />
                                                                        <asp:BoundColumn DataField="GUID" Visible="false" FooterText="objcolhRateGuid" />
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

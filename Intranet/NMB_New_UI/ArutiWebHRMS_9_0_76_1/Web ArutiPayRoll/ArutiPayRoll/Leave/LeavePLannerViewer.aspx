﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="LeavePLannerViewer.aspx.vb" Inherits="LeavePLannerViewer" Title="Leave PLanner Viewer" %>

<%@ Register Assembly="XGridView" Namespace="CustomControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <asp:Panel ID="MainPan" runat="server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="block-header">
                    <h2>
                         <asp:Label ID="lblPageHeader" runat="server" Text="Leave PLanner Viewer"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                             <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                        </h2>
                                    </div>
                                     <div class="body">
                                            <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                          <asp:Calendar ID="Calendar1" runat="server" CssClass="calendar" SelectedDate="2011-06-15"
                                                                ShowGridLines="True">
                                                                <SelectedDayStyle CssClass="Calselday" />
                                                                <SelectorStyle CssClass="calsele" />
                                                                <TitleStyle CssClass="cal_title" />
                                                            </asp:Calendar>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                         <div class="row clearfix">
                                                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lbldepartment" runat="server"  Text="Department:" CssClass="form-label"></asp:Label>
                                                                         <div class="form-group">
                                                                                <asp:DropDownList ID="drpdepartment" runat="server" />
                                                                         </div>
                                                                 </div>
                                                         </div>
                                                          <div class="row clearfix" >
                                                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lbljob"  runat="server" Text="Job:" CssClass="form-label"></asp:Label>
                                                                         <div class="form-group">
                                                                                <asp:DropDownList ID="drpjob" runat="server" />
                                                                         </div>
                                                                 </div>
                                                         </div>
                                                         <div class="row clearfix">         
                                                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblSection" runat="server"  Text="Section:" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                                <asp:DropDownList ID="drpsection" runat="server" />
                                                                        </div>
                                                                 </div>
                                                         </div>
                                                    </div>
                                            </div>
                                            <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                              <asp:RadioButtonList ID="RbtlistView" runat="server" CellSpacing="3"
                                                                RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                                <asp:ListItem Value="0">Weekly View</asp:ListItem>
                                                                <asp:ListItem Value="1">Month View</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                    </div>         
                                            </div>
                                     </div>
                                     <div class="footer">
                                         <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                          <asp:TextBox ID="txtleaveplannerunkid" runat="server" Visible="False"></asp:TextBox>
                                     </div>        
                                </div>
                         </div>           
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 200px">
                                             <cc1:XGridView ID='XGridView' runat="server" EnableCellClick="True" Width="200%"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                            </cc1:XGridView>
                                        
                                   </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>                          
                                        
                                        
                
                   <%-- <div class="panel-primary">
                        <div class="panel-heading">
                          
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                       
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 60%; text-align: center">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%;" align="center">
                                                           
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td align="center" style="width: 100%">
                                                          
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 40%">
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            
                                                        </td>
                                                        <td style="width: 70%">
                                                          
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                           
                                                        </td>
                                                        <td style="width: 70%">
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            
                                                        </td>
                                                        <td style="width: 70%">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       
                                    </div>
                                </div>
                            </div>
                            <div style="border: 2px solid #DDD; margin: 7px;">
                                <asp:Panel ID="Panel1" Width="100%" runat="server" ScrollBars="Horizontal" Wrap="False">
                                   
                                </asp:Panel>
                            </div>
                        </div>
                    </div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

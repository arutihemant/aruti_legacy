﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DeleteReason.ascx.vb"
    Inherits="Controls_DeleteReason" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modal-backdrop bd-l5"
    CancelControlID="btnDelReasonNo" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
</ajaxToolkit:ModalPopupExtender>

<asp:Panel ID="Panel1" runat="server" CssClass="card modal-dialog modal-l5" Style="display: none;">
    <div class="header">
        <h2>
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
        </h2>
    </div>
    <div class="body" style="max-height:400px">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="form-label"></asp:Label>
            <asp:TextBox ID="txtReason" CssClass="form-control" runat="server" TextMode="MultiLine" />
                        
            </div>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                ControlToValidate="txtReason" ErrorMessage="Delete Reason can not be blank. "
                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" SetFocusOnError="True"
                ValidationGroup="Reason"></asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="RequiredFieldValidator1" />
            </div>
        
    </div>
    <div class="footer">
       <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" ValidationGroup="Reason"
            CssClass="btn btn-danger" />
        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" CssClass="btn btn-default" />
        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
    </div>
</asp:Panel>

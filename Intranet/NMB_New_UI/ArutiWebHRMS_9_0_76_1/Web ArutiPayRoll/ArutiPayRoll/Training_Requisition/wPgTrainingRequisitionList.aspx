﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgTrainingRequisitionList.aspx.vb" Inherits="Training_Requisition_wPgTrainingRequisitionList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <style>
        .row:after
        {
            content: "";
            display: table;
            clear: both;
        }
        .ib
        {
            display: inline-block;
            margin-right: 10px;
            padding-left: 5px;
            padding-top: 5px;
        }
    </style>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc4:DelReason ID="Cnf_Delete" runat="server" Title="Enter reason to  void training requisition." />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Requsition List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Requsition List" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingCourse" runat="server" Text="Course Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCourseType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTrainingMode" runat="server" Text="Training Mode" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboTrainingMode" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="form-label"></asp:Label>
                                        <uc3:DateCtrl ID="dtpFromDate" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc3:DateCtrl ID="dtpToDate" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnListNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 250px;">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="gvRequisitionList" runat="server" DataKeyNames="requisitionmasterunkid,iStatusId,linkedmasterid,crmasterunkid,employeeunkid"
                                                    AutoGenerateColumns="False" Width="100%" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                    ShowFooter="false"  CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%#Eval("masterguid")%>'
                                                                    ToolTip="Edit" OnClick="lnkedit_Click">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("masterguid")%>'
                                                                    ToolTip="Delete" OnClick="lnkdelete_Click">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhview">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkview" runat="server" Text="View" CommandName="Preview"
                                                                    Font-Underline="false" CommandArgument='<%#Eval("masterguid")%>' ToolTip="Delete" OnClick="lnkpreview_Click">
                                                                </asp:LinkButton>                                                                
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:BoundField HeaderText="Training Name" DataField="CourseMaster" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhTrainingName" />
                                                        <asp:BoundField HeaderText="Vendor" DataField="institute_name" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhVendor" />
                                                        <asp:BoundField HeaderText="Training Mode" DataField="TrainingMode" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhVendor" />
                                                        <asp:BoundField HeaderText="Start Date" DataField="training_startdate" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhStartDate" />
                                                        <asp:BoundField HeaderText="End Date" DataField="training_enddate" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhEndDate" />
                                                        <asp:BoundField HeaderText="Duration" DataField="Duration" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhDuration" />
                                                        <asp:BoundField HeaderText="Venue" DataField="training_venue" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhVenue" />
                                                        <asp:BoundField HeaderText="Comments" DataField="additonal_comments" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhComments" />
                                                        <asp:BoundField HeaderText="Status" DataField="iStatus" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhStatus" />
                                                        <%--'S.SANDEEP [07-NOV-2018] -- START--%>
                                                        <asp:BoundField HeaderText="Training Type" DataField="trainingtype" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhtrainingtype" />
                                                        <%--'S.SANDEEP [07-NOV-2018] -- END--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

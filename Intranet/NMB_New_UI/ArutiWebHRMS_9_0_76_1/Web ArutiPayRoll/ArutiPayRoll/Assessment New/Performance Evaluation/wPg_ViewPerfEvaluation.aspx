﻿<%@ Page Title="View Performance Evaluation" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_ViewPerfEvaluation.aspx.vb" Inherits="wPg_ViewPerfEvaluation" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfDialog" TagPrefix="cf1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <%--<input type="hidden" id="endreq" value="0" />--%>
    <%--<input type="hidden" id="bodyy" value="0" />--%>
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        //        var scroll = {
        //            Y: '#<%= hfScrollPosition.ClientID %>'
        //        };
        //        var scroll1 = {
        //            Y: '#<%= hfScrollPosition1.ClientID %>'
        //        };
        //        var scroll2 = {
        //            Y: '#<%= hfScrollPosition2.ClientID %>'
        //        };
        //        prm = Sys.WebForms.PageRequestManager.getInstance();
        //        prm.add_beginRequest(beginRequestHandler);
        //        prm.add_endRequest(endRequestHandler);
        //        $(window).scroll(function() {
        //            var cend = $("#endreq").val();
        //            if (cend == "1") {
        //                $("#endreq").val("0");
        //                var nbodyY = $("#bodyy").val();
        //                $(window).scrollTop(nbodyY);
        //            }
        //        });

        //        function beginRequestHandler(sender, args) {
        //            $("#endreq").val("0");
        //            $("#bodyy").val($(window).scrollTop());
        //        }

        //        function endRequestHandler(sender, args) {
        //            $("#endreq").val("1");
        //            SetGeidScrolls();
        //            if (args.get_error() == undefined) {
        //                $("#scrollable-container").scrollTop($(scroll.Y).val());
        //                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
        //                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
        //            }
        //        }
    </script>

    <script type="text/javascript">
        //        function SetGeidScrolls() {
        //            var arrPnl = $('.gridscroll');
        //            for (j = 0; j < arrPnl.length; j++) {
        //                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
        //                if (trtag.length > 52) {
        //                    var trheight = 0;
        //                    for (i = 0; i < 52; i++) {
        //                        trheight = trheight + $(trtag[i]).height();
        //                    }
        //                    $(arrPnl[j]).css("overflow", "auto");
        //                    $(arrPnl[j]).css("height", trheight + "px");
        //                }
        //                else {
        //                    $(arrPnl[j]).css("overflow", "none");
        //                    $(arrPnl[j]).css("height", "100%");
        //                }
        //            }
        //        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="View Performance Evaluation"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="objlblCaption" runat="server" Text="#value"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 450px">
                                            <asp:Panel ID="objpnlBSC" runat="server" Visible="false" CssClass="table table-hover table-bordered">
                                                <asp:DataGrid ID="dgvBSC" runat="server" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Field1" HeaderText="" FooterText="objdgcolhBSCField1" />
                                                        <asp:BoundColumn DataField="Field2" HeaderText="" FooterText="objdgcolhBSCField2" />
                                                        <asp:BoundColumn DataField="Field3" HeaderText="" FooterText="objdgcolhBSCField3" />
                                                        <asp:BoundColumn DataField="Field4" HeaderText="" FooterText="objdgcolhBSCField4" />
                                                        <asp:BoundColumn DataField="Field5" HeaderText="" FooterText="objdgcolhBSCField5" />
                                                        <asp:BoundColumn DataField="Field6" HeaderText="" FooterText="objdgcolhBSCField6" />
                                                        <asp:BoundColumn DataField="Field7" HeaderText="" FooterText="objdgcolhBSCField7" />
                                                        <asp:BoundColumn DataField="Field8" HeaderText="" FooterText="objdgcolhBSCField8" />
                                                        <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="dgcolhSDate" />
                                                        <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="dgcolhEDate" />
                                                        <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhCompleted" />
                                                        <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhBSCWeight"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="eself" HeaderText="Self - Score" FooterText="dgcolheselfBSC"
                                                            ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkBSC" />
                                                        <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" FooterText="dgcolhaselfBSC" />
                                                        <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkBSC" />
                                                        <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreBSC" />
                                                        <asp:BoundColumn DataField="rself" HeaderText="Reviewer - Score" FooterText="dgcolhrselfBSC" />
                                                        <asp:BoundColumn DataField="rremark" HeaderText="Reviewer - Remark" FooterText="dgcolhrremarkBSC" />
                                                        <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpBSC" Visible="false" />
                                                        <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdBSC" Visible="false" />
                                                        <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhScaleMasterId"
                                                            Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                            <asp:Panel ID="objpnlGE" runat="server" Visible="false" CssClass="table table-hover table-bordered">
                                                <asp:DataGrid ID="dgvGE" runat="server" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                            ItemStyle-Width="35%" HeaderStyle-Width="35%" />
                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                            Visible="false" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <asp:BoundColumn DataField="eself" HeaderText="Self - Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolheselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <asp:BoundColumn DataField="escore" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                            ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                        <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolhaselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <asp:BoundColumn DataField="ascore" HeaderText="Final Score" FooterText="objdgcolhadisplayGE"
                                                            ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                        <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreGE"
                                                            ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <asp:BoundColumn DataField="rself" HeaderText="Reviewer - Score" ItemStyle-HorizontalAlign="Right"
                                                            FooterText="dgcolhrselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                        <asp:BoundColumn DataField="rscore" HeaderText="Final Score" FooterText="objdgcolhrdisplayGE"
                                                            ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                        <asp:BoundColumn DataField="rremark" HeaderText="Reviewer - Remark" FooterText="dgcolhrremarkGE"
                                                            ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                        <asp:BoundColumn DataField="scalemasterunkid" HeaderText="" FooterText="objdgcolhscalemasterunkidGE"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="competenciesunkid" HeaderText="" FooterText="objdgcolhcompetenciesunkidGE"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="objdgcolhassessgroupunkidGE"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                        <asp:BoundColumn DataField="IsPGrp" HeaderText="" FooterText="objdgcolhIsPGrpGE"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                            HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                    Font-Underline="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                            <asp:Panel ID="objpnlCItems" runat="server" Visible="false" CssClass="table table-hover table-bordered">
                                                <asp:GridView ID="dgvItems" runat="server" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="objlblValue1" runat="server" CssClass="form-label" Text="" Visible="false"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="objlblValue2" runat="server" CssClass="form-label" Text=""></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="objlblValue3" runat="server" CssClass="form-label" Text=""></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                        <asp:Label ID="objlblValue4" runat="server" CssClass="form-label" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnBack" runat="server" Text="Previous" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnAppRejAssessment" runat="server" Text="Agree/Disagree Assessmnet"
                                    CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                    CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label2" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblCompInfoHeader" runat="server" Text="Description" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" class="form-control"
                                            ReadOnly="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_AppRejAssessment" runat="server" BackgroundCssClass="ModalPopupBG2"
                    CancelControlID="btnAppRejAss_Close" PopupControlID="pnl_AppRejAssessment" TargetControlID="hdfAppRejAss">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_AppRejAssessment" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblApprejAssessmentHeader" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 200px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAppRejAssessment_AssessorPart" runat="server" Text="Assessor Part"
                                    CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <asp:Label ID="lblAppRejAssessement_Assessor_Status" runat="server" Text="Status"
                                    CssClass="form-label" />
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <asp:RadioButton ID="radAssessment_Assessor_Approve" runat="server" Text="Agree"
                                    GroupName="Assessment_Assessor" />
                                <asp:RadioButton ID="radAssessment_Assessor_Reject" runat="server" Text="Disagree"
                                    GroupName="Assessment_Assessor" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblAppRejAssessement_Assessor_Remark" runat="server" Text="Remark"
                                    CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtAppRejAssessment_Assessor_Remark" runat="server" TextMode="MultiLine"
                                            Style="resize: none" class="form-control" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Panel ID="pnlAppRejAssessment_Reviewer" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppRejAssessment_ReviewerPart" runat="server" Text="Reviewer Part" CssClass="form-label"/>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <asp:Label ID="lblAppRejAssessement_Reviewer_Status" runat="server" Text="Status"
                                                CssClass="form-label" />
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <asp:RadioButton ID="radAssessment_Reviewer_Approve" runat="server" Text="Agree"
                                                GroupName="Assessment_Reviewer" />
                                            <asp:RadioButton ID="radAssessment_Reviewer_Reject" runat="server" Text="Disagree"
                                                GroupName="Assessment_Reviewer" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppRejAssessement_Reviewer_Remark" runat="server" Text="Remark"
                                                CssClass="form-label" />
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppRejAssessment_Reviewer_Remark" runat="server" TextMode="MultiLine"
                                                        Style="resize: none" class="form-control" Rows="3"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnpostComment" runat="server" Text="Post Comment" CssClass="btn btn-primary" />
                        <asp:Button ID="btnAppRejAss_Close" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdfAppRejAss" runat="server" />
                    </div>
                </asp:Panel>
                <cf1:CnfDialog ID="cnfEdit" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

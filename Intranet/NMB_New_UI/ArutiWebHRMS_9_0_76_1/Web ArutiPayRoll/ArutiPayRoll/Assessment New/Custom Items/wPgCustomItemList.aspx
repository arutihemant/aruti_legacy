﻿<%@ Page Title="Custom Item List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgCustomItemList.aspx.vb" Inherits="wPgCustomItemList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        var hdf= $('#<%= hdf_btnCItemFld.ClientID %>');
        if (hdf.val()=='1')
        {
            $find("AddCustomItemList").show();
        }else{
            $find("AddCustomItemList").hide();
        }
    }	
    </script>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "auto"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };       
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <%--<script type="text/javascript">

        $('#imgclose').click(function() {
            alert('hiii');
            $find('<%= popup_AddCustomItems.ClientID %>').hide();
        });
maro code nati mukiyo haji?
mukyo ne
    </script>--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Custom Item List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblItemType" Style="margin-left: 10px" runat="server" Text="Item Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboItemType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblViewMode" runat="server" Text="View Mode"></asp:Label>
                                            </td>
                                            <td style="width: 85%" colspan="3">
                                                <asp:CheckBox ID="chkSelfAssessment" Text="All" runat="server" />
                                                <asp:CheckBox ID="chkAssessorAssessment" Text="Assessor" runat="server" />
                                                <asp:CheckBox ID="chkReviewerAssessment" Text="Reviewer" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="Add New" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top: 10px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto"
                                                onscroll="$(scroll.Y).val(this.scrollTop);">
                                                <asp:Panel ID="pnl_dgvData" runat="server" ScrollBars="Auto" Style="max-width: 1140px;
                                                    margin: auto">
                                                    <asp:DataGrid ID="dgvData" runat="server" Style="margin: auto" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="150%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="objEdit" ToolTip="Edit"
                                                                            CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgDelete" runat="server" CommandName="objDelete" ToolTip="Delete"
                                                                            CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="pname" HeaderText="Period" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="custom_item" HeaderText="Custom Item" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="iType" HeaderText="ItemType" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="iSType" HeaderText="Selection Mode" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="iViewType" HeaderText="Visible In" ReadOnly="true" />
                                                            <asp:BoundColumn DataField="periodunkid" HeaderText="objcolhPeriodId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="statusid" HeaderText="statusid" ReadOnly="true" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Label ID="objlblNotes" runat="server" Font-Bold="true" ForeColor="Maroon"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_AddCustomItems" runat="server" TargetControlID="hdf_btnCItemFld"
                        CancelControlID="hdf_btnCItemFld" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditCItem"
                        Drag="True" BehaviorID="AddCustomItemList">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_objfrmAddEditCItem" runat="server" CssClass="newpopup" Style="width: 50%;
                        display: none">
                        <div class="panel-primary" style="margin: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblHeadeText" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblPopPeriod" runat="server" Text="Period"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cbopopPeriod" Width="247px" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 40%">
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblCustomHeader" runat="server" Text="Custom Header"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="txtCustomHeader" runat="server" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td style="width: 40%">
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblpopItemType" runat="server" Text="Item Type"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cbopopItemType" Width="247px" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cboMode" Width="247px" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblItemApplicableto" runat="server" Text="Item Applicable To"></asp:Label>
                                                </td>
                                                <td style="width: 80%" colspan="2">
                                                    <asp:RadioButtonList ID="radItemSelection" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="All" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Assessor" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Reviewer" Value="3"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblCustomItem" runat="server" Text="Custom Item"></asp:Label>
                                                </td>
                                                <td style="width: 40%" colspan="2">
                                                    <asp:TextBox ID="txtCustomItem" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 20%;" align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                                <td style="text-align: right; width: 80%;" colspan="2">
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%" colspan="3">
                                                    <asp:Label ID="objlblpopupNotes" runat="server" Font-Bold="true" ForeColor="Maroon"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:HiddenField ID="hdf_btnCItemFld" runat="server" />
                                            <asp:Button ID="btnCItemSave" runat="server" Text="Save" CssClass="btndefault" />
                                            <asp:Button ID="btnCItemCancel" runat="server" Text="Cancel" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc2:Confirm ID="popConfirm" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="OrganizationChart.aspx.vb"
    Inherits="OrgChart_OrganizationChart" Title="View Organization Chart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script>
       function openwin() {
          debugger;

            var popUp = window.open('../OrganizationChart/Chart.aspx', '_blank');
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please disable your pop-up blocker and click on link again.');
            }
            else {
                popUp.focus();
            }
            }
    </script>

    <asp:UpdatePanel ID="uppnl_mianMSS" runat="server">
        <ContentTemplate>
            <div class="block-header">
                <h2>
                    <asp:Label ID="lblPageHeader" runat="server" Text="Organization Chart"></asp:Label>
                </h2>
                </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                            <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                            </div>
                        <div class="body">
                            <div class="row clearfix d--f ai--c">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblcharttype" runat="server" Text="View Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                    <asp:DropDownList ID="cbocharttype" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:CheckBox ID="chkViewFilters" runat="server" Text="View Filters On Chart?" Visible="false" />
                                    <asp:CheckBox ID="chkViewJobdetail" runat="server" Text="View Planned Position, Head Count, Position Variance?"
                                        Visible="false" />
                                </div>
                            </div>
                            <asp:Panel ID="pnljobFilter" runat="server" CssClass="row clearfix" Visible="false">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblbranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbobranch" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblunitgroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbounitgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lbldeptgroup" runat="server" Text="Dept. Group" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbodeptgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblunit" runat="server" Text="Units" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbounit" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lbldepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbodepartment" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblteam" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboteam" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblsectiongroup" runat="server" Text="Sec. Group" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbosectiongroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblclassgroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboclassgroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblsection" runat="server" Text="Sections" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cbosection" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblclass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboclass" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblJobGroup" runat="server" Text="Job Group" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboJobGroup" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblGrade" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboGrade" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboGradeLevel" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            
                            
                            <asp:Panel ID="pnlOrganizationChartFilter" runat="server" CssClass="row clearfix" Visible="false">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOrganizationChartDeptGroup" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                    <asp:DropDownList ID="drpOrganizationChartDeptGroup" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            
                            </div>
                        <div class="footer">
                            <asp:CheckBox ID="chkExpandAll" runat="server" Text="Expand All Level?" CssClass="pull-left" />
                            <asp:Button ID="btnViewchart" runat="server" CssClass="btn btn-primary" Text="View" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

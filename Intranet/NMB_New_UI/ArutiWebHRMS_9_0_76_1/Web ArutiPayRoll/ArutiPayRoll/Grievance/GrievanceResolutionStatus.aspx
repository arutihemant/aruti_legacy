﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="GrievanceResolutionStatus.aspx.vb"
    Inherits="Grievance_GrievanceResolutionStatus" Title="Grievance Resolution Status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Resolution Status"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee Name"></asp:Label>
                                        <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGrrefno" runat="server" Text="Ref No."></asp:Label>
                                        <asp:DropDownList ID="drpRefno" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApproverStatus" runat="server" Text="Approver Status"></asp:Label>
                                        <asp:DropDownList ID="drpApproverStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeeStatus" runat="server" Text="Employee Status"></asp:Label>
                                        <asp:DropDownList ID="drpEmployeeStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="levelname" HeaderText="Approver Level" HeaderStyle-Width="10%"
                                                        FooterText="colhlevelname"></asp:BoundField>
                                                    <asp:BoundField DataField="name" HeaderText="Approver Name" HeaderStyle-Width="10%"
                                                        FooterText="colhApproverName"></asp:BoundField>
                                                    <asp:BoundField DataField="ApprovalResponse" HeaderText="Approver Resolution" FooterText="colhApproverStatus">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="qualifyremark" HeaderText="Qualifing Remark" FooterText="colhqualifyremark">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="responseremark" HeaderText="Response Remark" FooterText="colhresponseremark">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" FooterText="colhemployeestatus">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response" FooterText="colhempresponseremark">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

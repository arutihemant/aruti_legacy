﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public NotInheritable Class frmSplash

    Private ReadOnly mstrModuleName As String = "frmSplash"
    Dim IsLogin As Boolean = True
    Dim p As Process()

    'TODO: This form can easily be set as the splash screen for the application by going to the "Application" tab
    '  of the Project Designer ("Properties" under the "Project" menu).


    Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up the dialog text at runtime according to the application's assembly information.  

        'TODO: Customize the application's assembly information in the "Application" pane of the project 
        '  properties dialog (under the "Project" menu).

        'Application title
        Me.Visible = False
        If My.Application.Info.Title <> "" Then
            'If the application title is missing, use the application name, without the extension
            ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If

        'Format the version information using the text set into the Version control at design time as the
        '  formatting string.  This allows for effective localization if desired.
        '  Build and revision information could be included by using the following code and changing the 
        '  Version control's designtime text to "Version {0}.{1:00}.{2}.{3}" or something similar.  See
        '  String.Format() in Help for more information.
        '
        '    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        tmrSplash.Enabled = False 'Sohail (30 Dec 2013)

        'Copyright info
        Copyright.Text = My.Application.Info.Copyright



        '  p = Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName)

        If UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0 Then
            If Not Form.ActiveForm Is Nothing Then
                My.Application.OpenForms.Item(Form.ActiveForm().Name).BringToFront()
                My.Application.OpenForms.Item(Form.ActiveForm().Name).Activate()
            End If
            IsLogin = False
            Exit Sub
        End If

        Dim objmain As New clsMain
        Try

            If objmain.DO_Main(enArutiApplicatinType.Aruti_TimeSheet) Then
                'Anjan [ 05 Feb 2013 ] -- Start
                'ENHANCEMENT : TRA CHANGES
                If ConfigParameter._Object._IsArutiDemo = False Then
                    If Not ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) Then
                        eZeeMsgBox.Show("You are not Authorized to use this module. Please contact Aruti Support team to get registration for this module.", enMsgBoxStyle.Information)
                        Me.Close()
                        Exit Sub
                    End If
                End If
            Else
                Me.Close()
                Exit Sub
            End If

            tmrSplash.Enabled = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tmrSplash_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSplash.Tick
        Dim objfrmHRLogin As frmHRLogin = Nothing
        Try

            If IsLogin = False Then Exit Sub

            tmrSplash.Enabled = False
            Dim mblnAppRun As Boolean = True
            Dim objMasterData As New clsMasterData
            If objMasterData.IsCompanyCreated = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please configure Company first from Aruti Configuration."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Do While mblnAppRun

                Dim cfrm As New frmCommonSelection
                cfrm._IsFromTimesheet = True
                Dim blnCompanyResult As Boolean = cfrm.displayDialog

                cfrm = Nothing
                If blnCompanyResult Then
                    Dim dfrm As New frmCommonSelection
                    dfrm._IsFinancial_Year = True
                    dfrm._IsFromTimesheet = True
                    Dim blnDatabaseResult As Boolean = dfrm.displayDialog
                    dfrm = Nothing
                    If blnDatabaseResult Then


                        eZeeCommonLib.eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Call objMasterData.InsertDefaultData(True)
                        Call objMasterData.InsertDefaultData(True, Company._Object._Companyunkid)
                        'S.SANDEEP [04 JUN 2015] -- END

                        Me.Hide()
                        Dim blnResult As Boolean = False

                        If Environment.GetCommandLineArgs.Count > 1 Then
                            If Environment.GetCommandLineArgs(1).ToString = "1" Then objfrmHRLogin.mblnIsSelfAssessment = True
                        End If
                        objfrmHRLogin = New frmHRLogin
                        objfrmHRLogin.StartPosition = FormStartPosition.CenterScreen
                        blnResult = objfrmHRLogin.displayDialog(-1, enAction.ADD_ONE)
                        If blnResult = False Then

                            'Pinkal (10-Mar-2011) -- Start

                            mblnAppRun = False
                            '    gfrmMDI.ShowDialog()
                            '    gfrmMDI = Nothing
                            'Else
                            'Application.Exit()
                            'Pinkal (10-Mar-2011) -- Start

                        End If
                    Else
                        Application.Exit()
                        mblnAppRun = False
                    End If
                Else
                    Application.Exit()
                    mblnAppRun = False
                End If
            Loop

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tmrSplash_Tick", mstrModuleName)
        Finally
            'Pinkal (10-Mar-2011) -- Start

            If objfrmHRLogin IsNot Nothing Then
                If objfrmHRLogin._IsExit = True Then
                    Me.Close()
                End If
                objfrmHRLogin = Nothing
            End If

            'Pinkal (10-Mar-2011) -- End

        End Try
    End Sub


End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserCreationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserCreationList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvUserList = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUserRole = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIsManager = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUserUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRoleUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCompanyUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhActiveStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.radShowInActiveUser = New System.Windows.Forms.RadioButton
        Me.radShowAllUser = New System.Windows.Forms.RadioButton
        Me.radShowActiveUser = New System.Windows.Forms.RadioButton
        Me.lblRole = New System.Windows.Forms.Label
        Me.cboRole = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuUserSpecificOperation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditReportPrivilege = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEditUserAccess = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportOperationalPrivilege = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalUserOperation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportADAttributeMapping = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportADusers = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEnableDisableADusers = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportEmp_User = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReportPrivilege = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAssignUserAccess = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportUserList = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMapUserRole = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvUserList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvUserList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilter)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(798, 480)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvUserList
        '
        Me.dgvUserList.AllowUserToAddRows = False
        Me.dgvUserList.AllowUserToDeleteRows = False
        Me.dgvUserList.AllowUserToResizeRows = False
        Me.dgvUserList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvUserList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvUserList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvUserList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvUserList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.dgcolhUserName, Me.dgcolhFirstName, Me.dgcolhLastName, Me.dgcolhEmail, Me.dgcolhUserRole, Me.dgcolhIsManager, Me.objdgcolhUserUnkid, Me.objdgcolhRoleUnkid, Me.objdgcolhEmployeeUnkid, Me.objdgcolhCompanyUnkid, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhActiveStatus})
        Me.dgvUserList.Location = New System.Drawing.Point(12, 135)
        Me.dgvUserList.MultiSelect = False
        Me.dgvUserList.Name = "dgvUserList"
        Me.dgvUserList.ReadOnly = True
        Me.dgvUserList.RowHeadersVisible = False
        Me.dgvUserList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvUserList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUserList.Size = New System.Drawing.Size(774, 284)
        Me.dgvUserList.TabIndex = 4
        '
        'objdgcolhCollapse
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhCollapse.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhCollapse.Frozen = True
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollapse.Width = 25
        '
        'dgcolhUserName
        '
        Me.dgcolhUserName.Frozen = True
        Me.dgcolhUserName.HeaderText = "User Name"
        Me.dgcolhUserName.Name = "dgcolhUserName"
        Me.dgcolhUserName.ReadOnly = True
        Me.dgcolhUserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhUserName.Width = 300
        '
        'dgcolhFirstName
        '
        Me.dgcolhFirstName.HeaderText = "Firstname"
        Me.dgcolhFirstName.Name = "dgcolhFirstName"
        Me.dgcolhFirstName.ReadOnly = True
        Me.dgcolhFirstName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhFirstName.Width = 130
        '
        'dgcolhLastName
        '
        Me.dgcolhLastName.HeaderText = "Lastname"
        Me.dgcolhLastName.Name = "dgcolhLastName"
        Me.dgcolhLastName.ReadOnly = True
        Me.dgcolhLastName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhLastName.Width = 130
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhEmail.Width = 154
        '
        'dgcolhUserRole
        '
        Me.dgcolhUserRole.HeaderText = "User Role"
        Me.dgcolhUserRole.Name = "dgcolhUserRole"
        Me.dgcolhUserRole.ReadOnly = True
        Me.dgcolhUserRole.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhUserRole.Width = 140
        '
        'dgcolhIsManager
        '
        Me.dgcolhIsManager.HeaderText = "Manager/Desktop"
        Me.dgcolhIsManager.Name = "dgcolhIsManager"
        Me.dgcolhIsManager.ReadOnly = True
        '
        'objdgcolhUserUnkid
        '
        Me.objdgcolhUserUnkid.HeaderText = "objdgcolhUserUnkid"
        Me.objdgcolhUserUnkid.Name = "objdgcolhUserUnkid"
        Me.objdgcolhUserUnkid.ReadOnly = True
        Me.objdgcolhUserUnkid.Visible = False
        '
        'objdgcolhRoleUnkid
        '
        Me.objdgcolhRoleUnkid.HeaderText = "objdgcolhRoleUnkid"
        Me.objdgcolhRoleUnkid.Name = "objdgcolhRoleUnkid"
        Me.objdgcolhRoleUnkid.ReadOnly = True
        Me.objdgcolhRoleUnkid.Visible = False
        '
        'objdgcolhEmployeeUnkid
        '
        Me.objdgcolhEmployeeUnkid.HeaderText = "objdgcolhEmployeeUnkid"
        Me.objdgcolhEmployeeUnkid.Name = "objdgcolhEmployeeUnkid"
        Me.objdgcolhEmployeeUnkid.ReadOnly = True
        Me.objdgcolhEmployeeUnkid.Visible = False
        '
        'objdgcolhCompanyUnkid
        '
        Me.objdgcolhCompanyUnkid.HeaderText = "objdgcolhCompanyUnkid"
        Me.objdgcolhCompanyUnkid.Name = "objdgcolhCompanyUnkid"
        Me.objdgcolhCompanyUnkid.ReadOnly = True
        Me.objdgcolhCompanyUnkid.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhActiveStatus
        '
        Me.objdgcolhActiveStatus.HeaderText = "objdgcolhActiveStatus"
        Me.objdgcolhActiveStatus.Name = "objdgcolhActiveStatus"
        Me.objdgcolhActiveStatus.ReadOnly = True
        Me.objdgcolhActiveStatus.Visible = False
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.objLine1)
        Me.gbFilter.Controls.Add(Me.radShowInActiveUser)
        Me.gbFilter.Controls.Add(Me.radShowAllUser)
        Me.gbFilter.Controls.Add(Me.radShowActiveUser)
        Me.gbFilter.Controls.Add(Me.lblRole)
        Me.gbFilter.Controls.Add(Me.cboRole)
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.cboUser)
        Me.gbFilter.Controls.Add(Me.lblUser)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(12, 64)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 90
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(774, 65)
        Me.gbFilter.TabIndex = 1
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(486, 25)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(9, 39)
        Me.objLine1.TabIndex = 94
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'radShowInActiveUser
        '
        Me.radShowInActiveUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowInActiveUser.Location = New System.Drawing.Point(595, 35)
        Me.radShowInActiveUser.Name = "radShowInActiveUser"
        Me.radShowInActiveUser.Size = New System.Drawing.Size(91, 17)
        Me.radShowInActiveUser.TabIndex = 71
        Me.radShowInActiveUser.Text = "Inactive User"
        Me.radShowInActiveUser.UseVisualStyleBackColor = True
        '
        'radShowAllUser
        '
        Me.radShowAllUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAllUser.Location = New System.Drawing.Point(692, 35)
        Me.radShowAllUser.Name = "radShowAllUser"
        Me.radShowAllUser.Size = New System.Drawing.Size(67, 17)
        Me.radShowAllUser.TabIndex = 71
        Me.radShowAllUser.Text = "All User"
        Me.radShowAllUser.UseVisualStyleBackColor = True
        '
        'radShowActiveUser
        '
        Me.radShowActiveUser.Checked = True
        Me.radShowActiveUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowActiveUser.Location = New System.Drawing.Point(507, 35)
        Me.radShowActiveUser.Name = "radShowActiveUser"
        Me.radShowActiveUser.Size = New System.Drawing.Size(82, 17)
        Me.radShowActiveUser.TabIndex = 71
        Me.radShowActiveUser.TabStop = True
        Me.radShowActiveUser.Text = "Active User"
        Me.radShowActiveUser.UseVisualStyleBackColor = True
        '
        'lblRole
        '
        Me.lblRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRole.Location = New System.Drawing.Point(286, 36)
        Me.lblRole.Name = "lblRole"
        Me.lblRole.Size = New System.Drawing.Size(47, 15)
        Me.lblRole.TabIndex = 3
        Me.lblRole.Text = "Role"
        '
        'cboRole
        '
        Me.cboRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRole.DropDownWidth = 250
        Me.cboRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRole.FormattingEnabled = True
        Me.cboRole.Location = New System.Drawing.Point(339, 33)
        Me.cboRole.Name = "cboRole"
        Me.cboRole.Size = New System.Drawing.Size(136, 21)
        Me.cboRole.TabIndex = 4
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(747, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(259, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(724, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 250
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(81, 33)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(172, 21)
        Me.cboUser.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(8, 36)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(67, 15)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "Username"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 425)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(798, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.mnuOperation
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(110, 30)
        Me.btnOperation.SplitButtonMenu = Me.mnuOperation
        Me.btnOperation.TabIndex = 4
        Me.btnOperation.Text = "&Operation"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUserSpecificOperation, Me.mnuGlobalUserOperation, Me.mnuMapUserRole})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(198, 92)
        Me.mnuOperation.Text = "Export User List"
        '
        'mnuUserSpecificOperation
        '
        Me.mnuUserSpecificOperation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditReportPrivilege, Me.mnuEditUserAccess, Me.mnuExportOperationalPrivilege})
        Me.mnuUserSpecificOperation.Name = "mnuUserSpecificOperation"
        Me.mnuUserSpecificOperation.Size = New System.Drawing.Size(197, 22)
        Me.mnuUserSpecificOperation.Tag = "mnuUserSpecificOperation"
        Me.mnuUserSpecificOperation.Text = "User Specific Operation"
        '
        'mnuEditReportPrivilege
        '
        Me.mnuEditReportPrivilege.Name = "mnuEditReportPrivilege"
        Me.mnuEditReportPrivilege.Size = New System.Drawing.Size(220, 22)
        Me.mnuEditReportPrivilege.Tag = "mnuEditReportPrivilege"
        Me.mnuEditReportPrivilege.Text = "Edit Report Privilege"
        '
        'mnuEditUserAccess
        '
        Me.mnuEditUserAccess.Name = "mnuEditUserAccess"
        Me.mnuEditUserAccess.Size = New System.Drawing.Size(220, 22)
        Me.mnuEditUserAccess.Tag = "mnuEditUserAccess"
        Me.mnuEditUserAccess.Text = "Edit User Access"
        '
        'mnuExportOperationalPrivilege
        '
        Me.mnuExportOperationalPrivilege.Name = "mnuExportOperationalPrivilege"
        Me.mnuExportOperationalPrivilege.Size = New System.Drawing.Size(220, 22)
        Me.mnuExportOperationalPrivilege.Tag = "mnuExportOperationalPrivilege"
        Me.mnuExportOperationalPrivilege.Text = "Export Operational Privilege"
        '
        'mnuGlobalUserOperation
        '
        Me.mnuGlobalUserOperation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportADAttributeMapping, Me.mnuImportADusers, Me.mnuEnableDisableADusers, Me.mnuImportEmp_User, Me.mnuReportPrivilege, Me.mnuAssignUserAccess, Me.mnuExportUserList})
        Me.mnuGlobalUserOperation.Name = "mnuGlobalUserOperation"
        Me.mnuGlobalUserOperation.Size = New System.Drawing.Size(197, 22)
        Me.mnuGlobalUserOperation.Tag = "mnuGlobalUserOperation"
        Me.mnuGlobalUserOperation.Text = "Global User Operation"
        '
        'mnuImportADAttributeMapping
        '
        Me.mnuImportADAttributeMapping.Enabled = False
        Me.mnuImportADAttributeMapping.Name = "mnuImportADAttributeMapping"
        Me.mnuImportADAttributeMapping.Size = New System.Drawing.Size(259, 22)
        Me.mnuImportADAttributeMapping.Text = "Active Directory Attribute Mapping"
        '
        'mnuImportADusers
        '
        Me.mnuImportADusers.Name = "mnuImportADusers"
        Me.mnuImportADusers.Size = New System.Drawing.Size(259, 22)
        Me.mnuImportADusers.Tag = "mnuImportADusers"
        Me.mnuImportADusers.Text = "Import AD Users"
        '
        'mnuEnableDisableADusers
        '
        Me.mnuEnableDisableADusers.Name = "mnuEnableDisableADusers"
        Me.mnuEnableDisableADusers.Size = New System.Drawing.Size(259, 22)
        Me.mnuEnableDisableADusers.Text = "Enable/Disable AD Users"
        '
        'mnuImportEmp_User
        '
        Me.mnuImportEmp_User.Name = "mnuImportEmp_User"
        Me.mnuImportEmp_User.Size = New System.Drawing.Size(259, 22)
        Me.mnuImportEmp_User.Tag = "mnuImportEmp_User"
        Me.mnuImportEmp_User.Text = "Import Employee As Users"
        '
        'mnuReportPrivilege
        '
        Me.mnuReportPrivilege.Name = "mnuReportPrivilege"
        Me.mnuReportPrivilege.Size = New System.Drawing.Size(259, 22)
        Me.mnuReportPrivilege.Tag = "mnuReportPrivilege"
        Me.mnuReportPrivilege.Text = "Assign Report Privilege"
        '
        'mnuAssignUserAccess
        '
        Me.mnuAssignUserAccess.Name = "mnuAssignUserAccess"
        Me.mnuAssignUserAccess.Size = New System.Drawing.Size(259, 22)
        Me.mnuAssignUserAccess.Tag = "mnuAssignUserAccess"
        Me.mnuAssignUserAccess.Text = "Assign User Access"
        '
        'mnuExportUserList
        '
        Me.mnuExportUserList.Name = "mnuExportUserList"
        Me.mnuExportUserList.Size = New System.Drawing.Size(259, 22)
        Me.mnuExportUserList.Tag = "mnuExportUserList"
        Me.mnuExportUserList.Text = "Export User List"
        '
        'mnuMapUserRole
        '
        Me.mnuMapUserRole.Name = "mnuMapUserRole"
        Me.mnuMapUserRole.Size = New System.Drawing.Size(197, 22)
        Me.mnuMapUserRole.Text = "Map User With Role"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(586, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(483, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(380, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(689, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(798, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Users List"
        '
        'frmUserCreationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(798, 480)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserCreationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Users"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvUserList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents lblRole As System.Windows.Forms.Label
    Friend WithEvents cboRole As System.Windows.Forms.ComboBox
    Friend WithEvents dgvUserList As System.Windows.Forms.DataGridView
    Friend WithEvents radShowInActiveUser As System.Windows.Forms.RadioButton
    Friend WithEvents radShowAllUser As System.Windows.Forms.RadioButton
    Friend WithEvents radShowActiveUser As System.Windows.Forms.RadioButton
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUserRole As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIsManager As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUserUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRoleUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCompanyUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhActiveStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents mnuUserSpecificOperation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditReportPrivilege As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditUserAccess As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportOperationalPrivilege As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalUserOperation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportADusers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportEmp_User As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReportPrivilege As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignUserAccess As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportUserList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEnableDisableADusers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportADAttributeMapping As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMapUserRole As System.Windows.Forms.ToolStripMenuItem
End Class

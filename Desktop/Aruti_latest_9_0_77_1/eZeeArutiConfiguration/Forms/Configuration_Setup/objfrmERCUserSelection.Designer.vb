﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmERCUserSelection
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbUserSelection = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetUser = New System.Windows.Forms.LinkLabel
        Me.objSpc1 = New System.Windows.Forms.SplitContainer
        Me.txtSearch1 = New eZee.TextBox.AlphanumericTextBox
        Me.objchkDCheck = New System.Windows.Forms.CheckBox
        Me.dgvDatesUser = New System.Windows.Forms.DataGridView
        Me.objdgcolhDCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhDUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDUserunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objlnkClose = New System.Windows.Forms.LinkLabel
        Me.gbUserSelection.SuspendLayout()
        Me.objSpc1.Panel1.SuspendLayout()
        Me.objSpc1.Panel2.SuspendLayout()
        Me.objSpc1.SuspendLayout()
        CType(Me.dgvDatesUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbUserSelection
        '
        Me.gbUserSelection.BorderColor = System.Drawing.Color.Black
        Me.gbUserSelection.Checked = False
        Me.gbUserSelection.CollapseAllExceptThis = False
        Me.gbUserSelection.CollapsedHoverImage = Nothing
        Me.gbUserSelection.CollapsedNormalImage = Nothing
        Me.gbUserSelection.CollapsedPressedImage = Nothing
        Me.gbUserSelection.CollapseOnLoad = False
        Me.gbUserSelection.Controls.Add(Me.lnkSetUser)
        Me.gbUserSelection.Controls.Add(Me.objSpc1)
        Me.gbUserSelection.Controls.Add(Me.objlnkClose)
        Me.gbUserSelection.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbUserSelection.ExpandedHoverImage = Nothing
        Me.gbUserSelection.ExpandedNormalImage = Nothing
        Me.gbUserSelection.ExpandedPressedImage = Nothing
        Me.gbUserSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserSelection.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserSelection.HeaderHeight = 25
        Me.gbUserSelection.HeaderMessage = ""
        Me.gbUserSelection.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUserSelection.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserSelection.HeightOnCollapse = 0
        Me.gbUserSelection.LeftTextSpace = 0
        Me.gbUserSelection.Location = New System.Drawing.Point(0, 0)
        Me.gbUserSelection.Name = "gbUserSelection"
        Me.gbUserSelection.OpenHeight = 300
        Me.gbUserSelection.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserSelection.ShowBorder = True
        Me.gbUserSelection.ShowCheckBox = False
        Me.gbUserSelection.ShowCollapseButton = False
        Me.gbUserSelection.ShowDefaultBorderColor = True
        Me.gbUserSelection.ShowDownButton = False
        Me.gbUserSelection.ShowHeader = True
        Me.gbUserSelection.Size = New System.Drawing.Size(349, 311)
        Me.gbUserSelection.TabIndex = 0
        Me.gbUserSelection.Temp = 0
        Me.gbUserSelection.Text = "User Selection"
        Me.gbUserSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetUser
        '
        Me.lnkSetUser.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetUser.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetUser.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetUser.Location = New System.Drawing.Point(249, 285)
        Me.lnkSetUser.Name = "lnkSetUser"
        Me.lnkSetUser.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetUser.TabIndex = 113
        Me.lnkSetUser.TabStop = True
        Me.lnkSetUser.Text = "[ Set User ]"
        Me.lnkSetUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSpc1
        '
        Me.objSpc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSpc1.IsSplitterFixed = True
        Me.objSpc1.Location = New System.Drawing.Point(2, 26)
        Me.objSpc1.Margin = New System.Windows.Forms.Padding(0)
        Me.objSpc1.Name = "objSpc1"
        Me.objSpc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSpc1.Panel1
        '
        Me.objSpc1.Panel1.Controls.Add(Me.txtSearch1)
        Me.objSpc1.Panel1MinSize = 22
        '
        'objSpc1.Panel2
        '
        Me.objSpc1.Panel2.Controls.Add(Me.objchkDCheck)
        Me.objSpc1.Panel2.Controls.Add(Me.dgvDatesUser)
        Me.objSpc1.Size = New System.Drawing.Size(344, 256)
        Me.objSpc1.SplitterDistance = 22
        Me.objSpc1.SplitterWidth = 1
        Me.objSpc1.TabIndex = 109
        '
        'txtSearch1
        '
        Me.txtSearch1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch1.Flags = 0
        Me.txtSearch1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch1.Location = New System.Drawing.Point(0, 0)
        Me.txtSearch1.Name = "txtSearch1"
        Me.txtSearch1.Size = New System.Drawing.Size(344, 21)
        Me.txtSearch1.TabIndex = 107
        '
        'objchkDCheck
        '
        Me.objchkDCheck.AutoSize = True
        Me.objchkDCheck.Location = New System.Drawing.Point(7, 5)
        Me.objchkDCheck.Name = "objchkDCheck"
        Me.objchkDCheck.Size = New System.Drawing.Size(15, 14)
        Me.objchkDCheck.TabIndex = 5
        Me.objchkDCheck.UseVisualStyleBackColor = True
        '
        'dgvDatesUser
        '
        Me.dgvDatesUser.AllowUserToAddRows = False
        Me.dgvDatesUser.AllowUserToDeleteRows = False
        Me.dgvDatesUser.AllowUserToResizeColumns = False
        Me.dgvDatesUser.AllowUserToResizeRows = False
        Me.dgvDatesUser.BackgroundColor = System.Drawing.Color.White
        Me.dgvDatesUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDatesUser.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvDatesUser.ColumnHeadersHeight = 22
        Me.dgvDatesUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDatesUser.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhDCheck, Me.dgcolhDUser, Me.objdgcolhDUserunkid})
        Me.dgvDatesUser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDatesUser.Location = New System.Drawing.Point(0, 0)
        Me.dgvDatesUser.Name = "dgvDatesUser"
        Me.dgvDatesUser.RowHeadersVisible = False
        Me.dgvDatesUser.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDatesUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDatesUser.Size = New System.Drawing.Size(344, 233)
        Me.dgvDatesUser.TabIndex = 16
        '
        'objdgcolhDCheck
        '
        Me.objdgcolhDCheck.HeaderText = ""
        Me.objdgcolhDCheck.Name = "objdgcolhDCheck"
        Me.objdgcolhDCheck.Width = 25
        '
        'dgcolhDUser
        '
        Me.dgcolhDUser.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhDUser.HeaderText = "User"
        Me.dgcolhDUser.Name = "dgcolhDUser"
        Me.dgcolhDUser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhDUserunkid
        '
        Me.objdgcolhDUserunkid.HeaderText = "objdgcolhDUserunkid"
        Me.objdgcolhDUserunkid.Name = "objdgcolhDUserunkid"
        Me.objdgcolhDUserunkid.Visible = False
        '
        'objlnkClose
        '
        Me.objlnkClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlnkClose.AutoSize = True
        Me.objlnkClose.BackColor = System.Drawing.Color.Transparent
        Me.objlnkClose.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkClose.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkClose.LinkColor = System.Drawing.Color.Red
        Me.objlnkClose.Location = New System.Drawing.Point(326, 4)
        Me.objlnkClose.Name = "objlnkClose"
        Me.objlnkClose.Size = New System.Drawing.Size(18, 17)
        Me.objlnkClose.TabIndex = 1
        Me.objlnkClose.TabStop = True
        Me.objlnkClose.Text = "X"
        Me.objlnkClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objfrmERCUserSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(349, 311)
        Me.ControlBox = False
        Me.Controls.Add(Me.gbUserSelection)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmERCUserSelection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.gbUserSelection.ResumeLayout(False)
        Me.gbUserSelection.PerformLayout()
        Me.objSpc1.Panel1.ResumeLayout(False)
        Me.objSpc1.Panel1.PerformLayout()
        Me.objSpc1.Panel2.ResumeLayout(False)
        Me.objSpc1.Panel2.PerformLayout()
        Me.objSpc1.ResumeLayout(False)
        CType(Me.dgvDatesUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbUserSelection As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlnkClose As System.Windows.Forms.LinkLabel
    Friend WithEvents objSpc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtSearch1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objchkDCheck As System.Windows.Forms.CheckBox
    Friend WithEvents dgvDatesUser As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhDCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhDUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDUserunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkSetUser As System.Windows.Forms.LinkLabel
End Class

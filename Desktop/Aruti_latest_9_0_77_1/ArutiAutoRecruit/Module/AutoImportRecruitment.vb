﻿#Region " Imports "

Imports eZeeCommonLib
Imports Microsoft.SqlServer
Imports System.IO
Imports Aruti.Data
Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports ArutiAutoRecruit.WebRef
#End Region

Module AutoImportRecruitment

#Region " Private Variables "

    Private objDatabase As New eZeeDatabase
    Private m_strLogFile As String
    Private ReadOnly mstrModuleName As String = "AutoImportRecruitment"
    Private objCompany As New clsCompany_Master
    Private mdtTable As DataTable
    Private StrQ As String = String.Empty

    Private mstrCompanyCode As String = ""
    Private mstrServerName As String = ""
    Private mstrMBoardSrNo As String = ""
    Private mstrMachineName As String = ""
    Private mintImportStatus As Integer = 0
    Dim mintCompID As Integer = 0

    Private Const MF_BYCOMMAND As Integer = &H0
    Public Const SC_CLOSE As Integer = &HF060
    Private WithEvents objBgWorker As BackgroundWorker

    <DllImport("user32.dll")> _
    Public Function DeleteMenu(ByVal hMenu As IntPtr, ByVal nPosition As Integer, ByVal wFlags As Integer) As Integer
    End Function

    <DllImport("user32.dll")> _
    Private Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    End Function

    <DllImport("kernel32.dll", ExactSpelling:=True)> _
    Private Function GetConsoleWindow() As IntPtr
    End Function
    'Sohail (04 Jul 2012) -- End

#End Region

#Region " Main "

    Public Sub Main()
        Dim objBackup As New clsBackup
        Dim m_strPath As String = String.Empty
        Dim mLinkDesk As String = String.Empty
        Dim dsList As DataSet
        Try

            DeleteMenu(GetSystemMenu(GetConsoleWindow(), False), SC_CLOSE, MF_BYCOMMAND)

            m_strLogFile = IO.Path.Combine(My.Application.Info.DirectoryPath, "AutoRecruit_LOG.txt")

            '===================================================================================
            System.Console.Title = "Aruti Auto Import"
            Call WriteLog("Aruti Auto Import task:.....", ConsoleColor.Magenta)
            Call WriteLog(String.Format("> started at {0}", Now.ToString))
            '===================================================================================

            '===================================================================================
            '***** Check Command Line parameter
            '===================================================================================
            If My.Application.CommandLineArgs.Count <> 1 Then
                Call WriteLog(String.Format("> Error: Invalid Parameters!", Now.ToString), ConsoleColor.Red)
                Exit Sub
            Else
                'm_strPath = My.Application.CommandLineArgs(0)
                mstrCompanyCode = My.Application.CommandLineArgs(0)

                'If Not IO.Directory.Exists(m_strPath) Then
                '    Call WriteLog(String.Format("> Error: Invalid Directory path!", Now.ToString), ConsoleColor.Red)
                '    Exit Sub
                'End If

                'mstrCompanyCode = My.Application.CommandLineArgs(1)

            End If

            '===================================================================================
            '***** Connect to Local Database
            '===================================================================================
            Dim objDatabaseConn As New eZeeCommonLib.eZeeDatabase
            Dim objGSettings As New clsGeneralSettings
            objGSettings._Section = enArutiApplicatinType.Aruti_Payroll.ToString
            objDatabase.ServerName = IIf(objGSettings._ServerName = "", "(Local)", objGSettings._ServerName)
            If Not objDatabase.Connect() Then
                Call WriteLog("> Error: Database Connection Faild.", ConsoleColor.Red)
                Exit Sub
            End If

            Dim objCompany As New clsCompany_Master
            Dim objMaster As New clsMasterData
            Dim intCompanyUnkid As Integer

            gobjCompany = New clsCompany_Master
            gobjConfigOptions = New clsConfigOptions

            dsList = objCompany.GetList("Company", True)
            Dim dtTable As DataTable = New DataView(dsList.Tables("Company"), "code = '" & mstrCompanyCode & "' ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count <= 0 Then
                Call WriteLog("> Error: Invalid Company code.", ConsoleColor.Red)
                Call WriteLog("Press any key to Continue...")
                Console.ReadKey(True)
                Exit Try
            Else
                intCompanyUnkid = CInt(dtTable.Rows(0).Item("companyunkid"))
            End If

            dsList = objMaster.GetCompanyCurrentYearInfo("Year", intCompanyUnkid)
            If dsList.Tables("Year").Rows.Count <= 0 Then
                Call WriteLog("> Error: There is no open Financial Year.", ConsoleColor.Red)
                Call WriteLog("Press any key to Continue...")
                Console.ReadKey(True)
                Exit Try
            End If
            Company._Object._Companyunkid = intCompanyUnkid
            mintCompID = Company._Object._Companyunkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid
            FinancialYear._Object._YearUnkid = CInt(dsList.Tables("Year").Rows(0).Item("yearunkid"))
            eZeeDatabase.change_database(FinancialYear._Object._DatabaseName)

            'objBgWorker = New BackgroundWorker
            'objBgWorker.WorkerReportsProgress = True
            'objBgWorker.WorkerSupportsCancellation = True

            'objBgWorker.RunWorkerAsync()


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Synchronize_Data(True) = False Then
            If Synchronize_Data(True, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                'Shani(24-Aug-2015) -- End

                Call WriteLog("> Error: Import Applicant Process Faild.", ConsoleColor.Red)
                Call WriteLog("Press any key to Continue...")
                Console.ReadKey(True)
                Exit Try
            End If


        Catch ex As Exception
            Call WriteLog("> Error: " & ex.Message)
        End Try
    End Sub

#End Region

#Region " Private Function "
    'Private Sub objBgWorker_Do_Work(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles objBgWorker.DoWork
    '    Try

    '        If Synchronize_Data(True, objBgWorker) = False Then
    '            Call WriteLog("> Error: Import Applicant Process Faild.", ConsoleColor.Red)
    '            Exit Try
    '        End If
    '    Catch ex As Exception
    '        Call WriteLog("> Error in objBgWorker_Do_Work: " & ex.Message, ConsoleColor.Red)
    '    End Try
    'End Sub
    'Private Sub objBgWorker_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles objBgWorker.ProgressChanged
    '    System.Console.SetCursorPosition(22, System.Console.CursorTop)
    '    System.Console.Write("{0}%", e.ProgressPercentage)
    '    If GintTotalApplicantToImport > 0 Then
    '        System.Console.Write("[ {0} / {1} ]", e.ProgressPercentage.ToString, GintTotalApplicantToImport.ToString)
    '    End If
    'End Sub

    'Private Sub objBgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles objBgWorker.RunWorkerCompleted
    '    Try
    '        System.Console.WriteLine("")
    '        If e.Error IsNot Nothing Then
    '            Call WriteLog(e.Error.Message)
    '        ElseIf e.Cancelled Then
    '            Call WriteLog("Canceled")
    '        Else
    '            Call WriteLog(e.Result.ToString)
    '        End If
    '    Catch ex As Exception

    '    Finally
    '        '===================================================================================
    '        Call WriteLog(String.Format("> stopped at {0}", Now.ToString))
    '        Call WriteLog(New String("=", 50))
    '        '===================================================================================
    '        Threading.Thread.Sleep(5000)
    '    End Try
    'End Sub

    Private Sub WriteLog(ByVal strMsg As String, Optional ByVal color As System.ConsoleColor = System.ConsoleColor.Gray)
        If m_strLogFile <> "" Then System.IO.File.AppendAllText(m_strLogFile, strMsg & vbCrLf)
        System.Console.ForegroundColor = color
        System.Console.WriteLine(strMsg)
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Synchronize_Data(ByVal blnIsImport As Boolean) As Boolean
    Public Function Synchronize_Data(ByVal blnIsImport As Boolean, ByVal strArutiSelfServiceURL As String) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim mlinkweb, mLink As String
        Dim mdatabaseweb, mdatabaseserverweb, mdatabaseuserweb, mdatabasepasswordweb, mDatabaseOwnerweb As String 'msql As String
        'Dim ds As DataSet
        Dim objDataOp = New clsDataOperation
        Try

            'Sohail (18 May 2015) -- Start
            'Enhancement - Importing and Exporting through Web Service.
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (06 Jun 2012)
            '    msql = "select * from sys.servers where name='WWW.ARUTIHR.COM' and is_linked = 1"
            'Else
            '    msql = "select * from sys.servers where name='" & ConfigParameter._Object._DatabaseServer & "' and is_linked = 1"
            'End If

            'ds = objDataOp.ExecQuery(msql, "servers")

            'If objDataOp.ErrorMessage <> "" Then
            '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Return False
            'End If

            'If ds.Tables(0).Rows.Count <= 0 Then
            '    Call WriteLog("You cannot perform synchronization process. Reason: Database is not linked with web. Please contact Aruti support team.")
            '    Return False
            'End If
            'Sohail (18 May 2015) -- End

            Dim objNetCon As New clsNetConnectivity
            If objNetCon._Conected = False Then
                Call WriteLog("Internet Connection : " & objNetCon._ConnectionStatus)
                Return False
            End If

            mlinkweb = String.Empty : mLink = String.Empty
            mDatabaseOwnerweb = String.Empty : mdatabasepasswordweb = String.Empty : mdatabaseserverweb = String.Empty : mdatabaseuserweb = String.Empty : mdatabaseweb = String.Empty


            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'NOTE : THIS WILL NOT CHANGE ONCE DEFINED, IT CAN ONLY BE CHANGED WEB SERVER CHANGED
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then
            '    mdatabaseserverweb = "www.arutihr.com"
            '    mdatabaseweb = "arutihrms"
            '    mdatabaseuserweb = "ezeearuti"
            '    mdatabasepasswordweb = "aRutipRo@99"
            '    mDatabaseOwnerweb = "dbo"

            'Else
            '    mdatabaseserverweb = ConfigParameter._Object._DatabaseServer
            '    mdatabaseweb = ConfigParameter._Object._DatabaseName
            '    mdatabaseuserweb = ConfigParameter._Object._DatabaseUserName
            '    mdatabasepasswordweb = ConfigParameter._Object._DatabaseUserPassword
            '    mDatabaseOwnerweb = ConfigParameter._Object._DatabaseOwner
            'End If

            'mlinkweb = "[" & mdatabaseserverweb & "]" & ".[" & mdatabaseweb & "].[" & mDatabaseOwnerweb & "]."
            'Sohail (16 Nov 2018) -- End

            Dim StrCode As String = Company._Object._Code
            Dim intClientCode As Integer = ConfigParameter._Object._WebClientCode

            'Sohail (18 May 2015) -- Start
            'Enhancement - Importing and Exporting through Web Service.
            'If objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcompany_info Where companyunkid = " & intClientCode & " AND company_code = '" & StrCode & "' AND authentication_code = '" & ConfigParameter._Object._AuthenticationCode & "'") <= 0 Then
            '    If objDataOp.ErrorMessage <> "" Then
            '        Call WriteLog("> Error: " & mstrModuleName & "; Proc:Synchronize_Data; " & objDataOp.ErrorMessage)
            '        Return False
            '    End If

            '    Call WriteLog("Sorry, you cannot do selected operation. Reason Invalid webclient code, company code or Authentication code.")
            '    Return False
            'End If
            'objDataOp = Nothing

            'Call WriteLog("Authentication completed.")
            'Sohail (18 May 2015) -- End

            Select Case blnIsImport
                Case True

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ImportFromWeb(mlinkweb, StrCode, intClientCode, mdatabaseserverweb) = False Then
                    If ImportFromWeb(mlinkweb, StrCode, intClientCode, mdatabaseserverweb, strArutiSelfServiceURL) = False Then
                        'Shani(24-Aug-2015) -- End

                        Call WriteLog("Data Import process failed.") : Return False
                    Else
                        Call WriteLog("Data Imported Successfully.") : Return True
                    End If
                Case False

            End Select
        Catch ex As Exception
            Call WriteLog("> Error in Synchronize_Data: " & ex.Message, ConsoleColor.Red)
            Return False
        Finally
            mlinkweb = String.Empty : mLink = String.Empty
            mDatabaseOwnerweb = String.Empty : mdatabasepasswordweb = String.Empty : mdatabaseserverweb = String.Empty : mdatabaseuserweb = String.Empty : mdatabaseweb = String.Empty
        End Try
    End Function

    'Sohail (18 May 2015) -- Start
    'Enhancement - Importing and Exporting through Web Service.
    '    Private Function ImportFromWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal mdatabaseserverweb As String) As Boolean
    '        Dim objApplicant As New clsApplicant_master
    '        Dim StrQ As String = String.Empty
    '        Dim mLinkDesk As String = String.Empty
    '        Dim dsList As New DataSet
    '        Dim mintCurrApplicantUnkID, mintNewApplicantUnkID As Integer
    '        Dim mstrApplicantIDs As String = ""
    '        Dim blnResult As Boolean = False
    '        Dim mintUserunkid As Integer
    '        Dim StartTime As DateTime = Now
    '        Dim objDataOp As New clsDataOperation

    '        Try

    '            mintCurrApplicantUnkID = -1 : mintNewApplicantUnkID = -1
    '            mLinkDesk = Company._Object._ConfigDatabaseName & ".."

    '            If ConfigParameter._Object._PhotoPath = "" Then
    '                Call WriteLog("Please select Image path from configuration -> Option -> Photo Path.")
    '                Threading.Thread.Sleep(5000)
    '                Exit Try
    '            End If

    '            Call GetApplicantImportApplicantStatus()

    '            Dim strMBoardNo As String = GetMotherBoardSrNo()
    '            Dim strMachine As String = My.Computer.Name
    '            Dim intTotalApplicant As Integer = 0

    '            '*** Compare current machine sr no with sr no in configuration database
    '            If mintImportStatus = 1 AndAlso strMBoardNo <> mstrMBoardSrNo Then
    '                Call WriteLog("Sorry! This Process is currently running on " & mstrMachineName & " Machine.")
    '                Exit Try
    '            End If

    '            '*** Set Current Machine SrNo & Name
    '            If mstrMBoardSrNo <> strMBoardNo Then mstrMBoardSrNo = strMBoardNo
    '            If mstrMachineName <> strMachine Then mstrMachineName = strMachine

    '            '*** Put current machine import status so others can't do this process same time.
    '            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|1' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '            If objDataOp.ErrorMessage <> "" Then
    '                Call WriteLog(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Exit Try
    '            End If

    '            StrQ = "SELECT applicantunkid, applicant_code,titleunkid,firstname,surname,othername,gender,email,present_address1,present_address2,present_countryunkid,present_stateunkid,present_province,present_post_townunkid,present_zipcode,present_road,present_estate,present_plotno,present_mobileno,present_alternateno,present_tel_no,present_fax,perm_address1,perm_address2,perm_countryunkid,perm_stateunkid,perm_province,perm_post_townunkid,perm_zipcode,perm_road,perm_estate,perm_plotno,perm_mobileno,perm_alternateno,perm_tel_no,perm_fax,birth_date,marital_statusunkid,anniversary_date,language1unkid,language2unkid,language3unkid,language4unkid,nationality,userunkid,vacancyunkid,isimport,other_skill,other_qualification "
    '            StrQ &= ", employeecode, referenceno "
    '            StrQ &= " , ISNULL(memberships, '') AS memberships, ISNULL(achievements, '') AS achievements, ISNULL(journalsresearchpapers, '') AS journalsresearchpapers " 'Sohail (30 May 2012)
    '            StrQ &= " FROM  " & mlinkweb & "rcapplicant_master where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND ISNULL(referenceno,'') <> ''  " & vbCrLf

    '            dsList = objDataOp.ExecQuery(StrQ, "rcapplicant_master")

    '            If objDataOp.ErrorMessage <> "" Then
    '                Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Return False
    '            End If

    '            GintTotalApplicantToImport = dsList.Tables("rcapplicant_master").Rows.Count

    '            Dim intCount As Integer = 0


    '            For Each drRow As DataRow In dsList.Tables("rcapplicant_master").Rows

    '                'Sohail (12 Nov 2012) -- Start
    '                'TRA - ENHANCEMENT - To Skip the marked for deletion error AND value of a column has been changed after the containing row was last fetched error
    '                objDataOp = New clsDataOperation
    '                'Sohail (12 Nov 2012) -- End

    '                mintCurrApplicantUnkID = CInt(drRow.Item("applicantunkid").ToString)

    '                With objApplicant
    '                    ._Email = drRow.Item("email").ToString
    '                    ._Firstname = drRow.Item("firstname").ToString
    '                    ._Surname = drRow.Item("surname").ToString
    '                    ._Othername = drRow.Item("othername").ToString
    '                    ._Titleunkid = IIf(CInt(drRow.Item("titleunkid")) < 0, 0, CInt(drRow.Item("titleunkid")))
    '                    ._Gender = IIf(CInt(drRow.Item("gender")) < 0, 0, CInt(drRow.Item("gender")))
    '                    ._Vacancyunkid = CInt(drRow.Item("vacancyunkid"))

    '                    '*******************| PERSONAL INFO |******************' START
    '                    ._Present_Address1 = drRow.Item("present_address1").ToString
    '                    ._Present_Address2 = drRow.Item("present_address2").ToString
    '                    ._Present_Alternateno = drRow.Item("present_alternateno").ToString
    '                    ._Present_Countryunkid = IIf(CInt(drRow.Item("present_countryunkid")) < 0, 0, CInt(drRow.Item("present_countryunkid")))
    '                    ._Present_Estate = drRow.Item("present_estate").ToString
    '                    ._Present_Fax = drRow.Item("present_fax").ToString
    '                    ._Present_Mobileno = drRow.Item("present_mobileno").ToString
    '                    ._Present_Plotno = drRow.Item("present_plotno").ToString
    '                    ._Present_Post_Townunkid = IIf(CInt(drRow.Item("present_post_townunkid")) < 0, 0, CInt(drRow.Item("present_post_townunkid")))
    '                    ._Present_Province = drRow.Item("present_province").ToString
    '                    ._Present_Road = drRow.Item("present_road").ToString
    '                    ._Present_Stateunkid = IIf(CInt(drRow.Item("present_stateunkid")) < 0, 0, CInt(drRow.Item("present_stateunkid")))
    '                    ._Present_Tel_No = drRow.Item("present_tel_no").ToString
    '                    ._Present_ZipCode = IIf(CInt(drRow.Item("present_zipcode")) < 0, 0, CInt(drRow.Item("present_zipcode")))
    '                    ._Perm_Address1 = drRow.Item("perm_address1").ToString
    '                    ._Perm_Address2 = drRow.Item("perm_address2").ToString
    '                    ._Perm_Alternateno = drRow.Item("perm_alternateno").ToString
    '                    ._Perm_Countryunkid = IIf(CInt(drRow.Item("perm_countryunkid")) < 0, 0, CInt(drRow.Item("perm_countryunkid")))
    '                    ._Perm_Estate = drRow.Item("perm_estate").ToString
    '                    ._Perm_Fax = drRow.Item("perm_fax").ToString
    '                    ._Perm_Mobileno = drRow.Item("perm_mobileno").ToString
    '                    ._Perm_Plotno = drRow.Item("perm_plotno").ToString
    '                    ._Perm_Post_Townunkid = IIf(CInt(drRow.Item("perm_post_townunkid")) < 0, 0, CInt(drRow.Item("perm_post_townunkid")))
    '                    ._Perm_Province = drRow.Item("perm_province").ToString
    '                    ._Perm_Road = drRow.Item("perm_road").ToString
    '                    ._Perm_Stateunkid = IIf(CInt(drRow.Item("perm_stateunkid")) < 0, 0, CInt(drRow.Item("perm_stateunkid")))
    '                    ._Perm_Tel_No = drRow.Item("perm_tel_no").ToString
    '                    ._Perm_ZipCode = IIf(CInt(drRow.Item("perm_zipcode")) < 0, 0, CInt(drRow.Item("perm_zipcode")))
    '                    '*******************| PERSONAL INFO |******************' END

    '                    '*******************| ADDITIONAL INFO |******************' START
    '                    If drRow.Item("anniversary_date").ToString.Trim.Length > 0 Then
    '                        ._Anniversary_Date = CDate(drRow.Item("anniversary_date"))
    '                    Else
    '                        ._Anniversary_Date = Nothing
    '                    End If

    '                    If drRow.Item("birth_date").ToString.Trim.Length > 0 Then
    '                        ._Birth_Date = CDate(drRow.Item("birth_date"))
    '                    Else
    '                        ._Birth_Date = Nothing
    '                    End If

    '                    ._Marital_Statusunkid = IIf(CInt(drRow.Item("marital_statusunkid")) < 0, 0, CInt(drRow.Item("marital_statusunkid")))
    '                    ._Nationality = IIf(CInt(drRow.Item("nationality")) < 0, 0, CInt(drRow.Item("nationality")))
    '                    ._Language1unkid = IIf(CInt(drRow.Item("language1unkid")) < 0, 0, CInt(drRow.Item("language1unkid")))
    '                    ._Language2unkid = IIf(CInt(drRow.Item("language2unkid")) < 0, 0, CInt(drRow.Item("language2unkid")))
    '                    ._Language3unkid = IIf(CInt(drRow.Item("language3unkid")) < 0, 0, CInt(drRow.Item("language3unkid")))
    '                    ._Language4unkid = IIf(CInt(drRow.Item("language4unkid")) < 0, 0, CInt(drRow.Item("language4unkid")))
    '                    ._Userunkid = 1
    '                    mintUserunkid = 1
    '                    ._OtherQualifications = drRow.Item("other_qualification").ToString
    '                    ._OtherSkills = drRow.Item("other_skill").ToString
    '                    ._Employeecode = drRow.Item("employeecode").ToString
    '                    ._Referenceno = drRow.Item("referenceno").ToString
    '                    ._Memberships = drRow.Item("memberships").ToString
    '                    ._Achievements = drRow.Item("achievements").ToString
    '                    ._JournalsResearchPapers = drRow.Item("journalsresearchpapers").ToString
    '                    '*******************| ADDITIONAL INFO |******************' END

    '                    Dim intExistingApplicantUnkId As Integer = 0
    '                    Dim strApplicant_Code As String = ""
    '                    Dim strFirstname As String = ""
    '                    Dim strSurname As String = ""

    '                    .isExistForWebImport(._Referenceno, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
    '                    If intExistingApplicantUnkId = 0 Then
    '                        ._Applicant_Code = ""
    '                        blnResult = .Insert()
    '                    Else
    '                        ._Applicantunkid = intExistingApplicantUnkId
    '                        ._Applicant_Code = strApplicant_Code
    '                        ._Firstname = strFirstname
    '                        ._Surname = strSurname
    '                        blnResult = .Update()
    '                    End If

    '                    If blnResult = False And ._Message <> "" Then
    '                        Call WriteLog("> Error: " & ._Message)
    '                        Return False
    '                    End If

    '                    If intExistingApplicantUnkId = 0 Then
    '                        mintNewApplicantUnkID = ._Applicantunkid
    '                    Else
    '                        mintNewApplicantUnkID = intExistingApplicantUnkId
    '                    End If


    '                    If mstrApplicantIDs.Trim = "" Then
    '                        mstrApplicantIDs = mintNewApplicantUnkID.ToString
    '                    Else
    '                        mstrApplicantIDs &= ", " & mintNewApplicantUnkID.ToString
    '                    End If

    '                    '**********[ JOBHISTORY ]********* START
    '                    If intExistingApplicantUnkId > 0 Then
    '                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If

    '                        StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If


    '                    StrQ = "INSERT INTO rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason) " & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason from  " & mlinkweb & "rcjobhistory where Comp_Code='" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                    StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcjobhistory", "jobhistorytranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Return False
    '                    End If

    '                    '**********[ JOBHISTORY ]********* END

    '                    '**********[ SKILL TRAN ]********* START
    '                    'Sohail (23 Dec 2011) -- Start
    '                    If intExistingApplicantUnkId > 0 Then
    '                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If

    '                        StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If

    '                    StrQ = "INSERT into  rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill )" & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill  from  " & mlinkweb & "rcapplicantskill_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' '" & vbCrLf
    '                    StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantskill_tran", "skilltranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Return False
    '                    End If
    '                    '**********[ SKILL TRAN ]********* END

    '                    '**********[ QUAKIFICATION TRAN ]********* START
    '                    If intExistingApplicantUnkId > 0 Then
    '                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If

    '                        StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If
    '                    StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode )" & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode  from  " & mlinkweb & "rcapplicantqualification_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                    StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantqualification_tran", "qualificationtranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Return False
    '                    End If

    '                    blnResult = True
    '                    '**********[ QUAKIFICATION TRAN ]********* END

    '                    '**********[ REFERENCE TRAN ]********* START
    '                    If intExistingApplicantUnkId > 0 Then
    '                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If

    '                        StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If
    '                    StrQ = "INSERT INTO rcapp_reference_tran(applicantunkid, name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid)" & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid from  " & mlinkweb & "rcapp_reference_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                    StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_reference_tran", "referencetranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Return False
    '                    End If
    '                    blnResult = True
    '                    '**********[ REFERENCE TRAN ]********* END

    '                    '**********[ VACANCY MAPPING ]********* START
    '                    If intExistingApplicantUnkId > 0 Then
    '                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_vacancy_mapping", "appvacancytranunkid", 3, 3) = False Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If

    '                        StrQ = "DELETE FROM rcapp_vacancy_mapping WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If
    '                    StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive)" & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",vacancyunkid, 1, 1 from  " & mlinkweb & "rcapp_vacancy_mapping where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                    StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
    '                    StrQ &= " AND vacancyunkid > 0 " ' To Prevent Vacancies which are updated from TEST LINK

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Return False
    '                    End If
    '                    blnResult = True
    '                    '**********[ VACANCY MAPPING ]********* END

    '                    '**********[ IMAGES TRAN ]********* START
    '                    If intExistingApplicantUnkId > 0 Then

    '                        StrQ = "DELETE FROM hr_images_tran WHERE employeeunkid  = " & intExistingApplicantUnkId & " and referenceid = " & enImg_Email_RefId.Applicant_Module & " AND isapplicant = 1 "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                    End If
    '                    StrQ = "INSERT INTO hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant) " & vbCrLf
    '                    StrQ &= "SELECT " & mintNewApplicantUnkID & ",imagename," & mintNewApplicantUnkID & ",referenceid,isapplicant from " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                    StrQ &= " AND employeeunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If
    '                    '**********[ IMAGES TRAN ]********* END


    '                    '*** Applicant Master
    '                    StrQ = "UPDATE   " & mlinkweb & "rcapplicant_master  set Syncdatetime= getdate()  where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    '*** Job History
    '                    StrQ = "UPDATE " & mlinkweb & "rcjobhistory  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and   isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    '*** Applicant Skill Tran
    '                    StrQ = "UPDATE " & mlinkweb & "rcapplicantskill_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & "  "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    '*** Applicant Qualification Tran
    '                    StrQ = "UPDATE " & mlinkweb & "rcapplicantqualification_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    '*** Applicant Reference Tran
    '                    StrQ = "UPDATE " & mlinkweb & "rcapp_reference_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If

    '                    '*** Applicant Vacancy Mapping Tran
    '                    StrQ = "UPDATE " & mlinkweb & "rcapp_vacancy_mapping  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        'Sohail (09 Nov 2012) -- Start
    '                        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        'Return False
    '                        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
    '                            GoTo CONTINUE_FOR
    '                        Else
    '                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                            Return False
    '                        End If
    '                        'Sohail (09 Nov 2012) -- End
    '                    End If
    '                End With

    'CONTINUE_FOR:   'Sohail (09 Nov 2012)
    '                intCount = intCount + 1
    '                WriteLog("[ " & intCount.ToString & " / " & GintTotalApplicantToImport.ToString & " ] Applicatnts Imported.")

    '            Next


    '            If mstrApplicantIDs.Trim <> "" Then

    '                'Sohail (12 Nov 2012) -- Start
    '                'TRA - ENHANCEMENT - To Skip the marked for deletion error and value of a column has been changed after the containing row was last fetched error
    '                objDataOp = New clsDataOperation
    '                'Sohail (12 Nov 2012) -- End

    '                Dim imgServerPath As String = "http://" & Split(mdatabaseserverweb, "\")(0)
    '                Dim imgLocalPath As String = ConfigParameter._Object._PhotoPath & "\"
    '                Dim objWebRequest As System.Net.WebRequest
    '                Dim objWebResponse As System.Net.WebResponse
    '                Dim reader As IO.Stream
    '                Dim writer As IO.Stream
    '                Dim buffer(1023) As Byte
    '                Dim bytesRead As Integer
    '                Dim lngTotalBytes As Long
    '                Dim lngMaxBytes As Long
    '                Dim intCnt, i As Integer
    '                Dim dsImages As DataSet
    '                Dim imgWebpath As String = "/Aruti/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >

    '                StrQ = "SELECT imagename FROM " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND  ISNULL(imagename,' ') <> ' ' "
    '                dsImages = objDataOp.ExecQuery(StrQ, "hr_images_tran")
    '                intCnt = dsImages.Tables("hr_images_tran").Rows.Count
    '                i = 0

    '                For Each dsRow As DataRow In dsImages.Tables("hr_images_tran").Rows
    '                    i += 1
    '                    objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dsRow.Item("imagename")))
    '                    objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

    '                    If objApplicant.WebFileExist(objWebRequest) = True Then
    '                        objWebResponse = objWebRequest.GetResponse

    '                        lngMaxBytes = CLng(objWebResponse.ContentLength)
    '                        reader = objWebResponse.GetResponseStream
    '                        writer = IO.File.Create(imgLocalPath & CStr(dsRow.Item("imagename")))
    '                        bytesRead = 0
    '                        lngTotalBytes = 0
    '                        While True
    '                            bytesRead = reader.Read(buffer, 0, buffer.Length)
    '                            If bytesRead <= 0 Then Exit While

    '                            writer.Write(buffer, 0, bytesRead)
    '                            lngTotalBytes += bytesRead
    '                        End While
    '                        reader.Close()
    '                        writer.Close()
    '                    End If
    '                Next

    '                StrQ = "UPDATE " & mlinkweb & "hr_images_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    Return False
    '                End If

    '                objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '                If objDataOp.ErrorMessage <> "" Then
    '                    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    Return False
    '                End If

    '                '*** Insert Audit Log 
    '                Call InsertATLog(mstrApplicantIDs, False, StartTime)
    '            End If



    '            Return True


    '        Catch ex As Exception
    '            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '            If objDataOp.ErrorMessage <> "" Then
    '                Call WriteLog(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            End If
    '            '*** Insert Audit Log 
    '            Call InsertATLog(mstrApplicantIDs, True, StartTime)
    '            Threading.Thread.Sleep(5000)
    '        End Try

    '    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function ImportFromWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal mdatabaseserverweb As String) As Boolean
    Private Function ImportFromWeb(ByVal mlinkweb As String, _
                                   ByVal StrCode As String, _
                                   ByVal intClientCode As Integer, _
                                   ByVal mdatabaseserverweb As String, _
                                   ByVal strArutiSelfServiceURL As String _
                                   ) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim objApplicant As New clsApplicant_master
        Dim StrQ As String = String.Empty
        Dim mLinkDesk As String = String.Empty
        Dim dsList As New DataSet
        Dim mintCurrApplicantUnkID, mintNewApplicantUnkID As Integer
        Dim mstrApplicantIDs As String = ""
        Dim blnResult As Boolean = False
        Dim mintUserunkid As Integer
        Dim StartTime As DateTime = Now
        Dim exForce As Exception
        Dim ds As DataSet = Nothing
        Dim objDataOp As New clsDataOperation

        Try

            mintCurrApplicantUnkID = -1 : mintNewApplicantUnkID = -1
            mLinkDesk = Company._Object._ConfigDatabaseName & ".."

            If ConfigParameter._Object._IsImgInDataBase = False AndAlso ConfigParameter._Object._PhotoPath = "" Then
                Call WriteLog("Please select Image path from configuration -> Option -> Photo Path.")
                Threading.Thread.Sleep(5000)
                Exit Try
            End If

            If ConfigParameter._Object._Document_Path = "" Then
                Call WriteLog("Please select Document path from configuration -> Option -> Documents Path.")
                Threading.Thread.Sleep(5000)
                Exit Try
            End If

            Call GetApplicantImportApplicantStatus()

            Dim strMBoardNo As String = GetMotherBoardSrNo()
            Dim strMachine As String = My.Computer.Name
            Dim intTotalApplicant As Integer = 0

            '*** Compare current machine sr no with sr no in configuration database
            If mintImportStatus = 1 AndAlso strMBoardNo <> mstrMBoardSrNo Then
                Call WriteLog("Sorry! This Process is currently running on " & mstrMachineName & " Machine.")
                Exit Try
            End If

            '*** Set Current Machine SrNo & Name
            If mstrMBoardSrNo <> strMBoardNo Then mstrMBoardSrNo = strMBoardNo
            If mstrMachineName <> strMachine Then mstrMachineName = strMachine

            '*** Put current machine import status so others can't do this process same time.
            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|1' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            If objDataOp.ErrorMessage <> "" Then
                Call WriteLog(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Exit Try
            End If


            Dim dir As String = System.IO.Directory.GetCurrentDirectory()

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport 'Sohail (05 Dec 2016)
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text 'Sohail (11 Dec 2015) - [CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
            End With

            'Dim EndPointAdd As System.ServiceModel.EndpointAddress 'Sohail (16 Nov 2018)

            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (16 Nov 2018)
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/Aruti/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/ArutiHRM/ArutiRecruitmentService.asmx") 'Sohail (16 Nov 2018)
                'Sohail (05 Dec 2016) -- End
            'Else 'Sohail (16 Nov 2018)
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & ConfigParameter._Object._DatabaseServer.Replace("\apayroll", "/Aruti") & "/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & ConfigParameter._Object._DatabaseServer.Replace("\apayroll", "") & "/ArutiHRM/ArutiRecruitmentService.asmx") 'Sohail (16 Nov 2018)
                'Sohail (26 May 2017) -- End
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & ConfigParameter._Object._DatabaseServer.Replace("\apayroll", "/ArutiHRM") & "/ArutiRecruitmentService.asmx")
                'Sohail (05 Dec 2016) -- End
                'End If 'Sohail (16 Nov 2018)

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim WebRef As New ArutiRecruitmentServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials
            Dim strWebServiceLink As String = ConfigParameter._Object._RecruitmentWebServiceLink
            Dim WebRef As New WR.ArutiRecruitmentService
            Dim UserCred As New WR.UserCredentials
            WebRef.Url = strWebServiceLink
            'Sohail (16 Nov 2018) -- End
            WebRef.Timeout = 1200000 '20 minutes 'Sohail (21 Sep 2019)
            Dim strErrorMessage As String = ""

            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'UserCred.Password = clsSecurity.Decrypt("CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo=", "ezee")
            UserCred.Password = "CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo="
            'Sohail (16 Nov 2018) -- End
            UserCred.WebClientID = intClientCode
            UserCred.CompCode = StrCode
            UserCred.AuthenticationCode = ConfigParameter._Object._AuthenticationCode
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            WebRef.UserCredentialsValue = UserCred
            'Sohail (16 Nov 2018) -- End


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsApplicant As DataSet = WebRef.GetApplicants(UserCred, strErrorMessage, True, 0)
            Dim dsApplicant As DataSet = WebRef.GetApplicants(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsJobHistory As DataSet = WebRef.GetJobHistory(UserCred, strErrorMessage, True, 0)
            Dim dsJobHistory As DataSet = WebRef.GetJobHistory(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsSkill As DataSet = WebRef.GetSkill(UserCred, strErrorMessage, True, 0)
            Dim dsSkill As DataSet = WebRef.GetSkill(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsQualification As DataSet = WebRef.GetQualification(UserCred, strErrorMessage, True, 0)
            Dim dsQualification As DataSet = WebRef.GetQualification(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsReference As DataSet = WebRef.GetReference(UserCred, strErrorMessage, True, 0)
            Dim dsReference As DataSet = WebRef.GetReference(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsVacancy As DataSet = WebRef.GetVacancy(UserCred, strErrorMessage, True, 0)
            Dim dsVacancy As DataSet = WebRef.GetVacancy(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsImages As DataSet = WebRef.GetImages(UserCred, strErrorMessage, True, 0)
            Dim dsImages As DataSet = WebRef.GetImages(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsAttachments As DataSet = WebRef.GetAttachments(UserCred, strErrorMessage, True, 0)
            Dim dsAttachments As DataSet = WebRef.GetAttachments(strErrorMessage, True, 0)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'StrQ = "SELECT applicantunkid, applicant_code,titleunkid,firstname,surname,othername,gender,email,present_address1,present_address2,present_countryunkid,present_stateunkid,present_province,present_post_townunkid,present_zipcode,present_road,present_estate,present_plotno,present_mobileno,present_alternateno,present_tel_no,present_fax,perm_address1,perm_address2,perm_countryunkid,perm_stateunkid,perm_province,perm_post_townunkid,perm_zipcode,perm_road,perm_estate,perm_plotno,perm_mobileno,perm_alternateno,perm_tel_no,perm_fax,birth_date,marital_statusunkid,anniversary_date,language1unkid,language2unkid,language3unkid,language4unkid,nationality,userunkid,vacancyunkid,isimport,other_skill,other_qualification "
            'StrQ &= ", employeecode, referenceno "
            'StrQ &= " , ISNULL(memberships, '') AS memberships, ISNULL(achievements, '') AS achievements, ISNULL(journalsresearchpapers, '') AS journalsresearchpapers " 'Sohail (30 May 2012)
            'StrQ &= " FROM  " & mlinkweb & "rcapplicant_master where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND ISNULL(referenceno,'') <> ''  " & vbCrLf

            'dsList = objDataOp.ExecQuery(StrQ, "rcapplicant_master")

            'If objDataOp.ErrorMessage <> "" Then
            '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Return False
            'End If

            GintTotalApplicantToImport = dsApplicant.Tables(0).Rows.Count

            Dim intCount As Integer = 0

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim imgServerPath As String = "http://" & Split(mdatabaseserverweb, "\")(0)
            Dim imgServerPath As String = strWebServiceLink.ToUpper.Replace("ARUTIRECRUITMENTSERVICE.ASMX", "")
            'Sohail (16 Nov 2018) -- End
            Dim imgLocalPath As String = ConfigParameter._Object._PhotoPath & "\"
            Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\" 'Sohail (22 Apr 2015)
            Dim objWebRequest As System.Net.WebRequest
            Dim objWebResponse As System.Net.WebResponse
            Dim reader As IO.Stream
            Dim writer As IO.Stream
            Dim buffer(1023) As Byte
            Dim bytesRead As Integer
            Dim lngTotalBytes As Long
            Dim lngMaxBytes As Long
            'Dim intCnt, i As Integer
            'Dim dsImagess As DataSet
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            'Dim imgWebpath As String = "/Aruti/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim imgWebpath As String = "/ArutiHRM/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >
            Dim imgWebpath As String = "/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >
            'Sohail (16 Nov 2018) -- End
            'Sohail (05 Dec 2016) -- End



            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            'If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
            'Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS)
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
            'Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
            Dim dsFolderName As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs", 0)
            Dim dicFolderName As Dictionary(Of Integer, String) = (From p In dsFolderName.Tables(0) Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            Dim strFolderName As String = ""
            'Sohail (04 Jul 2019) -- End
            If strFolderName IsNot Nothing Then strFolderName = strFolderName
            'SHANI (16 JUL 2015) -- End
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            'Sohail (05 Dec 2016) -- End

            For Each drRow As DataRow In dsApplicant.Tables(0).Rows

                'Sohail (12 Nov 2012) -- Start
                'TRA - ENHANCEMENT - To Skip the marked for deletion error AND value of a column has been changed after the containing row was last fetched error
                objDataOp = New clsDataOperation
                'Sohail (12 Nov 2012) -- End

                mintCurrApplicantUnkID = CInt(drRow.Item("applicantunkid").ToString)

                With objApplicant
                    ._Email = drRow.Item("email").ToString
                    ._Firstname = drRow.Item("firstname").ToString
                    ._Surname = drRow.Item("surname").ToString
                    ._Othername = drRow.Item("othername").ToString
                    ._Titleunkid = IIf(CInt(drRow.Item("titleunkid")) < 0, 0, CInt(drRow.Item("titleunkid")))
                    ._Gender = IIf(CInt(drRow.Item("gender")) < 0, 0, CInt(drRow.Item("gender")))
                    ._Vacancyunkid = CInt(drRow.Item("vacancyunkid"))

                    '*******************| PERSONAL INFO |******************' START
                    ._Present_Address1 = drRow.Item("present_address1").ToString
                    ._Present_Address2 = drRow.Item("present_address2").ToString
                    ._Present_Alternateno = drRow.Item("present_alternateno").ToString
                    ._Present_Countryunkid = IIf(CInt(drRow.Item("present_countryunkid")) < 0, 0, CInt(drRow.Item("present_countryunkid")))
                    ._Present_Estate = drRow.Item("present_estate").ToString
                    ._Present_Fax = drRow.Item("present_fax").ToString
                    ._Present_Mobileno = drRow.Item("present_mobileno").ToString
                    ._Present_Plotno = drRow.Item("present_plotno").ToString
                    ._Present_Post_Townunkid = IIf(CInt(drRow.Item("present_post_townunkid")) < 0, 0, CInt(drRow.Item("present_post_townunkid")))
                    ._Present_Province = drRow.Item("present_province").ToString
                    ._Present_Road = drRow.Item("present_road").ToString
                    ._Present_Stateunkid = IIf(CInt(drRow.Item("present_stateunkid")) < 0, 0, CInt(drRow.Item("present_stateunkid")))
                    ._Present_Tel_No = drRow.Item("present_tel_no").ToString
                    ._Present_ZipCode = IIf(CInt(drRow.Item("present_zipcode")) < 0, 0, CInt(drRow.Item("present_zipcode")))
                    ._Perm_Address1 = drRow.Item("perm_address1").ToString
                    ._Perm_Address2 = drRow.Item("perm_address2").ToString
                    ._Perm_Alternateno = drRow.Item("perm_alternateno").ToString
                    ._Perm_Countryunkid = IIf(CInt(drRow.Item("perm_countryunkid")) < 0, 0, CInt(drRow.Item("perm_countryunkid")))
                    ._Perm_Estate = drRow.Item("perm_estate").ToString
                    ._Perm_Fax = drRow.Item("perm_fax").ToString
                    ._Perm_Mobileno = drRow.Item("perm_mobileno").ToString
                    ._Perm_Plotno = drRow.Item("perm_plotno").ToString
                    ._Perm_Post_Townunkid = IIf(CInt(drRow.Item("perm_post_townunkid")) < 0, 0, CInt(drRow.Item("perm_post_townunkid")))
                    ._Perm_Province = drRow.Item("perm_province").ToString
                    ._Perm_Road = drRow.Item("perm_road").ToString
                    ._Perm_Stateunkid = IIf(CInt(drRow.Item("perm_stateunkid")) < 0, 0, CInt(drRow.Item("perm_stateunkid")))
                    ._Perm_Tel_No = drRow.Item("perm_tel_no").ToString
                    ._Perm_ZipCode = IIf(CInt(drRow.Item("perm_zipcode")) < 0, 0, CInt(drRow.Item("perm_zipcode")))
                    '*******************| PERSONAL INFO |******************' END

                    '*******************| ADDITIONAL INFO |******************' START
                    If drRow.Item("anniversary_date").ToString.Trim.Length > 0 Then
                        ._Anniversary_Date = CDate(drRow.Item("anniversary_date"))
                    Else
                        ._Anniversary_Date = Nothing
                    End If

                    If drRow.Item("birth_date").ToString.Trim.Length > 0 Then
                        ._Birth_Date = CDate(drRow.Item("birth_date"))
                    Else
                        ._Birth_Date = Nothing
                    End If

                    ._Marital_Statusunkid = IIf(CInt(drRow.Item("marital_statusunkid")) < 0, 0, CInt(drRow.Item("marital_statusunkid")))
                    ._Nationality = IIf(CInt(drRow.Item("nationality")) < 0, 0, CInt(drRow.Item("nationality")))
                    ._Language1unkid = IIf(CInt(drRow.Item("language1unkid")) < 0, 0, CInt(drRow.Item("language1unkid")))
                    ._Language2unkid = IIf(CInt(drRow.Item("language2unkid")) < 0, 0, CInt(drRow.Item("language2unkid")))
                    ._Language3unkid = IIf(CInt(drRow.Item("language3unkid")) < 0, 0, CInt(drRow.Item("language3unkid")))
                    ._Language4unkid = IIf(CInt(drRow.Item("language4unkid")) < 0, 0, CInt(drRow.Item("language4unkid")))
                    ._Userunkid = 1
                    mintUserunkid = 1
                    ._OtherQualifications = drRow.Item("other_qualification").ToString
                    ._OtherSkills = drRow.Item("other_skill").ToString
                    ._Employeecode = drRow.Item("employeecode").ToString
                    ._Referenceno = drRow.Item("referenceno").ToString
                    ._Memberships = drRow.Item("memberships").ToString
                    ._Achievements = drRow.Item("achievements").ToString
                    ._JournalsResearchPapers = drRow.Item("journalsresearchpapers").ToString
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                ._UserId = drRow.Item("UserId").ToString
                'Sohail (05 Dec 2016) -- End

                'Nilay (13 Apr 2017) -- Start
                ._MotherTongue = drRow.Item("mother_tongue").ToString
                ._IsImpaired = CBool(drRow.Item("isimpaired"))
                ._Impairment = drRow.Item("impairment").ToString
                ._CurrentSalary = CDec(drRow.Item("current_salary").ToString)
                ._ExpectedSalary = CDec(drRow.Item("expected_salary"))
                ._ExpectedBenefits = drRow.Item("expected_benefits").ToString
                ._WillingToRelocate = CBool(drRow.Item("willing_to_relocate"))
                ._WillingToTravel = CBool(drRow.Item("willing_to_travel"))
                ._NoticePeriodDays = CInt(drRow.Item("notice_period_days"))
                'Nilay (13 Apr 2017) -- End

                    'Hemant (09 July 2018) -- Start
                    'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
                    ._CurrentSalaryCurrencyId = CInt(drRow.Item("current_salary_currency_id"))
                    ._ExpectedSalaryCurrencyId = CInt(drRow.Item("expected_salary_currency_id"))
                    'Hemant (09 July 2018) -- End

                    'Sohail (09 Oct 2018) -- Start
                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                    ._IsFromOnline = True
                    'Sohail (09 Oct 2018) -- End

                    '*******************| ADDITIONAL INFO |******************' END

                    Dim intExistingApplicantUnkId As Integer = 0
                    Dim strApplicant_Code As String = ""
                    Dim strFirstname As String = ""
                    Dim strSurname As String = ""

                    'Sohail (05 Dec 2016) -- Start
                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                    '.isExistForWebImport(._Referenceno, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
                    .isExistForWebImport(._Email, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
                    'Sohail (05 Dec 2016) -- End

                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime

                    If intExistingApplicantUnkId = 0 Then
                        ._Applicant_Code = ""

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'blnResult = .Insert()
                        blnResult = .Insert(ConfigParameter._Object._ApplicantCodeNotype, ConfigParameter._Object._ApplicantCodePrifix)
                        'Shani(24-Aug-2015) -- End

                    Else
                        ._Applicantunkid = intExistingApplicantUnkId
                        ._Applicant_Code = strApplicant_Code
                    'Sohail (05 Dec 2016) -- Start
                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                        '._Firstname = strFirstname
                        '._Surname = strSurname
                    'Sohail (05 Dec 2016) -- End
                        blnResult = .Update()
                    End If

                    If blnResult = False And ._Message <> "" Then
                        Call WriteLog("> Error: " & ._Message)
                        Return False

                    ElseIf blnResult = False Then
                        Exit For

                    End If

                    If intExistingApplicantUnkId = 0 Then
                        mintNewApplicantUnkID = ._Applicantunkid
                    Else
                        mintNewApplicantUnkID = intExistingApplicantUnkId
                    End If


                    If mstrApplicantIDs.Trim = "" Then
                        mstrApplicantIDs = mintNewApplicantUnkID.ToString
                    Else
                        mstrApplicantIDs &= ", " & mintNewApplicantUnkID.ToString
                    End If

                    '**********[ JOBHISTORY ]********* START
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If intExistingApplicantUnkId > 0 Then
                    '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If

                    '    StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    '    objDataOp.ExecNonQuery(StrQ)

                    '    If objDataOp.ErrorMessage <> "" Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If
                    'End If
                    'Sohail (26 May 2017) -- End

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'Dim dr_Job() As DataRow = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    Dim dr_Job() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullJobHistoryUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetJobHistory(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetJobHistory(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_Job = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                            Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Return False
                        End If
                            objCommonATLog = Nothing

                        StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Return False
                        End If
                        Else
                            dr_Job = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                        End If
                    Else
                        dr_Job = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                    'Sohail (26 May 2017) -- End

                    If dr_Job.Length > 0 Then

                        For Each dRow As DataRow In dr_Job

                            'Sohail (26 May 2017) -- Start
                            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                            'StrQ = "INSERT INTO rcjobhistory(applicantunkid, employername,companyname, designation, responsibility,joiningdate,terminationdate,officephone,leavingreason, achievements) " & vbCrLf
                            'StrQ &= "VALUES (" & mintNewApplicantUnkID & " , '" & dRow.Item("employername").ToString.Replace("'", "''") & "', '" & dRow.Item("companyname").ToString.Replace("'", "''") & "', '" & dRow.Item("designation").ToString.Replace("'", "''") & "', '" & dRow.Item("responsibility").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("joiningdate")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & CDate(dRow.Item("terminationdate")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & dRow.Item("officephone").ToString.Replace("'", "''") & "', '" & dRow.Item("leavingreason").ToString.Replace("'", "''") & "', '" & dRow.Item("achievements").ToString.Replace("'", "''") & "' ) " & vbCrLf

                            'objDataOp.ExecNonQuery(StrQ)

                            'If objDataOp.ErrorMessage <> "" Then
                            '    'Sohail (09 Nov 2012) -- Start
                            '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            '    'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '    'Return False
                            '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                            '        Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                            '        GoTo CONTINUE_FOR
                            '    Else
                            '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '        Return False
                            '    End If
                            '    'Sohail (09 Nov 2012) -- End
                            'End If

                            'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcjobhistory", "jobhistorytranunkid", 1, 1, , , , mintUserunkid) = False Then
                            '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            '    Return False
                            'End If
                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerJobHistoryUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverjobhistorytranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("jobhistorytranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                            'Sohail (26 May 2017) -- End
                        Next

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        Dim objJobHistory As New clsJobhistory
                        'Sohail (24 Jan 2018) -- Start
                        'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                        'objJobHistory._Applicantunkid = intExistingApplicantUnkId
                        objJobHistory._Applicantunkid = mintNewApplicantUnkID
                        'Sohail (24 Jan 2018) -- End
                        objJobHistory._DataList = dr_Job.CopyToDataTable
                        With objJobHistory
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objJobHistory.InsertUpdateDelete_JobHistoryTran() = False Then
                            Call WriteLog("> Error: error in applicant job history")
                                Return False
                            End If
                        'Sohail (26 May 2017) -- End
                    End If
                    '**********[ JOBHISTORY ]********* END

                    '**********[ SKILL TRAN ]********* START
                    'Sohail (23 Dec 2011) -- Start
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If intExistingApplicantUnkId > 0 Then
                    '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If

                    '    StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    '    objDataOp.ExecNonQuery(StrQ)

                    '    If objDataOp.ErrorMessage <> "" Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If
                    'End If
                    'Sohail (26 May 2017) -- End

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'Dim dr_Skill() As DataRow = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    Dim dr_Skill() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullSkillUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetSkill(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetSkill(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_Skill = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                            Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Return False
                        End If
                            objCommonATLog = Nothing
                        StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Return False
                        End If
                        Else
                            dr_Skill = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                        End If
                    Else
                        dr_Skill = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                    'Sohail (26 May 2017) -- End

                    If dr_Skill.Length > 0 Then
                        For Each dRow As DataRow In dr_Skill

                            'Sohail (26 May 2017) -- Start
                            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                            'StrQ = "INSERT into  rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill )" & vbCrLf
                            'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("skillcategoryunkid") & ", " & dRow.Item("skillunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skillcategory").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skill").ToString.Replace("'", "''") & "' )" & vbCrLf

                            'objDataOp.ExecNonQuery(StrQ)

                            'If objDataOp.ErrorMessage <> "" Then
                            '    'Sohail (09 Nov 2012) -- Start
                            '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            '    'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '    'Return False
                            '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                            '        Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                            '        GoTo CONTINUE_FOR
                            '    Else
                            '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '        Return False
                            '    End If
                            'End If
                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerSkillUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverskilltranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("skilltranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                            'Sohail (26 May 2017) -- End
                        Next
                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        Dim objSkill As New clsApplicantSkill_tran
                        'Sohail (24 Jan 2018) -- Start
                        'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                        'objSkill._ApplicantUnkid = intExistingApplicantUnkId
                        objSkill._ApplicantUnkid = mintNewApplicantUnkID
                        'Sohail (24 Jan 2018) -- End
                        objSkill._DataList = dr_Skill.CopyToDataTable
                        With objSkill
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objSkill.InsertUpdateDelete_SkillTran() = False Then
                            Call WriteLog("error in applicant skill")
                            Return False
                        End If
                        'Sohail (26 May 2017) -- End
                    End If

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantskill_tran", "skilltranunkid", 1, 1, , , , mintUserunkid) = False Then
                    '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '    Return False
                    'End If
                    'Sohail (26 May 2017) -- End
                    '**********[ SKILL TRAN ]********* END

                    '**********[ QUAKIFICATION TRAN ]********* START
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If intExistingApplicantUnkId > 0 Then
                    '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If

                    '    StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    '    objDataOp.ExecNonQuery(StrQ)

                    '    If objDataOp.ErrorMessage <> "" Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If
                    'End If
                    'Sohail (26 May 2017) -- End

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'Dim dr_Quali() As DataRow = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    Dim dr_Quali() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullQualificationUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetQualification(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetQualification(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_Quali = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                            Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Return False
                        End If
                            objCommonATLog = Nothing
                        StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Return False
                        End If
                        Else
                            dr_Quali = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                        End If
                    Else
                        dr_Quali = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                    'Sohail (26 May 2017) -- End

                    If dr_Quali.Length > 0 Then
                        For Each dRow As DataRow In dr_Quali

                            'Sohail (26 May 2017) -- Start
                            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                            'StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno )" & vbCrLf
                            'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("qualificationgroupunkid") & ", " & dRow.Item("qualificationunkid") & ", '" & CDate(dRow.Item("transaction_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & dRow.Item("reference_no").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("award_start_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & CDate(dRow.Item("award_end_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', " & dRow.Item("instituteunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', " & dRow.Item("resultunkid") & ", " & dRow.Item("gpacode") & ", '" & dRow.Item("other_qualificationgrp").ToString.Replace("'", "''") & "', '" & dRow.Item("other_qualification").ToString.Replace("'", "''") & "', '" & dRow.Item("other_institute").ToString.Replace("'", "''") & "', '" & dRow.Item("other_resultcode").ToString.Replace("'", "''") & "', '" & dRow.Item("certificateno").ToString.Replace("'", "''") & "' ) " & vbCrLf

                            'objDataOp.ExecNonQuery(StrQ)

                            'If objDataOp.ErrorMessage <> "" Then
                            '    'Sohail (09 Nov 2012) -- Start
                            '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            '    'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '    'Return False
                            '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                            '        Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                            '        GoTo CONTINUE_FOR
                            '    Else
                            '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '        Return False
                            '    End If
                            'End If
                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerQualificationUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverqualificationtranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("qualificationtranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                            'Sohail (26 May 2017) -- End
                        Next
                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        Dim objQuali As New clsApplicantQualification_tran
                        'Sohail (24 Jan 2018) -- Start
                        'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                        'objQuali._ApplicantUnkid = intExistingApplicantUnkId
                        objQuali._ApplicantUnkid = mintNewApplicantUnkID
                        'Sohail (24 Jan 2018) -- End
                        objQuali._DataList = dr_Quali.CopyToDataTable
                        With objQuali
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objQuali.InsertUpdateDelete_QualificationTran() = False Then
                            Call WriteLog("error in applicant qualification")
                            Return False
                        End If
                        'Sohail (26 May 2017) -- End
                    End If

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantqualification_tran", "qualificationtranunkid", 1, 1, , , , mintUserunkid) = False Then
                    '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '    Return False
                    'End If
                    'Sohail (26 May 2017) -- End

                    blnResult = True
                    '**********[ QUAKIFICATION TRAN ]********* END

                    '**********[ REFERENCE TRAN ]********* START
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If intExistingApplicantUnkId > 0 Then
                    '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If

                    '    StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    '    objDataOp.ExecNonQuery(StrQ)

                    '    If objDataOp.ErrorMessage <> "" Then
                    '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '        Return False
                    '    End If
                    'End If
                    'Sohail (26 May 2017) -- End

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'Dim dr_Ref() As DataRow = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    Dim dr_Ref() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullReferenceUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetReference(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetReference(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_Ref = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                            Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Return False
                        End If
                            objCommonATLog = Nothing
                        StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Return False
                        End If
                        Else
                            dr_Ref = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                        End If
                    Else
                        dr_Ref = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                    'Sohail (26 May 2017) -- End

                    If dr_Ref.Length > 0 Then
                        For Each dRow As DataRow In dr_Ref

                            'Sohail (26 May 2017) -- Start
                            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                            'StrQ = "INSERT INTO rcapp_reference_tran(applicantunkid, name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid)" & vbCrLf
                            'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", '" & dRow.Item("name").ToString.Replace("'", "''") & "', '" & dRow.Item("address").ToString.Replace("'", "''") & "', " & dRow.Item("countryunkid") & ", " & dRow.Item("stateunkid") & ", " & dRow.Item("cityunkid") & ", '" & dRow.Item("email").ToString.Replace("'", "''") & "', '" & dRow.Item("gender").ToString.Replace("'", "''") & "', '" & dRow.Item("position").ToString.Replace("'", "''") & "', '" & dRow.Item("telephone_no").ToString.Replace("'", "''") & "', '" & dRow.Item("mobile_no").ToString.Replace("'", "''") & "', " & dRow.Item("relationunkid") & " ) " & vbCrLf

                            'objDataOp.ExecNonQuery(StrQ)

                            'If objDataOp.ErrorMessage <> "" Then
                            '    'Sohail (09 Nov 2012) -- Start
                            '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            '    'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '    'Return False
                            '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                            '        Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                            '        GoTo CONTINUE_FOR
                            '    Else
                            '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '        Return False
                            '    End If
                            'End If
                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerReferenceUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverreferencetranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("referencetranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                            'Sohail (26 May 2017) -- End
                        Next
                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        Dim objRef As New clsrcapp_reference_Tran
                        'Sohail (24 Jan 2018) -- Start
                        'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                        'objRef._Applicantunkid = intExistingApplicantUnkId
                        objRef._Applicantunkid = mintNewApplicantUnkID
                        'Sohail (24 Jan 2018) -- End
                        objRef._dtReferences = dr_Ref.CopyToDataTable
                        With objRef
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        If objRef.InsertUpdateDelete_ReferencesTran() = False Then
                            exForce = New Exception("error in applicant reference")
                            Throw exForce
                        End If
                        'Sohail (26 May 2017) -- End
                    End If

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_reference_tran", "referencetranunkid", 1, 1, , , , mintUserunkid) = False Then
                    '    Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '    Return False
                    'End If
                    'Sohail (26 May 2017) -- End
                    blnResult = True
                    '**********[ REFERENCE TRAN ]********* END

                    '**********[ VACANCY MAPPING ]********* START
                    'Sohail (09 Oct 2018) -- Start
                    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                    'Dim objVacancyMap As clsApplicant_Vacancy_Mapping
                    'Dim intAppVacancyMapId As Integer

                    'Dim dr_Vacancy() As DataRow = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")

                    'If dr_Vacancy.Length > 0 Then
                    '    For Each dRow As DataRow In dr_Vacancy
                    '        objVacancyMap = New clsApplicant_Vacancy_Mapping

                    '        intAppVacancyMapId = -1

                    '        objVacancyMap.isExist(mintNewApplicantUnkID, CInt(dRow.Item("vacancyunkid")), , intAppVacancyMapId)

                    '        If intAppVacancyMapId > 0 Then 'Update

                    '            StrQ = "UPDATE  rcapp_vacancy_mapping " & _
                    '                    "SET     applicantunkid = " & mintNewApplicantUnkID & " " & _
                    '                          ", vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & " " & _
                    '                          ", userunkid = " & mintUserunkid & " " & _
                    '                          ", isactive = " & CInt(dRow.Item("isactive")) & " " & _
                    '                    "WHERE   appvacancytranunkid = " & intAppVacancyMapId & " "

                    '            objDataOp.ExecNonQuery(StrQ)

                    '            If objDataOp.ErrorMessage <> "" Then
                    '                If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                    '                    Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                    '                    GoTo CONTINUE_FOR
                    '                Else
                    '                    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '                    Return False
                    '                End If
                    '            End If

                    '            If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 2, 2, , mintUserunkid) = False Then
                    '                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '                Throw exForce
                    '            End If

                    '        Else 'Insert

                    '            'Sohail (27 Apr 2017) -- Start
                    '            'TANAPA Issue - 66.1 - applicants were not coming in shortlisting due to isimport = 1.
                    '            'StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport)" & vbCrLf
                    '            'StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", 1, " & mintUserunkid & ", GETDATE(), 1 ) ; SELECT @@identity"
                    '            'Nilay (13 Apr 2017) -- Start
                    '            'StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport)" & vbCrLf
                    '            'StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", " & CInt(dRow.Item("isactive")) & ", 0, NULL, 0 ) ; SELECT @@identity"
                    '            StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport, earliest_possible_startdate, comments, vacancy_found_out_from)" & vbCrLf
                    '            StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", " & CInt(dRow.Item("isactive")) & ", 0, NULL, 0, " & If(IsDBNull(dRow.Item("earliest_possible_startdate")) = True, "NULL", " '" & CDate(dRow.Item("earliest_possible_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", '" & dRow.Item("comments").ToString.Replace("'", "''") & "', '" & dRow.Item("vacancy_found_out_from").ToString.Replace("'", "''") & "' ) ; SELECT @@identity"
                    '            'Nilay (13 Apr 2017) -- End
                    '            'Sohail (27 Apr 2017) -- End
                    '            'Sohail (05 Dec 2016) - [importuserunkid, importdatetime, isimport]

                    '            ds = objDataOp.ExecQuery(StrQ, "VM")

                    '            If objDataOp.ErrorMessage <> "" Then
                    '                'Sohail (09 Nov 2012) -- Start
                    '                'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                    '                'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '                'Return False
                    '                If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                    '                    Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                    '                    GoTo CONTINUE_FOR
                    '                Else
                    '                    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '                    Return False
                    '                End If
                    '                'Sohail (09 Nov 2012) -- End
                    '            End If

                    '            intAppVacancyMapId = ds.Tables(0).Rows(0).Item(0)

                    '            If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 1, 1, , mintUserunkid) = False Then
                    '                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '                Throw exForce
                    '            End If
                    '        End If
                    '    Next
                    'End If
                    Dim dr_AppVac() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullAppVacancyUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetVacancy(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetVacancy(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_AppVac = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")
Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_vacancy_mapping", "appvacancytranunkid", 3, 3) = False Then
                                Call WriteLog("> Error: " & objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                    Return False
                                End If

                            StrQ = "DELETE FROM rcapp_vacancy_mapping WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                            objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                        Return False
                                    End If
objCommonATLog = Nothing
                        Else
                            dr_AppVac = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")
                        End If
                    Else
                        dr_AppVac = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")
                                End If

                    If dr_AppVac.Length > 0 Then
                        For Each dRow As DataRow In dr_AppVac

                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerAppVacancyUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverappvacancytranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("appvacancytranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                        Next
                        Dim objVacancyMap As New clsApplicant_Vacancy_Mapping
                        objVacancyMap._Applicantunkid = mintNewApplicantUnkID
                        objVacancyMap._DataList = dr_AppVac.CopyToDataTable
                        'Sohail (12 Jan 2019) -- Start
                        'AT Testing - 76.1 - Fixing AT Issues.
                        With objVacancyMap
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        'Sohail (12 Jan 2019) -- End
                        If objVacancyMap.InsertUpdateDelete_AppVacancyMapping() = False Then
                            exForce = New Exception("error in applicant vacancy mapping")
                            Throw exForce
                        End If
                    End If
                    'Sohail (09 Oct 2018) -- End

                    blnResult = True
                    '**********[ VACANCY MAPPING ]********* END

                    '**********[ IMAGES TRAN ]********* START
                    If intExistingApplicantUnkId > 0 Then

                        StrQ = "DELETE FROM hr_images_tran WHERE employeeunkid  = " & intExistingApplicantUnkId & " and referenceid = " & enImg_Email_RefId.Applicant_Module & " AND isapplicant = 1 "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Return False
                        End If
                    End If

                    Dim dr_Images() As DataRow = dsImages.Tables(0).Select("employeeunkid = " & mintCurrApplicantUnkID & " ")

                    If dr_Images.Length > 0 Then
                        For Each dRow As DataRow In dr_Images

                            StrQ = "INSERT INTO hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant) " & vbCrLf
                            StrQ &= "VALUES (" & mintNewApplicantUnkID & ", '" & dRow.Item("imagename").ToString.Replace("'", "''") & "'," & mintNewApplicantUnkID & ", " & dRow.Item("referenceid") & ", " & dRow.Item("isapplicant") & " ) " & vbCrLf

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                'Sohail (09 Nov 2012) -- Start
                                'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                                'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                'Return False
                                If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                                    Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                                    GoTo CONTINUE_FOR
                                Else
                                    Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Return False
                                End If
                                'Sohail (09 Nov 2012) -- End
                            End If
                            '**********[ IMAGES TRAN ]********* END


                            '*** [Download Images / Attachements] ***
                            objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dRow.Item("imagename")))
                            objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

                            If WebFileExist(objWebRequest) = True Then
                                objWebResponse = objWebRequest.GetResponse

                                lngMaxBytes = CLng(objWebResponse.ContentLength)
                                reader = objWebResponse.GetResponseStream

                                writer = IO.File.Create(imgLocalPath & CStr(dRow.Item("imagename")))

                                bytesRead = 0
                                lngTotalBytes = 0
                                While True
                                    bytesRead = reader.Read(buffer, 0, buffer.Length)
                                    If bytesRead <= 0 Then Exit While

                                    writer.Write(buffer, 0, bytesRead)
                                    lngTotalBytes += bytesRead
                                End While
                                reader.Close()
                                writer.Close()


                                'Dim objScan As New clsScan_Attach_Documents
                                'Dim dt As DataTable = objScan._Datatable
                                'Dim rw As DataRow = dt.NewRow
                                'rw.Item("scanattachtranunkid") = -1
                                'rw.Item("documentunkid") = dRow.Item("documentunkid")
                                'rw.Item("employeeunkid") = mintNewApplicantUnkID
                                'rw.Item("filename") = dRow.Item("filename")
                                'rw.Item("scanattachrefid") = dRow.Item("attachrefid")
                                'rw.Item("modulerefid") = dRow.Item("modulerefid")
                                'rw.Item("userunkid") = User._Object._Userunkid
                                'rw.Item("transactionunkid") = -1
                                'rw.Item("attached_date") = dRow.Item("attached_date")
                                'rw.Item("destfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("filename"))
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), dRow.Item("filename").ToString, CInt(dRow.Item("employeeunkid"))) = False Then
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("imagename"))
                                '    rw.Item("AUD") = "A"
                                'Else
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("filename"))
                                '    rw.Item("AUD") = "U"
                                'End If

                                'dt.Rows.Add(rw)

                                'objScan._Datatable = dt
                                'If objScan.InsertUpdateDelete_Documents() = False Then
                                '    Throw New Exception(objScan._Message)
                                'Return False
                                'End If

                                'IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("imagename")))
                            End If

                        Next
                    End If
                    '**********[ IMAGES TRAN ]********* END

                    '**********[ ATTACH FILE TRAN ]********* START
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If intExistingApplicantUnkId > 0 Then

                    '    StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid  = " & intExistingApplicantUnkId & " and modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " "

                    '    objDataOp.ExecNonQuery(StrQ)

                    '    If objDataOp.ErrorMessage <> "" Then
                    '        'Sohail (09 Nov 2012) -- Start
                    '        'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                    '        'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '        'Return False
                    '        If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                    '            Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                    '            GoTo CONTINUE_FOR
                    '        Else
                    '            Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    '            Return False
                    '        End If
                    '        'Sohail (09 Nov 2012) -- End
                    '    End If
                    'End If
                    'Sohail (26 May 2017) -- End

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'Dim dr_Attach() As DataRow = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    Dim dr_Attach() As DataRow
                    If intExistingApplicantUnkId > 0 Then
                        If .isExistServerNullAttachmentUnkId(intExistingApplicantUnkId) = True Then
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'Dim dsTmp As DataSet = WebRef.GetAttachments(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                            Dim dsTmp As DataSet = WebRef.GetAttachments(strErrorMessage, False, mintCurrApplicantUnkID)
                            'Sohail (16 Nov 2018) -- End
                            dr_Attach = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                            Dim objCommonATLog As New clsCommonATLog
                            If objCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcattachfiletran", "attachfiletranunkid", 3, 3, , " AND modulerefid = " & enImg_Email_RefId.Applicant_Module & " attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " ") = False Then
                                Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Return False
                            End If
                            objCommonATLog = Nothing
                        StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid  = " & intExistingApplicantUnkId & " and modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                                Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                                GoTo CONTINUE_FOR
                            Else
                                Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Return False
                            End If
                        End If
                        Else
                            dr_Attach = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                        End If
                    Else
                        dr_Attach = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                    'Sohail (26 May 2017) -- End

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                'Sohail (05 Dec 2016) -- End
                Dim strError As String = ""
                    'SHANI (16 JUL 2015) -- End 

                    If dr_Attach.Length > 0 Then
                        For Each dRow As DataRow In dr_Attach

                            'Sohail (26 May 2017) -- Start
                            'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                            'StrQ = "INSERT INTO rcattachfiletran(applicantunkid, documentunkid, modulerefid, attachrefid, filepath, filename, fileuniquename, attached_date) " & vbCrLf
                            'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("documentunkid") & ", " & dRow.Item("modulerefid") & " , " & dRow.Item("attachrefid") & ", '" & dRow.Item("filepath").ToString.Replace("'", "''") & "', '" & dRow.Item("filename").ToString.Replace("'", "''") & "', '" & dRow.Item("fileuniquename").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("attached_date")).ToString("yyyy-MM-dd hh:mm:ss") & "' ) " & vbCrLf

                            'objDataOp.ExecNonQuery(StrQ)

                            'If objDataOp.ErrorMessage <> "" Then
                            '    'Sohail (09 Nov 2012) -- Start
                            '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            '    'Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '    'Return False
                            '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                            '        Call WriteLog("> Error: " & ._Firstname & " " & ._Surname & ";" & ._Email & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString)
                            '        GoTo CONTINUE_FOR
                            '    Else
                            '        Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            '        Return False
                            '    End If
                            '    'Sohail (09 Nov 2012) -- End
                            'End If
                            dRow.Item("AUD") = "A"
                            If intExistingApplicantUnkId > 0 Then
                                Dim intUnkId As Integer = .isExistServerAttachUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverattachfiletranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("attachfiletranunkid") = intUnkId
                                    dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                    dRow.Item("AUD") = "U"
                                End If
                            End If
                            dRow.AcceptChanges()
                            'Sohail (26 May 2017) -- End

                            '*** [Download Images / Attachements] ***

                            'SHANI (16 JUL 2015) -- Start
                            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                            'Upload Image folder to access those attachemts from Server and 
                            'all client machines if ArutiSelfService is installed otherwise save then on Document path

                            'If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                            '    objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & "UploadQualiCerti/" & CStr(dRow.Item("fileuniquename")))
                            'Else
                            '    objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dRow.Item("fileuniquename")))
                            'End If

                            'Sohail (22 Apr 2015) -- End
                            'objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

                            'If WebFileExist(objWebRequest) = True Then
                            '    objWebResponse = objWebRequest.GetResponse

                            '    lngMaxBytes = CLng(objWebResponse.ContentLength)
                            '    reader = objWebResponse.GetResponseStream
                            '    If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                            '        If IO.Directory.Exists(docLocalPath & strFolderName) = False Then
                            '            IO.Directory.CreateDirectory(docLocalPath & strFolderName)
                            '        End If
                            '        writer = IO.File.Create(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                            '    Else
                            '        writer = IO.File.Create(imgLocalPath & CStr(dRow.Item("fileuniquename")))
                            '    End If

                            '    bytesRead = 0
                            '    lngTotalBytes = 0
                            '    While True
                            '        bytesRead = reader.Read(buffer, 0, buffer.Length)
                            '        If bytesRead <= 0 Then Exit While

                            '        writer.Write(buffer, 0, bytesRead)
                            '        lngTotalBytes += bytesRead
                            '    End While
                            '    reader.Close()
                            '    writer.Close()
                            Dim byteImg As Byte() = Nothing
                            Dim strErrorMsg As String = ""

                            'Sohail (04 Jul 2019) -- Start
                            'PACT Enhancement - Support Issue Id # 3954 - 76.1 - Deleted attachments are getting downloaded when Import Data is done.
                            If CBool(dRow.Item("isvoid")) = False Then
                                'Sohail (04 Jul 2019) -- End

                            'Sohail (05 Dec 2016) -- Start
                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                            'If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                            '    byteImg = WebRef.DownloadFile(UserCred, "UploadQualiCerti", CStr(dRow.Item("fileuniquename")), strErrorMsg)
                            'Else

                            '    byteImg = WebRef.DownloadFile(UserCred, "", CStr(dRow.Item("fileuniquename")), strErrorMsg)
                            'End If

                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'byteImg = WebRef.DownloadFile(UserCred, CStr(dRow.Item("filepath")), strErrorMsg)
                            byteImg = WebRef.DownloadFile(CStr(dRow.Item("filepath")), strErrorMsg)
                            'Sohail (16 Nov 2018) -- End
                            'Sohail (05 Dec 2016) -- End
                            'Sohail (05 Dec 2016) -- Start
                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                            'If byteImg IsNot Nothing Then
                            If byteImg IsNot Nothing AndAlso strErrorMsg.Trim = "" Then
                                'Sohail (05 Dec 2016) -- End
                                'SHANI (16 JUL 2015) -- End 

                                    'Sohail (04 Jul 2019) -- Start
                                    'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
                                    If dicFolderName.ContainsKey(CInt(dRow.Item("attachrefid"))) = True Then
                                        strFolderName = dicFolderName.Item(CInt(dRow.Item("attachrefid")))
                                    Else
                                        strFolderName = "Qualifications"
                                    End If
                                    'Sohail (04 Jul 2019) -- End

                                'Sohail (22 Apr 2015) -- Start
                                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                                Dim objScan As New clsScan_Attach_Documents
                                Dim dt As DataTable = objScan._Datatable
                                Dim rw As DataRow = dt.NewRow
                                rw.Item("scanattachtranunkid") = -1
                                rw.Item("documentunkid") = dRow.Item("documentunkid")
                                rw.Item("employeeunkid") = mintNewApplicantUnkID
                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'rw.Item("filename") = clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename")
                                If drRow.Item("referenceno").ToString.Trim <> "" Then
                                rw.Item("filename") = clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename")
                                Else
                                    rw.Item("filename") = drRow.Item("UserId").ToString & "_" & dRow.Item("filename")
                                End If
                                'Sohail (05 Dec 2016) -- End
                                rw.Item("scanattachrefid") = dRow.Item("attachrefid")
                                rw.Item("modulerefid") = dRow.Item("modulerefid")
                                rw.Item("userunkid") = mintUserunkid
                                rw.Item("transactionunkid") = -1
                                rw.Item("attached_date") = dRow.Item("attached_date")
                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'rw.Item("destfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                If drRow.Item("referenceno").ToString.Trim <> "" Then
                                rw.Item("destfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                Else
                                    rw.Item("destfilepath") = docLocalPath & strFolderName & drRow.Item("UserId").ToString & "_" & CStr(dRow.Item("filename"))
                                End If
                                'Sohail (05 Dec 2016) -- End

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path

                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename"))
                                '    rw.Item("AUD") = "A"
                                'Else
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                '    rw.Item("AUD") = "U"
                                'End If
                                'SHANI (16 JUL 2015) -- End 
                                rw.Item("fileuniquename") = CStr(dRow.Item("fileuniquename"))
                                rw.Item("filesize") = dRow.Item("file_size")
                                rw.Item("orgfilepath") = ""
                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                '    rw.Item("AUD") = "A"
                                'Else
                                '    rw.Item("AUD") = "U"
                                '    Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString)
                                '    If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                '        rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                '    End If
                                'End If
                                If drRow.Item("referenceno").ToString.Trim <> "" Then
                                If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                    rw.Item("AUD") = "A"
                                Else
                                    rw.Item("AUD") = "U"
                                    Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString)
                                    If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                        rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                    End If
                                End If
                                Else
                                    If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                        rw.Item("AUD") = "A"
                                    Else
                                        rw.Item("AUD") = "U"
                                        Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString)
                                        If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                            rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                        End If
                                    End If
                                End If
                                'Sohail (05 Dec 2016) -- End
                                If blnIsIISInstalled Then

                                    'Shani(24-Aug-2015) -- Start
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'If clsFileUploadDownload.ByteImageUpload(byteImg, strFolderName, CStr(dRow.Item("fileuniquename")), strErrorMsg) = False Then
                                    If clsFileUploadDownload.ByteImageUpload(byteImg, strFolderName, CStr(dRow.Item("fileuniquename")), strErrorMsg, strArutiSelfServiceURL) = False Then
                                        'Shani(24-Aug-2015) -- End

                                        Call WriteLog("Error message For upload image : " & strErrorMsg)
                                        Exit Function
                                    Else
                                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                            strPath += "/"
                                        End If
                                        strPath += "uploadimage/" & strFolderName & "/" + CStr(dRow.Item("fileuniquename"))
                                        rw.Item("filepath") = strPath
                                    End If
                                Else
                                    If System.IO.Directory.Exists(docLocalPath) Then
                                        If System.IO.Directory.Exists(docLocalPath & strFolderName) = False Then
                                            System.IO.Directory.CreateDirectory(docLocalPath & strFolderName)
                                End If

                                        Dim mstrfilepath As String = docLocalPath & strFolderName & "\" & CStr(dRow.Item("fileuniquename"))
                                        Dim ms As New System.IO.MemoryStream(byteImg)
                                        Dim fs As New System.IO.FileStream(mstrfilepath, System.IO.FileMode.Create)
                                        ms.WriteTo(fs)
                                        rw.Item("filepath") = mstrfilepath
                                    Else
                                        Call WriteLog("Error message For upload image configuration path not exitst: ")
                                        Exit Function
                                    End If
                                End If
                                dt.Rows.Add(rw)

                                objScan._Datatable = dt
                                With objScan
                                    ._FormName = mstrModuleName
                                    ._LoginEmployeeunkid = 0
                                    ._ClientIP = getIP()
                                    ._HostName = getHostName()
                                    ._FromWeb = False
                                    ._AuditUserId = User._Object._Userunkid
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                End With

                                If objScan.InsertUpdateDelete_Documents(objDataOp) = False Then
                                    Throw New Exception(objScan._Message)
                                    Return False
                                End If

                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - error file not found from client machine as path not found.
                                'IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                                If IO.File.Exists(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename"))) = True Then
                                IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                                End If
                                'Sohail (26 May 2017) -- End
                                'Sohail (22 Apr 2015) -- End
                            End If

                                'Sohail (04 Jul 2019) -- Start
                                'PACT Enhancement - Support Issue Id # 3954 - 76.1 - Deleted attachments are getting downloaded when Import Data is done.
                            End If
                            'Sohail (04 Jul 2019) -- End

                        Next
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objAttach As New clsAppAttachFileTran
                        'Sohail (24 Jan 2018) -- Start
                        'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                        'objAttach._ApplicantUnkid = intExistingApplicantUnkId
                        objAttach._ApplicantUnkid = mintNewApplicantUnkID
                        'Sohail (24 Jan 2018) -- End
                    objAttach._DataList = dr_Attach.CopyToDataTable
                        With objAttach
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                    If objAttach.InsertUpdateDelete_AttachFileTran() = False Then
                        exForce = New Exception("error in applicant attach file")
                        Throw exForce
                    End If
                    'Sohail (26 May 2017) -- End
                    End If

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcattachfiletran", "attachfiletranunkid", 1, 1, , , , mintUserunkid) = False Then
                    '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                    '    Throw exForce
                    'End If
                'Sohail (26 May 2017) -- End
                    blnResult = True
                    '**********[ ATTACH FILE TRAN ]********* END






                    '**********[ UPDATING SYNC DATE TIME ]********* START
                    strErrorMessage = ""
                    'Sohail (16 Nov 2018) -- Start
                    'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                    'WebRef.UpdateSyncDateTimeForImport(UserCred, strErrorMessage, True, mintCurrApplicantUnkID.ToString)
                    WebRef.UpdateSyncDateTimeForImport(strErrorMessage, True, mintCurrApplicantUnkID.ToString)
                    'Sohail (16 Nov 2018) -- End

                    If strErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                        Throw exForce
                        Exit Try
                    End If
                    '**********[ UPDATING SYNC DATE TIME ]********* END




CONTINUE_FOR:

                End With

                intCount = intCount + 1
                WriteLog("[ " & intCount.ToString & " / " & GintTotalApplicantToImport.ToString & " ] Applicatnts Imported.")
            Next



            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            If objDataOp.ErrorMessage <> "" Then
                Call WriteLog("> Error: " & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Return False
            End If

            '*** Insert Audit Log 
            Call InsertATLog(mstrApplicantIDs, False, StartTime)

            Return True


        Catch ex As Exception
            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            If objDataOp.ErrorMessage <> "" Then
                Call WriteLog(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            End If
            '*** Insert Audit Log 
            Call InsertATLog(mstrApplicantIDs, True, StartTime)
            Threading.Thread.Sleep(5000)
        End Try

    End Function
    'Sohail (18 May 2015) -- End

    Private Sub GetApplicantImportApplicantStatus()
        Dim objDataOperation As New clsDataOperation
        Dim ds As DataSet
        Dim exForce As Exception
        Dim arr() As String
        Dim StrQ As String = String.Empty
        Try


            '*** Get Applicant Import Package Status :   Motherboard Serial Number|Machine Name|True/False
            StrQ = "SELECT * FROM " & Company._Object._ConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " "
            ds = objDataOperation.ExecQuery(StrQ, "Status")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ds.Tables("Status").Rows.Count > 0 Then
                '** Motherboard Serial Number|Machine Name|0/1
                arr = Split(CStr(ds.Tables("Status").Rows(0).Item("key_value")), "|")
                mstrMBoardSrNo = arr(0)
                mstrMachineName = arr(1)
                mintImportStatus = CInt(arr(2))
            Else
                mstrMBoardSrNo = GetMotherBoardSrNo()
                mstrMachineName = My.Computer.Name
                mintImportStatus = 1

                '*** Insert status if not exist: Motherboard Serial Number|Machine Name|0/1
                StrQ = "IF NOT EXISTS (SELECT * FROM " & Company._Object._ConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & ") " & _
                            "INSERT INTO " & Company._Object._ConfigDatabaseName & "..cfconfiguration ( key_name, key_value, companyunkid ) VALUES  ('ApplicantImportPackageStatus','" & mstrMBoardSrNo & "|" & mstrMachineName & "|" & mintImportStatus & "', " & mintCompID & " ) "

                objDataOperation.ExecNonQuery(StrQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantImportApplicantStatus", mstrModuleName)
        End Try
    End Sub

    Private Function InsertATLog(ByVal strApplicantIDs As String, ByVal blnErrorOccured As Boolean, ByVal StartTime As DateTime) As Boolean
        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As DataSet
        Dim StrQ As String


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicantids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strApplicantIDs.ToString)
            objDataOperation.AddParameter("@motherboardsrno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMBoardSrNo.ToString)
            objDataOperation.AddParameter("@issucceeded", SqlDbType.Bit, eZeeDataType.BIT_SIZE, Not blnErrorOccured)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Aruti.Data.User._Object._Userunkid)
            objDataOperation.AddParameter("@auditstarttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, StartTime)
            If blnErrorOccured = False Then
                objDataOperation.AddParameter("@auditendtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            Else
                objDataOperation.AddParameter("@auditendtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachineName.ToString)




            StrQ = "INSERT INTO atapplicant_import ( " & _
                          "  applicantids " & _
                          ", motherboardsrno " & _
                          ", issucceeded " & _
                          ", audituserunkid " & _
                          ", auditstarttime " & _
                          ", auditendtime " & _
                          ", ip " & _
                          ", machine_name" & _
                        ", form_name " & _
                        ", isweb " & _
                    ") VALUES (" & _
                          "  @applicantids " & _
                          ", @motherboardsrno " & _
                          ", @issucceeded " & _
                          ", @audituserunkid " & _
                          ", @auditstarttime " & _
                          ", @auditendtime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                        ", @form_name " & _
                        ", @isweb " & _
                    "); SELECT @@identity"


            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, "Auto Shedule Import Applicant") 'mstrForm_Name
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

            '
            '
            '
            '
            '

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertATLog", mstrModuleName)
        End Try
    End Function

    Public Function WebFileExist(ByRef objRequest As System.Net.WebRequest) As Boolean
        Dim objResponse As System.Net.HttpWebResponse
        Try
            objResponse = CType(objRequest.GetResponse, Net.HttpWebResponse)

            Return True
        Catch ex As System.Net.WebException
            Dim r As System.Net.HttpWebResponse = CType(ex.Response, Net.HttpWebResponse)
            If r.StatusCode = Net.HttpStatusCode.NotFound Then
                Return False
            Else
                Call WriteLog(ex.ToString)
            End If
        Finally
            objResponse = Nothing
        End Try

    End Function
#End Region

End Module

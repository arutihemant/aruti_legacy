
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO
Imports System.Text


Public Class clsApplicantQualification
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsApplicantQualification"
    Private mstrReportId As String = enArutiReport.ApplicantQualification
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private Variables "

    Private mstrOrderByQuery As String = String.Empty
    Private mintApplicantId As Integer = -1
    Private mstrApplicantName As String = String.Empty
    Private mintVacancyTypeId As Integer = -1
    Private mstrVacancyTypeName As String = String.Empty
    Private mintVacancyId As Integer = -1
    Private mstrVacancyName As String = String.Empty
    Private mintApplicantTypeId As Integer = -1
    Private mstrApplicantTypeName As String = String.Empty
    Private mstrReferenceNo As String = String.Empty
    Dim StrFinalPath As String = String.Empty


    'Anjan (29-Apr-2012) -- Start
    'Enhancement : TRA Changes
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    'Anjna (29-Apr-2012) -- End
#End Region

#Region "Properties"

    Public WriteOnly Property _ApplicantID() As Integer
        Set(ByVal value As Integer)
            mintApplicantId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantName() As String
        Set(ByVal value As String)
            mstrApplicantName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeID() As Integer
        Set(ByVal value As Integer)
            mintVacancyTypeId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyTypeName() As String
        Set(ByVal value As String)
            mstrVacancyTypeName = value
        End Set
    End Property

    Public WriteOnly Property _VacancyID() As Integer
        Set(ByVal value As Integer)
            mintVacancyId = value
        End Set
    End Property

    Public WriteOnly Property _VacancyName() As String
        Set(ByVal value As String)
            mstrVacancyName = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantTypeId() As Integer
        Set(ByVal value As Integer)
            mintApplicantTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ApplicantTypeName() As String
        Set(ByVal value As String)
            mstrApplicantTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ReferenceNo() As String
        Set(ByVal value As String)
            mstrReferenceNo = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    'Anjan (29 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'Anjan (29 Apr 2012)-End 



#End Region

#Region "Public Function & Procedures "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub SetDefaultValue()
        Try

            mintApplicantId = 0
            mstrApplicantName = ""
            mintVacancyTypeId = 0
            mstrVacancyTypeName = ""
            mintVacancyId = 0
            mstrVacancyName = ""
            mintApplicantTypeId = 0
            mstrApplicantTypeName = ""
            mstrReferenceNo = ""
            mstrOrderByQuery = ""

            'Anjan (29-Apr-2012) -- Start
            'Enhancement : TRA Changes
            mintReportTypeId = 0
            mstrReportTypeName = ""
            'Anjans (29-Apr-2012) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintApplicantId > 0 Then
                Me._FilterQuery &= " AND applicant.applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Applicant : ") & " " & mstrApplicantName & "    "
            End If

            If mintVacancyTypeId > 0 Then
                'Sohail (18 Apr 2020) -- Start
                'NMB Enhancement # : Need a filter type on vacancy type called Internal and External.
                'If mintVacancyTypeId = 1 Then
                '    Me._FilterQuery &= " AND applicant.isexternalvacancy = 1 "
                'ElseIf mintVacancyTypeId = 2 Then
                '    Me._FilterQuery &= " AND applicant.isexternalvacancy = 0 "
                'End If
                If mintVacancyTypeId = enVacancyType.EXTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 1 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_VACANCY Then
                    Me._FilterQuery &= " AND (applicant.isexternalvacancy = 0 AND ISNULL(applicant.isbothintext, 0) = 0) "
                ElseIf mintVacancyTypeId = enVacancyType.INTERNAL_AND_EXTERNAL Then
                    Me._FilterQuery &= " AND ISNULL(applicant.isbothintext, 0) = 1 "
                End If
                'Sohail (18 Apr 2020) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Vacancy Type : ") & "   " & mstrVacancyTypeName & "    "
            End If

            If mintVacancyId > 0 Then
                Me._FilterQuery &= " AND applicant.vacancyunkid = @vacancyunkid "
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyId)

                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Me._FilterTitle &= Language.getMessage(mstrModulename, 3, "Vacancy : ") & "   " & mstrVacancyName & "    "
                Select Case mintReportTypeId
                    Case 0
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Vacancy : ") & "   " & mstrVacancyName & "    "
                End Select
                'S.SANDEEP [ 04 MAY 2012 ] -- END
            End If

            If mintApplicantTypeId > 0 Then
                If mintApplicantTypeId = 1 Then
                    'Sohail (20 Oct 2020) -- Start
                    'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
                    'Me._FilterQuery &= " AND applicant.employeecode = '' "
                    Me._FilterQuery &= " AND applicant.employeeunkid <= 0 "
                    'Sohail (20 Oct 2020) -- End
                ElseIf mintApplicantTypeId = 2 Then
                    'Sohail (20 Oct 2020) -- Start
                    'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
                    'Me._FilterQuery &= " AND applicant.employeecode <> '' "
                    Me._FilterQuery &= " AND applicant.employeeunkid > 0 "
                    'Sohail (20 Oct 2020) -- End
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Applicant Type : ") & "   " & mstrApplicantTypeName & "    "
            End If


            If mstrReferenceNo.Trim.Length > 0 Then
                Me._FilterQuery &= " AND applicant.referenceno = @referenceno "
                objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceNo)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Reference No : ") & "   " & mstrReferenceNo & "    "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, " Order By : ") & "   " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
    Private Function Export_to_Excel(ByVal flFileName As String, _
                                     ByVal SavePath As String, _
                                     ByVal objDataReader As System.Data.DataTable, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        Dim objCompany As New clsCompany_Master
        'Shani(24-Aug-2015) -- End

        Try
            'HEADER PART


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = xUserUnkid
            objCompany._Companyunkid = xCompanyUnkid
            'Shani(24-Aug-2015) -- End

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><B><FONT SIZE=2> " & vbCrLf)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(objUser._Username & vbCrLf)
            'Shani(24-Aug-2015) -- End

            strBuilder.Append(" </FONT></B></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & objCompany._Name & " </B></FONT> " & vbCrLf)
            'Shani(24-Aug-2015) -- End

            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 7, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2><B> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </B></FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & mstrReportTypeName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append("<BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)


            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).ColumnName.ToString() = "isexternalvacancy" Or objDataReader.Columns(j).ColumnName.ToString() = "applicantunkid" Or objDataReader.Columns(j).ColumnName.ToString() = "qualificationunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "genderid" Or objDataReader.Columns(j).ColumnName.ToString() = "vacancyunkid" Or _
                objDataReader.Columns(j).ColumnName.ToString() = "award_start_date" Or objDataReader.Columns(j).ColumnName.ToString() = "award_end_date" Then Continue For

                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            Dim mintApplicantunkid As Integer = 0
            For i As Integer = 0 To objDataReader.Rows.Count - 1

                If mintApplicantunkid <> CInt(objDataReader.Rows(i)("applicantunkid")) Then
                    mintApplicantunkid = CInt(objDataReader.Rows(i)("applicantunkid"))
                Else
                    Continue For
                End If

                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1

                    If objDataReader.Columns(k).ColumnName.ToString() = "isexternalvacancy" Or objDataReader.Columns(k).ColumnName.ToString() = "applicantunkid" Or objDataReader.Columns(k).ColumnName.ToString() = "qualificationunkid" Or _
                        objDataReader.Columns(k).ColumnName.ToString() = "genderid" Or objDataReader.Columns(k).ColumnName.ToString() = "vacancyunkid" Or _
                        objDataReader.Columns(k).ColumnName.ToString() = "award_start_date" Or objDataReader.Columns(k).ColumnName.ToString() = "award_end_date" Then Continue For

                    If objDataReader.Columns(k).DataType.ToString() = "System.DateTime" Then
                        strBuilder.Append("<TD ALIGN = CENTER VALIGN=TOP BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k).ToString()) & "</FONT></TD>" & vbCrLf)
                    Else
                        If objDataReader.Columns(k).ColumnName.ToString() = "Reference No" Then
                            strBuilder.Append("<TD ALIGN = CENTER VALIGN=TOP BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & "&nbsp;" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD ALIGN = CENTER VALIGN=TOP BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                        End If
                    End If



                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)


            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            Else
                If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                    SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
                End If
            End If

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Public Sub Generate_DetailReport()
    Public Sub Generate_DetailReport(ByVal xUserUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xExportReportPath As String, _
                                     ByVal xOpenReportAfterExport As Boolean)
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            StrQ = "SELECT DISTINCT " & _
                        "applicantunkid " & _
                        ",isexternalvacancy " & _
                        ",isbothintext " & _
                        ",Vacancy " & _
                        ",odate as 'Opening Date' " & _
                        ",cdate as 'Closing Date' " & _
                        ",applicant_code as 'Applicant Code' " & _
                        ",Firstname " & _
                        ",Surname " & _
                        ",applicantname as 'Applicant Name' " & _
                        ",gender_name as 'Gender' " & _
                        ",birth_date as 'Birth Date' " & _
                        ",present_mobileno as 'Present Mobile No' " & _
                        ",Email " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                "+ CAST(cfcommon_master.name AS VARCHAR(MAX)) " & _
                       "FROM     rcapplicantqualification_tran " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid " & _
                                                             "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                       "WHERE    rcapplicantqualification_tran.applicantunkid = applicant.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                       "ORDER BY cfcommon_master.masterunkid " & _
                     "FOR " & _
                       "XML PATH('') " & _
                     "), 1, 1, ''), '') AS 'Qualification Group' " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(hrqualification_master.qualificationname AS VARCHAR(MAX)) " & _
                                       "FROM     rcapplicantqualification_tran " & _
                                                "LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                       "WHERE    rcapplicantqualification_tran.applicantunkid = applicant.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                                       "ORDER BY hrqualification_master.qualificationunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS Qualification " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(rcapplicantqualification_tran.gpacode AS VARCHAR(MAX)) " & _
                                       "FROM     rcapplicantqualification_tran " & _
                                       "WHERE    rcapplicantqualification_tran.applicantunkid = applicant.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS 'GPA' " & _
                        ",gender as genderid " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(cfcommon_master.name AS VARCHAR(MAX)) " & _
                                       "FROM     rcapplicantskill_tran " & _
                                                "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantskill_tran.skillcategoryunkid " & _
                                                                        "AND dbo.cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " " & _
                                       "WHERE    rcapplicantskill_tran.applicantunkid = applicant.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                                       "ORDER BY cfcommon_master.masterunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS 'Skill Category'  " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(hrskill_master.skillname AS VARCHAR(MAX)) " & _
                                       "FROM     rcapplicantskill_tran " & _
                                                "JOIN hrskill_master ON rcapplicantskill_tran.skillunkid = hrskill_master.skillunkid " & _
                                       "WHERE    rcapplicantskill_tran.applicantunkid = applicant.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                                       "ORDER BY hrskill_master.skillunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS Skill " & _
                        ",employeecode as 'Employee Code' " & _
                        ",referenceno as 'Reference No' " & _
                        ",award_start_date " & _
                        ",award_end_date " & _
                        ", applicant.employeeunkid " & _
                "FROM    ( SELECT    rcapplicant_master.applicantunkid , " & _
                                    "applicant_code , " & _
                                    "ISNULL(firstname, '') firstname , " & _
                                    "ISNULL(surname, '') surname , " & _
                                    "ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname , " & _
                                    "CASE WHEN gender = 1 THEN @Male " & _
                                         "WHEN gender = 2 THEN @Female " & _
                                    "END gender_name , " & _
                                    "gender , " & _
                                    "birth_date , " & _
                                    "present_mobileno , " & _
                                    "email , " & _
                                    "ISNULL(employeecode, '') employeecode , " & _
                                    "ISNULL(referenceno, '') referenceno , " & _
                                    "ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid , " & _
                                    "ISNULL(hrqualification_master.qualificationunkid, 0) qualificationunkid , " & _
                                    "ISNULL(Vac.vacancyunkid, 0) vacancyunkid , " & _
                                    "ISNULL(Vac.isexternalvacancy,0) isexternalvacancy, " & _
                                    "ISNULL(Vac.isbothintext,0) isbothintext, " & _
                                    "ISNULL(Vac.vacancy,'')vacancy, " & _
                                    "ISNULL(Vac.ODate,'')ODate, " & _
                                    "ISNULL(Vac.CDate,'')CDate, " & _
                                    "ISNULL(cfcommon_master.qlevel, 0) qlevel , " & _
                                    "ISNULL(hrresult_master.resultunkid, 0) resultunkid , " & _
                                    "ISNULL(hrresult_master.result_level, 0) result_level , " & _
                                    "ISNULL(YEAR(GETDATE()) " & _
                                           "- YEAR(rcapplicant_master.birth_date), 0) AS age , " & _
                                    "ISNULL(rcapplicantskill_tran.skillcategoryunkid, 0) skillcategoryunkid , " & _
                                    "ISNULL(rcapplicantskill_tran.skillunkid, 0) skillunkid , " & _
                                    "rcapplicantqualification_tran.award_start_date , " & _
                                    "rcapplicantqualification_tran.award_end_date " & _
                                    ", ISNULL(rcapplicant_master.employeeunkid, 0) AS employeeunkid " & _
                          "FROM      rcapplicant_master " & _
                                    "LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid AND rcapplicantqualification_tran.isvoid = 0 " & _
                                    "LEFT JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                    "LEFT JOIN rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid AND rcapplicantskill_tran.isvoid = 0 " & _
                                    "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicantqualification_tran.qualificationgroupunkid " & _
                                                            "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & "  " & _
                                    "LEFT JOIN hrresult_master ON hrresult_master.resultunkid = rcapplicantqualification_tran.resultunkid " & _
                                    "LEFT JOIN ( SELECT  applicantunkid , " & _
                                                        "CVac.vacancyunkid , " & _
                                                        "rcvacancy_master.isexternalvacancy, " & _
                                                        "rcvacancy_master.isbothintext, " & _
                                                        "cfcommon_master.name AS vacancy , " & _
                                                        "CONVERT(CHAR(8), openingdate, 112) AS ODate , " & _
                                                        "CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                                                "FROM    ( SELECT    applicantunkid , " & _
                                                                    "vacancyunkid , " & _
                                                                    "rcapp_vacancy_mapping.isimport AS isimport , " & _
                                                                    "ROW_NUMBER() OVER ( PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC ) AS Srno " & _
                                                          "FROM      rcapp_vacancy_mapping " & _
                                                          "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                                        ") AS CVac " & _
                                                        "JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                                                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                                                                              "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                                                "WHERE   CVac.Srno = 1 AND CVac.isimport = 0 " & _
                                              ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
                          "WHERE 1 = 1 " & _
                            "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 " & _
                        ") AS applicant " & _
                "WHERE   1 = 1 "
            'Sohail (20 Oct 2020) - [employeeunkid]
            'Sohail (20 Oct 2020) - []
            'Sohail (05 Jun 2020) - [isbothintext]
            'Sohail (09 Oct 2018) - [isactive = 1]=[ISNULL(isvoid, 0) = 0]
            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            '-> AND rcapplicantskill_tran.isvoid = 0
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            '[Add]--"rcapp_vacancy_mapping.isimport AS isimport , " & _
            '[Change]--"WHERE   CVac.Srno = 1 AND rcapp_vacancy_mapping.isimport = 0 " & _ --> "WHERE   CVac.Srno = 1 AND CVac.isimport = 0 " & _
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            '"WHERE rcapplicant_master.isimport = 0 " & _ Removed
            '"WHERE rcapp_vacancy_mapping.isimport = 0 " & _ Added
            'S.SANDEEP [ 14 May 2013 ] -- END


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
            StrQ &= "UNION ALL "

            StrQ &= "SELECT DISTINCT " & _
                        "applicantunkid " & _
                        ",isexternalvacancy " & _
                        ",isbothintext " & _
                        ",Vacancy " & _
                        ",odate as 'Opening Date' " & _
                        ",cdate as 'Closing Date' " & _
                        ",applicant_code as 'Applicant Code' " & _
                        ",Firstname " & _
                        ",Surname " & _
                        ",applicantname as 'Applicant Name' " & _
                        ",gender_name as 'Gender' " & _
                        ",birth_date as 'Birth Date' " & _
                        ",present_mobileno as 'Present Mobile No' " & _
                        ",Email " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                "+ CAST(cfcommon_master.name AS VARCHAR(MAX)) " & _
                       "FROM     hremp_qualification_tran " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid " & _
                                                             "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                       "WHERE    hremp_qualification_tran.employeeunkid = applicant.employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                       "ORDER BY cfcommon_master.masterunkid " & _
                     "FOR " & _
                       "XML PATH('') " & _
                     "), 1, 1, ''), '') AS 'Qualification Group' " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(hrqualification_master.qualificationname AS VARCHAR(MAX)) " & _
                                       "FROM     hremp_qualification_tran " & _
                                                "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                       "WHERE    hremp_qualification_tran.employeeunkid = applicant.employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                                       "ORDER BY hrqualification_master.qualificationunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS Qualification " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(hremp_qualification_tran.gpacode AS VARCHAR(MAX)) " & _
                                       "FROM     hremp_qualification_tran " & _
                                       "WHERE    hremp_qualification_tran.employeeunkid = applicant.employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS 'GPA' " & _
                        ",gender as genderid " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(cfcommon_master.name AS VARCHAR(MAX)) " & _
                                       "FROM     hremp_app_skills_tran " & _
                                                "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_app_skills_tran.skillcategoryunkid " & _
                                                                        "AND dbo.cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SKILL_CATEGORY & " " & _
                                       "WHERE    hremp_app_skills_tran.emp_app_unkid = applicant.employeeunkid AND hremp_app_skills_tran.isvoid = 0 " & _
                                       "ORDER BY cfcommon_master.masterunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS 'Skill Category'  " & _
                        ",ISNULL(STUFF(( SELECT   ',' " & _
                                                "+ CAST(hrskill_master.skillname AS VARCHAR(MAX)) " & _
                                       "FROM     hremp_app_skills_tran " & _
                                                "JOIN hrskill_master ON hremp_app_skills_tran.skillunkid = hrskill_master.skillunkid " & _
                                       "WHERE    hremp_app_skills_tran.emp_app_unkid = applicant.employeeunkid AND hremp_app_skills_tran.isvoid = 0 " & _
                                       "ORDER BY hrskill_master.skillunkid " & _
                                     "FOR " & _
                                       "XML PATH('') " & _
                                     "), 1, 1, ''), '') AS Skill " & _
                        ",employeecode as 'Employee Code' " & _
                        ",referenceno as 'Reference No' " & _
                        ",award_start_date " & _
                        ",award_end_date " & _
                        ", applicant.employeeunkid " & _
                "FROM    ( SELECT    rcapplicant_master.applicantunkid , " & _
                                    "applicant_code , " & _
                                    "ISNULL(firstname, '') firstname , " & _
                                    "ISNULL(surname, '') surname , " & _
                                    "ISNULL(firstname, '') + ' ' + ISNULL(surname, '') applicantname , " & _
                                    "CASE WHEN gender = 1 THEN @Male " & _
                                         "WHEN gender = 2 THEN @Female " & _
                                    "END gender_name , " & _
                                    "gender , " & _
                                    "birth_date , " & _
                                    "present_mobileno , " & _
                                    "email , " & _
                                    "ISNULL(employeecode, '') employeecode , " & _
                                    "ISNULL(referenceno, '') referenceno , " & _
                                    "ISNULL(cfcommon_master.masterunkid, 0) AS qualificationgroupunkid , " & _
                                    "ISNULL(hrqualification_master.qualificationunkid, 0) qualificationunkid , " & _
                                    "ISNULL(Vac.vacancyunkid, 0) vacancyunkid , " & _
                                    "ISNULL(Vac.isexternalvacancy,0) isexternalvacancy, " & _
                                    "ISNULL(Vac.isbothintext,0) isbothintext, " & _
                                    "ISNULL(Vac.vacancy,'')vacancy, " & _
                                    "ISNULL(Vac.ODate,'')ODate, " & _
                                    "ISNULL(Vac.CDate,'')CDate, " & _
                                    "ISNULL(cfcommon_master.qlevel, 0) qlevel , " & _
                                    "ISNULL(hrresult_master.resultunkid, 0) resultunkid , " & _
                                    "ISNULL(hrresult_master.result_level, 0) result_level , " & _
                                    "ISNULL(YEAR(GETDATE()) " & _
                                           "- YEAR(rcapplicant_master.birth_date), 0) AS age , " & _
                                    "ISNULL(hremp_app_skills_tran.skillcategoryunkid, 0) skillcategoryunkid , " & _
                                    "ISNULL(hremp_app_skills_tran.skillunkid, 0) skillunkid , " & _
                                    "hremp_qualification_tran.award_start_date , " & _
                                    "hremp_qualification_tran.award_end_date " & _
                                    ", ISNULL(rcapplicant_master.employeeunkid, 0) AS employeeunkid " & _
                          "FROM      rcapplicant_master " & _
                                    "LEFT JOIN hremp_qualification_tran ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                                    "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                                    "LEFT JOIN hremp_app_skills_tran ON rcapplicant_master.employeeunkid = hremp_app_skills_tran.emp_app_unkid AND hremp_app_skills_tran.isvoid = 0 " & _
                                    "JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid " & _
                                                            "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP) & "  " & _
                                    "LEFT JOIN hrresult_master ON hrresult_master.resultunkid = hremp_qualification_tran.resultunkid " & _
                                    "LEFT JOIN ( SELECT  applicantunkid , " & _
                                                        "CVac.vacancyunkid , " & _
                                                        "rcvacancy_master.isexternalvacancy, " & _
                                                        "rcvacancy_master.isbothintext, " & _
                                                        "cfcommon_master.name AS vacancy , " & _
                                                        "CONVERT(CHAR(8), openingdate, 112) AS ODate , " & _
                                                        "CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                                                "FROM    ( SELECT    applicantunkid , " & _
                                                                    "vacancyunkid , " & _
                                                                    "rcapp_vacancy_mapping.isimport AS isimport , " & _
                                                                    "ROW_NUMBER() OVER ( PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC ) AS Srno " & _
                                                          "FROM      rcapp_vacancy_mapping " & _
                                                          "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                                        ") AS CVac " & _
                                                        "JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                                                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
                                                                              "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                                                "WHERE   CVac.Srno = 1 AND CVac.isimport = 0 " & _
                                              ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
                          "WHERE 1 = 1 " & _
                            "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 " & _
                        ") AS applicant " & _
                "WHERE   1 = 1 "

            StrQ &= Me._FilterQuery
            'Sohail (20 Oct 2020) -- End

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim rpt_Data As DataTable = dsList.Tables("DataTable").Clone

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If dtRow("Opening Date").ToString().Trim <> "" AndAlso dtRow("Closing Date").ToString().Trim <> "" Then
                    dtRow("Vacancy") = dtRow("Vacancy") & " ( " & eZeeDate.convertDate(dtRow("Opening Date")).Date & " - " & eZeeDate.convertDate(dtRow("Closing Date")).Date & " ) "
                    dtRow("Opening Date") = "  " & eZeeDate.convertDate(dtRow("Opening Date").ToString()).Date
                    dtRow("Closing Date") = "  " & eZeeDate.convertDate(dtRow("Closing Date").ToString()).Date
                Else
                    dtRow("Vacancy") = dtRow("Vacancy")
                    dtRow("Opening Date") = ""
                    dtRow("Closing Date") = ""
                End If



                If Not IsDBNull(dtRow("Reference No")) And dtRow("Reference No").ToString() <> "" Then
                    dtRow("Reference No") = clsCrypto.Dicrypt(dtRow("Reference No").ToString())
                End If
                rpt_Data.ImportRow(dtRow)
            Next


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, rpt_Data) Then
            If Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), xExportReportPath, rpt_Data, xUserUnkid, xCompanyUnkid) Then
                'Shani(24-Aug-2015) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported to Report export path."), enMsgBoxStyle.Information)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
                Call ReportFunction.Open_ExportedFile(xOpenReportAfterExport, StrFinalPath)
                'Shani(24-Aug-2015) -- End

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub



    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
    'Public Sub Generate_SummaryReport()
    Public Sub Generate_SummaryReport(ByVal xUserUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xExportReportPath As String, _
                                      ByVal xOpenReportAfterExport As Boolean)
        'Shani(24-Aug-2015) -- End

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsListMain As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim dsApplicant As New DataSet
        Dim dsPQualification As New DataSet
        Dim dsWorkExperience As New DataSet
        Dim dsOtherShortCourses As New DataSet

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Dim objUser As New clsUserAddEdit
        'Shani(24-Aug-2015) -- End

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            objUser._Userunkid = xUserUnkid
            'Shani(24-Aug-2015) -- End



            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '               "applicantunkid " & _
            '               ",code " & _
            '               ",CASE WHEN age = '0' THEN '' ELSE age END as age " & _
            '               ",gender " & _
            '               ",NAMEAdd " & _
            '               ",vacancyunkid " & _
            '               ",isexternalvacancy " & _
            '               ",employeecode " & _
            '        "FROM ( " & _
            '        "SELECT " & _
            '                "rcapplicant_master.applicantunkid " & _
            '                ",rcapplicant_master.applicant_code AS code " & _
            '               ",CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') = '' THEN '' " & _
            '       "       ELSE CAST(ISNULL(DATEDIFF(yy,rcapplicant_master.birth_date,GETDATE()),'') AS NVARCHAR(50)) END AS age " & _
            '               ",ISNULL(CASE WHEN gender=1 THEN @MALE ELSE @FEMALE END ,'') AS gender " & _
            '               ",rcapplicant_master.firstname + ' ' + rcapplicant_master.surname +'\r\n' + rcapplicant_master.present_address1 +'\r\n'+ rcapplicant_master.present_address2 +'\r\n'+ rcapplicant_master.present_mobileno   AS NAMEAdd " & _
            '               ",ISNULL(Vac.vacancyunkid, 0) as vacancyunkid " & _
            '               ",ISNULL(Vac.isexternalvacancy,0) as isexternalvacancy " & _
            '               ",ISNULL(employeecode, '')  AS employeecode " & _
            '        "FROM rcapplicant_master " & _
            '             "LEFT JOIN ( SELECT  applicantunkid , " & _
            '                                 "CVac.vacancyunkid , " & _
            '                                 "rcvacancy_master.isexternalvacancy, " & _
            '                                  "cfcommon_master.name AS vacancy , " & _
            '                                  "CONVERT(CHAR(8), openingdate, 112) AS ODate , " & _
            '                                  "CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
            '                           "FROM    ( SELECT    applicantunkid , " & _
            '                                                "vacancyunkid , " & _
            '                                                "ROW_NUMBER() OVER ( PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC ) AS Srno " & _
            '                                       "FROM      rcapp_vacancy_mapping " & _
            '                                        "WHERE     isactive = 1 " & _
            '                                      ") AS CVac " & _
            '                                           "JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                                          "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle " & _
            '                                                                  "AND cfcommon_master.mastertype =  34 " & _
            '                                    "WHERE   CVac.Srno = 1 " & _
            '                                  ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
            '        "WHERE isimport = 0  ) " & _
            '        "as applicant  WHERE 1 = 1  "
            StrQ = "SELECT " & _
                           "applicantunkid " & _
                           ",code " & _
                    "	,CASE WHEN age = '0' THEN '' ELSE age END AS age " & _
                           ",gender " & _
                           ",NAMEAdd " & _
                           ",vacancyunkid " & _
                           ",isexternalvacancy " & _
                           ",isbothintext " & _
                           ",employeecode " & _
                    "FROM " & _
                    "( " & _
                    "SELECT " & _
                            "rcapplicant_master.applicantunkid " & _
                            ",rcapplicant_master.applicant_code AS code " & _
                           ",CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') = '' THEN '' " & _
                   "       ELSE CAST(ISNULL(DATEDIFF(yy,rcapplicant_master.birth_date,GETDATE()),'') AS NVARCHAR(50)) END AS age " & _
                           ",ISNULL(CASE WHEN gender=1 THEN @MALE ELSE @FEMALE END ,'') AS gender " & _
                    "		,rcapplicant_master.firstname + ' ' + rcapplicant_master.surname + '\r\n' + rcapplicant_master.present_address1 + '\r\n' " & _
                    "		+ rcapplicant_master.present_address2 + '\r\n'+ rcapplicant_master.present_mobileno AS NAMEAdd " & _
                    "		,ISNULL(Vac.vacancyunkid, 0) AS vacancyunkid " & _
                    "		,ISNULL(Vac.isexternalvacancy, 0) AS isexternalvacancy " & _
                    "		,ISNULL(Vac.isbothintext, 0) AS isbothintext " & _
                           ",ISNULL(employeecode, '')  AS employeecode " & _
                           ",ISNULL(employeeunkid, 0)  AS employeeunkid " & _
                    "FROM rcapplicant_master " & _
                    "	LEFT JOIN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 rcapp_vacancy_mapping.applicantunkid " & _
                    "			,rcapp_vacancy_mapping.vacancyunkid " & _
                    "			,rcvacancy_master.isexternalvacancy " & _
                    "			,rcvacancy_master.isbothintext " & _
                    "			,cfcommon_master.name AS vacancy " & _
                    "			,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                    "			,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                                                   "FROM      rcapp_vacancy_mapping " & _
                    "			JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid " & _
                    "			JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "  " & _
                    "		WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND rcapp_vacancy_mapping.vacancyunkid = '" & mintVacancyId & "'" & " " & _
                                              ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid " & _
                    "	WHERE isimport = 0 " & _
                    ") AS applicant " & _
                    "WHERE 1 = 1 "
            'Sohail (20 Oct 2020) - [,ISNULL(employeeunkid, 0)  AS employeeunkid]
            'Sohail (05 Jun 2020) - [isbothintext]
            'Sohail (09 Oct 2018) - [rcapp_vacancy_mapping.isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsListMain = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            StrQ = ""
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
            'StrQ = "SELECT " & _
            '            "applicantunkid " & _
            '            ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
            '             "hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') " & _
            '             "else " & _
            '            "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ hrqualification_master.qualificationname +' - '+ ISNULL(hrinstitute_master.institute_name,'') END,'') AS qualification " & _
            '           ",ISNULL(hrresult_master.resultname,'') AS classification " & _
            '           ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
            '           ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
            '           ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
            '        "FROM rcapplicantqualification_tran " & _
            '           "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=rcapplicantqualification_tran.qualificationunkid " & _
            '           "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=rcapplicantqualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
            '           "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=rcapplicantqualification_tran.resultunkid " & _
            '        " WHERE rcapplicantqualification_tran.qualificationunkid > 0 AND rcapplicantqualification_tran.isvoid = 0 "
            StrQ = "SELECT " & _
                        " rcapplicant_master.applicantunkid " & _
                        ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                         "CASE WHEN rcapplicantqualification_tran.qualificationunkid > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE rcapplicantqualification_tran.other_qualification END +' - '+ CASE WHEN rcapplicantqualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE rcapplicantqualification_tran.other_institute END " & _
                         "ELSE " & _
                        "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ CASE WHEN rcapplicantqualification_tran.qualificationunkid > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE rcapplicantqualification_tran.other_qualification END +' - '+ CASE WHEN rcapplicantqualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE rcapplicantqualification_tran.other_institute END END,'') AS qualification " & _
                       ",CASE WHEN rcapplicantqualification_tran.resultunkid > 0 THEN ISNULL(hrresult_master.resultname, '') ELSE rcapplicantqualification_tran.other_resultcode END AS classification " & _
                       ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                       ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                       ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                       "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid " & _
                       "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=rcapplicantqualification_tran.qualificationunkid " & _
                       "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=rcapplicantqualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=rcapplicantqualification_tran.resultunkid " & _
                    " WHERE rcapplicantqualification_tran.isvoid = 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "

            If mintApplicantId > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid = @applicantunkid "
            End If

            If mintApplicantTypeId > 0 Then
                If mintApplicantTypeId = 1 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                ElseIf mintApplicantTypeId = 2 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                End If
            End If

            If mstrReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND rcapplicant_master.referenceno = @referenceno "
            End If

            StrQ &= "UNION ALL "

            StrQ &= "SELECT " & _
                        " rcapplicant_master.applicantunkid " & _
                        ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112),'') ='' THEN " & _
                         "CASE WHEN hremp_qualification_tran.qualificationunkid > 0 THEN hrqualification_master.qualificationname ELSE hremp_qualification_tran.other_qualification END +' - '+ CASE WHEN hremp_qualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE hremp_qualification_tran.other_institute END " & _
                         "ELSE " & _
                        "ISNULL(CAST(YEAR(hremp_qualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(hremp_qualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ CASE WHEN hremp_qualification_tran.qualificationunkid > 0 THEN hrqualification_master.qualificationname ELSE hremp_qualification_tran.other_qualification END +' - '+ CASE WHEN hremp_qualification_tran.instituteunkid > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE hremp_qualification_tran.other_institute END END,'') AS qualification " & _
                       ",CASE WHEN hremp_qualification_tran.resultunkid > 0 THEN ISNULL(hrresult_master.resultname, '') ELSE hremp_qualification_tran.other_resultcode END AS classification " & _
                       ",CASE WHEN hremp_qualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(hremp_qualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                       ", ISNULL(hremp_qualification_tran.award_start_date,'') AS startdate " & _
                       ", ISNULL(hremp_qualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM hremp_qualification_tran " & _
                       "JOIN rcapplicant_master ON rcapplicant_master.employeeunkid = hremp_qualification_tran.employeeunkid " & _
                       "JOIN hrqualification_master ON hrqualification_master.qualificationunkid=hremp_qualification_tran.qualificationunkid " & _
                       "LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid=hremp_qualification_tran.instituteunkid AND hrinstitute_master.ishospital=0 " & _
                       "LEFT JOIN hrresult_master ON hrresult_master.resultunkid=hremp_qualification_tran.resultunkid " & _
                    " WHERE hremp_qualification_tran.isvoid = 0 " & _
                        "AND rcapplicant_master.isvoid = 0 " & _
                        "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "

            If mintApplicantId > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid = @applicantunkid "
            End If

            If mintApplicantTypeId > 0 Then
                If mintApplicantTypeId = 1 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                ElseIf mintApplicantTypeId = 2 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                End If
            End If

            If mstrReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND rcapplicant_master.referenceno = @referenceno "
            End If
            'Sohail (20 Oct 2020) -- End

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END
            dsPQualification = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            'Sohail (20 Oct 2020) -- Start
            'NMB Enhancement : # OLD-189  : Pick data from employee bio-data for internal applicant in Applicant Qualification Report.
            'StrQ = "SELECT " & _
            '          "applicantunkid " & _
            '       ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcjobhistory.joiningdate ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcjobhistory.terminationdate,112),'') ='' THEN " & _
            '         "ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') " & _
            '         "else " & _
            '        "ISNULL(CAST(YEAR(rcjobhistory.joiningdate) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(rcjobhistory.terminationdate) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') END,'') AS Experience " & _
            '        " ,ISNULL(rcjobhistory.joiningdate,'')  AS startdate " & _
            '        ",ISNULL(rcjobhistory.terminationdate,'') AS ENDdate " & _
            '    "FROM rcjobhistory WHERE 1 = 1 AND rcjobhistory.isvoid = 0 "
            StrQ = "SELECT " & _
                      " rcapplicant_master.applicantunkid " & _
                   ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcjobhistory.joiningdate ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcjobhistory.terminationdate,112),'') ='' THEN " & _
                     "ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') " & _
                     "else " & _
                    "ISNULL(CAST(YEAR(rcjobhistory.joiningdate) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(rcjobhistory.terminationdate) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(rcjobhistory.employername,'') +' - '+ ISNULL(rcjobhistory.companyname ,'') +' - '+ ISNULL(rcjobhistory.designation,'') +' - ' +ISNULL(rcjobhistory.responsibility,'') END,'') AS Experience " & _
                    " ,ISNULL(rcjobhistory.joiningdate,'')  AS startdate " & _
                    ",ISNULL(rcjobhistory.terminationdate,'') AS ENDdate " & _
                "FROM rcjobhistory " & _
                "JOIN rcapplicant_master ON rcapplicant_master.applicantunkid=rcjobhistory.applicantunkid " & _
                "WHERE 1 = 1 AND rcjobhistory.isvoid = 0 " & _
                "AND rcapplicant_master.isvoid = 0 " & _
                "AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "

            If mintApplicantId > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid = @applicantunkid "
            End If

            If mintApplicantTypeId > 0 Then
                If mintApplicantTypeId = 1 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                ElseIf mintApplicantTypeId = 2 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                End If
            End If

            If mstrReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND rcapplicant_master.referenceno = @referenceno "
            End If

            StrQ &= "UNION ALL "

            StrQ &= "SELECT " & _
                      " rcapplicant_master.applicantunkid " & _
                   ", ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),hremp_experience_tran.start_date ,112),'') ='' AND ISNULL(CONVERT(CHAR(8),hremp_experience_tran.end_date,112),'') ='' THEN " & _
                     "ISNULL(hremp_experience_tran.contact_person,'') +' - '+ ISNULL(hremp_experience_tran.company ,'') +' - '+ ISNULL(hremp_experience_tran.old_job,'') +' - ' +ISNULL(hremp_experience_tran.remark,'') " & _
                     "ELSE " & _
                    "ISNULL(CAST(YEAR(hremp_experience_tran.start_date) AS NVARCHAR(10)),'')+' - '+ ISNULL(CAST(YEAR(hremp_experience_tran.end_date) AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(hremp_experience_tran.contact_person,'') +' - '+ ISNULL(hremp_experience_tran.company ,'') +' - '+ ISNULL(hremp_experience_tran.old_job,'') +' - ' +ISNULL(hremp_experience_tran.remark,'') END,'') AS Experience " & _
                    " ,ISNULL(hremp_experience_tran.start_date,'')  AS startdate " & _
                    ",ISNULL(hremp_experience_tran.end_date,'') AS ENDdate " & _
                "FROM hremp_experience_tran " & _
                "JOIN rcapplicant_master ON rcapplicant_master.employeeunkid=hremp_experience_tran.employeeunkid " & _
                "WHERE 1 = 1 AND hremp_experience_tran.isvoid = 0 " & _
                "AND rcapplicant_master.isvoid = 0 " & _
                "AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "

            If mintApplicantId > 0 Then
                StrQ &= " AND rcapplicant_master.applicantunkid = @applicantunkid "
            End If

            If mintApplicantTypeId > 0 Then
                If mintApplicantTypeId = 1 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                ElseIf mintApplicantTypeId = 2 Then
                    StrQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                End If
            End If

            If mstrReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND rcapplicant_master.referenceno = @referenceno "
            End If
            'Sohail (20 Oct 2020) -- End
            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcjobhistory.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            dsWorkExperience = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            StrQ = ""
            StrQ = "SELECT " & _
                            "applicantunkid " & _
                           ",ISNULL(CASE WHEN ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_start_date,112),'') ='' AND ISNULL(CONVERT(CHAR(8),rcapplicantqualification_tran.award_end_date,112),'') ='' THEN " & _
                             "ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') " & _
                             "else " & _
                            "ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_start_date) AS NVARCHAR(10)),'') +' - '+ ISNULL(CAST(YEAR(rcapplicantqualification_tran.award_end_date)AS NVARCHAR(10)),'') +'\r\n'+ ISNULL(other_qualification,'') +' - '+ ISNULL(other_institute,'') END,'') AS qualification " & _
                           ",ISNULL(other_resultcode,'') AS classification " & _
                           ",CASE WHEN rcapplicantqualification_tran.gpacode  = 0 THEN '' ELSE ISNULL(CAST(rcapplicantqualification_tran.gpacode AS NVARCHAR(50)),'') END AS GPA " & _
                           ", ISNULL(rcapplicantqualification_tran.award_start_date,'') AS startdate " & _
                           ", ISNULL(rcapplicantqualification_tran.award_end_date,'') AS ENDdate " & _
                    "FROM rcapplicantqualification_tran " & _
                   "WHERE 1 = 2 /*rcapplicantqualification_tran.qualificationunkid <= 0 AND rcapplicantqualification_tran.isvoid = 0*/ "
            'Sohail (20 Oct 2020) - [WHERE 1 = 2]
            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            dsOtherShortCourses = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Rows As DataRow = Nothing
            Dim dtPQFilter As DataTable
            Dim dtWEFilter As DataTable
            Dim dtOQFilter As DataTable
            Dim blnIsAdded As Boolean = False
            Dim iColCnt As Integer = 1

            For Each dtRow As DataRow In dsListMain.Tables("DataTable").Rows
                dtPQFilter = New DataView(dsPQualification.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable
                dtWEFilter = New DataView(dsWorkExperience.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "startdate DESC", DataViewRowState.CurrentRows).ToTable
                dtOQFilter = New DataView(dsOtherShortCourses.Tables(0), "applicantunkid = '" & dtRow.Item("applicantunkid") & "'", "ENDdate DESC", DataViewRowState.CurrentRows).ToTable

                Dim iPQCnt As Integer = dtPQFilter.Rows.Count
                Dim iWECnt As Integer = dtWEFilter.Rows.Count
                Dim iOQCnt As Integer = dtOQFilter.Rows.Count

                blnIsAdded = False

                If iPQCnt <= 0 AndAlso iWECnt <= 0 AndAlso iOQCnt <= 0 Then 'This is for  when professional Qualification, Experience and other Qualification /Short courses is blank
                    If blnIsAdded = False Then
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        rpt_Rows.Item("Column1") = dtRow.Item("code")
                        rpt_Rows.Item("Column2") = dtRow.Item("age")
                        rpt_Rows.Item("Column3") = dtRow.Item("gender")
                        rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column5") = ""
                        rpt_Rows.Item("Column6") = ""
                        rpt_Rows.Item("Column7") = ""
                        rpt_Rows.Item("Column8") = ""
                        rpt_Rows.Item("Column9") = ""
                        rpt_Rows.Item("Column10") = iColCnt.ToString

                        iColCnt += 1
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                        blnIsAdded = True

                        Continue For
                    End If
                End If

                If iPQCnt >= iWECnt AndAlso iPQCnt >= iOQCnt Then 'This is for professional Qualification is greater than Experience and other Qualification /Short courses
                    For i As Integer = 0 To dtPQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If


                        rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                        rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")

                        If i < iWECnt AndAlso iWECnt > 0 Then 'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If


                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iWECnt >= iPQCnt AndAlso iWECnt >= iOQCnt Then 'This is for Experience  is greater than professional Qualification and other Qualification /Short courses
                    For i As Integer = 0 To dtWEFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iOQCnt AndAlso iOQCnt > 0 Then 'This is for data of Other Qualification/Short course based on first row
                            rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                        Else
                            rpt_Rows.Item("Column9") = ""
                        End If


                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                ElseIf iOQCnt >= iPQCnt AndAlso iOQCnt >= iWECnt Then 'This is for other Qualification /Short courses is greater than professional Qualification and Experience 
                    For i As Integer = 0 To dtOQFilter.Rows.Count - 1
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                        If blnIsAdded = False Then
                            rpt_Rows.Item("Column1") = dtRow.Item("code")
                            rpt_Rows.Item("Column2") = dtRow.Item("age")
                            rpt_Rows.Item("Column3") = dtRow.Item("gender")
                            rpt_Rows.Item("Column4") = dtRow.Item("NAMEAdd").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column10") = iColCnt.ToString
                            iColCnt += 1
                            blnIsAdded = True
                        Else
                            rpt_Rows.Item("Column1") = ""
                            rpt_Rows.Item("Column2") = ""
                            rpt_Rows.Item("Column3") = ""
                            rpt_Rows.Item("Column4") = ""
                        End If

                        rpt_Rows.Item("Column9") = dtOQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)

                        If i < iPQCnt AndAlso iPQCnt > 0 Then 'This is for data of Professional Qualification based on first row.
                            rpt_Rows.Item("Column5") = dtPQFilter.Rows(i).Item("qualification").ToString.Replace("\r\n", vbCrLf)
                            rpt_Rows.Item("Column6") = dtPQFilter.Rows(i).Item("classification")
                            rpt_Rows.Item("Column7") = dtPQFilter.Rows(i).Item("GPA")
                        Else
                            rpt_Rows.Item("Column5") = ""
                            rpt_Rows.Item("Column6") = ""
                            rpt_Rows.Item("Column7") = ""
                        End If

                        If i < iWECnt AndAlso iWECnt > 0 Then  'This is for data of Experience based on first row
                            rpt_Rows.Item("Column8") = dtWEFilter.Rows(i).Item("Experience").ToString.Replace("\r\n", vbCrLf)

                        Else
                            rpt_Rows.Item("Column8") = ""
                        End If
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    Next
                End If
            Next

            objRpt = New ArutiReport.Designer.rptApplicantSummaryListing


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 9, "Prepared By :"))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", objUser._Username)
                'Shani(24-Aug-2015) -- End

            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 10, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 11, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 12, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 13, "Sr No."))
            Call ReportFunction.TextChange(objRpt, "txtAppcode", Language.getMessage(mstrModuleName, 14, "App. Code"))

            Call ReportFunction.TextChange(objRpt, "txtAppAge", Language.getMessage(mstrModuleName, 15, "Age"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 16, "Gender"))
            Call ReportFunction.TextChange(objRpt, "txtNameAddressMobile", Language.getMessage(mstrModuleName, 17, "Name /Address /Mobile"))

            Call ReportFunction.TextChange(objRpt, "txtQualification", Language.getMessage(mstrModuleName, 18, "Qualification"))
            Call ReportFunction.TextChange(objRpt, "txtResultCode", Language.getMessage(mstrModuleName, 19, "Classification"))

            Call ReportFunction.TextChange(objRpt, "txtGPA", Language.getMessage(mstrModuleName, 20, "GPA"))
            Call ReportFunction.TextChange(objRpt, "txtExperience", Language.getMessage(mstrModuleName, 21, "Experience"))
            Call ReportFunction.TextChange(objRpt, "txtOtherShortcourses", Language.getMessage(mstrModuleName, 22, "Other Qualification/Short Courses"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)


            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName & Language.getMessage(mstrModuleName, 23, " For ") & mstrVacancyName)
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'Call ReportExecute(objRpt, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            Call ReportExecute(objRpt, enPrintAction.Preview, enExportAction.None, xExportReportPath, xOpenReportAfterExport)
            'Shani(24-Aug-2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
        Finally
            objUser = Nothing
            'Shani(24-Aug-2015) -- End
        End Try
    End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Applicant :")
            Language.setMessage(mstrModuleName, 2, "Vacancy Type :")
            Language.setMessage(mstrModuleName, 3, "Vacancy :")
            Language.setMessage(mstrModuleName, 4, "Applicant Type :")
            Language.setMessage(mstrModuleName, 5, "Reference No :")
            Language.setMessage(mstrModuleName, 6, " Order By :")
            Language.setMessage(mstrModuleName, 7, "Date :")
            Language.setMessage(mstrModuleName, 8, "Report successfully exported to Report export path.")
            Language.setMessage(mstrModuleName, 9, "Prepared By :")
            Language.setMessage(mstrModuleName, 10, "Checked By :")
            Language.setMessage(mstrModuleName, 11, "Approved By :")
            Language.setMessage(mstrModuleName, 12, "Received By :")
            Language.setMessage(mstrModuleName, 13, "Sr No.")
            Language.setMessage(mstrModuleName, 14, "App. Code")
            Language.setMessage(mstrModuleName, 15, "Age")
            Language.setMessage(mstrModuleName, 16, "Gender")
            Language.setMessage(mstrModuleName, 17, "Name /Address /Mobile")
            Language.setMessage(mstrModuleName, 18, "Qualification")
            Language.setMessage(mstrModuleName, 19, "Classification")
            Language.setMessage(mstrModuleName, 20, "GPA")
            Language.setMessage(mstrModuleName, 21, "Experience")
            Language.setMessage(mstrModuleName, 22, "Other Qualification/Short Courses")
            Language.setMessage(mstrModuleName, 23, " For")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

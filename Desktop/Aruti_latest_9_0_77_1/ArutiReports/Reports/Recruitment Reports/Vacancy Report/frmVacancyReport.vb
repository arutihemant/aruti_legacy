'************************************************************************************************************************************
'Class Name : frmVacancyReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmVacancyReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmVacancyReport"
    Private objVacReport As clsVacancyReport

#End Region

#Region " Contructor "

    Public Sub New()
        objVacReport = New clsVacancyReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objVacReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim objJMaster As New clsJobs
        Dim objVMaster As New clsVacancy
        Dim objMaster As New clsMasterData 'Hemant (02 Jul 2019)
        Try
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With cboEmployment
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "List")
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With

            dsCombo = objJMaster.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsCombo = objVMaster.getComboList(True, "List")

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objVMaster.getComboList(True, "List", , , , , True)
            dsCombo = objVMaster.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , , , , True)
            'Shani(24-Aug-2015) -- End


            'Anjan (02 Mar 2012)-End 
            With cboVacancy
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
            End With



            dsCombo = objVacReport.GetReport_TypeList("List")
            With cboReportType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dsCombo = objMaster.getVacancyStatus("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Status")
                .SelectedValue = 0
            End With
            'Hemant (02 Jul 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objCMaster = Nothing
            objJMaster = Nothing
            objVMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployment.SelectedValue = 0
            cboInterviewType.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboReportType.SelectedValue = 0
            cboVacancy.SelectedValue = 0
            objVacReport.setDefaultOrderBy(cboReportType.SelectedValue)
            txtOrderBy.Text = objVacReport.OrderByDisplay
            cboStatus.SelectedValue = 0 'Hemant (02 Jul 2019)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            objVacReport.SetDefaultValue()

            objVacReport._ReportType_Id = cboReportType.SelectedValue
            objVacReport._ReportType_Name = cboReportType.Text

            objVacReport._VacancyId = cboVacancy.SelectedValue
            objVacReport._VacancyName = cboVacancy.Text

            Select Case cboReportType.SelectedValue
                Case 0     'LIST
                    objVacReport._EmplTypeId = cboEmployment.SelectedValue
                    objVacReport._EmplTypeName = cboEmployment.Text

                    objVacReport._VacJobId = cboJob.SelectedValue
                    objVacReport._VacJob_Name = cboJob.Text
                    'Hemant (02 Jul 2019) -- Start
                    'ENHANCEMENT :  ZRA minimum Requirements
                    objVacReport._StatusId = cboStatus.SelectedValue
                    objVacReport._StatusName = cboStatus.Text
                    'Hemant (02 Jul 2019) -- End
                Case 1     'DETAILS
                    objVacReport._VacInterviewTypeId = cboInterviewType.SelectedValue
                    objVacReport._VacInterviewTypeName = cboInterviewType.Text
            End Select

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmVacancyReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objVacReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmVacancyReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmVacancyReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objVacReport._ReportName
            Me._Message = objVacReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancyReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objVacReport.generateReport(cboReportType.SelectedValue, e.Type, enExportAction.None)
            objVacReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           cboReportType.SelectedValue, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
            'objVacReport.generateReport(cboReportType.SelectedValue, enPrintAction.None, e.Type)
            objVacReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, _
                                           cboReportType.SelectedValue, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objVacReport.setOrderBy(cboReportType.SelectedValue)
            txtOrderBy.Text = objVacReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsVacancyReport.SetMessages()
            objfrm._Other_ModuleNames = "clsVacancyReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End


#End Region

#Region " Controls "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedValue
                Case 0     'LIST
                    cboInterviewType.SelectedValue = 0
                    cboInterviewType.Enabled = False
                    cboEmployment.SelectedValue = 0
                    cboEmployment.Enabled = True
                    cboJob.SelectedValue = 0
                    cboJob.Enabled = True
                    'Hemant (02 Jul 2019) -- Start
                    'ENHANCEMENT :  ZRA minimum Requirements
                    lblStatus.Visible = True
                    cboStatus.Visible = True
                    cboStatus.SelectedValue = 0
                    'Hemant (02 Jul 2019) -- End
                Case 1     'DETAILS
                    cboInterviewType.SelectedValue = 0
                    cboInterviewType.Enabled = True
                    cboEmployment.SelectedValue = 0
                    cboEmployment.Enabled = False
                    cboJob.SelectedValue = 0
                    cboJob.Enabled = False
                    'Hemant (02 Jul 2019) -- Start
                    'ENHANCEMENT :  ZRA minimum Requirements
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    cboStatus.SelectedValue = 0
                    'Hemant (02 Jul 2019) -- End
            End Select
            cboVacancy.SelectedValue = 0
            objVacReport.setDefaultOrderBy(cboReportType.SelectedValue)
            txtOrderBy.Text = objVacReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblEmplType.Text = Language._Object.getCaption(Me.lblEmplType.Name, Me.lblEmplType.Text)
			Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
			Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

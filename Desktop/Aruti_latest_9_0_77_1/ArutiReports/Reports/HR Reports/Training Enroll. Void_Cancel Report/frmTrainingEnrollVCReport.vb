'************************************************************************************************************************************
'Class Name : frmTrainingEnrollVCReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTrainingEnrollVCReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingEnrollVCReport"
    Private objTrainingEVCReport As clsTrainingEnrollVCReport

#End Region

#Region " Contructor "

    Public Sub New()
        objTrainingEVCReport = New clsTrainingEnrollVCReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTrainingEVCReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objUMaster As New clsUserAddEdit
        Dim ObjTrain_Sched As New clsTraining_Scheduling
        Dim ObjEmp As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsCombo = objUMaster.getComboList("List", True)

            'Nilay (01-Mar-2016) -- Start
            'dsCombo = objUMaster.getNewComboList("List", , True)
            dsCombo = objUMaster.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboCancelUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
            End With

            With cboVoidUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
            End With

            With cboEnrollUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
            End With

            dsCombo = objTrainingEVCReport.Get_Status("List")
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
            End With


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsCombo = ObjEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombo = ObjEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombo = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsCombo = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsCombo = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsCombo = ObjTrain_Sched.getComboList("Training", True,)
            dsCombo = ObjTrain_Sched.getComboList("Training", True, , True)
            'Anjan (10 Feb 2012)-End 

            dsCombo = ObjTrain_Sched.getComboList("Training", True, , True)
            With cboTraining
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Training")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCancelUser.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboEnrollUser.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboTraining.SelectedValue = 0
            cboVoidUser.SelectedValue = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objTrainingEVCReport.SetDefaultValue()

            objTrainingEVCReport._CancelUserId = cboCancelUser.SelectedValue
            objTrainingEVCReport._CancelUserName = cboCancelUser.Text

            objTrainingEVCReport._EmpId = cboEmployee.SelectedValue
            objTrainingEVCReport._EmpName = cboEmployee.Text

            objTrainingEVCReport._EnUserId = cboEnrollUser.SelectedValue
            objTrainingEVCReport._EnUserName = cboEnrollUser.Text

            objTrainingEVCReport._StatusId = cboStatus.SelectedValue
            objTrainingEVCReport._StatusName = cboStatus.Text

            objTrainingEVCReport._TrainingId = cboTraining.SelectedValue
            objTrainingEVCReport._TrainingName = cboTraining.Text

            objTrainingEVCReport._VoidUserId = cboVoidUser.SelectedValue
            objTrainingEVCReport._VoidUserName = cboVoidUser.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objTrainingEVCReport._IsAcitve = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmTrainingEnrollVCReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingEVCReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmTrainingEnrollVCReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingEnrollVCReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objTrainingEVCReport._ReportName
            Me._Message = objTrainingEVCReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingEnrollVCReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTrainingEVCReport.generateReport(0, e.Type, enExportAction.None)
            objTrainingEVCReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTrainingEVCReport.generateReport(0, enPrintAction.None, e.Type)
            objTrainingEVCReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTraining.DataSource
            frm.SelectedValue = cboTraining.SelectedValue
            frm.ValueMember = cboTraining.ValueMember
            frm.DisplayMember = cboTraining.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTraining.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.SelectedValue = cboEmployee.SelectedValue
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingEnrollVCReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingEnrollVCReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

#Region " Controls "

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Select Case cboStatus.SelectedIndex
                Case 1  'Cancelled
                    cboVoidUser.SelectedValue = 0
                    cboVoidUser.Enabled = False

                    cboCancelUser.SelectedValue = 0
                    cboCancelUser.Enabled = True
                Case 2  'Void
                    cboVoidUser.SelectedValue = 0
                    cboVoidUser.Enabled = True

                    cboCancelUser.SelectedValue = 0
                    cboCancelUser.Enabled = False
                Case Else
                    cboVoidUser.SelectedValue = 0
                    cboCancelUser.SelectedValue = 0

                    cboVoidUser.Enabled = True
                    cboCancelUser.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblTraining.Text = Language._Object.getCaption(Me.lblTraining.Name, Me.lblTraining.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblCancellationUser.Text = Language._Object.getCaption(Me.lblCancellationUser.Name, Me.lblCancellationUser.Text)
			Me.lblVoidUser.Text = Language._Object.getCaption(Me.lblVoidUser.Name, Me.lblVoidUser.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEnrollUser.Text = Language._Object.getCaption(Me.lblEnrollUser.Name, Me.lblEnrollUser.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmATNonDisclouserDeclarationReport
#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmATNonDisclouserDeclarationReport"
    Private objATNonDisclouserDeclarationReport As clsATNonDisclouserDeclarationReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        objATNonDisclouserDeclarationReport = New clsATNonDisclouserDeclarationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objATNonDisclouserDeclarationReport.SetDefaultValue()
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0

            dtpDeclarationFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDeclarationToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDeclarationFromDate.Checked = False
            dtpDeclarationToDate.Checked = False

            dtpAuditFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpAuditToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpAuditFromDate.Checked = False
            dtpAuditToDate.Checked = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objATNonDisclouserDeclarationReport.SetDefaultValue()

            If dtpDeclarationFromDate.Checked = True AndAlso dtpDeclarationToDate.Checked = True Then
                objATNonDisclouserDeclarationReport._DeclarationDateFrom = dtpDeclarationFromDate.Value.Date
                objATNonDisclouserDeclarationReport._DeclarationDateTo = dtpDeclarationToDate.Value.Date
            End If

            If dtpAuditFromDate.Checked = True AndAlso dtpAuditToDate.Checked = True Then
                objATNonDisclouserDeclarationReport._AuditDateFrom = dtpAuditFromDate.Value.Date
                objATNonDisclouserDeclarationReport._AuditDateTo = dtpAuditToDate.Value.Date
            End If


            objATNonDisclouserDeclarationReport._EmployeeId = cboEmployee.SelectedValue
            objATNonDisclouserDeclarationReport._EmployeeName = cboEmployee.Text

            objATNonDisclouserDeclarationReport._ViewByIds = mstrStringIds
            objATNonDisclouserDeclarationReport._ViewIndex = mintViewIdx
            objATNonDisclouserDeclarationReport._ViewByName = mstrStringName
            objATNonDisclouserDeclarationReport._Analysis_Fields = mstrAnalysis_Fields
            objATNonDisclouserDeclarationReport._Analysis_Join = mstrAnalysis_Join
            objATNonDisclouserDeclarationReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objATNonDisclouserDeclarationReport._Report_GroupName = mstrReport_GroupName

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmDisciplineOutcomeReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objATNonDisclouserDeclarationReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineOutcomeReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineOutcomeReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'Call OtherSettings()

            eZeeHeader.Title = objATNonDisclouserDeclarationReport._ReportName
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineOutcomeReport_Load", mstrModuleName)
        End Try
    End Sub

    'Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '    Try
    '        If e.Control Then
    '            If e.KeyCode = Windows.Forms.Keys.R Then
    '                Call btnExport_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Form_KeyDown", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("TAB")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "


    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim mac As String = GetMACAddress()
            If Not SetFilter() Then Exit Sub
            objATNonDisclouserDeclarationReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

            clsDisciplineOutComeReport.SetMessages()
            objfrm._Other_ModuleNames = "clsATNonDisclouserDeclarationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog("EM")
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAuditDateFrom.Text = Language._Object.getCaption(Me.lblAuditDateFrom.Name, Me.lblAuditDateFrom.Text)
			Me.lblAuditTo.Text = Language._Object.getCaption(Me.lblAuditTo.Name, Me.lblAuditTo.Text)
			Me.lblDeclarationDateFrom.Text = Language._Object.getCaption(Me.lblDeclarationDateFrom.Name, Me.lblDeclarationDateFrom.Text)
			Me.lblDeclarationTo.Text = Language._Object.getCaption(Me.lblDeclarationTo.Name, Me.lblDeclarationTo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region
    'Language & UI Settings
    '</Language>
End Class

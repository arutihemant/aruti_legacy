'************************************************************************************************************************************
'Class Name : clsManningLevel_Report.vb
'Purpose    :
'Date       : 05 April 2012
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class clsManningLevel_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsManningLevel_Report"
    Private mstrReportId As String = enArutiReport.ManningLevel_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintJobGroupId As Integer = -1
    Private mstrJobGroupName As String = String.Empty
    Private mintJobId As Integer = -1
    Private mstrJob_Name As String = String.Empty
    Private mintPlanned_From As Integer = -1
    Private mintPlanned_To As Integer = -1
    Private mintAvailable_From As Integer = -1
    Private mintAvailable_To As Integer = -1
    Private mintVariance_From As Integer = -1
    Private mintVariance_To As Integer = -1

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _JobGroupId() As Integer
        Set(ByVal value As Integer)
            mintJobGroupId = value
        End Set
    End Property

    Public WriteOnly Property _JobGroupName() As String
        Set(ByVal value As String)
            mstrJobGroupName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _Job_Name() As String
        Set(ByVal value As String)
            mstrJob_Name = value
        End Set
    End Property

    Public WriteOnly Property _Planned_From() As Integer
        Set(ByVal value As Integer)
            mintPlanned_From = value
        End Set
    End Property

    Public WriteOnly Property _Planned_To() As Integer
        Set(ByVal value As Integer)
            mintPlanned_To = value
        End Set
    End Property

    Public WriteOnly Property _Available_From() As Integer
        Set(ByVal value As Integer)
            mintAvailable_From = value
        End Set
    End Property

    Public WriteOnly Property _Available_To() As Integer
        Set(ByVal value As Integer)
            mintAvailable_To = value
        End Set
    End Property

    Public WriteOnly Property _Variance_From() As Integer
        Set(ByVal value As Integer)
            mintVariance_From = value
        End Set
    End Property

    Public WriteOnly Property _Variance_To() As Integer
        Set(ByVal value As Integer)
            mintVariance_To = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintJobGroupId = -1
            mstrJobGroupName = String.Empty
            mintJobId = -1
            mstrJob_Name = String.Empty
            mintPlanned_From = -1
            mintPlanned_To = -1
            mintAvailable_From = -1
            mintAvailable_To = -1
            mintVariance_From = -1
            mintVariance_To = -1
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintJobGroupId > 0 Then
                Me._FilterQuery &= "AND jobgroupunkid = @jobgroupunkid "
                objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupId)
                Me._FilterTitle = Language.getMessage(mstrModuleName, 1, "Job Group :") & " " & mstrJob_Name & " "
            End If

            If mintJobId > 0 Then
                Me._FilterQuery &= "AND jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterTitle = Language.getMessage(mstrModuleName, 2, "Job :") & " " & mstrJob_Name & " "
            End If

            If mintPlanned_From >= 0 AndAlso mintPlanned_To >= 0 Then
                objDataOperation.AddParameter("@Planned_From", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlanned_From)
                objDataOperation.AddParameter("@Planned_To", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlanned_To)
                Me._FilterQuery = "AND PLANNED BETWEEN @Planned_From AND @Planned_To "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Planned :") & " " & mintPlanned_From & " " & _
                                  Language.getMessage(mstrModuleName, 4, "To") & " " & mintPlanned_To & " "
            End If

            If mintAvailable_From >= 0 AndAlso mintAvailable_To >= 0 Then
                objDataOperation.AddParameter("@Available_From", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvailable_From)
                objDataOperation.AddParameter("@Available_To", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAvailable_To)
                Me._FilterQuery &= "AND AVAILABLE BETWEEN @Available_From AND @Available_To "
                Me._FilterTitle = Language.getMessage(mstrModuleName, 5, "Available :") & " " & mintAvailable_From & " " & _
                                  Language.getMessage(mstrModuleName, 4, "To") & " " & mintAvailable_To & " "
            End If

            If mintVariance_From >= 0 AndAlso mintVariance_To >= 0 Then
                objDataOperation.AddParameter("@Variance_From", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVariance_From)
                objDataOperation.AddParameter("@Variance_To", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVariance_To)
                Me._FilterQuery &= "AND (ISNULL(AVAILABLE,0) - ISNULL(PLANNED,0)) BETWEEN @Variance_From AND @Variance_To "
                Me._FilterTitle = Language.getMessage(mstrModuleName, 6, "Variance :") & " " & mintVariance_From & " " & _
                                  Language.getMessage(mstrModuleName, 4, "To") & " " & mintVariance_To & " "
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 7, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("GROUP_NAME", Language.getMessage(mstrModuleName, 8, "Job Group")))
            iColumn_DetailReport.Add(New IColumn("JOB_NAME", Language.getMessage(mstrModuleName, 9, "Job Name")))
            iColumn_DetailReport.Add(New IColumn("PLANNED", Language.getMessage(mstrModuleName, 10, "Planned Head Counts")))
            iColumn_DetailReport.Add(New IColumn("AVAILABLE", Language.getMessage(mstrModuleName, 11, "Available Head Counts")))
            iColumn_DetailReport.Add(New IColumn("(ISNULL(AVAILABLE,0) - ISNULL(PLANNED,0))", Language.getMessage(mstrModuleName, 12, "Variations")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '       "	 GROUP_NAME " & _
            '       "	,JOB_NAME " & _
            '       "	,PLANNED " & _
            '       "	,AVAILABLE " & _
            '       "	,ISNULL(AVAILABLE,0) - ISNULL(PLANNED,0) AS VARIATION " & _
            '       "FROM " & _
            '       "( " & _
            '       "	SELECT " & _
            '       "		 hrjobgroup_master.name AS GROUP_NAME " & _
            '       "		,hrjob_master.job_name AS JOB_NAME " & _
            '       "		,total_position AS PLANNED " & _
            '       "		,COUNT(employeeunkid) AS AVAILABLE " & _
            '       "		,hrjobgroup_master.jobgroupunkid " & _
            '       "		,hrjob_master.jobunkid " & _
            '       "	FROM hrjobgroup_master " & _
            '       "		JOIN hrjob_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
            '       "		JOIN hremployee_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '       "	GROUP BY hrjobgroup_master.name,hrjob_master.job_name,total_position,hrjobgroup_master.jobgroupunkid " & _
            '       "		,hrjob_master.jobunkid " & _
            '       ") AS A " & _
            '       "WHERE 1 = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '       "	 GROUP_NAME " & _
            '       "	,JOB_NAME " & _
            '       "	,PLANNED " & _
            '       "	,AVAILABLE " & _
            '       "	,ISNULL(AVAILABLE,0) - ISNULL(PLANNED,0) AS VARIATION " & _
            '       "FROM " & _
            '       "( " & _
            '       "	SELECT " & _
            '       "		 hrjobgroup_master.name AS GROUP_NAME " & _
            '       "		,hrjob_master.job_name AS JOB_NAME " & _
            '       "		,total_position AS PLANNED " & _
            '       "		,COUNT(employeeunkid) AS AVAILABLE " & _
            '       "		,hrjobgroup_master.jobgroupunkid " & _
            '       "		,hrjob_master.jobunkid " & _
            '       "	FROM hrjobgroup_master " & _
            '       "		JOIN hrjob_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
            '       "		LEFT JOIN hremployee_master ON hrjob_master.jobunkid = hremployee_master.jobunkid "

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            ''S.SANDEEP [ 28 JUN 2014 ] -- START
            ''"		JOIN hremployee_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " -- REMOVED
            ''"		LEFT JOIN hremployee_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " -- ADDED
            ''S.SANDEEP [ 28 JUN 2014 ] -- END


            ''S.SANDEEP [ 12 MAY 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END

            ''S.SANDEEP [ 12 MAY 2012 ] -- END


            StrQ = "SELECT " & _
                   "	 GROUP_NAME " & _
                   "	,JOB_NAME " & _
                   "	,PLANNED " & _
                   "	,AVAILABLE " & _
                   "	,ISNULL(AVAILABLE,0) - ISNULL(PLANNED,0) AS VARIATION " & _
                   "FROM " & _
                   "( " & _
                   "	SELECT " & _
                   "		 hrjobgroup_master.name AS GROUP_NAME " & _
                   "		,hrjob_master.job_name AS JOB_NAME " & _
                   "		,total_position AS PLANNED " & _
                   "		,COUNT(hremployee_master.employeeunkid) AS AVAILABLE " & _
                   "		,hrjobgroup_master.jobgroupunkid " & _
                   "		,hrjob_master.jobunkid " & _
                   "	FROM hrjobgroup_master " & _
                   "		JOIN hrjob_master ON hrjobgroup_master.jobgroupunkid = hrjob_master.jobgroupunkid " & _
                   "        LEFT JOIN " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 jobunkid " & _
                   "                ,employeeunkid " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "            FROM hremployee_categorization_tran " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        ) AS Jobs ON Jobs.jobunkid = hrjob_master.jobunkid AND Jobs.rno = 1 " & _
                   "        LEFT JOIN hremployee_master ON Jobs.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE hrjobgroup_master.isactive = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            'S.SANDEEP [04 JUN 2015] -- END



            StrQ &= "	GROUP BY hrjobgroup_master.name,hrjob_master.job_name,total_position,hrjobgroup_master.jobgroupunkid " & _
                   "		,hrjob_master.jobunkid " & _
                   ") AS A " & _
                   "WHERE 1 = 1 "
            'S.SANDEEP [ 12 MAY 2012 ] -- END


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GROUP_NAME")
                rpt_Rows.Item("Column2") = dtRow.Item("JOB_NAME")
                rpt_Rows.Item("Column3") = dtRow.Item("PLANNED")
                rpt_Rows.Item("Column4") = dtRow.Item("AVAILABLE")
                rpt_Rows.Item("Column5") = dtRow.Item("VARIATION")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptManningLevel_Report

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 13, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 14, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 15, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 16, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 17, "Job Group : "))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 9, "Job Name"))
            Call ReportFunction.TextChange(objRpt, "txtPlanned", Language.getMessage(mstrModuleName, 10, "Planned Head Counts"))
            Call ReportFunction.TextChange(objRpt, "txtAvailable", Language.getMessage(mstrModuleName, 11, "Available Head Counts"))
            Call ReportFunction.TextChange(objRpt, "txtVariation", Language.getMessage(mstrModuleName, 12, "Variations"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 20, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 21, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Job Group :")
            Language.setMessage(mstrModuleName, 2, "Job :")
            Language.setMessage(mstrModuleName, 3, "Planned :")
            Language.setMessage(mstrModuleName, 4, "To")
            Language.setMessage(mstrModuleName, 5, "Available :")
            Language.setMessage(mstrModuleName, 6, "Variance :")
            Language.setMessage(mstrModuleName, 7, " Order By :")
            Language.setMessage(mstrModuleName, 8, "Job Group")
            Language.setMessage(mstrModuleName, 9, "Job Name")
            Language.setMessage(mstrModuleName, 10, "Planned Head Counts")
            Language.setMessage(mstrModuleName, 11, "Available Head Counts")
            Language.setMessage(mstrModuleName, 12, "Variations")
            Language.setMessage(mstrModuleName, 13, "Prepared By :")
            Language.setMessage(mstrModuleName, 14, "Checked By :")
            Language.setMessage(mstrModuleName, 15, "Approved By :")
            Language.setMessage(mstrModuleName, 16, "Received By :")
            Language.setMessage(mstrModuleName, 17, "Job Group :")
            Language.setMessage(mstrModuleName, 18, "Printed By :")
            Language.setMessage(mstrModuleName, 19, "Printed Date :")
            Language.setMessage(mstrModuleName, 20, "Page :")
            Language.setMessage(mstrModuleName, 21, "Sub Total :")
            Language.setMessage(mstrModuleName, 22, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

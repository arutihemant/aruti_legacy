#Region " Imports "

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Text
Imports System.Windows.Forms

Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region


Public Class frmEmployeeIdentitiesReport

#Region "Private Variables"

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    'Private ReadOnly mstrModuleName As String = "frmEmployeeIdentities"
    Private ReadOnly mstrModuleName As String = "frmEmployeeIdentitiesReport"
    'S.SANDEEP [ 19 JUN 2014 ] -- END
    Private objEmpIdentity As clsEmployeeIdentitiesReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objEmpIdentity = New clsEmployeeIdentitiesReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpIdentity.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region "Public Functions"

    Public Sub ResetValue()
        Try
            mstrAdvanceFilter = ""
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            cboEmployee.SelectedValue = 0
            dtpExpiryDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpExpiryDateFrom.Checked = False
            dtpExpiryDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpExpiryDateTo.Checked = False
            dtpIssueDateFrom.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpIssueDateFrom.Checked = False
            dtpIssueDateTo.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpIssueDateTo.Checked = False

            'Pinkal (19-AUG-2014) -- Start
            'Enhancement - Added Include Inactive Employee
            chkInactiveemp.Checked = False
            'Pinkal (19-AUG-2014) -- End

            objEmpIdentity.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpIdentity.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet = Nothing
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmployee.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            dsList = Nothing
        End Try
    End Sub

    Public Function Validation() As Boolean
        Dim blnFlag As Boolean = False
        Try

            If dtpExpiryDateFrom.Checked = True AndAlso dtpExpiryDateTo.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expiry Date range are compulsory information.Please set both Expiry Date range."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Function

            ElseIf dtpExpiryDateFrom.Checked = False AndAlso dtpExpiryDateTo.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Expiry Date range are compulsory information.Please set both Expiry Date range."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Function

            ElseIf dtpIssueDateFrom.Checked = False AndAlso dtpIssueDateTo.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Issue Date range are compulsory information.Please set both Issue Date range."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Function

            ElseIf dtpIssueDateFrom.Checked = False AndAlso dtpIssueDateTo.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Issue Date range are compulsory information.Please set both Issue Date range."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Function
            End If
            blnFlag = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return blnFlag
    End Function

    Public Function SetFilter() As Boolean
        Try
            objEmpIdentity.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objEmpIdentity._EmployeeId = CInt(cboEmployee.SelectedValue)
                objEmpIdentity._Employee = cboEmployee.Text
            End If

            If dtpExpiryDateFrom.Checked AndAlso dtpExpiryDateTo.Checked Then
                objEmpIdentity._ExpiryDateFrom = dtpExpiryDateFrom.Value.Date
                objEmpIdentity._ExpiryDateTo = dtpExpiryDateTo.Value.Date
            End If

            If dtpIssueDateFrom.Checked AndAlso dtpIssueDateTo.Checked Then
                objEmpIdentity._IssueDateFrom = dtpIssueDateFrom.Value.Date
                objEmpIdentity._IssueDateTo = dtpIssueDateTo.Value.Date
            End If

            objEmpIdentity._Advance_Filter = mstrAdvanceFilter

            objEmpIdentity._ViewByIds = mstrStringIds
            objEmpIdentity._ViewIndex = mintViewIdx
            objEmpIdentity._ViewByName = mstrStringName
            objEmpIdentity._Analysis_Fields = mstrAnalysis_Fields
            objEmpIdentity._Analysis_Join = mstrAnalysis_Join
            objEmpIdentity._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpIdentity._Report_GroupName = mstrReport_GroupName

            'Pinkal (19-AUG-2014) -- Start
            'Enhancement - Added Include Inactive Employee
            objEmpIdentity._IsActive = chkInactiveemp.Checked
            'Pinkal (19-AUG-2014) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region "Form's Events"

    Private Sub frmEmployeeIdentitiesReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpIdentity = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 19 JUN 2014 ] -- END
            Me._Title = objEmpIdentity._ReportName
            Me._Message = objEmpIdentity._ReportDesc
            Call ResetValue()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeIdentitiesReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            clsEmployeeIdentitiesReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeIdentitiesReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_Report_Click(ByVal sender As System.Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles MyBase.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmpIdentity.generateReport(0, e.Type, enExportAction.None)
                objEmpIdentity.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                 ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, _
                                                 0, e.Type, enExportAction.None)
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmpIdentity.generateReport(0, enPrintAction.None, e.Type)
                objEmpIdentity.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                 ConfigParameter._Object._ExportReportPath, _
                                                 ConfigParameter._Object._OpenAfterExport, _
                                                 0, enPrintAction.None, e.Type)
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeIdentitiesReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeIdentitiesReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpIdentity.setOrderBy(0)
            txtOrderBy.Text = objEmpIdentity.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblIssueDateTo.Text = Language._Object.getCaption(Me.LblIssueDateTo.Name, Me.LblIssueDateTo.Text)
			Me.LblIssueDateFrom.Text = Language._Object.getCaption(Me.LblIssueDateFrom.Name, Me.LblIssueDateFrom.Text)
			Me.LblExpiryDateTo.Text = Language._Object.getCaption(Me.LblExpiryDateTo.Name, Me.LblExpiryDateTo.Text)
			Me.LblExpiryDateFrom.Text = Language._Object.getCaption(Me.LblExpiryDateFrom.Name, Me.LblExpiryDateFrom.Text)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
                        Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expiry Date range are compulsory information.Please set both Expiry Date range.")
			Language.setMessage(mstrModuleName, 2, "Issue Date range are compulsory information.Please set both Issue Date range.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'************************************************************************************************************************************
'Class Name : clsEmployeeSkills.vb
'Purpose    :
'Date       :01/08/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployeeAgeAnalysisReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeAgeAnalysisReport"
    Private mstrReportId As String = enArutiReport.EmployeeAgeAnalysis '28
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrEmployeeCode As String = String.Empty
    Private mdtAsOnDate As DateTime = Nothing

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mblnIsAppointmentDate As Boolean = False
    'Anjan (20 Mar 2012)-End 

    'S.SANDEEP [ 24 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintStartingYear As Integer = 0
    Private mintEndingYear As Integer = 0
    Private mintYearGap As Integer = 0
    'S.SANDEEP [ 24 MARCH 2012 ] -- END


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrUserAccessFilter As String = String.Empty
    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'S.SANDEEP [ 22 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrCondition As String = String.Empty
    Private mintAgeFilter As Integer = 0
    'S.SANDEEP [ 22 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _DateAsOn() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Public WriteOnly Property _IsFromAppointmentDate() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAppointmentDate = value
        End Set
    End Property
    'Anjan (20 Mar 2012)-End

    'S.SANDEEP [ 24 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _StartingYear() As Integer
        Set(ByVal value As Integer)
            mintStartingYear = value
        End Set
    End Property

    Public WriteOnly Property _EndingYear() As Integer
        Set(ByVal value As Integer)
            mintEndingYear = value
        End Set
    End Property

    Public WriteOnly Property _YearGap() As Integer
        Set(ByVal value As Integer)
            mintYearGap = value
        End Set
    End Property
    'S.SANDEEP [ 24 MARCH 2012 ] -- END


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'Pinkal (14-Aug-2012) -- End



    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (12-Nov-2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    'S.SANDEEP [ 22 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Condition() As String
        Set(ByVal value As String)
            mstrCondition = value
        End Set
    End Property

    Public WriteOnly Property _AgeFilter() As Integer
        Set(ByVal value As Integer)
            mintAgeFilter = value
        End Set
    End Property
    'S.SANDEEP [ 22 FEB 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mstrEmployeeCode = ""
            mdtAsOnDate = Nothing
            mintViewIndex = -1
            mstrViewByIds = ""
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End
            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End
            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintStartingYear = 0
            mintEndingYear = 0
            mintYearGap = 0
            mblnIsAppointmentDate = False
            'S.SANDEEP [ 24 MARCH 2012 ] -- END

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrCondition = String.Empty
            mintAgeFilter = 0
            'S.SANDEEP [ 22 FEB 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAsOnDate.AddDays(1)))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "As On Date :") & " " & mdtAsOnDate.Date & " "


            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrCondition.Trim.Length > 0 AndAlso mintAgeFilter >= 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "Year : ") & " " & mintAgeFilter & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Condition : ") & " " & mstrCondition & " "
                'S.SANDEEP [ 15 OCT 2013 ] -- START
                'ENHANCEMENT : ENHANCEMENT
                'Me._FilterQuery &= "AND (((YEAR(@Date) * 12 + MONTH(@Date)) - (YEAR(birthdate) * 12+ MONTH(birthdate)))/12) " & mstrCondition & " " & mintAgeFilter
                If mblnIsAppointmentDate = True Then
                    Me._FilterQuery &= "AND ((CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),appointeddate,112) AS INT))/10000) " & mstrCondition & " " & mintAgeFilter & " "
                Else
                    Me._FilterQuery &= "AND ((CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000) " & mstrCondition & " " & mintAgeFilter & " "
                End If
                'S.SANDEEP [ 15 OCT 2013 ] -- END
            End If
            'S.SANDEEP [ 22 FEB 2013 ] -- END

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Me._FilterQuery &= "AND employeeunkid = @EmployeeId "
                Me._FilterQuery &= "AND hremployee_master.employeeunkid = @EmployeeId "
                'S.SANDEEP [04 JUN 2015] -- END
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mstrEmployeeCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmployeeCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmployeeCode & "%")
                'Sohail (16 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= "AND Code LIKE @EmployeeCode "
                Me._FilterQuery &= "AND employeecode LIKE @EmployeeCode "
                'Sohail (16 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee Code :") & " " & mstrEmployeeCode & " "
            End If


            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If mblnIsAppointmentDate = True Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Report By Appointment Date") & " "
            Else
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Report By Birthdate") & " "
            End If
            'Anjan (20 Mar 2012)-End 

            If mstrViewByName.Length > 0 Then
                'Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Analysis By :") & " " & mstrViewByName & " " 'Sohail (16 May 2012)
            End If


            'Shani [ 22 NOV 2014 ] -- START
            'Enhancement - Included Gender Field  
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            'Shani [ 22 NOV 2014 ] -- END


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes
        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END




    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'iColumn_DetailReport.Add(New IColumn("ECode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            'iColumn_DetailReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            'iColumn_DetailReport.Add(New IColumn("OAppDate", Language.getMessage(mstrModuleName, 3, "Appointed Date")))
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 3, "Appointed Date")))
            'Sohail (16 May 2012) -- End

            'Shani [ 22 NOV 2014 ] -- START
            'Enhancement - Included Gender Field 
            iColumn_DetailReport.Add(New IColumn("CASE WHEN gender = 1 THEN @male " & _
                                                   " WHEN gender = 2 THEN @female ELSE '' END ", Language.getMessage(mstrModuleName, 29, "Gender")))

            'Shani [ 22 NOV 2014 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ = "SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= "  stationunkid , name AS GName " & _
            '                    "  ,ISNULL(ECode,'') AS ECode " & _
            '                    "  ,ISNULL(EName,'') AS EName " & _
            '                    "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                    "  ,ISNULL(Years,'') AS Years " & _
            '                    "  ,ISNULL(Months,'') AS Months " & _
            '                    "  ,ISNULL(Days,'') AS Days " & _
            '                    "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM hrstation_master " & _
            '                    " LEFT JOIN " & _
            '                    " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Department
            '        StrQ &= "  departmentunkid , name AS GName " & _
            '                  "  ,ISNULL(ECode,'') AS ECode " & _
            '                  "  ,ISNULL(EName,'') AS EName " & _
            '                  "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                  "  ,ISNULL(Years,'') AS Years " & _
            '                  "  ,ISNULL(Months,'') AS Months " & _
            '                  "  ,ISNULL(Days,'') AS Days " & _
            '                  "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM hrdepartment_master " & _
            '                  " LEFT JOIN " & _
            '                  " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Section
            '        StrQ &= "  sectionunkid , name AS GName " & _
            '                  "  ,ISNULL(ECode,'') AS ECode " & _
            '                  "  ,ISNULL(EName,'') AS EName " & _
            '                  "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                  "  ,ISNULL(Years,'') AS Years " & _
            '                  "  ,ISNULL(Months,'') AS Months " & _
            '                  "  ,ISNULL(Days,'') AS Days " & _
            '                  "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM hrsection_master " & _
            '                  " LEFT JOIN " & _
            '                  " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Unit
            '        StrQ &= "  unitunkid , name AS GName " & _
            '                  "  ,ISNULL(ECode,'') AS ECode " & _
            '                  "  ,ISNULL(EName,'') AS EName " & _
            '                  "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                  "  ,ISNULL(Years,'') AS Years " & _
            '                  "  ,ISNULL(Months,'') AS Months " & _
            '                  "  ,ISNULL(Days,'') AS Days " & _
            '                  "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM hrunit_master " & _
            '                  " LEFT JOIN " & _
            '                  " ( "
            '        blnFlag = True
            '    Case enAnalysisReport.Job
            '        StrQ &= "  jobunkid , job_name AS GName " & _
            '                  "  ,ISNULL(ECode,'') AS ECode " & _
            '                  "  ,ISNULL(EName,'') AS EName " & _
            '                  "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                  "  ,ISNULL(Years,'') AS Years " & _
            '                  "  ,ISNULL(Months,'') AS Months " & _
            '                  "  ,ISNULL(Days,'') AS Days " & _
            '                  "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM hrjob_master " & _
            '                  " LEFT JOIN " & _
            '                  " ( "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " costcenterunkid  , costcentername AS GName " & _
            '                  "  ,ISNULL(ECode,'') AS ECode " & _
            '                  "  ,ISNULL(EName,'') AS EName " & _
            '                  "  ,ISNULL(AppDate,'') AS AppDate " & _
            '                  "  ,ISNULL(Years,'') AS Years " & _
            '                  "  ,ISNULL(Months,'') AS Months " & _
            '                  "  ,ISNULL(Days,'') AS Days " & _
            '                  "  ,ISNULL(OAppDate,'') AS OAppDate "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(birthdate,'') as birthdate " & _
            '                ",ISNULL(retirementdate,'') as retirementdate "
            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= " FROM prcostcenter_master " & _
            '                  " LEFT JOIN " & _
            '                  " ( "
            '        blnFlag = True
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        'StrQ = "SELECT " & _
            '        '                             " 0 AS Id , '' AS GName " & _
            '        '                             ",Code AS ECode " & _
            '        '          ",EName AS EName " & _
            '        '          ",AppDate AS AppDate " & _
            '        '          ",Years AS Years " & _
            '        '          ",CASE WHEN Years < 0 THEN 0 ELSE Months END AS Months " & _
            '        '          ",CASE WHEN Years < 0 THEN 0 ELSE Days END AS Days " & _
            '        '          ",OAppDate AS OAppDate " & _
            '        '          ",employeeunkid AS employeeunkid " & _
            '        '        "FROM " & _
            '        '        "( " & _
            '        '             "SELECT " & _
            '        '                   "ISNULL(employeecode,'') AS Code " & _
            '        '                  ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '        '                  ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
            '        '                  ",appointeddate AS OAppDate " & _
            '        '                  ",employeeunkid AS employeeunkid " & _
            '        '                  ",CASE WHEN YEAR(@Date) - YEAR(appointeddate) <=0 THEN 0 ELSE YEAR(@Date) - YEAR(appointeddate) END AS Years " & _
            '        '                  ",CASE WHEN MONTH(@Date) - MONTH(appointeddate) <=0 THEN 0 ELSE MONTH(@Date) - MONTH(appointeddate) END  AS Months " & _
            '        '                  ",CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN 0 ELSE DAY(@Date) - DAY(appointeddate) END AS Days " & _
            '        '             "FROM hremployee_master WHERE isactive = 1 " & _
            '        '        ") AS Age " & _
            '        '        "WHERE 1 = 1 "


            '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
            '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            '        'StrQ = "SELECT " & _
            '        '          " 0 AS Id , '' AS GName " & _
            '        '          ",Code AS ECode " & _
            '        '          ",EName AS EName " & _
            '        '          ",AppDate AS AppDate " & _
            '        '          ",Years AS Years " & _
            '        '          ",CASE WHEN Years < 0 THEN 0 ELSE Months END AS Months " & _
            '        '          ",CASE WHEN Years < 0 THEN 0 ELSE Days END AS Days " & _
            '        '          ",OAppDate AS OAppDate " & _
            '        '          ",employeeunkid AS employeeunkid " & _
            '        '        "FROM " & _
            '        '        "( " & _
            '        '             "SELECT " & _
            '        '                   "ISNULL(employeecode,'') AS Code " & _
            '        '                  ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '        '                  ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
            '        '                  ",appointeddate AS OAppDate " & _
            '        '                  ",employeeunkid AS employeeunkid " & _
            '        '                  ",CASE WHEN YEAR(@Date) - YEAR(appointeddate) <=0 THEN 0 ELSE YEAR(@Date) - YEAR(appointeddate) END AS Years " & _
            '        '                  ",CASE WHEN MONTH(@Date) - MONTH(appointeddate) <=0 THEN 0 ELSE MONTH(@Date) - MONTH(appointeddate) END  AS Months " & _
            '        '                  ",CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN 0 ELSE DAY(@Date) - DAY(appointeddate) END AS Days " & _
            '        '                     "FROM hremployee_master "
            'StrQ = "SELECT " & _
            '                             " 0 AS Id , '' AS GName " & _
            '                             ",Code AS ECode " & _
            '          ",EName AS EName " & _
            '          ",AppDate AS AppDate " & _
            '          ",Years AS Years " & _
            '                ",Months AS Months " & _
            '                ",Days AS Days " & _
            '          ",OAppDate AS OAppDate " & _
            '          ",employeeunkid AS employeeunkid " & _
            '                ",birthdate as birthdate " & _
            '                ",retirementdate as retirementdate " & _
            '        "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "ISNULL(employeecode,'') AS Code " & _
            '                  ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '                  ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
            '                  ",appointeddate AS OAppDate " & _
            '                         ",employeeunkid AS employeeunkid "

            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        StrQ &= ",ISNULL(CONVERT(CHAR(8),birthdate,112),'') as birthdate " & _
            '                ",ISNULL(CONVERT(CHAR(8),termination_to_date,112),'') as retirementdate  "
            '        'Anjan (20 Mar 2012)-End 


            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        If mblnIsAppointmentDate = True Then
            '            StrQ &= " ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate)))/12 AS Years " & _
            '                                                 ",((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))))/12)*12) AS Months " & _
            '                                                 ",CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN DAY(appointeddate) - DAY(@Date) ELSE DAY(@Date) - DAY(appointeddate) END AS Days "
            '        Else
            '            StrQ &= " ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate)))/12 AS Years " & _
            '                         ",((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))))/12)*12) AS Months " & _
            '                         ",CASE WHEN DAY(@Date) - DAY(birthdate) <= 0 THEN DAY(birthdate) - DAY(@Date) ELSE DAY(@Date) - DAY(birthdate) END AS Days "
            '        End If

            '        'Anjan (20 Mar 2012)-End 

            '        StrQ &= "FROM hremployee_master "
            '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 



            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        StrQ &= " WHERE 1 = 1 "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END



            '        If mblnIsActive = False Then
            '            'S.SANDEEP [ 12 MAY 2012 ] -- START
            '            'ISSUE : TRA ENHANCEMENTS
            '            'StrQ &= " WHERE  hremployee_master.isactive = 1"
            '            StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                    " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '            'S.SANDEEP [ 12 MAY 2012 ] -- END
            '        End If

            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= UserAccessLevel._AccessLevelFilterString
            '        End If
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END

            '        StrQ &= ") AS Age " & _
            '        "WHERE 1 = 1 "


            '        'Anjan (20 Mar 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        If mblnIsAppointmentDate = False Then
            '            StrQ &= " AND birthdate <> '' "
            '        End If
            '        'Anjan (20 Mar 2012)-End 


            '        blnFlag = False
            'End Select

            'If blnFlag = True Then

            '    'S.SANDEEP [ 24 JUNE 2011 ] -- START
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            '    'StrQ &= "SELECT " & _
            '    '                " Code AS ECode " & _
            '    '                ",EName AS EName " & _
            '    '                ",AppDate AS AppDate " & _
            '    '                ",Years AS Years " & _
            '    '                ",CASE WHEN Years < 0 THEN 0 ELSE Months END AS Months " & _
            '    '                ",CASE WHEN Years < 0 THEN 0 ELSE Days END AS Days " & _
            '    '                ",OAppDate AS OAppDate " & _
            '    '                ",employeeunkid AS employeeunkid "
            '    StrQ &= "SELECT " & _
            '                    " Code AS ECode " & _
            '          ",EName AS EName " & _
            '          ",AppDate AS AppDate " & _
            '          ",Years AS Years " & _
            '                    ",Months AS Months " & _
            '                    ",Days AS Days " & _
            '          ",OAppDate AS OAppDate " & _
            '                    ",employeeunkid AS employeeunkid "

            '    'Anjan (20 Mar 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    StrQ &= ",birthdate as birthdate " & _
            '            ",retirementdate as retirementdate "
            '    'Anjan (20 Mar 2012)-End 

            '    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", BranchId  AS BranchId "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", DeptId  AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", SecId  AS SecId "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " , UnitId AS  UnitId "
            '        Case enAnalysisReport.Job
            '            StrQ &= " , JobId AS  JobId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", CCId As CCId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select

            '    StrQ &= "FROM " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "ISNULL(employeecode,'') AS Code " & _
            '                  ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '                  ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
            '                  ",appointeddate AS OAppDate " & _
            '                               ",employeeunkid AS employeeunkid "


            '    'Anjan (20 Mar 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    StrQ &= ",ISNULL(CONVERT(CHAR(8),birthdate,112),'') as birthdate " & _
            '            ",ISNULL(CONVERT(CHAR(8),termination_to_date,112),'') as retirementdate "
            '    'Anjan (20 Mar 2012)-End 

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= ", hremployee_master.stationunkid AS BranchId "
            '        Case enAnalysisReport.Department
            '            StrQ &= ", hremployee_master.departmentunkid AS DeptId "
            '        Case enAnalysisReport.Section
            '            StrQ &= ", hremployee_master.sectionunkid AS SecId  "
            '        Case enAnalysisReport.Unit
            '            StrQ &= ", hremployee_master.unitunkid AS UnitId  "
            '        Case enAnalysisReport.Job
            '            StrQ &= ", hremployee_master.jobunkid AS JobId  "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= ", hremployee_master.costcenterunkid As CCId "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select
            '    'StrQ &= ",CASE WHEN YEAR(@Date) - YEAR(appointeddate) <=0 THEN 0 ELSE YEAR(@Date) - YEAR(appointeddate) END AS Years " & _
            '    '               ",CASE WHEN MONTH(@Date) - MONTH(appointeddate) <=0 THEN 0 ELSE MONTH(@Date) - MONTH(appointeddate) END  AS Months " & _
            '    '               ",CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN 0 ELSE DAY(@Date) - DAY(appointeddate) END AS Days " & _
            '    '          "FROM hremployee_master WHERE isactive = 1 " & _
            '    '     ") AS Age "


            '    'S.SANDEEP [ 24 JUNE 2011 ] -- START
            '    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            '    'StrQ &= ",CASE WHEN YEAR(@Date) - YEAR(appointeddate) <=0 THEN 0 ELSE YEAR(@Date) - YEAR(appointeddate) END AS Years " & _
            '    '                        ",CASE WHEN MONTH(@Date) - MONTH(appointeddate) <=0 THEN 0 ELSE MONTH(@Date) - MONTH(appointeddate) END  AS Months " & _
            '    '                        ",CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN 0 ELSE DAY(@Date) - DAY(appointeddate) END AS Days " & _
            '    '                        "FROM hremployee_master "

            '    'Anjan (20 Mar 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    If mblnIsAppointmentDate = True Then
            '        StrQ &= "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate)))/12 AS Years " & _
            '                "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))))/12)*12) AS Months " & _
            '                "   ,CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN DAY(appointeddate) - DAY(@Date) ELSE DAY(@Date) - DAY(appointeddate) END AS Days "
            '    Else
            '        StrQ &= "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate)))/12 AS Years " & _
            '                         ",((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))))/12)*12) AS Months " & _
            '                         ",CASE WHEN DAY(@Date) - DAY(birthdate) <= 0 THEN DAY(birthdate) - DAY(@Date) ELSE DAY(@Date) - DAY(birthdate) END AS Days "
            '    End If


            '        'Anjan (20 Mar 2012)-End 

            '    StrQ &= "FROM hremployee_master "
            '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 




            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    StrQ &= " WHERE 1 = 1 "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    If mblnIsActive = False Then
            '        'S.SANDEEP [ 12 MAY 2012 ] -- START
            '        'ISSUE : TRA ENHANCEMENTS
            '        'StrQ &= " WHERE hremployee_master.isactive = 1 "
            '        StrQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '                " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "
            '        'S.SANDEEP [ 12 MAY 2012 ] -- END
            '    End If


            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '        End If
            '    'S.SANDEEP [ 12 MAY 2012 ] -- END

            '    StrQ &= ") AS Age "

            '    Select Case mintViewIndex
            '        Case enAnalysisReport.Branch
            '            StrQ &= " )AS EAS ON hrstation_master.stationunkid = EAS.BranchId " & _
            '            " WHERE hrstation_master.stationunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Department
            '            StrQ &= " )AS EAS ON hrdepartment_master.departmentunkid = EAS.DeptId " & _
            '            " WHERE hrdepartment_master.departmentunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Section
            '            StrQ &= " )AS EAS ON hrsection_master.sectionunkid = EAS.SecId " & _
            '            " WHERE hrsection_master.sectionunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Unit
            '            StrQ &= " )AS EAS ON hrunit_master.unitunkid = EAS.UnitId " & _
            '            " WHERE hrunit_master.unitunkid IN ( " & mstrViewByIds & " ) "
            '        Case enAnalysisReport.Job
            '            StrQ &= " )AS EAS ON hrjob_master.jobunkid = EAS.JobId " & _
            '            " WHERE hrjob_master.jobunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- START
            '        Case enAnalysisReport.CostCenter
            '            StrQ &= " ) AS EQS ON prcostcenter_master.costcenterunkid = EQS.CCId " & _
            '                          " WHERE prcostcenter_master.costcenterunkid IN ( " & mstrViewByIds & " ) "
            '            'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    End Select

            '    'Anjan (20 Mar 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    If mblnIsAppointmentDate = False Then
            '        StrQ &= " AND birthdate <> '' "
            '    End If
            '    'Anjan (20 Mar 2012)-End 

            'End If



            'Gajanan [1-July-2020] -- Start
            'Issue Display Wrong Age
            StrQ = "IF OBJECT_ID('tempdb..#tmp_empyear') IS NOT NULL " & _
                "DROP TABLE #tmp_empyear " & _
                "CREATE TABLE #tmp_empyear " & _
                "( " & _
                     "employeeunkid int, " & _
                     "eyear int, " & _
                     "yearTempDate datetime, " & _
                     "dob datetime, " & _
                ") " & _
                "INSERT INTO #tmp_empyear " & _
                     "SELECT " & _
                          "employeeunkid " & _
                        ",DATEDIFF(yy, #dateColumn#, @Date) - " & _
                          "CASE " & _
                               "WHEN " & _
                                    "(MONTH(#dateColumn#) > MONTH(@Date)) OR " & _
                                    "(MONTH(#dateColumn#) = MONTH(@Date) AND " & _
                                    "DAY(#dateColumn#) > DAY(@Date)) THEN 1 " & _
                               "ELSE 0 " & _
                          "END " & _
                        ",DATEADD(yy, DATEDIFF(yy, #dateColumn#, @Date) - " & _
                          "CASE " & _
                               "WHEN " & _
                                    "(MONTH(#dateColumn#) > MONTH(@Date)) OR " & _
                                    "(MONTH(#dateColumn#) = MONTH(@Date) AND " & _
                                    "DAY(birthdate) > DAY(@Date)) THEN 1 " & _
                               "ELSE 0 " & _
                          "END " & _
                          ", #dateColumn#) " & _
                        ",#dateColumn# " & _
                     "FROM hremployee_master " & _
                     " Where 1=1 "

            If mblnIsAppointmentDate = False Then
                StrQ &= " AND birthdate <> '' "
            End If
            StrQ &= " #strFilter# "

            StrQ &= "IF OBJECT_ID('tempdb..#tmp_empmonth') IS NOT NULL " & _
                "DROP TABLE #tmp_empmonth " & _
                "CREATE TABLE #tmp_empmonth " & _
                "( " & _
                "   employeeunkid int, " & _
                "   emonth int, " & _
                "   eyear int, " & _
                "   monthTempDate datetime " & _
                ") " & _
                "INSERT INTO #tmp_empmonth " & _
                     "SELECT " & _
                          "employeeunkid " & _
                        ",DATEDIFF(m, #tmp_empyear.yearTempDate, @Date) - " & _
                          "CASE " & _
                               "WHEN " & _
                                    "DAY(#tmp_empyear.dob) > DAY(@Date) THEN 1 " & _
                               "ELSE 0 " & _
                          "END " & _
                        ",#tmp_empyear.eyear " & _
                        ",DATEADD(m, DATEDIFF(m, #tmp_empyear.yearTempDate, @Date) - " & _
                          "CASE " & _
                               "WHEN " & _
                                    "DAY(#tmp_empyear.dob) > DAY(@Date) THEN 1 " & _
                               "ELSE 0 " & _
                          "END, #tmp_empyear.yearTempDate) " & _
                     "FROM #tmp_empyear " & _
                "DROP TABLE #tmp_empyear " & _
                "IF OBJECT_ID('tempdb..#tmp_empage') IS NOT NULL " & _
                "DROP TABLE #tmp_empage " & _
                "CREATE TABLE #tmp_empage " & _
                "( " & _
                "employeeunkid int, " & _
                "eyear int, " & _
                "emonth int, " & _
                "eday int, " & _
                ") " & _
                "INSERT INTO #tmp_empage " & _
                     "SELECT " & _
                          "#tmp_empmonth.employeeunkid " & _
                        ",#tmp_empmonth.eyear " & _
                        ",#tmp_empmonth.emonth " & _
                        ",DATEDIFF(d, #tmp_empmonth.monthTempDate, @Date) " & _
                     "FROM #tmp_empmonth " & _
                " " & _
                "DROP TABLE #tmp_empmonth "
            'Gajanan [1-July-2020] -- End




            StrQ &= "SELECT  ISNULL(employeecode, '') AS ECode "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " 
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(surname, '') + ' ' " & _
                            "+ ISNULL(firstname, '') + ' ' " & _
                            "+ ISNULL(othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(firstname, '') + ' ' " & _
                            "+ ISNULL(othername, '') + ' ' " & _
                            "+ ISNULL(surname, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
            '        ",appointeddate AS OAppDate " & _
            '        ",hremployee_master.employeeunkid AS employeeunkid " & _
            '        ",ISNULL(CONVERT(CHAR(8), birthdate, 112), '') AS birthdate " & _
            '        ",ISNULL(CONVERT(CHAR(8), termination_to_date, 112), '') AS retirementdate "

            StrQ &= ",CONVERT(CHAR(8),appointeddate,112) AS AppDate " & _
                    ",appointeddate AS OAppDate " & _
                    ",hremployee_master.employeeunkid AS employeeunkid " & _
                    ",ISNULL(CONVERT(CHAR(8), birthdate, 112), '') AS birthdate " & _
                    ",ISNULL(CONVERT(CHAR(8), RT.termination_to_date, 112), '') AS retirementdate "
            'S.SANDEEP [04 JUN 2015] -- END



            'S.SANDEEP [ 15 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            'If mblnIsAppointmentDate = True Then
            '    StrQ &= "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate)))/12 AS Years " & _
            '            "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(appointeddate)*12+MONTH(appointeddate))))/12)*12) AS Months " & _
            '            "   ,CASE WHEN DAY(@Date) - DAY(appointeddate) <= 0 THEN DAY(appointeddate) - DAY(@Date) ELSE DAY(@Date) - DAY(appointeddate) END AS Days "
            'Else
            '    StrQ &= "   ,((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate)))/12 AS Years " & _
            '                     ",((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))) - (((((YEAR(@Date)*12+MONTH(@Date)) - (YEAR(birthdate)*12+MONTH(birthdate))))/12)*12) AS Months " & _
            '                     ",CASE WHEN DAY(@Date) - DAY(birthdate) <= 0 THEN DAY(birthdate) - DAY(@Date) ELSE DAY(@Date) - DAY(birthdate) END AS Days "
            'End If

            'Gajanan [1-July-2020] -- Start
            'Issue Display Wrong Age

            'If mblnIsAppointmentDate = True Then
            '    StrQ &= ",(CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),appointeddate,112) AS INT))/10000 AS Years " & _
            '            ",(DATEDIFF(MONTH,appointeddate,@Date)+12)%12 - CASE WHEN DAY(appointeddate)>DAY(@Date) THEN 1 ELSE 0 END AS Months " & _
            '            ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,appointeddate,@Date)+12)%12 - CASE WHEN DAY(appointeddate)>DAY(@Date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),appointeddate,112) AS INT))/10000,appointeddate)),@Date) AS Days "
            'Else
            '    StrQ &= ",(CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000 AS Years " & _
            '            ",(DATEDIFF(MONTH,birthdate,@Date)+12)%12 - CASE WHEN DAY(birthdate)>DAY(@Date) THEN 1 ELSE 0 END AS Months " & _
            '            ",DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,birthdate,@Date)+12)%12 - CASE WHEN DAY(birthdate)>DAY(@Date) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),@Date,112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000,birthdate)),@Date) AS Days "
            'End If

            StrQ &= ",Empage.eyear AS Years " & _
                 ",Empage.emonth AS Months " & _
                 ",Empage.eday AS Days "

            If mblnIsAppointmentDate = True Then
                StrQ = StrQ.Replace("#dateColumn#", "appointeddate")
            Else
                StrQ = StrQ.Replace("#dateColumn#", "birthdate")
            End If
            'Gajanan [1-July-2020] -- End

            'S.SANDEEP [ 15 OCT 2013 ] -- END


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'Shani [ 22 NOV 2014 ] -- START
            'Enhancement - Included Gender Field 
            StrQ &= ",CASE WHEN gender = 1 THEN @male WHEN gender = 2 THEN @female ELSE '' END AS gender "
            'Shani [ 22 NOV 2014 ] -- END

            StrQ &= "FROM hremployee_master "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END


            'Gajanan [1-July-2020] -- Start
            'Issue Display Wrong Age
            StrQ &= "LEFT JOIN #tmp_empage AS Empage " & _
                    " ON Empage.employeeunkid = hremployee_master.employeeunkid "
            'Gajanan [1-July-2020] -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         date1 AS termination_to_date " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= mstrAnalysis_Join

            StrQ &= " WHERE 1 = 1 "

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            ''Pinkal (12-Nov-2012) -- Start
            ''Enhancement : TRA Changes

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END




            'Pinkal (12-Nov-2012) -- End

            If mblnIsAppointmentDate = False Then
                StrQ &= " AND birthdate <> '' "
            End If
            'Sohail (16 May 2012) -- End


            'Pinkal (24-Jun-2011) -- End
            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            'Gajanan [1-July-2020] -- Start
            'Issue Display Wrong Age
            StrQ = StrQ.Replace("#strFilter#", Me._FilterQuery)
            StrQ &= " DROP TABLE #tmp_empage "
            'Gajanan [1-July-2020] -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If



            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'rpt_Data = New ArutiReport.Designer.dsArutiReport

            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
            '    Dim rpt_Rows As DataRow
            '    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
            '    'S.SANDEEP [ 23 SEP 2011 ] -- START
            '    'ENHANCEMENT : CODE OPTIMIZATION
            '    If dtRow.Item("ECode").ToString.Trim.Length <= 0 Then Continue For
            '    'S.SANDEEP [ 23 SEP 2011 ] -- END
            '    rpt_Rows.Item("Column1") = dtRow.Item("ECode")
            '    rpt_Rows.Item("Column2") = dtRow.Item("EName")
            '    If dtRow.Item("AppDate").ToString.Trim <> "" Then
            '        rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("AppDate").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column3") = ""
            '    End If
            '    rpt_Rows.Item("Column4") = dtRow.Item("Years")
            '    rpt_Rows.Item("Column5") = dtRow.Item("Months")
            '    rpt_Rows.Item("Column6") = dtRow.Item("Days")
            '    rpt_Rows.Item("Column7") = dtRow.Item("GName")

            '    'Anjan (20 Mar 2012)-Start
            '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '    If dtRow.Item("birthdate").ToString.Trim <> "" Then
            '        rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column8") = ""
            '    End If

            '    If dtRow.Item("retirementdate").ToString.Trim <> "" Then
            '        rpt_Rows.Item("Column9") = eZeeDate.convertDate(dtRow.Item("retirementdate").ToString).ToShortDateString
            '    Else
            '        rpt_Rows.Item("Column9") = ""
            '    End If
            '    'Anjan (20 Mar 2012)-End 

            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            'Next


            Dim dTable As New DataTable("YearSlab")
            dTable.Columns.Add("subgrp", System.Type.GetType("System.String")).DefaultValue = ""
            dTable.Columns.Add("fage", System.Type.GetType("System.Int32")).DefaultValue = -1
            dTable.Columns.Add("tage", System.Type.GetType("System.Int32")).DefaultValue = -1
            dTable.Columns.Add("sortid", System.Type.GetType("System.Int32")).DefaultValue = -1

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim blnIsRowPresent As Boolean = False
            If mintStartingYear <> 0 Or mintEndingYear <> 0 Or mintYearGap <> 0 Then
                If mintStartingYear >= 0 Then
                    Dim i As Integer = 0
                    While True
                        Dim dRow As DataRow = dTable.NewRow
                        If (mintStartingYear + mintYearGap) > mintEndingYear Then
                            'S.SANDEEP [ 03 SEP 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'dRow.Item("subgrp") = mintStartingYear.ToString & " to " & mintEndingYear.ToString & " Years"
                            dRow.Item("subgrp") = mintStartingYear.ToString & Language.getMessage(mstrModuleName, 24, " to ") & mintEndingYear.ToString & Language.getMessage(mstrModuleName, 25, " Years")
                            'S.SANDEEP [ 03 SEP 2012 ] -- END

                            dRow.Item("fage") = mintStartingYear
                            dRow.Item("tage") = mintEndingYear
                            dRow.Item("sortid") = i
                            dTable.Rows.Add(dRow)

                            dRow = dTable.NewRow
                            'S.SANDEEP [ 03 SEP 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'dRow.Item("subgrp") = "Over " & mintEndingYear.ToString & " Years"
                            dRow.Item("subgrp") = Language.getMessage(mstrModuleName, 26, "Over ") & mintEndingYear.ToString & Language.getMessage(mstrModuleName, 25, " Years")
                            'S.SANDEEP [ 03 SEP 2012 ] -- END
                            dRow.Item("fage") = mintEndingYear + 1
                            dRow.Item("tage") = 0
                            dRow.Item("sortid") = i + 1
                            dTable.Rows.Add(dRow)
                            blnIsRowPresent = True
                            Exit While
                        End If

                        'S.SANDEEP [ 03 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dRow.Item("subgrp") = mintStartingYear.ToString & " to " & (mintStartingYear + mintYearGap - 1).ToString & " Years"
                        dRow.Item("subgrp") = mintStartingYear.ToString & Language.getMessage(mstrModuleName, 24, " to ") & (mintStartingYear + mintYearGap - 1).ToString & Language.getMessage(mstrModuleName, 25, " Years")
                        'S.SANDEEP [ 03 SEP 2012 ] -- END

                        dRow.Item("fage") = mintStartingYear
                        dRow.Item("tage") = mintStartingYear + (mintYearGap - 1)
                        dRow.Item("sortid") = i
                        dTable.Rows.Add(dRow)

                        mintStartingYear += mintYearGap
                        i += 1
                    End While
                    Dim dView As DataView = dTable.DefaultView
                    dView.Sort = "sortid DESC"
                    dTable = dView.ToTable
                End If
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END





            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Rows As DataRow = Nothing
            Dim dFilter As DataTable = Nothing
            Dim StrFilter As String = String.Empty
            If blnIsRowPresent = True Then
                For Each dtRow As DataRow In dTable.Rows
                    StrFilter = ""
                    If CInt(dtRow.Item("tage")) > 0 Then
                        StrFilter = "Years >= " & dtRow.Item("fage") & " AND Years <= " & dtRow.Item("tage")
                    Else
                        StrFilter = "Years >= " & dtRow.Item("fage")
                    End If

                    dFilter = New DataView(dsList.Tables(0), StrFilter, "", DataViewRowState.CurrentRows).ToTable
                    If dFilter.Rows.Count > 0 Then
                        For Each dFRow As DataRow In dFilter.Rows
                            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                            If dFRow.Item("ECode").ToString.Trim.Length <= 0 Then Continue For
                            rpt_Rows.Item("Column1") = dFRow.Item("ECode")
                            rpt_Rows.Item("Column2") = dFRow.Item("EName")
                            If dFRow.Item("AppDate").ToString.Trim <> "" Then
                                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dFRow.Item("AppDate").ToString).ToShortDateString
                            Else
                                rpt_Rows.Item("Column3") = ""
                            End If
                            rpt_Rows.Item("Column4") = dFRow.Item("Years")

                            'Pinkal (12-AUG-2014) -- Start
                            'Enhancement - DISPLAY -1 IN MONTH WHEN EMPLOYEE BIRTH MONTH AND REPORT MONTH IS SAME
                            'rpt_Rows.Item("Column5") = dFRow.Item("Months")
                            rpt_Rows.Item("Column5") = IIf(dFRow.Item("Months") < 0, 0, dFRow.Item("Months"))
                            'Pinkal (12-AUG-2014) -- End


                            rpt_Rows.Item("Column6") = dFRow.Item("Days")
                            rpt_Rows.Item("Column7") = dFRow.Item("GName")
                            If dFRow.Item("birthdate").ToString.Trim <> "" Then
                                rpt_Rows.Item("Column8") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
                            Else
                                rpt_Rows.Item("Column8") = ""
                            End If

                            If dFRow.Item("retirementdate").ToString.Trim <> "" Then
                                rpt_Rows.Item("Column9") = eZeeDate.convertDate(dFRow.Item("retirementdate").ToString).ToShortDateString
                            Else
                                rpt_Rows.Item("Column9") = ""
                            End If
                            rpt_Rows.Item("Column10") = dtRow.Item("subgrp")
                            rpt_Rows.Item("Column11") = dFilter.Rows.Count
                            'Shani [ 22 NOV 2014 ] -- START
                            'Enhancement - Included Gender Field 
                            rpt_Rows.Item("Column12") = dFRow.Item("gender")
                            'Shani [ 22 NOV 2014 ] -- END
                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                        Next
                    Else
                        rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Rows.Item("Column10") = dtRow.Item("subgrp")
                        rpt_Rows.Item("Column11") = 0
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If
                Next
            Else
                For Each dFRow As DataRow In dsList.Tables(0).Rows
                    rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                    If dFRow.Item("ECode").ToString.Trim.Length <= 0 Then Continue For
                    rpt_Rows.Item("Column1") = dFRow.Item("ECode")
                    rpt_Rows.Item("Column2") = dFRow.Item("EName")
                    If dFRow.Item("AppDate").ToString.Trim <> "" Then
                        rpt_Rows.Item("Column3") = eZeeDate.convertDate(dFRow.Item("AppDate").ToString).ToShortDateString
                    Else
                        rpt_Rows.Item("Column3") = ""
                    End If
                    rpt_Rows.Item("Column4") = dFRow.Item("Years")
                    'Pinkal (12-AUG-2014) -- Start
                    'Enhancement - DISPLAY -1 IN MONTH WHEN EMPLOYEE BIRTH MONTH AND REPORT MONTH IS SAME
                    'rpt_Rows.Item("Column5") = dFRow.Item("Months")
                    rpt_Rows.Item("Column5") = IIf(dFRow.Item("Months") < 0, 0, dFRow.Item("Months"))
                    'Pinkal (12-AUG-2014) -- End

                    rpt_Rows.Item("Column6") = dFRow.Item("Days")
                    rpt_Rows.Item("Column7") = dFRow.Item("GName")
                    If dFRow.Item("birthdate").ToString.Trim <> "" Then
                        rpt_Rows.Item("Column8") = eZeeDate.convertDate(dFRow.Item("birthdate").ToString).ToShortDateString
                    Else
                        rpt_Rows.Item("Column8") = ""
                    End If

                    If dFRow.Item("retirementdate").ToString.Trim <> "" Then
                        rpt_Rows.Item("Column9") = eZeeDate.convertDate(dFRow.Item("retirementdate").ToString).ToShortDateString
                    Else
                        rpt_Rows.Item("Column9") = ""
                    End If
                    rpt_Rows.Item("Column10") = ""
                    rpt_Rows.Item("Column11") = 0
                    'Shani [ 22 NOV 2014 ] -- START
                    'Enhancement - Included Gender Field 
                    rpt_Rows.Item("Column12") = dFRow.Item("gender")
                    'Shani [ 22 NOV 2014 ] -- END
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next
            End If

            'S.SANDEEP [ 24 MARCH 2012 ] -- END


            objRpt = New ArutiReport.Designer.rptEmployeeAgeAnalysisReport

            'Sandeep [ 10 FEB 2011 ] -- Start

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            'Dim arrImageRow As DataRow = Nothing
            'arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'ReportFunction.Logo_Display(objRpt, _
            '                            ConfigParameter._Object._IsDisplayLogo, _
            '                            ConfigParameter._Object._ShowLogoRightSide, _
            '                            "arutiLogo1", _
            '                            "arutiLogo2", _
            '                            arrImageRow, _
            '                            "txtCompanyName", _
            '                            "txtReportName", _
            '                            "txtFilterDescription", _
            '                            ConfigParameter._Object._GetLeftMargin, _
            '                            ConfigParameter._Object._GetRightMargin)

            'rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            'If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
            '    rpt_Data.Tables("ArutiTable").Rows.Add("")
            'End If

            'If ConfigParameter._Object._IsShowPreparedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
            '    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            'End If

            'If ConfigParameter._Object._IsShowCheckedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            'End If

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            'End If
            ''Sandeep [ 10 FEB 2011 ] -- End 

            Dim objReportFunction As New ReportFunction
            Call objReportFunction.GetReportSetting(CInt(mstrReportId), intCompanyUnkid)

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        objReportFunction._DisplayLogo, _
                                        objReportFunction._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        objReportFunction._LeftMargin, _
                                        objReportFunction._RightMargin, _
                                        objReportFunction._PageMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If objReportFunction._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 10, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If objReportFunction._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 11, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If objReportFunction._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 12, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If objReportFunction._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If


            objReportFunction = Nothing
            'S.SANDEEP [04 JUN 2015] -- END


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmplCode", Language.getMessage(mstrModuleName, 1, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmplName", Language.getMessage(mstrModuleName, 2, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtEmplDate", Language.getMessage(mstrModuleName, 3, "Appointed Date"))
            Call ReportFunction.TextChange(objRpt, "txtYear", Language.getMessage(mstrModuleName, 4, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 5, "Months"))
            Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 6, "Days"))
            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 29, "Gender"))




            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            Call ReportFunction.TextChange(objRpt, "txtBirthdate", Language.getMessage(mstrModuleName, 20, "Birthdate"))
            Call ReportFunction.TextChange(objRpt, "txtRetirementDate", Language.getMessage(mstrModuleName, 21, "Reitrement Date"))
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 22, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 23, "Total Employee :"))
            'S.SANDEEP [ 24 MARCH 2012 ] -- END

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 7, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 8, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 9, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 19, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 23, "Job :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- START
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 24, "Cost Center :"))
            '        'S.SANDEEP [ 06 SEP 2011 ] -- END 
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee Code")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Appointed Date")
            Language.setMessage(mstrModuleName, 4, "Years")
            Language.setMessage(mstrModuleName, 5, "Months")
            Language.setMessage(mstrModuleName, 6, "Days")
            Language.setMessage(mstrModuleName, 7, "Printed By :")
            Language.setMessage(mstrModuleName, 8, "Printed Date :")
            Language.setMessage(mstrModuleName, 9, "Page :")
            Language.setMessage(mstrModuleName, 10, "Prepared By :")
            Language.setMessage(mstrModuleName, 11, "Checked By :")
            Language.setMessage(mstrModuleName, 12, "Approved By :")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 14, "As On Date :")
            Language.setMessage(mstrModuleName, 15, "Employee :")
            Language.setMessage(mstrModuleName, 16, "Employee Code :")
            Language.setMessage(mstrModuleName, 17, "Order By :")
            Language.setMessage(mstrModuleName, 18, "Report By Appointment Date")
            Language.setMessage(mstrModuleName, 19, "Report By Birthdate")
            Language.setMessage(mstrModuleName, 20, "Birthdate")
            Language.setMessage(mstrModuleName, 21, "Reitrement Date")
            Language.setMessage(mstrModuleName, 22, "Grand Total :")
            Language.setMessage(mstrModuleName, 23, "Total Employee :")
            Language.setMessage(mstrModuleName, 24, " to")
            Language.setMessage(mstrModuleName, 25, " Years")
            Language.setMessage(mstrModuleName, 26, "Over")
            Language.setMessage(mstrModuleName, 27, "Condition :")
            Language.setMessage(mstrModuleName, 28, "Year :")
            Language.setMessage(mstrModuleName, 29, "Gender")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

Imports Aruti.Data
Imports eZeeCommonLib
Imports System

Public Class clsEmployeeNonDisclosureDeclarationReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeNonDisclosureDeclarationReport"
    Private mstrReportId As String = enArutiReport.Employee_NonDisclosure_Declaration_Report
    Dim objDataOperation As clsDataOperation
    Private mstrModuleName1 As String = "frmEmpNonDisclosure_Declaration"

#Region " Private variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mdtAdOnDate As DateTime = Nothing
    Private mintDeclarationStatusID As Integer
    Private mstrDeclarationStatusName As String


    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvance_Filter As String = String.Empty
    Private mblnApplyUserAccessFilter As Boolean = True

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mstrDeclareValueQuery As String = ""

    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Dim mstrCompanyCode As String = Company._Object._Code
    Dim mstrCompanyName As String = Company._Object._Name
    Dim mstrNonDisclosureDeclaration As String = ConfigParameter._Object._NonDisclosureDeclaration

#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusId() As Integer
        Set(ByVal value As Integer)
            mintDeclarationStatusID = value
        End Set
    End Property

    Public WriteOnly Property _DeclarationStatusName() As String
        Set(ByVal value As String)
            mstrDeclarationStatusName = value
        End Set
    End Property

    'Sohail (24 Jun 2020) -- Start
    'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
    Private mintYearUnkId As Integer = 0
    Public WriteOnly Property _FinancialYearId() As Integer
        Set(ByVal value As Integer)
            mintYearUnkId = value
        End Set
    End Property

    Private mstrYearName As String = ""
    Public WriteOnly Property _FinancialYearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property
    'Sohail (24 Jun 2020) -- End



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property


    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            mintYearUnkId = 0
            mstrYearName = ""
            'Sohail (24 Jun 2020) -- End

            mdtAsOnDate = Nothing
            mstrUserAccessFilter = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrReport_GroupName = ""
            mstrAdvance_Filter = ""
            mblnApplyUserAccessFilter = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim strDeclareValueQuery As String = ""
        Dim strLiabValueQuery As String = ""
        Dim strADCatQuery As String = ""
        Try



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Dim strReportExportFile As String = ""
        Try
            objCompany._Companyunkid = xCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            mstrCompanyCode = objCompany._Code
            mstrCompanyName = objCompany._Name
            mstrNonDisclosureDeclaration = objConfig._NonDisclosureDeclaration

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport, xBaseCurrencyId)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'If intReportType = 0 Then
            '    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            '    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            'Else

            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            'If intReportType = 0 Then
            '    Call OrderByExecute(iColumn_DetailReport)
            'Else

            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            'iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'If mblnFirstNamethenSurname = False Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            'End If

            'iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 45, "Appointed Date")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            'iColumn_DetailReport.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
            '                                    "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
            '                                    "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
            '                                    "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 50, "Declared Amount")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hrassetdeclaration_master.debttotal, 0)", Language.getMessage(mstrModuleName, 51, "Liability Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Function Generate_DetailReport(ByVal strDatabaseName As String _
                                          , ByVal intUserUnkid As Integer _
                                          , ByVal intYearUnkid As Integer _
                                          , ByVal intCompanyUnkid As Integer _
                                          , ByVal dtPeriodStart As Date _
                                          , ByVal dtPeriodEnd As Date _
                                          , ByVal strUserModeSetting As String _
                                          , ByVal blnIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal xExportReportPath As String _
                                          , ByVal xOpenReportAfterExport As Boolean _
                                          , ByVal xBaseCurrencyId As Integer _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim StrQ As String = ""
        Dim dsList As New DataSet

        Try
            'Sohail (10 Sep 2021) -- Start
            'NMB Issue :  : Non-Disclosure status report not showing any data when past year is selected.
            If mintYearUnkId > 0 Then
                Dim objComoany As New clsCompany_Master
                objComoany._YearUnkid = mintYearUnkId
                intYearUnkid = mintYearUnkId
                strDatabaseName = objComoany._DatabaseName
                dtPeriodStart = objComoany._Database_Start_Date
                dtPeriodEnd = objComoany._Database_End_Date
            End If
            'Sohail (10 Sep 2021) -- End

            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, True, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            Dim mdtExcel As New DataTable


            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", ISNULL(hrdepartment_master.name, '') AS department " & _
                            ", ISNULL(hrsectiongroup_master.name, '') AS sectiongroup " & _
                            ", ISNULL(hrclassgroup_master.name, '') AS classgroup " & _
                            ", ISNULL(hrclasses_master.name, '') AS class " & _
                            ", ISNULL(hrgradegroup_master.name, '') AS gradegroup " & _
                            ", hremployee_master.email " & _
                            ", hremployee_master.empsignature "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "INTO    #TableEmp " & _
                    "FROM  " & strDatabaseName & "..hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If 'ISSUE : REPORT OPTIMZATION

            'S.SANDEEP |28-OCT-2021| -- END
            
            StrQ &= mstrAnalysis_Join

            StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                        "( " & _
                        "	SELECT " & _
                        "		 gradegroupunkid " & _
                        "		,gradeunkid " & _
                        "		,gradelevelunkid " & _
                        "		,employeeunkid " & _
                        "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                        "	FROM " & strDatabaseName & "..prsalaryincrement_tran " & _
                        "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                        ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                    "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON hrdepartment_master.departmentunkid = T.departmentunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = T.sectiongroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON hrclassgroup_master.classgroupunkid = T.classgroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON hrclasses_master.classesunkid = T.classunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = G.gradegroupunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            StrQ &= "WHERE 1 = 1 "

            If mintEmployeeId > 0 Then
                StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If blnIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= "SELECT DISTINCT " & _
                        "hrempnondisclosure_declaration_tran.witness1userunkid AS witnessempid " & _
                    "INTO #witnessid " & _
                    "FROM " & strDatabaseName & "..hrempnondisclosure_declaration_tran " & _
                        "JOIN #TableEmp ON #TableEmp.employeeunkid = hrempnondisclosure_declaration_tran.employeeunkid " & _
                    "UNION " & _
                    "SELECT DISTINCT " & _
                        "hrempnondisclosure_declaration_tran.witness2userunkid AS witnessempid " & _
                    "FROM " & strDatabaseName & "..hrempnondisclosure_declaration_tran " & _
                        "JOIN #TableEmp ON #TableEmp.employeeunkid = hrempnondisclosure_declaration_tran.employeeunkid "

            StrQ &= "SELECT #witnessid.witnessempid " & _
                         ", ERECAT.jobunkid " & _
                         ", hrjob_master.job_code " & _
                         ", hrjob_master.job_name " & _
                         ", hremployee_master.employeecode AS witnessempcode " & _
                         ", hremployee_master.empsignature "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS witnessempname "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS witnessempname "
            End If

            StrQ &= "INTO #witnessjob " & _
                    "FROM #witnessid " & _
                        "JOIN " & strDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = #witnessid.witnessempid " & _
                        "LEFT JOIN " & _
                        "( " & _
                            "SELECT Cat.CatEmpId " & _
                                 ", Cat.jobgroupunkid " & _
                                 ", Cat.jobunkid " & _
                                 ", Cat.CEfDt " & _
                            "FROM " & _
                            "( " & _
                                "SELECT ECT.employeeunkid AS CatEmpId " & _
                                     ", ECT.jobgroupunkid " & _
                                     ", ECT.jobunkid " & _
                                     ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                "FROM " & strDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                "WHERE isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                            ") AS Cat " & _
                            "WHERE Cat.Rno = 1 " & _
                        ") AS ERECAT ON ERECAT.CatEmpId = #witnessid.witnessempid " & _
                        "LEFT JOIN " & strDatabaseName & "..hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid " & _
                               "AND hrjob_master.isactive = 1 "

            StrQ &= "SELECT " & _
                      "  hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid " & _
                      ", hrempnondisclosure_declaration_tran.employeeunkid " & _
                      ", #TableEmp.employeecode " & _
                      ", #TableEmp.employeename " & _
                      ", #TableEmp.department " & _
                      ", #TableEmp.sectiongroup " & _
                      ", #TableEmp.classgroup " & _
                      ", #TableEmp.class " & _
                      ", #TableEmp.gradegroup " & _
                      ", #TableEmp.email " & _
                      ", #TableEmp.empsignature " & _
                      ", #TableEmp.GName " & _
                      ", hrempnondisclosure_declaration_tran.yearunkid " & _
                      ", cffinancial_year_tran.financialyear_name " & _
                      ", hrempnondisclosure_declaration_tran.declaration_date " & _
                      ", CONVERT(CHAR(8), hrempnondisclosure_declaration_tran.declaration_date, 112) AS strdeclaration_date " & _
                      ", hrempnondisclosure_declaration_tran.witness1userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness1_date " & _
                      ", ISNULL(witness1.witnessempcode, '') AS witness1empcode " & _
                      ", ISNULL(witness1.witnessempname, '') AS witness1empname " & _
                      ", ISNULL(witness1.job_name, '') AS witness1jobname " & _
                      ", ISNULL(witness1.empsignature, '') AS witness1signature " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness1_date, ''), 112)) AS strwitness1_date " & _
                      ", hrempnondisclosure_declaration_tran.witness2userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness2_date " & _
                      ", ISNULL(witness2.witnessempcode, '') AS witness2empcode " & _
                      ", ISNULL(witness2.witnessempname, '') AS witness2empname " & _
                      ", ISNULL(witness2.job_name, '') AS witness2jobname " & _
                      ", ISNULL(witness2.empsignature, '') AS witness2signature " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness2_date, ''), 112)) AS strwitness2_date " & _
                      ", hrempnondisclosure_declaration_tran.issubmitforapproval " & _
                      ", hrempnondisclosure_declaration_tran.approvalstatusunkid " & _
                      ", hrempnondisclosure_declaration_tran.finalapproverunkid " & _
                      ", hrempnondisclosure_declaration_tran.userunkid " & _
                      ", hrempnondisclosure_declaration_tran.loginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.isweb " & _
                      ", hrempnondisclosure_declaration_tran.isvoid " & _
                      ", hrempnondisclosure_declaration_tran.voiduserunkid " & _
                      ", hrempnondisclosure_declaration_tran.voidloginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.voiddatetime " & _
                      ", hrempnondisclosure_declaration_tran.voidreason " & _
                 "FROM #TableEmp " & _
                      "JOIN " & strDatabaseName & "..hrempnondisclosure_declaration_tran ON #TableEmp.employeeunkid = hrempnondisclosure_declaration_tran.employeeunkid " & _
                      "LEFT JOIN #witnessjob AS witness1 ON witness1.witnessempid = hrempnondisclosure_declaration_tran.witness1userunkid " & _
                      "LEFT JOIN #witnessjob AS witness2 ON witness2.witnessempid = hrempnondisclosure_declaration_tran.witness2userunkid " & _
                      "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = hrempnondisclosure_declaration_tran.yearunkid "

            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            If mintYearUnkId > 0 Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.yearunkid = @yearunkid "
                objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkId)
            End If
            'Sohail (24 Jun 2020) -- End

            StrQ &= "WHERE hrempnondisclosure_declaration_tran.isvoid = 0 "

            'Sohail (10 Sep 2021) -- Start
            'NMB Issue :  : Non-Disclosure status report not showing any data when past year is selected.
            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrempnondisclosure_declaration_tran.declaration_date, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' "
            End If
            'Sohail (10 Sep 2021) -- End

            If mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.NOT_SUBMITTED) Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid IS NULL  "
            ElseIf mintDeclarationStatusID = CInt(enAssetDeclarationStatusType.SUMBITTED) Then
                StrQ &= " AND hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid IS NOT NULL  "
            End If

            StrQ &= " ORDER BY #TableEmp.employeename "

            StrQ &= " DROP TABLE #TableEmp " & _
                    " DROP TABLE #witnessid " & _
                    " DROP TABLE #witnessjob "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Dim strAcknowledged As String = Language.getMessage(mstrModuleName, 12, "Acknowledged")
            Dim strNotAcknowledged As String = Language.getMessage(mstrModuleName, 13, "Not Acknowledged")

            Dim str As String = mstrNonDisclosureDeclaration.Replace(vbCrLf, "<br> </br> ").Replace(ChrW(9), "&emsp;")

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim intSrNo As Integer = 0
            Dim dr As DataRow = Nothing
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                intSrNo += 1

                rpt_Row.Item("Column1") = dsRow.Item("employeecode")
                rpt_Row.Item("Column2") = dsRow.Item("employeename")
                If IsDBNull(dsRow.Item("declaration_date")) = True Then
                    rpt_Row.Item("Column3") = ""
                Else
                    rpt_Row.Item("Column3") = CDate(dsRow.Item("declaration_date")).ToString("dd-MMM-yyyy")
                End If


                'Gajanan [13-June-2020] -- Start
                'Enhancement NMB Employee Declaration Changes.
                'rpt_Row.Item("Column4") = dsRow.Item("witness1empcode")
                'rpt_Row.Item("Column5") = dsRow.Item("witness1empname")
                'rpt_Row.Item("Column6") = dsRow.Item("witness1jobname")
                'If IsDBNull(dsRow.Item("witness1_date")) = True Then
                '    rpt_Row.Item("Column7") = ""
                'Else
                '    rpt_Row.Item("Column7") = CDate(dsRow.Item("witness1_date")).ToString("dd-MMM-yyyy")
                'End If

                Dim objLeave As New clsleaveform
                Dim blnWitnesser1status As Boolean = False

                If objLeave.isDayExist(False, CInt(dsRow.Item("witness1userunkid")), dtPeriodStart, dtPeriodStart) = True Then
                    blnWitnesser1status = True
                ElseIf objLeave.isDayExist(True, CInt(dsRow.Item("witness1userunkid")), dtPeriodStart, dtPeriodStart) = True Then
                    blnWitnesser1status = True
                End If

                If blnWitnesser1status Then
                    rpt_Row.Item("Column4") = dsRow.Item("witness2empcode")
                    rpt_Row.Item("Column5") = dsRow.Item("witness2empname")
                    rpt_Row.Item("Column6") = dsRow.Item("witness2jobname")
                    If IsDBNull(dsRow.Item("witness2_date")) = True Then
                        rpt_Row.Item("Column7") = ""
                    Else
                        rpt_Row.Item("Column7") = CDate(dsRow.Item("witness2_date")).ToString("dd-MMM-yyyy")
                    End If
                    'S.SANDEEP |04-JUL-2020| -- START
                    'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (SIG. DECRYPTION)                
                    'rpt_Row.Item("sign2") = dsRow.Item("witness2signature")
                    If IsDBNull(dsRow.Item("witness2signature")) = False Then
                        Try
                            rpt_Row.Item("sign2") = clsSecurity.DecryptByte(CType(dsRow.Item("witness2signature"), Byte()), "ezee")
                        Catch ex As Exception
                            rpt_Row.Item("sign2") = CType(dsRow.Item("witness2signature"), Byte())
                        End Try
                    Else
                        Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                        rpt_Row.Item("sign2") = eZeeDataType.image2Data(imgBlank)
                    End If
                    'S.SANDEEP |04-JUL-2020| -- END
                Else
                    rpt_Row.Item("Column4") = dsRow.Item("witness1empcode")
                    rpt_Row.Item("Column5") = dsRow.Item("witness1empname")
                    rpt_Row.Item("Column6") = dsRow.Item("witness1jobname")
                    If IsDBNull(dsRow.Item("witness1_date")) = True Then
                        rpt_Row.Item("Column7") = ""
                    Else
                        rpt_Row.Item("Column7") = CDate(dsRow.Item("witness1_date")).ToString("dd-MMM-yyyy")
                    End If
                    'S.SANDEEP |04-JUL-2020| -- START
                    'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (SIG. DECRYPTION)                
                    'rpt_Row.Item("sign2") = dsRow.Item("witness1signature")
                    If IsDBNull(dsRow.Item("witness1signature")) = False Then
                        Try
                            rpt_Row.Item("sign2") = clsSecurity.DecryptByte(CType(dsRow.Item("witness1signature"), Byte()), "ezee")
                        Catch ex As Exception
                            rpt_Row.Item("sign2") = CType(dsRow.Item("witness1signature"), Byte())
                        End Try
                    Else
                        Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                        rpt_Row.Item("sign2") = eZeeDataType.image2Data(imgBlank)
                    End If
                    'S.SANDEEP |04-JUL-2020| -- END
                End If
                'Gajanan [13-June-2020] -- End

                rpt_Row.Item("Column8") = dsRow.Item("witness2empcode")
                rpt_Row.Item("Column9") = dsRow.Item("witness2empname")
                rpt_Row.Item("Column10") = dsRow.Item("witness2jobname")
                If IsDBNull(dsRow.Item("witness2_date")) = True Then
                    rpt_Row.Item("Column11") = ""
                Else
                    rpt_Row.Item("Column11") = CDate(dsRow.Item("witness2_date")).ToString("dd-MMM-yyyy")
                End If
                'S.SANDEEP |04-JUL-2020| -- START
                'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (SIG. DECRYPTION)                
                'rpt_Row.Item("sign1") = dsRow.Item("empsignature")
                If IsDBNull(dsRow.Item("empsignature")) = False Then
                    Try
                        rpt_Row.Item("sign1") = clsSecurity.DecryptByte(CType(dsRow.Item("empsignature"), Byte()), "ezee")
                    Catch ex As Exception
                        rpt_Row.Item("sign1") = CType(dsRow.Item("empsignature"), Byte())
                    End Try
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    rpt_Row.Item("sign1") = eZeeDataType.image2Data(imgBlank)
                End If
                'S.SANDEEP |04-JUL-2020| -- END


                'Gajanan [13-June-2020] -- Start
                'Enhancement NMB Employee Declaration Changes.
                'rpt_Row.Item("sign2") = dsRow.Item("witness1signature")
                'Gajanan [13-June-2020] -- End


                'S.SANDEEP |04-JUL-2020| -- START
                'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (SIG. DECRYPTION)                
                'rpt_Row.Item("sign3") = dsRow.Item("witness2signature")
                If IsDBNull(dsRow.Item("witness2signature")) = False Then
                    Try
                        rpt_Row.Item("sign3") = clsSecurity.DecryptByte(CType(dsRow.Item("witness2signature"), Byte()), "ezee")
                    Catch ex As Exception
                        rpt_Row.Item("sign3") = CType(dsRow.Item("witness2signature"), Byte())
                    End Try
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    rpt_Row.Item("sign3") = eZeeDataType.image2Data(imgBlank)
                End If
                'S.SANDEEP |04-JUL-2020| -- END

                rpt_Row.Item("Column21") = dsRow.Item("GName").ToString
                rpt_Row.Item("Column22") = str.Replace("#employeename#", "<B>" & dsRow.Item("employeename").ToString.Replace("  ", " ") & "</B>")
                rpt_Row.Item("Column23") = mstrCompanyName

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            'Dim intArrayColumnWidth As Integer() = Nothing
            'Dim strarrGroupColumns As String() = Nothing
            'ReDim intArrayColumnWidth(mdtExcel.Columns.Count - 1)
            'For ii As Integer = 0 To intArrayColumnWidth.Length - 1
            '    intArrayColumnWidth(ii) = 125
            'Next
            'Dim rowsArrayHeader As New ArrayList
            'Dim rowsArrayFooter As New ArrayList
            'Dim row As WorksheetRow
            'Dim wcell As WorksheetCell = Nothing

            'If mintViewIndex <= 0 Then
            '    mdtExcel.Columns.Remove("GName")
            'Else
            '    Dim strGrpCols As String() = {"GName"}
            '    strarrGroupColumns = strGrpCols
            'End If

            Dim strFilterTitle As String = Language.getMessage(mstrModuleName, 14, "Status :")
            If mintDeclarationStatusID > 0 Then
                strFilterTitle &= mstrDeclarationStatusName
            Else
                strFilterTitle &= Language.getMessage(mstrModuleName, 15, "All")
            End If
            'Sohail (24 Jun 2020) -- Start
            'NMB Enhancement # : On the confidentiality non disclosure agreement reports, provide option to pull the reports by financial year.
            If mintYearUnkId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 18, "Year :") & mstrYearName
            End If
            'Sohail (24 Jun 2020) -- End
            strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 16, "As On Date :") & mdtAsOnDate.ToString("dd-MMM-yyyy")

            If mintEmployeeId > 0 Then
                strFilterTitle &= "; " & Language.getMessage(mstrModuleName, 17, "Employee :") & mstrEmployeeName
            End If
            'row = New WorksheetRow()
            'wcell = New WorksheetCell(strFilterTitle, "s9bw")
            'wcell.MergeAcross = mdtExcel.Columns.Count - 1
            'row.Cells.Add(wcell)
            'rowsArrayHeader.Add(row)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            objRpt = New ArutiReport.Designer.rptEmpNonDiscDeclaration

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow

            ReportFunction.Logo_Display(objRpt, _
                                       True, _
                                       False, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       "", _
                                       "", _
                                       "", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            Language.setLanguage(mstrModuleName) 'Languages from current report
            'Call ReportFunction.TextChange(objRpt, "lblJobEmp", Language._Object.getCaption("lblEmpName", "Name:"))
            Call ReportFunction.TextChange(objRpt, "lblDeclaredByWitness", Language.getMessage(mstrModuleName, 1, "Witnessed by on behalf of NMB Bank Plc:"))
            Call ReportFunction.TextChange(objRpt, "lblDateWitness1", Language.getMessage(mstrModuleName, 2, "Date:"))
            Call ReportFunction.TextChange(objRpt, "lblNameWitness1", Language.getMessage(mstrModuleName, 3, "Name:"))

            Call ReportFunction.TextChange(objRpt, "lblDateWitness2", Language.getMessage(mstrModuleName, 4, "Date:"))
            Call ReportFunction.TextChange(objRpt, "lblNameWitness2", Language.getMessage(mstrModuleName, 5, "Name:"))

            Call ReportFunction.TextChange(objRpt, "lblSignEmp", Language.getMessage(mstrModuleName, 6, "Signature:"))
            Call ReportFunction.TextChange(objRpt, "lblSignWitness1", Language.getMessage(mstrModuleName, 7, "Signature:"))
            Call ReportFunction.TextChange(objRpt, "lblSignWitness2", Language.getMessage(mstrModuleName, 8, "Signature:"))
            'Sohail (29 May 2020) -- Start
            'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee"))
            'Sohail (29 May 2020) -- End


            Language.setLanguage(mstrModuleName1) 'Languages from Non-Disclosure Declaration page
            Call ReportFunction.TextChange(objRpt, "lblFormCaption", Language._Object.getCaption("lblDetailHeader", "Confidentiality Agreement and Non-Disclosure Declaration"))
            Call ReportFunction.TextChange(objRpt, "lblDeclaredByEmp", Language._Object.getCaption("lblDeclaredBy", "Declared by:"))
            Call ReportFunction.TextChange(objRpt, "lblDateEmp", Language._Object.getCaption("lblEmpDeclareDate", "Date:"))
            Call ReportFunction.TextChange(objRpt, "lblNameEmp", Language._Object.getCaption("lblEmpName", "Name:"))




        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
        Return objRpt
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Witnessed by on behalf of NMB Bank Plc:")
            Language.setMessage(mstrModuleName, 2, "Date:")
            Language.setMessage(mstrModuleName, 3, "Name:")
            Language.setMessage(mstrModuleName, 4, "Date:")
            Language.setMessage(mstrModuleName, 5, "Name:")
            Language.setMessage(mstrModuleName, 6, "Signature:")
            Language.setMessage(mstrModuleName, 7, "Signature:")
            Language.setMessage(mstrModuleName, 8, "Signature:")
            Language.setMessage(mstrModuleName, 9, "Employee")
            Language.setMessage(mstrModuleName, 12, "Acknowledged")
            Language.setMessage(mstrModuleName, 13, "Not Acknowledged")
            Language.setMessage(mstrModuleName, 14, "Status :")
            Language.setMessage(mstrModuleName, 15, "All")
            Language.setMessage(mstrModuleName, 16, "As On Date :")
            Language.setMessage(mstrModuleName, 17, "Employee :")
            Language.setMessage(mstrModuleName, 18, "Year :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessmentCalibrationReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessmentCalibrationReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlColumn = New System.Windows.Forms.Panel
        Me.objchkdisplay = New System.Windows.Forms.CheckBox
        Me.lvDisplayCol = New eZee.Common.eZeeListView(Me.components)
        Me.colhDisplayName = New System.Windows.Forms.ColumnHeader
        Me.objcolhSelectCol = New System.Windows.Forms.ColumnHeader
        Me.objcolhJoin = New System.Windows.Forms.ColumnHeader
        Me.objcolhDisplay = New System.Windows.Forms.ColumnHeader
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.objpnlRating = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvRating = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRating = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCalibrationNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhScF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhScT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRatingId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCalibrationId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportPlanning = New System.Windows.Forms.Label
        Me.lblCalibrationRating = New System.Windows.Forms.Label
        Me.lblCalibrationStaus = New System.Windows.Forms.Label
        Me.cboCalibrationStatus = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblProvisionalRating = New System.Windows.Forms.Label
        Me.lblCalibrationNo = New System.Windows.Forms.Label
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        Me.objpnlColumn.SuspendLayout()
        Me.objpnlRating.SuspendLayout()
        CType(Me.dgvRating, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objpnlColumn)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objpnlRating)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportPlanning)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalibrationRating)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalibrationStaus)
        Me.gbFilterCriteria.Controls.Add(Me.cboCalibrationStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblProvisionalRating)
        Me.gbFilterCriteria.Controls.Add(Me.lblCalibrationNo)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(709, 331)
        Me.gbFilterCriteria.TabIndex = 20
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlColumn
        '
        Me.objpnlColumn.Controls.Add(Me.objchkdisplay)
        Me.objpnlColumn.Controls.Add(Me.lvDisplayCol)
        Me.objpnlColumn.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlColumn.Location = New System.Drawing.Point(437, 37)
        Me.objpnlColumn.Name = "objpnlColumn"
        Me.objpnlColumn.Size = New System.Drawing.Size(259, 283)
        Me.objpnlColumn.TabIndex = 320
        '
        'objchkdisplay
        '
        Me.objchkdisplay.AutoSize = True
        Me.objchkdisplay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkdisplay.Location = New System.Drawing.Point(212, 9)
        Me.objchkdisplay.Name = "objchkdisplay"
        Me.objchkdisplay.Size = New System.Drawing.Size(15, 14)
        Me.objchkdisplay.TabIndex = 319
        Me.objchkdisplay.UseVisualStyleBackColor = True
        '
        'lvDisplayCol
        '
        Me.lvDisplayCol.BackColorOnChecked = False
        Me.lvDisplayCol.CheckBoxes = True
        Me.lvDisplayCol.ColumnHeaders = Nothing
        Me.lvDisplayCol.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhDisplayName, Me.objcolhSelectCol, Me.objcolhJoin, Me.objcolhDisplay})
        Me.lvDisplayCol.CompulsoryColumns = ""
        Me.lvDisplayCol.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvDisplayCol.FullRowSelect = True
        Me.lvDisplayCol.GridLines = True
        Me.lvDisplayCol.GroupingColumn = Nothing
        Me.lvDisplayCol.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvDisplayCol.HideSelection = False
        Me.lvDisplayCol.Location = New System.Drawing.Point(0, 0)
        Me.lvDisplayCol.MinColumnWidth = 50
        Me.lvDisplayCol.MultiSelect = False
        Me.lvDisplayCol.Name = "lvDisplayCol"
        Me.lvDisplayCol.OptionalColumns = ""
        Me.lvDisplayCol.ShowMoreItem = False
        Me.lvDisplayCol.ShowSaveItem = False
        Me.lvDisplayCol.ShowSelectAll = True
        Me.lvDisplayCol.ShowSizeAllColumnsToFit = True
        Me.lvDisplayCol.Size = New System.Drawing.Size(259, 283)
        Me.lvDisplayCol.Sortable = True
        Me.lvDisplayCol.TabIndex = 318
        Me.lvDisplayCol.UseCompatibleStateImageBehavior = False
        Me.lvDisplayCol.View = System.Windows.Forms.View.Details
        '
        'colhDisplayName
        '
        Me.colhDisplayName.Tag = "colhDisplayName"
        Me.colhDisplayName.Text = "Display Column On Report"
        Me.colhDisplayName.Width = 245
        '
        'objcolhSelectCol
        '
        Me.objcolhSelectCol.Tag = "objcolhSelectCol"
        Me.objcolhSelectCol.Text = ""
        Me.objcolhSelectCol.Width = 0
        '
        'objcolhJoin
        '
        Me.objcolhJoin.Tag = "objcolhJoin"
        Me.objcolhJoin.Text = ""
        Me.objcolhJoin.Width = 0
        '
        'objcolhDisplay
        '
        Me.objcolhDisplay.Tag = "objcolhDisplay"
        Me.objcolhDisplay.Text = ""
        Me.objcolhDisplay.Width = 0
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(424, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(5, 306)
        Me.objStLine1.TabIndex = 317
        Me.objStLine1.Text = "EZeeStraightLine2"
        '
        'objpnlRating
        '
        Me.objpnlRating.Controls.Add(Me.objchkAll)
        Me.objpnlRating.Controls.Add(Me.dgvRating)
        Me.objpnlRating.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlRating.Location = New System.Drawing.Point(116, 118)
        Me.objpnlRating.Name = "objpnlRating"
        Me.objpnlRating.Size = New System.Drawing.Size(276, 202)
        Me.objpnlRating.TabIndex = 315
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(7, 6)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 322
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvRating
        '
        Me.dgvRating.AllowUserToAddRows = False
        Me.dgvRating.AllowUserToDeleteRows = False
        Me.dgvRating.AllowUserToResizeColumns = False
        Me.dgvRating.AllowUserToResizeRows = False
        Me.dgvRating.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvRating.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvRating.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvRating.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhRating, Me.dgcolhCalibrationNo, Me.objdgcolhScF, Me.objdgcolhScT, Me.objdgcolhRatingId, Me.objdgcolhCalibrationId})
        Me.dgvRating.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvRating.Location = New System.Drawing.Point(0, 0)
        Me.dgvRating.Name = "dgvRating"
        Me.dgvRating.RowHeadersVisible = False
        Me.dgvRating.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvRating.Size = New System.Drawing.Size(276, 202)
        Me.dgvRating.TabIndex = 315
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhRating
        '
        Me.dgcolhRating.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhRating.HeaderText = "Rating"
        Me.dgcolhRating.Name = "dgcolhRating"
        Me.dgcolhRating.ReadOnly = True
        '
        'dgcolhCalibrationNo
        '
        Me.dgcolhCalibrationNo.HeaderText = "Calibration No"
        Me.dgcolhCalibrationNo.Name = "dgcolhCalibrationNo"
        Me.dgcolhCalibrationNo.ReadOnly = True
        Me.dgcolhCalibrationNo.Visible = False
        '
        'objdgcolhScF
        '
        Me.objdgcolhScF.HeaderText = "objdgcolhScF"
        Me.objdgcolhScF.Name = "objdgcolhScF"
        Me.objdgcolhScF.ReadOnly = True
        Me.objdgcolhScF.Visible = False
        '
        'objdgcolhScT
        '
        Me.objdgcolhScT.HeaderText = "objdgcolhScT"
        Me.objdgcolhScT.Name = "objdgcolhScT"
        Me.objdgcolhScT.ReadOnly = True
        Me.objdgcolhScT.Visible = False
        '
        'objdgcolhRatingId
        '
        Me.objdgcolhRatingId.HeaderText = "objdgcolhRatingId"
        Me.objdgcolhRatingId.Name = "objdgcolhRatingId"
        Me.objdgcolhRatingId.ReadOnly = True
        Me.objdgcolhRatingId.Visible = False
        '
        'objdgcolhCalibrationId
        '
        Me.objdgcolhCalibrationId.HeaderText = "objdgcolhCalibrationId"
        Me.objdgcolhCalibrationId.Name = "objdgcolhCalibrationId"
        Me.objdgcolhCalibrationId.ReadOnly = True
        Me.objdgcolhCalibrationId.Visible = False
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 210
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(116, 64)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(276, 21)
        Me.cboReportType.TabIndex = 132
        '
        'lblReportPlanning
        '
        Me.lblReportPlanning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportPlanning.Location = New System.Drawing.Point(8, 67)
        Me.lblReportPlanning.Name = "lblReportPlanning"
        Me.lblReportPlanning.Size = New System.Drawing.Size(102, 15)
        Me.lblReportPlanning.TabIndex = 133
        Me.lblReportPlanning.Text = "Report Type"
        Me.lblReportPlanning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCalibrationRating
        '
        Me.lblCalibrationRating.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalibrationRating.Location = New System.Drawing.Point(8, 121)
        Me.lblCalibrationRating.Name = "lblCalibrationRating"
        Me.lblCalibrationRating.Size = New System.Drawing.Size(102, 15)
        Me.lblCalibrationRating.TabIndex = 314
        Me.lblCalibrationRating.Text = "Calibration Rating"
        Me.lblCalibrationRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCalibrationStaus
        '
        Me.lblCalibrationStaus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalibrationStaus.Location = New System.Drawing.Point(8, 121)
        Me.lblCalibrationStaus.Name = "lblCalibrationStaus"
        Me.lblCalibrationStaus.Size = New System.Drawing.Size(102, 15)
        Me.lblCalibrationStaus.TabIndex = 313
        Me.lblCalibrationStaus.Text = "Calibration Staus"
        Me.lblCalibrationStaus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCalibrationStaus.Visible = False
        '
        'cboCalibrationStatus
        '
        Me.cboCalibrationStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalibrationStatus.DropDownWidth = 210
        Me.cboCalibrationStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalibrationStatus.FormattingEnabled = True
        Me.cboCalibrationStatus.Location = New System.Drawing.Point(116, 118)
        Me.cboCalibrationStatus.Name = "cboCalibrationStatus"
        Me.cboCalibrationStatus.Size = New System.Drawing.Size(276, 21)
        Me.cboCalibrationStatus.TabIndex = 312
        Me.cboCalibrationStatus.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 94)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(102, 15)
        Me.lblEmployee.TabIndex = 309
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 350
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(116, 91)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(276, 21)
        Me.cboEmployee.TabIndex = 308
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(398, 91)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 310
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(611, 5)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 16)
        Me.lnkSetAnalysis.TabIndex = 94
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 40)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(102, 15)
        Me.lblPeriod.TabIndex = 79
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 210
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(116, 37)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(276, 21)
        Me.cboPeriod.TabIndex = 77
        '
        'lblProvisionalRating
        '
        Me.lblProvisionalRating.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvisionalRating.Location = New System.Drawing.Point(8, 121)
        Me.lblProvisionalRating.Name = "lblProvisionalRating"
        Me.lblProvisionalRating.Size = New System.Drawing.Size(102, 15)
        Me.lblProvisionalRating.TabIndex = 316
        Me.lblProvisionalRating.Text = "Provisional Rating"
        Me.lblProvisionalRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCalibrationNo
        '
        Me.lblCalibrationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalibrationNo.Location = New System.Drawing.Point(8, 121)
        Me.lblCalibrationNo.Name = "lblCalibrationNo"
        Me.lblCalibrationNo.Size = New System.Drawing.Size(102, 15)
        Me.lblCalibrationNo.TabIndex = 322
        Me.lblCalibrationNo.Text = "Calibration Number"
        Me.lblCalibrationNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(779, 58)
        Me.EZeeHeader1.TabIndex = 21
        Me.EZeeHeader1.Title = "Assessment Calibration Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 460)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(779, 55)
        Me.EZeeFooter1.TabIndex = 34
        '
        'btnAdvanceFilter
        '
        Me.btnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.btnAdvanceFilter.BackgroundImage = CType(resources.GetObject("btnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.btnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.btnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.btnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Location = New System.Drawing.Point(375, 13)
        Me.btnAdvanceFilter.Name = "btnAdvanceFilter"
        Me.btnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdvanceFilter.Size = New System.Drawing.Size(105, 30)
        Me.btnAdvanceFilter.TabIndex = 87
        Me.btnAdvanceFilter.Text = "&Advance Filter"
        Me.btnAdvanceFilter.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(486, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(582, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(678, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(12, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 32
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'frmAssessmentCalibrationReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 515)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.EZeeHeader1)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessmentCalibrationReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmAssessmentCalibrationReport"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.objpnlColumn.ResumeLayout(False)
        Me.objpnlColumn.PerformLayout()
        Me.objpnlRating.ResumeLayout(False)
        Me.objpnlRating.PerformLayout()
        CType(Me.dgvRating, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReportPlanning As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCalibrationRating As System.Windows.Forms.Label
    Friend WithEvents lblCalibrationStaus As System.Windows.Forms.Label
    Friend WithEvents cboCalibrationStatus As System.Windows.Forms.ComboBox
    Friend WithEvents objpnlRating As System.Windows.Forms.Panel
    Friend WithEvents lblProvisionalRating As System.Windows.Forms.Label
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objpnlColumn As System.Windows.Forms.Panel
    Friend WithEvents objchkdisplay As System.Windows.Forms.CheckBox
    Friend WithEvents lvDisplayCol As eZee.Common.eZeeListView
    Friend WithEvents colhDisplayName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhSelectCol As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhJoin As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDisplay As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvRating As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRating As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCalibrationNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhScF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhScT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRatingId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCalibrationId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblCalibrationNo As System.Windows.Forms.Label
End Class

'************************************************************************************************************************************
'Class Name : frmEmployeeRelationReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeRelationReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeRelationReport"
    Private objRelationReport As clsEmployeeRelationReport

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'Sandeep [ 12 MARCH 2011 ] -- End 
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objRelationReport = New clsEmployeeRelationReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objRelationReport.SetDefaultValue()
        InitializeComponent()
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim dsList As New DataSet
        Try


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = ObjEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = ObjEmp.GetEmployeeList("Emp", True, False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'Pinkal (24-Jun-2011) -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            'S.SANDEEP [15-SEP-2017] -- START
            'dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION", )
            'With cboRelation
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("RELATION")
            '    .SelectedValue = 0
            'End With
            'S.SANDEEP [15-SEP-2017] -- END

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Dependants Wise"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Referee Wise"))


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True, False, False)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 5
            End With
            'Pinkal (16-AUG-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            ObjEmp = Nothing
            ObjCMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboRelation.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            txtCode.Text = ""
            txtFirstName.Text = ""
            txtLastName.Text = ""

            'Sandeep [ 12 MARCH 2011 ] -- Start
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sandeep [ 12 MARCH 2011 ] -- End 
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [ 06 JUN 2014 ] -- START
            chkshowdependantswithoutphotos.Checked = False
            'S.SANDEEP [ 06 JUN 2014 ] -- END


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            dtpAsonDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboCondition.SelectedValue = 5
            chkIncludeInactiveDependents.Checked = False
            chkShowExemptedDependents.Checked = False
            chkShowInactiveDependents.Checked = False
            rdAgeLimitWise.Checked = True
            rdExemptionWise.Checked = False
            'Pinkal (16-AUG-2014) -- End

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            chkShowSubGrandTotal.Checked = False
            'S.SANDEEP [16-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objRelationReport.SetDefaultValue()

            objRelationReport._EmployeeId = cboEmployee.SelectedValue
            objRelationReport._EmployeeName = cboEmployee.Text

            objRelationReport._RelationId = cboRelation.SelectedValue
            objRelationReport._RelationName = cboRelation.Text

            objRelationReport._ReportTypeId = cboReportType.SelectedIndex
            objRelationReport._ReportTypeName = cboReportType.Text

            Select Case cboReportType.SelectedIndex
                Case 0  'Dependants
                    objRelationReport._FirstName = txtFirstName.Text.Trim
                    objRelationReport._LastName = txtLastName.Text.Trim
                Case 1  'Referee
                    objRelationReport._FirstName = txtFirstName.Text.Trim
            End Select

            'Sandeep [ 12 MARCH 2011 ] -- Start
            objRelationReport._ViewByIds = mstrStringIds
            objRelationReport._ViewIndex = mintViewIdx
            objRelationReport._ViewByName = mstrStringName
            'Sandeep [ 12 MARCH 2011 ] -- End 
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            objRelationReport._Analysis_Fields = mstrAnalysis_Fields
            objRelationReport._Analysis_Join = mstrAnalysis_Join
            objRelationReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objRelationReport._Report_GroupName = mstrReport_GroupName
            'Sohail (16 May 2012) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objRelationReport._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objRelationReport._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END

            'S.SANDEEP [16-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 16
            objRelationReport._ShowSubAndGrandTotalRelationWise = chkShowSubGrandTotal.Checked
            'S.SANDEEP [16-SEP-2017] -- END

            'S.SANDEEP [ 06 JUN 2014 ] -- START
            Select Case cboReportType.SelectedIndex
                Case 0  'DEPENDANTS
                    objRelationReport._ShowDepedantsWithoutImage = chkshowdependantswithoutphotos.Checked
                    objRelationReport._ShowDepedantsWithoutImageText = chkshowdependantswithoutphotos.Text
                    If ConfigParameter._Object._IsImgInDataBase = True Then
                        objRelationReport._CompanyUnkId = Company._Object._Companyunkid
                    End If
                    objRelationReport._IsImageInDatabase = ConfigParameter._Object._IsImgInDataBase

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    objRelationReport._AsonDate = dtpAsonDate.Value.Date
                    objRelationReport._Condition = cboCondition.Text

                    If chkIncludeInactiveDependents.Checked Then
                        objRelationReport._IncludeInactiveDependantsText = chkIncludeInactiveDependents.Text
                    End If
                    objRelationReport._IncludeInactiveDependants = chkIncludeInactiveDependents.Checked

                    If chkShowExemptedDependents.Checked Then
                        objRelationReport._ShowOnlyExemptedDependentsText = chkShowExemptedDependents.Text
                    End If
                    objRelationReport._ShowOnlyExemptedDependents = chkShowExemptedDependents.Checked

                    If chkShowInactiveDependents.Checked Then
                        objRelationReport._ShowOnlyInActiveDependantsText = chkShowInactiveDependents.Text
                    End If
                    objRelationReport._ShowOnlyInactiveDependants = chkShowInactiveDependents.Checked

                    If pnlInactivedependentoption.Enabled Then
                        If rdAgeLimitWise.Checked Then
                            objRelationReport._AgeLimitWiseText = rdAgeLimitWise.Text
                        End If
                        objRelationReport._AgeLimitWise = rdAgeLimitWise.Checked

                        If rdExemptionWise.Checked Then
                            objRelationReport._ExemptionWiseText = rdExemptionWise.Text
                        End If
                        objRelationReport._ExemptionWise = rdExemptionWise.Checked
                    End If

                    'Pinkal (16-AUG-2014) -- End

            End Select
            'S.SANDEEP [ 06 JUN 2014 ] -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmEmployeeRelationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRelationReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeRelationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeRelationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objRelationReport._ReportName
            Me._Message = objRelationReport._ReportDesc

            Call FillCombo()
            Call ResetValue()


            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
            pnlInactivedependentoption.Enabled = False
            'Pinkal (16-AUG-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeRelationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRelationReport.generateReport(0, e.Type, enExportAction.None)
            objRelationReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRelationReport.generateReport(0, enPrintAction.None, e.Type)
            objRelationReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeRelationReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeRelationReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Controls "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'Pinkal (16-AUG-2014) -- Start
            'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

            Select Case cboReportType.SelectedIndex
                Case 0  'Depandants
                    lblName.Visible = False
                    lblFirstname.Visible = True
                    txtLastName.Enabled = True
                    'S.SANDEEP [ 06 JUN 2014 ] -- START
                    chkshowdependantswithoutphotos.Enabled = True
                    'S.SANDEEP [ 06 JUN 2014 ] -- END

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    dtpAsonDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpAsonDate.Enabled = True
                    chkIncludeInactiveDependents.Checked = False
                    chkIncludeInactiveDependents.Enabled = True
                    chkShowExemptedDependents.Checked = False
                    chkShowExemptedDependents.Enabled = True
                    chkShowInactiveDependents.Checked = False
                    chkShowInactiveDependents.Enabled = True
                    rdAgeLimitWise.Checked = True
                    rdExemptionWise.Checked = False
                    pnlInactivedependentoption.Enabled = True
                    cboCondition.SelectedValue = 5  'EQUAL TO
                    cboCondition.Enabled = True
                    'Pinkal (16-AUG-2014) -- End


                Case 1  'Referee
                    txtLastName.Text = ""
                    lblName.Visible = True
                    lblFirstname.Visible = False
                    txtLastName.Enabled = False
                    'S.SANDEEP [ 06 JUN 2014 ] -- START
                    chkshowdependantswithoutphotos.Checked = False
                    chkshowdependantswithoutphotos.Enabled = False
                    'S.SANDEEP [ 06 JUN 2014 ] -- END

                    'Pinkal (16-AUG-2014) -- Start
                    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report
                    dtpAsonDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpAsonDate.Enabled = False
                    chkIncludeInactiveDependents.Checked = False
                    chkIncludeInactiveDependents.Enabled = False
                    chkShowExemptedDependents.Checked = False
                    chkShowExemptedDependents.Enabled = False
                    chkShowInactiveDependents.Checked = False
                    chkShowInactiveDependents.Enabled = False
                    rdAgeLimitWise.Checked = True
                    rdExemptionWise.Checked = False
                    pnlInactivedependentoption.Enabled = False
                    cboCondition.SelectedValue = 5  'EQUAL TO
                    cboCondition.Enabled = False
                    'Pinkal (16-AUG-2014) -- End

            End Select

            'Pinkal (16-AUG-2014) -- End

            'S.SANDEEP [15-SEP-2017] -- START
            Dim dsList As New DataSet
            Dim objCMaster As New clsCommon_Master
            Select Case cboReportType.SelectedIndex
                Case 0 'Depandants
                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION", )
                Case 1 'Referee
                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "RELATION", -1, True)
            End Select
            With cboRelation
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("RELATION")
                .SelectedValue = 0
            End With
            'S.SANDEEP [15-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 12 MARCH 2011 ] -- Start
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (16 May 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sandeep [ 12 MARCH 2011 ] -- End 

#End Region


    'Pinkal (16-AUG-2014) -- Start
    'Enhancement - TRA Medical Enhancement for Medical & Leave Dependants AND It's Report

#Region "CheckBox Event"

    Private Sub chkShowExemptedDependents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowExemptedDependents.CheckedChanged
        Try
            If chkShowExemptedDependents.Checked Then
                chkIncludeInactiveDependents.Checked = False
                chkIncludeInactiveDependents.Enabled = False
                chkShowInactiveDependents.Checked = False
                chkShowInactiveDependents.Enabled = False
                cboCondition.SelectedValue = 5 'EQUAL TO
                pnlInactivedependentoption.Enabled = False
            Else
                chkIncludeInactiveDependents.Checked = False
                chkIncludeInactiveDependents.Enabled = True
                chkShowInactiveDependents.Checked = False
                chkShowInactiveDependents.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowExemptedDependents_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkIncludeInactiveDependents_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveDependents.CheckedChanged, chkShowInactiveDependents.CheckedChanged
        Try
            If CType(sender, CheckBox).Name = "chkIncludeInactiveDependents" AndAlso CType(sender, CheckBox).Checked Then
                chkShowInactiveDependents.Checked = False
                pnlInactivedependentoption.Enabled = True
                cboCondition.Enabled = False
                cboCondition.SelectedValue = 1 '>
            ElseIf CType(sender, CheckBox).Name = "chkShowInactiveDependents" AndAlso CType(sender, CheckBox).Checked Then
                chkIncludeInactiveDependents.Checked = False
                pnlInactivedependentoption.Enabled = True
                cboCondition.Enabled = False
                cboCondition.SelectedValue = 2 '<
            Else
                cboCondition.SelectedValue = 5 'EQUAL TO
                cboCondition.Enabled = True
                rdAgeLimitWise.Checked = True
                rdExemptionWise.Checked = False
                pnlInactivedependentoption.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIncludeInactiveDependents_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Radio Button Event"

    Private Sub rdAgeLimitWise_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdAgeLimitWise.CheckedChanged, rdExemptionWise.CheckedChanged
        Try
            If CType(sender, RadioButton).Name = rdAgeLimitWise.Name AndAlso CType(sender, RadioButton).Checked Then
                cboCondition.SelectedValue = 1 '>
            ElseIf CType(sender, RadioButton).Name = rdExemptionWise.Name AndAlso CType(sender, RadioButton).Checked Then
                cboCondition.SelectedValue = 2 '<
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdAgeLimitWise_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (16-AUG-2014) -- End

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLastname.Text = Language._Object.getCaption(Me.lblLastname.Name, Me.lblLastname.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
			Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.chkshowdependantswithoutphotos.Text = Language._Object.getCaption(Me.chkshowdependantswithoutphotos.Name, Me.chkshowdependantswithoutphotos.Text)
			Me.chkIncludeInactiveDependents.Text = Language._Object.getCaption(Me.chkIncludeInactiveDependents.Name, Me.chkIncludeInactiveDependents.Text)
			Me.chkShowExemptedDependents.Text = Language._Object.getCaption(Me.chkShowExemptedDependents.Name, Me.chkShowExemptedDependents.Text)
			Me.chkShowInactiveDependents.Text = Language._Object.getCaption(Me.chkShowInactiveDependents.Name, Me.chkShowInactiveDependents.Text)
			Me.LblAsonDate.Text = Language._Object.getCaption(Me.LblAsonDate.Name, Me.LblAsonDate.Text)
			Me.LblCondition.Text = Language._Object.getCaption(Me.LblCondition.Name, Me.LblCondition.Text)
			Me.lnInActiveDependentOption.Text = Language._Object.getCaption(Me.lnInActiveDependentOption.Name, Me.lnInActiveDependentOption.Text)
			Me.rdExemptionWise.Text = Language._Object.getCaption(Me.rdExemptionWise.Name, Me.rdExemptionWise.Text)
			Me.rdAgeLimitWise.Text = Language._Object.getCaption(Me.rdAgeLimitWise.Name, Me.rdAgeLimitWise.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Dependants Wise")
			Language.setMessage(mstrModuleName, 2, "Referee Wise")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

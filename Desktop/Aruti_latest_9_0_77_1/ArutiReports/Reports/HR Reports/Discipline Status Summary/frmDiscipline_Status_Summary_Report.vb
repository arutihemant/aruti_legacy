#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDiscipline_Status_Summary_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDiscipline_Status_Summary_Report"
    Private objDiscipline_Summary As clsDiscipline_Status_Summary_Report

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        objDiscipline_Summary = New clsDiscipline_Status_Summary_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
    End Sub

#End Region

#Region " Forms "

    Private Sub frmDiscipline_Status_Summary_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objDiscipline_Summary = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Status_Summary_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Status_Summary_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objDiscipline_Summary._ReportName
            Me._Message = objDiscipline_Summary._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDiscipline_Status_Summary_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Status_Summary_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    'Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Status_Summary_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDiscipline_Status_Summary_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDiscipline_Status_Summary_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objDisciplineStatus As New clsDiscipline_Status
            dsList = objDisciplineStatus.getComboList("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboStatus.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDiscipline_Summary.SetDefaultValue()

            objDiscipline_Summary._DisciplineStatusId = cboStatus.SelectedValue
            objDiscipline_Summary._DisciplineStatus = cboStatus.Text

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Button's Event"

    Private Sub frmEmployeeWorkingStation_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDiscipline_Summary.generateReport(0, e.Type, enExportAction.None)
            objDiscipline_Summary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, _
                                                    ConfigParameter._Object._OpenAfterExport, _
                                                    0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWorkingStation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWorkingStation_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDiscipline_Summary.generateReport(0, enPrintAction.None, e.Type)
            objDiscipline_Summary.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                    ConfigParameter._Object._ExportReportPath, _
                                                    ConfigParameter._Object._OpenAfterExport, _
                                                    0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWorkingStation_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWorkingStation_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWorkingStation_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeWorkingStation_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeWorkingStation_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboStatus
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.DataSource = .DataSource
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmp_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Status_Summary_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Status_Summary_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

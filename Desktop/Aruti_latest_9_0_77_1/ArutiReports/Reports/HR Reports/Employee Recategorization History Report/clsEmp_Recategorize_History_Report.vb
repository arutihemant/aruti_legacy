'************************************************************************************************************************************
'Class Name : clsEmp_RecategorizationHistory_Report.vb
'Purpose    :
'Date       : 30/03/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsEmp_RecategorizationHistory_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmp_RecategorizationHistory_Report"
    Private mstrReportId As String = enArutiReport.Emp_RecategorizationHistory_Report  '251
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Dim mblnIncludeInactiveEmp As Boolean = False
    Private mstrOrderByQuery As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mblnIncludeInactiveEmp = False
            mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery(ByVal strEmployeeAsOnDate As String)
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            If mdtFromDate <> Nothing And mdtToDate <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date: ") & " " & mdtFromDate.Date & " " & Language.getMessage(mstrModuleName, 2, "To Date: ") & " " & mdtToDate.Date & " "
                objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromDate))
                objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtToDate))
            Else
                objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
                objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsOnDate)
            End If

            If mintEmployeeId > 0 Then
                Me._FilterQuery &= " AND #Final.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee: ") & " " & mstrEmployeeName & " "
            End If

            If mintViewIndex > 0 Then
                mstrOrderByQuery &= "ORDER BY GName,StaffNo,effectiveDate"
            Else
                mstrOrderByQuery &= "  ORDER BY StaffNo,effectiveDate"
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Order By : ") & " " & Me.OrderByDisplay

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
  
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String _
                                                              , ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean _
                                                              , ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview _
                                                              , Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                                              , Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("effectivedate", Language.getMessage(mstrModuleName, 38, "Effective Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtFromDate, mdtToDate, , , xDatabaseName)
                If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtToDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtToDate, xDatabaseName)
            Else
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
                If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmployeeAsOnDate), xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)
            End If

            

            StrQ = " IF OBJECT_ID('tempdb..#Final') IS NOT NULL DROP TABLE #Final; " & _
                       " IF OBJECT_ID('tempdb..#emp') IS NOT NULL DROP TABLE #emp; " & _
                       " IF OBJECT_ID('tempdb..#full') IS NOT NULL DROP TABLE #full; " & _
                       " IF OBJECT_ID('tempdb..#tab') IS NOT NULL DROP TABLE #tab; " & _
                       " CREATE TABLE #full " & _
                       " ( " & _
                       "        employeeunkid INT " & _
                       "       ,jobunkid INT " & _
                       "       ,effectivedate DATETIME  " & _
                       "       ,changereasonunkid INT  " & _
                       "       ,Id INT  " & _
                       "       ,GName NVARCHAR(MAX)  " & _
                       "); " & _
                       " IF OBJECT_ID('tempdb..#tab2') IS NOT NULL DROP TABLE #tab2; " & _
                       " IF OBJECT_ID('tempdb..#tab3') IS NOT NULL DROP TABLE #tab3; " & _
                       " SELECT " & _
                       "      hremployee_master.employeeunkid " & _
                       "    , companyunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 As Id, '' AS GName "
            End If

            StrQ &= " INTO #emp " & _
                       " FROM hremployee_master "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If mblnIncludeInactiveEmp = False Then
            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry
            '    End If
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            StrQ &= " LEFT JOIN  " & _
                       " ( " & _
                       "        SELECT  TERM.TEemployeeunkid " & _
                       "                   ,TERM.empl_enddate " & _
                       "                   ,TERM.termination_from_date " & _
                       "                   ,TERM.TEfDt " & _
                       "                   ,TERM.isexclude_payroll " & _
                       "        FROM " & _
                       "        ( " & _
                       "                SELECT TRM.employeeunkid AS TEemployeeunkid " & _
                       "                            ,CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                       "                            ,CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                       "                            ,CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                       "                            ,TRM.isexclude_payroll " & _
                       "                            ,ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                       "                FROM hremployee_dates_tran AS TRM " & _
                       "                WHERE isvoid = 0 	AND TRM.datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & _
                       "                AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                       "        ) AS TERM    WHERE TERM.Rno = 1 " & _
                       " ) AS ETERM ON ETERM.TEemployeeunkid = hremployee_master.employeeunkid " & _
                       " LEFT JOIN  " & _
                       " ( " & _
                       "        SELECT RET.Remployeeunkid " & _
                       "                   ,RET.termination_to_date " & _
                       "                   ,RET.REfDt " & _
                       "        FROM ( " & _
                       "                        SELECT RTD.employeeunkid AS Remployeeunkid " & _
                       "                                   ,CONVERT(CHAR(8), RTD.date1, 112) AS termination_to_date " & _
                       "                                   ,CONVERT(CHAR(8), RTD.effectivedate, 112) AS REfDt " & _
                       "                                   ,ROW_NUMBER() OVER (PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC ) AS Rno " & _
                       "                        FROM hremployee_dates_tran AS RTD " & _
                       "                        WHERE isvoid = 0 AND RTD.datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & _
                       "                        AND CONVERT(CHAR(8), RTD.effectivedate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                       "                 ) AS RET WHERE RET.Rno = 1 " & _
                       " ) AS ERET ON ERET.Remployeeunkid = hremployee_master.employeeunkid " & _
                       " WHERE hremployee_master.isapproved = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If



            StrQ &= "; SELECT " & _
                          "         ROW_NUMBER()OVER(ORDER BY A.database_name ASC) AS id " & _
                          "        ,A.database_name " & _
                          " INTO #tab FROM " & _
                          "( " & _
                          "    Select DISTINCT " & _
                          "              ft.database_name " & _
                          "    FROM hrmsConfiguration..cffinancial_year_tran AS ft " & _
                          "   JOIN #emp ON ft.companyunkid = #emp.companyunkid " & _
                          ") AS A ;" & _
                          " DECLARE @SQ AS NVARCHAR(MAX) " & _
                          " DECLARE @Counter INT " & _
                          " SET @Counter=1 " & _
                          " SET @SQ = '' " & _
                          " WHILE (@Counter <= (SELECT MAX(id) FROM #tab)) " & _
                          " BEGIN " & _
                          "     DECLARE @name AS NVARCHAR(MAX) " & _
                          "     SET @name = (SELECT database_name FROM #tab WHERE id = @Counter) " & _
                          "     SET @SQ = @SQ + 'INSERT INTO #full(employeeunkid,jobunkid,effectivedate,changereasonunkid,Id,GName) SELECT hremployee_categorization_tran.employeeunkid,jobunkid,effectivedate,changereasonunkid,#emp.Id,#emp.GName FROM ' + @name +'..hremployee_categorization_tran JOIN #emp ON #emp.employeeunkid = hremployee_categorization_tran.employeeunkid WHERE isvoid = 0 ' " & _
                          "     SET @Counter = @Counter + 1" & _
                          "     EXEC(@SQ) " & _
                          " END;"


            StrQ &= " SELECT DISTINCT * INTO #tab2 FROM #full  "

            If mdtFromDate <> Nothing AndAlso mdtToDate <> Nothing Then
                StrQ &= " WHERE CONVERT(NVARCHAR(8),effectivedate,112) BETWEEN @StartDate AND @EndDate"
            End If

            StrQ &= " SELECT * INTO #tab3 from #tab2 WHERE employeeunkid IN (SELECT employeeunkid FROM #tab2 GROUP BY employeeunkid HAVING COUNT(employeeunkid) = 1) "


            StrQ &= " INSERT INTO #tab2(employeeunkid,jobunkid,effectivedate,changereasonunkid,Id,GName) " & _
                         " SELECT " & _
                         "      a.employeeunkid " & _
                         "     ,a.jobunkid " & _
                         "     ,a.effectivedate " & _
                         "     ,a.changereasonunkid " & _
                         "     ,a.Id " & _
                         "     ,a.GName " & _
                         "  FROM " & _
                         " ( " & _
                         "      SELECT #full.* " & _
                         "               ,ROW_NUMBER() OVER (PARTITION BY #full.employeeunkid ORDER BY CONVERT(NVARCHAR(8),#full.effectivedate,112) DESC) as rno " & _
                         "      FROM #full " & _
                         "      LEFT JOIN #tab3 on #full.employeeunkid = #tab3.employeeunkid AND CONVERT(NVARCHAR(8),#full.effectivedate,112) < CONVERT(NVARCHAR(8),#tab3.effectivedate,112) " & _
                         "      JOIN #tab2 on #full.employeeunkid = #tab2.employeeunkid AND CONVERT(NVARCHAR(8),#full.effectivedate,112) <> CONVERT(NVARCHAR(8),#tab2.effectivedate,112) " & _
                         " ) as a WHERE a.rno = 1 AND NOT EXISTS (SELECT * FROM #tab2 WHERE employeeunkid = a.employeeunkid AND CONVERT(NVARCHAR(8),a.effectivedate,112) = CONVERT(NVARCHAR(8),effectivedate,112)) "


            StrQ &= " SELECT  ROW_NUMBER() over (ORDER BY staffno) as RowNo  , * FROM  " & _
                          " ( " & _
                          "      SELECT " & _
                          "             hremployee_master.employeeunkid  " & _
                          "           , ISNULL(hremployee_master.employeecode,'') AS StaffNo " & _
                          "           ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Staff " & _
                          "           ,ROW_NUMBER() OVER (PARTITION BY #tab2.employeeunkid,#tab2.jobunkid ORDER BY CONVERT(NVARCHAR(8),#tab2.effectivedate,112) DESC) as rno " & _
                          "           ,CONVERT(NVARCHAR(8),#tab2.effectivedate,112) AS effectivedate  " & _
                          "           ,ISNULL(hrjob_master.job_name,'') AS Job " & _
                          "           ,ISNULL(cfcommon_master.name,'') AS Reason " & _
                          "           ,#tab2.Id " & _
                          "           ,#tab2.GName " & _
                          "     FROM #tab2 " & _
                          "     LEFT Join hremployee_master ON #tab2.employeeunkid = hremployee_master.employeeunkid " & _
                          "     LEFT Join hrjob_master ON #tab2.jobunkid = hrjob_master.jobunkid " & _
                          "     LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = #tab2.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                          " ) AS #Final WHERE rno = 1 "

            Call FilterTitleAndFilterQuery(strEmployeeAsOnDate)

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim dtTable As New DataTable("History")

            Dim dcColumn As New DataColumn(Language.getMessage(mstrModuleName, 5, "Staff No"), Type.GetType("System.String"))
            dcColumn.DefaultValue = ""
            dtTable.Columns.Add(dcColumn)

            dcColumn = Nothing
            dcColumn = New DataColumn(Language.getMessage(mstrModuleName, 6, "Staff"), Type.GetType("System.String"))
            dcColumn.DefaultValue = ""
            dtTable.Columns.Add(dcColumn)

            dcColumn = New DataColumn(Language.getMessage(mstrModuleName, 7, "Effective Date"), Type.GetType("System.String"))
            dcColumn.DefaultValue = DBNull.Value
            dtTable.Columns.Add(dcColumn)

            dcColumn = New DataColumn(Language.getMessage(mstrModuleName, 8, "Previous"), Type.GetType("System.String"))
            dcColumn.DefaultValue = ""
            dtTable.Columns.Add(dcColumn)

            dcColumn = New DataColumn(Language.getMessage(mstrModuleName, 9, "New"), Type.GetType("System.String"))
            dcColumn.DefaultValue = ""
            dtTable.Columns.Add(dcColumn)

            dcColumn = New DataColumn(Language.getMessage(mstrModuleName, 10, "Reason"), Type.GetType("System.String"))
            dcColumn.DefaultValue = ""
            dtTable.Columns.Add(dcColumn)

            If mstrReport_GroupName.Trim.Length > 0 Then
                dcColumn = New DataColumn("GName", Type.GetType("System.String"))
                dcColumn.DefaultValue = ""
                dtTable.Columns.Add(dcColumn)
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim xEmployeeId As Integer = 0
                Dim mstrPrevJob As String = ""

                For Each dr As DataRow In dsList.Tables(0).Rows
                    Dim drRow As DataRow = Nothing

                    If xEmployeeId <> CInt(dr("employeeunkid")) Then
                        xEmployeeId = CInt(dr("employeeunkid"))
                        mstrPrevJob = ""
                    End If

                    'Dim xCount = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(dr("employeeunkid"))).Count
                    'If xCount > 1 Then

                    Dim dCount As DataRow() = dsList.Tables(0).Select("employeeunkid = " & CInt(dr("employeeunkid")))
                    If dCount IsNot Nothing AndAlso dCount.Length > 0 Then
                        'Dim mintCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)(Language.getMessage(mstrModuleName, 1111, "Staff No")) = dr("StaffNo").ToString()).Count
                        'If mintCount <= 0 Then
                        Dim drCount As DataRow() = dtTable.Select("[" & Language.getMessage(mstrModuleName, 5, "Staff No") & "] = '" & dr("StaffNo").ToString() & "'")
                        If drCount IsNot Nothing AndAlso drCount.Length <= 0 Then
                            drRow = dtTable.NewRow
                            drRow(Language.getMessage(mstrModuleName, 5, "Staff No")) = dr("StaffNo").ToString()
                            drRow(Language.getMessage(mstrModuleName, 6, "Staff")) = dr("Staff").ToString()
                            drRow(Language.getMessage(mstrModuleName, 7, "Effective Date")) = eZeeDate.convertDate(dr("effectivedate").ToString()).ToShortDateString()
                            drRow(Language.getMessage(mstrModuleName, 8, "Previous")) = ""
                            drRow(Language.getMessage(mstrModuleName, 9, "New")) = dr("Job").ToString()
                            drRow(Language.getMessage(mstrModuleName, 10, "Reason")) = dr("Reason").ToString()
                            If mstrReport_GroupName.Trim.Length > 0 Then
                                drRow("GName") = dr("GName").ToString()
                            End If
                            mstrPrevJob = dr("Job").ToString()
                            dtTable.Rows.Add(drRow)
                        Else
                            'Dim dRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)(Language.getMessage(mstrModuleName, 1111, "Staff No")) = dr("StaffNo").ToString() And x.Field(Of String)(Language.getMessage(mstrModuleName, 1114, "Previous")) <> "" And x.Field(Of String)(Language.getMessage(mstrModuleName, 1115, "New")) <> "")
                            'If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                            Dim dRow As DataRow() = dtTable.Select("[" & Language.getMessage(mstrModuleName, 5, "Staff No") & "] = '" & dr("StaffNo").ToString() & "' AND [" & Language.getMessage(mstrModuleName, 8, "Previous") & "] <> '' AND [" & Language.getMessage(mstrModuleName, 9, "New") & "] <> '' ")
                            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                                drRow = dtTable.NewRow
                                drRow(Language.getMessage(mstrModuleName, 5, "Staff No")) = dr("StaffNo").ToString()
                                drRow(Language.getMessage(mstrModuleName, 6, "Staff")) = dr("Staff").ToString()
                                drRow(Language.getMessage(mstrModuleName, 7, "Effective Date")) = eZeeDate.convertDate(dr("effectivedate").ToString()).ToShortDateString()
                                drRow(Language.getMessage(mstrModuleName, 8, "Previous")) = mstrPrevJob
                                drRow(Language.getMessage(mstrModuleName, 9, "New")) = dr("Job").ToString()
                                drRow(Language.getMessage(mstrModuleName, 10, "Reason")) = dr("Reason").ToString()
                                If mstrReport_GroupName.Trim.Length > 0 Then
                                    drRow("GName") = dr("GName").ToString()
                                End If
                                mstrPrevJob = dr("Job").ToString()
                                dtTable.Rows.Add(drRow)
                            Else
                                'dRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of String)(Language.getMessage(mstrModuleName, 1111, "Staff No")) = dr("StaffNo").ToString())
                                'If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                                dRow = dtTable.Select("[" & Language.getMessage(mstrModuleName, 5, "Staff No") & "] =  '" & dr("StaffNo").ToString() & "'")
                                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                                    dRow(0)(Language.getMessage(mstrModuleName, 7, "Effective Date")) = eZeeDate.convertDate(dr("effectivedate").ToString()).ToShortDateString()
                                    dRow(0)(Language.getMessage(mstrModuleName, 8, "Previous")) = mstrPrevJob
                                    dRow(0)(Language.getMessage(mstrModuleName, 9, "New")) = dr("Job").ToString()
                                    dRow(0)(Language.getMessage(mstrModuleName, 10, "Reason")) = dr("Reason").ToString()
                                    dRow(0).AcceptChanges()
                                    dtTable.AcceptChanges()
                                    mstrPrevJob = dr("Job").ToString()
                                End If
                            End If
                        End If
                    Else
                        drRow = dtTable.NewRow
                        drRow(Language.getMessage(mstrModuleName, 5, "Staff No")) = dr("StaffNo").ToString()
                        drRow(Language.getMessage(mstrModuleName, 6, "Staff")) = dr("Staff").ToString()
                        drRow(Language.getMessage(mstrModuleName, 7, "Effective Date")) = eZeeDate.convertDate(dr("effectivedate").ToString()).ToShortDateString()
                        drRow(Language.getMessage(mstrModuleName, 8, "Previous")) = ""
                        drRow(Language.getMessage(mstrModuleName, 9, "New")) = dr("Job").ToString()
                        drRow(Language.getMessage(mstrModuleName, 10, "Reason")) = dr("Reason").ToString()
                        If mstrReport_GroupName.Trim.Length > 0 Then
                            drRow("GName") = dr("GName").ToString()
                        End If
                        mstrPrevJob = dr("Job").ToString()
                        dtTable.Rows.Add(drRow)
                    End If
                Next
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim mdtTableExcel As DataTable = dtTable.Clone

            If mintViewIndex > 0 Then
                mdtTableExcel = New DataView(dtTable, "", "GName,[Staff No]", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTableExcel = New DataView(dtTable, "", "[Staff No]", DataViewRowState.CurrentRows).ToTable
            End If

            If mintViewIndex > 0 Then
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If

            Dim columnNames As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, columnNames)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)
            '--------------------

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Date:")
			Language.setMessage(mstrModuleName, 2, "To Date:")
			Language.setMessage(mstrModuleName, 3, "Employee:")
			Language.setMessage(mstrModuleName, 4, " Order By :")
			Language.setMessage(mstrModuleName, 5, "Staff No")
			Language.setMessage(mstrModuleName, 6, "Staff")
			Language.setMessage(mstrModuleName, 7, "Effective Date")
			Language.setMessage(mstrModuleName, 8, "Previous")
			Language.setMessage(mstrModuleName, 9, "New")
			Language.setMessage(mstrModuleName, 10, "Reason")
			Language.setMessage(mstrModuleName, 11, "Prepared By :")
			Language.setMessage(mstrModuleName, 12, "Checked By :")
			Language.setMessage(mstrModuleName, 13, "Approved By")
			Language.setMessage(mstrModuleName, 14, "Received By :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

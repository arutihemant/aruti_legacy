﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessmentFormReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkProbationTemplate = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboFromYear = New System.Windows.Forms.ComboBox
        Me.gbMultiperiod = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.fpnlControls = New System.Windows.Forms.FlowLayoutPanel
        Me.gbYearlyBasis = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboToYear = New System.Windows.Forms.ComboBox
        Me.lblToYear = New System.Windows.Forms.Label
        Me.gbDisplayScore = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboScoreOption = New System.Windows.Forms.ComboBox
        Me.lblDisplayScore = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbMultiperiod.SuspendLayout()
        Me.fpnlControls.SuspendLayout()
        Me.gbYearlyBasis.SuspendLayout()
        Me.gbDisplayScore.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 469)
        Me.NavPanel.Size = New System.Drawing.Size(751, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkProbationTemplate)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(379, 111)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Mandatory Info"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkProbationTemplate
        '
        Me.chkProbationTemplate.AutoSize = True
        Me.chkProbationTemplate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkProbationTemplate.Location = New System.Drawing.Point(87, 88)
        Me.chkProbationTemplate.Name = "chkProbationTemplate"
        Me.chkProbationTemplate.Size = New System.Drawing.Size(119, 17)
        Me.chkProbationTemplate.TabIndex = 81
        Me.chkProbationTemplate.Text = "Probation Template"
        Me.chkProbationTemplate.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(73, 15)
        Me.lblPeriod.TabIndex = 79
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(87, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(256, 21)
        Me.cboPeriod.TabIndex = 77
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(349, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(87, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(256, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 37)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(73, 15)
        Me.lblYear.TabIndex = 122
        Me.lblYear.Text = "From Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromYear
        '
        Me.cboFromYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromYear.DropDownWidth = 180
        Me.cboFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromYear.FormattingEnabled = True
        Me.cboFromYear.Location = New System.Drawing.Point(87, 34)
        Me.cboFromYear.Name = "cboFromYear"
        Me.cboFromYear.Size = New System.Drawing.Size(256, 21)
        Me.cboFromYear.TabIndex = 121
        '
        'gbMultiperiod
        '
        Me.gbMultiperiod.BorderColor = System.Drawing.Color.Black
        Me.gbMultiperiod.Checked = False
        Me.gbMultiperiod.CollapseAllExceptThis = False
        Me.gbMultiperiod.CollapsedHoverImage = Nothing
        Me.gbMultiperiod.CollapsedNormalImage = Nothing
        Me.gbMultiperiod.CollapsedPressedImage = Nothing
        Me.gbMultiperiod.CollapseOnLoad = False
        Me.gbMultiperiod.Controls.Add(Me.lblToPeriod)
        Me.gbMultiperiod.Controls.Add(Me.cboToPeriod)
        Me.gbMultiperiod.Controls.Add(Me.lblFromPeriod)
        Me.gbMultiperiod.Controls.Add(Me.cboFromPeriod)
        Me.gbMultiperiod.ExpandedHoverImage = Nothing
        Me.gbMultiperiod.ExpandedNormalImage = Nothing
        Me.gbMultiperiod.ExpandedPressedImage = Nothing
        Me.gbMultiperiod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMultiperiod.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMultiperiod.HeaderHeight = 25
        Me.gbMultiperiod.HeaderMessage = ""
        Me.gbMultiperiod.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMultiperiod.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMultiperiod.HeightOnCollapse = 0
        Me.gbMultiperiod.LeftTextSpace = 0
        Me.gbMultiperiod.Location = New System.Drawing.Point(3, 191)
        Me.gbMultiperiod.Name = "gbMultiperiod"
        Me.gbMultiperiod.OpenHeight = 300
        Me.gbMultiperiod.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMultiperiod.ShowBorder = True
        Me.gbMultiperiod.ShowCheckBox = False
        Me.gbMultiperiod.ShowCollapseButton = False
        Me.gbMultiperiod.ShowDefaultBorderColor = True
        Me.gbMultiperiod.ShowDownButton = False
        Me.gbMultiperiod.ShowHeader = True
        Me.gbMultiperiod.Size = New System.Drawing.Size(379, 93)
        Me.gbMultiperiod.TabIndex = 123
        Me.gbMultiperiod.Temp = 0
        Me.gbMultiperiod.Text = "Period Selection"
        Me.gbMultiperiod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(73, 15)
        Me.lblToPeriod.TabIndex = 84
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.DropDownWidth = 180
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(87, 60)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(256, 21)
        Me.cboToPeriod.TabIndex = 83
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(73, 15)
        Me.lblFromPeriod.TabIndex = 82
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.DropDownWidth = 180
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(87, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(256, 21)
        Me.cboFromPeriod.TabIndex = 81
        '
        'fpnlControls
        '
        Me.fpnlControls.AutoSize = True
        Me.fpnlControls.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.fpnlControls.Controls.Add(Me.gbFilterCriteria)
        Me.fpnlControls.Controls.Add(Me.gbDisplayScore)
        Me.fpnlControls.Controls.Add(Me.gbMultiperiod)
        Me.fpnlControls.Controls.Add(Me.gbYearlyBasis)
        Me.fpnlControls.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.fpnlControls.Location = New System.Drawing.Point(12, 66)
        Me.fpnlControls.Name = "fpnlControls"
        Me.fpnlControls.Size = New System.Drawing.Size(385, 386)
        Me.fpnlControls.TabIndex = 124
        '
        'gbYearlyBasis
        '
        Me.gbYearlyBasis.BorderColor = System.Drawing.Color.Black
        Me.gbYearlyBasis.Checked = False
        Me.gbYearlyBasis.CollapseAllExceptThis = False
        Me.gbYearlyBasis.CollapsedHoverImage = Nothing
        Me.gbYearlyBasis.CollapsedNormalImage = Nothing
        Me.gbYearlyBasis.CollapsedPressedImage = Nothing
        Me.gbYearlyBasis.CollapseOnLoad = False
        Me.gbYearlyBasis.Controls.Add(Me.cboToYear)
        Me.gbYearlyBasis.Controls.Add(Me.lblToYear)
        Me.gbYearlyBasis.Controls.Add(Me.cboFromYear)
        Me.gbYearlyBasis.Controls.Add(Me.lblYear)
        Me.gbYearlyBasis.ExpandedHoverImage = Nothing
        Me.gbYearlyBasis.ExpandedNormalImage = Nothing
        Me.gbYearlyBasis.ExpandedPressedImage = Nothing
        Me.gbYearlyBasis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbYearlyBasis.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbYearlyBasis.HeaderHeight = 25
        Me.gbYearlyBasis.HeaderMessage = ""
        Me.gbYearlyBasis.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbYearlyBasis.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbYearlyBasis.HeightOnCollapse = 0
        Me.gbYearlyBasis.LeftTextSpace = 0
        Me.gbYearlyBasis.Location = New System.Drawing.Point(3, 290)
        Me.gbYearlyBasis.Name = "gbYearlyBasis"
        Me.gbYearlyBasis.OpenHeight = 300
        Me.gbYearlyBasis.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbYearlyBasis.ShowBorder = True
        Me.gbYearlyBasis.ShowCheckBox = False
        Me.gbYearlyBasis.ShowCollapseButton = False
        Me.gbYearlyBasis.ShowDefaultBorderColor = True
        Me.gbYearlyBasis.ShowDownButton = False
        Me.gbYearlyBasis.ShowHeader = True
        Me.gbYearlyBasis.Size = New System.Drawing.Size(379, 93)
        Me.gbYearlyBasis.TabIndex = 124
        Me.gbYearlyBasis.Temp = 0
        Me.gbYearlyBasis.Text = "Year Selection"
        Me.gbYearlyBasis.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbYearlyBasis.Visible = False
        '
        'cboToYear
        '
        Me.cboToYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToYear.DropDownWidth = 180
        Me.cboToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToYear.FormattingEnabled = True
        Me.cboToYear.Location = New System.Drawing.Point(87, 61)
        Me.cboToYear.Name = "cboToYear"
        Me.cboToYear.Size = New System.Drawing.Size(256, 21)
        Me.cboToYear.TabIndex = 123
        '
        'lblToYear
        '
        Me.lblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToYear.Location = New System.Drawing.Point(8, 64)
        Me.lblToYear.Name = "lblToYear"
        Me.lblToYear.Size = New System.Drawing.Size(73, 15)
        Me.lblToYear.TabIndex = 124
        Me.lblToYear.Text = "To Year"
        Me.lblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbDisplayScore
        '
        Me.gbDisplayScore.BorderColor = System.Drawing.Color.Black
        Me.gbDisplayScore.Checked = False
        Me.gbDisplayScore.CollapseAllExceptThis = False
        Me.gbDisplayScore.CollapsedHoverImage = Nothing
        Me.gbDisplayScore.CollapsedNormalImage = Nothing
        Me.gbDisplayScore.CollapsedPressedImage = Nothing
        Me.gbDisplayScore.CollapseOnLoad = False
        Me.gbDisplayScore.Controls.Add(Me.cboScoreOption)
        Me.gbDisplayScore.Controls.Add(Me.lblDisplayScore)
        Me.gbDisplayScore.ExpandedHoverImage = Nothing
        Me.gbDisplayScore.ExpandedNormalImage = Nothing
        Me.gbDisplayScore.ExpandedPressedImage = Nothing
        Me.gbDisplayScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDisplayScore.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDisplayScore.HeaderHeight = 25
        Me.gbDisplayScore.HeaderMessage = ""
        Me.gbDisplayScore.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDisplayScore.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDisplayScore.HeightOnCollapse = 0
        Me.gbDisplayScore.LeftTextSpace = 0
        Me.gbDisplayScore.Location = New System.Drawing.Point(3, 120)
        Me.gbDisplayScore.Name = "gbDisplayScore"
        Me.gbDisplayScore.OpenHeight = 300
        Me.gbDisplayScore.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDisplayScore.ShowBorder = True
        Me.gbDisplayScore.ShowCheckBox = False
        Me.gbDisplayScore.ShowCollapseButton = False
        Me.gbDisplayScore.ShowDefaultBorderColor = True
        Me.gbDisplayScore.ShowDownButton = False
        Me.gbDisplayScore.ShowHeader = True
        Me.gbDisplayScore.Size = New System.Drawing.Size(379, 65)
        Me.gbDisplayScore.TabIndex = 125
        Me.gbDisplayScore.Temp = 0
        Me.gbDisplayScore.Text = "Show Final Score"
        Me.gbDisplayScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbDisplayScore.Visible = False
        '
        'cboScoreOption
        '
        Me.cboScoreOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScoreOption.DropDownWidth = 180
        Me.cboScoreOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScoreOption.FormattingEnabled = True
        Me.cboScoreOption.Location = New System.Drawing.Point(87, 34)
        Me.cboScoreOption.Name = "cboScoreOption"
        Me.cboScoreOption.Size = New System.Drawing.Size(256, 21)
        Me.cboScoreOption.TabIndex = 121
        '
        'lblDisplayScore
        '
        Me.lblDisplayScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayScore.Location = New System.Drawing.Point(8, 37)
        Me.lblDisplayScore.Name = "lblDisplayScore"
        Me.lblDisplayScore.Size = New System.Drawing.Size(73, 15)
        Me.lblDisplayScore.TabIndex = 122
        Me.lblDisplayScore.Text = "Display"
        Me.lblDisplayScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmAssessmentFormReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 524)
        Me.Controls.Add(Me.fpnlControls)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessmentFormReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.fpnlControls, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbMultiperiod.ResumeLayout(False)
        Me.fpnlControls.ResumeLayout(False)
        Me.gbYearlyBasis.ResumeLayout(False)
        Me.gbDisplayScore.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboFromYear As System.Windows.Forms.ComboBox
    Friend WithEvents gbMultiperiod As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents fpnlControls As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents gbYearlyBasis As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboToYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblToYear As System.Windows.Forms.Label
    Friend WithEvents chkProbationTemplate As System.Windows.Forms.CheckBox
    Friend WithEvents gbDisplayScore As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboScoreOption As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisplayScore As System.Windows.Forms.Label
End Class

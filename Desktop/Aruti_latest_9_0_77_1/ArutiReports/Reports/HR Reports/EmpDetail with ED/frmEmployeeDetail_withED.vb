#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeDetail_withED

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDetail_BBL"
    Private objEmployeeDetail As clsEmployeeDetail_withED
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvanceFilter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        objEmployeeDetail = New clsEmployeeDetail_withED(User._Object._Languageunkid,Company._Object._Companyunkid)
        'S.SANDEEP [ 13 FEB 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        _Show_AdvanceFilter = True
        'S.SANDEEP [ 13 FEB 2013 ] -- END
    End Sub

#End Region

#Region " Forms "

    Private Sub frmEmployeeDetail_BBL_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeDetail = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeDetail_BBL_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objEmployeeDetail._ReportName
            Me._Message = objEmployeeDetail._ReportDesc
            Call FillCombo()
            Call ResetValue()


            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Visible = User._Object.Privilege._AllowTo_View_Scale
            'Pinkal (30-Apr-2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    'Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeDetail_BBL_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmployeeDetail_BBL_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objDept As New clsDepartment
            Dim objJob As New clsJobs
            Dim objPeriod As New clscommom_period_Tran
            Dim objTranHead As New clsTransactionHead
            Dim objMembership As New clsmembership_master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables("Employee")

            dsList = objDept.getComboList("Department", True)
            cboDepartment.DisplayMember = "name"
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DataSource = dsList.Tables("Department")

            dsList = objJob.getComboList("Job", True)
            cboJob.DisplayMember = "name"
            cboJob.ValueMember = "jobunkid"
            cboJob.DataSource = dsList.Tables("Job")

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.DisplayMember = "name"
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DataSource = dsList.Tables("Period")

            dsList = objMembership.getListForCombo("Membership", True)
            cboMembership.DisplayMember = "name"
            cboMembership.ValueMember = "membershipunkid"
            cboMembership.DataSource = dsList.Tables("Membership")

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            'Sohail (21 Aug 2015) -- End
            cboTranHead1.DisplayMember = "name"
            cboTranHead1.ValueMember = "tranheadunkid"
            cboTranHead1.DataSource = dsList.Tables("TranHead")

            cboTranHead2.DisplayMember = "name"
            cboTranHead2.ValueMember = "tranheadunkid"
            cboTranHead2.DataSource = dsList.Tables("TranHead").Copy()

            cboTranHead3.DisplayMember = "name"
            cboTranHead3.ValueMember = "tranheadunkid"
            cboTranHead3.DataSource = dsList.Tables("TranHead").Copy()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboTranHead1.SelectedValue = 0
            cboTranHead2.SelectedValue = 0
            cboTranHead3.SelectedValue = 0
            objEmployeeDetail.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmployeeDetail.OrderByDisplay
            chkInactiveemp.Checked = False

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvanceFilter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Checked = False
            'Pinkal (24-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmployeeDetail.SetDefaultValue()

            objEmployeeDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEmployeeDetail._EmployeeName = cboEmployee.Text
            objEmployeeDetail._DepartmentId = CInt(cboDepartment.SelectedValue)
            objEmployeeDetail._Department = cboDepartment.Text
            objEmployeeDetail._JobId = CInt(cboJob.SelectedValue)
            objEmployeeDetail._Job = cboJob.Text
            objEmployeeDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmployeeDetail._Period = cboPeriod.Text
            objEmployeeDetail._MembershipId = CInt(cboMembership.SelectedValue)
            objEmployeeDetail._Membership = cboMembership.Text
            objEmployeeDetail._TranheadId1 = CInt(cboTranHead1.SelectedValue)
            objEmployeeDetail._TranHead1 = cboTranHead1.Text
            objEmployeeDetail._TranheadId2 = CInt(cboTranHead2.SelectedValue)
            objEmployeeDetail._TranHead2 = cboTranHead2.Text
            objEmployeeDetail._TranheadId3 = CInt(cboTranHead3.SelectedValue)
            objEmployeeDetail._TranHead3 = cboTranHead3.Text
            objEmployeeDetail._IncludeInactiveEmp = chkInactiveemp.Checked

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployeeDetail._Advance_Filter = mstrAdvanceFilter
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmployeeDetail._ShowEmployeeScale = chkShowEmpScale.Checked
            'Pinkal (24-Apr-2013) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmp_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDept.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboDepartment.DataSource, DataTable)
            With cboDepartment
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDept_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim objfrm As New frmCommonSearch
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboJob.DataSource, DataTable)
            With cboJob
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboPeriod.DataSource, DataTable)
            With cboPeriod
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchPeriod_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboMembership.DataSource, DataTable)
            With cboMembership
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead1.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead1.DataSource, DataTable)
            With cboTranHead1
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead1_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead2.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead2.DataSource, DataTable)
            With cboTranHead2
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead2_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnsearchTranHead3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnsearchTranHead3.Click
        Dim objfrm As New frmCommonSearch
        Dim dtTable As DataTable
        Try
            dtTable = CType(cboTranHead3.DataSource, DataTable)
            With cboTranHead3
                objfrm.DataSource = dtTable
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchTranHead3_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is compulsory information.Please Select Membership."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead1.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Transaction Head1 is compulsory information.Please Select Transaction Head1."), enMsgBoxStyle.Information)
                cboTranHead1.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead2.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Transaction Head2 is compulsory information.Please Select Transaction Head2."), enMsgBoxStyle.Information)
                cboTranHead2.Focus()
                Exit Sub
            ElseIf CInt(cboTranHead3.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Transaction Head3 is compulsory information.Please Select Transaction Head3."), enMsgBoxStyle.Information)
                cboTranHead3.Focus()
                Exit Sub
            End If


            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeDetail.generateReport(0, e.Type, enExportAction.None)
            Dim objperiod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEmployeeDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                objperiod._Start_Date, _
                                                objperiod._End_Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeDetail.generateReport(0, enPrintAction.None, e.Type)
            objEmployeeDetail.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeDetail_BBL_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDetail_BBL_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmployeeDetail.setOrderBy(0)
            txtOrderBy.Text = objEmployeeDetail.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDetail_withED.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDetail_BBL"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 13 FEB 2013 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
			Me.LblTranHead3.Text = Language._Object.getCaption(Me.LblTranHead3.Name, Me.LblTranHead3.Text)
			Me.LblTranHead2.Text = Language._Object.getCaption(Me.LblTranHead2.Name, Me.LblTranHead2.Text)
			Me.LblTranHead1.Text = Language._Object.getCaption(Me.LblTranHead1.Name, Me.LblTranHead1.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.Name, Me.chkShowEmpScale.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
			Language.setMessage(mstrModuleName, 2, "Membership is compulsory information.Please Select Membership.")
			Language.setMessage(mstrModuleName, 3, "Transaction Head1 is compulsory information.Please Select Transaction Head1.")
			Language.setMessage(mstrModuleName, 4, "Transaction Head2 is compulsory information.Please Select Transaction Head2.")
			Language.setMessage(mstrModuleName, 5, "Transaction Head3 is compulsory information.Please Select Transaction Head3.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

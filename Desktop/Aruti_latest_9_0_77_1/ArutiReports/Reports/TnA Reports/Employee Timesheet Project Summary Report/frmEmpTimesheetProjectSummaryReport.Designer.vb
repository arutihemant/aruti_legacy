﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpTimesheetProjectSummaryReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpTimesheetProjectSummaryReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowExtraHrs = New System.Windows.Forms.CheckBox
        Me.chkShowHolidayHrs = New System.Windows.Forms.CheckBox
        Me.chkShowLeaveTypeHrs = New System.Windows.Forms.CheckBox
        Me.chkShowEmpIdentifyNo = New System.Windows.Forms.CheckBox
        Me.lnkAnalysisBy = New System.Windows.Forms.LinkLabel
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.pnlGridPanel = New System.Windows.Forms.Panel
        Me.objSelectAll = New System.Windows.Forms.CheckBox
        Me.dgTransactionHead = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhTranheadcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTransactionHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranHeadID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtTransactionHeadFilter = New eZee.TextBox.AlphanumericTextBox
        Me.LblTransactionHead = New System.Windows.Forms.Label
        Me.LblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchProjectCode = New eZee.Common.eZeeGradientButton
        Me.LblProjectCode = New System.Windows.Forms.Label
        Me.cboProjectCode = New System.Windows.Forms.ComboBox
        Me.chkShowReportInHtml = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.chkIgnoreZeroLeaveTypehrs = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlGridPanel.SuspendLayout()
        CType(Me.dgTransactionHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZeroLeaveTypehrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowExtraHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowHolidayHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLeaveTypeHrs)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEmpIdentifyNo)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAnalysisBy)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSave)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.pnlGridPanel)
        Me.gbFilterCriteria.Controls.Add(Me.LblTransactionHead)
        Me.gbFilterCriteria.Controls.Add(Me.LblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.LblProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowReportInHtml)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(479, 380)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowExtraHrs
        '
        Me.chkShowExtraHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowExtraHrs.Location = New System.Drawing.Point(11, 355)
        Me.chkShowExtraHrs.Name = "chkShowExtraHrs"
        Me.chkShowExtraHrs.Size = New System.Drawing.Size(151, 18)
        Me.chkShowExtraHrs.TabIndex = 259
        Me.chkShowExtraHrs.Text = "Show Extra Hours"
        Me.chkShowExtraHrs.UseVisualStyleBackColor = True
        '
        'chkShowHolidayHrs
        '
        Me.chkShowHolidayHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowHolidayHrs.Location = New System.Drawing.Point(181, 331)
        Me.chkShowHolidayHrs.Name = "chkShowHolidayHrs"
        Me.chkShowHolidayHrs.Size = New System.Drawing.Size(176, 18)
        Me.chkShowHolidayHrs.TabIndex = 257
        Me.chkShowHolidayHrs.Text = "Show Holiday Hours"
        Me.chkShowHolidayHrs.UseVisualStyleBackColor = True
        '
        'chkShowLeaveTypeHrs
        '
        Me.chkShowLeaveTypeHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLeaveTypeHrs.Location = New System.Drawing.Point(11, 331)
        Me.chkShowLeaveTypeHrs.Name = "chkShowLeaveTypeHrs"
        Me.chkShowLeaveTypeHrs.Size = New System.Drawing.Size(151, 18)
        Me.chkShowLeaveTypeHrs.TabIndex = 256
        Me.chkShowLeaveTypeHrs.Text = "Show Leave Type Hours"
        Me.chkShowLeaveTypeHrs.UseVisualStyleBackColor = True
        '
        'chkShowEmpIdentifyNo
        '
        Me.chkShowEmpIdentifyNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmpIdentifyNo.Location = New System.Drawing.Point(181, 307)
        Me.chkShowEmpIdentifyNo.Name = "chkShowEmpIdentifyNo"
        Me.chkShowEmpIdentifyNo.Size = New System.Drawing.Size(176, 18)
        Me.chkShowEmpIdentifyNo.TabIndex = 254
        Me.chkShowEmpIdentifyNo.Text = "Show Employee Identify No "
        Me.chkShowEmpIdentifyNo.UseVisualStyleBackColor = True
        '
        'lnkAnalysisBy
        '
        Me.lnkAnalysisBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAnalysisBy.BackColor = System.Drawing.Color.Transparent
        Me.lnkAnalysisBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAnalysisBy.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAnalysisBy.Location = New System.Drawing.Point(383, 4)
        Me.lnkAnalysisBy.Name = "lnkAnalysisBy"
        Me.lnkAnalysisBy.Size = New System.Drawing.Size(93, 17)
        Me.lnkAnalysisBy.TabIndex = 201
        Me.lnkAnalysisBy.TabStop = True
        Me.lnkAnalysisBy.Text = "Analysis By"
        Me.lnkAnalysisBy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkSave
        '
        Me.lnkSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSave.BackColor = System.Drawing.Color.Transparent
        Me.lnkSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.Location = New System.Drawing.Point(359, 355)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(108, 18)
        Me.lnkSave.TabIndex = 252
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "Save Settings"
        Me.lnkSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 120
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(300, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(148, 21)
        Me.cboPeriod.TabIndex = 5
        '
        'pnlGridPanel
        '
        Me.pnlGridPanel.Controls.Add(Me.objSelectAll)
        Me.pnlGridPanel.Controls.Add(Me.dgTransactionHead)
        Me.pnlGridPanel.Controls.Add(Me.txtTransactionHeadFilter)
        Me.pnlGridPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlGridPanel.Location = New System.Drawing.Point(11, 134)
        Me.pnlGridPanel.Name = "pnlGridPanel"
        Me.pnlGridPanel.Size = New System.Drawing.Size(456, 166)
        Me.pnlGridPanel.TabIndex = 24
        '
        'objSelectAll
        '
        Me.objSelectAll.AutoSize = True
        Me.objSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSelectAll.Location = New System.Drawing.Point(9, 32)
        Me.objSelectAll.Name = "objSelectAll"
        Me.objSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objSelectAll.TabIndex = 246
        Me.objSelectAll.UseVisualStyleBackColor = True
        '
        'dgTransactionHead
        '
        Me.dgTransactionHead.AllowUserToAddRows = False
        Me.dgTransactionHead.AllowUserToDeleteRows = False
        Me.dgTransactionHead.AllowUserToResizeRows = False
        Me.dgTransactionHead.BackgroundColor = System.Drawing.Color.White
        Me.dgTransactionHead.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgTransactionHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhTranheadcode, Me.dgcolhTransactionHead, Me.objdgcolhTranHeadID})
        Me.dgTransactionHead.Location = New System.Drawing.Point(2, 26)
        Me.dgTransactionHead.Name = "dgTransactionHead"
        Me.dgTransactionHead.RowHeadersVisible = False
        Me.dgTransactionHead.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgTransactionHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgTransactionHead.Size = New System.Drawing.Size(452, 138)
        Me.dgTransactionHead.TabIndex = 249
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.Frozen = True
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhTranheadcode
        '
        Me.dgcolhTranheadcode.HeaderText = "Code"
        Me.dgcolhTranheadcode.Name = "dgcolhTranheadcode"
        Me.dgcolhTranheadcode.ReadOnly = True
        Me.dgcolhTranheadcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTranheadcode.Width = 120
        '
        'dgcolhTransactionHead
        '
        Me.dgcolhTransactionHead.HeaderText = "Transaction Head"
        Me.dgcolhTransactionHead.Name = "dgcolhTransactionHead"
        Me.dgcolhTransactionHead.ReadOnly = True
        Me.dgcolhTransactionHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTransactionHead.Width = 285
        '
        'objdgcolhTranHeadID
        '
        Me.objdgcolhTranHeadID.HeaderText = "TranHeadID"
        Me.objdgcolhTranHeadID.Name = "objdgcolhTranHeadID"
        Me.objdgcolhTranHeadID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhTranHeadID.Visible = False
        '
        'txtTransactionHeadFilter
        '
        Me.txtTransactionHeadFilter.Flags = 0
        Me.txtTransactionHeadFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransactionHeadFilter.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTransactionHeadFilter.Location = New System.Drawing.Point(2, 3)
        Me.txtTransactionHeadFilter.Name = "txtTransactionHeadFilter"
        Me.txtTransactionHeadFilter.Size = New System.Drawing.Size(452, 21)
        Me.txtTransactionHeadFilter.TabIndex = 250
        '
        'LblTransactionHead
        '
        Me.LblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTransactionHead.Location = New System.Drawing.Point(8, 116)
        Me.LblTransactionHead.Name = "LblTransactionHead"
        Me.LblTransactionHead.Size = New System.Drawing.Size(95, 15)
        Me.LblTransactionHead.TabIndex = 244
        Me.LblTransactionHead.Text = "Transaction Head"
        Me.LblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblReportType
        '
        Me.LblReportType.BackColor = System.Drawing.Color.Transparent
        Me.LblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblReportType.Location = New System.Drawing.Point(8, 37)
        Me.LblReportType.Name = "LblReportType"
        Me.LblReportType.Size = New System.Drawing.Size(95, 15)
        Me.LblReportType.TabIndex = 243
        Me.LblReportType.Text = "Report Type"
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 120
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(113, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(129, 21)
        Me.cboReportType.TabIndex = 241
        '
        'objbtnSearchProjectCode
        '
        Me.objbtnSearchProjectCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProjectCode.BorderSelected = False
        Me.objbtnSearchProjectCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProjectCode.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProjectCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProjectCode.Location = New System.Drawing.Point(454, 88)
        Me.objbtnSearchProjectCode.Name = "objbtnSearchProjectCode"
        Me.objbtnSearchProjectCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProjectCode.TabIndex = 238
        '
        'LblProjectCode
        '
        Me.LblProjectCode.BackColor = System.Drawing.Color.Transparent
        Me.LblProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblProjectCode.Location = New System.Drawing.Point(8, 91)
        Me.LblProjectCode.Name = "LblProjectCode"
        Me.LblProjectCode.Size = New System.Drawing.Size(95, 15)
        Me.LblProjectCode.TabIndex = 240
        Me.LblProjectCode.Text = "Project Code"
        '
        'cboProjectCode
        '
        Me.cboProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProjectCode.DropDownWidth = 120
        Me.cboProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProjectCode.FormattingEnabled = True
        Me.cboProjectCode.Location = New System.Drawing.Point(113, 88)
        Me.cboProjectCode.Name = "cboProjectCode"
        Me.cboProjectCode.Size = New System.Drawing.Size(337, 21)
        Me.cboProjectCode.TabIndex = 239
        '
        'chkShowReportInHtml
        '
        Me.chkShowReportInHtml.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReportInHtml.Location = New System.Drawing.Point(11, 307)
        Me.chkShowReportInHtml.Name = "chkShowReportInHtml"
        Me.chkShowReportInHtml.Size = New System.Drawing.Size(151, 18)
        Me.chkShowReportInHtml.TabIndex = 236
        Me.chkShowReportInHtml.Text = "Show Report In HTML"
        Me.chkShowReportInHtml.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(454, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 60
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(454, 34)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 104
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(113, 61)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(337, 21)
        Me.cboEmployee.TabIndex = 196
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 64)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(95, 15)
        Me.lblEmployee.TabIndex = 197
        Me.lblEmployee.Text = "Employee"
        '
        'LblPeriod
        '
        Me.LblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(246, 36)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(50, 16)
        Me.LblPeriod.TabIndex = 190
        Me.LblPeriod.Text = "Period"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 512)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(767, 55)
        Me.EZeeFooter1.TabIndex = 23
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(474, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(570, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(666, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(767, 60)
        Me.eZeeHeader.TabIndex = 22
        Me.eZeeHeader.Title = ""
        '
        'chkIgnoreZeroLeaveTypehrs
        '
        Me.chkIgnoreZeroLeaveTypehrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZeroLeaveTypehrs.Location = New System.Drawing.Point(181, 355)
        Me.chkIgnoreZeroLeaveTypehrs.Name = "chkIgnoreZeroLeaveTypehrs"
        Me.chkIgnoreZeroLeaveTypehrs.Size = New System.Drawing.Size(176, 18)
        Me.chkIgnoreZeroLeaveTypehrs.TabIndex = 261
        Me.chkIgnoreZeroLeaveTypehrs.Text = "Ignore Zero hours Leave Type"
        Me.chkIgnoreZeroLeaveTypehrs.UseVisualStyleBackColor = True
        '
        'frmEmpTimesheetProjectSummaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frmEmpTimesheetProjectSummaryReport"
        Me.Text = "Employee Timesheet Project Summary Report"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.pnlGridPanel.ResumeLayout(False)
        Me.pnlGridPanel.PerformLayout()
        CType(Me.dgTransactionHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents LblPeriod As System.Windows.Forms.Label
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents chkShowReportInHtml As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchProjectCode As eZee.Common.eZeeGradientButton
    Public WithEvents cboProjectCode As System.Windows.Forms.ComboBox
    Private WithEvents LblProjectCode As System.Windows.Forms.Label
    Private WithEvents LblReportType As System.Windows.Forms.Label
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents LblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents dgTransactionHead As System.Windows.Forms.DataGridView
    Friend WithEvents txtTransactionHeadFilter As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlGridPanel As System.Windows.Forms.Panel
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhTranheadcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTransactionHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranHeadID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAnalysisBy As System.Windows.Forms.LinkLabel
    Friend WithEvents chkShowEmpIdentifyNo As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowHolidayHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowLeaveTypeHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowExtraHrs As System.Windows.Forms.CheckBox
    Friend WithEvents chkIgnoreZeroLeaveTypehrs As System.Windows.Forms.CheckBox
End Class

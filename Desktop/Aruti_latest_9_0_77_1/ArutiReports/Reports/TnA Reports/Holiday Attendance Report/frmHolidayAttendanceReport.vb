'************************************************************************************************************************************
'Class Name : frmHolidayAttendanceReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmHolidayAttendanceReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmHolidayAttendanceReport"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private objHAttendance As clsHolidayAttendanceReport

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        _Show_AdvanceFilter = True
        objHAttendance = New clsHolidayAttendanceReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objHAttendance.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Try

            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objPolicy.getListForCombo("List", True)
            With cboPolicy
                .ValueMember = "policyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objShift = Nothing : objPolicy = Nothing : dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
            cboEmployee.SelectedValue = 0
            cboPolicy.SelectedValue = 0
            cboShift.SelectedValue = 0
            chkDayOff.Checked = False
            txtOT1HrsFrom.Decimal = 0
            txtOT1HrsTo.Decimal = 0
            txtOT2HrsFrom.Decimal = 0
            txtOT2HrsTo.Decimal = 0
            txtOT3HrsFrom.Decimal = 0
            txtOT3HrsTo.Decimal = 0
            txtOT4HrsFrom.Decimal = 0
            txtOT4HrsTo.Decimal = 0
            txtWHrsFrom.Decimal = 0
            txtWHrsTo.Decimal = 0
            mstrAdvanceFilter = ""

            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            'Pinkal (06-Aug-2019) -- End

            objHAttendance.setDefaultOrderBy(0)
            txtOrderBy.Text = objHAttendance.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objHAttendance.SetDefaultValue()

            objHAttendance._Date1 = dtpFromDate.Value.Date
            objHAttendance._Date2 = dtpToDate.Value.Date
            objHAttendance._IncludeDayOff = chkDayOff.Checked
            objHAttendance._WorkFromHrs = txtWHrsFrom.Decimal
            objHAttendance._WorkToHrs = txtWHrsTo.Decimal
            objHAttendance._OT1FromHrs = txtOT1HrsFrom.Decimal
            objHAttendance._OT1ToHrs = txtOT1HrsTo.Decimal
            objHAttendance._OT2FromHrs = txtOT2HrsFrom.Decimal
            objHAttendance._OT2ToHrs = txtOT2HrsTo.Decimal
            objHAttendance._OT3FromHrs = txtOT3HrsFrom.Decimal
            objHAttendance._OT3ToHrs = txtOT3HrsTo.Decimal
            objHAttendance._OT4FromHrs = txtOT4HrsFrom.Decimal
            objHAttendance._OT4ToHrs = txtOT4HrsTo.Decimal
            objHAttendance._PolicyId = cboPolicy.SelectedValue
            objHAttendance._PolicyName = cboPolicy.Text
            objHAttendance._ShiftId = cboShift.SelectedValue
            objHAttendance._ShiftName = cboShift.Text
            objHAttendance._EmployeeId = cboEmployee.SelectedValue
            objHAttendance._EmployeeName = cboEmployee.Text
            objHAttendance._ViewByIds = mstrStringIds
            objHAttendance._ViewIndex = mintViewIdx
            objHAttendance._ViewByName = mstrStringName
            objHAttendance._Analysis_Fields = mstrAnalysis_Fields
            objHAttendance._Analysis_Join = mstrAnalysis_Join
            objHAttendance._Analysis_OrderBy = mstrAnalysis_OrderBy
            objHAttendance._Report_GroupName = mstrReport_GroupName
            objHAttendance._AdvanceFilter = mstrAdvanceFilter

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            objHAttendance._IsPolicManagement = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (13-Dec-2018) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function


#End Region

#Region " Form's Events "

    Private Sub frmHolidayAttendanceReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmHolidayAttendanceReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objHAttendance._ReportName
            Me._Message = objHAttendance._ReportDesc
            Call FillCombo()
            Call ResetValue()
            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            cboPolicy.Enabled = ConfigParameter._Object._PolicyManagementTNA
            objbtnSearchPolicy.Enabled = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (13-Dec-2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmHolidayAttendanceReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objHAttendance.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, e.Type, Aruti.Data.enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objHAttendance.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, e.Type, Aruti.Data.enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmHolidayAttendanceReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmHolidayAttendanceReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmHolidayAttendanceReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsHolidayAttendanceReport.SetMessages()
            objfrm._Other_ModuleNames = "clsHolidayAttendanceReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmHolidayAttendanceReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnCommonSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchPolicy.Click, objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            Dim iCbo As New ComboBox
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToString.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    iCbo = cboEmployee
                Case objbtnSearchPolicy.Name.ToUpper
                    iCbo = cboPolicy
                Case objbtnSearchShift.Name.ToUpper
                    iCbo = cboShift
            End Select
            With frm
                .ValueMember = iCbo.ValueMember
                .DisplayMember = iCbo.DisplayMember
                .DataSource = iCbo.DataSource
                If iCbo.Name.ToUpper = cboEmployee.Name.ToUpper Then
                    .CodeMember = "employeecode"
                ElseIf iCbo.Name.ToUpper = cboPolicy.Name.ToUpper Then
                    .CodeMember = "policycode"
                End If
                If .DisplayDialog() = True Then
                    iCbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnCommonSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objHAttendance.setOrderBy(0)
            txtOrderBy.Text = objHAttendance.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "TextBox Event"

    Private Sub txtWHrsFrom_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWHrsFrom.Validated, txtWHrsTo.Validated, _
                                                                                                                                                                    txtOT1HrsFrom.Validated, txtOT1HrsTo.Validated, _
                                                                                                                                                                    txtOT2HrsFrom.Validated, txtOT2HrsTo.Validated, _
                                                                                                                                                                    txtOT3HrsFrom.Validated, txtOT3HrsTo.Validated, _
                                                                                                                                                                    txtOT4HrsFrom.Validated, txtOT4HrsTo.Validated
        Try

            Dim txt As TextBox

            If CType(sender, TextBox).Text.Trim = "" Then
                CType(sender, TextBox).Text = "0"
            End If

            txt = CType(sender, TextBox)

            If txt.Text = "" Then
                txt.Text = "0"
            ElseIf txt.Text.Trim.Contains(".") Then
                If txt.Text.Trim.Contains(".60") Then
calFromHr:          txt.Text = CDec(CDec(txt.Text.Trim) + 0.4).ToString("#0.00")
                ElseIf txt.Text <> "" And txt.Text <> "." Then
                    If txt.Text.Trim.IndexOf(".") = 0 And txt.Text.Trim.Contains(".60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf("."), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(".")) = "." Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(txt.Text.Substring(txt.Text.Trim.IndexOf("."), txt.Text.Trim.Length - txt.Text.Trim.IndexOf("."))) > 0.6 Then
                        GoTo calFromHr
                    Else
                        txt.Text = CDec(txt.Text).ToString("#0.00")
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtWHrsFrom_Validated", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (06-Aug-2019) -- Start
    'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    'Pinkal (06-Aug-2019) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.Name, Me.lblPolicy.Text)
			Me.chkDayOff.Text = Language._Object.getCaption(Me.chkDayOff.Name, Me.chkDayOff.Text)
			Me.lblOT4HrsTo.Text = Language._Object.getCaption(Me.lblOT4HrsTo.Name, Me.lblOT4HrsTo.Text)
			Me.lblOT4HrsFrom.Text = Language._Object.getCaption(Me.lblOT4HrsFrom.Name, Me.lblOT4HrsFrom.Text)
			Me.lblOT3HrsTo.Text = Language._Object.getCaption(Me.lblOT3HrsTo.Name, Me.lblOT3HrsTo.Text)
			Me.lblOT3HrsFrom.Text = Language._Object.getCaption(Me.lblOT3HrsFrom.Name, Me.lblOT3HrsFrom.Text)
			Me.lblOT2HrsTo.Text = Language._Object.getCaption(Me.lblOT2HrsTo.Name, Me.lblOT2HrsTo.Text)
			Me.lblOT2HrsFrom.Text = Language._Object.getCaption(Me.lblOT2HrsFrom.Name, Me.lblOT2HrsFrom.Text)
			Me.lblOT1HrsTo.Text = Language._Object.getCaption(Me.lblOT1HrsTo.Name, Me.lblOT1HrsTo.Text)
			Me.lblOT1HrsFrom.Text = Language._Object.getCaption(Me.lblOT1HrsFrom.Name, Me.lblOT1HrsFrom.Text)
			Me.lblToHrs.Text = Language._Object.getCaption(Me.lblToHrs.Name, Me.lblToHrs.Text)
			Me.lblWorkeHrs.Text = Language._Object.getCaption(Me.lblWorkeHrs.Name, Me.lblWorkeHrs.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

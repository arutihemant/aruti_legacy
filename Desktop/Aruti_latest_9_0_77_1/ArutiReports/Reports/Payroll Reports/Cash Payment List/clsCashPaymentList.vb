'************************************************************************************************************************************
'Class Name : clsBankPaymentList.vb
'Purpose    :
'Date       :11/04/2011
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Anjan
''' </summary>
Public Class clsCashPaymentList
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsCashPaymentList"
    Private mstrReportId As String = enArutiReport.CashPaymentList
    Dim objDataOperation As clsDataOperation

#Region "Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnCashPaymentReport()

    End Sub

#End Region


    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
#Region " Private Variables "

    Private mstrEmpCode As String = String.Empty
    Private mstrEmpName As String = String.Empty
    Private mintEmployeeunkid As Integer = -1
    Private mintReportId As Integer = 0
    Private mintNetPayAdvance As Integer = 0
    Private mdblAmount As Decimal = 0
    Private mdblAmountTo As Decimal = 0
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrNetPayAdvance As String = String.Empty
    Private mblnShowSign As Boolean = False
    Private mblnIncludeInactiveEmp As Boolean = True

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (26 Nov 2011) -- Start
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (26 Nov 2011) -- End

    'Sohail (16 Dec 2011) -- Start
    Private mintCurrencyId As Integer = 0
    Private mstrCurrencyName As String = ""
    Private mblnIsMultipleCurrency As Boolean = False
    'Sohail (16 Dec 2011) -- End

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintReportModeId As Integer = 0
    Private mstrReportModeName As String = ""
    'Sohail (02 Jul 2012) -- End


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrCurrencySign As String = ""
    'Pinkal (14-Dec-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (24-May-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Sohail (29 Jan 2016) -- Start
    'Enhancement - 3 Logo options on Cash Payment Report for KBC.
    Private mblnIsLogo As Boolean = True
    Private mblnIsCompanyInfo As Boolean = False
    Private mblnIsLogoCompanyInfo As Boolean = False
    Private mintPayslipTemplate As Integer = 1
    'Sohail (29 Jan 2016) -- End

#End Region
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 



#Region " Properties "
    Public WriteOnly Property _EmpCode() As String
        Set(ByVal value As String)
            mstrEmpCode = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _Amount() As Double
        Set(ByVal value As Double)
            mdblAmount = value
        End Set
    End Property
    Public WriteOnly Property _AmountTo() As Double
        Set(ByVal value As Double)
            mdblAmountTo = value
        End Set
    End Property
    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _NetPayAdvanceId() As Integer
        Set(ByVal value As Integer)
            mintNetPayAdvance = value
        End Set
    End Property

    Public WriteOnly Property _NetPayAdvance() As String
        Set(ByVal value As String)
            mstrNetPayAdvance = value
        End Set
    End Property
    Public WriteOnly Property _Period() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _IsShowSign() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSign = value
        End Set
    End Property


    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (26 Nov 2011) -- Start
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (26 Nov 2011) -- End

    'Sohail (16 Dec 2011) -- Start
    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyName() As String
        Set(ByVal value As String)
            mstrCurrencyName = value
        End Set
    End Property
    'Sohail (16 Dec 2011) -- End

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End

    'Sohail (02 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ReportModeId() As Integer
        Set(ByVal value As Integer)
            mintReportModeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportModeName() As String
        Set(ByVal value As String)
            mstrReportModeName = value
        End Set
    End Property
    'Sohail (02 Jul 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Sohail (29 Jan 2016) -- Start
    'Enhancement - 3 Logo options on Cash Payment Report for KBC.
    Public WriteOnly Property _IsOnlyLogo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogo = value
        End Set
    End Property
    Public WriteOnly Property _IsOnlyCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCompanyInfo = value
        End Set
    End Property
    Public WriteOnly Property _IsLogoCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogoCompanyInfo = value
        End Set
    End Property

    Public WriteOnly Property _PayslipTemplate() As Integer
        Set(ByVal value As Integer)
            mintPayslipTemplate = value
        End Set
    End Property
    'Sohail (29 Jan 2016) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()

        Try
            mstrEmpCode = ""
            mstrEmpName = ""
            mintEmployeeunkid = -1
            mdblAmount = 0
            mdblAmountTo = 0
            mintPeriodId = 0
            mintNetPayAdvance = 0
            mstrPeriodName = ""
            mblnShowSign = False
            mblnIncludeInactiveEmp = True

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (26 Nov 2011) -- Start
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (26 Nov 2011) -- End

            'Sohail (16 Dec 2011) -- Start
            mintCurrencyId = 0
            mstrCurrencyName = ""
            'Sohail (16 Dec 2011) -- End

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            mintReportModeId = 0
            mstrReportModeName = ""
            'Sohail (02 Jul 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim mintReferenceType As Integer = 0

        Try

            Select Case mintNetPayAdvance
                Case 0
                    mintReferenceType = 3
                Case 1
                    mintReferenceType = 2
            End Select

            objDataOperation.AddParameter("@RefId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceType)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Type : ") & mstrNetPayAdvance & " "

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mintReportModeId = 0 Then 'DRAFT
                objDataOperation.AddParameter("@isauthorized", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportModeId)
                Me._FilterQuery &= " AND ISNULL(prpayment_tran.isauthorized, 0 ) = @isauthorized "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Report Mode : ") & mstrReportModeName & " "
            ElseIf mintReportModeId = 1 Then 'AUTHORIZED
                objDataOperation.AddParameter("@isauthorized", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportModeId)
                Me._FilterQuery &= " AND ISNULL(prpayment_tran.isauthorized, 0 ) = @isauthorized "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Report Mode : ") & mstrReportModeName & " "
            End If
            'Sohail (02 Jul 2012) -- End

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Period : ") & mstrPeriodName & " "

            If mstrEmpCode.Trim <> "" Then
                objDataOperation.AddParameter("@EmpCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmpCode & "%")
                Me._FilterQuery &= " AND hremployee_master.employeecode LIKE @EmpCode "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee Code : ") & mstrEmpCode & " "
            End If

            If mintEmployeeunkid > 0 Then
                objDataOperation.AddParameter("@Employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @Employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Employee Name : ") & mstrEmpName & " "
            End If

            If mdblAmount > 0 And mdblAmountTo > 0 Then
                objDataOperation.AddParameter("@Amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmount)
                objDataOperation.AddParameter("@AmountTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmountTo)
                'Sohail (16 Dec 2011) -- Start
                'Me._FilterQuery &= " AND ISNULL(prpayment_tran.amount,0) BETWEEN  @Amount AND @AmountTo "
                Me._FilterQuery &= " AND ISNULL(prpayment_tran.expaidamt,0) BETWEEN  @Amount AND @AmountTo "
                'Sohail (16 Dec 2011) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Amount From : ") & mdblAmount & Language.getMessage(mstrModuleName, 19, " To ") & mdblAmountTo & " "
            End If

            'Sohail (16 Dec 2011) -- Start
            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@Currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'Me._FilterQuery &= " AND paidcurrencyid = @Currencyunkid "
                Me._FilterQuery &= " AND prpayment_tran.countryunkid = @Currencyunkid "
                'Sohail (03 Sep 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Paid Currency : ") & mstrCurrencyName & " "
            End If
            'Sohail (16 Dec 2011) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Order By :") & " " & Me.OrderByDisplay
                'Sohail (26 Nov 2011) -- Start
                'Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
                'Sohail (26 Nov 2011) -- End
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (20 Apr 2012) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try

    End Sub
#End Region

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (14-Dec-2012) -- Start
        '    'Enhancement : TRA Changes

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    'Pinkal (14-Dec-2012) -- End


        '    'Pinkal (24-May-2013) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_CashPaymentReport()
        '    Rpt = objRpt

        '    ' objRpt = Generate_CashPaymentReport()

        '    'Pinkal (24-May-2013) -- End


        '    If Not IsNothing(objRpt) Then


        '        'Pinkal (14-Dec-2012) -- Start
        '        'Enhancement : TRA Changes

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
        '        Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

        '            If mintViewIndex > 0 Then
        '                Dim strGrpCols As String() = {"column4"}
        '                strarrGroupColumns = strGrpCols
        '                intCurrencyColumn -= 1
        '            End If

        '            If mblnShowSign = True Then
        '                intCurrencyColumn -= 1
        '            End If

        '            objDic.Add(intCurrencyColumn, mstrCurrencySign)

        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next


        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, Not mblnIsMultipleCurrency, strarrGroupColumns, "", mstrReportModeName, "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

        '        'Pinkal (14-Dec-2012) -- End



        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit
        Dim objCompany As New clsCompany_Master 'Sohail (29 Jan 2016)

        Try


            menExportAction = ExportAction
            mdtTableExcel = Nothing

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            objConfig._Companyunkid = xCompanyUnkid
            objCompany._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid
            'Sohail (29 Jan 2016) -- End

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            'objRpt = Generate_CashPaymentReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, mblnIncludeInactiveEmp, True, objConfig._CurrencyFormat, objUser._Username)
            objRpt = Generate_CashPaymentReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, mblnIncludeInactiveEmp, True, objConfig._CurrencyFormat, objUser._Username, objCompany._Name)
            'Sohail (29 Jan 2016) -- End
            Rpt = objRpt


            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
                Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then

                    Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column4"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    End If

                    If mblnShowSign = True Then
                        intCurrencyColumn -= 1
                    End If

                    objDic.Add(intCurrencyColumn, mstrCurrencySign)

                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next


                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------


                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 5, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, Not mblnIsMultipleCurrency, strarrGroupColumns, "", mstrReportModeName, "", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, objDic)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_CashPaymentReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_CashPaymentReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_CashPaymentReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#Region " Report Generation "
    Dim iColumn_CashPaymentReport As New IColumnCollection

    Public Property Field_OnCashPaymentReport() As IColumnCollection
        Get
            Return iColumn_CashPaymentReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_CashPaymentReport = value
        End Set
    End Property

    Private Sub Create_OnCashPaymentReport()

        Try
            iColumn_CashPaymentReport.Clear()
            iColumn_CashPaymentReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 1, "Emp. Code")))

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_CashPaymentReport.Add(New IColumn("hremployee_master.surname+' ' + hremployee_master.firstname", Language.getMessage(mstrModuleName, 2, "Emp. Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_CashPaymentReport.Add(New IColumn("hremployee_master.surname+' ' + hremployee_master.firstname", Language.getMessage(mstrModuleName, 2, "Emp. Name")))
            Else
                iColumn_CashPaymentReport.Add(New IColumn("hremployee_master.firstname+' ' + hremployee_master.surname", Language.getMessage(mstrModuleName, 2, "Emp. Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_CashPaymentReport.Add(New IColumn("hremployee_master.surname+' ' + hremployee_master.firstname", Language.getMessage(mstrModuleName, 2, "Emp. Name")))
            iColumn_CashPaymentReport.Add(New IColumn("ISNULL(prpayment_tran.amount,0)", Language.getMessage(mstrModuleName, 3, "Total")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnCashPaymentReport; Module Name: " & mstrModuleName)
        End Try


    End Sub

    Private Function Generate_CashPaymentReport(ByVal xDatabaseName As String _
                                                , ByVal xUserUnkid As Integer _
                                                , ByVal xYearUnkid As Integer _
                                                , ByVal xCompanyUnkid As Integer _
                                                , ByVal xPeriodStart As Date _
                                                , ByVal xPeriodEnd As Date _
                                                , ByVal xUserModeSetting As String _
                                                , ByVal xOnlyApproved As Boolean _
                                                , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                , ByVal blnApplyUserAccessFilter As Boolean _
                                                , ByVal strFmtCurrency As String _
                                                , ByVal strUsername As String _
                                                , ByVal strCompanyName As String _
                                                ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (29 Jan 2016) - [strCompanyName]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, strUsername]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport

        'Sohail (26 Nov 2011) -- Start
        Dim intPrevId As Integer = 0
        Dim decSubTot As Decimal = 0
        'Sohail (26 Nov 2011) -- End

        'Anjan (05 Jul 2011)-Start
        'Issue : Removing formula and setting total manually.
        Dim mdecTotal As Decimal = 0
        'Anjan (05 Jul 2011)-End 


        'Pinkal (14-Dec-2012) -- Start
        'Enhancement : TRA Changes
        mstrCurrencySign = ""
        'Pinkal (14-Dec-2012) -- End


        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation
        Try


            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'strQ = "SELECT  hremployee_master.employeecode AS code " & _
            '        ",hremployee_master.surname+' ' + hremployee_master.firstname AS empname " & _
            '        ",ISNULL(prpayment_tran.amount,0) AS total " & _
            '        ",referenceid " & _
            '        ",paymentmode " & _
            '        ",referencetranunkid " & _
            '    "FROM prpayment_tran " & _
            '        "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
            '                                  " AND hremployee_master.isactive = 1 " & _
            '    "WHERE paymentmode = 1 " & _
            '        " AND referenceid = @RefId  " & _
            '        " AND ISNULL(prpayment_tran.isvoid ,0) = 0 " & _
            '        " AND ISNULL (prpayment_tran.isreceipt ,0) = 0 " & _
            '        " AND paytypeid=1 " & _
            '        " AND prpayment_tran.periodunkid = @PeriodId  "

            'Sohail (26 Nov 2011) -- Start
            'strQ = "SELECT " & _
            '        "	 hremployee_master.employeecode AS code " & _
            '        ",hremployee_master.surname+' ' + hremployee_master.firstname AS empname " & _
            '        ",ISNULL(prpayment_tran.amount,0) AS total " & _
            '        ",referenceid " & _
            '        ",paymentmode " & _
            '        ",referencetranunkid " & _
            '    "FROM prpayment_tran " & _
            '        "	JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid "
            'If mblnIncludeInactiveEmp = False Then
            '    strQ &= "	AND hremployee_master.isactive = 1 "
            'End If
            'strQ &= "WHERE paymentmode = 1 " & _
            '        " AND referenceid = @RefId  " & _
            '        " AND ISNULL(prpayment_tran.isvoid ,0) = 0 " & _
            '        " AND ISNULL (prpayment_tran.isreceipt ,0) = 0 " & _
            '        " AND paytypeid=1 " & _
            '        " AND prpayment_tran.periodunkid = @PeriodId  "
            strQ = "SELECT  hremployee_master.employeecode AS code "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'hremployee_master.surname+' ' + hremployee_master.firstname AS empname
            If mblnFirstNamethenSurname = False Then
                'Sohail (01 Feb 2016) -- Start
                'Enhancement - Show employee middle name as well for Cash Payment Report for KBC.
                'strQ &= ",hremployee_master.surname+' ' + hremployee_master.firstname AS empname "
                strQ &= ", REPLACE(hremployee_master.surname+ ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.firstname, '  ',' ') AS empname "
                'Sohail (01 Feb 2016) -- End
            Else
                'Sohail (01 Feb 2016) -- Start
                'Enhancement - Show employee middle name as well for Cash Payment Report for KBC.
                strQ &= ", REPLACE(hremployee_master.firstname+' ' + ISNULL(hremployee_master.othername, '') + ' '  + hremployee_master.surname, '  ',' ') AS empname "
                'Sohail (01 Feb 2016) -- End
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            strQ &= ",ISNULL(prpayment_tran.amount,0) AS total " & _
                    ",referenceid " & _
                    ",paymentmode " & _
                    ", referencetranunkid " & _
                    ", paidcurrencyid " & _
                    ", cfexchange_rate.currency_name " & _
                    ", cfexchange_rate.currency_sign " & _
                    ", expaidamt " 'Sohail (16 Dec 2011) - TRA - Mult icurrency Payment List, for Bank Payment and Cash Payment List.

            strQ &= ", ISNULL(prpayment_tran.isauthorized, 0) AS isauthorized " 'Sohail (02 Jul 2012) - [isauthorized]

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Fields
            Else
                strQ &= ", '' AS GName "
            End If

            strQ &= "FROM    prpayment_tran " & _
                    "	JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                    "   LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = paidcurrencyid " 'Sohail (16 Dec 2011) - TRA - Mult icurrency Payment List, for Bank Payment and Cash Payment List.

            If mintViewIndex > 0 Then
                strQ &= mstrAnalysis_Join
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            strQ &= "           LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         stationunkid " & _
                               "        ,deptgroupunkid " & _
                               "        ,departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,sectionunkid " & _
                               "        ,unitgroupunkid " & _
                               "        ,unitunkid " & _
                               "        ,teamunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "WHERE   paymentmode = " & enPaymentMode.CASH & " " & _
                    " AND referenceid = @RefId  " & _
                    " AND ISNULL(prpayment_tran.isvoid ,0) = 0 " & _
                    " AND ISNULL (prpayment_tran.isreceipt ,0) = 0 " & _
                    " AND paytypeid=1 " & _
                    " AND prpayment_tran.periodunkid = @PeriodId  "



            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= "	AND hremployee_master.isactive = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            '    'Sohail (20 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (26 Nov 2011) -- End



            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (20 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (20 Apr 2012) -- End
            'End If
            'Sohail (28 Jun 2011) -- End
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'strQ &= " AND hremployee_master.stationunkid = " & mintBranchId
                strQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            Call FilterTitleAndFilterQuery()

            strQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (16 Dec 2011) -- Start
            Dim intPrevCurrId As Integer = 0
            mblnIsMultipleCurrency = False
            'Sohail (16 Dec 2011) -- End

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("code")
                rpt_Row.Item("Column2") = dtRow.Item("empname")
                'Sohail (16 Dec 2011) -- Start
                'TRA - Mult icurrency Payment List, for Bank Payment and Cash Payment List
                'rpt_Row.Item("Column3") = Format(dtRow.Item("total"), GUI.fmtCurrency)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Column3") = Format(dtRow.Item("expaidamt"), GUI.fmtCurrency)
                rpt_Row.Item("Column3") = Format(dtRow.Item("expaidamt"), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Sohail (16 Dec 2011) -- End

                'Sohail (26 Nov 2011) -- Start
                rpt_Row.Item("Column4") = dtRow.Item("GName")
                If mintViewIndex > 0 Then
                    If intPrevId <> CInt(dtRow.Item("Id")) Then
                        'Sohail (16 Dec 2011) -- Start
                        'decSubTot = dtRow.Item("total")
                        decSubTot = dtRow.Item("expaidamt")
                        'Sohail (16 Dec 2011) -- End
                    Else
                        'Sohail (16 Dec 2011) -- Start
                        'decSubTot += dtRow.Item("total")
                        decSubTot += dtRow.Item("expaidamt")
                        'Sohail (16 Dec 2011) -- End
                    End If
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Column5") = Format(decSubTot, GUI.fmtCurrency)
                    rpt_Row.Item("Column5") = Format(decSubTot, strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    intPrevId = CInt(dtRow.Item("Id"))
                Else
                    rpt_Row.Item("Column5") = ""
                End If
                'Sohail (26 Nov 2011) -- End

                'Sohail (16 Dec 2011) -- Start
                rpt_Row.Item("Column6") = dtRow.Item("paidcurrencyid")
                rpt_Row.Item("Column7") = dtRow.Item("currency_name")
                rpt_Row.Item("Column8") = dtRow.Item("currency_sign")


                'Pinkal (14-Dec-2012) -- Start
                'Enhancement : TRA Changes
                mstrCurrencySign = dtRow.Item("currency_sign")
                'Pinkal (14-Dec-2012) -- End


                If mblnIsMultipleCurrency = False AndAlso intPrevCurrId <= 0 Then
                    intPrevCurrId = CInt(dtRow.Item("paidcurrencyid"))
                End If
                If mblnIsMultipleCurrency = False AndAlso intPrevCurrId > 0 AndAlso intPrevCurrId <> CInt(dtRow.Item("paidcurrencyid")) Then
                    mblnIsMultipleCurrency = True
                End If
                'Sohail (16 Dec 2011) -- End

                'Sohail (16 Dec 2011) -- Start
                'mdecTotal += dtRow.Item("total")
                'Sohail (17 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'mdecTotal += dtRow.Item("expaidamt") 'Paid Currency Amount (Not Base currency amount)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'mdecTotal += CDec(Format(dtRow.Item("expaidamt"), GUI.fmtCurrency)) 'Paid Currency Amount (Not Base currency amount)
                mdecTotal += CDec(Format(dtRow.Item("expaidamt"), strFmtCurrency)) 'Paid Currency Amount (Not Base currency amount)
                'Sohail (21 Aug 2015) -- End
                'Sohail (17 Aug 2012) -- End
                'Sohail (16 Dec 2011) -- End


                'Pinkal (14-Dec-2012) -- Start
                'Enhancement : TRA Changes
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("expaidamt")), GUI.fmtCurrency)
                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("expaidamt")), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Pinkal (14-Dec-2012) -- End


                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next
            objRpt = New ArutiReport.Designer.rptCashPaymentList

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            'ReportFunction.Logo_Display(objRpt, _
            '                            ConfigParameter._Object._IsDisplayLogo, _
            '                            ConfigParameter._Object._ShowLogoRightSide, _
            '                            "arutiLogo1", _
            '                            "arutiLogo2", _
            '                            arrImageRow, _
            '                            "txtCompanyName", _
            '                            "txtReportName", _
            '                            "txtFilterDescription", _
            '                            ConfigParameter._Object._GetLeftMargin, _
            '                            ConfigParameter._Object._GetRightMargin)
            If mblnIsLogoCompanyInfo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                            ConfigParameter._Object._IsDisplayLogo, _
                                            ConfigParameter._Object._ShowLogoRightSide, _
                                            "arutiLogo1", _
                                            "arutiLogo2", _
                                            arrImageRow, _
                                            "txtCompanyName", _
                                            "txtReportName", _
                                            "txtFilterDescription", _
                                            ConfigParameter._Object._GetLeftMargin, _
                                            ConfigParameter._Object._GetRightMargin)

            ElseIf mblnIsLogo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                           ConfigParameter._Object._IsDisplayLogo, _
                                           False, _
                                           "arutiLogo1", _
                                           "arutiLogo2", _
                                           arrImageRow, _
                                           "txtCompanyName", _
                                           "txtReportName", _
                                           "txtFilterDescription", _
                                           ConfigParameter._Object._GetLeftMargin, _
                                           ConfigParameter._Object._GetRightMargin)

            End If
            'Sohail (29 Jan 2016) -- End

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            If mblnIsCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)
            ElseIf mblnIsLogo = True Then
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = False 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName2", Me._ReportName) 'Sohail (07 Mar 2016)

                If mintPayslipTemplate = enPayslipTemplate.ONE_SIDED_KBC_13 Then
                    objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = False
                    Call ReportFunction.TextChange(objRpt, "txtCompanyName2", strCompanyName)
                End If

            ElseIf mblnIsLogoCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = True
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtCompanyName", strCompanyName) 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName) 'Sohail (07 Mar 2016)
            End If
            'Sohail (29 Jan 2016) -- End

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 4, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUsername)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mblnShowSign = False Then
                Call ReportFunction.EnableSuppress(objRpt, "Line3", True)
                'S.SANDEEP [ 17 AUG 2011 ] -- START
                'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                Call ReportFunction.EnableSuppress(objRpt, "txtSign", True)
                'S.SANDEEP [ 17 AUG 2011 ] -- END 
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 1, "Emp. Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Emp. Name"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 8, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtSign", Language.getMessage(mstrModuleName, 9, "Signature"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 10, "Grand Total :"))


            'Anjan (05 Jul 2011)-Start
            'Issue : Removing formula and setting total manually.
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlTotCol31")
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportFunction.TextChange(objRpt, "txtCol3Tot", Format(mdecTotal, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtCol3Tot", Format(mdecTotal, strFmtCurrency))
            'Sohail (21 Aug 2015) -- End
            'Anjan (05 Jul 2011)-End 

            'Sohail (26 Nov 2011) -- Start
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            If mintViewIndex > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 11, "Sub Total"))
                'Sohail (16 Dec 2011) -- Start
                If mblnIsMultipleCurrency = True Then
                    Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
                    Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
                End If
                'Sohail (16 Dec 2011) -- End
            Else
                Call ReportFunction.TextChange(objRpt, "txtSubTotal", "")
                'Sohail (16 Dec 2011) -- Start
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
                If mblnIsMultipleCurrency = True Then
                    Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
                End If
                'Sohail (16 Dec 2011) -- End
            End If
            'Sohail (26 Nov 2011) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription1", Me._FilterTitle) 'Sohail (29 Jan 2016)

            'Sohail (02 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            If mintReportModeId = 0 Then
                Call ReportFunction.TextChange(objRpt, "txtReportMode", "DRAFT") 'mstrReportModeName
            ElseIf mintReportModeId = 1 Then
                Call ReportFunction.TextChange(objRpt, "txtReportMode", "AUTHORIZED") 'mstrReportModeName
            End If
            objRpt.ReportDefinition.ReportObjects("txtReportMode").Left = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Left
            objRpt.ReportDefinition.ReportObjects("txtReportMode").Width = objRpt.ReportDefinition.ReportObjects("txtCompanyName").Width
            'Sohail (02 Jul 2012) -- End


            'Pinkal (14-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)

                Dim mintColumn As Integer = 0

                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 1, "Emp. Code")
                mdtTableExcel.Columns("Column1").SetOrdinal(0)
                mintColumn += 1

                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 2, "Emp. Name")
                mdtTableExcel.Columns("Column2").SetOrdinal(1)
                mintColumn += 1

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 41, "Total")
                mdtTableExcel.Columns("Column81").SetOrdinal(2)
                mintColumn += 1

                mdtTableExcel.Columns("Column8").Caption = ""
                mdtTableExcel.Columns("Column8").SetOrdinal(3)
                mintColumn += 1

                If mblnShowSign Then
                    mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 9, "Signature")
                    mdtTableExcel.Columns("Column9").SetOrdinal(4)
                    mintColumn += 1
                End If

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column4").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Substring(0, mstrReport_GroupName.Length - 1).Trim, mstrReport_GroupName.Trim)
                    mdtTableExcel.Columns("Column4").SetOrdinal(5)
                    mintColumn += 1
                End If


                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If

            'Pinkal (14-Dec-2012) -- End

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CashPaymentReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose()
            dsList = Nothing

        End Try

    End Function
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Emp. Code")
            Language.setMessage(mstrModuleName, 2, "Emp. Name")
            Language.setMessage(mstrModuleName, 3, "Total")
            Language.setMessage(mstrModuleName, 4, "Prepared By :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Total")
            Language.setMessage(mstrModuleName, 9, "Signature")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")
            Language.setMessage(mstrModuleName, 11, "Sub Total")
            Language.setMessage(mstrModuleName, 12, "Type :")
            Language.setMessage(mstrModuleName, 13, "Report Mode :")
            Language.setMessage(mstrModuleName, 14, "Report Mode :")
            Language.setMessage(mstrModuleName, 15, " Period :")
            Language.setMessage(mstrModuleName, 16, "Employee Code :")
            Language.setMessage(mstrModuleName, 17, "Employee Name :")
            Language.setMessage(mstrModuleName, 18, "Amount From :")
            Language.setMessage(mstrModuleName, 19, " To")
            Language.setMessage(mstrModuleName, 20, "Paid Currency :")
            Language.setMessage(mstrModuleName, 21, "Order By :")
            Language.setMessage(mstrModuleName, 22, "Branch :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

'************************************************************************************************************************************
'Class Name : frmGratuityReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGratuityReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGratuityReport"
    Private objGratuity As clsGratuityReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""



    Private dsPeriod As DataSet = Nothing

    Private mstrAdvanceFilter As String = String.Empty

    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        objGratuity = New clsGratuityReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objGratuity.SetDefaultValue()
        InitializeComponent()


        '_Show_ExcelExtra_Menu = True
        _Show_AdvanceFilter = True

    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet
            Dim objTranHead As New clsTransactionHead
            Dim objMaster As New clsMasterData

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End



            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("List", True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboGratuityHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("List", True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboCurrMonthGratuityHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboGratuityHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Gratuity Head."), enMsgBoxStyle.Information)
                cboGratuityHead.Select()
                Exit Function
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function
            ElseIf CInt(cboCurrMonthGratuityHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Current Month Gratuity Head."), enMsgBoxStyle.Information)
                cboCurrMonthGratuityHead.Select()
                Exit Function
            End If


            objGratuity.SetDefaultValue()

            objGratuity._EmployeeId = CInt(cboEmployee.SelectedValue)
            objGratuity._EmployeeName = cboEmployee.Text

            objGratuity._HeadId = CInt(cboGratuityHead.SelectedValue)
            objGratuity._HeadName = cboGratuityHead.Text

            objGratuity._Current_HeadId = CInt(cboCurrMonthGratuityHead.SelectedValue)
            objGratuity._Current_HeadName = cboCurrMonthGratuityHead.Text

            objGratuity._PeriodId = CInt(cboPeriod.SelectedValue)
            objGratuity._PeriodName = cboPeriod.Text

            objGratuity._ViewByIds = mstrStringIds
            objGratuity._ViewIndex = mintViewIdx
            objGratuity._ViewByName = mstrStringName
            objGratuity._Analysis_Fields = mstrAnalysis_Fields
            objGratuity._Analysis_Join = mstrAnalysis_Join
            objGratuity._Analysis_OrderBy = mstrAnalysis_OrderBy
            objGratuity._Report_GroupName = mstrReport_GroupName

            objGratuity._IsActive = chkInactiveemp.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objGratuity._PeriodStartDate = objPeriod._Start_Date
            objGratuity._PeriodEndDate = objPeriod._End_Date

            objPeriod = Nothing
            objGratuity._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objGratuity.setDefaultOrderBy(0)
            txtOrderBy.Text = objGratuity.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboGratuityHead.SelectedValue = 0
            cboCurrMonthGratuityHead.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmGratuityReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objGratuity = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGratuityReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objGratuity._ReportName
            Me._Message = objGratuity._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmGratuityReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboGratuityHead
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCurrMonthTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCurrMonthTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboCurrMonthGratuityHead
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCurrMonthTranHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objGratuity.generateReport(0, e.Type, enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objGratuity.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGratuityReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objGratuity.generateReportNew(0, enPrintAction.None, e.Type)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objGratuity.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGratuityReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGratuityReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGratuityReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGratuityReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub



    Private Sub frmGratuityReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDDetailReport.SetMessages()
            objfrm._Other_ModuleNames = "clsGratuityReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmGratuityReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " LinkLabel Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region " ComboBox Event "

#End Region

#Region " Control "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objGratuity.setOrderBy(0)
            txtOrderBy.Text = objGratuity.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblGratuityHead.Text = Language._Object.getCaption(Me.lblGratuityHead.Name, Me.lblGratuityHead.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblCurrMonthGratuityHead.Text = Language._Object.getCaption(Me.lblCurrMonthGratuityHead.Name, Me.lblCurrMonthGratuityHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Gratuity Head.")
			Language.setMessage(mstrModuleName, 2, "Please Select Period.")
			Language.setMessage(mstrModuleName, 3, "Please Select Current Month Gratuity Head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

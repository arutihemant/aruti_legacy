Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoanAdvanceListReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceList"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private objLAReport As clsLoanAdvanceList_Report

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objLAReport = New clsLoanAdvanceList_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLAReport.SetDefaultValue()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objMData As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Try

            dsList = objMData.getYearList("FromYear")
            With cboFYear
                .DisplayMember = dsList.Tables("FromYear").Columns(0).ColumnName
                .DataSource = dsList.Tables("FromYear")
            End With
            cboFYear.SelectedIndex = cboFYear.FindStringExact(CStr(Now.Date.Year))

            dsList = objMData.getYearList("ToYear")
            With cboTYear
                .DisplayMember = dsList.Tables("ToYear").Columns(0).ColumnName
                .DataSource = dsList.Tables("ToYear")
            End With
            cboTYear.SelectedIndex = cboTYear.FindStringExact(CStr(Now.Date.Year))

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = 0
            End With

            dsList = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            dsList = objMData.GetLoan_Saving_Status("LoanStatus")
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables("LoanStatus")
                .SelectedValue = 0
            End With

            With cboLoanAdvance
                .Items.Clear()
                .Items.Add(Language.getMessage("frmLoanAdvanceList", 4, "Select"))
                .Items.Add(Language.getMessage("frmLoanAdvanceList", 5, "Loan"))
                .Items.Add(Language.getMessage("frmLoanAdvanceList", 6, "Advance"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objMData = Nothing : objLoanScheme = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboFYear.SelectedIndex = cboFYear.FindStringExact(CStr(Now.Date.Year))
            cboTYear.SelectedIndex = cboTYear.FindStringExact(CStr(Now.Date.Year))
            cboEmployee.SelectedValue = 0
            cboLoanScheme.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboLoanAdvance.SelectedIndex = 0
            objLAReport.setDefaultOrderBy(0)
            txtOrderBy.Text = objLAReport.OrderByDisplay

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Filter() As Boolean
        Try
            objLAReport.SetDefaultValue()

            objLAReport._CompanyId = Company._Object._Companyunkid
            objLAReport._Employee_Id = CInt(cboEmployee.SelectedValue)
            objLAReport._Employee_Name = cboEmployee.Text
            objLAReport._From_Year = CInt(cboFYear.Text)
            objLAReport._LATypeId = cboLoanAdvance.SelectedIndex
            objLAReport._LATypeName = cboLoanAdvance.Text
            objLAReport._LoanSchemeId = CInt(cboLoanScheme.SelectedValue)
            objLAReport._LoanSchemeName = cboLoanScheme.Text
            objLAReport._StatusId = CInt(cboStatus.SelectedValue)
            objLAReport._StatusName = cboStatus.Text
            objLAReport._To_Year = CInt(cboTYear.Text)


            objLAReport._ViewIndex = mintViewIdx
            objLAReport._Analysis_Fields = mstrAnalysis_Fields
            objLAReport._Analysis_Join = mstrAnalysis_Join
            objLAReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLAReport._Report_GroupName = mstrReport_GroupName
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objLAReport._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Filter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmLoanAdvanceListReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLAReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceListReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Me._Title = objLAReport._ReportName
            Me._Message = objLAReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmLoanAdvanceListReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Set_Filter() = False Then Exit Sub
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objLAReport.generateReport(0, CType(e.Type, enPrintAction), enExportAction.None)
            objLAReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, CType(e.Type, enPrintAction), enExportAction.None)
            'Nilay (10-Oct-2015) -- End

            If objLAReport._IsDataPresent = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data present for the selected years"), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceListReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Set_Filter() = False Then Exit Sub
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objLAReport.generateReport(0, enPrintAction.None, CType(e.Type, enExportAction))
            objLAReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, CType(e.Type, enExportAction))
            'Nilay (10-Oct-2015) -- End

            If objLAReport._IsDataPresent = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data present for the selected years"), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceListReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceListReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLoanScheme.DataSource, DataTable)
            frm.DisplayMember = cboLoanScheme.DisplayMember
            frm.ValueMember = cboLoanScheme.ValueMember
            'Nilay (09-Dec-2015) -- Start
            'frm.CodeMember = "employeecode"
            frm.CodeMember = "code"
            'Nilay (09-Dec-2015) -- End
            If frm.DisplayDialog Then
                cboLoanScheme.SelectedValue = frm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLAReport.setOrderBy(0)
            txtOrderBy.Text = objLAReport.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub frmLoanAdvanceListReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoanAdvanceSaving.SetMessages()
            objfrm._Other_ModuleNames = "clsLoanAdvanceList_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmLoanAdvanceListReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


#End Region

#Region " Control's Events "

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblToYear.Text = Language._Object.getCaption(Me.lblToYear.Name, Me.lblToYear.Text)
			Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, No data present for the selected years")
			Language.setMessage("frmLoanAdvanceList", 4, "Select")
			Language.setMessage("frmLoanAdvanceList", 5, "Loan")
			Language.setMessage("frmLoanAdvanceList", 6, "Advance")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

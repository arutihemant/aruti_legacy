#Region " Imports "
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region

Public Class frmPaymentJVReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPaymentJVReport"
    Private objPaymentJV As clsPaymentJVReport
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
#End Region

#Region " Constructor "
    Public Sub New()
        objPaymentJV = New clsPaymentJVReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPaymentJV.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Public Functions "
    Public Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objExRate As New clsExchangeRate
        Try


            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Main Payment Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "General Staff Payment Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Cost Center Payment Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Combined Payment JV"))
                .SelectedIndex = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, 0).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0).Tables(0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "Name"
            cboPeriod.SelectedValue = mintFirstOpenPeriod

            Dim objBranch As New clsStation
            Dim dsList As New DataSet
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objBranch = Nothing : dsList.Dispose()

            Dim objCCGroup As New clspayrollgroup_master
            dsList = objCCGroup.getListForCombo(enPayrollGroupType.CostCenter, "CCGroup", True)
            With cboCCGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("CCGroup")
                .SelectedValue = 0
            End With
            objCCGroup = Nothing

            dsList = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = 0
            End With

            dsList = objMaster.GetComboListForCustomCCenter("CCenter")
            With cboCustomCCenter
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("CCenter")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objExRate = Nothing
        End Try
    End Sub

    Public Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            lvTranHead.Items.Clear()
            objchkSelectAll.Checked = False
            Dim lvItem As ListViewItem

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objTranHead.getComboList("TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            'Sohail (21 Aug 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                lvItem = New ListViewItem

                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))

                lvItem.SubItems.Add(dsRow.Item("code").ToString)

                lvItem.SubItems.Add(dsRow.Item("name").ToString)
                lvItem.SubItems(colhEContribHead.Index).Tag = objTranHead.GetEmpContribPayableHeadID(CInt(dsRow.Item("tranheadunkid")))

                RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
                lvTranHead.Items.Add(lvItem)
                AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTranHead.Items
                RemoveHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvTranHead.ItemChecked, AddressOf lvTranHead_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHeads", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            txtDebitAmountFrom.Decimal = 0
            txtDebitAmountTo.Decimal = 0
            txtCreditAMountFrom.Decimal = 0
            txtCreditAMountTo.Decimal = 0

            objPaymentJV.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objPaymentJV.OrderByDisplay


            chkInactiveemp.Checked = False

            chkShowSummaryBottom.Checked = False
            chkShowSummaryNewPage.Checked = False

            cboBranch.SelectedValue = 0

            chkShowGroupByCCGroup.Checked = False
            cboCCGroup.SelectedValue = 0
            chkIgnoreZero.Checked = True
            objchkSelectAll.Checked = True

            cboCustomCCenter.SelectedValue = 0
            chkShowColumnHeader.Checked = False

            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter(ByRef dtPeriodStartDate As Date _
                              , ByRef dtPeriodEndDate As Date _
                              ) As Boolean
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Function
            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = cboReportType.Text


            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._ShowSummaryAtBootom = chkShowSummaryBottom.Checked
            objPaymentJV._ShowSummaryOnNextPage = chkShowSummaryNewPage.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._ShowGroupByCCenerGroup = chkShowGroupByCCGroup.Checked
            objPaymentJV._CCenterGroupId = CInt(cboCCGroup.SelectedValue)
            objPaymentJV._CCenterGroup_Name = cboCCGroup.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked


            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            dtPeriodStartDate = objPeriod._Start_Date
            dtPeriodEndDate = objPeriod._End_Date
            'Sohail (21 Aug 2015) -- End

            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "
    Private Sub frmPaymentJVReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentJV = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentJVReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                    e.Handled = True
                Case Keys.R
                    If e.Control = True Then
                        Call frmPaymentJVReport_Report_Click(Nothing, Nothing)
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentJVReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Message = objPaymentJV._ReportDesc
            Me._Title = objPaymentJV._ReportName

            Call FillCombo()
            Call ResetValue()

            If ConfigParameter._Object._AccountingSoftWare = enIntegration.SAP_JV Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_Load", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Buttons "

    Private Sub frmPaymentJVReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        'Sohail (21 Aug 2015) -- End
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objPaymentJV.generateReport(0, e.Type, enExportAction.None)
            If SetFilter(dtPeriodStart, dtPeriodEnd) = False Then Exit Sub

            objPaymentJV.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmPaymentJVReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim dtPeriodStart As Date = Nothing
        Dim dtPeriodEnd As Date = Nothing
        'Sohail (21 Aug 2015) -- End
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetFilter() = False Then Exit Sub

            'objPaymentJV.generateReport(0, enPrintAction.None, e.Type)
            If SetFilter(dtPeriodStart, dtPeriodEnd) = False Then Exit Sub

            objPaymentJV.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentJVReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentJVReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentJVReport_Language_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPaymentJVReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPaymentJVReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmPaymentJVReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

#End Region

#Region " CheckBox's Events "
    Private Sub chkShowSummaryBottom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowSummaryBottom.CheckedChanged
        Try
            If chkShowSummaryBottom.Checked = True Then chkShowSummaryNewPage.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowSummaryBottom_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowSummaryNewPage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowSummaryNewPage.CheckedChanged
        Try
            If chkShowSummaryNewPage.Checked = True Then chkShowSummaryBottom.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowSummaryNewPage_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkShowGroupByCCGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowGroupByCCGroup.CheckedChanged
        Try
            If chkShowGroupByCCGroup.Checked = True Then
                cboCCGroup.Enabled = True
            Else
                cboCCGroup.Enabled = False
                cboCCGroup.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowGroupByCCGroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllHeads(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview's Events "
    Private Sub lvTranHead_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTranHead.ItemChecked
        Try
            If lvTranHead.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count < lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf lvTranHead.CheckedItems.Count = lvTranHead.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTranHead_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPaymentJV.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objPaymentJV.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then 'Main ledger, Combined JV
                chkShowSummaryBottom.Checked = False
                chkShowSummaryNewPage.Checked = False

                chkShowSummaryBottom.Visible = False
                chkShowSummaryNewPage.Visible = False
            Else
                chkShowSummaryBottom.Visible = True
                chkShowSummaryNewPage.Visible = True
            End If

            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.Sun_Account Then
                'lnkSunJVExport.Visible = True
            Else
                lnkSunJVExport.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.iScala Then
                'lnkiScalaJVExport.Visible = True
                lnkiScalaJVExport.Location = lnkSunJVExport.Location
                lnkiScalaJVExport.Size = lnkSunJVExport.Size
            Else
                lnkiScalaJVExport.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.SAP_JV Then
                'lnkTBCJVExport.Visible = True
                lnkTBCJVExport.Location = lnkSunJVExport.Location
                lnkTBCJVExport.Size = lnkSunJVExport.Size

                'gbFilterTBCJV.Visible = True
            Else
                lnkTBCJVExport.Visible = False

                gbFilterTBCJV.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.Sun_Account_5 Then
                'lnkSunJV5Export.Visible = True
                lnkSunJV5Export.Location = lnkSunJVExport.Location
                lnkSunJV5Export.Size = lnkSunJVExport.Size
            Else
                lnkSunJV5Export.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso ConfigParameter._Object._AccountingSoftWare = enIntegration.iScala_2 Then
                'lnkiScala2JVExport.Visible = True
                lnkiScala2JVExport.Location = lnkSunJVExport.Location
                lnkiScala2JVExport.Size = lnkSunJVExport.Size

                'lblCustomCCenter.Visible = True
                'cboCustomCCenter.Visible = True
                'lblInvoiceRef.Visible = True
                'txtInvoiceRef.Visible = True
                'chkShowColumnHeader.Visible = True
            Else
                lnkiScala2JVExport.Visible = False

                lblCustomCCenter.Visible = False
                cboCustomCCenter.Visible = False
                cboCustomCCenter.SelectedValue = 0
                lblInvoiceRef.Visible = False
                txtInvoiceRef.Visible = False
                txtInvoiceRef.Text = ""
                chkShowColumnHeader.Visible = False
                chkShowColumnHeader.Checked = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSunJVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSunJVExport.LinkClicked
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try

            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "Text File|*.txt"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = cboReportType.Text

            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            'If objPaymentJV.Sun_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSunJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkiScalaJVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkiScalaJVExport.LinkClicked
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = "iScala"

            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            'If objPaymentJV.iScala_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkiScalaJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkiScala2JVExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkiScala2JVExport.LinkClicked
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCustomCCenter.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please Select Custom Cost Center."), enMsgBoxStyle.Information)
                cboCustomCCenter.Focus()
                Exit Try
            Else

            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = "iScala2"

            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked

            objPaymentJV._CustomCCetnterId = CInt(cboCustomCCenter.SelectedValue)
            objPaymentJV._CustomCCetnterName = cboCustomCCenter.Text
            objPaymentJV._InvoiceReference = txtInvoiceRef.Text.Trim
            objPaymentJV._ShowColumnHeader = chkShowColumnHeader.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            'If objPaymentJV.iScala2_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkiScala2JVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkTBCJVExport_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkTBCJVExport.LinkClicked
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                SaveDialog.FileName = ""
                SaveDialog.Filter = "CSV File|*.csv"
                SaveDialog.FilterIndex = 0
                SaveDialog.ShowDialog()
                If SaveDialog.FileName = "" Then
                    Exit Try
                End If
            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = cboReportType.Text

            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked


            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            'If objPaymentJV.TBC_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkTBCJVExport_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSunJV5Export_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSunJV5Export.LinkClicked
        Try
            objPaymentJV.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Try
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Exit Try
            Else
                'SaveDialog.FileName = ""
                'SaveDialog.Filter = "Excel File|*.xls"
                'SaveDialog.FilterIndex = 0
                'SaveDialog.ShowDialog()
                'If SaveDialog.FileName = "" Then
                '    Exit Try
                'End If
            End If

            objPaymentJV._PeriodId = cboPeriod.SelectedValue
            objPaymentJV._PeriodName = cboPeriod.Text

            objPaymentJV._DebitAmountFrom = txtDebitAmountFrom.Decimal
            objPaymentJV._DebitAmountTo = txtDebitAmountTo.Decimal

            objPaymentJV._CreditAmountFrom = txtCreditAMountFrom.Decimal
            objPaymentJV._CreditAmountTo = txtCreditAMountTo.Decimal

            objPaymentJV._ReportId = cboReportType.SelectedIndex
            objPaymentJV._ReportName = "Sun_JV_5"

            objPaymentJV._IsActive = chkInactiveemp.Checked

            objPaymentJV._BranchId = cboBranch.SelectedValue
            objPaymentJV._Branch_Name = cboBranch.Text

            objPaymentJV._IgnoreZero = chkIgnoreZero.Checked

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objPaymentJV._PeriodStartDate = objPeriod._Start_Date
            objPaymentJV._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing

            objPaymentJV._Ex_Rate = mdecEx_Rate
            objPaymentJV._Currency_Sign = mstrCurr_Sign

            'If objPaymentJV.Sun5_JV_Export_CombinedJVReport(SaveDialog.FileName) = True Then
            '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSunJV5Export_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , cboPeriod.SelectedIndexChanged
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                        lblExRate.Text = Language.getMessage(mstrModuleName, 9, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterTBCJV.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterTBCJV.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDebitAmountTo.Text = Language._Object.getCaption(Me.lblDebitAmountTo.Name, Me.lblDebitAmountTo.Text)
            Me.lblDebitAmountFrom.Text = Language._Object.getCaption(Me.lblDebitAmountFrom.Name, Me.lblDebitAmountFrom.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCreditAMountTo.Text = Language._Object.getCaption(Me.lblCreditAMountTo.Name, Me.lblCreditAMountTo.Text)
            Me.lblCreditAMountFrom.Text = Language._Object.getCaption(Me.lblCreditAMountFrom.Name, Me.lblCreditAMountFrom.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.chkShowSummaryBottom.Text = Language._Object.getCaption(Me.chkShowSummaryBottom.Name, Me.chkShowSummaryBottom.Text)
            Me.chkShowSummaryNewPage.Text = Language._Object.getCaption(Me.chkShowSummaryNewPage.Name, Me.chkShowSummaryNewPage.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.chkShowGroupByCCGroup.Text = Language._Object.getCaption(Me.chkShowGroupByCCGroup.Name, Me.chkShowGroupByCCGroup.Text)
            Me.lblCCGroup.Text = Language._Object.getCaption(Me.lblCCGroup.Name, Me.lblCCGroup.Text)
            Me.lnkSunJVExport.Text = Language._Object.getCaption(Me.lnkSunJVExport.Name, Me.lnkSunJVExport.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lnkiScalaJVExport.Text = Language._Object.getCaption(Me.lnkiScalaJVExport.Name, Me.lnkiScalaJVExport.Text)
            Me.lnkTBCJVExport.Text = Language._Object.getCaption(Me.lnkTBCJVExport.Name, Me.lnkTBCJVExport.Text)
            Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
            Me.gbFilterTBCJV.Text = Language._Object.getCaption(Me.gbFilterTBCJV.Name, Me.gbFilterTBCJV.Text)
            Me.colhCheck.Text = Language._Object.getCaption(CStr(Me.colhCheck.Tag), Me.colhCheck.Text)
            Me.colhEContribHead.Text = Language._Object.getCaption(CStr(Me.colhEContribHead.Tag), Me.colhEContribHead.Text)
            Me.colhEContribHeadCode.Text = Language._Object.getCaption(CStr(Me.colhEContribHeadCode.Tag), Me.colhEContribHeadCode.Text)
            Me.lnkSunJV5Export.Text = Language._Object.getCaption(Me.lnkSunJV5Export.Name, Me.lnkSunJV5Export.Text)
            Me.lnkiScala2JVExport.Text = Language._Object.getCaption(Me.lnkiScala2JVExport.Name, Me.lnkiScala2JVExport.Text)
            Me.lblCustomCCenter.Text = Language._Object.getCaption(Me.lblCustomCCenter.Name, Me.lblCustomCCenter.Text)
            Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.Name, Me.chkShowColumnHeader.Text)
            Me.lblInvoiceRef.Text = Language._Object.getCaption(Me.lblInvoiceRef.Name, Me.lblInvoiceRef.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Main Ledger")
            Language.setMessage(mstrModuleName, 2, "General Staff Ledger")
            Language.setMessage(mstrModuleName, 3, "Cost Center Ledger")
            Language.setMessage(mstrModuleName, 4, "Please Select Period.")
            Language.setMessage(mstrModuleName, 5, "Combined JV")
            Language.setMessage(mstrModuleName, 6, "Data Exported Successfuly.")
            Language.setMessage(mstrModuleName, 7, "Please Select Combined JV.")
            Language.setMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected.")
            Language.setMessage(mstrModuleName, 9, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 10, "Please Select Custom Cost Center.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

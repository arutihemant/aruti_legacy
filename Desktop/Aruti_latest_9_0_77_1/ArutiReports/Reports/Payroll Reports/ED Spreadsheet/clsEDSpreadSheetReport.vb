Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

Public Class clsEDSpreadSheetReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEDSpreadSheetReport"
    Private mstrReportId As String = enArutiReport.EDSpreadSheet
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""

    Dim dblColTot As Decimal()

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""

    'Sandeep [ 09 MARCH 2011 ] -- Start
    Dim StrFinalPath As String = String.Empty
    'Sandeep [ 09 MARCH 2011 ] -- End 


    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    Private mstrEarningHeadsIds As String = String.Empty
    Private mstrDeductionHeadIds As String = String.Empty
    Private mstrNonEarningDeductionTranHeadIds As String = String.Empty 'Sohail (09 Mar 2012)
    'S.SANDEEP [ 08 June 2011 ] -- START


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintTotEarningCols As Integer = -1
    Private mintTotDeductionCols As Integer = -1
    Private mintTotNonEarningDeductionCols As Integer = -1

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = "" 'Sohail (08 Mar 2013)
    Private mstrReport_GroupName As String = ""
    'Sohail (09 Mar 2012) -- End

    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIgnorezeroHeads As Boolean = False
    'Pinkal (05-Mar-2012) -- End

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnShowLoan As Boolean = False
    Private mblnShowAdvance As Boolean = False
    Private mblnShowSavings As Boolean = False
    'Sohail (21 Nov 2014) -- Start
    'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
    Private mblnShowPaymentDetails As Boolean = False
    Private mstrUserAccessFilter As String = ""
    'Sohail (21 Nov 2014) -- End

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    'S.SANDEEP [ 11 SEP 2012 ] -- END
    Private mstrCurrency_Rate As String = String.Empty 'Sohail (18 Jun 2013)

    Private mintModeId As Integer = 0 'Sohail (19 Oct 2012)

    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Pinkal (14-Dec-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'S.SANDEEP [ 08 June 2011 ] -- START
    'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    Public WriteOnly Property _EarningHeads() As String
        Set(ByVal value As String)
            mstrEarningHeadsIds = value
        End Set
    End Property

    Public WriteOnly Property _DeductionHeads() As String
        Set(ByVal value As String)
            mstrDeductionHeadIds = value
        End Set
    End Property
    'S.SANDEEP [ 08 June 2011 ] -- START
    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _NonEarningDeductionHeads() As String
        Set(ByVal value As String)
            mstrNonEarningDeductionTranHeadIds = value
        End Set
    End Property
    'Sohail (09 Mar 2012) -- End

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    'Sohail (08 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property
    'Sohail (08 Mar 2013) -- End

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (09 Mar 2012) -- End

    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property
    'Pinkal (05-Mar-2012) -- End

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ShowLoan() As Boolean
        Set(ByVal value As Boolean)
            mblnShowLoan = value
        End Set
    End Property

    Public WriteOnly Property _ShowAdvance() As Boolean
        Set(ByVal value As Boolean)
            mblnShowAdvance = value
        End Set
    End Property

    Public WriteOnly Property _ShowSavings() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSavings = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property
    'S.SANDEEP [ 11 SEP 2012 ] -- END

    'Sohail (19 Oct 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property
    'Sohail (19 Oct 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Sohail (18 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Currency_Rate() As String
        Set(ByVal value As String)
            mstrCurrency_Rate = value
        End Set
    End Property
    'Sohail (18 Jun 2013) -- End

    'Sohail (21 Nov 2014) -- Start
    'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
    Public WriteOnly Property _ShowPaymentDetails() As Boolean
        Set(ByVal value As Boolean)
            mblnShowPaymentDetails = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'Sohail (21 Nov 2014) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = ""
            mintPeriodId = -1
            mstrPeriodName = ""

            'Vimal (27 Nov 2010) -- Start 
            mintReportTypeId = 0
            mstrReportTypeName = ""
            'Vimal (27 Nov 2010) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            mblnIgnorezeroHeads = False
            'Pinkal (05-Mar-2012) -- End

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            mblnShowLoan = True
            mblnShowAdvance = True
            mblnShowSavings = True
            'Sohail (17 Apr 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrCurrency_Sign = ""
            mdecEx_Rate = 0
            'S.SANDEEP [ 11 SEP 2012 ] -- END
            mstrCurrency_Rate = "" 'Sohail (18 Jun 2013)

            mblnShowPaymentDetails = False 'Sohail (21 Nov 2014)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Period : ") & " " & mstrPeriodName & " "
            End If


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder ' 
        Dim blnFlag As Boolean = False
        Try

            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            Dim icolumnCnt As Integer = 0
            For j As Integer = 0 To objDataReader.Columns.Count - 1

                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "TGP" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(j).ColumnName <> "OUDPay" Then

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = '0' OR " & objDataReader.Columns(j).ColumnName & " = '0.00'")
                            If drRow.Length = objDataReader.Rows.Count Then
                                icolumnCnt += 1
                            End If
                        End If

                    End If
                End If
            Next

            'Pinkal (05-Mar-2012) -- End


            strBuilder.Append(" <TITLE> " & Me._ReportName & " " & mstrReportTypeName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 3, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If mintReportTypeId = 0 Then
                ' strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 4 & " align='center' > " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - icolumnCnt - 4 & " align='center' > " & vbCrLf)
            ElseIf mintReportTypeId = 1 Then
                'strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 5 & " align='center' > " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - icolumnCnt - 5 & " align='center' > " & vbCrLf)
            End If

            'Pinkal (05-Mar-2012) -- End

            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If mintReportTypeId = 0 Then
                ' strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 4 & "  align='center' > " & vbCrLf)
                strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - icolumnCnt - 4 & "  align='center' > " & vbCrLf)
            ElseIf mintReportTypeId = 1 Then
                'strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 5 & "  align='center' > " & vbCrLf)
                strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - icolumnCnt - 5 & "  align='center' > " & vbCrLf)
            End If

            'Pinkal (05-Mar-2012) -- End

            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & " " & mstrReportTypeName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            If mintReportTypeId = 0 Then
                For j As Integer = 0 To objDataReader.Columns.Count - 1

                    'Pinkal (05-Mar-2012) -- Start
                    'Enhancement : TRA Changes
                    If mblnIgnorezeroHeads Then

                        If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "TGP" _
                               AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" _
                               AndAlso objDataReader.Columns(j).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(j).ColumnName <> "OUDPay" Then

                            Dim drRow As DataRow() = Nothing
                            If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                                drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = '0' OR " & objDataReader.Columns(j).ColumnName & " = '0.00'")
                                If drRow.Length = objDataReader.Rows.Count Then
                                    Continue For
                                End If
                            End If

                        End If

                    End If
                    'Pinkal (05-Mar-2012) -- End

                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                Next

            ElseIf mintReportTypeId = 1 Then
                For j As Integer = 0 To objDataReader.Columns.Count - 2

                    'Pinkal (05-Mar-2012) -- Start
                    'Enhancement : TRA Changes
                    If mblnIgnorezeroHeads Then

                        If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "TGP" _
                               AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" _
                               AndAlso objDataReader.Columns(j).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(j).ColumnName <> "OUDPay" Then

                            Dim drRow As DataRow() = Nothing
                            If objDataReader.Columns(j).DataType Is Type.GetType("System.String") Then
                                drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = '0' OR " & objDataReader.Columns(j).ColumnName & " = '0.00'")
                                If drRow.Length = objDataReader.Rows.Count Then
                                    Continue For
                                End If
                            End If

                        End If

                    End If
                    'Pinkal (05-Mar-2012) -- End

                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                Next
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            If mintReportTypeId = 0 Then
                For i As Integer = 0 To objDataReader.Rows.Count - 1
                    strBuilder.Append(" <TR> " & vbCrLf)
                    For k As Integer = 0 To objDataReader.Columns.Count - 1
                        'S.SANDEEP [ 17 AUG 2011 ] -- START
                        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                        'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)

                        'Pinkal (05-Mar-2012) -- Start
                        'Enhancement : TRA Changes
                        If mblnIgnorezeroHeads Then

                            If objDataReader.Columns(k).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(k).ColumnName <> "EmpName" AndAlso objDataReader.Columns(k).ColumnName <> "TGP" _
                                AndAlso objDataReader.Columns(k).ColumnName <> "TDD" AndAlso objDataReader.Columns(k).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(k).ColumnName <> "NetPay" _
                                AndAlso objDataReader.Columns(k).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(k).ColumnName <> "OUDPay" Then

                                Dim drRow As DataRow() = Nothing
                                If objDataReader.Columns(k).DataType Is Type.GetType("System.String") Then
                                    drRow = objDataReader.Select(objDataReader.Columns(k).ColumnName & " = '0' OR " & objDataReader.Columns(k).ColumnName & " = '0.00'")
                                    If drRow.Length = objDataReader.Rows.Count Then
                                        Continue For
                                    End If
                                End If
                            End If

                        End If
                        'Pinkal (05-Mar-2012) -- End

                        Select Case k
                            Case 0, 1
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                        End Select
                        'S.SANDEEP [ 17 AUG 2011 ] -- END 
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

            ElseIf mintReportTypeId = 1 Then
                Dim GroupArrayTotal(objDataReader.Columns.Count - 4) As Double

                For i As Integer = 0 To objDataReader.Rows.Count - 1
                    strBuilder.Append(" <TR> " & vbCrLf)
                    If objDataReader.Rows(i)("isGroup") = True AndAlso i > 0 Then
                        For p As Integer = 0 To objDataReader.Columns.Count - 2

                            'Pinkal (05-Mar-2012) -- Start
                            'Enhancement : TRA Changes
                            If mblnIgnorezeroHeads Then

                                If objDataReader.Columns(p).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(p).ColumnName <> "EmpName" AndAlso objDataReader.Columns(p).ColumnName <> "TGP" _
                                       AndAlso objDataReader.Columns(p).ColumnName <> "TDD" AndAlso objDataReader.Columns(p).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(p).ColumnName <> "NetPay" _
                                       AndAlso objDataReader.Columns(p).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(p).ColumnName <> "OUDPay" Then

                                    Dim drRow As DataRow() = Nothing
                                    If objDataReader.Columns(p).DataType Is Type.GetType("System.String") Then
                                        drRow = objDataReader.Select(objDataReader.Columns(p).ColumnName & " = '0' OR " & objDataReader.Columns(p).ColumnName & " = '0.00'")
                                        If drRow.Length = objDataReader.Rows.Count Then
                                            Continue For
                                        End If
                                    End If

                                End If

                            End If
                            'Pinkal (05-Mar-2012) -- End

                            If p = 0 Then
                                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                            ElseIf p = 1 Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                                'S.SANDEEP [ 30 May 2011 ] -- START
                            ElseIf p = objDataReader.Columns.Count - 2 Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp; (" & Format(System.Math.Abs(CDec(GroupArrayTotal(p - 2))), GUI.fmtCurrency) & ")</B></FONT></TD>" & vbCrLf)
                                'S.SANDEEP [ 30 May 2011 ] -- END 
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 2)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                            End If
                        Next
                        strBuilder.Append("</TR> " & vbCrLf)

                        strBuilder.Append(" <TR> " & vbCrLf)
                        ReDim GroupArrayTotal(objDataReader.Columns.Count - 4)
                    End If
                    For k As Integer = 0 To objDataReader.Columns.Count - 2

                        'Pinkal (05-Mar-2012) -- Start
                        'Enhancement : TRA Changes
                        If mblnIgnorezeroHeads Then

                            If objDataReader.Columns(k).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(k).ColumnName <> "EmpName" AndAlso objDataReader.Columns(k).ColumnName <> "TGP" _
                                   AndAlso objDataReader.Columns(k).ColumnName <> "TDD" AndAlso objDataReader.Columns(k).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(k).ColumnName <> "NetPay" _
                                   AndAlso objDataReader.Columns(k).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(k).ColumnName <> "OUDPay" Then

                                Dim drRow As DataRow() = Nothing
                                If objDataReader.Columns(k).DataType Is Type.GetType("System.String") Then
                                    drRow = objDataReader.Select(objDataReader.Columns(k).ColumnName & " = '0' OR " & objDataReader.Columns(k).ColumnName & " = '0.00'")
                                    If drRow.Length = objDataReader.Rows.Count Then
                                        Continue For
                                    End If
                                End If
                            End If

                        End If
                        'Pinkal (05-Mar-2012) -- End

                        If objDataReader.Rows(i)("isGroup") = True Then
                            If k = 0 Then

                                'Pinkal (05-Mar-2012) -- Start
                                'Enhancement : TRA Changes

                                'strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 1 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Department :") & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - icolumnCnt - 1 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Department :") & objDataReader.Rows(i)(k) & "</B></FONT></TD>" & vbCrLf)
                                'Pinkal (05-Mar-2012) -- End

                            Else
                                'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                            End If

                        Else

                            'S.SANDEEP [ 17 AUG 2011 ] -- START
                            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                            'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            Select Case k
                                Case 0, 1
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                                Case Else
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                            End Select
                            'S.SANDEEP [ 17 AUG 2011 ] -- END 
                            If i > 0 AndAlso k > 1 Then
                                GroupArrayTotal(k - 2) += CDec(objDataReader.Rows(i)(k))
                            End If
                        End If
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" <TR> " & vbCrLf)
                For p As Integer = 0 To objDataReader.Columns.Count - 2

                    'Pinkal (05-Mar-2012) -- Start
                    'Enhancement : TRA Changes
                    If mblnIgnorezeroHeads Then

                        If objDataReader.Columns(p).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(p).ColumnName <> "EmpName" AndAlso objDataReader.Columns(p).ColumnName <> "TGP" _
                               AndAlso objDataReader.Columns(p).ColumnName <> "TDD" AndAlso objDataReader.Columns(p).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(p).ColumnName <> "NetPay" _
                               AndAlso objDataReader.Columns(p).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(p).ColumnName <> "OUDPay" Then

                            Dim drRow As DataRow() = Nothing
                            If objDataReader.Columns(p).DataType Is Type.GetType("System.String") Then
                                drRow = objDataReader.Select(objDataReader.Columns(p).ColumnName & " = '0' OR " & objDataReader.Columns(p).ColumnName & " = '0.00'")
                                If drRow.Length = objDataReader.Rows.Count Then
                                    Continue For
                                End If
                            End If
                        End If

                    End If
                    'Pinkal (05-Mar-2012) -- End

                    If p = 0 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                    ElseIf p = 1 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 2)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            End If


            strBuilder.Append(" <TR> " & vbCrLf)
            Dim intLoop As Integer = 0
            If mintReportTypeId = 0 Then
                intLoop = objDataReader.Columns.Count - 1

            ElseIf mintReportTypeId = 1 Then
                intLoop = objDataReader.Columns.Count - 2
            End If

            For k As Integer = 0 To intLoop

                'Pinkal (05-Mar-2012) -- Start
                'Enhancement : TRA Changes
                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(k).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(k).ColumnName <> "EmpName" AndAlso objDataReader.Columns(k).ColumnName <> "TGP" _
                               AndAlso objDataReader.Columns(k).ColumnName <> "TDD" AndAlso objDataReader.Columns(k).ColumnName <> "Openingbalance" AndAlso objDataReader.Columns(k).ColumnName <> "NetPay" _
                               AndAlso objDataReader.Columns(k).ColumnName <> "TotNetPay" AndAlso objDataReader.Columns(k).ColumnName <> "OUDPay" Then

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(k).DataType Is Type.GetType("System.String") Then
                            drRow = objDataReader.Select(objDataReader.Columns(k).ColumnName & " = '0' OR " & objDataReader.Columns(k).ColumnName & " = '0.00'")
                            If drRow.Length = objDataReader.Rows.Count Then
                                Continue For
                            End If
                        End If

                    End If

                End If
                'Pinkal (05-Mar-2012) -- End

                If k = 0 Then
                    'S.SANDEEP [ 17 AUG 2011 ] -- START
                    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
                    'strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 17 AUG 2011 ] -- END 
                ElseIf k <= 1 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & objDataReader.Rows.Count & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 30 May 2011 ] -- START
                    'Else
                    '    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(cdec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                ElseIf k = intLoop Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;( " & Format(System.Math.Abs(CDec(dblColTot(k))), GUI.fmtCurrency) & " ) </B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                    'S.SANDEEP [ 30 May 2011 ] -- END 
                End If
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)



            'Sandeep [ 09 MARCH 2011 ] -- Start
            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            'Sandeep [ 09 MARCH 2011 ] -- End 

            If SaveExcelfile(SavePath & "\" & flFileName & ".xls", strBuilder) Then
                'Sandeep [ 09 MARCH 2011 ] -- Start
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
                'Sandeep [ 09 MARCH 2011 ] -- End 
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Private Function Export_to_Excel_New(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder ' 
        Dim blnFlag As Boolean = False
        Try

            strBuilder.Append(" <TITLE> " & Me._ReportName & " " & mstrReportTypeName & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If
                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 3, "Prepared By :") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(User._Object._Username & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            If mintViewIndex > 0 Then
                strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 7 & " align='center' > " & vbCrLf)
            Else
                strBuilder.Append(" <TD WIDTH='60%' colspan=" & objDataReader.Columns.Count - 4 & " align='center' > " & vbCrLf)
            End If

            strBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 4, "Date :") & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)

            If mintViewIndex > 0 Then
                strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 6 & "  align='center' > " & vbCrLf)
            Else
                strBuilder.Append(" <TD width='60%' colspan=" & objDataReader.Columns.Count - 4 & "  align='center' > " & vbCrLf)
            End If

            strBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & " " & mstrReportTypeName & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            If mintViewIndex > 0 Then
                For j As Integer = 1 To objDataReader.Columns.Count - 3
                    If j <= 2 Then
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 3 Then 'Earning
                        strBuilder.Append("<TD colspan=" & mintTotEarningCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Earning</B></FONT></TD>" & vbCrLf)
                    ElseIf j = 2 + mintTotEarningCols + 1 Then 'Gross Pay
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 2 + mintTotEarningCols + 2 Then 'Deduction
                        strBuilder.Append("<TD colspan=" & mintTotDeductionCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Deduction</B></FONT></TD>" & vbCrLf)
                    ElseIf j > 3 + mintTotEarningCols + mintTotDeductionCols AndAlso j < 3 + mintTotEarningCols + mintTotDeductionCols + 6 Then
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 3 + mintTotEarningCols + mintTotDeductionCols + 6 Then 'Non Earning Deduction
                        strBuilder.Append("<TD colspan=" & mintTotNonEarningDeductionCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Non-Earning Deduction</B></FONT></TD>" & vbCrLf)
                    Else

                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                For j As Integer = 1 To objDataReader.Columns.Count - 3
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                Next
            Else
                For j As Integer = 1 To objDataReader.Columns.Count - 1
                    If j <= 2 Then
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 3 Then 'Earning
                        strBuilder.Append("<TD colspan=" & mintTotEarningCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Earning</B></FONT></TD>" & vbCrLf)
                    ElseIf j = 2 + mintTotEarningCols + 1 Then 'Gross Pay
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 2 + mintTotEarningCols + 2 Then 'Deduction
                        strBuilder.Append("<TD colspan=" & mintTotDeductionCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Deduction</B></FONT></TD>" & vbCrLf)
                    ElseIf j > 3 + mintTotEarningCols + mintTotDeductionCols AndAlso j < 3 + mintTotEarningCols + mintTotDeductionCols + 6 Then
                        strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    ElseIf j = 3 + mintTotEarningCols + mintTotDeductionCols + 6 Then 'Non Earning Deduction
                        strBuilder.Append("<TD colspan=" & mintTotNonEarningDeductionCols & " BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>Non-Earning Deduction</B></FONT></TD>" & vbCrLf)
                    Else

                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                For j As Integer = 1 To objDataReader.Columns.Count - 1
                    strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                Next
            End If
            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            Dim intPrevGrpID As Integer = 0
            Dim intPrevEmpID As Integer = 0
            If mintViewIndex > 0 Then
                Dim GroupArrayTotal(objDataReader.Columns.Count - 5) As Double

                For i As Integer = 0 To objDataReader.Rows.Count - 1
                    strBuilder.Append(" <TR> " & vbCrLf)

                    If intPrevGrpID = 0 OrElse intPrevGrpID <> CInt(objDataReader.Rows(i).Item("ID")) Then
                        If i > 0 Then '*** Sub Total ********
                            For p As Integer = 1 To objDataReader.Columns.Count - 3
                                If p = 1 Then
                                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                                ElseIf p = 2 Then
                                    'S.SANDEEP [ 18 SEP 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                                    Dim iCnt As Integer = objDataReader.Compute("COUNT(EmpCode)", "Id = '" & intPrevGrpID & "'")
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><b>" & iCnt.ToString & "</b></FONT></TD>" & vbCrLf)
                                    'S.SANDEEP [ 18 SEP 2012 ] -- END
                                ElseIf objDataReader.Columns(p).ColumnName = "OUDPay" Then
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp; (" & Format(System.Math.Abs(CDec(GroupArrayTotal(p - 3))), GUI.fmtCurrency) & ")</B></FONT></TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 3)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                                End If
                            Next
                            strBuilder.Append("</TR> " & vbCrLf)

                            strBuilder.Append(" <TR> " & vbCrLf)
                            ReDim GroupArrayTotal(objDataReader.Columns.Count - 5)
                        End If

                        '*** Group Header ***
                        strBuilder.Append("<TD colspan=" & objDataReader.Columns.Count - 3 & " BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & mstrReport_GroupName & objDataReader.Rows(i)("GNAME") & "</B></FONT></TD>" & vbCrLf)

                        strBuilder.Append(" </TR> " & vbCrLf)
                        strBuilder.Append(" <TR> " & vbCrLf)
                    End If

                    For k As Integer = 1 To objDataReader.Columns.Count - 3
                        Select Case k
                            Case 0 To 2
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>&nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>&nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                        End Select

                        If k > 2 Then
                            GroupArrayTotal(k - 3) += CDec(objDataReader.Rows(i)(k))
                        End If
                    Next

                    strBuilder.Append(" </TR> " & vbCrLf)

                    intPrevGrpID = CInt(objDataReader.Rows(i).Item("ID"))
                    intPrevEmpID = CInt(objDataReader.Rows(i).Item("EmpID"))
                Next

                strBuilder.Append(" <TR> " & vbCrLf)
                For p As Integer = 1 To objDataReader.Columns.Count - 3
                    If p = 1 Then
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 6, "Sub Total :") & "</B></FONT></TD>" & vbCrLf)
                    ElseIf p = 2 Then
                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & "" & "</FONT></TD>" & vbCrLf)
                        Dim iCnt As Integer = objDataReader.Compute("COUNT(EmpCode)", "Id = '" & intPrevGrpID & "'")
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><b>" & iCnt.ToString & "</b></FONT></TD>" & vbCrLf)
                        'S.SANDEEP [ 18 SEP 2012 ] -- END
                    ElseIf objDataReader.Columns(p).ColumnName = "OUDPay" Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp; (" & Format(System.Math.Abs(CDec(GroupArrayTotal(p - 3))), GUI.fmtCurrency) & ")</B></FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(GroupArrayTotal(p - 3)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

            Else
                For i As Integer = 0 To objDataReader.Rows.Count - 1
                    strBuilder.Append(" <TR> " & vbCrLf)
                    For k As Integer = 1 To objDataReader.Columns.Count - 1
                        Select Case k
                            Case 0 To 2
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                            Case Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2> &nbsp;" & Format(CDec(objDataReader.Rows(i)(k)), GUI.fmtCurrency) & "</FONT></TD>" & vbCrLf)
                        End Select
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            Dim intLoop As Integer = 0
            If mintViewIndex > 0 Then
                intLoop = objDataReader.Columns.Count - 3
            Else
                intLoop = objDataReader.Columns.Count - 1

            End If

            For k As Integer = 1 To intLoop
                If k = 1 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 5, "Grand Total :") & "</B></FONT></TD>" & vbCrLf)
                ElseIf k <= 2 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & objDataReader.Rows.Count & "</B></FONT></TD>" & vbCrLf)
                ElseIf objDataReader.Columns(k).ColumnName = "OUDPay" Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;( " & Format(System.Math.Abs(CDec(dblColTot(k))), GUI.fmtCurrency) & " ) </B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>&nbsp;" & Format(CDec(dblColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                End If
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)



            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If
            If SavePath.Substring(SavePath.Length - 1, 1) = "\" Then
                StrFinalPath = SavePath & flFileName & ".xls"
            Else
                StrFinalPath = SavePath & "\" & flFileName & ".xls"
            End If
            If SaveExcelfile(StrFinalPath, strBuilder) Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
    'Sohail (09 Mar 2012) -- End

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                Dim drRow As DataRow() = Nothing
                If objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "TGP" _
                    AndAlso objDataReader.Columns(j).ColumnName <> "OUDPay" AndAlso objDataReader.Columns(j).ColumnName <> "TotNetPay" _
                    AndAlso (mblnShowLoan And objDataReader.Columns(j).ColumnName = "Loan") _
                    AndAlso (mblnShowAdvance And objDataReader.Columns(j).ColumnName = "Advance") _
                    AndAlso (mblnShowSavings And objDataReader.Columns(j).ColumnName = "Saving") Then

                    If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                        drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 OR " & objDataReader.Columns(j).ColumnName & " = 0.00")
                        If drRow.Length = objDataReader.Rows.Count Then
                            objDataReader.Columns.RemoveAt(j)
                            GoTo SetColumnCount
                        End If
                    End If
                End If
            Next

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreZeroHead; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (14-Dec-2012) -- End


#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'Sohail (07 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 May 2014) -- Start
            'Enhancement - performance issue on ed spreadsheet for more than 2000 employees (TANAPA).
            'iColumn_DetailReport.Add(New IColumn("ISNULL(vwPayroll.employeecode,'')", Language.getMessage(mstrModuleName, 33, "Employee Code")))
            'iColumn_DetailReport.Add(New IColumn("ISNULL(vwPayroll.employeename, '')", Language.getMessage(mstrModuleName, 34, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 16, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname", Language.getMessage(mstrModuleName, 17, "Employee Name")))
            'Sohail (21 May 2014) -- End
            'Sohail (07 Aug 2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    'Public Sub Generate_EDSpreadsheetReport()
    '    Dim dsEarning As New DataSet
    '    Dim dsDeduction As New DataSet
    '    Dim dtFinalTable As DataTable
    '    Dim dtTable As DataTable
    '    Dim dtDeduction As DataTable
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty

    '    Try

    '        If ConfigParameter._Object._ExportReportPath = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        objDataOperation = New clsDataOperation

    '        dtFinalTable = New DataTable("EDSpreadsheet")
    '        Dim dCol As DataColumn

    '        dCol = New DataColumn("EmpCode")
    '        dCol.Caption = "Code"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("EmpName")
    '        dCol.Caption = "Employee"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'StrQ = "SELECT " & _
    '        '       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '        '       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '        '       "FROM prtranhead_master " & _
    '        '       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '       "AND prtranhead_master.trnheadtype_id = 1 ORDER BY prtranhead_master.tranheadunkid "

    'StrQ = "SELECT " & _
    '       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '       "FROM prtranhead_master " & _
    '               "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "
    '        If mstrEarningHeadsIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ")" & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
    '        End If


    '        'S.SANDEEP [ 08 June 2011 ] -- START

    '        Dim dsList As New DataSet
    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
    '                dCol.DefaultValue = 0
    '                dCol.Caption = dtRow.Item("Tname")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If
    '        '------------------ Add Gross Pay Column
    '        dCol = New DataColumn("TGP")
    '        dCol.Caption = "Gross Pay"
    '        dCol.DefaultValue = 0
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        ''/* Deduction Part */

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '               "WHERE ISNULL(prtranhead_master.isvoid, 0) = 0  "

    '        If mstrDeductionHeadIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") " & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrDeductionHeadIds & ") " & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid "
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- START

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
    '                dCol.Caption = dtRow.Item("Tname")
    '                dCol.DefaultValue = 0
    '                'dCol.DataType = System.Type.GetType("System.Decimal")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If

    '        '------------------ Add Loan Advance Savings
    '        dCol = New DataColumn("Loan")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Loan"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("Advance")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Advance"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("Saving")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Saving"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)



    '        '------------------ Add Total Deduction Column
    '        dCol = New DataColumn("TDD")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Deduction"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        ' Pinkal (26-Mar-2011) -- Start

    '        '------------------ Add Net B/F Column
    '        dCol = New DataColumn("Openingbalance")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net B/F"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)


    '        '------------------ Add Net Pay Column
    '        dCol = New DataColumn("NetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net Pay"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Total Net Pay Column
    '        dCol = New DataColumn("TotNetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Net Pay"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Over/Under Deduction Pay Column
    '        dCol = New DataColumn("OUDPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Over/Under Deduction Pay"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)



    '        'StrQ = "SELECT " & _
    '        '        "	 cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '        '        "	,hremployee_master.employeeunkid AS EmpId " & _
    '        '        "	,cfcommon_period_tran.periodunkid AS PeriodId " & _
    '        '        "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '        '        "FROM  prpayrollprocess_tran " & _
    '        '        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '        "	AND hremployee_master.isactive = 1 " & _
    '        '        "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "	AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "	AND cfcommon_period_tran.periodunkid = @PeriodId "


    'StrQ = "SELECT " & _
    '        "	 cfcommon_period_tran.period_name AS PeriodName " & _
    '        "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '        "	,hremployee_master.employeeunkid AS EmpId " & _
    '        "	,cfcommon_period_tran.periodunkid AS PeriodId " & _
    '        "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '                    "  , ISNULL(prtnaleave_tran.openingbalance,0) as Openingbalance " & _
    '        "FROM  prpayrollprocess_tran " & _
    '        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "	AND cfcommon_period_tran.isactive = 1 " & _
    '                    "	AND cfcommon_period_tran.periodunkid = @PeriodId "
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId  "
    '        End If

    '        'StrQ &= "GROUP BY " & _
    '        '        "	cfcommon_period_tran.period_name " & _
    '        '        "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '        '        "	,hremployee_master.employeeunkid " & _
    '        '        "	,cfcommon_period_tran.periodunkid " & _
    '        '        "  ,ISNULL(hremployee_master.employeecode,'') " & _
    '        '        "ORDER BY  " & _
    '        '        "	hremployee_master.employeeunkid "

    'StrQ &= "GROUP BY " & _
    '        "	cfcommon_period_tran.period_name " & _
    '        "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '        "	,hremployee_master.employeeunkid " & _
    '        "	,cfcommon_period_tran.periodunkid " & _
    '        "  ,ISNULL(hremployee_master.employeecode,'') " & _
    '                    " , ISNULL(prtnaleave_tran.openingbalance,0.00) " & _
    '        "ORDER BY  " & _
    '        "	hremployee_master.employeeunkid "

    '        ' Pinkal (26-Mar-2011) -- End

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsEarning = objDataOperation.ExecQuery(StrQ, "ERNS")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsEarningTotal As New DataSet

    '        objDataOperation.ClearParameters()

    '        ' Pinkal (26-Mar-2011) -- Start

    '        'StrQ = "SELECT " & _
    '        '              "	 hremployee_master.employeeunkid AS EmpId " & _
    '        '              "	,prtranhead_master.tranheadunkid AS TranId " & _
    '        '              "	,SUM(amount) AS Amount " & _
    '        '              "FROM  prpayrollprocess_tran " & _
    '        '              "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '              "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '              "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '              "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '              "	AND hremployee_master.isactive = 1 " & _
    '        '              "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '              "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '              "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '              "	AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'StrQ = "SELECT " & _
    '        '       "	 hremployee_master.employeeunkid AS EmpId " & _
    '        '       "	,prtranhead_master.tranheadunkid AS TranId " & _
    '        '       "	,SUM(amount) AS Amount " & _
    '        '       "  , 0 as Openingbalance " & _
    '        '       "FROM  prpayrollprocess_tran " & _
    '        '       "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '       "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '       "	AND hremployee_master.isactive = 1 " & _
    '        '       "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '       "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '       "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '       "	AND prtnaleave_tran.payperiodunkid = @PeriodId "

    'StrQ = "SELECT " & _
    '       "	 hremployee_master.employeeunkid AS EmpId " & _
    '       "	,prtranhead_master.tranheadunkid AS TranId " & _
    '       "	,SUM(amount) AS Amount " & _
    '       "  , 0 as Openingbalance " & _
    '       "FROM  prpayrollprocess_tran " & _
    '       "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '       "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '       "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= "	AND hremployee_master.isactive = 1 "
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                        "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                        "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '                        "	AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        If mstrEarningHeadsIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ")"
    '        End If
    '        'prtranhead_master

    '        'S.SANDEEP [ 08 June 2011 ] -- START


    '        ' Pinkal (26-Mar-2011) -- End

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If
    '        StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid " & _
    '                        "ORDER BY  " & _
    '                        "hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsEarningTotal = objDataOperation.ExecQuery(StrQ, "ETotal")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsDeductionTotal As New DataSet

    '        objDataOperation.ClearParameters()
    '        StrQ = "SELECT " & _
    '                    "	 EmpId AS EmpId " & _
    '                    "	,TranId AS TranId " & _
    '                    "	,Amount AS Amount " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "	SELECT " & _
    '                    "		 hremployee_master.employeeunkid AS EmpId " & _
    '                    "		,prtranhead_master.tranheadunkid AS TranId " & _
    '                    "		,SUM(amount) AS Amount " & _
    '                    "	FROM  prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "	WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= "		AND hremployee_master.isactive = 1 "
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '                    "		AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                    "	GROUP BY  hremployee_master.employeeunkid " & _
    '                    "	,prtranhead_master.tranheadunkid " & _
    '                    "UNION ALL " & _
    '                    "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                                  ", -1 AS TranId " & _
    '                                  ", SUM(amount) AS Amount " & _
    '                          "FROM      prpayrollprocess_tran " & _
    '                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                    "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                    "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                          "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= "AND hremployee_master.isactive = 1 "
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                    "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                    "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                    "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                    "AND prpayrollprocess_tran.savingtranunkid = -1 " & _
    '                                    "AND lnloan_advance_tran.isloan = 1 " & _
    '                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                          "GROUP BY  hremployee_master.employeeunkid " & _
    '                          "UNION ALL " & _
    '                          "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                                  ", -2 AS TranId " & _
    '                                  ", SUM(amount) AS Amount " & _
    '                          "FROM      prpayrollprocess_tran " & _
    '                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                    "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                    "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                              "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= "AND hremployee_master.isactive = 1 "
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                    "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                    "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                    "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                    "AND prpayrollprocess_tran.savingtranunkid = -1 " & _
    '                                    "AND lnloan_advance_tran.isloan = 0 " & _
    '                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                          "GROUP BY  hremployee_master.employeeunkid " & _
    '                          "UNION ALL " & _
    '                           "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                                  ", -3 AS TranId " & _
    '                                  ", SUM(amount) AS Amount " & _
    '                          "FROM      prpayrollprocess_tran " & _
    '                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                    "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                    "JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
    '                              "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            StrQ &= "AND hremployee_master.isactive = 1 "
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                    "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
    '                                    "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                    "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                    "AND prpayrollprocess_tran.loanadvancetranunkid = -1 " & _
    '                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                          "GROUP BY  hremployee_master.employeeunkid " & _
    '                    ") AS Deduction WHERE 1 = 1 "

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'If mintEmployeeId > 0 Then
    '        '    StrQ &= "	WHERE EmpId = @EmpId "
    '        'End If

    'If mintEmployeeId > 0 Then
    '            StrQ &= "	AND  EmpId = @EmpId "
    'End If

    '        If mstrDeductionHeadIds.Length > 0 Then
    '            StrQ &= " AND TranId IN (" & mstrDeductionHeadIds & ")"
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- START


    '        StrQ &= "ORDER BY EmpId,TranId "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsDeductionTotal = objDataOperation.ExecQuery(StrQ, "Deduction")


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim mdicEarning As New Dictionary(Of Integer, Integer)
    '        For Each drRow As DataRow In dsEarning.Tables("ERNS").Rows
    '            If mdicEarning.ContainsKey(drRow.Item("EmpId")) Then Continue For
    '            mdicEarning.Add(drRow.Item("EmpId"), drRow.Item("EmpId"))

    '            dtTable = New DataView(dsEarningTotal.Tables("ETotal"), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable
    '            dtDeduction = New DataView(dsDeductionTotal.Tables("Deduction"), "EmpId = " & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '            Dim rpt_Row As DataRow
    '            rpt_Row = dtFinalTable.NewRow

    '            rpt_Row.Item("EmpCode") = drRow.Item("Code")
    '            rpt_Row.Item("EmpName") = drRow.Item("EmpName")

    '            ' Pinkal (26-Mar-2011) -- Start

    '            rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency)

    '            ' Pinkal (26-Mar-2011) -- End

    '            For Each dtRow As DataRow In dtTable.Rows
    '                rpt_Row.Item("Column" & dtRow.Item("TranId")) = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '                rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            Next

    '            For Each dtDRow As DataRow In dtDeduction.Rows
    '                If dtDRow.Item("TranId").ToString = "-1" Then
    '                    rpt_Row.Item("Loan") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("TranId").ToString = "-2" Then
    '                    rpt_Row.Item("Advance") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                ElseIf dtDRow.Item("TranId").ToString = "-3" Then
    '                    rpt_Row.Item("Saving") = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                Else
    '                    rpt_Row.Item("Column" & dtDRow.Item("TranId")) = Format(CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '                End If
    '                rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(dtDRow.Item("Amount")), GUI.fmtCurrency)
    '            Next
    '            rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)
    '            'Anjan (29 Dec 2010)-Start
    '            'rpt_Row.Item("NetPay") = Format(cdec(rpt_Row.Item("NetPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)
    '            'Anjan (29 Dec 2010)-End

    '            ' Pinkal (26-Mar-2011) -- Start

    '            rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), GUI.fmtCurrency)

    '            ' Pinkal (26-Mar-2011) -- End


    '            'S.SANDEEP [ 30 May 2011 ] -- START
    '            If CDec(rpt_Row.Item("TotNetPay")) < 0 Then
    '                rpt_Row.Item("OUDPay") = IIf(CDec(rpt_Row.Item("TotNetPay")) < 0, "(" & Format(System.Math.Abs(CDec(rpt_Row.Item("TotNetPay"))), GUI.fmtCurrency) & ")", 0)
    '                rpt_Row.Item("TotNetPay") = Format(CDec(0), GUI.fmtCurrency)
    '            Else
    '                rpt_Row.Item("OUDPay") = Format(CDec(0), GUI.fmtCurrency)
    '            End If
    '            'S.SANDEEP [ 30 May 2011 ] -- END 


    '            'Anjan (29 Dec 2010)-End
    '            dtFinalTable.Rows.Add(rpt_Row)
    '        Next

    '        dtFinalTable.AcceptChanges()


    '        Dim dblTotal As Decimal = 0
    '        Dim m As Integer = 2
    '        Dim n As Integer = 2
    '        dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

    '        While m < dtFinalTable.Columns.Count
    '            For t As Integer = 0 To dtFinalTable.Rows.Count - 1
    '                dblTotal += CDec(dtFinalTable.Rows(t)(m))
    '            Next
    '            dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
    '            dblTotal = 0
    '            m += 1
    '            n += 1
    '        End While

    '        Call FilterTitleAndFilterQuery()

    '        If Export_to_Excel(mstrReportTypeName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
    '            'S.SANDEEP [ 30 May 2011 ] -- START
    '            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
    '            'S.SANDEEP [ 30 May 2011 ] -- END 
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PayrollReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (17 Apr 2012) -- End

    'Public Sub Generate_ByAnalysisEDSpreadsheetReport()
    '    Dim dsEmpPeriodDetail As New DataSet
    '    Dim dsDeduction As New DataSet
    '    Dim dtFinalTable As DataTable
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty
    '    Try

    '        If ConfigParameter._Object._ExportReportPath = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        objDataOperation = New clsDataOperation

    '        dtFinalTable = New DataTable("ByAnalysisEDSpreadsheet")
    '        Dim dCol As DataColumn

    '        'dCol = New DataColumn("PeriodId")
    '        'dCol.DataType = System.Type.GetType("System.Int32")
    '        'dtFinalTable.Columns.Add(dCol)

    '        'dCol = New DataColumn("EmpId")
    '        'dCol.DataType = System.Type.GetType("System.Int32")
    '        'dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("EmpCode")
    '        dCol.Caption = "Code"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("EmpName")
    '        dCol.Caption = "Employee"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'StrQ = "SELECT " & _
    '        '       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '        '       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '        '       "FROM prtranhead_master " & _
    '        '       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '       "AND prtranhead_master.trnheadtype_id = 1 ORDER BY prtranhead_master.tranheadunkid "

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '               "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0 "
    '        If mstrEarningHeadsIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ") " & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            StrQ &= " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- START

    '        Dim dsList As New DataSet
    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

    '                'Pinkal (14-Dec-2012) -- Start
    '                'Enhancement : TRA Changes
    '                dCol.DataType = System.Type.GetType("System.Decimal")
    '                'Pinkal (14-Dec-2012) -- End

    '                dCol.DefaultValue = 0
    '                dCol.Caption = dtRow.Item("Tname")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If
    '        '------------------ Add Gross Pay Column
    '        dCol = New DataColumn("TGP")
    '        dCol.Caption = "Gross Pay"
    '        dCol.DefaultValue = 0

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)




    '        ''/* Deduction Part */

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'StrQ = "SELECT " & _
    '        '       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '        '       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '        '       "FROM prtranhead_master " & _
    '        '       "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '       "AND prtranhead_master.trnheadtype_id IN (2,3) ORDER BY prtranhead_master.tranheadunkid "

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '               "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "
    '        If mstrDeductionHeadIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") " & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrDeductionHeadIds & ") " & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid "
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- START

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
    '                dCol.Caption = dtRow.Item("Tname")
    '                dCol.DefaultValue = 0
    '                'dCol.DataType = System.Type.GetType("System.Decimal")
    '                dtFinalTable.Columns.Add(dCol)
    '            Next
    '        End If

    '        '------------------ Add Loan Advance Savings
    '        dCol = New DataColumn("Loan")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Loan"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("Advance")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Advance"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("Saving")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Saving"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Total Deduction Column
    '        dCol = New DataColumn("TDD")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Deduction"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)

    '        ' Pinkal (26-Mar-2011) -- Start

    '        '------------------ Add Net B/F Column
    '        dCol = New DataColumn("Openingbalance")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net B/F"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)



    '        '------------------ Add Net Pay Column
    '        dCol = New DataColumn("NetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net Pay"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)


    '        '------------------ Add Total Net Pay Column
    '        dCol = New DataColumn("TotNetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Net Pay"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)

    '        ' Pinkal (26-Mar-2011) -- End

    '        '------------------ Add Over/Under Deduction Pay Column
    '        dCol = New DataColumn("OUDPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Over/Under Deduction Pay"
    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End
    '        dtFinalTable.Columns.Add(dCol)


    '        dCol = New DataColumn("isGroup")
    '        dCol.DataType = System.Type.GetType("System.Boolean")
    '        dCol.AllowDBNull = True
    '        dtFinalTable.Columns.Add(dCol)







    '        ''/* Employee-Period Details */
    '        StrQ = "SELECT " & _
    '                    "	 cfcommon_period_tran.period_name AS PeriodName " & _
    '                    "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '                    "	,hremployee_master.employeeunkid AS EmpId " & _
    '                    "	,cfcommon_period_tran.periodunkid AS PeriodId " & _
    '                    "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '                    "   , hrdepartment_master.departmentunkid AS DeptId " & _
    '                    "   , hrdepartment_master.name AS DeptName " & _
    '                    "FROM  prpayrollprocess_tran " & _
    '                    "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                    "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End


    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "	AND cfcommon_period_tran.isactive = 1 " & _
    '                    "   AND hrdepartment_master.isactive = 1 " & _
    '                    "	AND cfcommon_period_tran.periodunkid = @PeriodId "
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId  "
    '        End If
    '        StrQ &= "GROUP BY " & _
    '                    "	cfcommon_period_tran.period_name " & _
    '                    "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '                    "	,hremployee_master.employeeunkid " & _
    '                    "	,cfcommon_period_tran.periodunkid " & _
    '                    "  ,ISNULL(hremployee_master.employeecode,'') " & _
    '                    "   , hrdepartment_master.departmentunkid  " & _
    '                    "   , hrdepartment_master.name " & _
    '                    "ORDER BY  " & _
    '                    "	hremployee_master.employeeunkid "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 


    '        dsEmpPeriodDetail = objDataOperation.ExecQuery(StrQ, "EmpPeriod")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If





    '        ''/* Earning Tran Amount Details */
    '        Dim dsEarningTran As New DataSet

    '        objDataOperation.ClearParameters()

    '        ' Pinkal (26-Mar-2011) -- Start

    '        'StrQ = "SELECT " & _
    '        '               "	 hremployee_master.employeeunkid AS EmpId " & _
    '        '               "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '        '               "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '        '               "	,prtranhead_master.tranheadunkid AS TranId " & _
    '        '               "	,SUM(amount) AS Amount " & _
    '        '               "   , hremployee_master.departmentunkid AS DeptId " & _
    '        '               " FROM  prpayrollprocess_tran " & _
    '        '               "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '               "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '               "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '               "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '               "	AND hremployee_master.isactive = 1 " & _
    '        '               "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '               "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '               "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '        '               "	AND prtnaleave_tran.payperiodunkid = @PeriodId "


    '        StrQ = "SELECT " & _
    '                        "	 hremployee_master.employeeunkid AS EmpId " & _
    '                        "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '                        "   ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '                        "	,prtranhead_master.tranheadunkid AS TranId " & _
    '                        "	,SUM(amount) AS Amount " & _
    '                        "   , hremployee_master.departmentunkid AS DeptId " & _
    '                         "   ,ISNULL(prtnaleave_tran.Openingbalance,0.00) as Openingbalance   " & _
    '                        "FROM  prpayrollprocess_tran " & _
    '                        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        "	JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        "WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "



    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End


    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                        "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                        "	AND prtranhead_master.trnheadtype_id = 1 " & _
    '                        "	AND prtnaleave_tran.payperiodunkid = @PeriodId "

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        If mstrEarningHeadsIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ")"
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- START


    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
    '        End If

    '        'StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '        '                ",prtranhead_master.tranheadunkid " & _
    '        '                ",hremployee_master.departmentunkid " & _
    '        '                ",(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '        '                ",ISNULL(hremployee_master.employeecode,'') " & _
    '        '                "ORDER BY  " & _
    '        '                "hremployee_master.employeeunkid " & _
    '        '                ",prtranhead_master.tranheadunkid "

    '        StrQ &= "GROUP BY  hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid " & _
    '                        ",hremployee_master.departmentunkid " & _
    '                        ",(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '                        ",ISNULL(hremployee_master.employeecode,'') " & _
    '                       ",ISNULL(prtnaleave_tran.Openingbalance,0.00) " & _
    '                        "ORDER BY  " & _
    '                        "hremployee_master.employeeunkid " & _
    '                        ",prtranhead_master.tranheadunkid "

    '        ' Pinkal (26-Mar-2011) -- End

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        dsEarningTran = objDataOperation.ExecQuery(StrQ, "Earning")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If






    '        ''/* Deduction Tran Amount With Sum of Loan/Advance/Saving Details */
    '        Dim dsDeductionTran As New DataSet

    '        objDataOperation.ClearParameters()
    '        StrQ = "SELECT " & _
    '                    "	 EmpId AS EmpId " & _
    '                    "	,TranId AS TranId " & _
    '                    "	,Amount AS Amount " & _
    '                    "   , DeptId AS DeptId " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "	SELECT " & _
    '                    "		 hremployee_master.employeeunkid AS EmpId " & _
    '                    "		,prtranhead_master.tranheadunkid AS TranId " & _
    '                    "		,SUM(amount) AS Amount " & _
    '                    "       , hremployee_master.departmentunkid AS DeptId " & _
    '                    "	    ,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,''))AS EmpName " & _
    '                    "       ,ISNULL(hremployee_master.employeecode,'') As Code " & _
    '                    "	FROM  prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "		JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "	WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "	AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '              " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prpayrollprocess_tran.add_deduct IN (2) " & _
    '                    "		AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                    "	GROUP BY  hremployee_master.employeeunkid " & _
    '                    "	,prtranhead_master.tranheadunkid " & _
    '                    "   , hremployee_master.departmentunkid " & _
    '                    "	,(ISNULL(hremployee_master.firstname,'')+ ' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'')) " & _
    '                    "   ,ISNULL(hremployee_master.employeecode,'') " & _
    '                    "UNION ALL " & _
    '                   "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                              ", -1 AS TranId " & _
    '                              ", SUM(amount) AS Amount " & _
    '                              ", hremployee_master.departmentunkid AS DeptId " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) AS EmpName " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') AS Code " & _
    '                      "FROM      prpayrollprocess_tran " & _
    '                                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                      "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                                "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                "AND prpayrollprocess_tran.savingtranunkid = -1 " & _
    '                                "AND lnloan_advance_tran.isloan = 1 " & _
    '                      "GROUP BY  hremployee_master.employeeunkid " & _
    '                              ", hremployee_master.departmentunkid " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') " & _
    '                      "UNION ALL " & _
    '                      "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                              ", -2 AS TranId " & _
    '                              ", SUM(amount) AS Amount " & _
    '                              ", hremployee_master.departmentunkid AS DeptId " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) AS EmpName " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') AS Code " & _
    '                      "FROM      prpayrollprocess_tran " & _
    '                                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                "JOIN lnloan_advance_tran ON prpayrollprocess_tran.loanadvancetranunkid = lnloan_advance_tran.loanadvancetranunkid " & _
    '                      "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If

    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '                                "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                                "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                "AND prpayrollprocess_tran.savingtranunkid = -1 " & _
    '                                "AND lnloan_advance_tran.isloan = 0 " & _
    '                      "GROUP BY  hremployee_master.employeeunkid " & _
    '                              ", hremployee_master.departmentunkid " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') " & _
    '                      "UNION ALL " & _
    '                      "SELECT    hremployee_master.employeeunkid AS EmpId " & _
    '                              ", -3 AS TranId " & _
    '                              ", SUM(amount) AS Amount " & _
    '                              ", hremployee_master.departmentunkid AS DeptId " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) AS EmpName " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') AS Code " & _
    '                      "FROM      prpayrollprocess_tran " & _
    '                                "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                "JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid " & _
    '                      "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (24-Jun-2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Pinkal (24-Jun-2011) -- End

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = @BranchId "
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
    '                                "AND prpayrollprocess_tran.add_deduct IN ( 2 ) " & _
    '                                "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
    '                                "AND prpayrollprocess_tran.tranheadunkid = -1 " & _
    '                                "AND prpayrollprocess_tran.loanadvancetranunkid = -1 " & _
    '                      "GROUP BY  hremployee_master.employeeunkid " & _
    '                              ", hremployee_master.departmentunkid " & _
    '                              ", ( ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                                  "+ ISNULL(hremployee_master.surname, '') ) " & _
    '                              ", ISNULL(hremployee_master.employeecode, '') " & _
    '                    ") AS Deduction WHERE 1 = 1 "
    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : 400+ TRANS. HEADS, SO SELECTION IS GIVEN
    '        'If mintEmployeeId > 0 Then
    '        '    StrQ &= "	WHERE EmpId = @EmpId "
    '        'End If

    '        If mintEmployeeId > 0 Then
    '            StrQ &= "	AND  EmpId = @EmpId "
    '        End If

    '        If mstrDeductionHeadIds.Length > 0 Then
    '            StrQ &= " AND TranId IN (" & mstrDeductionHeadIds & ")"
    '        End If
    '        'S.SANDEEP [ 08 June 2011 ] -- STA
    '        StrQ &= "ORDER BY EmpId,TranId "

    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        If mintEmployeeId > 0 Then
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (17 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If mblnIsActive = False Then
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '        End If
    '        'Sohail (17 Apr 2012) -- End

    '        dsDeductionTran = objDataOperation.ExecQuery(StrQ, "Deduction")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtEarningDeptTable As DataTable
    '        Dim dtDeductionDeptTable As DataTable
    '        Dim blnIsGroup As Boolean = False
    '        Dim mdicDept As New Dictionary(Of Integer, Integer)
    '        Dim IsChangeDept As Boolean = False
    '        Dim rpt_row As DataRow = Nothing
    '        Dim mdicEmp As New Dictionary(Of Integer, Integer)
    '        Dim dtEarEmpTable As DataTable
    '        Dim dtDedEmpTable As DataTable


    '        'S.SANDEEP [ 30 May 2011 ] -- START
    '        For Each dtRow As DataRow In dsEmpPeriodDetail.Tables("EmpPeriod").Rows
    '            rpt_row = dtFinalTable.NewRow
    '            If mdicDept.ContainsKey(dtRow.Item("DeptId")) Then Continue For
    '            mdicDept.Add(dtRow.Item("DeptId"), dtRow.Item("DeptId"))

    '            dtEarningDeptTable = New DataView(dsEarningTran.Tables("Earning"), "DeptId =" & dtRow.Item("DeptId"), "", DataViewRowState.CurrentRows).ToTable
    '            dtDeductionDeptTable = New DataView(dsDeductionTran.Tables("Deduction"), "DeptId =" & dtRow.Item("DeptId"), "", DataViewRowState.CurrentRows).ToTable

    '            IsChangeDept = True

    '            If IsChangeDept = True Then
    '                rpt_row.Item("EmpCode") = dtRow.Item("DeptName")
    '                blnIsGroup = True
    '                rpt_row.Item("isGroup") = True
    '                dtFinalTable.Rows.Add(rpt_row)
    '            End If

    '            For Each drRow As DataRow In dtEarningDeptTable.Rows
    '                rpt_row = dtFinalTable.NewRow

    '                If mdicEmp.ContainsKey(drRow.Item("EmpId")) Then Continue For
    '                mdicEmp.Add(drRow.Item("EmpId"), drRow.Item("EmpId"))

    '                rpt_row.Item("isGroup") = False

    '                dtEarEmpTable = New DataView(dtEarningDeptTable, "EmpId =" & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '                rpt_row.Item("EmpCode") = Space(10) & drRow.Item("Code")
    '                rpt_row.Item("EmpName") = drRow.Item("EmpName")

    '                For i As Integer = 0 To dtEarEmpTable.Rows.Count - 1
    '                    rpt_row.Item("Column" & dtEarEmpTable.Rows(i).Item("TranId")) = Format(CDec(dtEarEmpTable.Rows(i).Item("Amount")), GUI.fmtCurrency)
    '                    rpt_row.Item("TGP") = Format(CDec(rpt_row.Item("TGP")) + CDec(dtEarEmpTable.Rows(i).Item("Amount")), GUI.fmtCurrency)
    '                Next

    '                dtDedEmpTable = New DataView(dtDeductionDeptTable, "EmpId =" & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '                For j As Integer = 0 To dtDedEmpTable.Rows.Count - 1
    '                    If dtDedEmpTable.Rows(j).Item("TranId").ToString = "-1" Then
    '                        rpt_row.Item("Loan") = Format(CDec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '                    ElseIf dtDedEmpTable.Rows(j).Item("TranId").ToString = "-2" Then
    '                        rpt_row.Item("Advance") = Format(CDec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '                    ElseIf dtDedEmpTable.Rows(j).Item("TranId").ToString = "-3" Then
    '                        rpt_row.Item("Saving") = Format(CDec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '                    Else
    '                        rpt_row.Item("Column" & dtDedEmpTable.Rows(j).Item("TranId")) = Format(CDec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '                    End If
    '                    rpt_row.Item("TDD") = Format(CDec(rpt_row.Item("TDD")) + CDec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '                Next
    '                rpt_row.Item("NetPay") = Format(CDec(rpt_row("TGP")) - CDec(rpt_row("TDD")), GUI.fmtCurrency)

    '                'Anjan (29 Dec 2010)-Start
    '                'rpt_Row.Item("NetPay") = Format(cdec(rpt_row.Item("NetPay"), GUI.fmtCurrency)
    '                rpt_row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)

    '                'Anjan (29 Dec 2010)-End


    '                ' Pinkal (26-Mar-2011) -- Start
    '                rpt_row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency)
    '                rpt_row.Item("TotNetPay") = Format(CDec(rpt_row.Item("NetPay")) + CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency)

    '                ' Pinkal (26-Mar-2011) -- End

    '                If CDec(rpt_row.Item("TotNetPay")) < 0 Then

    '                    'Pinkal (14-Dec-2012) -- Start
    '                    'Enhancement : TRA Changes
    '                    'rpt_row.Item("OUDPay") = IIf(CDec(rpt_row.Item("TotNetPay")) < 0, "(" & Format(System.Math.Abs(CDec(rpt_row.Item("TotNetPay"))), GUI.fmtCurrency) & ")", 0)
    '                    rpt_row.Item("OUDPay") = IIf(CDec(rpt_row.Item("TotNetPay")) < 0, Format(CDec(rpt_row.Item("TotNetPay")), GUI.fmtCurrency), 0)
    '                    'Pinkal (14-Dec-2012) -- End


    '                    rpt_row.Item("TotNetPay") = Format(CDec(0), GUI.fmtCurrency)
    '                Else
    '                    rpt_row.Item("OUDPay") = Format(CDec(0), GUI.fmtCurrency)
    '                End If
    '                dtFinalTable.Rows.Add(rpt_row)
    '            Next
    '        Next
    '        'S.SANDEEP [ 30 May 2011 ] -- END 






    '        'For Each dtRow As DataRow In dsEmpPeriodDetail.Tables("EmpPeriod").Rows
    '        '    If mdicDept.ContainsKey(dtRow.Item("DeptId")) Then Continue For
    '        '    mdicDept.Add(dtRow.Item("DeptId"), dtRow.Item("DeptId"))

    '        '    dtEarningDeptTable = New DataView(dsEarningTran.Tables("Earning"), "DeptId =" & dtRow.Item("DeptId"), "", DataViewRowState.CurrentRows).ToTable
    '        '    dtDeductionDeptTable = New DataView(dsDeductionTran.Tables("Deduction"), "DeptId =" & dtRow.Item("DeptId"), "", DataViewRowState.CurrentRows).ToTable

    '        '    IsChangeDept = True

    '        '    Dim rpt_row As DataRow = Nothing

    '        '    Dim mdicEmp As New Dictionary(Of Integer, Integer)
    '        '    Dim dtEarEmpTable As DataTable
    '        '    Dim dtDedEmpTable As DataTable

    '        '    For Each drRow As DataRow In dtEarningDeptTable.Rows
    '        '        rpt_row = dtFinalTable.NewRow

    '        '        If blnIsGroup = False Then
    '        '            rpt_row.Item("EmpCode") = dtRow.Item("DeptName")
    '        '            blnIsGroup = True
    '        '            rpt_row.Item("isGroup") = True
    '        '        Else
    '        '            rpt_row.Item("isGroup") = False

    '        '            If mdicEmp.ContainsKey(drRow.Item("EmpId")) Then Continue For
    '        '            mdicEmp.Add(drRow.Item("EmpId"), drRow.Item("EmpId"))

    '        '            dtEarEmpTable = New DataView(dtEarningDeptTable, "EmpId =" & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '        '            rpt_row.Item("EmpCode") = Space(10) & drRow.Item("Code")
    '        '            rpt_row.Item("EmpName") = drRow.Item("EmpName")

    '        '            For i As Integer = 0 To dtEarEmpTable.Rows.Count - 1
    '        '                rpt_row.Item("Column" & dtEarEmpTable.Rows(i).Item("TranId")) = Format(cdec(dtEarEmpTable.Rows(i).Item("Amount")), GUI.fmtCurrency)
    '        '                rpt_row.Item("TGP") = Format(cdec(rpt_row.Item("TGP")) + cdec(dtEarEmpTable.Rows(i).Item("Amount")), GUI.fmtCurrency)
    '        '            Next

    '        '            dtDedEmpTable = New DataView(dtDeductionDeptTable, "EmpId =" & drRow.Item("EmpId"), "", DataViewRowState.CurrentRows).ToTable

    '        '            For j As Integer = 0 To dtDedEmpTable.Rows.Count - 1
    '        '                If dtDedEmpTable.Rows(j).Item("TranId").ToString = "-1" Then
    '        '                    rpt_row.Item("Loan") = Format(cdec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '        '                ElseIf dtDedEmpTable.Rows(j).Item("TranId").ToString = "-2" Then
    '        '                    rpt_row.Item("Advance") = Format(cdec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '        '                ElseIf dtDedEmpTable.Rows(j).Item("TranId").ToString = "-3" Then
    '        '                    rpt_row.Item("Saving") = Format(cdec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '        '                Else
    '        '                    rpt_row.Item("Column" & dtDedEmpTable.Rows(j).Item("TranId")) = Format(cdec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '        '                End If
    '        '                rpt_row.Item("TDD") = Format(cdec(rpt_row.Item("TDD")) + cdec(dtDedEmpTable.Rows(j).Item("Amount")), GUI.fmtCurrency)
    '        '            Next
    '        '            rpt_row.Item("NetPay") = Format(cdec(rpt_row("TGP")) - cdec(rpt_row("TDD")), GUI.fmtCurrency)

    '        '            'Anjan (29 Dec 2010)-Start
    '        '            'rpt_Row.Item("NetPay") = Format(cdec(rpt_row.Item("NetPay"), GUI.fmtCurrency)
    '        '            rpt_row.Item("NetPay") = Format(Rounding.BRound(cdec(rpt_row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)

    '        '            'Anjan (29 Dec 2010)-End


    '        '            ' Pinkal (26-Mar-2011) -- Start
    '        '            rpt_row.Item("Openingbalance") = Format(cdec(drRow.Item("Openingbalance")), GUI.fmtCurrency)
    '        '            rpt_row.Item("TotNetPay") = Format(cdec(rpt_row.Item("NetPay")) + cdec(drRow.Item("Openingbalance")), GUI.fmtCurrency)

    '        '            ' Pinkal (26-Mar-2011) -- End

    '        '            'S.SANDEEP [ 30 May 2011 ] -- START
    '        '            If cdec(rpt_row.Item("TotNetPay")) < 0 Then
    '        '                rpt_row.Item("OUDPay") = IIf(cdec(rpt_row.Item("TotNetPay")) < 0, "(" & Format(System.Math.Abs(cdec(rpt_row.Item("TotNetPay"))), GUI.fmtCurrency) & ")", 0)
    '        '                rpt_row.Item("TotNetPay") = Format(cdec(0), GUI.fmtCurrency)
    '        '            Else
    '        '                rpt_row.Item("OUDPay") = Format(cdec(0), GUI.fmtCurrency)
    '        '            End If
    '        '            'S.SANDEEP [ 30 May 2011 ] -- END 


    '        '        End If

    '        '        dtFinalTable.Rows.Add(rpt_row)
    '        '    Next

    '        '    blnIsGroup = False
    '        'Next


    '        Dim dblTotal As Decimal = 0
    '        Dim m As Integer = 2
    '        Dim n As Integer = 2
    '        dblColTot = New Decimal(dtFinalTable.Columns.Count) {}

    '        While m < dtFinalTable.Columns.Count - 1
    '            For t As Integer = 0 To dtFinalTable.Rows.Count - 1
    '                dblTotal += CDec(dtFinalTable.Rows(t)(m))
    '            Next
    '            dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
    '            dblTotal = 0
    '            m += 1
    '            n += 1
    '        End While

    '        Call FilterTitleAndFilterQuery()

    '        If Export_to_Excel(mstrReportTypeName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
    '            'Sandeep [ 09 MARCH 2011 ] -- Start
    '            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
    '            'Sandeep [ 09 MARCH 2011 ] -- End 
    '        End If



    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_ByAnalysisEDSpreadsheetReport; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub

    'Sohail (09 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Public Sub Generate_EDSpreadsheetReport_New(ByVal xDatabaseName As String _
                                                , ByVal xUserUnkid As Integer _
                                                , ByVal xYearUnkid As Integer _
                                                , ByVal xCompanyUnkid As Integer _
                                                , ByVal xPeriodStart As Date _
                                                , ByVal xPeriodEnd As Date _
                                                , ByVal xUserModeSetting As String _
                                                , ByVal xOnlyApproved As Boolean _
                                                , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                , ByVal blnApplyUserAccessFilter As Boolean _
                                                , ByVal strFmtCurrency As String _
                                                , ByVal intBase_CurrencyId As Integer _
                                                , ByVal xExportReportPath As String _
                                                , ByVal xOpenReportAfterExport As Boolean _
                                                , ByVal dblRoundOff_Type As Double _
                                                )
        'Sohail (22 Sep 2017) - [dblRoundOff_Type]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, intBase_CurrencyId, xExportReportPath, xOpenReportAfterExport]

        Dim dsList As DataSet
        Dim dsData As New DataSet
        Dim dtFinalTable As DataTable
        Dim exForce As Exception
        Dim StrQ As String = String.Empty

        Dim objActivity As New clsActivity_Master
        'Sohail (21 Nov 2014) -- Start
        'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
        Dim mdicEmpBank As New Dictionary(Of String, String)
        Dim mdicEmpAccount As New Dictionary(Of String, String)
        Dim mdicEmpPayment As New Dictionary(Of String, String)
        Dim dsPaymetDetail As DataSet
        Dim strCurrKey As String = ""
        'Sohail (21 Nov 2014) -- End
        Dim objExpense As New clsExpense_Master 'Sohail (12 Nov 2014)
        Try
            'Nilay (18-Mar-2015) -- Start
            'Issue : Aga Khan wanted to export reports on local computer and not on cloud server.
            'If ConfigParameter._Object._ExportReportPath = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Nilay (18-Mar-2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBase_CurrencyId
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'Sohail (20 Oct 2014) -- Start
            'Enhancement - New Report Termination Package Report for FINCA DRC.

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter.Trim = "" Then
            '    mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (20 Oct 2014) -- End


            StrQ = "SELECT  prtnaleave_tran.payperiodunkid AS PeriodId " & _
                          ", period_name AS PeriodName " & _
                          ", prpayrollprocess_tran.employeeunkid AS EmpId " & _
                          ", hremployee_master.employeecode AS Code " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS EmpName " & _
                          ", CASE WHEN prpayrollprocess_tran.tranheadunkid > 0 THEN prtranhead_master.trnheadtype_id WHEN prpayrollprocess_tran.loanadvancetranunkid > 0 THEN 2 WHEN prpayrollprocess_tran.savingtranunkid > 0 THEN 2 WHEN prpayrollprocess_tran.activityunkid > 0 THEN practivity_master.trnheadtype_id WHEN ISNULL(cmexpense_master.expenseunkid, -1) > 0 THEN cmexpense_master.trnheadtype_id WHEN ISNULL(crretireexpense.expenseunkid, -1) > 0 THEN CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 1 ELSE 2 END END AS trnheadtype_id " & _
                          ", prpayrollprocess_tran.tranheadunkid AS TranId " & _
                          ", prpayrollprocess_tran.loanadvancetranunkid " & _
                          ", prpayrollprocess_tran.savingtranunkid " & _
                          ", prpayrollprocess_tran.activityunkid " & _
                          ", CASE WHEN ISNULL(cmexpense_master.expenseunkid, -1) > 0 THEN ISNULL(cmexpense_master.expenseunkid, -1) WHEN ISNULL(crretireexpense.expenseunkid, -1) > 0 THEN ISNULL(crretireexpense.expenseunkid, -1) END AS expenseunkid " & _
                          ", CASE ISNULL(prtranhead_master.trnheadtype_id, 0) " & _
                              "WHEN " & enTranHeadType.Informational & " " & _
                              "THEN CASE ISNULL(prtranhead_master.ismonetary, 0) " & _
                                     "WHEN 1 " & _
                                     "THEN ( SUM(CAST(prpayrollprocess_tran.Amount AS DECIMAL(36, " & decDecimalPlaces & "))) * " & mdecEx_Rate & " ) " & _
                                     "ELSE SUM(prpayrollprocess_tran.Amount) * 1.0000 " & _
                                   "END " & _
                              "ELSE ( SUM(CAST(prpayrollprocess_tran.Amount AS DECIMAL(36, " & decDecimalPlaces & "))) *" & mdecEx_Rate & " ) " & _
                            "END AS Amount " & _
                          ", ( SUM(CAST(openingbalance AS DECIMAL(36, " & decDecimalPlaces & "))) * " & mdecEx_Rate & " ) AS Openingbalance " & _
                          ", CASE WHEN prpayrollprocess_tran.tranheadunkid > 0 THEN CASE prtranhead_master.trnheadtype_id WHEN 1 THEN 1 WHEN 2 THEN 2 WHEN 3 THEN 2 WHEN 4 THEN 3 WHEN 5 THEN 4 END WHEN prpayrollprocess_tran.loanadvancetranunkid > 0 THEN CASE lnloan_advance_tran.isloan WHEN 1 THEN 5 ELSE 6 END WHEN prpayrollprocess_tran.savingtranunkid > 0 THEN 7 WHEN prpayrollprocess_tran.activityunkid > 0 THEN 8 WHEN ISNULL(cmexpense_master.expenseunkid, -1) > 0 OR ISNULL(crretireexpense.expenseunkid, -1) > 0 THEN 9 END AS GroupID "
            'Sohail (09 Jun 2021) - [crretireexpense]
            '             'Sohail (23 Jun 2015) - Issue: Conversion from type DBNull to type Integer is not valid. -> [WHEN ISNULL(cmexpense_master.expenseunkid, -1) > 0 THEN 9]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
                If mintViewIndex = enAnalysisReport.CostCenter Then
                    StrQ &= ", prcostcenter_master.costcentercode AS GCode "
                End If
            Else
                StrQ &= ", '' AS GName "
            End If


            StrQ &= "FROM    prpayrollprocess_tran " & _
                            "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                            "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                           "AND ISNULL(prpayrollprocess_tran.tranheadunkid, 0) > 0 " & _
                            "LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                                                             "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "LEFT JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnloan_advance_tran.loanschemeunkid " & _
                                                              "AND lnloan_scheme_master.isactive = 1 " & _
                            "LEFT JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
                                                       "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
                            "LEFT JOIN svsavingscheme_master ON svsavingscheme_master.savingschemeunkid = svsaving_tran.savingschemeunkid " & _
                                                               "AND ISNULL(svsavingscheme_master.isvoid, 0) = 0 " & _
                            "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayrollprocess_tran.activityunkid " & _
                                                           "AND practivity_master.isactive = 1 " & _
                            "LEFT JOIN cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                            "LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                            "LEFT JOIN cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                            "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                            "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                            "LEFT JOIN cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "           LEFT JOIN " & _
                              "( " & _
                              "    SELECT " & _
                              "         stationunkid " & _
                              "        ,deptgroupunkid " & _
                              "        ,departmentunkid " & _
                              "        ,sectiongroupunkid " & _
                              "        ,sectionunkid " & _
                              "        ,unitgroupunkid " & _
                              "        ,unitunkid " & _
                              "        ,teamunkid " & _
                              "        ,classgroupunkid " & _
                              "        ,classunkid " & _
                              "        ,employeeunkid " & _
                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                              "    FROM hremployee_transfer_tran " & _
                              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                              ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(lnloan_scheme_master.isactive, 1) = 1 " & _
                            "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(svsavingscheme_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(practivity_master.isactive, 1) = 1 " & _
                            "AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                            "AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                            "AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                            "AND cfcommon_period_tran.isactive = 1 " & _
                            "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                            "AND prtnaleave_tran.payperiodunkid = @PeriodId "
            'Sohail (09 Jun 2021) - [crretireexpense]
            'Sohail (12 Nov 2014) - [cmclaim_process_tran]
            '"AND ISNULL(prtranhead_master.calctype_id, 0) <> " & enCalcType.ROUNDING_ADJUSTMENT & " " & _

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            Dim strHeadIDs As String = ""
            Dim strHeadTypeIDs As String = ""
            If mstrEarningHeadsIds.Length > 0 Then
                strHeadIDs = "," & mstrEarningHeadsIds
            Else
                If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
                    strHeadTypeIDs = "," & enTranHeadType.EarningForEmployees
                End If
            End If

            If mstrDeductionHeadIds.Length > 0 Then
                strHeadIDs &= "," & mstrDeductionHeadIds
            Else
                If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
                    strHeadTypeIDs &= "," & enTranHeadType.DeductionForEmployee & " ," & enTranHeadType.EmployeesStatutoryDeductions
                End If
            End If

            If mblnShowLoan = False Then
                StrQ &= " AND ISNULL(lnloan_advance_tran.isloan, 0) <> 1 "
            End If
            If mblnShowAdvance = False Then
                StrQ &= " AND ISNULL(lnloan_advance_tran.isloan, 1) <> 0 "
            End If
            If mblnShowSavings = False Then
                StrQ &= " AND prpayrollprocess_tran.savingtranunkid <= 0 "
            End If


            If mstrNonEarningDeductionTranHeadIds.Length > 0 Then
                strHeadIDs &= "," & mstrNonEarningDeductionTranHeadIds
            Else
                'strHeadTypeIDs &= "," & enViewPayroll_Group.EmployerContribution & "," & enViewPayroll_Group.Informational
            End If
            If strHeadIDs.Length > 0 Then
                strHeadIDs = strHeadIDs.Substring(1)
            End If
            If strHeadTypeIDs.Length > 0 Then
                strHeadTypeIDs = strHeadTypeIDs.Substring(1)
            End If

            Dim strNonLoanAdvanceGroup As String = "" & enViewPayroll_Group.Earning & "," & enViewPayroll_Group.Deduction & "," & enViewPayroll_Group.EmployerContribution & "," & enViewPayroll_Group.Informational & ""
            If strHeadIDs.Length > 0 AndAlso strHeadTypeIDs.Length <= 0 Then
                StrQ &= "AND ( prtranhead_master.trnheadtype_id IS NULL " & _
                              "OR ( ISNULL(prtranhead_master.tranheadunkid, 0) IN ( " & strHeadIDs & " ) " & _
                                 ") " & _
                            ") "

            ElseIf strHeadIDs.Length > 0 AndAlso strHeadTypeIDs.Length > 0 Then
                StrQ &= "AND ( prtranhead_master.trnheadtype_id IS NULL " & _
                              "OR ( ISNULL(prtranhead_master.trnheadtype_id, 0) IN ( " & strHeadTypeIDs & " ) " & _
                                   "AND ISNULL(prtranhead_master.tranheadunkid, 0) IN ( " & strHeadIDs & " ) " & _
                                 ") " & _
                            ") "
            ElseIf strHeadIDs.Length <= 0 AndAlso strHeadTypeIDs.Length > 0 Then
                StrQ &= "AND ( prtranhead_master.trnheadtype_id IS NULL " & _
                              "OR ( ISNULL(prtranhead_master.trnheadtype_id, 0) IN ( " & strHeadTypeIDs & " ) " & _
                                 ") " & _
                            ") "
            End If

            If mintBranchId > 0 Then
                'StrQ &= "AND vwPayroll.BranchId = @BranchId "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= "AND hremployee_master.stationunkid = @BranchId "
                StrQ &= "AND T.stationunkid = @BranchId "
                'Sohail (21 Aug 2015) -- End
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
            End If

            'Sohail (21 Nov 2014) -- Start
            'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter.Length > 0 Then
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (21 Nov 2014) -- End

            If mintEmployeeId > 0 Then
                StrQ &= "AND prpayrollprocess_tran.employeeunkid = @EmpId  "
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            End If

            StrQ &= "GROUP BY prtnaleave_tran.payperiodunkid " & _
                          ", period_name " & _
                          ", prpayrollprocess_tran.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname " & _
                          ", prpayrollprocess_tran.tranheadunkid " & _
                          ", openingbalance " & _
                          ", prtranhead_master.trnheadtype_id " & _
                          ", practivity_master.trnheadtype_id " & _
                          ", cmexpense_master.trnheadtype_id " & _
                          ", prtranhead_master.ismonetary " & _
                          ", lnloan_advance_tran.isloan " & _
                          ", prpayrollprocess_tran.loanadvancetranunkid " & _
                          ", prpayrollprocess_tran.savingtranunkid " & _
                          ", prpayrollprocess_tran.activityunkid " & _
                          ", ISNULL(cmexpense_master.expenseunkid, -1) " & _
                          ", prpayrollprocess_tran.add_deduct " & _
                          ", ISNULL(crretireexpense.expenseunkid, -1) "
            'Sohail (09 Jun 2021) - [crretireexpense]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "") & " "
                If mintViewIndex = enAnalysisReport.CostCenter Then
                    StrQ &= ", prcostcenter_master.costcentercode "
                End If
            End If
            If mblnIgnorezeroHeads = True Then
                StrQ &= " HAVING SUM(prpayrollprocess_tran.amount) <> 0 "
            End If

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", " & Me.OrderByQuery & ", prpayrollprocess_tran.tranheadunkid "
                Else
                    StrQ &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", hremployee_master.employeecode, vwPayroll.tranheadunkid "
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY " & Me.OrderByQuery & ", prpayrollprocess_tran.tranheadunkid "
                Else
                    StrQ &= "ORDER BY hremployee_master.employeecode, prpayrollprocess_tran.tranheadunkid "
                End If
            End If


            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsData = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            dtFinalTable = New DataTable("EDSpreadsheet")
            Dim dCol As DataColumn
            mintTotEarningCols = 0
            mintTotDeductionCols = 0
            mintTotNonEarningDeductionCols = 0

            dCol = New DataColumn("EmpId")
            dCol.Caption = "EmpId"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            If mintViewIndex > 0 AndAlso mintViewIndex = enAnalysisReport.CostCenter Then
                dCol = New DataColumn("GCode")
                dCol.Caption = "Cost Center Code"
                dCol.DataType = System.Type.GetType("System.String")
                dCol.DefaultValue = ""
                dtFinalTable.Columns.Add(dCol)
            End If

            dCol = New DataColumn("EmpCode")
            dCol.Caption = "Code"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpName")
            dCol.Caption = "Employee"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       "FROM prtranhead_master " & _
                   "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "
            If mstrEarningHeadsIds.Length > 0 Then
                StrQ &= " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                        " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ")" & _
                        " ORDER BY prtranhead_master.tranheadunkid "
            Else
                If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
                    StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
                Else
                    StrQ &= "AND prtranhead_master.trnheadtype_id = -999 ORDER BY prtranhead_master.tranheadunkid "
                End If
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dRow() As DataRow = Nothing
            Dim blnAddCol As Boolean = False
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    If mblnIgnorezeroHeads = True Then
                        dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
                        If dRow.Length > 0 Then
                            blnAddCol = True
                        End If
                    Else
                        blnAddCol = True
                    End If
                    If blnAddCol = True Then
                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dtRow.Item("Tname")
                        dtFinalTable.Columns.Add(dCol)
                        mintTotEarningCols += 1
                    End If
                    blnAddCol = False
                Next
            End If



            If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
                'dsList = objActivity.getComboList("Activity", False, , enTranHeadType.EarningForEmployees)
                'If dsList.Tables("Activity").Rows.Count > 0 Then
                '    For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                '        dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                '        dCol.DataType = System.Type.GetType("System.Decimal")
                '        dCol.DefaultValue = 0
                '        dCol.Caption = dtRow.Item("name")
                '        dtFinalTable.Columns.Add(dCol)
                '        mintTotEarningCols += 1
                '    Next
                'End If

                'Sohail (12 Nov 2014) -- Start
                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                'Sohail (29 Mar 2017) -- Start
                'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
                'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.EarningForEmployees)
                dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.EarningForEmployees)
                'Sohail (29 Mar 2017) -- End
                If dsList.Tables("Expense").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                        dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dtRow.Item("name")
                        dtFinalTable.Columns.Add(dCol)
                        mintTotEarningCols += 1
                    Next
                End If
                'Sohail (12 Nov 2014) -- End

            End If



            '------------------ Add Gross Pay Column
            dCol = New DataColumn("TGP")
            dCol.Caption = "Gross Pay"
            dCol.DefaultValue = 0

            dCol.DataType = System.Type.GetType("System.Decimal")

            dtFinalTable.Columns.Add(dCol)

            ''/* Deduction Part */

            StrQ = "SELECT " & _
                       "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                       ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                       "FROM prtranhead_master " & _
                   "WHERE ISNULL(prtranhead_master.isvoid, 0) = 0  "

            If mstrDeductionHeadIds.Length > 0 Then
                StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") " & _
                        " AND prtranhead_master.tranheadunkid IN (" & mstrDeductionHeadIds & ") " & _
                        " ORDER BY prtranhead_master.tranheadunkid "
            Else
                If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
                    StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid "
                Else
                    StrQ &= " AND prtranhead_master.trnheadtype_id = -999 ORDER BY prtranhead_master.tranheadunkid "
                End If
                'Sohail (19 Oct 2012) -- End
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            blnAddCol = False
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    If mblnIgnorezeroHeads = True Then
                        dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
                        If dRow.Length > 0 Then
                            blnAddCol = True
                        End If
                    Else
                        blnAddCol = True
                    End If
                    If blnAddCol = True Then
                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

                        'Pinkal (14-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        'Pinkal (14-Dec-2012) -- End

                        dCol.Caption = dtRow.Item("Tname")
                        dCol.DefaultValue = 0

                        dtFinalTable.Columns.Add(dCol)
                        mintTotDeductionCols += 1
                    End If
                    blnAddCol = False
                Next
            End If


            If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
                'dsList = objActivity.getComboList("Activity", False, , enTranHeadType.DeductionForEmployee)
                'If dsList.Tables("Activity").Rows.Count > 0 Then
                '    For Each dtRow As DataRow In dsList.Tables("Activity").Rows
                '        dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
                '        dCol.DataType = System.Type.GetType("System.Decimal")
                '        dCol.DefaultValue = 0
                '        dCol.Caption = dtRow.Item("name")
                '        dtFinalTable.Columns.Add(dCol)
                '        mintTotDeductionCols += 1
                '    Next
                'End If

                'Sohail (12 Nov 2014) -- Start
                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                'Sohail (29 Mar 2017) -- Start
                'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
                'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
                dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.DeductionForEmployee)
                'Sohail (29 Mar 2017) -- End
                If dsList.Tables("Expense").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                        dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dtRow.Item("name")
                        dtFinalTable.Columns.Add(dCol)
                        mintTotDeductionCols += 1
                    Next
                End If
                'Sohail (12 Nov 2014) -- End
            End If

            '------------------ Add Loan Advance Savings

            If mblnShowLoan = True Then
                dCol = New DataColumn("Loan")
                dCol.DefaultValue = 0
                dCol.Caption = "Loan"
                dCol.DataType = System.Type.GetType("System.Decimal")
                dtFinalTable.Columns.Add(dCol)

                mintTotDeductionCols = mintTotDeductionCols + 1
            End If

            If mblnShowAdvance = True Then
                dCol = New DataColumn("Advance")
                dCol.DefaultValue = 0
                dCol.Caption = "Advance"
                dCol.DataType = System.Type.GetType("System.Decimal")
                dtFinalTable.Columns.Add(dCol)

                mintTotDeductionCols = mintTotDeductionCols + 1
            End If

            If mblnShowSavings = True Then
                dCol = New DataColumn("Saving")
                dCol.DefaultValue = 0
                dCol.Caption = "Saving"
                dCol.DataType = System.Type.GetType("System.Decimal")
                dtFinalTable.Columns.Add(dCol)

                mintTotDeductionCols = mintTotDeductionCols + 1
            End If

            '------------------ Add Total Deduction Column
            dCol = New DataColumn("TDD")
            dCol.DefaultValue = 0
            dCol.Caption = "Total Deduction"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Net B/F Column
            dCol = New DataColumn("Openingbalance")
            dCol.DefaultValue = 0
            dCol.Caption = "Net B/F"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)


            '------------------ Add Net Pay Column
            dCol = New DataColumn("NetPay")
            dCol.DefaultValue = 0
            dCol.Caption = "Net Pay"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Total Net Pay Column
            dCol = New DataColumn("TotNetPay")
            dCol.DefaultValue = 0
            dCol.Caption = "Total Net Pay"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '------------------ Add Over/Under Deduction Pay Column
            dCol = New DataColumn("OUDPay")
            dCol.DefaultValue = 0
            dCol.Caption = "Over/Under Deduction Pay"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dtFinalTable.Columns.Add(dCol)

            '*** [Non Earning Deduction Columns]
            StrQ = "SELECT " & _
                      "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
                      ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
                      "FROM prtranhead_master " & _
                  "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "

            If mstrNonEarningDeductionTranHeadIds.Length > 0 Then

                If mstrCurrency_Sign.Trim.Length > 0 Then
                    StrQ &= " AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
                        " AND prtranhead_master.tranheadunkid IN (" & mstrNonEarningDeductionTranHeadIds & ") " & _
                        " ORDER BY prtranhead_master.tranheadunkid "
                Else
                    StrQ &= " AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
                            " AND prtranhead_master.tranheadunkid IN (" & mstrNonEarningDeductionTranHeadIds & ")" & _
                            " ORDER BY prtranhead_master.tranheadunkid "
                End If

            Else
                'StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
                StrQ &= "AND 1 = 2 "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            blnAddCol = False
            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("List").Rows
                    If mblnIgnorezeroHeads = True Then
                        dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
                        If dRow.Length > 0 Then
                            blnAddCol = True
                        End If
                    Else
                        blnAddCol = True
                    End If
                    If blnAddCol = True Then
                        dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dtRow.Item("Tname")
                        dtFinalTable.Columns.Add(dCol)
                        mintTotNonEarningDeductionCols += 1
                    End If
                    blnAddCol = False
                Next
            End If

            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
            If mintModeId = 3 Then 'Earning and Deduction
                'Sohail (29 Mar 2017) -- Start
                'PACRA Enhancement - 65.1 - Include all types of Claim Expense in Reports.
                'dsList = objExpense.getComboList(enExpenseType.EXP_LEAVE, False, "Expense", , , enTranHeadType.Informational)
                dsList = objExpense.getComboList(enExpenseType.EXP_NONE, False, "Expense", , , enTranHeadType.Informational)
                'Sohail (29 Mar 2017) -- End
                If dsList.Tables("Expense").Rows.Count > 0 Then
                    For Each dtRow As DataRow In dsList.Tables("Expense").Rows
                        dCol = New DataColumn("ColumnCR" & dtRow.Item("Id"))
                        dCol.DataType = System.Type.GetType("System.Decimal")
                        dCol.DefaultValue = 0
                        dCol.Caption = dtRow.Item("name")
                        dtFinalTable.Columns.Add(dCol)
                        mintTotNonEarningDeductionCols += 1
                    Next
                End If
            End If
            'Sohail (12 Nov 2014) -- End


            'Sohail (21 Nov 2014) -- Start
            'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
            If mblnShowPaymentDetails = True Then
                dtFinalTable.Columns.Add("Bank", System.Type.GetType("System.String")).DefaultValue = ""
                dtFinalTable.Columns("Bank").Caption = Language.getMessage(mstrModuleName, 18, "Bank")

                dtFinalTable.Columns.Add("AccountNo", System.Type.GetType("System.String")).DefaultValue = ""
                dtFinalTable.Columns("AccountNo").Caption = Language.getMessage(mstrModuleName, 19, "Account #")

                dtFinalTable.Columns.Add("PaymentDetail", System.Type.GetType("System.String")).DefaultValue = ""
                dtFinalTable.Columns("PaymentDetail").Caption = Language.getMessage(mstrModuleName, 20, "Payment Detail")
            End If
            'Sohail (21 Nov 2014) -- End

            If mintViewIndex > 0 Then
                dCol = New DataColumn("Id")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)

                dCol = New DataColumn("GName")
                dCol.DefaultValue = 0
                dCol.DataType = System.Type.GetType("System.String")
                dtFinalTable.Columns.Add(dCol)

            End If


            'Sohail (21 Nov 2014) -- Start
            'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
            If mblnShowPaymentDetails = True Then
                StrQ = "SELECT  TotalPaymentDetails.PeriodId " & _
                              ", TotalPaymentDetails.EmpId " & _
                              ", TotalPaymentDetails.EmpBankTranId " & _
                              ", TotalPaymentDetails.BankName " & _
                              ", TotalPaymentDetails.BranchName " & _
                              ", TotalPaymentDetails.AccountNo " & _
                              ", TotalPaymentDetails.Amount " & _
                              ", TotalPaymentDetails.PaidAmount " & _
                              ", TotalPaymentDetails.basecurrencyid " & _
                              ", TotalPaymentDetails.paidcurrencyid " & _
                              ", TotalPaymentDetails.baseexchangerate " & _
                              ", TotalPaymentDetails.expaidrate " & _
                              ", TotalPaymentDetails.BaseSign " & _
                              ", TotalPaymentDetails.PaidSign " & _
                              ", TotalPaymentDetails.exchange_rate1 " & _
                              ", TotalPaymentDetails.exchange_rate2 " & _
                       "FROM  ( "


                StrQ &= "SELECT  prempsalary_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", prempsalary_tran.empbanktranid AS EmpBankTranId " & _
                                    ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
                                    ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                                    ", premployee_bank_tran.accountno AS AccountNo " & _
                                    ", prempsalary_tran.amount AS Amount " & _
                                    ", prempsalary_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  prempsalary_tran " & _
                                    "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid " & _
                                    "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                    "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = prempsalary_tran.employeeunkid " & _
                                    "JOIN cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "     LEFT JOIN " & _
                             "( " & _
                             "    SELECT " & _
                             "         stationunkid " & _
                             "        ,deptgroupunkid " & _
                             "        ,departmentunkid " & _
                             "        ,sectiongroupunkid " & _
                             "        ,sectionunkid " & _
                             "        ,unitgroupunkid " & _
                             "        ,unitunkid " & _
                             "        ,teamunkid " & _
                             "        ,classgroupunkid " & _
                             "        ,classunkid " & _
                             "        ,employeeunkid " & _
                             "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                             "    FROM hremployee_transfer_tran " & _
                             "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                             ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= " WHERE ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 " & _
                                    "AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "
                'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= "UNION ALL " & _
                             "SELECT  prpayment_tran.employeeunkid AS EmpId " & _
                                    ", prpayment_tran.periodunkid AS PeriodId " & _
                                    ", -1 AS EmpBankTranId " & _
                                    ", @CashPayment AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prpayment_tran.amount AS Amount " & _
                                    ", prpayment_tran.expaidamt AS PaidAmount " & _
                                    ", prpayment_tran.basecurrencyid " & _
                                    ", prpayment_tran.paidcurrencyid " & _
                                    ", prpayment_tran.baseexchangerate " & _
                                    ", prpayment_tran.expaidrate " & _
                                    ", base.currency_sign AS BaseSign " & _
                                    ", paid.currency_sign AS PaidSign " & _
                                    ", paid.exchange_rate1 " & _
                                    ", paid.exchange_rate2 " & _
                             "FROM  prpayment_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_tran.employeeunkid " & _
                                    "JOIN cfexchange_rate AS base ON base.exchangerateunkid = prpayment_tran.basecurrencyid " & _
                                    "JOIN cfexchange_rate AS paid ON paid.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "     LEFT JOIN " & _
                             "( " & _
                             "    SELECT " & _
                             "         stationunkid " & _
                             "        ,deptgroupunkid " & _
                             "        ,departmentunkid " & _
                             "        ,sectiongroupunkid " & _
                             "        ,sectionunkid " & _
                             "        ,unitgroupunkid " & _
                             "        ,unitunkid " & _
                             "        ,teamunkid " & _
                             "        ,classgroupunkid " & _
                             "        ,classunkid " & _
                             "        ,employeeunkid " & _
                             "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                             "    FROM hremployee_transfer_tran " & _
                             "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                             ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                    "AND prpayment_tran.referenceid = 3 " & _
                                    "AND prpayment_tran.paymentmode = 1 " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId  "

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= "UNION ALL " & _
                             "SELECT  prtnaleave_tran.employeeunkid AS EmpId " & _
                                    ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
                                    ", -2 AS EmpBankTranId " & _
                                    ", @SalaryOnHold AS BankName " & _
                                    ", '' AS BranchName " & _
                                    ", '' AS AccountNo " & _
                                    ", prtnaleave_tran.balanceamount AS Amount " & _
                                    ", prtnaleave_tran.balanceamount AS PaidAmount " & _
                                    ", 0 AS basecurrencyid " & _
                                    ", 0 AS paidcurrencyid " & _
                                    ", 0 AS baseexchangerate " & _
                                    ", 0 AS expaidrate " & _
                                    ", '' AS BaseSign " & _
                                    ", '' AS PaidSign " & _
                                    ", 0 AS exchange_rate1 " & _
                                    ", 0 AS exchange_rate2 " & _
                             "FROM  prtnaleave_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = prtnaleave_tran.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "     LEFT JOIN " & _
                             "( " & _
                             "    SELECT " & _
                             "         stationunkid " & _
                             "        ,deptgroupunkid " & _
                             "        ,departmentunkid " & _
                             "        ,sectiongroupunkid " & _
                             "        ,sectionunkid " & _
                             "        ,unitgroupunkid " & _
                             "        ,unitunkid " & _
                             "        ,teamunkid " & _
                             "        ,classgroupunkid " & _
                             "        ,classunkid " & _
                             "        ,employeeunkid " & _
                             "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                             "    FROM hremployee_transfer_tran " & _
                             "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                             ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE prtnaleave_tran.isvoid = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND prtnaleave_tran.balanceamount <> 0 "

                If mintEmployeeId > 0 Then
                    StrQ &= "	AND hremployee_master.employeeunkid = @EmpId "
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '          " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter.Length > 0 Then
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND hremployee_master.stationunkid = @BranchId "
                    StrQ &= " AND T.stationunkid = @BranchId "
                    'Sohail (21 Aug 2015) -- End
                End If




                StrQ &= ") AS TotalPaymentDetails " & _
                        "GROUP BY  TotalPaymentDetails.PeriodId " & _
                            ",TotalPaymentDetails.EmpId " & _
                            ",TotalPaymentDetails.EmpBankTranId " & _
                            ",TotalPaymentDetails.BankName " & _
                            ",TotalPaymentDetails.BranchName " & _
                            ",TotalPaymentDetails.AccountNo " & _
                            ",TotalPaymentDetails.Amount " & _
                            ", TotalPaymentDetails.PaidAmount " & _
                            ",TotalPaymentDetails.basecurrencyid " & _
                      ", TotalPaymentDetails.paidcurrencyid " & _
                      ", TotalPaymentDetails.baseexchangerate " & _
                      ", TotalPaymentDetails.expaidrate " & _
                      ", TotalPaymentDetails.BaseSign " & _
                      ", TotalPaymentDetails.PaidSign " & _
                      ", TotalPaymentDetails.exchange_rate1 " & _
                      ", TotalPaymentDetails.exchange_rate2 "

                objDataOperation.AddParameter("@CashPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "Cash Payment"))
                objDataOperation.AddParameter("@SalaryOnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "Salary On Hold"))

                dsPaymetDetail = objDataOperation.ExecQuery(StrQ, "Payment")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If



                For Each dtBank As DataRow In dsPaymetDetail.Tables("Payment").Rows
                    strCurrKey = dtBank.Item("PeriodId") & "_" & dtBank.Item("EmpId")
                    If mdicEmpBank.ContainsKey(strCurrKey) Then

                        If dtBank("EmpBankTranId") > 0 Then
                            mdicEmpBank(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 13, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 14, "Branch :") & dtBank.Item("BranchName")
                            'Sohail (09 Dec 2014) -- Start
                            'Error - Given key was not found in dictionary when employee is paid rounding amount. (bank details + salary on hold -- and first entry is salary on hold no accountno detail in dictionary).
                            'mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 15, "Account # :") & dtBank.Item("AccountNo")
                            If mdicEmpAccount.ContainsKey(strCurrKey) = True Then
                                mdicEmpAccount(strCurrKey) &= "#10;" & Language.getMessage(mstrModuleName, 15, "Account # :") & dtBank.Item("AccountNo")
                            Else
                                mdicEmpAccount.Add(strCurrKey, "#10;" & Language.getMessage(mstrModuleName, 15, "Account # :") & dtBank.Item("AccountNo"))
                            End If
                            'Sohail (09 Dec 2014) -- End
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                            mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), strFmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                            'Sohail (21 Aug 2015) -- End
                        Else
                            mdicEmpBank(strCurrKey) &= "#10;" & dtBank.Item("BankName")
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                            mdicEmpPayment(strCurrKey) &= "#10;" & Format(CDec(dtBank.Item("PaidAmount")), strFmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & ")"
                            'Sohail (21 Aug 2015) -- End
                        End If

                    Else

                        If dtBank("EmpBankTranId") > 0 Then
                            mdicEmpBank.Add(strCurrKey, Language.getMessage(mstrModuleName, 13, "Bank :") & dtBank.Item("BankName") & ", " & Language.getMessage(mstrModuleName, 14, "Branch :") & dtBank.Item("BranchName"))
                            mdicEmpAccount.Add(strCurrKey, Language.getMessage(mstrModuleName, 15, "Account # :") & dtBank.Item("AccountNo") & ",")
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                            mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), strFmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                            'Sohail (21 Aug 2015) -- End
                        Else
                            If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso CDec(dtBank.Item("amount")) < 0 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                                Continue For 'Over Payment; Show Payment detail only
                            End If
                            If CInt(dtBank.Item("EmpBankTranId")) = -2 AndAlso dsPaymetDetail.Tables("Payment").Select("PeriodId = " & dtBank.Item("PeriodId") & " AND EmpId = " & dtBank.Item("EmpId") & " ").Length > 1 Then
                                mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                                mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), strFmtCurrency) & "),")
                                'Sohail (21 Aug 2015) -- End
                            Else
                                If CInt(dtBank.Item("EmpBankTranId")) = -2 Then 'Salary On Hold
                                    mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), GUI.fmtCurrency) & "),")
                                    mdicEmpPayment.Add(strCurrKey, " (" & Format(dtBank.Item("amount"), strFmtCurrency) & "),")
                                    'Sohail (21 Aug 2015) -- End
                                Else
                                    mdicEmpBank.Add(strCurrKey, dtBank.Item("BankName"))
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), GUI.fmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                    mdicEmpPayment.Add(strCurrKey, Format(CDec(dtBank.Item("PaidAmount")), strFmtCurrency) & " (" & dtBank.Item("PaidSign").ToString & "),")
                                    'Sohail (21 Aug 2015) -- End
                                End If
                            End If
                        End If

                    End If
                Next
            End If
            'Sohail (21 Nov 2014) -- End


            Dim rpt_Row As DataRow = Nothing
            Dim intPrevGrpID As Integer = 0
            Dim intPrevEmpID As Integer = 0
            Dim strKey As String = "" 'Sohail (21 Nov 2014)
            For Each drRow As DataRow In dsData.Tables("List").Rows
                strKey = drRow.Item("PeriodID").ToString & "_" & drRow.Item("EmpID").ToString 'Sohail (21 Nov 2014)

                If (mintViewIndex > 0 AndAlso intPrevGrpID = 0 AndAlso intPrevGrpID <> CInt(drRow.Item("ID"))) OrElse intPrevEmpID = 0 OrElse intPrevEmpID <> CInt(drRow.Item("EmpID")) Then
                    rpt_Row = dtFinalTable.NewRow

                    rpt_Row.Item("EmpId") = drRow.Item("EmpID")
                    rpt_Row.Item("EmpCode") = drRow.Item("Code")
                    rpt_Row.Item("EmpName") = drRow.Item("EmpName")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency)
                    rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    rpt_Row.Item("TGP") = "0"

                    rpt_Row.Item("TDD") = "0"

                    'Sohail (21 Nov 2014) -- Start
                    'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
                    If mblnShowPaymentDetails = True Then
                        If mdicEmpBank.ContainsKey(strKey) = True Then
                            If mdicEmpBank.Item(strKey).Contains("#10;") = False Then
                                rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey).Replace(",", "")
                            Else
                                rpt_Row.Item("Bank") = mdicEmpBank.Item(strKey)
                            End If
                        Else
                            rpt_Row.Item("Bank") = ""
                        End If

                        If mdicEmpAccount.ContainsKey(strKey) Then
                            If mdicEmpAccount.Item(strKey).Contains("#10;") = False Then
                                rpt_Row.Item("AccountNo") = mdicEmpAccount.Item(strKey).Replace(",", "")
                            Else
                                rpt_Row.Item("AccountNo") = mdicEmpAccount.Item(strKey)
                            End If
                        Else
                            rpt_Row.Item("AccountNo") = ""
                        End If

                        If mdicEmpPayment.ContainsKey(strKey) = True Then
                            If mdicEmpPayment.Item(strKey).Contains("#10;") = False Then
                                rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey).Replace(",", "")
                            Else
                                rpt_Row.Item("PaymentDetail") = mdicEmpPayment.Item(strKey)
                            End If
                        Else
                            rpt_Row.Item("PaymentDetail") = ""
                        End If
                    Else
                        'rpt_Row.Item("Bank") = ""
                        'rpt_Row.Item("AccountNo") = ""
                        'rpt_Row.Item("PaymentDetail") = ""
                    End If
                    'Sohail (21 Nov 2014) -- End

                    dtFinalTable.Rows.Add(rpt_Row)
                End If

                rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)

                Select Case CInt(drRow.Item("GroupID"))
                    Case enViewPayroll_Group.Earning
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (22 Sep 2017) -- Start
                        'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                        'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                        'Sohail (22 Sep 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.Deduction
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (22 Sep 2017) -- Start
                        'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                        'Sohail (22 Sep 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.EmployerContribution
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.Informational
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.Loan
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Loan") = Format(CDec(rpt_Row.Item("Loan")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Loan") = Format(CDec(rpt_Row.Item("Loan")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (22 Sep 2017) -- Start
                        'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                        'Sohail (22 Sep 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.Advance
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Advance") = Format(CDec(rpt_Row.Item("Advance")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Advance") = Format(CDec(rpt_Row.Item("Advance")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (22 Sep 2017) -- Start
                        'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                        'Sohail (22 Sep 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.Saving
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Row.Item("Saving") = Format(CDec(rpt_Row.Item("Saving")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                        rpt_Row.Item("Saving") = Format(CDec(rpt_Row.Item("Saving")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        'Sohail (22 Sep 2017) -- Start
                        'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                        'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                        rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                        'Sohail (22 Sep 2017) -- End
                        'Sohail (21 Aug 2015) -- End

                    Case enViewPayroll_Group.PayPerActivity
                        objActivity._Activityunkid = CInt(drRow.Item("activityunkid"))
                        If objActivity._TranheadTypeId = enTranHeadType.EarningForEmployees AndAlso (mintModeId = 1 OrElse mintModeId = 3) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'rpt_Row.Item("ColumnPPA" & drRow.Item("activityunkid")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            rpt_Row.Item("ColumnPPA" & drRow.Item("activityunkid")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                            'Sohail (22 Sep 2017) -- Start
                            'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                            'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                            rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                            'Sohail (22 Sep 2017) -- End
                            'Sohail (21 Aug 2015) -- End
                        ElseIf objActivity._TranheadTypeId = enTranHeadType.DeductionForEmployee AndAlso (mintModeId = 2 OrElse mintModeId = 3) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'rpt_Row.Item("ColumnPPA" & drRow.Item("activityunkid")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            rpt_Row.Item("ColumnPPA" & drRow.Item("activityunkid")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                            rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                            'Sohail (21 Aug 2015) -- End
                        End If

                        'Sohail (12 Nov 2014) -- Start
                        'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                    Case enViewPayroll_Group.CRExpense
                        objExpense._Expenseunkid = CInt(drRow.Item("expenseunkid"))
                        If objExpense._tranheadtypeunkid = enTranHeadType.EarningForEmployees AndAlso (mintModeId = 1 OrElse mintModeId = 3) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                            'Sohail (22 Sep 2017) -- Start
                            'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                            'rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                            rpt_Row.Item("TGP") = CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount"))
                            'Sohail (22 Sep 2017) -- End
                            'Sohail (21 Aug 2015) -- End
                        ElseIf objExpense._tranheadtypeunkid = enTranHeadType.DeductionForEmployee AndAlso (mintModeId = 2 OrElse mintModeId = 3) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                            'Sohail (22 Sep 2017) -- Start
                            'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                            'rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), strFmtCurrency)
                            rpt_Row.Item("TDD") = CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount"))
                            'Sohail (22 Sep 2017) -- End
                            'Sohail (21 Aug 2015) -- End
                        ElseIf objExpense._tranheadtypeunkid = enTranHeadType.Informational AndAlso (mintModeId = 3) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
                            rpt_Row.Item("ColumnCR" & drRow.Item("expenseunkid")) = Format(CDec(drRow.Item("Amount")), strFmtCurrency)
                            'Sohail (21 Aug 2015) -- End
                        End If
                        'Sohail (12 Nov 2014) -- End

                End Select

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)
                'Sohail (22 Sep 2017) -- Start
                'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                'rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), strFmtCurrency)
                rpt_Row.Item("NetPay") = CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD"))
                'Sohail (22 Sep 2017) -- End
                'Sohail (21 Aug 2015) -- End
                'rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency) 'Sohail (26 May 2014) - Issue : Net Pay not matching with Payroll Report.

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), GUI.fmtCurrency)
                'Sohail (22 Sep 2017) -- Start
                'SUMATRA Issue - 69.1 - Total Net Pay was not matching with Bank Payment List Report and Global Payment Total.
                'rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), strFmtCurrency)
                rpt_Row.Item("TotNetPay") = Rounding.BRound(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), dblRoundOff_Type).ToString()
                'Sohail (22 Sep 2017) -- End
                'Sohail (21 Aug 2015) -- End

                If CDec(rpt_Row.Item("TotNetPay")) < 0 Then

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("OUDPay") = IIf(CDec(rpt_Row.Item("TotNetPay")) < 0, Format(CDec(rpt_Row.Item("TotNetPay")), GUI.fmtCurrency), 0)
                    'rpt_Row.Item("TotNetPay") = Format(CDec(0), GUI.fmtCurrency)
                    rpt_Row.Item("OUDPay") = IIf(CDec(rpt_Row.Item("TotNetPay")) < 0, Format(CDec(rpt_Row.Item("TotNetPay")), strFmtCurrency), 0)
                    rpt_Row.Item("TotNetPay") = Format(CDec(0), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("OUDPay") = Format(CDec(0), GUI.fmtCurrency)
                    rpt_Row.Item("OUDPay") = Format(CDec(0), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                End If

                If mintViewIndex > 0 Then
                    rpt_Row.Item("ID") = CInt(drRow.Item("ID"))
                    rpt_Row.Item("GName") = drRow.Item("GName").ToString
                    If mintViewIndex = enAnalysisReport.CostCenter Then
                        rpt_Row.Item("GCode") = drRow.Item("GCode").ToString
                    End If
                    intPrevGrpID = CInt(drRow.Item("ID"))
                End If
                intPrevEmpID = CInt(drRow.Item("EmpID"))
            Next

            dtFinalTable.AcceptChanges()


            Dim dblTotal As Decimal = 0
            Dim m As Integer = 3
            Dim n As Integer = 3
            If mintViewIndex > 0 AndAlso mintViewIndex = enAnalysisReport.CostCenter Then
                m = 4
                n = 4
            End If
            dblColTot = New Decimal(dtFinalTable.Columns.Count) {}
            Dim c As Integer
            If mintViewIndex > 0 Then
                c = dtFinalTable.Columns.Count - 2
            Else
                c = dtFinalTable.Columns.Count
            End If

            'Sohail (21 Nov 2014) -- Start
            'FDRC Enhancement - Show Payment Detail on ED Spreadsheet Report.
            If mblnShowPaymentDetails = True Then
                c -= 3
            End If
            'Sohail (21 Nov 2014) -- End

            While m < c
                For t As Integer = 0 To dtFinalTable.Rows.Count - 1
                    dblTotal += CDec(dtFinalTable.Rows(t)(m))
                Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
                dblColTot(n) = Format(CDec(dblTotal), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                dblTotal = 0
                m += 1
                n += 1
            End While

            Call FilterTitleAndFilterQuery()


            Dim strGTotal As String = Language.getMessage(mstrModuleName, 5, "Grand Total :")
            Dim strSubTotal As String = ""
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            If mblnIgnorezeroHeads Then
                mdtTableExcel = IgnoreZeroHead(dtFinalTable)
            Else
                mdtTableExcel = dtFinalTable
            End If


            'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

            mdtTableExcel.Columns.RemoveAt(0)
            If mintViewIndex > 0 Then
                mdtTableExcel.Columns.Remove("ID")
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
                strSubTotal = Language.getMessage(mstrModuleName, 6, "Sub Total :")
            End If

            'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 



            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName, "s10bw")
            row.Cells.Add(wcell)
            If mstrCurrency_Sign.Trim <> "" AndAlso mstrCurrency_Rate.Trim <> "" Then
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 23, "Currency Rate :") & " " & mstrCurrency_Rate, "s10bw")
                wcell.MergeAcross = 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)



            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            wcell = New WorksheetCell("", "HeaderStyle")
            row.Cells.Add(wcell)

            If mintTotEarningCols > 0 AndAlso mintTotDeductionCols <= 0 Then

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mintTotEarningCols - 1

                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
                row.Cells.Add(wcell)

                Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 3 - mintTotEarningCols - 2)

                For i As Integer = 0 To mintRow
                    wcell = New WorksheetCell("", "HeaderStyle")
                    row.Cells.Add(wcell)
                Next

            ElseIf mintTotDeductionCols > 0 AndAlso mintTotEarningCols <= 0 Then

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mintTotDeductionCols - 1

                Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 3 - mintTotDeductionCols - 1)

                For i As Integer = 0 To mintRow
                    wcell = New WorksheetCell("", "HeaderStyle")
                    row.Cells.Add(wcell)
                Next

            ElseIf mintTotEarningCols > 0 AndAlso mintTotDeductionCols > 0 Then

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mintTotEarningCols - 1

                wcell = New WorksheetCell("", "HeaderStyle")
                row.Cells.Add(wcell)

                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
                row.Cells.Add(wcell)
                wcell.MergeAcross = mintTotDeductionCols - 1

                Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 2 - mintTotEarningCols - mintTotDeductionCols - 2)

                For i As Integer = 0 To mintRow
                    wcell = New WorksheetCell("", "HeaderStyle")
                    row.Cells.Add(wcell)
                Next

            End If

            rowsArrayHeader.Add(row)


            'SET EXCEL CELL WIDTH
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName & " " & mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName & " " & mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EDSpreadsheetReport_New; Module Name: " & mstrModuleName)
            'Sohail (12 Nov 2014) -- Start
            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
        Finally
            objActivity = Nothing
            objExpense = Nothing
            'Sohail (12 Nov 2014) -- End
        End Try
    End Sub
    'Sohail (09 Mar 2012) -- End

#Region " Gene PERFORMANCE Issue on more than 2000 employees (TANAPA) moved on 21-May-2014 by Sohail "
    'Public Sub Generate_EDSpreadsheetReport_New()
    '    Dim dsList As DataSet
    '    Dim dsData As New DataSet
    '    Dim dtFinalTable As DataTable
    '    Dim exForce As Exception
    '    Dim StrQ As String = String.Empty

    '    Dim objActivity As New clsActivity_Master   'Pinkal (03-Jul-2013)

    '    Try

    '        If ConfigParameter._Object._ExportReportPath = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        objDataOperation = New clsDataOperation

    '        'Sohail (17 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Sohail (17 Aug 2012) -- End

    '        'S.SANDEEP [ 11 SEP 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT  payperiodunkid AS PeriodId " & _
    '        '       ", period_name AS PeriodName " & _
    '        '       ", vwPayroll.employeeunkid AS EmpId " & _
    '        '       ", vwPayroll.employeecode AS Code " & _
    '        '       ", employeename AS EmpName " & _
    '        '       ", tranheadunkid AS TranId " & _
    '        '       ", SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '        '       ", openingbalance AS Openingbalance " & _
    '        '       ", GroupID "

    '        StrQ = "SELECT  payperiodunkid AS PeriodId " & _
    '                      ", period_name AS PeriodName " & _
    '                      ", vwPayroll.employeeunkid AS EmpId " & _
    '                      ", vwPayroll.employeecode AS Code " & _
    '                      ", employeename AS EmpName " & _
    '                      ",  vwPayroll.tranheadunkid AS TranId " & _
    '                      ", CASE GroupID " & _
    '                      "     WHEN " & enViewPayroll_Group.Informational & " " & _
    '                      "     THEN CASE ISNULL(prtranhead_master.ismonetary, 0) " & _
    '                      "             WHEN 1 " & _
    '                      "             THEN ( SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) * " & mdecEx_Rate & ") " & _
    '                      "             ELSE SUM(Amount) * 1.0000 " & _
    '                      "          END" & _
    '                      "     ELSE (SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) * " & mdecEx_Rate & ") " & _
    '                      "  END AS Amount " & _
    '                      ", (SUM(CAST(openingbalance AS DECIMAL(36," & decDecimalPlaces & "))) * " & mdecEx_Rate & ")  AS Openingbalance " & _
    '                      ", GroupID "
    '        'Sohail (18 Jun 2013) - [", (SUM(CAST(Amount AS DECIMAL(36, " & decDecimalPlaces & "))) * " & mdecEx_Rate & ") AS Amount " & _]
    '        'S.SANDEEP [ 11 SEP 2012 ] -- END

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '            'Sohail (08 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex = enAnalysisReport.CostCenter Then
    '                StrQ &= ", prcostcenter_master.costcentercode AS GCode "
    '            End If
    '            'Sohail (08 Mar 2013) -- End
    '        Else
    '            StrQ &= ", '' AS GName "
    '        End If

    '        StrQ &= "FROM    vwPayroll " & _
    '                "JOIN hremployee_master ON hremployee_master.employeeunkid = vwPayroll.employeeunkid " & _
    '                "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '                        "AND vwPayroll.GroupID IN ( " & enViewPayroll_Group.Earning & ", " & enViewPayroll_Group.Deduction & ", " & enViewPayroll_Group.EmployerContribution & ", " & enViewPayroll_Group.Informational & " ) "
    '        'Sohail (18 Jun 2013) - [LEFT JOIN prtranhead_master]

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        StrQ &= "WHERE   payperiodunkid = @PeriodId " & _
    '                        "AND  GroupID IN ( " & enViewPayroll_Group.Earning & ", " & _
    '                                               enViewPayroll_Group.Deduction & ", " & _
    '                                               enViewPayroll_Group.EmployerContribution & ", " & _
    '                                               enViewPayroll_Group.Informational & ", " & _
    '                                               enViewPayroll_Group.Loan & ", " & _
    '                                               enViewPayroll_Group.Advance & ", " & _
    '                                               enViewPayroll_Group.Saving & ", " & _
    '                                               enViewPayroll_Group.PayPerActivity & " )  "     'Pinkal (03-Jul-2013) - [PayPerActivity]


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End


    '        If mblnIsActive = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND vwPayroll.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        Dim strHeadIDs As String = ""
    '        Dim strHeadTypeIDs As String = ""
    '        If mstrEarningHeadsIds.Length > 0 Then
    '            strHeadIDs = "," & mstrEarningHeadsIds
    '        Else
    '            'Sohail (19 Oct 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strHeadTypeIDs = "," & enViewPayroll_Group.Earning
    '            If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
    '                strHeadTypeIDs = "," & enViewPayroll_Group.Earning
    '            End If
    '            'Sohail (19 Oct 2012) -- End
    '        End If
    '        If mstrDeductionHeadIds.Length > 0 Then
    '            strHeadIDs &= "," & mstrDeductionHeadIds
    '        Else
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strHeadTypeIDs &= "," & enViewPayroll_Group.Deduction & "," & enViewPayroll_Group.Loan & "," & enViewPayroll_Group.Advance & "," & enViewPayroll_Group.Saving
    '            'Sohail (19 Oct 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'strHeadTypeIDs &= "," & enViewPayroll_Group.Deduction
    '            If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
    '                strHeadTypeIDs &= "," & enViewPayroll_Group.Deduction
    '            End If
    '            'Sohail (19 Oct 2012) -- End
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (17 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If mblnShowLoan = True Then
    '            strHeadTypeIDs &= "," & enViewPayroll_Group.Loan
    '        End If
    '        If mblnShowAdvance = True Then
    '            strHeadTypeIDs &= "," & enViewPayroll_Group.Advance
    '        End If
    '        If mblnShowSavings = True Then
    '            strHeadTypeIDs &= "," & enViewPayroll_Group.Saving
    '        End If


    '        'Pinkal (03-Jul-2013) -- Start
    '        'Enhancement : TRA Changes
    '        strHeadTypeIDs &= "," & enViewPayroll_Group.PayPerActivity
    '        'Pinkal (03-Jul-2013) -- End


    '        'Sohail (17 Apr 2012) -- End
    '        If mstrNonEarningDeductionTranHeadIds.Length > 0 Then
    '            strHeadIDs &= "," & mstrNonEarningDeductionTranHeadIds
    '        Else
    '            'strHeadTypeIDs &= "," & enViewPayroll_Group.EmployerContribution & "," & enViewPayroll_Group.Informational
    '        End If
    '        If strHeadIDs.Length > 0 Then
    '            strHeadIDs = strHeadIDs.Substring(1)
    '        End If
    '        If strHeadTypeIDs.Length > 0 Then
    '            strHeadTypeIDs = strHeadTypeIDs.Substring(1)
    '        End If
    '        Dim strNonLoanAdvanceGroup As String = "" & enViewPayroll_Group.Earning & "," & enViewPayroll_Group.Deduction & "," & enViewPayroll_Group.EmployerContribution & "," & enViewPayroll_Group.Informational & "" 'Sohail (17 Apr 2012)
    '        If strHeadIDs.Length > 0 AndAlso strHeadTypeIDs.Length <= 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND vwPayroll.tranheadunkid IN (" & strHeadIDs & ")"
    '            StrQ &= " AND (vwPayroll.tranheadunkid IN (" & strHeadIDs & ") AND GroupID IN (" & strNonLoanAdvanceGroup & ") ) "
    '            'Sohail (17 Apr 2012) -- End
    '        ElseIf strHeadIDs.Length > 0 AndAlso strHeadTypeIDs.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND ( vwPayroll.tranheadunkid IN (" & strHeadIDs & ") OR GroupID IN (" & strHeadTypeIDs & ") )"
    '            StrQ &= " AND ( ( vwPayroll.tranheadunkid IN (" & strHeadIDs & ") AND GroupID IN (" & strNonLoanAdvanceGroup & ") ) OR GroupID IN (" & strHeadTypeIDs & ") )"
    '            'Sohail (17 Apr 2012) -- End
    '        ElseIf strHeadIDs.Length <= 0 AndAlso strHeadTypeIDs.Length > 0 Then
    '            StrQ &= " AND GroupID IN (" & strHeadTypeIDs & ") "
    '        End If

    '        If mintBranchId > 0 Then
    '            StrQ &= "AND vwPayroll.BranchId = @BranchId "
    '            objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
    '        End If
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND vwPayroll.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        If mintEmployeeId > 0 Then
    '            StrQ &= "AND vwPayroll.employeeunkid = @EmpId  "
    '            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
    '        End If

    '        StrQ &= "GROUP BY payperiodunkid " & _
    '                      ", period_name " & _
    '                      ", vwPayroll.employeeunkid " & _
    '                      ", vwPayroll.employeecode " & _
    '                      ", employeename " & _
    '                      ",  vwPayroll.tranheadunkid " & _
    '                      ", GroupID " & _
    '                      ", openingbalance " & _
    '                      ", prtranhead_master.ismonetary "
    '        'Sohail (18 Jun 2013) - [ismonetary]

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "") & " "
    '            'Sohail (08 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex = enAnalysisReport.CostCenter Then
    '                StrQ &= ", prcostcenter_master.costcentercode "
    '            End If
    '            'Sohail (08 Mar 2013) -- End
    '        End If
    '        If mblnIgnorezeroHeads = True Then
    '            'Sohail (19 Dec 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " HAVING SUM(amount) > 0 "
    '            StrQ &= " HAVING SUM(amount) <> 0 "
    '            'Sohail (19 Dec 2012) -- End
    '        End If

    '        If mintViewIndex > 0 Then
    '            'Sohail (08 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "ORDER BY " & mstrAnalysis_OrderBy & ", vwPayroll.employeeunkid, tranheadunkid "
    '            'Sohail (07 Aug 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", vwPayroll.employeeunkid,  vwPayroll.tranheadunkid "
    '            If Me.OrderByQuery <> "" Then
    '                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", " & Me.OrderByQuery & ", vwPayroll.tranheadunkid "
    '            Else
    '                StrQ &= "ORDER BY " & mstrAnalysis_OrderBy_GName & ", vwPayroll.employeecode, vwPayroll.tranheadunkid "
    '            End If
    '            'Sohail (07 Aug 2013) -- End
    '            'Sohail (08 Mar 2013) -- End
    '        Else
    '            'Sohail (07 Aug 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "ORDER BY vwPayroll.employeeunkid,  vwPayroll.tranheadunkid "
    '            If Me.OrderByQuery <> "" Then
    '                StrQ &= "ORDER BY " & Me.OrderByQuery & ", vwPayroll.tranheadunkid "
    '            Else
    '                StrQ &= "ORDER BY vwPayroll.employeecode, vwPayroll.tranheadunkid "
    '            End If
    '            'Sohail (07 Aug 2013) -- End
    '        End If


    '        objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

    '        dsData = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        dtFinalTable = New DataTable("EDSpreadsheet")
    '        Dim dCol As DataColumn
    '        mintTotEarningCols = 0
    '        mintTotDeductionCols = 0
    '        mintTotNonEarningDeductionCols = 0

    '        dCol = New DataColumn("EmpId")
    '        dCol.Caption = "EmpId"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        'Sohail (08 Mar 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mintViewIndex > 0 AndAlso mintViewIndex = enAnalysisReport.CostCenter Then
    '            dCol = New DataColumn("GCode")
    '            dCol.Caption = "Cost Center Code"
    '            dCol.DataType = System.Type.GetType("System.String")
    '            dCol.DefaultValue = ""
    '            dtFinalTable.Columns.Add(dCol)
    '        End If
    '        'Sohail (08 Mar 2013) -- End

    '        dCol = New DataColumn("EmpCode")
    '        dCol.Caption = "Code"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        dCol = New DataColumn("EmpName")
    '        dCol.Caption = "Employee"
    '        dCol.DataType = System.Type.GetType("System.String")
    '        dtFinalTable.Columns.Add(dCol)

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '               "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "
    '        If mstrEarningHeadsIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrEarningHeadsIds & ")" & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            'Sohail (19 Oct 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
    '            If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
    '                StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
    '            Else
    '                StrQ &= "AND prtranhead_master.trnheadtype_id = -999 ORDER BY prtranhead_master.tranheadunkid "
    '            End If
    '            'Sohail (19 Oct 2012) -- End
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dRow() As DataRow = Nothing
    '        Dim blnAddCol As Boolean = False
    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                If mblnIgnorezeroHeads = True Then
    '                    dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
    '                    If dRow.Length > 0 Then
    '                        blnAddCol = True
    '                    End If
    '                Else
    '                    blnAddCol = True
    '                End If
    '                If blnAddCol = True Then
    '                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

    '                    'Pinkal (14-Dec-2012) -- Start
    '                    'Enhancement : TRA Changes
    '                    dCol.DataType = System.Type.GetType("System.Decimal")
    '                    'Pinkal (14-Dec-2012) -- End

    '                    dCol.DefaultValue = 0
    '                    dCol.Caption = dtRow.Item("Tname")
    '                    dtFinalTable.Columns.Add(dCol)
    '                    mintTotEarningCols += 1
    '                End If
    '                blnAddCol = False
    '            Next
    '        End If




    '        'Pinkal (03-Jul-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If mintModeId = 1 OrElse mintModeId = 3 Then ' Earning OR Earning and Deduction
    '            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.EarningForEmployees)
    '            If dsList.Tables("Activity").Rows.Count > 0 Then
    '                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
    '                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
    '                    dCol.DataType = System.Type.GetType("System.Decimal")
    '                    dCol.DefaultValue = 0
    '                    dCol.Caption = dtRow.Item("name")
    '                    dtFinalTable.Columns.Add(dCol)
    '                    mintTotEarningCols += 1
    '                Next
    '            End If
    '        End If

    '        'Pinkal (03-Jul-2013) -- End



    '        '------------------ Add Gross Pay Column
    '        dCol = New DataColumn("TGP")
    '        dCol.Caption = "Gross Pay"
    '        dCol.DefaultValue = 0

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End

    '        dtFinalTable.Columns.Add(dCol)

    '        ''/* Deduction Part */

    '        StrQ = "SELECT " & _
    '                   "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                   ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                   "FROM prtranhead_master " & _
    '               "WHERE ISNULL(prtranhead_master.isvoid, 0) = 0  "

    '        If mstrDeductionHeadIds.Length > 0 Then
    '            StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") " & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrDeductionHeadIds & ") " & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '        Else
    '            'Sohail (19 Oct 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid "
    '            If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
    '                StrQ &= " AND prtranhead_master.trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ORDER BY prtranhead_master.tranheadunkid "
    '            Else
    '                StrQ &= " AND prtranhead_master.trnheadtype_id = -999 ORDER BY prtranhead_master.tranheadunkid "
    '            End If
    '            'Sohail (19 Oct 2012) -- End
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        blnAddCol = False
    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                If mblnIgnorezeroHeads = True Then
    '                    dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
    '                    If dRow.Length > 0 Then
    '                        blnAddCol = True
    '                    End If
    '                Else
    '                    blnAddCol = True
    '                End If
    '                If blnAddCol = True Then
    '                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

    '                    'Pinkal (14-Dec-2012) -- Start
    '                    'Enhancement : TRA Changes
    '                    dCol.DataType = System.Type.GetType("System.Decimal")
    '                    'Pinkal (14-Dec-2012) -- End

    '                    dCol.Caption = dtRow.Item("Tname")
    '                    dCol.DefaultValue = 0

    '                    dtFinalTable.Columns.Add(dCol)
    '                    mintTotDeductionCols += 1
    '                End If
    '                blnAddCol = False
    '            Next
    '        End If



    '        'Pinkal (03-Jul-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If mintModeId = 2 OrElse mintModeId = 3 Then ' Deduction OR Earning and Deduction
    '            dsList = objActivity.getComboList("Activity", False, , enTranHeadType.DeductionForEmployee)
    '            If dsList.Tables("Activity").Rows.Count > 0 Then
    '                For Each dtRow As DataRow In dsList.Tables("Activity").Rows
    '                    dCol = New DataColumn("ColumnPPA" & dtRow.Item("activityunkid"))
    '                    dCol.DataType = System.Type.GetType("System.Decimal")
    '                    dCol.DefaultValue = 0
    '                    dCol.Caption = dtRow.Item("name")
    '                    dtFinalTable.Columns.Add(dCol)
    '                    mintTotDeductionCols += 1
    '                Next
    '            End If
    '        End If

    '        'Pinkal (03-Jul-2013) -- End


    '        '------------------ Add Loan Advance Savings

    '        'Sohail (17 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'dCol = New DataColumn("Loan")
    '        'dCol.DefaultValue = 0
    '        'dCol.Caption = "Loan"
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        'dtFinalTable.Columns.Add(dCol)

    '        'dCol = New DataColumn("Advance")
    '        'dCol.DefaultValue = 0
    '        'dCol.Caption = "Advance"
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        'dtFinalTable.Columns.Add(dCol)

    '        'dCol = New DataColumn("Saving")
    '        'dCol.DefaultValue = 0
    '        'dCol.Caption = "Saving"
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        'dtFinalTable.Columns.Add(dCol)

    '        'mintTotDeductionCols = mintTotDeductionCols + 3
    '        If mblnShowLoan = True Then
    '            dCol = New DataColumn("Loan")
    '            dCol.DefaultValue = 0
    '            dCol.Caption = "Loan"

    '            'Pinkal (14-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            'dCol.DataType = System.Type.GetType("System.String")
    '            dCol.DataType = System.Type.GetType("System.Decimal")
    '            'Pinkal (14-Dec-2012) -- End


    '            dtFinalTable.Columns.Add(dCol)

    '            mintTotDeductionCols = mintTotDeductionCols + 1
    '        End If

    '        If mblnShowAdvance = True Then
    '            dCol = New DataColumn("Advance")
    '            dCol.DefaultValue = 0
    '            dCol.Caption = "Advance"

    '            'Pinkal (14-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            'dCol.DataType = System.Type.GetType("System.String")
    '            dCol.DataType = System.Type.GetType("System.Decimal")
    '            'Pinkal (14-Dec-2012) -- End


    '            dtFinalTable.Columns.Add(dCol)
    '            mintTotDeductionCols = mintTotDeductionCols + 1
    '        End If

    '        If mblnShowSavings = True Then
    '            dCol = New DataColumn("Saving")
    '            dCol.DefaultValue = 0
    '            dCol.Caption = "Saving"

    '            'Pinkal (14-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            'dCol.DataType = System.Type.GetType("System.String")
    '            dCol.DataType = System.Type.GetType("System.Decimal")
    '            'Pinkal (14-Dec-2012) -- End


    '            dtFinalTable.Columns.Add(dCol)
    '            mintTotDeductionCols = mintTotDeductionCols + 1
    '        End If
    '        'Sohail (17 Apr 2012) -- End

    '        '------------------ Add Total Deduction Column
    '        dCol = New DataColumn("TDD")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Deduction"

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Net B/F Column
    '        dCol = New DataColumn("Openingbalance")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net B/F"

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)


    '        '------------------ Add Net Pay Column
    '        dCol = New DataColumn("NetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Net Pay"

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Total Net Pay Column
    '        dCol = New DataColumn("TotNetPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Total Net Pay"

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)

    '        '------------------ Add Over/Under Deduction Pay Column
    '        dCol = New DataColumn("OUDPay")
    '        dCol.DefaultValue = 0
    '        dCol.Caption = "Over/Under Deduction Pay"

    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'dCol.DataType = System.Type.GetType("System.String")
    '        dCol.DataType = System.Type.GetType("System.Decimal")
    '        'Pinkal (14-Dec-2012) -- End


    '        dtFinalTable.Columns.Add(dCol)

    '        '*** [Non Earning Deduction Columns]
    '        StrQ = "SELECT " & _
    '                  "ISNULL(prtranhead_master.trnheadname,'') As Tname  " & _
    '                  ",ISNULL(prtranhead_master.tranheadunkid,'') As tranheadunkid " & _
    '                  "FROM prtranhead_master " & _
    '              "WHERE   ISNULL(prtranhead_master.isvoid, 0) = 0  "
    '        If mstrNonEarningDeductionTranHeadIds.Length > 0 Then

    '            'S.SANDEEP [ 11 SEP 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If mstrCurrency_Sign.Trim.Length > 0 Then
    '                'Sohail (18 Jun 2013) -- Start
    '                'TRA - ENHANCEMENT - Now Informational heads when currency is selected convert amount in to currency if it is ticked as Monetary Heads on Transaction Head Add/Edit Screen
    '                'StrQ &= " AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
    '                '    " AND prtranhead_master.tranheadunkid IN (" & mstrNonEarningDeductionTranHeadIds & ") AND prtranhead_master.trnheadtype_id <> " & enTranHeadType.Informational & _
    '                '    " ORDER BY prtranhead_master.tranheadunkid "
    '                StrQ &= " AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
    '                    " AND prtranhead_master.tranheadunkid IN (" & mstrNonEarningDeductionTranHeadIds & ") " & _
    '                    " ORDER BY prtranhead_master.tranheadunkid "
    '                'Sohail (18 Jun 2013) -- End
    '            Else
    '                StrQ &= " AND prtranhead_master.trnheadtype_id IN ( " & enTranHeadType.EmployersStatutoryContributions & ", " & enTranHeadType.Informational & " ) " & _
    '                        " AND prtranhead_master.tranheadunkid IN (" & mstrNonEarningDeductionTranHeadIds & ")" & _
    '                        " ORDER BY prtranhead_master.tranheadunkid "
    '            End If
    '            'S.SANDEEP [ 11 SEP 2012 ] -- END

    '        Else
    '            'StrQ &= "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " ORDER BY prtranhead_master.tranheadunkid "
    '            StrQ &= "AND 1 = 2 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        blnAddCol = False
    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            For Each dtRow As DataRow In dsList.Tables("List").Rows
    '                If mblnIgnorezeroHeads = True Then
    '                    dRow = dsData.Tables("List").Select("TranId = " & CInt(dtRow.Item("tranheadunkid")) & " ")
    '                    If dRow.Length > 0 Then
    '                        blnAddCol = True
    '                    End If
    '                Else
    '                    blnAddCol = True
    '                End If
    '                If blnAddCol = True Then
    '                    dCol = New DataColumn("Column" & dtRow.Item("tranheadunkid"))

    '                    'Pinkal (14-Dec-2012) -- Start
    '                    'Enhancement : TRA Changes
    '                    dCol.DataType = System.Type.GetType("System.Decimal")
    '                    'Pinkal (14-Dec-2012) -- End

    '                    dCol.DefaultValue = 0
    '                    dCol.Caption = dtRow.Item("Tname")
    '                    dtFinalTable.Columns.Add(dCol)
    '                    mintTotNonEarningDeductionCols += 1
    '                End If
    '                blnAddCol = False
    '            Next
    '        End If

    '        If mintViewIndex > 0 Then
    '            dCol = New DataColumn("Id")
    '            dCol.DefaultValue = 0

    '            'Pinkal (14-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            'dCol.Caption = "Over/Under Deduction Pay"
    '            'Pinkal (14-Dec-2012) -- End


    '            dCol.DataType = System.Type.GetType("System.String")


    '            dtFinalTable.Columns.Add(dCol)

    '            dCol = New DataColumn("GName")
    '            dCol.DefaultValue = 0

    '            'Pinkal (14-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            'dCol.Caption = "Over/Under Deduction Pay"
    '            'Pinkal (14-Dec-2012) -- End


    '            dCol.DataType = System.Type.GetType("System.String")

    '            dtFinalTable.Columns.Add(dCol)

    '        End If

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim intPrevGrpID As Integer = 0
    '        Dim intPrevEmpID As Integer = 0
    '        For Each drRow As DataRow In dsData.Tables("List").Rows

    '            If (mintViewIndex > 0 AndAlso intPrevGrpID = 0 AndAlso intPrevGrpID <> CInt(drRow.Item("ID"))) OrElse intPrevEmpID = 0 OrElse intPrevEmpID <> CInt(drRow.Item("EmpID")) Then
    '                rpt_Row = dtFinalTable.NewRow

    '                rpt_Row.Item("EmpId") = drRow.Item("EmpID")
    '                rpt_Row.Item("EmpCode") = drRow.Item("Code")
    '                rpt_Row.Item("EmpName") = drRow.Item("EmpName")
    '                rpt_Row.Item("Openingbalance") = Format(CDec(drRow.Item("Openingbalance")), GUI.fmtCurrency)

    '                rpt_Row.Item("TGP") = "0"

    '                rpt_Row.Item("TDD") = "0"

    '                dtFinalTable.Rows.Add(rpt_Row)
    '            End If

    '            rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)

    '            Select Case CInt(drRow.Item("GroupID"))
    '                Case enViewPayroll_Group.Earning
    '                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                Case enViewPayroll_Group.Deduction
    '                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                Case enViewPayroll_Group.EmployerContribution
    '                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                Case enViewPayroll_Group.Informational
    '                    'Sohail (18 Jun 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'If mstrCurrency_Sign.Trim.Length <= 0 Then
    '                    '    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    'End If
    '                    rpt_Row.Item("Column" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    'Sohail (18 Jun 2013) -- End
    '                Case enViewPayroll_Group.Loan
    '                    rpt_Row.Item("Loan") = Format(CDec(rpt_Row.Item("Loan")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                Case enViewPayroll_Group.Advance
    '                    rpt_Row.Item("Advance") = Format(CDec(rpt_Row.Item("Advance")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                Case enViewPayroll_Group.Saving
    '                    rpt_Row.Item("Saving") = Format(CDec(rpt_Row.Item("Saving")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)

    '                    'Pinkal (03-Jul-2013) -- Start
    '                    'Enhancement : TRA Changes

    '                Case enViewPayroll_Group.PayPerActivity
    '                    objActivity._Activityunkid = CInt(drRow.Item("TranId"))
    '                    If objActivity._TranheadTypeId = enTranHeadType.EarningForEmployees AndAlso (mintModeId = 1 OrElse mintModeId = 3) Then
    '                        rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                        rpt_Row.Item("TGP") = Format(CDec(rpt_Row.Item("TGP")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    ElseIf objActivity._TranheadTypeId = enTranHeadType.DeductionForEmployee AndAlso (mintModeId = 2 OrElse mintModeId = 3) Then
    '                        rpt_Row.Item("ColumnPPA" & drRow.Item("TranId")) = Format(CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                        rpt_Row.Item("TDD") = Format(CDec(rpt_Row.Item("TDD")) + CDec(drRow.Item("Amount")), GUI.fmtCurrency)
    '                    End If

    '                    'Pinkal (03-Jul-2013) -- End

    '            End Select

    '            rpt_Row.Item("NetPay") = Format(CDec(rpt_Row("TGP")) - CDec(rpt_Row("TDD")), GUI.fmtCurrency)
    '            rpt_Row.Item("NetPay") = Format(Rounding.BRound(CDec(rpt_Row.Item("NetPay")), ConfigParameter._Object._RoundOff_Type), GUI.fmtCurrency)

    '            rpt_Row.Item("TotNetPay") = Format(CDec(rpt_Row.Item("NetPay")) + CDec(rpt_Row.Item("Openingbalance")), GUI.fmtCurrency)

    '            If CDec(rpt_Row.Item("TotNetPay")) < 0 Then

    '                'Pinkal (14-Dec-2012) -- Start
    '                'Enhancement : TRA Changes
    '                'rpt_Row.Item("OUDPay") = IIf(CDec(rpt_Row.Item("TotNetPay")) < 0, "(" & Format(System.Math.Abs(CDec(rpt_Row.Item("TotNetPay"))), GUI.fmtCurrency) & ")", 0)
    '                rpt_Row.Item("OUDPay") = IIf(CDec(rpt_Row.Item("TotNetPay")) < 0, Format(CDec(rpt_Row.Item("TotNetPay")), GUI.fmtCurrency), 0)
    '                'Pinkal (14-Dec-2012) -- End


    '                rpt_Row.Item("TotNetPay") = Format(CDec(0), GUI.fmtCurrency)
    '            Else
    '                rpt_Row.Item("OUDPay") = Format(CDec(0), GUI.fmtCurrency)
    '            End If

    '            If mintViewIndex > 0 Then
    '                rpt_Row.Item("ID") = CInt(drRow.Item("ID"))
    '                rpt_Row.Item("GName") = drRow.Item("GName").ToString
    '                'Sohail (08 Mar 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                If mintViewIndex = enAnalysisReport.CostCenter Then
    '                    rpt_Row.Item("GCode") = drRow.Item("GCode").ToString
    '                End If
    '                'Sohail (08 Mar 2013) -- End
    '                intPrevGrpID = CInt(drRow.Item("ID"))
    '            End If
    '            intPrevEmpID = CInt(drRow.Item("EmpID"))
    '        Next

    '        dtFinalTable.AcceptChanges()


    '        Dim dblTotal As Decimal = 0
    '        Dim m As Integer = 3
    '        Dim n As Integer = 3
    '        'Sohail (08 Mar 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mintViewIndex > 0 AndAlso mintViewIndex = enAnalysisReport.CostCenter Then
    '            m = 4
    '            n = 4
    '        End If
    '        'Sohail (08 Mar 2013) -- End
    '        dblColTot = New Decimal(dtFinalTable.Columns.Count) {}
    '        Dim c As Integer
    '        If mintViewIndex > 0 Then
    '            c = dtFinalTable.Columns.Count - 2
    '        Else
    '            c = dtFinalTable.Columns.Count
    '        End If

    '        While m < c
    '            For t As Integer = 0 To dtFinalTable.Rows.Count - 1
    '                dblTotal += CDec(dtFinalTable.Rows(t)(m))
    '            Next
    '            dblColTot(n) = Format(CDec(dblTotal), GUI.fmtCurrency)
    '            dblTotal = 0
    '            m += 1
    '            n += 1
    '        End While

    '        Call FilterTitleAndFilterQuery()



    '        'Pinkal (14-Dec-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'If Export_to_Excel_New(mstrReportTypeName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), ConfigParameter._Object._ExportReportPath, dtFinalTable) Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported to Report export path"), enMsgBoxStyle.Information)
    '        '    Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, StrFinalPath)
    '        'End If

    '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 5, "Grand Total :")
    '        Dim strSubTotal As String = ""
    '        Dim objDic As New Dictionary(Of Integer, Object)
    '        Dim strarrGroupColumns As String() = Nothing
    '        Dim rowsArrayHeader As New ArrayList
    '        Dim row As WorksheetRow
    '        Dim wcell As WorksheetCell

    '        If mblnIgnorezeroHeads Then
    '            mdtTableExcel = IgnoreZeroHead(dtFinalTable)
    '        Else
    '            mdtTableExcel = dtFinalTable
    '        End If


    '        'START TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 

    '        mdtTableExcel.Columns.RemoveAt(0)
    '        If mintViewIndex > 0 Then
    '            mdtTableExcel.Columns.Remove("ID")
    '            mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
    '            Dim strGrpCols As String() = {"GName"}
    '            strarrGroupColumns = strGrpCols
    '            strSubTotal = Language.getMessage(mstrModuleName, 6, "Sub Total :")
    '        End If

    '        'END TO REMOVE PERIODID,GROUPID,GRPNAME COLUMN FROM THE "mdtTableExcel" 



    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Period :") & mstrPeriodName, "s10bw")
    '        row.Cells.Add(wcell)
    '        'Sohail (18 Jun 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If mstrCurrency_Sign.Trim <> "" AndAlso mstrCurrency_Rate.Trim <> "" Then
    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Currency Rate :") & " " & mstrCurrency_Rate, "s10bw")
    '            wcell.MergeAcross = 1
    '            row.Cells.Add(wcell)
    '        End If
    '        'Sohail (18 Jun 2013) -- End
    '        rowsArrayHeader.Add(row)



    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "s10bw")
    '        row.Cells.Add(wcell)
    '        rowsArrayHeader.Add(row)

    '        row = New WorksheetRow()
    '        wcell = New WorksheetCell("", "HeaderStyle")
    '        row.Cells.Add(wcell)

    '        wcell = New WorksheetCell("", "HeaderStyle")
    '        row.Cells.Add(wcell)

    '        If mintTotEarningCols > 0 AndAlso mintTotDeductionCols <= 0 Then

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
    '            row.Cells.Add(wcell)
    '            wcell.MergeAcross = mintTotEarningCols - 1

    '            wcell = New WorksheetCell("", "HeaderStyle")
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
    '            row.Cells.Add(wcell)

    '            Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 3 - mintTotEarningCols - 2)

    '            For i As Integer = 0 To mintRow
    '                wcell = New WorksheetCell("", "HeaderStyle")
    '                row.Cells.Add(wcell)
    '            Next

    '        ElseIf mintTotDeductionCols > 0 AndAlso mintTotEarningCols <= 0 Then

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
    '            row.Cells.Add(wcell)
    '            wcell.MergeAcross = mintTotDeductionCols - 1

    '            Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 3 - mintTotDeductionCols - 1)

    '            For i As Integer = 0 To mintRow
    '                wcell = New WorksheetCell("", "HeaderStyle")
    '                row.Cells.Add(wcell)
    '            Next

    '        ElseIf mintTotEarningCols > 0 AndAlso mintTotDeductionCols > 0 Then

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 11, "Earning"), "HeaderStyle")
    '            row.Cells.Add(wcell)
    '            wcell.MergeAcross = mintTotEarningCols - 1

    '            wcell = New WorksheetCell("", "HeaderStyle")
    '            row.Cells.Add(wcell)

    '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Deduction"), "HeaderStyle")
    '            row.Cells.Add(wcell)
    '            wcell.MergeAcross = mintTotDeductionCols - 1

    '            Dim mintRow As Integer = (mdtTableExcel.Columns.Count - 2 - mintTotEarningCols - mintTotDeductionCols - 2)

    '            For i As Integer = 0 To mintRow
    '                wcell = New WorksheetCell("", "HeaderStyle")
    '                row.Cells.Add(wcell)
    '            Next

    '        End If

    '        'wcell = New WorksheetCell("", "HeaderStyle")
    '        'row.Cells.Add(wcell)
    '        'wcell.MergeAcross = (mdtTableExcel.Columns.Count - 3 - (IIf(mintTotEarningCols <= 0, 1, mintTotEarningCols) + IIf(mintTotDeductionCols <= 0, 1, mintTotDeductionCols)))

    '        rowsArrayHeader.Add(row)


    '        'SET EXCEL CELL WIDTH
    '        Dim intArrayColumnWidth As Integer() = Nothing
    '        ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
    '        For i As Integer = 0 To intArrayColumnWidth.Length - 1
    '            intArrayColumnWidth(i) = 125
    '        Next
    '        'SET EXCEL CELL WIDTH

    '        'Sohail (18 Jun 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName & " " & mstrReportTypeName, "", " ", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
    '        Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, Me._ReportName & " " & mstrReportTypeName & "(" & mstrCurrency_Sign & ")", "", " ", Nothing, strGTotal, True, rowsArrayHeader, Nothing, Nothing)
    '        'Sohail (18 Jun 2013) -- End


    '        'Pinkal (14-Dec-2012) -- End

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_EDSpreadsheetReport_New; Module Name: " & mstrModuleName)
    '    End Try
    'End Sub
#End Region

    'Messages
    '1, "Employee : "
    '2, "Period : "
    '3, "Prepared By :"
    '4, "Date :"
    '5, "Grand Total :"
    '6, "Sub Total :"
    '7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."
    '8, "Report successfully exported to Report export path"
    '9, "Department :"


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Period :")
            Language.setMessage(mstrModuleName, 3, "Prepared By :")
            Language.setMessage(mstrModuleName, 4, "Date :")
            Language.setMessage(mstrModuleName, 5, "Grand Total :")
            Language.setMessage(mstrModuleName, 6, "Sub Total :")
            Language.setMessage(mstrModuleName, 7, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 9, "Department :")
            Language.setMessage(mstrModuleName, 10, "Branch :")
            Language.setMessage(mstrModuleName, 11, "Earning")
            Language.setMessage(mstrModuleName, 12, "Deduction")
            Language.setMessage(mstrModuleName, 13, "Bank :")
            Language.setMessage(mstrModuleName, 14, "Branch :")
            Language.setMessage(mstrModuleName, 15, "Account # :")
            Language.setMessage(mstrModuleName, 16, "Employee Code")
            Language.setMessage(mstrModuleName, 17, "Employee Name")
            Language.setMessage(mstrModuleName, 18, "Bank")
            Language.setMessage(mstrModuleName, 19, "Account #")
            Language.setMessage(mstrModuleName, 20, "Payment Detail")
            Language.setMessage(mstrModuleName, 21, "Currency :")
            Language.setMessage(mstrModuleName, 22, "Exchange Rate:")
            Language.setMessage(mstrModuleName, 23, "Currency Rate :")
            Language.setMessage(mstrModuleName, 24, "Cash Payment")
            Language.setMessage(mstrModuleName, 25, "Salary On Hold")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

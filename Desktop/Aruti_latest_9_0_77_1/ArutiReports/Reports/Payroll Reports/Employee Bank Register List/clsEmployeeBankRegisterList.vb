Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsEmployeeBankRegisterList
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeBankRegisterListReport"
    Private mstrReportId As String = enArutiReport.EmployeeBankRegisterList
    Dim objDataOperation As clsDataOperation

#Region "Constructor"
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region "Private Variables"
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintEmployeeUnkId As Integer = 0
    Private mstrEmployeeName As String = ""


    'Vimal (27 Nov 2010) -- Start 
    'Private mintEmpId As Integer = 0
    'Private mstrEmployeeCode As String = ""
    'Vimal (27 Nov 2010) -- End

    Private mintBankId As Integer = 0
    Private mstrBankName As String = ""

    Private mintBranchId As Integer = 0
    Private mstrBranchName As String = ""

    'Sohail (03 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    'Private mintDeptId As Integer = 0
    'Private mstrDeptName As String = ""
    'Sohail (03 Aug 2013) -- End

    Private mstrAccountNo As String = ""

    Private mstrOrderByQuery As String = ""


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END


#End Region

#Region "Properties"

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    'Vimal (27 Nov 2010) -- Start 
    'Public WriteOnly Property _EmpId() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmpId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmployeeCode() As String
    '    Set(ByVal value As String)
    '        mstrEmployeeCode = value
    '    End Set
    'End Property
    'Vimal (27 Nov 2010) -- End

    Public WriteOnly Property _BankId() As Integer
        Set(ByVal value As Integer)
            mintBankId = value
        End Set
    End Property

    Public WriteOnly Property _BankName() As String
        Set(ByVal value As String)
            mstrBankName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _AccountNo() As String
        Set(ByVal value As String)
            mstrAccountNo = value
        End Set
    End Property

    'Sohail (03 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    'Public WriteOnly Property _DeptId() As Integer
    '    Set(ByVal value As Integer)
    '        mintDeptId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _DeptName() As String
    '    Set(ByVal value As String)
    '        mstrDeptName = value
    '    End Set
    'End Property
    'Sohail (03 Aug 2013) -- End

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End


    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region


#Region "Public Function & Procedures"

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintEmployeeUnkId = 0
            mstrEmployeeName = ""

            'Vimal (27 Nov 2010) -- Start 
            'mintEmpId = 0
            'mstrEmployeeCode = ""
            'Vimal (27 Nov 2010) -- End

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'mintDeptId = 0
            'mstrDeptName = ""
            'Sohail (03 Aug 2013) -- End

            mintBankId = 0
            mstrBankName = ""

            mintBranchId = 0
            mstrBranchName = ""

            mstrAccountNo = ""

            mstrOrderByQuery = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End



        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Me._FilterQuery = ""
        Try
            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@mintEmployeeUnkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= "AND hremployee_master.employeeunkid = @mintEmployeeUnkId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, " Employee : ") & " " & mstrEmployeeName & " "
            End If

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If mintDeptId > 0 Then
            '    objDataOperation.AddParameter("@mintDeptId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptId)
            '    Me._FilterQuery &= "AND hrdepartment_master.departmentunkid = @mintDeptId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, " Department : ") & " " & mstrDeptName & " "
            'End If
            'Sohail (03 Aug 2013) -- End

            If mintBankId > 0 Then
                objDataOperation.AddParameter("@mintBankId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankId)
                Me._FilterQuery &= "AND hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = @mintBankId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, " Bank : ") & " " & mstrBankName & " "
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@mintBranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                Me._FilterQuery &= "AND hrmsConfiguration..cfbankbranch_master.branchunkid = @mintBranchId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, " Branch : ") & " " & mstrBranchName & " "
            End If

            If mstrAccountNo <> "" Then
                objDataOperation.AddParameter("@mstrAccoNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrAccountNo & "%")
                Me._FilterQuery &= "AND premployee_bank_tran.accountno LIKE @mstrAccoNo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Account No : ") & " " & mstrAccountNo & " "
            End If

            If Me.OrderByQuery <> "" Then
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                'mstrOrderByQuery &= "ORDER BY " & Me.OrderByQuery
                mstrOrderByQuery &= "ORDER BY end_date DESC, " & Me.OrderByQuery
                'Sohail (25 Apr 2014) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order by : ") & " " & Me.OrderByDisplay
                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
            Else
                mstrOrderByQuery &= "ORDER BY end_date DESC "
                'Sohail (25 Apr 2014) -- End
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit

        Try
            objConfig._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objUser._Username)

            If Not IsNothing(objRpt) Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
                'Sohail (21 Aug 2015) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Report Generation"
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 1, "Emp Code")))
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 2, "Emp Name")))
            'iColumn_DetailReport.Add(New IColumn("DeptName", Language.getMessage(mstrModuleName, 3, "Dept Name"))) 'Sohail (03 Aug 2013)
            iColumn_DetailReport.Add(New IColumn("BankName", Language.getMessage(mstrModuleName, 4, "Bank Name")))
            iColumn_DetailReport.Add(New IColumn("BranchName", Language.getMessage(mstrModuleName, 5, "Branch Name")))
            iColumn_DetailReport.Add(New IColumn("AccountNo", Language.getMessage(mstrModuleName, 6, "AccountNo")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xYearUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strFmtCurrency As String _
                                          , ByVal strUsername As String _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, strUsername]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            StrQ = "SELECT  hrdepartment_master.name AS DeptName " & _
                          ", hremployee_master.employeecode AS EmpCode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'hremployee_master.surname+' ' + hremployee_master.firstname AS empname
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EmpName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EmpName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankName " & _
                          ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                          ", premployee_bank_tran.accountno AS AccountNo " & _
                          ", hremployee_master.employeeunkid  " & _
                          ", hrdepartment_master.departmentunkid  " & _
                          ", hrmsConfiguration..cfbankbranch_master.branchunkid  " & _
                          ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid  " & _
                          ", premployee_bank_tran.percentage " & _
                          ", ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
                          ", ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
                          ", ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
                          ", ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
                          ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
                          ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
                    "FROM    premployee_bank_tran " & _
                            "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "           LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         stationunkid " & _
                               "        ,deptgroupunkid " & _
                               "        ,departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,sectionunkid " & _
                               "        ,unitgroupunkid " & _
                               "        ,unitunkid " & _
                               "        ,teamunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "        LEFT JOIN hrdepartment_master ON T.departmentunkid = hrdepartment_master.departmentunkid " & _
                            "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                            "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   ISNULL(premployee_bank_tran.isvoid,0) = 0 "
            'Sohail (25 Apr 2014) - [modeid, amount, priority, periodunkid, period_name, start_date, end_date]
            'Sohail (03 Aug 2013) - [percentage]


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    'Sohail (16 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.isactive = 1 "
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'Sohail (16 Apr 2012) -- End
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-Jun-2011) -- End

            'Sohail (28 Jun 2011) -- Start
            'Issue : According to prvilege that lower level user should not see superior level employees.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    'Sohail (17 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'Sohail (17 Apr 2012) -- End
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (28 Jun 2011) -- End
            StrQ &= "AND hrdepartment_master.isactive = 1 " & _
                            "AND hrmsConfiguration..cfbankbranch_master.isactive = 1 " & _
                            "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("EmpCode")
                'Sohail (03 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'rpt_Rows.Item("Column2") = dtRow.Item("EmpName")
                'rpt_Rows.Item("Column3") = dtRow.Item("DeptName")
                'rpt_Rows.Item("Column4") = dtRow.Item("BankName")
                'rpt_Rows.Item("Column5") = dtRow.Item("BranchName")
                rpt_Rows.Item("Column3") = dtRow.Item("BankName")
                rpt_Rows.Item("Column2") = dtRow.Item("BranchName")
                rpt_Rows.Item("Column4") = dtRow.Item("EmpName")
                rpt_Rows.Item("Column5") = dtRow.Item("DeptName")
                rpt_Rows.Item("Column7") = Format(CDec(dtRow.Item("percentage")), "00.00")
                'Sohail (03 Aug 2013) -- End
                rpt_Rows.Item("Column6") = dtRow.Item("AccountNo")

                'Sohail (25 Apr 2014) -- Start
                'Enhancement - Employee Bank Details Period Wise.
                rpt_Rows.Item("Column8") = dtRow.Item("periodunkid")
                rpt_Rows.Item("Column9") = dtRow.Item("period_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("amount")), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                'Sohail (25 Apr 2014) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeBankRegisterList

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 21, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUsername)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 22, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 23, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 24, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 

            objRpt.SetDataSource(rpt_Data)

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "Code :"))
            'Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 8, "Employee :"))
            'Call ReportFunction.TextChange(objRpt, "txtDeptName", Language.getMessage(mstrModuleName, 9, "Department :"))
            'Call ReportFunction.TextChange(objRpt, "txtBankName", Language.getMessage(mstrModuleName, 4, "Bank Name"))
            'Call ReportFunction.TextChange(objRpt, "txtBranchName", Language.getMessage(mstrModuleName, 5, "Branch Name"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 7, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 8, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtDeptName", Language.getMessage(mstrModuleName, 9, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtBankName", Language.getMessage(mstrModuleName, 25, "Bank Name :"))
            Call ReportFunction.TextChange(objRpt, "txtBranchName", Language.getMessage(mstrModuleName, 26, "Branch Name :"))
            'Sohail (03 Aug 2013) -- End
            Call ReportFunction.TextChange(objRpt, "txtAccountNo", Language.getMessage(mstrModuleName, 27, "Account No"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 10, "Grand Total :"))
            Call ReportFunction.TextChange(objRpt, "txtPercentage", Language.getMessage(mstrModuleName, 28, "Percentage (%)"))
            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 11, "Group Total :"))
            Call ReportFunction.TextChange(objRpt, "txtBankTotal", Language.getMessage(mstrModuleName, 11, "Bank Total :"))
            Call ReportFunction.TextChange(objRpt, "txtBranchTotal", Language.getMessage(mstrModuleName, 29, "Branch Total :"))
            'Sohail (03 Aug 2013) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 30, "As On Period"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 31, "Amount"))
            'Sohail (25 Apr 2014) -- End

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


#End Region

    'Messages
    '1, "Emp Code"
    '2, "Emp Name"
    '3, "Dept Name"
    '4, "Bank Name"
    '5, "Branch Name"
    '6, "AccountNo"
    '7, "Code :"
    '8, "Employee :"
    '9, "Department :"
    '10, "Grand Total :"
    '11, "Group Total :"
    '12, "Printed By :"
    '13, "Printed Date :"
    '14, "Page :"
    '15, " Employee : "
    '16, " Department : "
    '17, " Bank : "
    '18, " Branch : "
    '19, " Account No : "
    '20, " Order by : "

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Emp Code")
            Language.setMessage(mstrModuleName, 2, "Emp Name")
            Language.setMessage(mstrModuleName, 4, "Bank Name")
            Language.setMessage(mstrModuleName, 5, "Branch Name")
            Language.setMessage(mstrModuleName, 6, "Account No")
            Language.setMessage(mstrModuleName, 7, "Code")
            Language.setMessage(mstrModuleName, 8, "Employee")
            Language.setMessage(mstrModuleName, 9, "Department")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")
            Language.setMessage(mstrModuleName, 11, "Bank Total :")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Page :")
            Language.setMessage(mstrModuleName, 15, " Employee :")
            Language.setMessage(mstrModuleName, 17, " Bank :")
            Language.setMessage(mstrModuleName, 18, " Branch :")
            Language.setMessage(mstrModuleName, 19, " Account No :")
            Language.setMessage(mstrModuleName, 20, " Order by :")
            Language.setMessage(mstrModuleName, 21, "Prepared By :")
            Language.setMessage(mstrModuleName, 22, "Checked By :")
            Language.setMessage(mstrModuleName, 23, "Approved By :")
            Language.setMessage(mstrModuleName, 24, "Received By :")
            Language.setMessage(mstrModuleName, 25, "Bank Name :")
            Language.setMessage(mstrModuleName, 26, "Branch Name :")
            Language.setMessage(mstrModuleName, 27, "Account No")
            Language.setMessage(mstrModuleName, 28, "Percentage (%)")
            Language.setMessage(mstrModuleName, 29, "Branch Total :")
            Language.setMessage(mstrModuleName, 30, "As On Period")
            Language.setMessage(mstrModuleName, 31, "Amount")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

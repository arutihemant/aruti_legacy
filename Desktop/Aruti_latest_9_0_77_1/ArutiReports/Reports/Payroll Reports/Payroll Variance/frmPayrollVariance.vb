#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayrollVariance

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayrollVariance"
    Private objReconciliation As clsPayrollVariance

    'Sohail (20 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    'Sohail (20 Jul 2012) -- End


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (09-Jan-2013) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End
    Private mintFirstOpenPeriod As Integer = 0 'Sohail (17 Sep 2014)
#End Region

#Region " Constructor "

    Public Sub New()
        objReconciliation = New clsPayrollVariance(User._Object._Languageunkid,Company._Object._Companyunkid)
        objReconciliation.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim objBranch As New clsStation
            Dim dsList As New DataSet
            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objMaster As New clsMasterData
            Dim dtTable As DataTable
            'Sohail (12 Jul 2012) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End
            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (17 Sep 2014)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (17 Sep 2014)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (20 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Allow Payroll Variance Report to Closed FY
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (20 Jul 2012) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (17 Sep 2014) -- End
            End With

            With cboPeriodTo
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                'Sohail (17 Sep 2014) -- Start
                'Enhancement - System generated Base, OT and PH heads for PPA Activity.
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (17 Sep 2014) -- End
            End With
            objperiod = Nothing

            dsList = objBranch.getComboList("Branch", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            dsList = Nothing

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            dsList = objMaster.getComboListForHeadType("HeadType")
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'dtTable = New DataView(dsList.Tables("HeadType"), " ID IN (0, " & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ", "", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dsList.Tables("HeadType"), "", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (23 Feb 2017) -- End
            With cboTrnHeadType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            'Sohail (12 Jul 2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'Sohail (15 Apr 2014) -- Start
            'Enhancement - Sorting on Payroll Variance Report
            objReconciliation.setDefaultOrderBy(0)
            txtOrderBy.Text = objReconciliation.OrderByDisplay
            'Sohail (15 Apr 2014) -- End
            cboEmployee.SelectedValue = 0
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'cboPeriod.SelectedValue = 0
            'cboPeriodTo.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboPeriodTo.SelectedValue = mintFirstOpenPeriod
            If cboPeriod.SelectedIndex > 0 Then cboPeriod.SelectedIndex = cboPeriodTo.SelectedIndex - 1
            'Sohail (17 Sep 2014) -- End
            cboBranch.SelectedValue = 0


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = True
            'Pinkal (24-Jun-2011) -- End
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            chkShowEmployerContribution.Checked = False
            chkShowInformationalHeads.Checked = False
            'Sohail (23 Feb 2017) -- End
            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            cboTrnHeadType.SelectedValue = 0
            chkIgnoreZeroVariance.Checked = True
            'Sohail (12 Jul 2012) -- End

            cboTranHead.SelectedValue = 0 'Sohail (20 Jul 2012)


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Pinkal (09-Jan-2013) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPayollReconciliationReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objReconciliation = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayollReconciliationReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayollReconciliationReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            eZeeHeader.Title = objReconciliation._ReportName
            eZeeHeader.Message = objReconciliation._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayollReconciliationReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (20 Jul 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboPeriodTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodTo.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriodTo.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriodTo.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriodTo_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , , , , , , , True)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , , , , , , True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (17 Sep 2014) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("TranHead")
                .SelectedValue = 0
            End With

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            chkShowEmployerContribution.Enabled = True
            chkShowInformationalHeads.Enabled = True
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EarningForEmployees OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.DeductionForEmployee OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EmployeesStatutoryDeductions Then
                chkShowEmployerContribution.Checked = False
                chkShowEmployerContribution.Enabled = False

                chkShowInformationalHeads.Checked = False
                chkShowInformationalHeads.Enabled = False

            ElseIf CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EmployersStatutoryContributions Then
                chkShowInformationalHeads.Checked = False
                chkShowInformationalHeads.Enabled = False

                chkShowEmployerContribution.Checked = True
                chkShowEmployerContribution.Enabled = False
            ElseIf CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                chkShowEmployerContribution.Checked = False
                chkShowEmployerContribution.Enabled = False

                chkShowInformationalHeads.Checked = True
                chkShowInformationalHeads.Enabled = False
            End If
            'Sohail (23 Feb 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTrnHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (20 Jul 2012) -- End

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim dtPeriodStart As Date
        Dim dtPeriodEnd As Date
        'Sohail (21 Aug 2015) -- End
        Try
            If cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If

            If cboPeriodTo.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriodTo.Focus()
                Exit Sub
            End If

            If cboPeriodTo.SelectedIndex <= cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, " To Period cannot be less than or equal to Form Period"), enMsgBoxStyle.Information)
                cboPeriodTo.Focus()
                Exit Sub
            End If

            'If cboBranch.SelectedValue <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Branch is compulsory infomation. Please select Branch to continue."), enMsgBoxStyle.Information)
            '    cboBranch.Focus()
            '    Exit Sub
            'End If

            objReconciliation.SetDefaultValue()

            objReconciliation._EmpId = cboEmployee.SelectedValue
            objReconciliation._EmpName = cboEmployee.Text

            objReconciliation._FromPeriodId = cboPeriod.SelectedValue
            objReconciliation._FromPeriodName = cboPeriod.Text

            objReconciliation._NextPeriodId = cboPeriodTo.SelectedValue
            objReconciliation._NextPeriodName = cboPeriodTo.Text

            objReconciliation._BranchId = cboBranch.SelectedValue
            objReconciliation._BranchName = cboBranch.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objReconciliation._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            'Sohail (21 Aug 2015) -- End
            objReconciliation._PeriodStartDate = objPeriod._Start_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriodTo.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriodTo.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date 'Nilay (10-Feb-2016) -- objPeriod._Start_Date
            'Sohail (21 Aug 2015) -- End
            objReconciliation._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (03 Aug 2019)
            'Sohail (17 Apr 2012) -- End

            'Sohail (12 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objReconciliation._TranHeadTypeId = cboTrnHeadType.SelectedValue
            objReconciliation._TranHeadTypeName = cboTrnHeadType.Text
            objReconciliation._IgnoreZeroVariance = chkIgnoreZeroVariance.Checked
            'Sohail (12 Jul 2012) -- End

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            objReconciliation._ShowEmployerContribution = chkShowEmployerContribution.Checked
            objReconciliation._ShowInformationalHeads = chkShowInformationalHeads.Checked
            'Sohail (23 Feb 2017) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            objReconciliation._ShowVariancePercentageColumns = chkShowVariancePercentageColumns.Checked
            'Hemant (22 Jan 2019) -- End

            'Sohail (20 Jul 2012) -- Start
            'TRA - ENHANCEMENT
            objReconciliation._TranHeadUnkId = CInt(cboTranHead.SelectedValue)
            objReconciliation._FromDatabaseName = mstrFromDatabaseName
            objReconciliation._ToDatabaseName = mstrToDatabaseName
            'Sohail (20 Jul 2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            objReconciliation._FromYearUnkId = CInt(CType(cboPeriod.SelectedItem, DataRowView).Item("yearunkid"))
            objReconciliation._ToYearUnkId = CInt(CType(cboPeriodTo.SelectedItem, DataRowView).Item("yearunkid"))
            'Sohail (21 Aug 2015) -- End

            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            objReconciliation._ViewByIds = mstrStringIds
            objReconciliation._ViewIndex = mintViewIdx
            objReconciliation._ViewByName = mstrStringName
            objReconciliation._Analysis_Fields = mstrAnalysis_Fields
            objReconciliation._Analysis_Join = mstrAnalysis_Join
            objReconciliation._Analysis_OrderBy = mstrAnalysis_OrderBy
            objReconciliation._Report_GroupName = mstrReport_GroupName

            'Pinkal (09-Jan-2013) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objReconciliation._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (03 Aug 2019) -- End


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objReconciliation._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objReconciliation.Export_Recociliaton_Report()
            objReconciliation.Export_Recociliaton_Report(User._Object._Userunkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            'Sohail (21 Aug 2015) -- End

            objPeriod = Nothing 'Sohail (03 Aug 2019)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End

    'Sohail (20 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsTransactionHead
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With cboTranHead
                objfrm.DataSource = CType(cboTranHead.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (20 Jul 2012) -- End


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayrollVariance.SetMessages()
            objfrm._Other_ModuleNames = "clsPayrollVariance"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- End

    'Sohail (15 Apr 2014) -- Start
    'Enhancement - Sorting on Payroll Variance Report
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objReconciliation.setOrderBy(0)
            txtOrderBy.Text = objReconciliation.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Apr 2014) -- End

#End Region


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes

#Region "LinkButton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try

    End Sub

#End Region

    'Pinkal (09-Jan-2013) -- End


  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriodFrom.Text = Language._Object.getCaption(Me.lblPeriodFrom.Name, Me.lblPeriodFrom.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPeriodTo.Text = Language._Object.getCaption(Me.lblPeriodTo.Name, Me.lblPeriodTo.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.Name, Me.lblTrnHeadType.Text)
			Me.chkIgnoreZeroVariance.Text = Language._Object.getCaption(Me.chkIgnoreZeroVariance.Name, Me.chkIgnoreZeroVariance.Text)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkShowInformationalHeads.Text = Language._Object.getCaption(Me.chkShowInformationalHeads.Name, Me.chkShowInformationalHeads.Text)
			Me.chkShowEmployerContribution.Text = Language._Object.getCaption(Me.chkShowEmployerContribution.Name, Me.chkShowEmployerContribution.Text)
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            Me.chkShowVariancePercentageColumns.Text = Language._Object.getCaption(Me.chkShowVariancePercentageColumns.Name, Me.chkShowVariancePercentageColumns.Text)
            'Hemant (22 Jan 2019) -- End

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue.")
			Language.setMessage(mstrModuleName, 2, " To Period cannot be less than or equal to Form Period")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

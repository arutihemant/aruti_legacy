﻿Option Strict On
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region
Public Class frmTrainingStatusSummaryReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTrainingStatusSummaryReport"
    Private objTrainingStatusSummaryReport As clsTrainingStatusSummaryReport
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objTrainingStatusSummaryReport = New clsTrainingStatusSummaryReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objTrainingStatusSummaryReport.SetDefaultValue()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DisplayMember = "name"
                .ValueMember = "calendarunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With


            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            cboTrainingStatus.Items.Clear()
            With cboTrainingStatus
                .Items.Add(Language.getMessage("clsTrainingStatusSummaryReport", 1, "Select"))
                .Items.Add(Language.getMessage("clsTrainingStatusSummaryReport", 2, "Enrolled"))
                .Items.Add(Language.getMessage("clsTrainingStatusSummaryReport", 3, "Completed"))
                .Items.Add(Language.getMessage("clsTrainingStatusSummaryReport", 4, "Cancelled"))
                .Items.Add(Language.getMessage("clsTrainingStatusSummaryReport", 5, "Pending To Enroll"))
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objCalendar = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboTrainingStatus.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            chkInactiveemp.Checked = False
            chkShowTrainingStatusChart.Checked = False
            mstrAdvanceFilter = ""
            objTrainingStatusSummaryReport.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), enMsgBoxStyle.Information)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboTrainingName.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Training Name."), enMsgBoxStyle.Information)
                cboTrainingName.Focus()
                Exit Function
            End If

            objTrainingStatusSummaryReport.SetDefaultValue()

            objTrainingStatusSummaryReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingStatusSummaryReport._CalendarName = cboTrainingCalendar.Text

            objTrainingStatusSummaryReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingStatusSummaryReport._TrainingName = cboTrainingName.Text

            objTrainingStatusSummaryReport._TrainingStatusid = CInt(cboTrainingStatus.SelectedIndex)
            objTrainingStatusSummaryReport._TrainingStatusName = cboTrainingStatus.Text

            objTrainingStatusSummaryReport._IsActive = CBool(chkInactiveemp.Checked)

            objTrainingStatusSummaryReport._IsShowTrainingStatusChart = CBool(chkShowTrainingStatusChart.Checked)
            objTrainingStatusSummaryReport._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTrainingStatusSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTrainingStatusSummaryReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingStatusSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()

            Me._Title = objTrainingStatusSummaryReport._ReportName
            Me._Message = objTrainingStatusSummaryReport._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmTrainingStatusSummaryReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objTrainingStatusSummaryReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, CType(e.Type, enPrintAction), enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingStatusSummaryReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objTrainingStatusSummaryReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, _
                                          0, enPrintAction.None, CType(e.Type, enExportAction))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingStatusSummaryReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTrainingStatusSummaryReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTraining_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraining.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboTrainingName.DataSource, DataTable)
            frm.ValueMember = cboTrainingName.ValueMember
            frm.DisplayMember = cboTrainingName.DisplayMember
            frm.CodeMember = ""
            If frm.DisplayDialog Then
                cboTrainingName.SelectedValue = frm.SelectedValue
                cboTrainingName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTraining_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmTrainingStatusSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTrainingStatusSummaryReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTrainingStatusSummaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmTrainingStatusSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblTrainingStatus.Text = Language._Object.getCaption(Me.lblTrainingStatus.Name, Me.lblTrainingStatus.Text)
            Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.Name, Me.lblTrainingName.Text)
            Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.Name, Me.lblTrainingCalendar.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.chkShowTrainingStatusChart.Text = Language._Object.getCaption(Me.chkShowTrainingStatusChart.Name, Me.chkShowTrainingStatusChart.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Public Sub SetMessages()
        Try
            'Language.setMessage("clsTrainingStatusSummaryReport", 1, "Select")
            'Language.setMessage("clsTrainingStatusSummaryReport", 2, "Enrolled")
            'Language.setMessage("clsTrainingStatusSummaryReport", 3, "Completed")
            'Language.setMessage("clsTrainingStatusSummaryReport", 4, "Cancelled")
            'Language.setMessage("clsTrainingStatusSummaryReport", 5, "Pending To Enroll")
            Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
            Language.setMessage(mstrModuleName, 2, "Please Select Training Name.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
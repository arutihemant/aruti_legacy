﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingFeedbackFormReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lblEmployeeList = New System.Windows.Forms.Label
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.cboEvaluationCategory = New System.Windows.Forms.ComboBox
        Me.lblEvaluationCategory = New System.Windows.Forms.Label
        Me.objbtnSearchTraining = New eZee.Common.eZeeGradientButton
        Me.cboTrainingName = New System.Windows.Forms.ComboBox
        Me.lblTrainingName = New System.Windows.Forms.Label
        Me.cboTrainingCalendar = New System.Windows.Forms.ComboBox
        Me.lblTrainingCalendar = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 450)
        Me.NavPanel.Size = New System.Drawing.Size(647, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.gbEmployeeList)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployeeList)
        Me.gbFilterCriteria.Controls.Add(Me.cboEvaluationCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblEvaluationCategory)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTraining)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingCalendar)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingCalendar)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 69)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(459, 375)
        Me.gbFilterCriteria.TabIndex = 61
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(11, 36)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 62
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lblEmployeeList
        '
        Me.lblEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeList.Location = New System.Drawing.Point(14, 113)
        Me.lblEmployeeList.Name = "lblEmployeeList"
        Me.lblEmployeeList.Size = New System.Drawing.Size(84, 49)
        Me.lblEmployeeList.TabIndex = 291
        Me.lblEmployeeList.Text = "Employee List"
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhEmployeeunkid, Me.dgColhEmpCode, Me.dgColhEmployee})
        Me.dgEmployee.Location = New System.Drawing.Point(5, 30)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(275, 194)
        Me.dgEmployee.TabIndex = 292
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhEmployeeunkid
        '
        Me.objdgcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objdgcolhEmployeeunkid.Name = "objdgcolhEmployeeunkid"
        Me.objdgcolhEmployeeunkid.ReadOnly = True
        Me.objdgcolhEmployeeunkid.Visible = False
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Emp. Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Width = 70
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee Name"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(5, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(276, 21)
        Me.txtSearchEmp.TabIndex = 290
        '
        'cboEvaluationCategory
        '
        Me.cboEvaluationCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEvaluationCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEvaluationCategory.FormattingEnabled = True
        Me.cboEvaluationCategory.Location = New System.Drawing.Point(127, 58)
        Me.cboEvaluationCategory.Name = "cboEvaluationCategory"
        Me.cboEvaluationCategory.Size = New System.Drawing.Size(287, 21)
        Me.cboEvaluationCategory.TabIndex = 106
        '
        'lblEvaluationCategory
        '
        Me.lblEvaluationCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEvaluationCategory.Location = New System.Drawing.Point(14, 61)
        Me.lblEvaluationCategory.Name = "lblEvaluationCategory"
        Me.lblEvaluationCategory.Size = New System.Drawing.Size(108, 15)
        Me.lblEvaluationCategory.TabIndex = 105
        Me.lblEvaluationCategory.Text = "Evaluation Category"
        Me.lblEvaluationCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTraining
        '
        Me.objbtnSearchTraining.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTraining.BorderSelected = False
        Me.objbtnSearchTraining.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTraining.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTraining.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTraining.Location = New System.Drawing.Point(425, 87)
        Me.objbtnSearchTraining.Name = "objbtnSearchTraining"
        Me.objbtnSearchTraining.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTraining.TabIndex = 98
        '
        'cboTrainingName
        '
        Me.cboTrainingName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingName.FormattingEnabled = True
        Me.cboTrainingName.Location = New System.Drawing.Point(127, 85)
        Me.cboTrainingName.Name = "cboTrainingName"
        Me.cboTrainingName.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingName.TabIndex = 24
        '
        'lblTrainingName
        '
        Me.lblTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingName.Location = New System.Drawing.Point(14, 88)
        Me.lblTrainingName.Name = "lblTrainingName"
        Me.lblTrainingName.Size = New System.Drawing.Size(84, 15)
        Me.lblTrainingName.TabIndex = 23
        Me.lblTrainingName.Text = "Training Name"
        Me.lblTrainingName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingCalendar
        '
        Me.cboTrainingCalendar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingCalendar.FormattingEnabled = True
        Me.cboTrainingCalendar.Location = New System.Drawing.Point(127, 30)
        Me.cboTrainingCalendar.Name = "cboTrainingCalendar"
        Me.cboTrainingCalendar.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingCalendar.TabIndex = 4
        '
        'lblTrainingCalendar
        '
        Me.lblTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingCalendar.Location = New System.Drawing.Point(14, 33)
        Me.lblTrainingCalendar.Name = "lblTrainingCalendar"
        Me.lblTrainingCalendar.Size = New System.Drawing.Size(108, 15)
        Me.lblTrainingCalendar.TabIndex = 3
        Me.lblTrainingCalendar.Text = "Training Calendar"
        Me.lblTrainingCalendar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(127, 114)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(287, 258)
        Me.gbEmployeeList.TabIndex = 239
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlEmployeeList.Controls.Add(Me.txtSearchEmp)
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.dgEmployee)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(284, 229)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'frmTrainingFeedbackFormReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 505)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTrainingFeedbackFormReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Feedback Form Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEvaluationCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblEvaluationCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTraining As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTrainingName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingName As System.Windows.Forms.Label
    Friend WithEvents cboTrainingCalendar As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingCalendar As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeList As System.Windows.Forms.Label
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
End Class

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 2

Public Class frmLeavePlanReport

    Private mstrModuleName As String = "frmLeavePlanReport"
    Private objLeavePlannerRpt As clsLeavePlanReport

#Region "Private Variables"

    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region "Constructor"

    Public Sub New()
        objLeavePlannerRpt = New clsLeavePlanReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLeavePlannerRpt.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region "Private Function"

    Public Function Validation() As Boolean
        Try
         
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Public Sub FillCombo()
        Try
            Dim dsList As New DataSet

            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmployee.GetList("Employee", False, True)
            'Else
            '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise


            'dsList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, True, _
            '                             ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                             "Employee", _
            '                             ConfigParameter._Object._ShowFirstAppointmentDate)
            ''S.SANDEEP [04 JUN 2015] -- END


            'Dim dRow As DataRow = dsList.Tables(0).NewRow
            'dRow("employeeunkid") = 0
            'dRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            'dsList.Tables(0).Rows.InsertAt(dRow, 0)

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, _
                                         "Employee", True)

            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename" 'Shani(11-Feb-2016) [name]
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmployee = Nothing


            Dim objJob As New clsJobs
            dsList = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")
            End With
            objJob = Nothing

            Dim objLeave As New clsleavetype_master
            dsList = objLeave.getListForCombo("Leave", True)
            With cboLeave
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objLeave = Nothing


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpLeaveFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLeaveToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = 0
            cboLeave.SelectedValue = 0
            cboJob.SelectedValue = 0
            objLeavePlannerRpt.setDefaultOrderBy(0)
            txtOrderBy.Text = objLeavePlannerRpt.OrderByDisplay
            mstrAdvanceFilter = ""
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objLeavePlannerRpt.SetDefaultValue()

            objLeavePlannerRpt._LeaveFromDate = dtpLeaveFromDate.Value.Date
            objLeavePlannerRpt._LeaveToDate = dtpLeaveToDate.Value.Date

            objLeavePlannerRpt._EmployeeId = CInt(cboEmployee.SelectedValue)
            If CInt(cboEmployee.SelectedValue) > 0 Then
                objLeavePlannerRpt._Employee = cboEmployee.Text
            End If

            objLeavePlannerRpt._JobId = CInt(cboJob.SelectedValue)
            If CInt(cboJob.SelectedValue) > 0 Then
                objLeavePlannerRpt._Job = cboJob.Text
            End If

            objLeavePlannerRpt._LeaveId = cboLeave.SelectedValue
            If CInt(cboLeave.SelectedValue) > 0 Then
                objLeavePlannerRpt._Leave = cboLeave.Text
            End If
            objLeavePlannerRpt._Advance_Filter = mstrAdvanceFilter
            objLeavePlannerRpt._ViewByIds = mstrStringIds
            objLeavePlannerRpt._ViewIndex = mintViewIdx
            objLeavePlannerRpt._ViewByName = mstrStringName
            objLeavePlannerRpt._Analysis_Fields = mstrAnalysis_Fields
            objLeavePlannerRpt._Analysis_Join = mstrAnalysis_Join
            objLeavePlannerRpt._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLeavePlannerRpt._Report_GroupName = mstrReport_GroupName

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            objLeavePlannerRpt._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            'Pinkal (24-Aug-2015) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmLeavePlanReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLeavePlannerRpt = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeavePlanReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objLeavePlannerRpt._ReportName
            Me._Message = objLeavePlannerRpt._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePlanReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmLeavePlanReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePlanReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmLeavePlanReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeavePlannerRpt.generateReport(0, e.Type, enExportAction.None)
                objLeavePlannerRpt.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                            , 0, e.Type, enExportAction.None, -1)
                'Pinkal (24-Aug-2015) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePlanReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub


                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'objLeavePlannerRpt.generateReport(0, enPrintAction.None, e.Type)
                objLeavePlannerRpt.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                                            , True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                            , 0, enPrintAction.None, e.Type, -1)
                'Pinkal (24-Aug-2015) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePlanReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLeavePlanReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeavePlanReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLeavePlanReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLeavePlanReport.SetMessages()
            objfrm._Other_ModuleNames = "clsLeavePlanReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLeavePlanReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboEmployee
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboJob
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeaveType.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboLeave
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeaveType_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLeavePlannerRpt.setOrderBy(0)
            txtOrderBy.Text = objLeavePlannerRpt.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()

            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.Name, Me.lblLeaveName.Text)
			Me.lblLeaveTodate.Text = Language._Object.getCaption(Me.lblLeaveTodate.Name, Me.lblLeaveTodate.Text)
			Me.lblLeaveFromDate.Text = Language._Object.getCaption(Me.lblLeaveFromDate.Name, Me.lblLeaveFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.LblJob.Text = Language._Object.getCaption(Me.LblJob.Name, Me.LblJob.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'************************************************************************************************************************************
'Class Name : EmpListWithoutApprover.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************


#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmpListWithoutApprover

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpListWithoutApprover"
    Private objEmpWOApprover As clsEmpListWithoutApprover
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        objEmpWOApprover = New clsEmpListWithoutApprover(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpWOApprover.SetDefaultValue()
        InitializeComponent()

        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End

    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim dsList As New DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).ToShortDateString)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            dsList = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpWOApprover.SetDefaultValue()
            objEmpWOApprover._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpWOApprover._EmployeeName = cboEmployee.Text
            objEmpWOApprover._JobID = CInt(cboJob.SelectedValue)
            objEmpWOApprover._JobName = cboJob.Text
            objEmpWOApprover._IsActive = chkInactiveemp.Checked
            objEmpWOApprover._ViewByIds = mstrStringIds
            objEmpWOApprover._ViewIndex = mintViewIdx
            objEmpWOApprover._ViewByName = mstrStringName
            objEmpWOApprover._Analysis_Fields = mstrAnalysis_Fields
            objEmpWOApprover._Analysis_Join = mstrAnalysis_Join
            objEmpWOApprover._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpWOApprover._Report_GroupName = mstrReport_GroupName
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEmpWOApprover._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objEmpWOApprover.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpWOApprover.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboJob.SelectedIndex = 0
            chkInactiveemp.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmpListWithoutApprover_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpWOApprover = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Me._Title = objEmpWOApprover._ReportName
            Me._Message = objEmpWOApprover._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEmpListWithoutApprover_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboJob
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub


            'Pinkal (12-Feb-2016) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objEmpWOApprover.generateReport(0, e.Type, enExportAction.None)
            objEmpWOApprover.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                   , 0, e.Type, enExportAction.None, 0)
            'Pinkal (12-Feb-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub


            'Pinkal (12-Feb-2016) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            ' objEmpWOApprover.generateReport(0, enPrintAction.None, e.Type)
            objEmpWOApprover.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                   , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport _
                                                                   , 0, enPrintAction.None, e.Type, 0)
            'Pinkal (12-Feb-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpListWithoutApprover_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpListWithoutApprover_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpListWithoutApprover.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpListWithoutApprover"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpWOApprover.setOrderBy(0)
            txtOrderBy.Text = objEmpWOApprover.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "LinkLabel Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()

            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

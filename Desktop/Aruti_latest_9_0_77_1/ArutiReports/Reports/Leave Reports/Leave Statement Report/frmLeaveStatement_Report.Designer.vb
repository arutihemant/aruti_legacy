﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveStatement_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpAsonDate = New System.Windows.Forms.DateTimePicker
        Me.LblAsOnDate = New System.Windows.Forms.Label
        Me.objbtnSearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblLeaveName = New System.Windows.Forms.Label
        Me.cboLeave = New System.Windows.Forms.ComboBox
        Me.chkShowAdjustmentRemark = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowAdjustmentRemark)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAsonDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblAsOnDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.LblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveName)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeave)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(369, 119)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAsonDate
        '
        Me.dtpAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsonDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsonDate.Location = New System.Drawing.Point(76, 31)
        Me.dtpAsonDate.Name = "dtpAsonDate"
        Me.dtpAsonDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpAsonDate.TabIndex = 213
        '
        'LblAsOnDate
        '
        Me.LblAsOnDate.BackColor = System.Drawing.Color.Transparent
        Me.LblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAsOnDate.Location = New System.Drawing.Point(8, 35)
        Me.LblAsOnDate.Name = "LblAsOnDate"
        Me.LblAsOnDate.Size = New System.Drawing.Size(63, 13)
        Me.LblAsOnDate.TabIndex = 212
        Me.LblAsOnDate.Text = "As on Date"
        '
        'objbtnSearchLeaveType
        '
        Me.objbtnSearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeaveType.BorderSelected = False
        Me.objbtnSearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeaveType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeaveType.Location = New System.Drawing.Point(336, 87)
        Me.objbtnSearchLeaveType.Name = "objbtnSearchLeaveType"
        Me.objbtnSearchLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeaveType.TabIndex = 210
        '
        'LblEmployee
        '
        Me.LblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(8, 62)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(63, 13)
        Me.LblEmployee.TabIndex = 205
        Me.LblEmployee.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(336, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 209
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(76, 58)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(254, 21)
        Me.cboEmployee.TabIndex = 204
        '
        'lblLeaveName
        '
        Me.lblLeaveName.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveName.Location = New System.Drawing.Point(8, 89)
        Me.lblLeaveName.Name = "lblLeaveName"
        Me.lblLeaveName.Size = New System.Drawing.Size(62, 13)
        Me.lblLeaveName.TabIndex = 190
        Me.lblLeaveName.Text = "Leave "
        '
        'cboLeave
        '
        Me.cboLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeave.DropDownWidth = 120
        Me.cboLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeave.FormattingEnabled = True
        Me.cboLeave.Location = New System.Drawing.Point(76, 85)
        Me.cboLeave.Name = "cboLeave"
        Me.cboLeave.Size = New System.Drawing.Size(254, 21)
        Me.cboLeave.TabIndex = 5
        '
        'chkShowAdjustmentRemark
        '
        Me.chkShowAdjustmentRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAdjustmentRemark.Location = New System.Drawing.Point(190, 33)
        Me.chkShowAdjustmentRemark.Name = "chkShowAdjustmentRemark"
        Me.chkShowAdjustmentRemark.Size = New System.Drawing.Size(167, 17)
        Me.chkShowAdjustmentRemark.TabIndex = 215
        Me.chkShowAdjustmentRemark.Text = "Show Adjustment Remark"
        Me.chkShowAdjustmentRemark.UseVisualStyleBackColor = True
        '
        'frmLeaveStatement_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.KeyPreview = True
        Me.Name = "frmLeaveStatement_Report"
        Me.Text = "frmLeaveStatement_Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblLeaveName As System.Windows.Forms.Label
    Public WithEvents cboLeave As System.Windows.Forms.ComboBox
    Private WithEvents LblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpAsonDate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblAsOnDate As System.Windows.Forms.Label
    Friend WithEvents chkShowAdjustmentRemark As System.Windows.Forms.CheckBox
End Class

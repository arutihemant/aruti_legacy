#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class clsEmpMedicalCover_DetailReport
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsEmpMedicalCover_DetailReport"
    Private mstrReportId As String = enArutiReport.Employee_Medical_Cover_Detail
    Private objDataOperation As clsDataOperation
    Private mdtExcelTable As DataTable
    Private mdtSummTable As DataTable
    Private mdtSummTableClone As DataTable
    Private mdtFilter As DataTable
    Private xStringBuilder As StringBuilder
    Private xDicColumnCollection As Dictionary(Of String, String)

#End Region

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
    End Sub

#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mstrServiceProviderIDs As String = String.Empty
    Private mintViewAllocationRefId As Integer = 0
    Private mstrViewAllocationRefName As String = String.Empty
    Private mstrViewAllocationIds As String = String.Empty
    Private mintCompanyUnkid As Integer = 0
    Private mintUserUnkid As Integer = 0
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mintSpouseRelationId As Integer = 0
    Private mstrSpouseRelationName As String = String.Empty
    Private mintChildRelationId As Integer = 0
    Private mstrChildRelationName As String = String.Empty
    Private mblnIncludeInactiveEmp As Boolean = False
    Private mblnFirstNameThenLastName As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Private mstrCurrencyFormat As String = GUI.fmtCurrency

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ServiceProviderIDs() As String
        Set(ByVal value As String)
            mstrServiceProviderIDs = value
        End Set
    End Property

    Public WriteOnly Property _ViewAllocationRefId() As Integer
        Set(ByVal value As Integer)
            mintViewAllocationRefId = value
        End Set
    End Property

    Public WriteOnly Property _ViewAllocationRefName() As String
        Set(ByVal value As String)
            mstrViewAllocationRefName = value
        End Set
    End Property

    Public WriteOnly Property _ViewAllocationIds() As String
        Set(ByVal value As String)
            mstrViewAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _CompanyID() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserID() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _SpouseRelationId() As Integer
        Set(ByVal value As Integer)
            mintSpouseRelationId = value
        End Set
    End Property

    Public WriteOnly Property _SpouseRelationName() As String
        Set(ByVal value As String)
            mstrSpouseRelationName = value
        End Set
    End Property

    Public WriteOnly Property _ChildRelationId() As Integer
        Set(ByVal value As Integer)
            mintChildRelationId = value
        End Set
    End Property

    Public WriteOnly Property _ChildRelationName() As String
        Set(ByVal value As String)
            mstrChildRelationName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNameThenLastName = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyFormat() As String
        Set(ByVal value As String)
            mstrCurrencyFormat = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrServiceProviderIDs = String.Empty
            mintViewAllocationRefId = 0
            mstrViewAllocationIds = String.Empty
            mintCompanyUnkid = 0
            mintUserUnkid = 0
            mintSpouseRelationId = 0
            mstrSpouseRelationName = String.Empty
            mintChildRelationId = 0
            mstrChildRelationName = String.Empty
            mblnIncludeInactiveEmp = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery(ByVal strDatabaseName As String)
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintPeriodId > 0 Then
                Me._FilterTitle = Language.getMessage(mstrModuleName, 1, "Period:") & " " & mstrPeriodName & " "
            End If

            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mintPeriodId
            objPrd._Periodunkid(strDatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPrd._Start_Date))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPrd._End_Date))
            objPrd = Nothing

            objDataOperation.AddParameter("@M", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "M"))
            objDataOperation.AddParameter("@F", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "F"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try


            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub Generate_Table()
        Dim blnFlag As Boolean = False
        Try
            mdtExcelTable = New DataTable("Medical")
            Dim dCol As DataColumn

            dCol = New DataColumn
            With dCol
                .ColumnName = "SrNo"
                .Caption = Language.getMessage(mstrModuleName, 2, "Nr")
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ECode"
                .Caption = Language.getMessage(mstrModuleName, 3, "Employee #")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Employee"
                .Caption = Language.getMessage(mstrModuleName, 4, "Employee Name")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Department"
                .Caption = Language.getMessage(mstrModuleName, 5, "Department")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Job"
                .Caption = Language.getMessage(mstrModuleName, 6, "Job Title")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Gender"
                .Caption = Language.getMessage(mstrModuleName, 7, "Gender")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            If mstrServiceProviderIDs.Trim.Length > 0 Then
                Dim objProvider As clsinstitute_master
                For Each xProviderId As String In mstrServiceProviderIDs.Split(",")
                    objProvider = New clsinstitute_master
                    objProvider._Instituteunkid = CInt(xProviderId)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "Col_" & xProviderId
                        .Caption = objProvider._Institute_Name
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtExcelTable.Columns.Add(dCol)
                Next
                objProvider = Nothing
            End If

            dCol = New DataColumn
            With dCol
                .ColumnName = "MStatus"
                .Caption = Language.getMessage(mstrModuleName, 8, "Marital Status")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)


            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation1"
                .Caption = mstrSpouseRelationName
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation2"
                .Caption = mstrChildRelationName
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = ""
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "TotalDep"
                .Caption = Language.getMessage(mstrModuleName, 9, "Total Dependent")
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation1Name"
                .Caption = mstrSpouseRelationName & " " & Language.getMessage(mstrModuleName, 10, "Name")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)


            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation1DOB"
                .Caption = mstrSpouseRelationName & " " & Language.getMessage(mstrModuleName, 21, "BirthDate")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation1Gender"
                .Caption = mstrSpouseRelationName & " " & Language.getMessage(mstrModuleName, 22, "Gender")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)
            'Pinkal (04-Jan-2020) -- End


            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation2Name"
                .Caption = mstrChildRelationName & " " & Language.getMessage(mstrModuleName, 10, "Name")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)


            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation2DOB"
                .Caption = mstrChildRelationName & " " & Language.getMessage(mstrModuleName, 21, "BirthDate")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Relation2Gender"
                .Caption = mstrChildRelationName & " " & Language.getMessage(mstrModuleName, 22, "Gender")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = 0
            End With
            mdtExcelTable.Columns.Add(dCol)
            'Pinkal (04-Jan-2020) -- End


            '************************** SUMMARY TABLE COLUMNS ************************ ' -- START
            Dim StrQ As String = String.Empty : Dim dsCategory As New DataSet : Dim dsFilter As New DataSet
            Using objDatOpr As New clsDataOperation
                '------------------------------------ SUMMARY TABLE COLUMNS
                StrQ = "SELECT DISTINCT name,maritalstatusunkid FROM mdmedical_category_master JOIN cfcommon_master ON masterunkid = maritalstatusunkid WHERE maritalstatusunkid > 0 AND cfcommon_master.isactive = 1 AND mdmedical_category_master.isactive = 1 "

                dsCategory = objDatOpr.ExecQuery(StrQ, "List")

                If objDatOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDatOpr.ErrorNumber & " : " & objDatOpr.ErrorMessage)
                End If

                If mstrServiceProviderIDs.Trim.Length > 0 Then
                    xDicColumnCollection = New Dictionary(Of String, String)

                    Dim objProvider As clsinstitute_master
                    For Each xProviderId As String In mstrServiceProviderIDs.Split(",")
                        objProvider = New clsinstitute_master
                        objProvider._Instituteunkid = CInt(xProviderId)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Head_" & xProviderId
                            .Caption = objProvider._Institute_Name
                            .DataType = System.Type.GetType("System.Int32")
                            .DefaultValue = 0
                        End With
                        dsCategory.Tables(0).Columns.Add(dCol)
                        xDicColumnCollection.Add(dCol.ColumnName, dCol.ColumnName)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Amount_" & xProviderId
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Decimal")
                            .DefaultValue = 0
                        End With
                        dsCategory.Tables(0).Columns.Add(dCol)
                        xDicColumnCollection.Add(dCol.ColumnName, dCol.ColumnName)

                        dCol = New DataColumn
                        With dCol
                            .ColumnName = "Total_" & xProviderId
                            .Caption = ""
                            .DataType = System.Type.GetType("System.Decimal")
                            .DefaultValue = 0
                        End With
                        dsCategory.Tables(0).Columns.Add(dCol)
                        xDicColumnCollection.Add(dCol.ColumnName, dCol.ColumnName)
                    Next
                    objProvider = Nothing
                End If

                dCol = New DataColumn
                With dCol
                    .ColumnName = "Total"
                    .Caption = Language.getMessage(mstrModuleName, 14, "Total")
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = 0
                End With
                dsCategory.Tables(0).Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "FinalTotal"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Decimal")
                    .DefaultValue = 0
                End With
                dsCategory.Tables(0).Columns.Add(dCol)

                mdtSummTable = dsCategory.Tables(0).Copy : mdtSummTableClone = dsCategory.Tables(0).Copy
                '------------------------------------ SUMMARY TABLE COLUMNS

                '------------------------------------ SUMMART TABLE FILTER -- START
                StrQ = "SELECT DISTINCT maritalstatusunkid FROM mdmedical_category_master WHERE maritalstatusunkid > 0 AND isactive = 1 "

                dsFilter = objDatOpr.ExecQuery(StrQ, "List")

                If objDatOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDatOpr.ErrorNumber & " : " & objDatOpr.ErrorMessage)
                End If

                mdtFilter = dsFilter.Tables(0)
                '------------------------------------ SUMMART TABLE FILTER -- END
            End Using
            '************************** SUMMARY TABLE COLUMNS ************************ ' -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Table; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GenerateHeader()
        Try
            xStringBuilder = New StringBuilder
            xStringBuilder.Append(" <HTML> " & vbCrLf)
            xStringBuilder.Append(" <TITLE>" & Me._ReportName & "</TITLE> " & vbCrLf)
            xStringBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            xStringBuilder.Append(" <BR> " & vbCrLf)
            xStringBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            If ConfigParameter._Object._IsDisplayLogo = True Then
                xStringBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                xStringBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                xStringBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                xStringBuilder.Append(" </TD> " & vbCrLf)
                xStringBuilder.Append(" </TR> " & vbCrLf)
            End If

            xStringBuilder.Append(" <TR> " & vbCrLf)
            xStringBuilder.Append(" <TD width='10%'> " & vbCrLf)
            xStringBuilder.Append(" <FONT SIZE=2><b>" & Language.getMessage(mstrModuleName, 20, "Prepared By :") & " </B></FONT> " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            xStringBuilder.Append(User._Object._Username & vbCrLf)
            xStringBuilder.Append(" </FONT></TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            xStringBuilder.Append(" <FONT SIZE=3><B> " & Company._Object._Name & " </B></FONT> " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            xStringBuilder.Append(" &nbsp; " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            xStringBuilder.Append(" &nbsp; " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" </TR> " & vbCrLf)
            xStringBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            xStringBuilder.Append(" <TD width='10%'> " & vbCrLf)
            xStringBuilder.Append(" <FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 19, "Date :") & "</B></FONT> " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            xStringBuilder.Append(Now & vbCrLf)
            xStringBuilder.Append(" </FONT></TD> " & vbCrLf)
            xStringBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)
            xStringBuilder.Append(" <FONT SIZE=3><b> " & Me._ReportName & "</B></FONT> " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            xStringBuilder.Append(" &nbsp; " & vbCrLf)
            xStringBuilder.Append(" </TD> " & vbCrLf)
            xStringBuilder.Append(" </TR> " & vbCrLf)
            xStringBuilder.Append(" <TR> " & vbCrLf)
            xStringBuilder.Append(" </TABLE> " & vbCrLf)
            xStringBuilder.Append(" <HR> " & vbCrLf)
            xStringBuilder.Append(" <B> " & Me._FilterTitle & " </B><BR> " & vbCrLf)
            xStringBuilder.Append(" </HR> " & vbCrLf)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateHeader; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function GenerateDetailPart(ByVal sb As StringBuilder, ByVal xTable As DataTable, ByVal xAllocName As String, ByVal strDatabaseName As String, Optional ByVal xEmpIds As String = "") As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Try
            sb.Append(" <BR> " & vbCrLf)

            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            'sb.Append(" <FONT SIZE=2><B> " & mstrViewAllocationRefName & " : " & xAllocName & "</B></FONT> " & vbCrLf)
            If mintViewAllocationRefId > 0 Then
                sb.Append(" <FONT SIZE=2><B> " & mstrViewAllocationRefName & " : " & xAllocName & "</B></FONT> " & vbCrLf)
            End If
            'Pinkal (04-Jan-2020) -- End


            sb.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            sb.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To xTable.Columns.Count - 1 '#B0B0B0  - LIGHT GRAY
                sb.Append("<TD BORDER=1 BGCOLOR = #B0B0B0 WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2><B>" & xTable.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            sb.Append(" </TR> " & vbCrLf)
            For i As Integer = 0 To xTable.Rows.Count - 1
                sb.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To xTable.Columns.Count - 1
                    If i = xTable.Rows.Count - 1 Then
                        sb.Append("<TD BORDER=1 BGCOLOR = #B0B0B0 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & IIf(xTable.Rows(i)(k) <= "0", "", xTable.Rows(i)(k)) & "<B></FONT></TD>" & vbCrLf)
                    Else
                        sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & IIf(xTable.Rows(i)(k) <= "0", "", xTable.Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                    End If
                Next
                sb.Append(" </TR> " & vbCrLf)
            Next
            sb.Append(" </TABLE> " & vbCrLf)

            '********************************************* SUMMARY PART *********************************************' -- START
            If xEmpIds.Trim.Length > 0 Then
                Dim dsSummary As New DataSet
                Dim StrQ As String = "SELECT DISTINCT " & _
                                     "   mdmedical_master.mdmasterunkid " & _
                                     "  ,SUM(1) OVER(PARTITION BY mdmedical_master.mdmastername) AS Heads " & _
                                     "  ,mdmedical_category_master.amount " & _
                                     "  ,SUM(1) OVER(PARTITION BY mdmedical_master.mdmastername) * mdmedical_category_master.amount AS Total " & _
                                     "  ,maritalstatusunkid " & _
                                     "  ,providerunkid AS PovId " & _
                                     "  ,SUM(1) OVER(PARTITION BY maritalstatusunkid) AS TotalHeads " & _
                                     "FROM mdmedical_cover " & _
                                     "  JOIN mdcategory_providermapping ON mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid " & _
                                     "  JOIN mdmedical_category_master ON mdcategory_providermapping.medicalcategoryunkid  = mdmedical_category_master.medicalcategoryunkid " & _
                                     "  JOIN mdmedical_master ON mdmedical_category_master.mdcategorymasterunkid = mdmedical_master.mdmasterunkid " & _
                                     "WHERE mdmedical_cover.isvoid = 0 AND mdcategory_providermapping.isvoid = 0 " & _
                                     "  AND employeeunkid IN(" & xEmpIds & ") AND providerunkid IN(" & mstrServiceProviderIDs & ") " & _
                                     "  AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
                                     "    OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate)) "
                Using objDataOpr As New clsDataOperation

                    Dim objPrd As New clscommom_period_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPrd._Periodunkid = mintPeriodId
                    objPrd._Periodunkid(strDatabaseName) = mintPeriodId
                    'Sohail (21 Aug 2015) -- End
                    objDataOpr.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPrd._Start_Date))
                    objDataOpr.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPrd._End_Date))
                    objPrd = Nothing

                    dsSummary = objDataOpr.ExecQuery(StrQ, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    End If
                End Using

                If mdtFilter.Rows.Count > 0 Then
                    For Each fRow As DataRow In mdtFilter.Rows
                        Dim xFilterRow() As DataRow = dsSummary.Tables(0).Select("maritalstatusunkid = '" & fRow.Item("maritalstatusunkid") & "'")
                        If xFilterRow.Length > 0 Then
                            Dim dtDummy As DataTable = xFilterRow.CopyToDataTable()

                            For index As Integer = 0 To xFilterRow.Length - 1
                                Dim xRow() As DataRow = mdtSummTable.Select("maritalstatusunkid = '" & xFilterRow(index).Item("maritalstatusunkid") & "'")
                                If xRow.Length > 0 Then
                                    If mdtSummTable.Columns.Contains("Head_" & xFilterRow(index).Item("PovId")) Then
                                        xRow(0).Item("Head_" & xFilterRow(index).Item("PovId")) = xFilterRow(index).Item("Heads")
                                        xRow(0).Item("Amount_" & xFilterRow(index).Item("PovId")) = Format(CDec(xFilterRow(index).Item("amount")), mstrCurrencyFormat)
                                        xRow(0).Item("Total_" & xFilterRow(index).Item("PovId")) = Format(CDec(xFilterRow(index).Item("Total")), mstrCurrencyFormat)
                                        xRow(0).Item("Total") = xFilterRow(index).Item("TotalHeads")
                                        xRow(0).Item("FinalTotal") = Format(CDec(dtDummy.Compute("SUM(Total)", "")), mstrCurrencyFormat)
                                    End If
                                End If
                                mdtSummTable.AcceptChanges()
                            Next
                        End If
                    Next
                    mdtSummTable.Rows.Add(mdtSummTable.NewRow)

                    mdtSummTable.Rows(mdtSummTable.Rows.Count - 1).Item("Total") = CInt(mdtSummTable.Compute("SUM(Total)", ""))
                    mdtSummTable.Rows(mdtSummTable.Rows.Count - 1).Item("FinalTotal") = Format(CDec(mdtSummTable.Compute("SUM(FinalTotal)", "")), mstrCurrencyFormat)

                    If xDicColumnCollection.Keys.Count > 0 Then
                        For Each xKey As String In xDicColumnCollection.Keys
                            If xKey.Contains("Amount_") Then
                                mdtSummTable.Rows(mdtSummTable.Rows.Count - 1).Item(xKey) = Format(CDec(mdtSummTable.Compute("SUM(" & xKey & ")", "")), mstrCurrencyFormat)
                            Else
                                mdtSummTable.Rows(mdtSummTable.Rows.Count - 1).Item(xKey) = CInt(mdtSummTable.Compute("SUM(" & xKey & ")", ""))
                            End If
                        Next
                        mdtSummTable.AcceptChanges()
                    End If


                    mdtSummTable.Columns.Remove("maritalstatusunkid")

                    sb.Append(" <BR> " & vbCrLf)
                    sb.Append(" <BR> " & vbCrLf)
                    sb.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3> " & vbCrLf)
                    sb.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                    For j As Integer = 0 To mdtSummTable.Columns.Count - 1 '#FFB763  - LIGHT BROWN
                        If j = 0 Then
                            sb.Append("<TD BORDER=1 BGCOLOR = #FFB763 ALIGN='LEFT'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                        Else
                            sb.Append("<TD BORDER=1 BGCOLOR = #FFB763 ALIGN='LEFT'><FONT SIZE=2><B>" & mdtSummTable.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                    sb.Append(" </TR> " & vbCrLf)
                    For i As Integer = 0 To mdtSummTable.Rows.Count - 1
                        sb.Append(" <TR> " & vbCrLf)
                        For k As Integer = 0 To mdtSummTable.Columns.Count - 1
                            If i = mdtSummTable.Rows.Count - 1 Then
                                If mdtSummTable.Columns(k).DataType.FullName = "System.Decimal" Then
                                    sb.Append("<TD BORDER=1 BGCOLOR = #FFB763 BORDERCOLOR=BLACK ALIGN='Rigth'><FONT SIZE=2><B>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", Format(CDec(mdtSummTable.Rows(i)(k)), mstrCurrencyFormat)) & "<B></FONT></TD>" & vbCrLf)
                                ElseIf mdtSummTable.Columns(k).DataType.FullName = "System.Int32" Then
                                    sb.Append("<TD BORDER=1 BGCOLOR = #FFB763 BORDERCOLOR=BLACK ALIGN='Right'><FONT SIZE=2><B>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", mdtSummTable.Rows(i)(k)) & "<B></FONT></TD>" & vbCrLf)
                                Else
                                    sb.Append("<TD BORDER=1 BGCOLOR = #FFB763 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", mdtSummTable.Rows(i)(k)) & "<B></FONT></TD>" & vbCrLf)
                                End If
                            Else
                                If mdtSummTable.Columns(k).DataType.FullName = "System.Decimal" Then
                                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='Rigth'><FONT SIZE=2>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", Format(CDec(mdtSummTable.Rows(i)(k)), mstrCurrencyFormat)) & "</FONT></TD>" & vbCrLf)
                                ElseIf mdtSummTable.Columns(k).DataType.FullName = "System.Int32" Then
                                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='Rigth'><FONT SIZE=2>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", mdtSummTable.Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                Else
                                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & IIf(mdtSummTable.Rows(i)(k).ToString.Trim.Length <= 0, "", mdtSummTable.Rows(i)(k)) & "</FONT></TD>" & vbCrLf)
                                End If
                            End If
                        Next
                        sb.Append(" </TR> " & vbCrLf)
                    Next
                    sb.Append(" </TABLE> " & vbCrLf)
                End If
            End If
            '********************************************* SUMMARY PART *********************************************' -- END
            sb.Append(" <BR> " & vbCrLf)
            sb.Append(" <BR> " & vbCrLf)
            sb.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            For P As Integer = 0 To 6
                sb.Append(" <TR> " & vbCrLf)
                If P = 0 Then
                    For k As Integer = 0 To 4
                        If k = 0 Then
                            sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Compiled By : ") & "</B></FONT></TD>" & vbCrLf)
                        ElseIf k = 1 Then
                            sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Reviewed By : ") & "</B></FONT></TD>" & vbCrLf)
                        ElseIf k > 1 AndAlso k <= 3 Then
                            sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '3'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 17, "Approved By : ") & "</B></FONT></TD>" & vbCrLf)
                        Else
                            sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' ROWSPAN='3' COLSPAN = '2'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 18, "Approved By : ") & "</B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                ElseIf P > 2 Then
                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = '3'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                    sb.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'  COLSPAN = '2'><FONT SIZE=2><B></B></FONT></TD>" & vbCrLf)
                End If
                sb.Append(" </TR> " & vbCrLf)
            Next
            sb.Append(" </TABLE> ")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateDetailPart; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal sb As StringBuilder) As Boolean
        Try

            If System.IO.Directory.Exists(SavePath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    SavePath = dig.SelectedPath
                Else
                    Exit Function
                End If
            End If

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            Dim fsFile As New FileStream(SavePath & "\" & flFileName & ".xls", FileMode.Create, FileAccess.Write)
            Dim strWriter As New StreamWriter(fsFile)
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_to_Excel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Report Generation "

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function Generate_DetailReport() As Boolean
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Try
    '        Call Generate_Table()
    '        objDataOperation = New clsDataOperation


    '        Dim dsProvider As New DataSet

    '        StrQ = "SELECT " & _
    '               "     employeeunkid AS EmpId " & _
    '               "    ,providerunkid AS PovId " & _
    '               "FROM mdmedical_cover " & _
    '               "    JOIN mdcategory_providermapping ON mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid " & _
    '               "WHERE mdmedical_cover.isvoid = 0 AND mdcategory_providermapping.isvoid = 0 " & _
    '               "    AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
    '               "    OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate)) " & _
    '               "AND providerunkid IN(" & mstrServiceProviderIDs & ") "

    '        Call FilterTitleAndFilterQuery()

    '        dsProvider = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        If mstrViewAllocationIds.Trim.Length > 0 Then Call GenerateHeader()

    '        Dim xAllocationName As String = String.Empty
    '        For Each xAllocationId As String In mstrViewAllocationIds.Split(",")
    '            objDataOperation.ClearParameters()
    '            mdtExcelTable.Rows.Clear()
    '            mdtSummTable.Rows.Clear()

    '            mdtSummTable = mdtSummTableClone.Copy

    '            Select Case mintViewAllocationRefId
    '                Case enAllocation.BRANCH
    '                    Dim objBranch As New clsStation
    '                    objBranch._Stationunkid = CInt(xAllocationId)
    '                    xAllocationName = objBranch._Name
    '                    objBranch = Nothing

    '                Case enAllocation.DEPARTMENT_GROUP
    '                    Dim objDeptGrp As New clsDepartmentGroup
    '                    objDeptGrp._Deptgroupunkid = CInt(xAllocationId)
    '                    xAllocationName = objDeptGrp._Name
    '                    objDeptGrp = Nothing

    '                Case enAllocation.DEPARTMENT
    '                    Dim objDept As New clsDepartment
    '                    objDept._Departmentunkid = CInt(xAllocationId)
    '                    xAllocationName = objDept._Name
    '                    objDept = Nothing

    '                Case enAllocation.SECTION_GROUP
    '                    Dim objSecGrp As New clsSectionGroup
    '                    objSecGrp._Sectiongroupunkid = CInt(xAllocationId)
    '                    xAllocationName = objSecGrp._Name
    '                    objSecGrp = Nothing

    '                Case enAllocation.SECTION
    '                    Dim objSec As New clsSections
    '                    objSec._Sectionunkid = CInt(xAllocationId)
    '                    xAllocationName = objSec._Name
    '                    objSec = Nothing

    '                Case enAllocation.UNIT_GROUP
    '                    Dim objUnitGrp As New clsUnitGroup
    '                    objUnitGrp._Unitgroupunkid = CInt(xAllocationId)
    '                    xAllocationName = objUnitGrp._Name
    '                    objUnitGrp = Nothing

    '                Case enAllocation.UNIT
    '                    Dim objUnit As New clsUnits
    '                    objUnit._Unitunkid = CInt(xAllocationId)
    '                    xAllocationName = objUnit._Name
    '                    objUnit = Nothing

    '                Case enAllocation.TEAM
    '                    Dim objTeam As New clsTeams
    '                    objTeam._Teamunkid = CInt(xAllocationId)
    '                    xAllocationName = objTeam._Name
    '                    objTeam = Nothing

    '                Case enAllocation.CLASS_GROUP
    '                    Dim objClsGrp As New clsClassGroup
    '                    objClsGrp._Classgroupunkid = CInt(xAllocationId)
    '                    xAllocationName = objClsGrp._Name
    '                    objClsGrp = Nothing

    '                Case enAllocation.CLASSES
    '                    Dim objCls As New clsClass
    '                    objCls._Classesunkid = CInt(xAllocationId)
    '                    xAllocationName = objCls._Name
    '                    objCls = Nothing

    '            End Select

    '            StrQ = "SELECT " & _
    '                   "     employeecode AS ECode "
    '            If mblnFirstNameThenLastName = True Then
    '                StrQ &= "    ,firstname + ' ' + surname AS Employee "
    '            Else
    '                StrQ &= "    ,surname + ' ' + firstname AS Employee "
    '            End If
    '            StrQ &= "    ,hrdepartment_master.name AS Department " & _
    '                    "    ,job_name AS Job " & _
    '                    "    ,CASE WHEN gender = 1 THEN @M WHEN gender = 2 THEN @F ELSE '' END AS Gender " & _
    '                    "    ,ISNULL(cfcommon_master.name,'') AS MStatus " & _
    '                    "    ,ISNULL(CAST(R1.Relation1 AS NVARCHAR(MAX)),'') AS Relation1 " & _
    '                    "    ,ISNULL(CAST(R2.Relation2 AS NVARCHAR(MAX)),'') AS Relation2 " & _
    '                    "    ,ISNULL(R1.Relation1,0)+ISNULL(R2.Relation2,0) AS TotalDep " & _
    '                    "    ,ISNULL(R1.Relation1Name,'') AS Relation1Name " & _
    '                    "    ,ISNULL(R2.Relation2Name,'') AS Relation2Name " & _
    '                    "    ,hremployee_master.employeeunkid " & _
    '                    "    ,ROW_NUMBER() OVER(ORDER BY employeeunkid) AS SrNo " & _
    '                    "FROM hremployee_master " & _
    '                    "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' " & _
    '                    "    JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                    "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
    '                    "    LEFT JOIN " & _
    '                    "    ( " & _
    '                    "        SELECT " & _
    '                    "             employeeunkid AS R1EmpId " & _
    '                    "            ,COUNT(dpndtbeneficetranunkid) AS Relation1 " & _
    '                    "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ t1.first_name+' '+t1.last_name FROM hrdependants_beneficiaries_tran AS t1 WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND isvoid = 0 AND relationunkid = '" & mintSpouseRelationId & "' FOR XML PATH('')),1,1,'') AS Relation1Name " & _
    '                    "        FROM hrdependants_beneficiaries_tran " & _
    '                    "        WHERE isvoid = 0 AND relationunkid = '" & mintSpouseRelationId & "' " & _
    '                    "        GROUP BY employeeunkid " & _
    '                    "    ) AS R1 ON R1.R1EmpId = hremployee_master.employeeunkid " & _
    '                    "    LEFT JOIN " & _
    '                    "    ( " & _
    '                    "        SELECT " & _
    '                    "             employeeunkid AS R2EmpId " & _
    '                    "            ,COUNT(dpndtbeneficetranunkid) Relation2 " & _
    '                    "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ t1.first_name+' '+t1.last_name FROM hrdependants_beneficiaries_tran AS t1 WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' FOR XML PATH('')),1,1,'') AS Relation2Name " & _
    '                    "        FROM hrdependants_beneficiaries_tran " & _
    '                    "        WHERE isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' " & _
    '                    "        GROUP BY employeeunkid " & _
    '                    "    ) AS R2 ON R2.R2EmpId = hremployee_master.employeeunkid " & _
    '                    "WHERE 1 = 1 "

    '            If mblnIncludeInactiveEmp = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If
    '            Select Case mintViewAllocationRefId
    '                Case enAllocation.BRANCH
    '                    StrQ &= "AND hremployee_master.stationunkid = '" & xAllocationId & "' "
    '                Case enAllocation.DEPARTMENT_GROUP
    '                    StrQ &= "AND hremployee_master.deptgroupunkid = '" & xAllocationId & "' "
    '                Case enAllocation.DEPARTMENT
    '                    StrQ &= "AND hremployee_master.departmentunkid = '" & xAllocationId & "' "
    '                Case enAllocation.SECTION_GROUP
    '                    StrQ &= "AND hremployee_master.sectiongroupunkid = '" & xAllocationId & "' "
    '                Case enAllocation.SECTION
    '                    StrQ &= "AND hremployee_master.sectionunkid = '" & xAllocationId & "' "
    '                Case enAllocation.UNIT_GROUP
    '                    StrQ &= "AND hremployee_master.unitgroupunkid = '" & xAllocationId & "' "
    '                Case enAllocation.UNIT
    '                    StrQ &= "AND hremployee_master.unitunkid = '" & xAllocationId & "' "
    '                Case enAllocation.TEAM
    '                    StrQ &= "AND hremployee_master.teamunkid = '" & xAllocationId & "' "
    '                Case enAllocation.CLASS_GROUP
    '                    StrQ &= "AND hremployee_master.classgroupunkid = '" & xAllocationId & "' "
    '                Case enAllocation.CLASSES
    '                    StrQ &= "AND hremployee_master.classunkid = '" & xAllocationId & "' "
    '            End Select

    '            Call FilterTitleAndFilterQuery()

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            End If

    '            Dim CsvEmpIds As String = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())

    '            Dim xRelation1Tot, xRelation2Tot, xTotalDep As Integer : Dim xProviderTot As New Dictionary(Of String, Integer)
    '            xRelation1Tot = 0 : xRelation2Tot = 0 : xTotalDep = 0
    '            For Each dRow As DataRow In dsList.Tables("DataTable").Rows
    '                dRow.Item("Relation1Name") = dRow.Item("Relation1Name").ToString.Replace("|", "<BR>")
    '                dRow.Item("Relation2Name") = dRow.Item("Relation2Name").ToString.Replace("|", "<BR>")
    '                If dRow.Item("Relation1").ToString.Trim.Length > 0 Then
    '                    xRelation1Tot += CInt(dRow.Item("Relation1"))
    '                End If
    '                If dRow.Item("Relation2").ToString.Trim.Length > 0 Then
    '                    xRelation2Tot += CInt(dRow.Item("Relation2"))
    '                End If
    '                xTotalDep += CInt(dRow.Item("TotalDep"))
    '                mdtExcelTable.ImportRow(dRow)
    '                Dim xTemp() As DataRow = dsProvider.Tables("DataTable").Select("EmpId = '" & dRow.Item("employeeunkid") & "'")
    '                If xTemp.Length > 0 Then
    '                    For index As Integer = 0 To xTemp.Length - 1
    '                        mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Col_" & xTemp(index).Item("PovId")) = 1
    '                        If xProviderTot.ContainsKey("Col_" & xTemp(index).Item("PovId")) Then
    '                            xProviderTot("Col_" & xTemp(index).Item("PovId")) += 1
    '                        Else
    '                            xProviderTot.Add("Col_" & xTemp(index).Item("PovId"), 1)
    '                        End If
    '                    Next
    '                End If
    '            Next
    '            mdtExcelTable.AcceptChanges()
    '            mdtExcelTable.Rows.Add(mdtExcelTable.NewRow)

    '            mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("ECode") = Language.getMessage(mstrModuleName, 13, "Sub Total :")
    '            mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Relation1") = xRelation1Tot
    '            mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Relation2") = xRelation2Tot
    '            mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("TotalDep") = xTotalDep
    '            For Each xKey As String In xProviderTot.Keys
    '                mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item(xKey) = xProviderTot(xKey)
    '            Next
    '            If GenerateDetailPart(xStringBuilder, mdtExcelTable, xAllocationName, CsvEmpIds) = False Then
    '                Exit Function
    '            End If
    '        Next
    '        xStringBuilder.Append(" </BODY> " & vbCrLf)
    '        xStringBuilder.Append(" </HTML> " & vbCrLf)

    '        Dim blnFlag As Boolean = False

    '        blnFlag = Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), mstrExportReportPath, xStringBuilder)

    '        If blnFlag Then
    '            If mstrExportReportPath.LastIndexOf("\") = mstrExportReportPath.Length - 1 Then
    '                mstrExportReportPath = mstrExportReportPath.Remove(mstrExportReportPath.LastIndexOf("\"))
    '            End If
    '            Call ReportFunction.Open_ExportedFile(mblnOpenAfterExport, mstrExportReportPath & "\" & Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls")
    '        End If

    '        Return blnFlag
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    Public Function Generate_DetailReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                           , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date, ByVal mblnIncludeInactiveEmp As Boolean) As Boolean
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            Call Generate_Table()
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)

            Dim dsProvider As New DataSet

            StrQ = "SELECT " & _
                   "     employeeunkid AS EmpId " & _
                   "    ,providerunkid AS PovId " & _
                   "FROM mdmedical_cover " & _
                   "    JOIN mdcategory_providermapping ON mdcategory_providermapping.mdcategorymasterunkid = mdmedical_cover.medicalcategoryunkid " & _
                   "WHERE mdmedical_cover.isvoid = 0 AND mdcategory_providermapping.isvoid = 0 " & _
                   "    AND (@startdate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate) " & _
                   "    OR @enddate BETWEEN ISNULL(CONVERT(CHAR(8),mdmedical_cover.startdate,112),@startdate) AND ISNULL(CONVERT(CHAR(8),mdmedical_cover.enddate,112),@enddate)) " & _
                   "AND providerunkid IN(" & mstrServiceProviderIDs & ") "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call FilterTitleAndFilterQuery()
            Call FilterTitleAndFilterQuery(xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            dsProvider = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If


            'Pinkal (04-Jan-2020) -- Start
            'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
            'If mstrViewAllocationIds.Trim.Length > 0 Then Call GenerateHeader()
            GenerateHeader()
            'Pinkal (04-Jan-2020) -- End



            Dim xAllocationName As String = String.Empty
            For Each xAllocationId As String In mstrViewAllocationIds.Split(",")
                objDataOperation.ClearParameters()
                mdtExcelTable.Rows.Clear()
                mdtSummTable.Rows.Clear()

                mdtSummTable = mdtSummTableClone.Copy

                Select Case mintViewAllocationRefId
                    Case enAllocation.BRANCH
                        Dim objBranch As New clsStation
                        objBranch._Stationunkid = CInt(xAllocationId)
                        xAllocationName = objBranch._Name
                        objBranch = Nothing

                    Case enAllocation.DEPARTMENT_GROUP
                        Dim objDeptGrp As New clsDepartmentGroup
                        objDeptGrp._Deptgroupunkid = CInt(xAllocationId)
                        xAllocationName = objDeptGrp._Name
                        objDeptGrp = Nothing

                    Case enAllocation.DEPARTMENT
                        Dim objDept As New clsDepartment
                        objDept._Departmentunkid = CInt(xAllocationId)
                        xAllocationName = objDept._Name
                        objDept = Nothing

                    Case enAllocation.SECTION_GROUP
                        Dim objSecGrp As New clsSectionGroup
                        objSecGrp._Sectiongroupunkid = CInt(xAllocationId)
                        xAllocationName = objSecGrp._Name
                        objSecGrp = Nothing

                    Case enAllocation.SECTION
                        Dim objSec As New clsSections
                        objSec._Sectionunkid = CInt(xAllocationId)
                        xAllocationName = objSec._Name
                        objSec = Nothing

                    Case enAllocation.UNIT_GROUP
                        Dim objUnitGrp As New clsUnitGroup
                        objUnitGrp._Unitgroupunkid = CInt(xAllocationId)
                        xAllocationName = objUnitGrp._Name
                        objUnitGrp = Nothing

                    Case enAllocation.UNIT
                        Dim objUnit As New clsUnits
                        objUnit._Unitunkid = CInt(xAllocationId)
                        xAllocationName = objUnit._Name
                        objUnit = Nothing

                    Case enAllocation.TEAM
                        Dim objTeam As New clsTeams
                        objTeam._Teamunkid = CInt(xAllocationId)
                        xAllocationName = objTeam._Name
                        objTeam = Nothing

                    Case enAllocation.CLASS_GROUP
                        Dim objClsGrp As New clsClassGroup
                        objClsGrp._Classgroupunkid = CInt(xAllocationId)
                        xAllocationName = objClsGrp._Name
                        objClsGrp = Nothing

                    Case enAllocation.CLASSES
                        Dim objCls As New clsClass
                        objCls._Classesunkid = CInt(xAllocationId)
                        xAllocationName = objCls._Name
                        objCls = Nothing

                End Select

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ = "SELECT * " & _
                           "INTO #TableDepn " & _
                           "FROM " & _
                           "( " & _
                               "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                    ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                               "FROM hrdependant_beneficiaries_status_tran "

                StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

                If mdtEmployeeAsonDate <> Nothing Then
                    StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' "
                Else
                    'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
                End If

                StrQ &= ") AS A " & _
                        "WHERE 1 = 1 " & _
                        " AND A.ROWNO = 1 "
                'Sohail (18 May 2019) -- End

                StrQ &= "SELECT " & _
                       "     employeecode AS ECode "
                If mblnFirstNameThenLastName = True Then
                    StrQ &= "    ,firstname + ' ' + surname AS Employee "
                Else
                    StrQ &= "    ,surname + ' ' + firstname AS Employee "
                End If
                StrQ &= "    ,hrdepartment_master.name AS Department " & _
                        "    ,job_name AS Job " & _
                        "    ,CASE WHEN gender = 1 THEN @M WHEN gender = 2 THEN @F ELSE '' END AS Gender " & _
                        "    ,ISNULL(cfcommon_master.name,'') AS MStatus " & _
                        "    ,ISNULL(CAST(R1.Relation1 AS NVARCHAR(MAX)),'') AS Relation1 " & _
                        "    ,ISNULL(CAST(R2.Relation2 AS NVARCHAR(MAX)),'') AS Relation2 " & _
                        "    ,ISNULL(R1.Relation1,0)+ISNULL(R2.Relation2,0) AS TotalDep " & _
                        "    ,ISNULL(R1.Relation1Name,'') AS Relation1Name " & _
                        "    ,ISNULL(R1.Relation1DOB, '') AS Relation1DOB " & _
                        "    ,ISNULL(R1.Relation1Gender,'') AS Relation1Gender " & _
                        "    ,ISNULL(R2.Relation2Name,'') AS Relation2Name " & _
                        "    ,ISNULL(R2.Relation2DOB, '') AS Relation2DOB " & _
                        "    ,ISNULL(R2.Relation2Gender,'') AS Relation2Gender " & _
                        "    ,hremployee_master.employeeunkid " & _
                        "    ,ROW_NUMBER() OVER(ORDER BY hremployee_master.employeeunkid) AS SrNo " & _
                        "FROM hremployee_master " & _
                        "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.maritalstatusunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.MARRIED_STATUS & "' " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "      SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        "   JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "    SELECT " & _
                        "         jobunkid " & _
                        "        ,jobgroupunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        "    JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             employeeunkid AS R1EmpId " & _
                        "            ,COUNT(hrdependants_beneficiaries_tran.dpndtbeneficetranunkid) AS Relation1 " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ t1.first_name+' '+t1.last_name FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND t1.isvoid = 0 AND relationunkid = '" & mintSpouseRelationId & "' AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation1Name " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ CASE WHEN t1.birthdate IS NOT NULL THEN REPLACE(convert(Char(11),t1.birthdate,106),' ','-') ELSE '' END FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND relationunkid = '" & mintSpouseRelationId & "' AND t1.isvoid = 0 AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation1DOB " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ CASE WHEN t1.gender = 1 THEN @M WHEN gender = 2 THEN @F ELSE '' END FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND relationunkid = '" & mintSpouseRelationId & "' AND t1.isvoid = 0 AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation1Gender " & _
                        "        FROM hrdependants_beneficiaries_tran " & _
                        "        JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                        "        WHERE hrdependants_beneficiaries_tran.isvoid = 0 AND relationunkid = '" & mintSpouseRelationId & "' " & _
                        "        AND #TableDepn.isactive = 1 " & _
                        "        GROUP BY employeeunkid " & _
                        "    ) AS R1 ON R1.R1EmpId = hremployee_master.employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             employeeunkid AS R2EmpId " & _
                        "            ,COUNT(hrdependants_beneficiaries_tran.dpndtbeneficetranunkid) Relation2 " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ t1.first_name+' '+t1.last_name FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND t1.isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation2Name " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ CASE WHEN t1.birthdate IS NOT NULL THEN REPLACE(convert(Char(11),t1.birthdate,106),' ','-') ELSE '' END FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND t1.isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation2DOB " & _
                        "            ,STUFF((SELECT '|'+ CAST(ROW_NUMBER() OVER(ORDER BY t1.dpndtbeneficetranunkid) AS NVARCHAR(MAX)) + '. '+ CASE WHEN t1.gender = 1 THEN @M WHEN gender = 2 THEN @F ELSE '' END FROM hrdependants_beneficiaries_tran AS t1 JOIN #TableDepn ON t1.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid WHERE hrdependants_beneficiaries_tran.employeeunkid = t1.employeeunkid AND t1.isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' AND #TableDepn.isactive = 1 FOR XML PATH('')),1,1,'') AS Relation2Gender " & _
                        "        FROM hrdependants_beneficiaries_tran " & _
                        "        JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                        "        WHERE hrdependants_beneficiaries_tran.isvoid = 0 AND relationunkid = '" & mintChildRelationId & "' " & _
                        "        AND #TableDepn.isactive = 1 " & _
                        "        GROUP BY employeeunkid " & _
                        "    ) AS R2 ON R2.R2EmpId = hremployee_master.employeeunkid "
                'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]


                'Gajanan [31-JUL-2019] -- START
                'Defect  [0003054 : FINCA DRC] : Medical cover detail report does not filter by service provider.



                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If
                'StrQ &= "WHERE 1 = 1 "

                'Gajanan [31-JUL-2019] -- END


                'If mblnIncludeInactiveEmp = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If


                'Select Case mintViewAllocationRefId
                '    Case enAllocation.BRANCH
                '        StrQ &= "AND hremployee_master.stationunkid = '" & xAllocationId & "' "
                '    Case enAllocation.DEPARTMENT_GROUP
                '        StrQ &= "AND hremployee_master.deptgroupunkid = '" & xAllocationId & "' "
                '    Case enAllocation.DEPARTMENT
                '        StrQ &= "AND hremployee_master.departmentunkid = '" & xAllocationId & "' "
                '    Case enAllocation.SECTION_GROUP
                '        StrQ &= "AND hremployee_master.sectiongroupunkid = '" & xAllocationId & "' "
                '    Case enAllocation.SECTION
                '        StrQ &= "AND hremployee_master.sectionunkid = '" & xAllocationId & "' "
                '    Case enAllocation.UNIT_GROUP
                '        StrQ &= "AND hremployee_master.unitgroupunkid = '" & xAllocationId & "' "
                '    Case enAllocation.UNIT
                '        StrQ &= "AND hremployee_master.unitunkid = '" & xAllocationId & "' "
                '    Case enAllocation.TEAM
                '        StrQ &= "AND hremployee_master.teamunkid = '" & xAllocationId & "' "
                '    Case enAllocation.CLASS_GROUP
                '        StrQ &= "AND hremployee_master.classgroupunkid = '" & xAllocationId & "' "
                '    Case enAllocation.CLASSES
                '        StrQ &= "AND hremployee_master.classunkid = '" & xAllocationId & "' "
                'End Select


                If mblnIncludeInactiveEmp = False Then

                    'Gajanan [31-JUL-2019] -- START
                    'Defect  [0003054 : FINCA DRC] : Medical cover detail report does not filter by service provider.
                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If
                    StrQ &= "WHERE 1 = 1 "

                    'Gajanan [31-JUL-2019] -- END

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                Select Case mintViewAllocationRefId
                    Case enAllocation.BRANCH
                        StrQ &= "AND Alloc.stationunkid = '" & xAllocationId & "' "
                    Case enAllocation.DEPARTMENT_GROUP
                        StrQ &= "AND Alloc.deptgroupunkid = '" & xAllocationId & "' "
                    Case enAllocation.DEPARTMENT
                        StrQ &= "AND Alloc.departmentunkid = '" & xAllocationId & "' "
                    Case enAllocation.SECTION_GROUP
                        StrQ &= "AND Alloc.sectiongroupunkid = '" & xAllocationId & "' "
                    Case enAllocation.SECTION
                        StrQ &= "AND Alloc.sectionunkid = '" & xAllocationId & "' "
                    Case enAllocation.UNIT_GROUP
                        StrQ &= "AND Alloc.unitgroupunkid = '" & xAllocationId & "' "
                    Case enAllocation.UNIT
                        StrQ &= "AND Alloc.unitunkid = '" & xAllocationId & "' "
                    Case enAllocation.TEAM
                        StrQ &= "AND Alloc.teamunkid = '" & xAllocationId & "' "
                    Case enAllocation.CLASS_GROUP
                        StrQ &= "AND Alloc.classgroupunkid = '" & xAllocationId & "' "
                    Case enAllocation.CLASSES
                        StrQ &= "AND Alloc.classunkid = '" & xAllocationId & "' "
                End Select

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call FilterTitleAndFilterQuery()
                Call FilterTitleAndFilterQuery(xDatabaseName)
                'Sohail (21 Aug 2015) -- End

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ &= " DROP TABLE #TableDepn "
                'Sohail (18 May 2019) -- End

                dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                Dim CsvEmpIds As String = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())

                Dim xRelation1Tot, xRelation2Tot, xTotalDep As Integer : Dim xProviderTot As New Dictionary(Of String, Integer)
                xRelation1Tot = 0 : xRelation2Tot = 0 : xTotalDep = 0
                For Each dRow As DataRow In dsList.Tables("DataTable").Rows
                    dRow.Item("Relation1Name") = dRow.Item("Relation1Name").ToString.Replace("|", "<BR>")
                    dRow.Item("Relation2Name") = dRow.Item("Relation2Name").ToString.Replace("|", "<BR>")

                    'Pinkal (04-Jan-2020) -- Start
                    'Enhancement - FINCA DRC [0003074] - Medical cover detail report Enhancement.
                    dRow.Item("Relation1DOB") = dRow.Item("Relation1DOB").ToString.Replace("|", "<BR>")
                    dRow.Item("Relation1Gender") = dRow.Item("Relation1Gender").ToString.Replace("|", "<BR>")
                    dRow.Item("Relation2DOB") = dRow.Item("Relation2DOB").ToString.Replace("|", "<BR>")
                    dRow.Item("Relation2Gender") = dRow.Item("Relation2Gender").ToString.Replace("|", "<BR>")
                    'Pinkal (04-Jan-2020) -- End

                    If dRow.Item("Relation1").ToString.Trim.Length > 0 Then
                        xRelation1Tot += CInt(dRow.Item("Relation1"))
                    End If
                    If dRow.Item("Relation2").ToString.Trim.Length > 0 Then
                        xRelation2Tot += CInt(dRow.Item("Relation2"))
                    End If
                    xTotalDep += CInt(dRow.Item("TotalDep"))
                    mdtExcelTable.ImportRow(dRow)
                    Dim xTemp() As DataRow = dsProvider.Tables("DataTable").Select("EmpId = '" & dRow.Item("employeeunkid") & "'")
                    If xTemp.Length > 0 Then
                        For index As Integer = 0 To xTemp.Length - 1
                            mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Col_" & xTemp(index).Item("PovId")) = 1
                            If xProviderTot.ContainsKey("Col_" & xTemp(index).Item("PovId")) Then
                                xProviderTot("Col_" & xTemp(index).Item("PovId")) += 1
                            Else
                                xProviderTot.Add("Col_" & xTemp(index).Item("PovId"), 1)
                            End If
                        Next
                    End If
                Next
                mdtExcelTable.AcceptChanges()



                'Gajanan [31-JUL-2019] -- START
                'Defect  [0003054 : FINCA DRC] : Medical cover detail report does not filter by service provider.
                Dim irow As DataRow()
                Dim strFilter = String.Empty
                For Each item As KeyValuePair(Of String, Integer) In xProviderTot
                    If strFilter.Length > 0 Then
                        strFilter &= "and " & item.Key & "= '' "
                    Else
                        strFilter = " " & item.Key & "= '' "
                    End If
                Next
                irow = mdtExcelTable.Select(strFilter)
                For index As Integer = 0 To irow.Length - 1
                    mdtExcelTable.Rows.Remove(irow(index))
                    mdtExcelTable.AcceptChanges()
                Next


                For i As Integer = 0 To mdtExcelTable.Rows.Count - 1
                    mdtExcelTable.Rows(i)("SrNo") = i + 1
                Next


                'Gajanan [31-JUL-2019] -- END


                mdtExcelTable.Rows.Add(mdtExcelTable.NewRow)

                mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("ECode") = Language.getMessage(mstrModuleName, 13, "Sub Total :")
                mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Relation1") = xRelation1Tot
                mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("Relation2") = xRelation2Tot
                mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item("TotalDep") = xTotalDep
                For Each xKey As String In xProviderTot.Keys
                    mdtExcelTable.Rows(mdtExcelTable.Rows.Count - 1).Item(xKey) = xProviderTot(xKey)
                Next
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If GenerateDetailPart(xStringBuilder, mdtExcelTable, xAllocationName, CsvEmpIds) = False Then
                If GenerateDetailPart(xStringBuilder, mdtExcelTable, xAllocationName, xDatabaseName, CsvEmpIds) = False Then
                    'Sohail (21 Aug 2015) -- End
                    Exit Function
                End If
            Next
            xStringBuilder.Append(" </BODY> " & vbCrLf)
            xStringBuilder.Append(" </HTML> " & vbCrLf)

            Dim blnFlag As Boolean = False

            blnFlag = Export_to_Excel(Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss"), mstrExportReportPath, xStringBuilder)

            If blnFlag Then
                If mstrExportReportPath.LastIndexOf("\") = mstrExportReportPath.Length - 1 Then
                    mstrExportReportPath = mstrExportReportPath.Remove(mstrExportReportPath.LastIndexOf("\"))
                End If
                Call ReportFunction.Open_ExportedFile(mblnOpenAfterExport, mstrExportReportPath & "\" & Me._ReportName.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls")
            End If

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period:")
            Language.setMessage(mstrModuleName, 2, "Nr")
            Language.setMessage(mstrModuleName, 3, "Employee #")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Job Title")
            Language.setMessage(mstrModuleName, 7, "Gender")
            Language.setMessage(mstrModuleName, 8, "Marital Status")
            Language.setMessage(mstrModuleName, 9, "Total Dependent")
            Language.setMessage(mstrModuleName, 10, "Name")
            Language.setMessage(mstrModuleName, 11, "M")
            Language.setMessage(mstrModuleName, 12, "F")
            Language.setMessage(mstrModuleName, 13, "Sub Total :")
            Language.setMessage(mstrModuleName, 14, "Total")
            Language.setMessage(mstrModuleName, 15, "Compiled By :")
            Language.setMessage(mstrModuleName, 16, "Reviewed By :")
            Language.setMessage(mstrModuleName, 17, "Approved By :")
            Language.setMessage(mstrModuleName, 18, "Approved By :")
            Language.setMessage(mstrModuleName, 19, "Date :")
            Language.setMessage(mstrModuleName, 20, "Prepared By :")
            Language.setMessage(mstrModuleName, 21, "BirthDate")
            Language.setMessage(mstrModuleName, 22, "Gender")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

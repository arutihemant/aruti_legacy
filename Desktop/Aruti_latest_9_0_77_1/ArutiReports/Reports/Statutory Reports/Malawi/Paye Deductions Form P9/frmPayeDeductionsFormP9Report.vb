'************************************************************************************************************************************
'Class Name : frmPayeDeductionsFormP9Report.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayeDeductionsFormP9Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayeDeductionsFormP9Report"
    Private objPayeP9 As clsPayeDeductionsFormP9Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintYearUnkId As Integer = Nothing
    Private mstrDatabaseName As String
    Private mdtDatabaseStartDate As DateTime = Nothing
    Private mdtDatabaseEndDate As DateTime = Nothing
    Private mstrSearchText As String = ""
    Private dvPeriod As DataView
    Private mstrSearchHeadText As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objPayeP9 = New clsPayeDeductionsFormP9Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPayeP9.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Relation = 1
        Paye = 2
        GrossBasic = 3
        HouseAllowance = 4
        FuelLight = 5
        EduAllowance = 6
        OtherBenefit = 7
        LeavePassage = 8
        Gratuity = 9
        Pension = 10
        TaxFreeHouseAllowance = 11
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objTranHead As New clsTransactionHead
        Dim objCommon As New clsCommon_Master
        Dim objCompany As New clsCompany_Master
        Dim dsCombos As New DataSet
        Try

            dsCombos = objCompany.GetFinancialYearList(Company._Object._Companyunkid, -1, "List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables("List"), " end_date <= '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date) & "' ", "end_date DESC ", DataViewRowState.CurrentRows).ToTable
            With cboFYear
                .ValueMember = "yearunkid"
                .DisplayMember = "financialyear_name"
                .DataSource = dtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "List")
            With cboRelation
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With


            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True)
            With cboGrossBasic
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGrossBasic)

            With cboHouseAllowance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboHouseAllowance)

            With cboFuelLight
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboFuelLight)

            With cboEduAllowance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboEduAllowance)

            With cboOtherBenefit
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboOtherBenefit)

            With cboLeavePassage
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboLeavePassage)

            With cboGratuity
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboGratuity)

            With cboPension
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboPension)

            With cboTaxFreeHouseAllowance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboTaxFreeHouseAllowance)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objTranHead = Nothing
            objCommon = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objEmp As New clsEmployee_Master
        Dim objCompany As New clsCompany_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim dsCombos As DataSet
        Try
            objCompany._YearUnkid = CInt(cboFYear.SelectedValue)
            mintYearUnkId = CInt(cboFYear.SelectedValue)
            mstrDatabaseName = objCompany._DatabaseName
            mdtDatabaseStartDate = objCompany._Database_Start_Date
            mdtDatabaseEndDate = objCompany._Database_End_Date

            dsCombos = objEmp.GetEmployeeList(mstrDatabaseName, User._Object._Userunkid, CInt(cboFYear.SelectedValue), Company._Object._Companyunkid, mdtDatabaseStartDate, mdtDatabaseEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            Call SetDefaultSearchText(cboEmployee)

            dgPeriod.DataSource = Nothing
            RemoveHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged
            Call SetDefaultSearchPeriodText()
            AddHandler txtSearchPeriod.TextChanged, AddressOf txtSearchPeriod_TextChanged

            dsList = objPeriod.GetList("Period", enModuleReference.Payroll, CInt(cboFYear.SelectedValue), mdtDatabaseStartDate, True, 0, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0)).ToTable

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvPeriod = dtTable.DefaultView
            dgPeriod.AutoGenerateColumns = False
            objdgperiodcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhPeriodunkid.DataPropertyName = "periodunkid"
            dgColhPeriodCode.DataPropertyName = "period_code"
            dgColhPeriod.DataPropertyName = "period_name"
            dgcolhPeriodStart.DataPropertyName = "start_date"
            dgcolhPeriodEnd.DataPropertyName = "end_date"

            dgPeriod.DataSource = dvPeriod
            dvPeriod.Sort = "IsChecked DESC, end_date "

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objEmp = Nothing
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub SetDefaultSearchPeriodText()
        Try
            mstrSearchHeadText = Language.getMessage(mstrModuleName, 5, "Type to Search")
            With txtSearchPeriod
                .ForeColor = Color.Gray
                .Text = mstrSearchHeadText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchPeriodText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgPeriod.CurrentRow.Cells(objdgperiodcolhCheck.Index).Value)

            Dim intCount As Integer = dvPeriod.Table.Select("IsChecked = 1").Length

            If intCount <= 0 Then
                objchkPeriodSelectAll.CheckState = CheckState.Unchecked
            ElseIf intCount < dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Indeterminate
            ElseIf intCount = dgPeriod.Rows.Count Then
                objchkPeriodSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkPeriodSelectAll.CheckedChanged, AddressOf objchkPeriodSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub CheckAllHead(ByVal blnCheckAll As Boolean)
        Try
            If dvPeriod IsNot Nothing Then
                For Each dr As DataRowView In dvPeriod
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvPeriod.ToTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllHead", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboRelation.SelectedValue = 0

            cboPAYE.SelectedValue = 0
            cboGrossBasic.SelectedValue = 0
            cboHouseAllowance.SelectedValue = 0
            cboFuelLight.SelectedValue = 0
            cboEduAllowance.SelectedValue = 0
            cboOtherBenefit.SelectedValue = 0
            cboLeavePassage.SelectedValue = 0
            cboGratuity.SelectedValue = 0
            cboPension.SelectedValue = 0
            cboTaxFreeHouseAllowance.SelectedValue = 0
            cboOtherBenefit.SelectedValue = 0

            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""

            Call GetValue()

            Call SetDefaultSearchPeriodText()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objPayeP9.SetDefaultValue()

            If cboFYear.Items.Count <= 0 OrElse CInt(cboFYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, There is no closed financial year selected."), enMsgBoxStyle.Information)
                cboFYear.Focus()
                Return False
            ElseIf CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            ElseIf CInt(cboRelation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Relation is mandatory information. Please select Relation."), enMsgBoxStyle.Information)
                cboRelation.Focus()
                Return False
            ElseIf dvPeriod.Table.Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select atleast one Period."), enMsgBoxStyle.Information)
                objchkPeriodSelectAll.Focus()
                Return False
            End If

            'If CInt(cboGrossBasic.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is mandatory information. Please select Period"), enMsgBoxStyle.Information)
            '    cboGrossBasic.Focus()
            '    Return False
            'ElseIf CInt(cboHouseAllowance.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Tax Adjusted Head is mandatory information. Please select Tax Adjusted Head."), enMsgBoxStyle.Information)
            '    cboHouseAllowance.Focus()
            '    Return False

            'ElseIf CInt(cboFuelLight.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Membership is mandatory information. Please select Membership."), enMsgBoxStyle.Information)
            '    cboFuelLight.Focus()
            '    Return False
            'ElseIf chkShowIdentity.Checked = True AndAlso CInt(cboEduAllowance.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Identity is mandatory information. Please select Identity."), enMsgBoxStyle.Information)
            '    cboEduAllowance.Focus()
            '    Return False
            'End If

            objPayeP9._EmployeeID = CInt(cboEmployee.SelectedValue)
            objPayeP9._EmployeeName = cboEmployee.Text
            objPayeP9._RelationID = CInt(cboRelation.SelectedValue)
            objPayeP9._RelationName = cboRelation.Text
            objPayeP9._PayeHeadID = CInt(cboPAYE.SelectedValue)
            objPayeP9._PayeHeadName = cboPAYE.Text
            objPayeP9._GrossBasicHeadID = CInt(cboGrossBasic.SelectedValue)
            objPayeP9._GrossBasicHeadName = cboGrossBasic.Text
            objPayeP9._HouseAllowanceHeadID = CInt(cboHouseAllowance.SelectedValue)
            objPayeP9._HouseAllowanceHeadName = cboHouseAllowance.Text
            objPayeP9._FuelLightHeadID = CInt(cboFuelLight.SelectedValue)
            objPayeP9._FuelLightHeadName = cboFuelLight.Text
            objPayeP9._EduAllowanceHeadID = CInt(cboEduAllowance.SelectedValue)
            objPayeP9._EduAllowanceHeadName = cboEduAllowance.Text
            objPayeP9._OtherBenefitHeadID = CInt(cboOtherBenefit.SelectedValue)
            objPayeP9._OtherBenefitHeadName = cboOtherBenefit.Text
            objPayeP9._LeavePassageHeadID = CInt(cboLeavePassage.SelectedValue)
            objPayeP9._LeavePassageHeadName = cboLeavePassage.Text
            objPayeP9._GratuityHeadID = CInt(cboGratuity.SelectedValue)
            objPayeP9._GratuityHeadName = cboGratuity.Text
            objPayeP9._PensionHeadID = CInt(cboPension.SelectedValue)
            objPayeP9._PensionHeadName = cboPension.Text
            objPayeP9._TaxFreeHouseAllowanceHeadID = CInt(cboTaxFreeHouseAllowance.SelectedValue)
            objPayeP9._TaxFreeHouseAllowanceHeadName = cboTaxFreeHouseAllowance.Text

            objPayeP9._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objPayeP9._FmtCurrency = GUI.fmtCurrency

            Dim strIDs As String = String.Join(",", (From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("periodunkid").ToString)).ToArray)
            If strIDs.Trim.Length <= 0 Then
                strIDs = " 0 "
            End If
            objPayeP9._PeriodIDs = strIDs
            objPayeP9._PeriodNames = String.Join(",", (From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("period_name").ToString)).ToArray)
            objPayeP9._PeriodStartDate = eZeeDate.convertDate((From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("start_date").ToString)).FirstOrDefault)
            objPayeP9._PeriodEndDate = eZeeDate.convertDate((From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("end_date").ToString)).LastOrDefault)
            objPayeP9._DatabaseEndDate = mdtDatabaseEndDate

            objPayeP9._ViewByIds = mstrStringIds
            objPayeP9._ViewIndex = mintViewIdx
            objPayeP9._ViewByName = mstrStringName
            objPayeP9._Analysis_Fields = mstrAnalysis_Fields
            objPayeP9._Analysis_Join = mstrAnalysis_Join
            objPayeP9._Analysis_OrderBy = mstrAnalysis_OrderBy_GName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPayeP9._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate((From p In dvPeriod.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("end_date").ToString)).LastOrDefault))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PAYE_DEDUCTION_FORM_P9_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case CInt(enHeadTypeId.Relation)
                            cboRelation.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Paye)
                            cboPAYE.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.GrossBasic)
                            cboGrossBasic.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.HouseAllowance)
                            cboHouseAllowance.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.FuelLight)
                            cboFuelLight.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.EduAllowance)
                            cboEduAllowance.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.OtherBenefit)
                            cboOtherBenefit.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.LeavePassage)
                            cboLeavePassage.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Gratuity)
                            cboGratuity.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.Pension)
                            cboPension.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case CInt(enHeadTypeId.TaxFreeHouseAllowance)
                            cboTaxFreeHouseAllowance.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 5, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmPayeDeductionsFormP9Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayeP9 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayeDeductionsFormP9Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayeDeductionsFormP9Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objPayeP9._ReportName
            Me._Message = objPayeP9._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayeDeductionsFormP9Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            objPayeP9.generateReportNew(mstrDatabaseName, _
                                         User._Object._Userunkid, _
                                         mintYearUnkId, _
                                         Company._Object._Companyunkid, _
                                         mdtDatabaseStartDate, _
                                         mdtDatabaseEndDate, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            objPayeP9.generateReportNew(mstrDatabaseName, _
                                         User._Object._Userunkid, _
                                         mintYearUnkId, _
                                         Company._Object._Companyunkid, _
                                         mdtDatabaseStartDate, _
                                         mdtDatabaseEndDate, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayeDeductionsFormP9Report.SetMessages()
            objfrm._Other_ModuleNames = "clsPayeDeductionsFormP9Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.PAYE_DEDUCTION_FORM_P9_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case CInt(enHeadTypeId.Relation)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboRelation.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.Paye)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboPAYE.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.GrossBasic)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboGrossBasic.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.HouseAllowance)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboHouseAllowance.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.FuelLight)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboFuelLight.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.EduAllowance)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboEduAllowance.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.OtherBenefit)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboOtherBenefit.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.LeavePassage)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboLeavePassage.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.Gratuity)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboGratuity.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.Pension)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboPension.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                    Case CInt(enHeadTypeId.TaxFreeHouseAllowance)
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboTaxFreeHouseAllowance.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_DEDUCTION_FORM_P9_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboFYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFYear.SelectedIndexChanged
        Try
            If CInt(cboFYear.SelectedValue) > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress, _
                                                                                                                cboRelation.KeyPress, cboPAYE.KeyPress, _
                                                                                                                cboGrossBasic.KeyPress, cboHouseAllowance.KeyPress, _
                                                                                                                cboFuelLight.KeyPress, cboEduAllowance.KeyPress, cboOtherBenefit.KeyPress, _
                                                                                                                cboLeavePassage.KeyPress, cboGratuity.KeyPress, cboPension.KeyPress, cboTaxFreeHouseAllowance.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    ElseIf cbo.Name = cboRelation.Name Then
                        .CodeMember = "description"
                    Else
                        .CodeMember = "code"
                    End If

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, _
                                                                                                      cboRelation.SelectedIndexChanged, cboPAYE.SelectedIndexChanged, _
                                                                                                      cboGrossBasic.SelectedIndexChanged, cboHouseAllowance.SelectedIndexChanged, _
                                                                                                      cboFuelLight.SelectedIndexChanged, cboEduAllowance.SelectedIndexChanged, cboOtherBenefit.SelectedIndexChanged, _
                                                                                                      cboLeavePassage.SelectedIndexChanged, cboGratuity.SelectedIndexChanged, cboPension.SelectedIndexChanged, cboTaxFreeHouseAllowance.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus, _
                                                                                          cboRelation.GotFocus, cboPAYE.GotFocus, _
                                                                                          cboGrossBasic.GotFocus, cboHouseAllowance.GotFocus, _
                                                                                          cboFuelLight.GotFocus, cboEduAllowance.GotFocus, cboOtherBenefit.GotFocus, _
                                                                                          cboLeavePassage.GotFocus, cboGratuity.GotFocus, cboPension.GotFocus, cboTaxFreeHouseAllowance.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave, _
                                                                                       cboGrossBasic.Leave, cboHouseAllowance.Leave, _
                                                                                       cboFuelLight.Leave, cboEduAllowance.Leave, cboOtherBenefit.Leave, _
                                                                                       cboLeavePassage.Leave, cboGratuity.Leave, cboPension.Leave, cboTaxFreeHouseAllowance.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPAYE_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboPAYE.Validating, cboGrossBasic.Validating, cboHouseAllowance.Validating, _
                                                                                                                        cboFuelLight.Validating, cboEduAllowance.Validating, cboOtherBenefit.Validating, _
                                                                                                                        cboLeavePassage.Validating, cboGratuity.Validating, cboPension.Validating, cboTaxFreeHouseAllowance.Validating
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboFYear.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboRelation.Name AndAlso t.Name <> cboPAYE.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Validating", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub gbFilterCriteria_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles gbFilterCriteria.Scroll
        Try
            gbFilterCriteria.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbFilterCriteria_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objchkPeriodSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkPeriodSelectAll.CheckedChanged
        Try
            Call CheckAllHead(objchkPeriodSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkPeriodSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.GotFocus
        Try
            With txtSearchPeriod
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchHeadText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.Leave
        Try
            If txtSearchPeriod.Text.Trim = "" Then
                Call SetDefaultSearchPeriodText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchPeriod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchPeriod.TextChanged
        Try
            If txtSearchPeriod.Text.Trim = mstrSearchHeadText Then Exit Sub
            If dvPeriod IsNot Nothing Then
                dvPeriod.RowFilter = "period_code LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'  OR period_name LIKE '%" & txtSearchPeriod.Text.Replace("'", "''") & "%'"
                dgPeriod.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchPeriod_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)
			Me.lblGrossBasic.Text = Language._Object.getCaption(Me.lblGrossBasic.Name, Me.lblGrossBasic.Text)
            Me.lblFYear.Text = Language._Object.getCaption(Me.lblFYear.Name, Me.lblFYear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblFuelLight.Text = Language._Object.getCaption(Me.lblFuelLight.Name, Me.lblFuelLight.Text)
			Me.lblHouseAllowance.Text = Language._Object.getCaption(Me.lblHouseAllowance.Name, Me.lblHouseAllowance.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblEduAllowance.Text = Language._Object.getCaption(Me.lblEduAllowance.Name, Me.lblEduAllowance.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)
			Me.lblOtherBenefit.Text = Language._Object.getCaption(Me.lblOtherBenefit.Name, Me.lblOtherBenefit.Text)
			Me.lblTaxFreeHouseAllowance.Text = Language._Object.getCaption(Me.lblTaxFreeHouseAllowance.Name, Me.lblTaxFreeHouseAllowance.Text)
			Me.lblPension.Text = Language._Object.getCaption(Me.lblPension.Name, Me.lblPension.Text)
			Me.lblGratuity.Text = Language._Object.getCaption(Me.lblGratuity.Name, Me.lblGratuity.Text)
			Me.lblLeavePassage.Text = Language._Object.getCaption(Me.lblLeavePassage.Name, Me.lblLeavePassage.Text)
			Me.dgColhPeriodCode.HeaderText = Language._Object.getCaption(Me.dgColhPeriodCode.Name, Me.dgColhPeriodCode.HeaderText)
			Me.dgColhPeriod.HeaderText = Language._Object.getCaption(Me.dgColhPeriod.Name, Me.dgColhPeriod.HeaderText)
			Me.dgcolhPeriodStart.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodStart.Name, Me.dgcolhPeriodStart.HeaderText)
			Me.dgcolhPeriodEnd.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodEnd.Name, Me.dgcolhPeriodEnd.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Slab Transaction Head is mandatory information. Please select Slab Transaction Head.")
			Language.setMessage(mstrModuleName, 2, "Relation is mandatory information. Please select Relation.")
			Language.setMessage(mstrModuleName, 3, "Period is mandatory information. Please select atleast one Period.")
			Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 5, "Type to Search")
			Language.setMessage(mstrModuleName, 6, "Sorry, This transaction head is already mapped.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class

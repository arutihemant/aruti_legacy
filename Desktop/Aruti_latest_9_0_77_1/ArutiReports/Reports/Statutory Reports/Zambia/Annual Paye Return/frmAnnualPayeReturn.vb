'************************************************************************************************************************************
'Class Name : frmAnnualPayeReturn.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAnnualPayeReturn

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAnnualPayeReturn"
    Private objAnnualPayeReturn As clsAnnualPayeReturn
    Private mstrEndPeriodName As String = String.Empty
    Private mdtStartPeriodDate As DateTime
    Private mdtEndPeriodDate As DateTime
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

#End Region

#Region " Contructor "

    Public Sub New()
        objAnnualPayeReturn = New clsAnnualPayeReturn(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAnnualPayeReturn.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As New DataSet
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Taxes", True, enTranHeadType.EmployeesStatutoryDeductions, enCalcType.AsComputedOnWithINEXCESSOFTaxSlab, enTypeOf.Taxes)
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With

            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "Employement")
            With cboPermanent
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Employement")
                .SelectedValue = 0
            End With

            With cboTemparory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Employement").Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub DoOperation(ByVal blnOperation As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriod.Items
                lvItem.Checked = blnOperation
            Next
            lvPeriod.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GetCommaSeparatedTags(ByRef mstrIds As String)
        Dim intCnt As Integer = 1
        Try
            For Each lvItem As ListViewItem In lvPeriod.CheckedItems
                mstrIds &= "," & lvItem.Tag.ToString
                If intCnt = lvPeriod.CheckedItems.Count Then
                    mstrEndPeriodName = lvItem.SubItems(colhPeriodName.Index).Text
                    mdtEndPeriodDate = eZeeDate.convertDate(lvItem.SubItems(colhPeriodName.Index).Tag.ToString) 'Sohail (21 Nov 2011)
                End If
                intCnt += 1
            Next
            mstrIds = Mid(mstrIds, 2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparatedTags", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboTranHead.SelectedValue = 0
            cboPermanent.SelectedValue = 0
            cboTemparory.SelectedValue = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAnnualPayeReturn.SetDefaultValue()

            If cboTranHead.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Slab Tax Head is compulsory information.Please select Slab Tax Head to continue."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            End If

            If lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one Period to view report."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
            End If

            If CInt(cboPermanent.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Permanent Employement Type is compulosry information.Please Select Permanent Employement Type."), enMsgBoxStyle.Information)
                cboPermanent.Focus()
                Return False
            End If

            If CInt(cboPermanent.SelectedValue) = CInt(cboTemparory.SelectedValue) AndAlso (CInt(cboPermanent.SelectedValue) > 0 AndAlso CInt(cboTemparory.SelectedValue) > 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Permanent and Temparory Employement Type can't be same."), enMsgBoxStyle.Information)
                cboPermanent.Focus()
                Return False
            End If

            objAnnualPayeReturn._SlabTranId = cboTranHead.SelectedValue

            Dim strPreriodIds As String = ""
            Call GetCommaSeparatedTags(strPreriodIds)
            objAnnualPayeReturn._PeriodIds = strPreriodIds
            objAnnualPayeReturn._EndPeriodName = mstrEndPeriodName
            objAnnualPayeReturn._EndPeriodDate = mdtEndPeriodDate
            objAnnualPayeReturn._PeriodCount = lvPeriod.CheckedItems.Count
            objAnnualPayeReturn._PermanantEmpTypeID = CInt(cboPermanent.SelectedValue)
            objAnnualPayeReturn._PermanantEmpType = cboPermanent.Text.Trim
            objAnnualPayeReturn._TemparoryEmpTypeID = CInt(cboTemparory.SelectedValue)
            objAnnualPayeReturn._TemparoryEmpType = cboTemparory.Text.Trim
            objAnnualPayeReturn._ViewByIds = mstrStringIds
            objAnnualPayeReturn._ViewIndex = mintViewIdx
            objAnnualPayeReturn._ViewByName = mstrStringName
            objAnnualPayeReturn._Analysis_Fields = mstrAnalysis_Fields
            objAnnualPayeReturn._Analysis_Join = mstrAnalysis_Join
            objAnnualPayeReturn._Report_GroupName = mstrReport_GroupName
            objAnnualPayeReturn._Analysis_OrderBy = mstrAnalysis_OrderBy_GName

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAnnualPayeReturn_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAnnualPayeReturn = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAnnualPayeReturn_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAnnualPayeReturn_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Me._Title = objAnnualPayeReturn._ReportName
            Me._Message = objAnnualPayeReturn._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAnnualPayeReturn_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAnnualPayeReturn.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objAnnualPayeReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                      User._Object._Userunkid, _
            '                                      FinancialYear._Object._YearUnkid, _
            '                                      Company._Object._Companyunkid, _
            '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                      ConfigParameter._Object._UserAccessModeSetting, _
            '                                      True, ConfigParameter._Object._ExportReportPath, _
            '                                      ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objAnnualPayeReturn._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objAnnualPayeReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                                  eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAnnualPayeReturn.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objAnnualPayeReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                      User._Object._Userunkid, _
            '                                      FinancialYear._Object._YearUnkid, _
            '                                      Company._Object._Companyunkid, _
            '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                      ConfigParameter._Object._UserAccessModeSetting, _
            '                                      True, ConfigParameter._Object._ExportReportPath, _
            '                                      ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objAnnualPayeReturn._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max)

            objAnnualPayeReturn.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                                  eZeeDate.convertDate((From lv In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, ConfigParameter._Object._ExportReportPath, _
                                                  ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAnnualPayeReturn.SetMessages()
            objfrm._Other_ModuleNames = "clsAnnualPayeReturn"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End


                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                'Sohail (03 Aug 2019) -- Start
                'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
                lvItem.SubItems.Add(dtRow.Item("start_Date").ToString)
                lvItem.SubItems(objcolhPeriodStart.Index).Tag = dtRow.Item("end_Date").ToString
                'Sohail (03 Aug 2019) -- End
                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 6 Then
                colhPeriodName.Width = 200 - 18
            Else
                colhPeriodName.Width = 200
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblSlabTranHead.Text = Language._Object.getCaption(Me.lblSlabTranHead.Name, Me.lblSlabTranHead.Text)
			Me.lblSlabEffectivePeriod.Text = Language._Object.getCaption(Me.lblSlabEffectivePeriod.Name, Me.lblSlabEffectivePeriod.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Slab Tax Head is compulsory information.Please select Slab Tax Head to continue.")
			Language.setMessage(mstrModuleName, 2, "Please check atleast one Period to view report.")
			Language.setMessage(mstrModuleName, 3, "Permanent Employement Type is compulosry information.Please Select Permanent Employement Type.")
			Language.setMessage(mstrModuleName, 4, "Permanent and Temparory Employement Type can't be same.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

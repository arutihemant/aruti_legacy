﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewWizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTemplateName = New System.Windows.Forms.Label
        Me.txtType = New System.Windows.Forms.TextBox
        Me.txtDatabase = New System.Windows.Forms.TextBox
        Me.txtServerName = New System.Windows.Forms.TextBox
        Me.lblQuery = New System.Windows.Forms.Label
        Me.txtQuery = New System.Windows.Forms.RichTextBox
        Me.btnOpen = New System.Windows.Forms.Button
        Me.txtXmlPath = New System.Windows.Forms.TextBox
        Me.rbQuery = New System.Windows.Forms.RadioButton
        Me.rbXML = New System.Windows.Forms.RadioButton
        Me.lblNote = New System.Windows.Forms.Label
        Me.lblWelcome = New System.Windows.Forms.Label
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnPrevious = New System.Windows.Forms.Button
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblSeverName = New System.Windows.Forms.Label
        Me.cmbSeverName = New System.Windows.Forms.ComboBox
        Me.btnTestConnection = New System.Windows.Forms.Button
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlWizard = New System.Windows.Forms.Panel
        Me.pnlStep1_TemplateName = New System.Windows.Forms.Panel
        Me.pnlStep4_SelectDatabase = New System.Windows.Forms.Panel
        Me.cboDatabase = New System.Windows.Forms.ComboBox
        Me.lblDatabase = New System.Windows.Forms.Label
        Me.pnlStep3_XmlSelection = New System.Windows.Forms.Panel
        Me.lblXmlPath = New System.Windows.Forms.Label
        Me.pnlStep3_ServerSelection = New System.Windows.Forms.Panel
        Me.pnlStep2_SelectionType = New System.Windows.Forms.Panel
        Me.pnlStep5_Query = New System.Windows.Forms.Panel
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.btnFinish = New System.Windows.Forms.Button
        Me.bgFillDataSources = New System.ComponentModel.BackgroundWorker
        Me.pnlMain.SuspendLayout()
        Me.pnlWizard.SuspendLayout()
        Me.pnlStep1_TemplateName.SuspendLayout()
        Me.pnlStep4_SelectDatabase.SuspendLayout()
        Me.pnlStep3_XmlSelection.SuspendLayout()
        Me.pnlStep3_ServerSelection.SuspendLayout()
        Me.pnlStep2_SelectionType.SuspendLayout()
        Me.pnlStep5_Query.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTemplateName
        '
        Me.lblTemplateName.BackColor = System.Drawing.Color.Transparent
        Me.lblTemplateName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTemplateName.ForeColor = System.Drawing.Color.Black
        Me.lblTemplateName.Location = New System.Drawing.Point(11, 56)
        Me.lblTemplateName.Name = "lblTemplateName"
        Me.lblTemplateName.Size = New System.Drawing.Size(102, 15)
        Me.lblTemplateName.TabIndex = 0
        Me.lblTemplateName.Text = "Template Name"
        '
        'txtType
        '
        Me.txtType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtType.Location = New System.Drawing.Point(115, 53)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(400, 21)
        Me.txtType.TabIndex = 1
        '
        'txtDatabase
        '
        Me.txtDatabase.Location = New System.Drawing.Point(354, 202)
        Me.txtDatabase.Name = "txtDatabase"
        Me.txtDatabase.Size = New System.Drawing.Size(173, 21)
        Me.txtDatabase.TabIndex = 13
        Me.txtDatabase.Visible = False
        '
        'txtServerName
        '
        Me.txtServerName.Location = New System.Drawing.Point(6, 202)
        Me.txtServerName.Name = "txtServerName"
        Me.txtServerName.Size = New System.Drawing.Size(173, 21)
        Me.txtServerName.TabIndex = 12
        Me.txtServerName.Visible = False
        '
        'lblQuery
        '
        Me.lblQuery.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuery.Location = New System.Drawing.Point(3, 10)
        Me.lblQuery.Name = "lblQuery"
        Me.lblQuery.Size = New System.Drawing.Size(98, 15)
        Me.lblQuery.TabIndex = 8
        Me.lblQuery.Text = "Query"
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.Beige
        Me.txtQuery.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuery.Location = New System.Drawing.Point(3, 28)
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(517, 110)
        Me.txtQuery.TabIndex = 7
        Me.txtQuery.Text = ""
        '
        'btnOpen
        '
        Me.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpen.Location = New System.Drawing.Point(491, 47)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(25, 20)
        Me.btnOpen.TabIndex = 6
        Me.btnOpen.Text = "..."
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'txtXmlPath
        '
        Me.txtXmlPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtXmlPath.ForeColor = System.Drawing.Color.Black
        Me.txtXmlPath.Location = New System.Drawing.Point(23, 47)
        Me.txtXmlPath.Name = "txtXmlPath"
        Me.txtXmlPath.Size = New System.Drawing.Size(462, 21)
        Me.txtXmlPath.TabIndex = 5
        '
        'rbQuery
        '
        Me.rbQuery.AutoSize = True
        Me.rbQuery.Checked = True
        Me.rbQuery.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbQuery.Location = New System.Drawing.Point(204, 44)
        Me.rbQuery.Name = "rbQuery"
        Me.rbQuery.Size = New System.Drawing.Size(72, 22)
        Me.rbQuery.TabIndex = 4
        Me.rbQuery.TabStop = True
        Me.rbQuery.Text = "Query"
        Me.rbQuery.UseVisualStyleBackColor = True
        '
        'rbXML
        '
        Me.rbXML.AutoSize = True
        Me.rbXML.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbXML.Location = New System.Drawing.Point(46, 46)
        Me.rbXML.Name = "rbXML"
        Me.rbXML.Size = New System.Drawing.Size(58, 22)
        Me.rbXML.TabIndex = 3
        Me.rbXML.Text = "XML"
        Me.rbXML.UseVisualStyleBackColor = True
        Me.rbXML.Visible = False
        '
        'lblNote
        '
        Me.lblNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNote.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.Location = New System.Drawing.Point(0, 0)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(529, 54)
        Me.lblNote.TabIndex = 2
        Me.lblNote.Text = "Label1"
        Me.lblNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWelcome
        '
        Me.lblWelcome.BackColor = System.Drawing.Color.Silver
        Me.lblWelcome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWelcome.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblWelcome.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWelcome.ForeColor = System.Drawing.Color.Black
        Me.lblWelcome.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblWelcome.Location = New System.Drawing.Point(0, 0)
        Me.lblWelcome.Name = "lblWelcome"
        Me.lblWelcome.Size = New System.Drawing.Size(531, 42)
        Me.lblWelcome.TabIndex = 3
        Me.lblWelcome.Text = "Welcome To Template Wizard"
        Me.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnNext
        '
        Me.btnNext.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNext.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.Location = New System.Drawing.Point(226, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(90, 29)
        Me.btnNext.TabIndex = 3
        Me.btnNext.Text = "&Next >>"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(418, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(107, 29)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPrevious.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrevious.Location = New System.Drawing.Point(130, 3)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(90, 29)
        Me.btnPrevious.TabIndex = 5
        Me.btnPrevious.Text = "<< &Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        Me.btnPrevious.Visible = False
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(171, 83)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(226, 21)
        Me.txtPassword.TabIndex = 11
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(171, 56)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(226, 21)
        Me.txtUserName.TabIndex = 10
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(74, 87)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(91, 13)
        Me.lblPassword.TabIndex = 9
        Me.lblPassword.Text = "Password"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUserName
        '
        Me.lblUserName.BackColor = System.Drawing.Color.Transparent
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(74, 60)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(91, 13)
        Me.lblUserName.TabIndex = 8
        Me.lblUserName.Text = "User Name"
        Me.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSeverName
        '
        Me.lblSeverName.BackColor = System.Drawing.Color.Transparent
        Me.lblSeverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeverName.Location = New System.Drawing.Point(74, 32)
        Me.lblSeverName.Name = "lblSeverName"
        Me.lblSeverName.Size = New System.Drawing.Size(91, 13)
        Me.lblSeverName.TabIndex = 7
        Me.lblSeverName.Text = "Server Name"
        Me.lblSeverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbSeverName
        '
        Me.cmbSeverName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSeverName.FormattingEnabled = True
        Me.cmbSeverName.Location = New System.Drawing.Point(171, 29)
        Me.cmbSeverName.Name = "cmbSeverName"
        Me.cmbSeverName.Size = New System.Drawing.Size(226, 21)
        Me.cmbSeverName.TabIndex = 6
        '
        'btnTestConnection
        '
        Me.btnTestConnection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestConnection.Location = New System.Drawing.Point(3, 3)
        Me.btnTestConnection.Name = "btnTestConnection"
        Me.btnTestConnection.Size = New System.Drawing.Size(121, 29)
        Me.btnTestConnection.TabIndex = 7
        Me.btnTestConnection.Text = "Test Connection"
        Me.btnTestConnection.UseVisualStyleBackColor = True
        Me.btnTestConnection.Visible = False
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.White
        Me.pnlMain.Controls.Add(Me.pnlWizard)
        Me.pnlMain.Controls.Add(Me.TableLayoutPanel1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 42)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(531, 241)
        Me.pnlMain.TabIndex = 12
        '
        'pnlWizard
        '
        Me.pnlWizard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWizard.Controls.Add(Me.pnlStep5_Query)
        Me.pnlWizard.Controls.Add(Me.pnlStep1_TemplateName)
        Me.pnlWizard.Controls.Add(Me.pnlStep4_SelectDatabase)
        Me.pnlWizard.Controls.Add(Me.txtDatabase)
        Me.pnlWizard.Controls.Add(Me.pnlStep3_XmlSelection)
        Me.pnlWizard.Controls.Add(Me.txtServerName)
        Me.pnlWizard.Controls.Add(Me.pnlStep3_ServerSelection)
        Me.pnlWizard.Controls.Add(Me.lblNote)
        Me.pnlWizard.Controls.Add(Me.pnlStep2_SelectionType)
        Me.pnlWizard.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlWizard.Location = New System.Drawing.Point(0, 0)
        Me.pnlWizard.Name = "pnlWizard"
        Me.pnlWizard.Size = New System.Drawing.Size(531, 200)
        Me.pnlWizard.TabIndex = 14
        '
        'pnlStep1_TemplateName
        '
        Me.pnlStep1_TemplateName.Controls.Add(Me.lblTemplateName)
        Me.pnlStep1_TemplateName.Controls.Add(Me.txtType)
        Me.pnlStep1_TemplateName.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep1_TemplateName.Name = "pnlStep1_TemplateName"
        Me.pnlStep1_TemplateName.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep1_TemplateName.TabIndex = 16
        '
        'pnlStep4_SelectDatabase
        '
        Me.pnlStep4_SelectDatabase.Controls.Add(Me.cboDatabase)
        Me.pnlStep4_SelectDatabase.Controls.Add(Me.lblDatabase)
        Me.pnlStep4_SelectDatabase.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep4_SelectDatabase.Name = "pnlStep4_SelectDatabase"
        Me.pnlStep4_SelectDatabase.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep4_SelectDatabase.TabIndex = 18
        Me.pnlStep4_SelectDatabase.Visible = False
        '
        'cboDatabase
        '
        Me.cboDatabase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDatabase.FormattingEnabled = True
        Me.cboDatabase.Location = New System.Drawing.Point(182, 29)
        Me.cboDatabase.Name = "cboDatabase"
        Me.cboDatabase.Size = New System.Drawing.Size(226, 21)
        Me.cboDatabase.TabIndex = 6
        '
        'lblDatabase
        '
        Me.lblDatabase.BackColor = System.Drawing.Color.Transparent
        Me.lblDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatabase.Location = New System.Drawing.Point(43, 32)
        Me.lblDatabase.Name = "lblDatabase"
        Me.lblDatabase.Size = New System.Drawing.Size(133, 13)
        Me.lblDatabase.TabIndex = 7
        Me.lblDatabase.Text = "Select the database"
        Me.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlStep3_XmlSelection
        '
        Me.pnlStep3_XmlSelection.Controls.Add(Me.lblXmlPath)
        Me.pnlStep3_XmlSelection.Controls.Add(Me.txtXmlPath)
        Me.pnlStep3_XmlSelection.Controls.Add(Me.btnOpen)
        Me.pnlStep3_XmlSelection.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep3_XmlSelection.Name = "pnlStep3_XmlSelection"
        Me.pnlStep3_XmlSelection.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep3_XmlSelection.TabIndex = 17
        Me.pnlStep3_XmlSelection.Visible = False
        '
        'lblXmlPath
        '
        Me.lblXmlPath.BackColor = System.Drawing.Color.Transparent
        Me.lblXmlPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblXmlPath.ForeColor = System.Drawing.Color.Black
        Me.lblXmlPath.Location = New System.Drawing.Point(20, 25)
        Me.lblXmlPath.Name = "lblXmlPath"
        Me.lblXmlPath.Size = New System.Drawing.Size(208, 15)
        Me.lblXmlPath.TabIndex = 7
        Me.lblXmlPath.Text = "Select the xml path"
        '
        'pnlStep3_ServerSelection
        '
        Me.pnlStep3_ServerSelection.Controls.Add(Me.cmbSeverName)
        Me.pnlStep3_ServerSelection.Controls.Add(Me.lblSeverName)
        Me.pnlStep3_ServerSelection.Controls.Add(Me.lblUserName)
        Me.pnlStep3_ServerSelection.Controls.Add(Me.txtUserName)
        Me.pnlStep3_ServerSelection.Controls.Add(Me.lblPassword)
        Me.pnlStep3_ServerSelection.Controls.Add(Me.txtPassword)
        Me.pnlStep3_ServerSelection.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep3_ServerSelection.Name = "pnlStep3_ServerSelection"
        Me.pnlStep3_ServerSelection.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep3_ServerSelection.TabIndex = 15
        Me.pnlStep3_ServerSelection.Visible = False
        '
        'pnlStep2_SelectionType
        '
        Me.pnlStep2_SelectionType.Controls.Add(Me.rbXML)
        Me.pnlStep2_SelectionType.Controls.Add(Me.rbQuery)
        Me.pnlStep2_SelectionType.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep2_SelectionType.Name = "pnlStep2_SelectionType"
        Me.pnlStep2_SelectionType.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep2_SelectionType.TabIndex = 15
        Me.pnlStep2_SelectionType.Visible = False
        '
        'pnlStep5_Query
        '
        Me.pnlStep5_Query.Controls.Add(Me.lblQuery)
        Me.pnlStep5_Query.Controls.Add(Me.txtQuery)
        Me.pnlStep5_Query.Location = New System.Drawing.Point(3, 54)
        Me.pnlStep5_Query.Name = "pnlStep5_Query"
        Me.pnlStep5_Query.Size = New System.Drawing.Size(524, 141)
        Me.pnlStep5_Query.TabIndex = 15
        Me.pnlStep5_Query.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel1.ColumnCount = 5
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.btnFinish, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnCancel, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnTestConnection, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnNext, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnPrevious, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 203)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(528, 35)
        Me.TableLayoutPanel1.TabIndex = 15
        '
        'btnFinish
        '
        Me.btnFinish.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnFinish.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinish.Location = New System.Drawing.Point(322, 3)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(90, 29)
        Me.btnFinish.TabIndex = 8
        Me.btnFinish.Text = "&Finish"
        Me.btnFinish.UseVisualStyleBackColor = True
        Me.btnFinish.Visible = False
        '
        'bgFillDataSources
        '
        Me.bgFillDataSources.WorkerReportsProgress = True
        Me.bgFillDataSources.WorkerSupportsCancellation = True
        '
        'frmNewWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(531, 283)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.lblWelcome)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Template Wizard"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlWizard.ResumeLayout(False)
        Me.pnlWizard.PerformLayout()
        Me.pnlStep1_TemplateName.ResumeLayout(False)
        Me.pnlStep1_TemplateName.PerformLayout()
        Me.pnlStep4_SelectDatabase.ResumeLayout(False)
        Me.pnlStep3_XmlSelection.ResumeLayout(False)
        Me.pnlStep3_XmlSelection.PerformLayout()
        Me.pnlStep3_ServerSelection.ResumeLayout(False)
        Me.pnlStep3_ServerSelection.PerformLayout()
        Me.pnlStep2_SelectionType.ResumeLayout(False)
        Me.pnlStep2_SelectionType.PerformLayout()
        Me.pnlStep5_Query.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTemplateName As System.Windows.Forms.Label
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents lblWelcome As System.Windows.Forms.Label
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents rbQuery As System.Windows.Forms.RadioButton
    Friend WithEvents rbXML As System.Windows.Forms.RadioButton
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents txtXmlPath As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblSeverName As System.Windows.Forms.Label
    Friend WithEvents cmbSeverName As System.Windows.Forms.ComboBox
    Friend WithEvents btnTestConnection As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.RichTextBox
    Friend WithEvents lblQuery As System.Windows.Forms.Label
    Friend WithEvents txtServerName As System.Windows.Forms.TextBox
    Friend WithEvents txtDatabase As System.Windows.Forms.TextBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlWizard As System.Windows.Forms.Panel
    Friend WithEvents pnlStep3_ServerSelection As System.Windows.Forms.Panel
    Friend WithEvents pnlStep5_Query As System.Windows.Forms.Panel
    Friend WithEvents pnlStep2_SelectionType As System.Windows.Forms.Panel
    Friend WithEvents pnlStep1_TemplateName As System.Windows.Forms.Panel
    Friend WithEvents pnlStep3_XmlSelection As System.Windows.Forms.Panel
    Friend WithEvents lblXmlPath As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnFinish As System.Windows.Forms.Button
    Friend WithEvents pnlStep4_SelectDatabase As System.Windows.Forms.Panel
    Friend WithEvents cboDatabase As System.Windows.Forms.ComboBox
    Friend WithEvents lblDatabase As System.Windows.Forms.Label
    Friend WithEvents bgFillDataSources As System.ComponentModel.BackgroundWorker
End Class

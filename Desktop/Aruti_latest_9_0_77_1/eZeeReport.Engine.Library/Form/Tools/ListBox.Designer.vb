﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tlp_ListBox = New System.Windows.Forms.TableLayoutPanel
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SubToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tlp_ListBox.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlp_ListBox
        '
        Me.tlp_ListBox.ColumnCount = 3
        Me.tlp_ListBox.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.62162!))
        Me.tlp_ListBox.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.35135!))
        Me.tlp_ListBox.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.67568!))
        Me.tlp_ListBox.Controls.Add(Me.Button1, 1, 1)
        Me.tlp_ListBox.Controls.Add(Me.Button2, 0, 1)
        Me.tlp_ListBox.Controls.Add(Me.Button3, 2, 1)
        Me.tlp_ListBox.Controls.Add(Me.Button4, 1, 2)
        Me.tlp_ListBox.Controls.Add(Me.Button5, 1, 0)
        Me.tlp_ListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlp_ListBox.Location = New System.Drawing.Point(0, 0)
        Me.tlp_ListBox.Name = "tlp_ListBox"
        Me.tlp_ListBox.RowCount = 3
        Me.tlp_ListBox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.25397!))
        Me.tlp_ListBox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.28571!))
        Me.tlp_ListBox.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.46032!))
        Me.tlp_ListBox.Size = New System.Drawing.Size(74, 66)
        Me.tlp_ListBox.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button1.Location = New System.Drawing.Point(19, 15)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(32, 36)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Bottons"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button2.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Button2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button2.Location = New System.Drawing.Point(3, 15)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(10, 36)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "<"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button3.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Button3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button3.Location = New System.Drawing.Point(57, 15)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(14, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = ">"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button4.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Button4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button4.Location = New System.Drawing.Point(19, 57)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(32, 6)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "V"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button5.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Button5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Button5.Location = New System.Drawing.Point(19, 3)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(32, 6)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "V"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddToolStripMenuItem, Me.SubToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(153, 70)
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AddToolStripMenuItem.Text = "Add"
        '
        'SubToolStripMenuItem
        '
        Me.SubToolStripMenuItem.Name = "SubToolStripMenuItem"
        Me.SubToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SubToolStripMenuItem.Text = "Sub"
        '
        'ListBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tlp_ListBox)
        Me.Name = "ListBox"
        Me.Size = New System.Drawing.Size(74, 66)
        Me.tlp_ListBox.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tlp_ListBox As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AddToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SubToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

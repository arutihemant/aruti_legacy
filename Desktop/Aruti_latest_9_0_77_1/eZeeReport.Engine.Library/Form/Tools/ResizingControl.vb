Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Public Delegate Sub MovedResizedEventHandler(ByVal sender As Object, ByVal e As EventArgs)
Public Delegate Sub SelectedEventHandler(ByVal sender As Object, ByVal e As EventArgs)


<Serializable()> _
Public Class ResizingControl
    Inherits UserControl

    ' Events
    Public Event MovedResized As MovedResizedEventHandler
    Public Event Selected As SelectedEventHandler

    ' Methods
    Public Sub New()
        Me.InitializeComponent()
        MyBase.SetStyle((ControlStyles.OptimizedDoubleBuffer Or _
                        (ControlStyles.AllPaintingInWmPaint Or _
                        ControlStyles.UserPaint)), True)

        Me._resizedRect = MyBase.Bounds
        Me._topRect = New Rectangle(0, 0, 0, 0)
        Me._bottomRect = New Rectangle(0, 0, 0, 0)
        Me._leftRect = New Rectangle(0, 0, 0, 0)
        Me._rightRect = New Rectangle(0, 0, 0, 0)
        Me._upperLeftRect = New Rectangle(0, 0, 0, 0)
        Me._upperRightRect = New Rectangle(0, 0, 0, 0)
        Me._lowerLeftRect = New Rectangle(0, 0, 0, 0)
        Me._lowerRightRect = New Rectangle(0, 0, 0, 0)
        Me._sizerRectangles = New ArrayList
        Me._sizerRectangles.Add(New Sizer((Me._topRect), Cursors.SizeNS, DragMode.ResizeTop))
        Me._sizerRectangles.Add(New Sizer((Me._bottomRect), Cursors.SizeNS, DragMode.ResizeBottom))
        Me._sizerRectangles.Add(New Sizer((Me._leftRect), Cursors.SizeWE, DragMode.ResizeLeft))
        Me._sizerRectangles.Add(New Sizer((Me._rightRect), Cursors.SizeWE, DragMode.ResizeRight))
        Me._sizerRectangles.Add(New Sizer((Me._upperLeftRect), Cursors.SizeNWSE, DragMode.ResizeUpperLeft))
        Me._sizerRectangles.Add(New Sizer((Me._upperRightRect), Cursors.SizeNESW, DragMode.ResizeUpperRight))
        Me._sizerRectangles.Add(New Sizer((Me._lowerLeftRect), Cursors.SizeNESW, DragMode.ResizeLowerLeft))
        Me._sizerRectangles.Add(New Sizer((Me._lowerRightRect), Cursors.SizeNWSE, DragMode.ResizeLowerRight))
        Me._textAlign = ContentAlignment.MiddleLeft
        Me._formatSting = New StringFormat
        Me.UpdateSizers()
    End Sub

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing AndAlso (Not Me.components Is Nothing)) Then
            Me.components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private Sub DrawHandles(ByVal g As Graphics)
        Dim sizer As Sizer
        For Each sizer In Me._sizerRectangles
            ControlPaint.DrawGrabHandle(g, sizer._rect, False, True)
        Next
    End Sub

    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'ResizingControl
        '
        Me.DoubleBuffered = True
        Me.Name = "ResizingControl"
        Me.Size = New System.Drawing.Size(40, 32)
        Me.ResumeLayout(False)

    End Sub

    Private Function KeepInBounds(ByVal bounds As Rectangle, ByVal rect As Rectangle, ByVal originalRect As Rectangle) As Rectangle
        If (rect.Width < Me.MinWidth) Then
            If (((Me._dragMode = DragMode.ResizeLeft) OrElse (Me._dragMode = DragMode.ResizeLowerLeft)) OrElse (Me._dragMode = DragMode.ResizeUpperLeft)) Then
                rect.X = ((originalRect.X + originalRect.Width) - Me.MinWidth)
            Else
                rect.X = originalRect.X
            End If
            rect.Width = Me.MinWidth
        End If
        If (rect.Height < Me.MinHeight) Then
            If (((Me._dragMode = DragMode.ResizeTop) OrElse (Me._dragMode = DragMode.ResizeUpperLeft)) OrElse (Me._dragMode = DragMode.ResizeUpperRight)) Then
                rect.Y = ((originalRect.Y + originalRect.Height) - Me.MinHeight)
            Else
                rect.Y = originalRect.Y
            End If
            rect.Height = Me.MinHeight
        End If
        If (rect.X < bounds.X) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Width = Math.Min(originalRect.Width, bounds.Width)
                rect.X = bounds.X
            Else
                rect.X = bounds.X
                rect.Width = ((originalRect.Width + originalRect.X) - bounds.X)
            End If
        End If
        If ((rect.X + rect.Width) > (bounds.X + bounds.Width)) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Width = Math.Min(originalRect.Width, bounds.Width)
                rect.X = ((bounds.X + bounds.Width) - rect.Width)
            Else
                rect.X = originalRect.X
                rect.Width = ((bounds.X + bounds.Width) - rect.X)
            End If
        End If
        If (rect.Y < bounds.Y) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Height = Math.Min(originalRect.Height, bounds.Height)
                rect.Y = bounds.Y
            Else
                rect.Y = bounds.Y
                rect.Height = ((originalRect.Height + originalRect.Y) - bounds.Y)
            End If
        End If
        If ((rect.Y + rect.Height) > (bounds.Y + bounds.Height)) Then
            If (Me._dragMode = DragMode.Move) Then
                rect.Height = Math.Min(originalRect.Height, bounds.Height)
                rect.Y = ((bounds.Y + bounds.Height) - rect.Height)
                Return rect
            End If
            rect.Y = originalRect.Y
            rect.Height = ((bounds.Y + bounds.Height) - rect.Y)
        End If
        Return rect
    End Function

    'Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
    '    Call Unselect()
    '    MyBase.OnLostFocus(e)
    'End Sub


    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        MyBase.OnMouseDown(e)
        Me.BringToFront()
        Dim flag As Boolean = False
        Me._isSelected = True
        Me.OnSelected()
        Me._isDragging = True
        Dim sizer As Sizer
        For Each sizer In Me._sizerRectangles
            If sizer._rect.Contains(e.X, e.Y) Then
                Me._dragMode = sizer._dragMode
                flag = True
                Exit For
            End If
        Next
        If Not flag Then
            Me._dragMode = DragMode.Move
        End If
        Me._lastMouseX = e.X
        Me._lastMouseY = e.Y
        MyBase.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        If Not Me._isSelected Then
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._isDragging = False
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        MyBase.OnMouseMove(e)
        If Me._isSelected Then
            Dim flag As Boolean = False
            Dim sizer As Sizer
            For Each sizer In Me._sizerRectangles
                If sizer._rect.Contains(e.X, e.Y) Then
                    flag = True
                    Me.Cursor = sizer._cursor
                    Exit For
                End If
            Next
            If Not flag Then
                Me.Cursor = Cursors.SizeAll
            End If
        End If
        If Me._isDragging Then
            ControlPaint.DrawReversibleFrame(Me._screenRect, Me.BackColor, FrameStyle.Dashed)
            Dim num As Integer = (e.X - Me._lastMouseX)
            Dim num2 As Integer = (e.Y - Me._lastMouseY)
            MyBase.RectangleToClient(MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle))
            Dim originalRect As Rectangle = MyBase.RectangleToScreen(MyBase.ClientRectangle)
            Select Case Me._dragMode
                Case DragMode.Move
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Exit Select
                Case DragMode.ResizeUpperLeft
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Me._resizedRect.Width = (Me._resizedRect.Width - num)
                    Me._resizedRect.Height = (Me._resizedRect.Height - num2)
                    Exit Select
                Case DragMode.ResizeTop
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Me._resizedRect.Height = (Me._resizedRect.Height - num2)
                    Exit Select
                Case DragMode.ResizeUpperRight
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Me._resizedRect.Width = (Me._resizedRect.Width + num)
                    Me._resizedRect.Height = (Me._resizedRect.Height - num2)
                    Exit Select
                Case DragMode.ResizeRight
                    Me._resizedRect.Width = (Me._resizedRect.Width + num)
                    Exit Select
                Case DragMode.ResizeLowerRight
                    Me._resizedRect.Width = (Me._resizedRect.Width + num)
                    Me._resizedRect.Height = (Me._resizedRect.Height + num2)
                    Exit Select
                Case DragMode.ResizeBottom
                    Me._resizedRect.Height = (Me._resizedRect.Height + num2)
                    Exit Select
                Case DragMode.ResizeLowerLeft
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Width = (Me._resizedRect.Width - num)
                    Me._resizedRect.Height = (Me._resizedRect.Height + num2)
                    Exit Select
                Case DragMode.ResizeLeft
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Width = (Me._resizedRect.Width - num)
                    Exit Select
            End Select
            Me._screenRect = MyBase.RectangleToScreen(Me._resizedRect)
            Dim bounds As Rectangle = MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle)
            If (TypeOf MyBase.Parent Is ScrollableControl AndAlso (Not MyBase.Parent.Parent Is Nothing)) Then
                bounds.Intersect(MyBase.Parent.Parent.RectangleToScreen(MyBase.Parent.Parent.ClientRectangle))
            End If
            Me._screenRect = Me.KeepInBounds(bounds, Me._screenRect, originalRect)
            Me._resizedRect = MyBase.RectangleToClient(Me._screenRect)
            Me._lastMouseX = e.X
            Me._lastMouseY = e.Y
            ControlPaint.DrawReversibleFrame(Me._screenRect, Me.BackColor, FrameStyle.Dashed)
        End If
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        MyBase.OnMouseUp(e)
        Me._isDragging = False
        If (((Me._resizedRect.X <> 0) OrElse (Me._resizedRect.Y <> 0)) OrElse ((Me._resizedRect.Width <> MyBase.Bounds.Width) OrElse (Me._resizedRect.Height <> MyBase.Bounds.Height))) Then
            MyBase.SetBounds((MyBase.Bounds.X + Me._resizedRect.X), (MyBase.Bounds.Y + Me._resizedRect.Y), Me._resizedRect.Width, Me._resizedRect.Height, BoundsSpecified.All)
            MyBase.Invalidate()
            Me.OnMovedResized()
        End If
    End Sub

    Protected Overrides Sub OnMove(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnMove(e)
    End Sub

    Protected Overridable Sub OnMovedResized()
        'If (Not Me._MovedResized Is Nothing) Then
        RaiseEvent MovedResized(Me, EventArgs.Empty)
        'End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
        e.Graphics.DrawString([Text], Me.Font, New SolidBrush(Me.ForeColor), New Rectangle(0, 0, MyBase.Width, MyBase.Height), _formatSting)
        If Me._isSelected Then

            ControlPaint.DrawLockedFrame(e.Graphics, New Rectangle(0, 0, MyBase.Width, MyBase.Height), True)
            'ControlPaint.DrawSelectionFrame(e.Graphics, True, New Rectangle(0, 0, MyBase.Width, MyBase.Height), New Rectangle(2, 2, (MyBase.Width - 4), (MyBase.Height - 4)), Me.BackColor)
            Me.DrawHandles(e.Graphics)
        Else
            ControlPaint.DrawLockedFrame(e.Graphics, New Rectangle(0, 0, MyBase.Width, MyBase.Height), True)
        End If
    End Sub

    Protected Overridable Sub OnSelected()
        RaiseEvent Selected(Me, EventArgs.Empty)
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnSizeChanged(e)
    End Sub

    Protected Overrides Sub [Select](ByVal directed As Boolean, ByVal forward As Boolean)
        Me._isSelected = True
        Me._isDragging = False
        MyBase.Select(directed, forward)
        Me.OnSelected()
    End Sub

    Public Sub Unselect()
        Me._isSelected = False
        Me._isDragging = False
        MyBase.Invalidate()
    End Sub

    Private Sub UpdateSizers()
        If (Not Me._sizerRectangles Is Nothing) Then
            Me._horizontalMid = ((MyBase.Bounds.Width / 2) - (Me.SizerWidth / 2))
            Me._verticalMid = ((MyBase.Bounds.Height / 2) - (Me.SizerWidth / 2))
            Dim sizer As Sizer = DirectCast(Me._sizerRectangles.Item(0), Sizer)
            sizer._rect.X = CInt(Me._horizontalMid)
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(1), Sizer)
            sizer._rect.X = CInt(Me._horizontalMid)
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(2), Sizer)
            sizer._rect.X = 0
            sizer._rect.Y = CInt(Me._verticalMid)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(3), Sizer)
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = CInt(Me._verticalMid)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(4), Sizer)
            sizer._rect.X = 0
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(5), Sizer)
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = 0
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(6), Sizer)
            sizer._rect.X = 0
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
            sizer = DirectCast(Me._sizerRectangles.Item(7), Sizer)
            sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
            sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
            sizer._rect.Width = Me.SizerWidth
            sizer._rect.Height = Me.SizerWidth
        End If
    End Sub

    Protected Sub UpdateSizingData()
        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
        Me._screenRect = New Rectangle(0, 0, 0, 0)
        Me.UpdateSizers()
    End Sub

    ' Properties
    <Description("Minimum height of resizable control"), Category("Appearance")> _
    Public Property MinHeight() As Integer
        Get
            Return Me._minHeight
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinHeight", (value > 0))
            Me._minHeight = value
        End Set
    End Property

    <Description("Minimum width of resizable control"), Category("Appearance")> _
    Public Property MinWidth() As Integer
        Get
            Return Me._minWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinWidth", (value > 0))
            Me._minWidth = value
        End Set
    End Property

    <Description("Width of resize handles"), Category("Appearance")> _
    Public Property SizerWidth() As Integer
        Get
            Return Me._sizerWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "SizerWidth", (value > 0))
            Me._sizerWidth = value
            Me.UpdateSizers()
        End Set
    End Property

    <Browsable(True), DefaultValue("Text"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
            Me.Invalidate()
        End Set
    End Property


    <DefaultValue(&H10), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Property TextAlign() As ContentAlignment
        Get
            Return Me._textAlign
        End Get
        Set(ByVal value As ContentAlignment)
            Me._textAlign = value
            Select Case Me._textAlign
                Case ContentAlignment.TopLeft
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.TopCenter
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.TopRight
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.MiddleLeft
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.MiddleCenter
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.MiddleRight
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.BottomLeft
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.BottomCenter
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.BottomRight
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Far
            End Select
            Me.Invalidate()
        End Set
    End Property

    ' Fields
    Private _bottomRect As Rectangle
    Private components As Container
    Private _dragMode As DragMode
    Private _horizontalMid As Single
    Private _isDragging As Boolean
    Private _isSelected As Boolean
    Private _lastMouseX As Integer
    Private _lastMouseY As Integer
    Private _leftRect As Rectangle
    Private _lowerLeftRect As Rectangle
    Private _lowerRightRect As Rectangle
    Private _minHeight As Integer = 10
    Private _minWidth As Integer = 10
    Private _resizedRect As Rectangle
    Private _rightRect As Rectangle
    Private _screenRect As Rectangle
    Private _sizerRectangles As ArrayList
    Private _sizerWidth As Integer = 6
    Private _topRect As Rectangle
    Private _upperLeftRect As Rectangle
    Private _upperRightRect As Rectangle
    Private _verticalMid As Single
    Private _textAlign As ContentAlignment
    Private _formatSting As StringFormat

    ' Nested Types
    Public Enum DragMode
        ' Fields
        Move = 0
        ResizeBottom = 6
        ResizeLeft = 8
        ResizeLowerLeft = 7
        ResizeLowerRight = 5
        ResizeRight = 4
        ResizeTop = 2
        ResizeUpperLeft = 1
        ResizeUpperRight = 3
    End Enum

    Private Class Sizer
        ' Methods
        Public Sub New(ByRef rect As Rectangle, ByVal cursor As Cursor, ByVal dragMode As DragMode)
            Me._rect = rect
            Me._cursor = cursor
            Me._dragMode = dragMode
        End Sub


        ' Fields
        Public _cursor As Cursor
        Public _dragMode As DragMode
        Public _rect As Rectangle
    End Class


End Class

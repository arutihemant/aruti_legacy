Imports System.Windows.Forms
Imports System.Drawing
Imports System.ComponentModel

Public Class TemplateLineField
    Inherits LineControl

    Implements ITemplatePropertyClass

    Private m_publicproperties As New TemplatePropertyList()
    Public Property PublicProperties() As TemplatePropertyList Implements ITemplatePropertyClass.PublicProperties
        Get
            Return m_publicproperties
        End Get
        Set(ByVal value As TemplatePropertyList)
            m_publicproperties = value
        End Set
    End Property

    Private mstrFieldName As String = ""
    Public Property _FieldName() As String
        Get
            Return mstrFieldName
        End Get
        Set(ByVal value As String)
            mstrFieldName = value
        End Set
    End Property

    Private intFieldUnkID As Integer = -1
    Public Property _FieldUnkID() As Integer
        Get
            Return intFieldUnkID
        End Get
        Set(ByVal value As Integer)
            intFieldUnkID = value
        End Set
    End Property

    'Private intFieldType As Integer = 0
    'Public Property _FieldType() As Integer
    '    Get
    '        Return intFieldType
    '    End Get
    '    Set(ByVal value As Integer)
    '        intFieldType = value
    '    End Set
    'End Property

    Private strValue As String = ""
    Public Property _Value() As String
        Get
            Return strValue
        End Get
        Set(ByVal value As String)
            strValue = value
            Me.Text = value
        End Set
    End Property

    Private intFieldId As Integer = 0
    Public Property _FieldId() As Integer
        Get
            Return intFieldId
        End Get
        Set(ByVal value As Integer)
            intFieldId = value
        End Set
    End Property

    Private intSectionId As Integer = 0
    Public Property _SectionId() As Integer
        Get
            Return intSectionId
        End Get
        Set(ByVal value As Integer)
            intSectionId = value
        End Set
    End Property


    Public Shadows Property Vertical() As Boolean
        Get
            Return MyBase._isvertical
        End Get
        Set(ByVal value As Boolean)
            MyBase._isvertical = value
            If MyBase._isvertical = True Then
                If MyBase.Width > MyBase.Height Then
                    Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
                End If
            Else
                If MyBase.Height > MyBase.Width Then
                    Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
                End If
            End If
        End Set
    End Property

    Public Shadows Property LineStyle() As System.Drawing.Drawing2D.DashStyle
        Get
            Return MyBase._lineStyle
        End Get
        Set(ByVal value As System.Drawing.Drawing2D.DashStyle)
            MyBase._lineStyle = value
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property LineThickness() As Integer
        Get
            Return MyBase._lineThickness
        End Get
        Set(ByVal value As Integer)
            If value > 6 Then
                value = 6
                MyBase._lineThickness = value
            Else
                MyBase._lineThickness = value
            End If
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property LineColor() As Color
        Get
            Return MyBase._lineColor
        End Get
        Set(ByVal value As Color)
            MyBase._lineColor = value
            Me.Refresh()
        End Set
    End Property

    Public Shadows Property FieldType() As Integer
        Get
            Return MyBase.intFieldType
        End Get
        Set(ByVal value As Integer)
            MyBase.intFieldType = value
            Me.Invalidate()
        End Set
    End Property


        'Private blnIsVertical As Boolean = False
        'Public Property _IsVerticalLine() As Boolean
        '    Get
        '        Return blnIsVertical
        '    End Get
        '    Set(ByVal value As Boolean)
        '        blnIsVertical = value
        '        Me._isvertical = value
        '        If Me._isvertical = True Then
        '            If MyBase.Width > MyBase.Height Then
        '                Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
        '            End If
        '        Else
        '            If MyBase.Height > MyBase.Width Then
        '                Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
        '            End If
        '        End If
        '    End Set
        'End Property

        'Private lineStyles As System.Drawing.Drawing2D.DashStyle = Drawing2D.DashStyle.Solid
        'Public Property _LineStyles() As System.Drawing.Drawing2D.DashStyle
        '    Get
        '        Return lineStyles
        '    End Get
        '    Set(ByVal value As System.Drawing.Drawing2D.DashStyle)
        '        lineStyles = value
        '        Me._lineStyle = value
        '        Me.Refresh()
        '    End Set
        'End Property

        'Private lineColors As Color = Color.Black
        'Public Property _LineColors() As Color
        '    Get
        '        Return lineColors
        '    End Get
        '    Set(ByVal value As Color)
        '        lineColors = value
        '        Me._lineColor = value
        '        Me.Refresh()
        '    End Set
        'End Property



#Region "Base Property "
    <Browsable(True)> _
    Public Shadows Property Height() As Integer
        Get
            Return MyBase.Height
        End Get
        Set(ByVal value As Integer)
            'If MyBase._isvertical = True Then
            MyBase.Height = value
            'Else
            'MyBase.Height = 6
            'End If

        End Set
    End Property

    <Browsable(True)> _
   Public Shadows Property Width() As Integer
        Get
            Return MyBase.Width
        End Get
        Set(ByVal value As Integer)
            ''If MyBase._isvertical = True Then
            'MyBase.Width = 6
            'Else
            MyBase.Width = value
            'End If
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Top() As Integer
        Get
            Return MyBase.Top
        End Get
        Set(ByVal value As Integer)
            MyBase.Top = value
        End Set
    End Property

    <Browsable(True)> _
    Public Shadows Property Left() As Integer
        Get
            Return MyBase.Left
        End Get
        Set(ByVal value As Integer)
            MyBase.Left = value
        End Set
    End Property

#End Region
End Class

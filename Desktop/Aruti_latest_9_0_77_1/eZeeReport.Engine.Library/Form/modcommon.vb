Imports System.Drawing
Imports Aruti.Data
Module modcommon


    Public gstrMasterXML_Path As String = AppSettings._Object._ApplicationPath & "TemplateMst"
    Public gstrDetailXML_Path As String = AppSettings._Object._ApplicationPath & "TemplateDet"
    Public gstrFormulaXML_Path As String = AppSettings._Object._ApplicationPath & "TemplateFormula"
    Public gstrDataXML_Path As String = AppSettings._Object._ApplicationPath & "DataXML"

    Public gstrMasterXML As String = "TemplateMst"
    Public gstrDetailXML As String = "TemplateDet"
    Public gstrFormulaXML As String = "TemplateFormula"
    Public gstrDataXML As String = "DataXML"

    Private _fontWidthDic As Dictionary(Of String, Integer())
    Private ReadOnly Property FontWidthDic() As Dictionary(Of String, Integer())
        Get
            If _fontWidthDic Is Nothing Then
                _fontWidthDic = New Dictionary(Of String, Integer())()
            End If
            Return _fontWidthDic
        End Get
    End Property

    Public Function GetFotsNameTTF(ByVal FontsName As String) As String
        Dim sAns As String
        Dim sErr As String = ""
        Dim mykey As String
        If (FontsMod.getOSVer() > 4) Then
            mykey = "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts"
        Else
            mykey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Fonts"
        End If
        sAns = FontsMod.RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontsName, sErr)
        If sAns <> "" Then
            MsgBox("File Name = " & sAns)
        Else
            sAns = FontsMod.RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontsName & " (TrueType)", sErr)
            If sAns <> "" Then
            Else
                sAns = FontsMod.RegValue(Microsoft.Win32.RegistryHive.LocalMachine, mykey, FontsName & " (All res)", sErr)
                If sAns <> "" Then
                Else
                    MsgBox("This error occurred: " & sErr)
                End If
            End If
        End If
        Return Environment.SystemDirectory & "\..\Fonts\" & sAns
    End Function

    Public Function TruncateText(ByVal text As String, ByVal textMaxWidth As Integer, ByVal fontName As String, ByVal fontSizeInPixels As Integer, ByVal isFontBold As Boolean) As String
        If String.IsNullOrEmpty(text) Then
            Return text
        End If

        ' Check
        '
        If textMaxWidth < 1 OrElse String.IsNullOrEmpty(fontName) OrElse fontSizeInPixels < 1 Then
            Throw New ArgumentException()
        End If

        Dim fontWidthArray() As Integer = GetFontWidthArray(fontName, fontSizeInPixels, isFontBold)
        Dim ellipsisWidth As Integer = CType(fontWidthArray(1) * 3, Integer)
        Dim totalCharCount As Integer = text.Length
        Dim textWidth As Integer = 0
        Dim charIndex As Integer = 0
        For i As Integer = 0 To totalCharCount - 1
            textWidth += fontWidthArray(text.Chars(i).ToString)
            If textWidth > textMaxWidth Then
                Return text.Substring(0, charIndex) & "..."
            ElseIf textWidth + ellipsisWidth <= textMaxWidth Then
                charIndex = i
            End If
        Next i
        Return text
    End Function
    Private Function GetFontWidthArray(ByVal fontName As String, ByVal fontSizeInPixels As Integer, ByVal isFontBold As Boolean) As Integer()
        Dim fontEntryName As String = fontName.ToLower() & "_" & fontSizeInPixels.ToString() & "px" & (IIf(isFontBold, "_bold", ""))
        Dim fontWidthArray() As Integer
        If (Not FontWidthDic.TryGetValue(fontName, fontWidthArray)) Then
            fontWidthArray = CreateFontWidthArray(New Font(fontName, fontSizeInPixels, IIf(isFontBold, FontStyle.Bold, FontStyle.Regular), GraphicsUnit.Pixel))
            FontWidthDic(fontName) = fontWidthArray
        End If

        Return fontWidthArray
    End Function

    Private Function CreateFontWidthArray(ByVal font As Font) As Integer()
        Dim fontWidthArray(255) As Integer
        For i As Integer = 32 To 255
            Dim c As Char = ChrW(i)
            fontWidthArray(i) = IIf(IsIllegalCharacter(c, False), 0, GetCharWidth(c, font))
        Next i
        Return fontWidthArray
    End Function

    Private Function GetCharWidth(ByVal c As Char, ByVal font As Font) As Integer
        ' Note1: For typography related reasons,
        ' TextRenderer.MeasureText() doesn't return the correct
        ' width of the character in pixels, hence the need
        ' to use this hack (with the '<' & '>'
        ' characters and the subtraction). Note that <'
        ' and '>' were chosen randomly, other characters 
        ' can be used.
        '

        ' Note2: As the TextRenderer class is intended
        ' to be used with Windows Forms Applications, it has a 
        ' special use for the ampersand character (used for Mnemonics).
        ' Therefore, we need to check for the 
        ' ampersand character and replace it with '&&'
        ' to escape it (TextRenderer.MeasureText() will treat 
        ' it as one ampersand character)
        '

        Return TextRenderer.MeasureText("<" & (IIf(c = "&"c, "&&", c.ToString())) & ">", font).Width - TextRenderer.MeasureText("<>", font).Width
    End Function

    Private Function ContainsIllegalCharacters(ByVal text As String, ByVal excludeLineBreaks As Boolean) As Boolean
        If (Not String.IsNullOrEmpty(text)) Then
            For Each c As Char In text
                If IsIllegalCharacter(c, excludeLineBreaks) Then
                    Return True
                End If
            Next c
        End If

        Return False
    End Function

    Private Function IsIllegalCharacter(ByVal c As Char, ByVal excludeLineBreaks As Boolean) As Boolean
        ' See the Windows-1252 encoding
        ' (we use ISO-8859-1, but all browsers, or at least
        ' IE, FF, Opera, Chrome and Safari,
        ' interpret ISO-8859-1 as Windows-1252).
        ' For more information,
        ' see http://en.wikipedia.org/wiki/ISO/
        '        IEC_8859-1#ISO-8859-1_and_Windows-1252_confusion
        '

        Return (AscW(c) < 32 AndAlso ((Not excludeLineBreaks) OrElse c <> ControlChars.Lf)) OrElse AscW(c) > 255 OrElse AscW(c) = 127 OrElse AscW(c) = 129 OrElse AscW(c) = 141 OrElse AscW(c) = 143 OrElse AscW(c) = 144 OrElse AscW(c) = 157
    End Function
End Module

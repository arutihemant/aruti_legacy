'************************************************************************************************************************************
'Class Name : clsLanguage.vb
'Purpose    : All owner level opration like getlist, getComboList, insert, update, delete, checkduplicate.
'Date       : 2/18/2008
'Written By : Naimish
'Modified   : 05/06/2009
'************************************************************************************************************************************
Imports eZeeCommonLib

''' <summary>
''' Developer: Naimish
''' </summary>
Public Class clsLanguage
    Private ReadOnly mstrModuleName As String = "clsLanguage"

#Region " Private variables "
    Private mintLangUnkId As Integer = -1
    Private mintApplication As Integer = -1
    Private mstrFormName As String = ""
    Private mstrControlName As String = ""
    Private mstrLanguage As String = ""
    Private mstrLanguage1 As String = ""
    Private mstrLanguage2 As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _LangUnkId() As Integer
        Get
            Return mintLangUnkId
        End Get
        Set(ByVal value As Integer)
            mintLangUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Application() As Integer
        Get
            Return mintApplication
        End Get
        Set(ByVal value As Integer)
            mintApplication = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _ControlName() As String
        Get
            Return mstrControlName
        End Get
        Set(ByVal value As String)
            mstrControlName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Language() As String
        Get
            Return mstrLanguage
        End Get
        Set(ByVal value As String)
            mstrLanguage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Language1() As String
        Get
            Return mstrLanguage1
        End Get
        Set(ByVal value As String)
            mstrLanguage1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Language2() As String
        Get
            Return mstrLanguage2
        End Get
        Set(ByVal value As String)
            mstrLanguage2 = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' For insert given property value in database table (cflanguage).
    ''' Modify By: Naimish
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function Insert() As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.AddParameter("@application", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, mintApplication)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName.ToString)
            objDataOperation.AddParameter("@controlname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlName.ToString)
            objDataOperation.AddParameter("@language", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLanguage.ToString)
            objDataOperation.AddParameter("@language1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLanguage1.ToString)
            objDataOperation.AddParameter("@language2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLanguage2.ToString)

            strQ = "INSERT INTO cflanguage ( " & _
                        "application, " & _
                        "formname, " & _
                        "controlname, " & _
                        "language, " & _
                        "language1, " & _
                        "language2 " & _
                    ") VALUES (" & _
                        "@application, " & _
                        "@formname, " & _
                        "@controlname, " & _
                        "@language, " & _
                        "@language1, " & _
                        "@language2) "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Naimish
    ''' </summary>
    Public Sub Truncate()

        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "TRUNCATE TABLE cflanguage "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objDataOperation = Nothing
        End Try

    End Sub

#Region "List"
    ''' <summary>
    ''' Get list of all fields from database table (cflanguage).
    ''' Modify By: Naimish
    ''' </summary>
    ''' <param name="strListName">Optional Para, DataTable name.</param>
    ''' <returns>DataSet</returns>
    Public Function getList(Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "SELECT * FROM cflanguage "

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try

        Return dsList

    End Function
#End Region

    'Naimish (03 Feb 2010) -- Start
    Public Sub ImportLanguage(ByVal strFile As String)
        Dim dsList As New DataSet
        Dim dsImportTable As New DataSet

        Dim objLanguage As New clsLanguage
        Dim objMessages As New clsMessages
        Dim strQ As String = ""

        Dim objDataOperation As New clsDataOperation
        Try
            'Import File
            dsImportTable.ReadXml(strFile)

            dsList.Tables.Add(objLanguage.getList("Language").Tables(0).Copy)
            dsList.Tables.Add(objMessages.getList("Message").Tables(0).Copy)

            If Not dsImportTable.Tables.Contains("Language") Then
                eZeeMsgBox.Show("Invalid Language data file.")
                Exit Sub
            End If

            If Not dsImportTable.Tables.Contains("Message") Then
                eZeeMsgBox.Show("Invalid Language data file.")
                Exit Sub
            End If

            For Each col As DataColumn In dsList.Tables("Language").Columns
                If Not dsImportTable.Tables("Language").Columns.Contains(col.ColumnName) Then
                    eZeeMsgBox.Show("Language data file corrupted.")
                    Exit Sub
                End If
            Next

            For Each col As DataColumn In dsList.Tables("Message").Columns
                If Not dsImportTable.Tables("Message").Columns.Contains(col.ColumnName) Then
                    eZeeMsgBox.Show("Language data file corrupted.")
                    Exit Sub
                End If
            Next

            objDataOperation.BindTransaction()

            objLanguage.Truncate()

            For Each dtRow As DataRow In dsImportTable.Tables("Language").Rows
                Try
                    objLanguage._Application = dtRow.Item("Application")
                    objLanguage._ControlName = dtRow.Item("ControlName") & ""
                    objLanguage._FormName = dtRow.Item("FormName") & ""
                    objLanguage._Language = dtRow.Item("Language") & ""
                    objLanguage._Language1 = dtRow.Item("Language1") & ""
                    objLanguage._Language2 = dtRow.Item("Language2") & ""
                    objLanguage.Insert()
                Catch ex As Exception

                End Try
            Next

            objMessages.Truncate()

            For Each dtRow As DataRow In dsImportTable.Tables("Message").Rows
                Try
                    objMessages._Application = dtRow.Item("Application")
                    objMessages._Messagecode = dtRow.Item("Messagecode") & ""
                    objMessages._Module_Name = dtRow.Item("Module_Name") & ""
                    objMessages._Message = dtRow.Item("Message") & ""
                    objMessages._Message1 = dtRow.Item("Message1") & ""
                    objMessages._Message2 = dtRow.Item("Message2") & ""
                    objMessages.Insert()
                Catch ex As Exception

                End Try
            Next

            strQ = "UPDATE cfdeskuserprivilegegroup_master " & _
                       "SET Name = @Name " & _
                   "WHERE privilegegroupunkid = @Code "

            If dsImportTable.Tables.Contains("PrivilegeGroup") Then
                For Each dtRow As DataRow In dsImportTable.Tables("PrivilegeGroup").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE cfdeskprivilege_master " & _
                    "SET privilege_name = @Name " & _
                "WHERE privilegeunkid = @Code "

            If dsImportTable.Tables.Contains("Privilege") Then
                For Each dtRow As DataRow In dsImportTable.Tables("Privilege").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If


            strQ = "UPDATE cfdeskuserabilitylevel_master " & _
                    "SET Name = @Name " & _
                "WHERE deskuserabilitylevelunkid = @Code "

            If dsImportTable.Tables.Contains("AbilityLevel") Then
                For Each dtRow As DataRow In dsImportTable.Tables("AbilityLevel").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE cfplan_master " & _
                    "SET Name = @Name " & _
                "WHERE planunkid = @Code "

            If dsImportTable.Tables.Contains("Plan") Then
                For Each dtRow As DataRow In dsImportTable.Tables("Plan").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)

                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE cfvehicle_master " & _
                   "SET vehicle_make = @Name " & _
                   ", vehicle_model = @description " & _
               "WHERE vehicleunkid = @Code "

            If dsImportTable.Tables.Contains("Vehicle") Then
                For Each dtRow As DataRow In dsImportTable.Tables("Vehicle").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))
                        objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("description"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE cfreport_category " & _
                   "SET Name = @Name " & _
               "WHERE reportcategoryunkid = @Code "

            If dsImportTable.Tables.Contains("ReportCategory") Then
                For Each dtRow As DataRow In dsImportTable.Tables("ReportCategory").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            strQ = "UPDATE cfreport_master " & _
                        "SET Name = @Name " & _
                    "WHERE reportunkid = @Code "

            If dsImportTable.Tables.Contains("Report") Then
                For Each dtRow As DataRow In dsImportTable.Tables("Report").Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name"))

                        objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            'Common
            'Naimish (03 Feb 2010) -- Start
            strQ = "UPDATE hrmsConfiguration..cfcountry_master " & _
                        "SET country_name1 = @Name1 " & _
                        ", country_name2 = @Name2 " & _
                    "WHERE countryunkid = @Code "

            If dsImportTable.Tables.Contains("Country") Then
                If Not dsImportTable.Tables("Country").Columns.Contains("name1") Then
                    dsImportTable.Tables("Country").Columns.Add(New DataColumn("name1", System.Type.GetType("System.String")))
                    dsImportTable.Tables("Country").Columns.Add(New DataColumn("name2", System.Type.GetType("System.String")))
                End If
                For Each dtRow As DataRow In dsImportTable.Tables("Country").Rows
                    objDataOperation.ClearParameters()
                    If IsDBNull(dtRow.Item("Name1")) Then dtRow.Item("Name1") = dtRow.Item("Name")
                    If IsDBNull(dtRow.Item("Name2")) Then dtRow.Item("Name2") = dtRow.Item("Name")
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name1"))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Name2"))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            'Naimish (03 Feb 2010) -- End

            'POS 
            Dim strName1 As String = "Name1"
            Dim strName2 As String = "Name2"
            Dim strDescription1 As String = "description1"
            Dim strDescription2 As String = "description2"

            ''for Form PrivilegeGroup 
            strQ = "UPDATE posdeskprivilege_header " & _
                        "SET headername1 = @Name1 " & _
                        ", headername2 = @Name2 " & _
                    "WHERE privilegeheaderunkid = @Code "

            If dsImportTable.Tables.Contains("POS_PrivilegeGroup") Then
                If Not dsImportTable.Tables.Item("POS_PrivilegeGroup").Columns.Contains("Name1") Then
                    strName1 = "name"
                    strName2 = "name"
                Else
                    strName1 = "Name1"
                    strName2 = "Name2"
                End If
                For Each dtRow As DataRow In dsImportTable.Tables("POS_PrivilegeGroup").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName1))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName2))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            ''For From Privilege 
            strQ = "UPDATE posdeskprivilege_list " & _
                        "SET privilege_name1 = @Name1 " & _
                        ", privilege_name2 = @Name2 " & _
                    "WHERE privilegeunkid = @Code "

            If dsImportTable.Tables.Contains("POS_Privilege") Then
                If Not dsImportTable.Tables.Item("POS_Privilege").Columns.Contains("Name1") Then
                    strName1 = "name"
                    strName2 = "name"
                Else
                    strName1 = "Name1"
                    strName2 = "Name2"
                End If
                For Each dtRow As DataRow In dsImportTable.Tables("POS_Privilege").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName1))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName2))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            ''For From Report Category
            strQ = "UPDATE posreport_category " & _
                        "SET name1 = @Name1 " & _
                        ", name2 = @Name2 " & _
                    "WHERE reportcategoryunkid = @Code "

            If dsImportTable.Tables.Contains("POS_ReportCategory") Then
                If Not dsImportTable.Tables.Item("POS_ReportCategory").Columns.Contains("Name1") Then
                    strName1 = "name"
                    strName2 = "name"
                Else
                    strName1 = "Name1"
                    strName2 = "Name2"
                End If
                For Each dtRow As DataRow In dsImportTable.Tables("POS_ReportCategory").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName1))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName2))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If


            ''For From Report Master 
            strQ = "UPDATE posreport_master " & _
                        "SET name1 = @Name1 " & _
                        ", name2 = @Name2 " & _
                        ", description1 = @description1 " & _
                        ", description2 = @description2 " & _
                    "WHERE reportunkid = @Code "

            If dsImportTable.Tables.Contains("POS_Report") Then

                If Not dsImportTable.Tables.Item("POS_Report").Columns.Contains("Name1") Then
                    strName1 = "name"
                    strName2 = "name"
                    strDescription1 = "Description"
                    strDescription2 = "Description"
                Else
                    strName1 = "Name1"
                    strName2 = "Name2"
                    strDescription1 = "description1"
                    strDescription2 = "description2"
                End If
                For Each dtRow As DataRow In dsImportTable.Tables("POS_Report").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName1))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName2))
                    objDataOperation.AddParameter("@description1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strDescription1))
                    objDataOperation.AddParameter("@description2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strDescription2))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            ''For From Windows Template Filed
            strQ = "UPDATE postemplate_field " & _
                        "SET name1 = @Name1 " & _
                        ", name2 = @Name2 " & _
                    "WHERE fieldunkid = @Code "

            If dsImportTable.Tables.Contains("POS_Template_Field") Then
                If Not dsImportTable.Tables.Item("POS_Template_Field").Columns.Contains("Name1") Then
                    strName1 = "name"
                    strName2 = "name"
                Else
                    strName1 = "Name1"
                    strName2 = "Name2"
                End If

                For Each dtRow As DataRow In dsImportTable.Tables("POS_Template_Field").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@Code", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item("Code"))
                    objDataOperation.AddParameter("@Name1", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName1))
                    objDataOperation.AddParameter("@Name2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dtRow.Item(strName2))

                    objDataOperation.ExecNonQuery(strQ)
                Next
            End If
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
            End If

            objDataOperation.ReleaseTransaction(True)
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw ex
        Finally
            objDataOperation.ReleaseTransaction(False)
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing

            If dsImportTable IsNot Nothing Then dsImportTable.Dispose()
            dsImportTable = Nothing

            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            objMessages = Nothing
            objLanguage = Nothing
        End Try
    End Sub
    'Naimish (03 Feb 2010) -- End
End Class
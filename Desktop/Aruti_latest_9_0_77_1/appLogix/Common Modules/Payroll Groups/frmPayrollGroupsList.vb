﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmPayrollGroupsList

#Region "Private Variable"

    Private objPayrollGroupMaster As clspayrollgroup_master
    Private ReadOnly mstrModuleName As String = "frmPayrollGroupsList"
    Dim mintGroupID As Integer = 0 'Sohail (19 Nov 2010)
#End Region

#Region "Form's Event"

    Private Sub frmPayrollGroupsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objPayrollGroupMaster = New clspayrollgroup_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()

            cboGroupList.Items.Clear()
            cboGroupList.Items.Add(PayrollGroupType.CostCenter.ToString())
            'cboGroupList.Items.Add("")
            cboGroupList.Items.Add(PayrollGroupType.Bank.ToString())
            cboGroupList.SelectedIndex = 0
            Language.setLanguage(Me.Name)
            fillList()
            If lvGroupList.Items.Count > 0 Then lvGroupList.Items(0).Selected = True
            lvGroupList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollGroupsList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvGroupList.Focused = True Then
                'Sohail (24 Jun 2011) -- Start
                'Issue : TAB key fired instead of DELETE key
                'SendKeys.Send("{TAB}")
                'e.Handled = True
                Call btnDelete.PerformClick()
                'Sohail (24 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayrollGroupsList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayrollGroupsList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objPayrollGroupMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clspayrollgroup_master.SetMessages()
            objfrm._Other_ModuleNames = "clspayrollgroup_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjPayrollGroups_AddEdit As New frmPayrollGroups_AddEdit
            If mintGroupID = CInt(PayrollGroupType.CostCenter) Then 'Sohail (19 Nov 2010)
                'If cboGroupList.SelectedIndex + 2 <> CInt(PayrollGroupType.Bank) Then
                ObjPayrollGroups_AddEdit.lblwebsite.Visible = False
                ObjPayrollGroups_AddEdit.txtwebsite.Visible = False

                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                ObjPayrollGroups_AddEdit.lblReferenceNo.Visible = False
                ObjPayrollGroups_AddEdit.txtReferenceNo.Visible = False
                'Sohail (31 Mar 2014) -- End


                'Pinkal (12-Oct-2011) -- Start

                ObjPayrollGroups_AddEdit.lblDescription.Location = ObjPayrollGroups_AddEdit.lblSwiftcode.Location
                ObjPayrollGroups_AddEdit.txtDescription.Location = ObjPayrollGroups_AddEdit.txtSwiftcode.Location

                ObjPayrollGroups_AddEdit.lblSwiftcode.Visible = False
                ObjPayrollGroups_AddEdit.txtSwiftcode.Visible = False

                'ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 180)
                'ObjPayrollGroups_AddEdit.Size = New Size(403, 279)

                ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(402, 155)
                ObjPayrollGroups_AddEdit.Size = New Size(431, 255)

                'Pinkal (12-Oct-2011) -- End

            Else
                ObjPayrollGroups_AddEdit.lblwebsite.Visible = True
                ObjPayrollGroups_AddEdit.txtwebsite.Visible = True
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                ObjPayrollGroups_AddEdit.lblReferenceNo.Visible = True
                ObjPayrollGroups_AddEdit.txtReferenceNo.Visible = True
                'Sohail (31 Mar 2014) -- End


                'Pinkal (12-Oct-2011) -- Start

                'ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 208)
                ' ObjPayrollGroups_AddEdit.Size = New Size(403, 309)

                ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(402, 208)
                ObjPayrollGroups_AddEdit.Size = New Size(431, 309)

                'Pinkal (12-Oct-2011) -- End

            End If
            If ObjPayrollGroups_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, mintGroupID) Then 'Sohail (19 Nov 2010)
                'if ObjPayrollGroups_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, CInt(IIf(cboGroupList.SelectedIndex = 0, cboGroupList.SelectedIndex + 1, cboGroupList.SelectedIndex + 2))) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvGroupList.DoubleClick 'Sohail (19 Nov 2010)
        'Sohail (24 Jun 2011) -- Start
        If User._Object.Privilege._EditPayrollGroup = False Then Exit Sub
        'Sohail (24 Jun 2011) -- End
        'Sohail (19 Nov 2010) -- Start
        'Dim strName As String = GetPayrollGroupTypeName()
        Dim strName As String = GetPayrollGroupTypeName(mintGroupID)
        'Sohail (19 Nov 2010) -- End

        If lvGroupList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select ") & strName & Language.getMessage(mstrModuleName, 2, " Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvGroupList.Select()
            Exit Sub
        End If
        Dim objfrmPayrollGroups_AddEdit As New frmPayrollGroups_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvGroupList.SelectedItems(0).Index

            If mintGroupID = CInt(PayrollGroupType.CostCenter) Then 'Sohail (19 Nov 2010)
                'If cboGroupList.SelectedIndex + 2 <> CInt(PayrollGroupType.Bank) Then
                objfrmPayrollGroups_AddEdit.lblSwiftcode.Visible = False
                objfrmPayrollGroups_AddEdit.txtSwiftcode.Visible = False
                objfrmPayrollGroups_AddEdit.lblwebsite.Visible = False
                objfrmPayrollGroups_AddEdit.txtwebsite.Visible = False
                objfrmPayrollGroups_AddEdit.lblDescription.Location = objfrmPayrollGroups_AddEdit.lblSwiftcode.Location
                objfrmPayrollGroups_AddEdit.txtDescription.Location = objfrmPayrollGroups_AddEdit.txtSwiftcode.Location
                objfrmPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(402, 155)
                objfrmPayrollGroups_AddEdit.Size = New Size(431, 255)
            Else
                objfrmPayrollGroups_AddEdit.lblwebsite.Visible = True
                objfrmPayrollGroups_AddEdit.txtwebsite.Visible = True
                objfrmPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 208)
                objfrmPayrollGroups_AddEdit.Size = New Size(403, 309)
            End If

            If objfrmPayrollGroups_AddEdit.displayDialog(CInt(lvGroupList.SelectedItems(0).Tag), enAction.EDIT_ONE, mintGroupID) Then 'Sohail (19 Nov 2010)
                'If objfrmPayrollGroups_AddEdit.displayDialog(CInt(lvGroupList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(IIf(cboGroupList.SelectedIndex = 0, cboGroupList.SelectedIndex + 1, cboGroupList.SelectedIndex + 2))) Then
                Call fillList()
            End If
            objfrmPayrollGroups_AddEdit = Nothing

            lvGroupList.Items(intSelectedIndex).Selected = True
            lvGroupList.EnsureVisible(intSelectedIndex)
            lvGroupList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmPayrollGroups_AddEdit IsNot Nothing Then objfrmPayrollGroups_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (19 Nov 2010) -- Start
        'Dim strName As String = GetPayrollGroupTypeName()
        Dim strName As String = GetPayrollGroupTypeName(mintGroupID)
        'Sohail (19 Nov 2010) -- End

        If lvGroupList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select ") & strName & Language.getMessage(mstrModuleName, 2, " Group from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvGroupList.Select()
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- Start
        'If objMedicalMaster.isUsed(CInt(lvMastercodes.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this " & strName & " . Reason: This " & strName & " is in use."), enMsgBoxStyle.Information) '?2
        '    lvMastercodes.Select()
        '    Exit Sub
        'End If
        If objPayrollGroupMaster.isUsed(CInt(lvGroupList.SelectedItems(0).Tag), mintGroupID) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this ") & strName & Language.getMessage(mstrModuleName, 4, " . Reason: This ") & strName & Language.getMessage(mstrModuleName, 5, " is in use."), enMsgBoxStyle.Information) '?2
            lvGroupList.Select()
            Exit Sub
        End If
        'Sohail (19 Nov 2010) -- End
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvGroupList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete this ") & strName & Language.getMessage(mstrModuleName, 6, " Group ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objPayrollGroupMaster._FormName = mstrModuleName
                objPayrollGroupMaster._LoginEmployeeunkid = 0
                objPayrollGroupMaster._ClientIP = getIP()
                objPayrollGroupMaster._HostName = getHostName()
                objPayrollGroupMaster._FromWeb = False
                objPayrollGroupMaster._AuditUserId = User._Object._Userunkid
objPayrollGroupMaster._CompanyUnkid = Company._Object._Companyunkid
                objPayrollGroupMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objPayrollGroupMaster.Delete(CInt(lvGroupList.SelectedItems(0).Tag))
                lvGroupList.SelectedItems(0).Remove()

                If lvGroupList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvGroupList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvGroupList.Items.Count - 1
                    lvGroupList.Items(intSelectedIndex).Selected = True
                    lvGroupList.EnsureVisible(intSelectedIndex)
                ElseIf lvGroupList.Items.Count <> 0 Then
                    lvGroupList.Items(intSelectedIndex).Selected = True
                    lvGroupList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvGroupList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboGroupList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGroupList.SelectedIndexChanged
        Try
            'Sohail (19 Nov 2010) -- Start
            If cboGroupList.SelectedIndex = 0 Then
                mintGroupID = CInt(PayrollGroupType.CostCenter)
            ElseIf cboGroupList.SelectedIndex = 1 Then
                mintGroupID = CInt(PayrollGroupType.Bank)
            End If
            'Sohail (19 Nov 2010) -- End
            Call SetVisibility() 'Sohail (05 Mar 2013)
            fillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGroupList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsPayrollGroupList As New DataSet
        Dim strSearching As String = String.Empty
        Dim dtPayrollGroupTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewPayrollGroupList = True Then   'Pinkal (09-Jul-2012) -- Start

                dsPayrollGroupList = objPayrollGroupMaster.GetList("Group List")

                'Sohail (19 Nov 2010) -- Start
                'If CInt(cboGroupList.SelectedIndex) > -1 Then
                '    If cboGroupList.SelectedIndex = 1 Then
                '        strSearching = "AND grouptype_id =" & CInt(cboGroupList.SelectedIndex + 2)
                '    Else
                '        strSearching = "AND grouptype_id =" & CInt(cboGroupList.SelectedIndex + 1)
                '    End If
                'End If
                strSearching = "AND grouptype_id =" & mintGroupID
                'Sohail (19 Nov 2010) -- End

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtPayrollGroupTable = New DataView(dsPayrollGroupList.Tables("Group List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtPayrollGroupTable = dsPayrollGroupList.Tables("Group List")
                End If

                Dim lvItem As ListViewItem

                lvGroupList.Items.Clear()
                For Each drRow As DataRow In dtPayrollGroupTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("groupcode").ToString
                    lvItem.Tag = drRow("groupmasterunkid")
                    lvItem.SubItems.Add(drRow("groupname").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)
                    lvGroupList.Items.Add(lvItem)
                Next

                If lvGroupList.Items.Count > 12 Then 'Sohail (19 Nov 2010)
                    colhDescription.Width = 405 - 18
                Else
                    colhDescription.Width = 405
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsPayrollGroupList.Dispose()
        End Try
    End Sub

    Private Function GetPayrollGroupTypeName(ByVal intGroupID As Integer) As String
        Try
            'Sohail (19 Nov 2010) -- Start
            'If cboGroupList.SelectedIndex > -1 Then
            '    Select Case cboGroupList.SelectedIndex + 1
            '        Case CInt(PayrollGroupType.CostCenter)
            '            Return PayrollGroupType.CostCenter.ToString()
            '            'Case CInt(PayrollGroupType.Vendor)
            '            '    Return PayrollGroupType.Vendor.ToString()
            '        Case CInt(PayrollGroupType.Bank)
            '            Return PayrollGroupType.Bank.ToString()
            '    End Select
            'End If
            Select Case intGroupID
                Case CInt(PayrollGroupType.CostCenter)
                    Return PayrollGroupType.CostCenter.ToString()
                    'Case CInt(PayrollGroupType.Vendor)
                    '    Return PayrollGroupType.Vendor.ToString()
                Case CInt(PayrollGroupType.Bank)
                    Return PayrollGroupType.Bank.ToString()
            End Select
            'Sohail (19 Nov 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPayrollGroupTypeName", mstrModuleName)
        End Try
        Return "Master Type"
    End Function

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPayrollGroup
            btnEdit.Enabled = User._Object.Privilege._EditPayrollGroup
            'Sohail (05 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'btnDelete.Enabled = User._Object.Privilege._DeletePayrollGroup
            If cboGroupList.SelectedIndex = 0 Then
                btnDelete.Enabled = False
            ElseIf cboGroupList.SelectedIndex = 1 Then
                btnDelete.Enabled = User._Object.Privilege._DeletePayrollGroup
            End If
            'Sohail (05 Mar 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
            Me.lblGroupLists.Text = Language._Object.getCaption(Me.lblGroupLists.Name, Me.lblGroupLists.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select")
            Language.setMessage(mstrModuleName, 2, " Group from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this")
            Language.setMessage(mstrModuleName, 4, " . Reason: This")
            Language.setMessage(mstrModuleName, 5, " is in use.")
            Language.setMessage(mstrModuleName, 6, " Group ?")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete this")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
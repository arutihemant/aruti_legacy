﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmATEmail_LogView
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmATEmail_LogView))
        Me.gbGroupList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblViewType = New System.Windows.Forms.Label
        Me.cboViewType = New System.Windows.Forms.ComboBox
        Me.lblSubjectFilter = New System.Windows.Forms.Label
        Me.txtSubjectFilter = New System.Windows.Forms.TextBox
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.lblModuleName = New System.Windows.Forms.Label
        Me.lblMachine = New System.Windows.Forms.Label
        Me.txtModuleName = New System.Windows.Forms.TextBox
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.txtMachine = New System.Windows.Forms.TextBox
        Me.lblIPAddress = New System.Windows.Forms.Label
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.lblTodate = New System.Windows.Forms.Label
        Me.txtIPAddress = New System.Windows.Forms.TextBox
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboFilter = New System.Windows.Forms.ComboBox
        Me.objwbContent = New System.Windows.Forms.WebBrowser
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.lblSubject = New System.Windows.Forms.Label
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.gbGroupList.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbGroupList
        '
        Me.gbGroupList.BorderColor = System.Drawing.Color.Black
        Me.gbGroupList.Checked = False
        Me.gbGroupList.CollapseAllExceptThis = False
        Me.gbGroupList.CollapsedHoverImage = Nothing
        Me.gbGroupList.CollapsedNormalImage = Nothing
        Me.gbGroupList.CollapsedPressedImage = Nothing
        Me.gbGroupList.CollapseOnLoad = False
        Me.gbGroupList.Controls.Add(Me.lblViewType)
        Me.gbGroupList.Controls.Add(Me.cboViewType)
        Me.gbGroupList.Controls.Add(Me.lblSubjectFilter)
        Me.gbGroupList.Controls.Add(Me.txtSubjectFilter)
        Me.gbGroupList.Controls.Add(Me.cboViewBy)
        Me.gbGroupList.Controls.Add(Me.lblViewBy)
        Me.gbGroupList.Controls.Add(Me.lblModuleName)
        Me.gbGroupList.Controls.Add(Me.lblMachine)
        Me.gbGroupList.Controls.Add(Me.txtModuleName)
        Me.gbGroupList.Controls.Add(Me.dtpFromDate)
        Me.gbGroupList.Controls.Add(Me.txtMachine)
        Me.gbGroupList.Controls.Add(Me.lblIPAddress)
        Me.gbGroupList.Controls.Add(Me.lblFromDate)
        Me.gbGroupList.Controls.Add(Me.dtpTodate)
        Me.gbGroupList.Controls.Add(Me.lblTodate)
        Me.gbGroupList.Controls.Add(Me.txtIPAddress)
        Me.gbGroupList.Controls.Add(Me.btnReset)
        Me.gbGroupList.Controls.Add(Me.btnSearch)
        Me.gbGroupList.Controls.Add(Me.objbtnSearch)
        Me.gbGroupList.Controls.Add(Me.objlblCaption)
        Me.gbGroupList.Controls.Add(Me.cboFilter)
        Me.gbGroupList.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbGroupList.ExpandedHoverImage = Nothing
        Me.gbGroupList.ExpandedNormalImage = Nothing
        Me.gbGroupList.ExpandedPressedImage = Nothing
        Me.gbGroupList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGroupList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbGroupList.HeaderHeight = 25
        Me.gbGroupList.HeaderMessage = ""
        Me.gbGroupList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbGroupList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbGroupList.HeightOnCollapse = 0
        Me.gbGroupList.LeftTextSpace = 0
        Me.gbGroupList.Location = New System.Drawing.Point(0, 0)
        Me.gbGroupList.Name = "gbGroupList"
        Me.gbGroupList.OpenHeight = 63
        Me.gbGroupList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbGroupList.ShowBorder = True
        Me.gbGroupList.ShowCheckBox = False
        Me.gbGroupList.ShowCollapseButton = False
        Me.gbGroupList.ShowDefaultBorderColor = True
        Me.gbGroupList.ShowDownButton = False
        Me.gbGroupList.ShowHeader = True
        Me.gbGroupList.Size = New System.Drawing.Size(894, 89)
        Me.gbGroupList.TabIndex = 9
        Me.gbGroupList.Temp = 0
        Me.gbGroupList.Text = "Filter Criteria"
        Me.gbGroupList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblViewType
        '
        Me.lblViewType.BackColor = System.Drawing.Color.Transparent
        Me.lblViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewType.Location = New System.Drawing.Point(541, 5)
        Me.lblViewType.Name = "lblViewType"
        Me.lblViewType.Size = New System.Drawing.Size(71, 15)
        Me.lblViewType.TabIndex = 243
        Me.lblViewType.Text = "View Type"
        Me.lblViewType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewType
        '
        Me.cboViewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewType.DropDownWidth = 200
        Me.cboViewType.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cboViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewType.FormattingEnabled = True
        Me.cboViewType.Location = New System.Drawing.Point(618, 2)
        Me.cboViewType.Name = "cboViewType"
        Me.cboViewType.Size = New System.Drawing.Size(222, 21)
        Me.cboViewType.TabIndex = 242
        '
        'lblSubjectFilter
        '
        Me.lblSubjectFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubjectFilter.Location = New System.Drawing.Point(659, 63)
        Me.lblSubjectFilter.Name = "lblSubjectFilter"
        Me.lblSubjectFilter.Size = New System.Drawing.Size(77, 15)
        Me.lblSubjectFilter.TabIndex = 240
        Me.lblSubjectFilter.Text = "Subject"
        '
        'txtSubjectFilter
        '
        Me.txtSubjectFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubjectFilter.Location = New System.Drawing.Point(742, 60)
        Me.txtSubjectFilter.Name = "txtSubjectFilter"
        Me.txtSubjectFilter.Size = New System.Drawing.Size(140, 21)
        Me.txtSubjectFilter.TabIndex = 239
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(108, 33)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(135, 21)
        Me.cboViewBy.TabIndex = 237
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(8, 36)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(94, 15)
        Me.lblViewBy.TabIndex = 236
        Me.lblViewBy.Text = "Operation Mode"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblModuleName
        '
        Me.lblModuleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModuleName.Location = New System.Drawing.Point(276, 36)
        Me.lblModuleName.Name = "lblModuleName"
        Me.lblModuleName.Size = New System.Drawing.Size(77, 15)
        Me.lblModuleName.TabIndex = 224
        Me.lblModuleName.Text = "Screen Name"
        '
        'lblMachine
        '
        Me.lblMachine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMachine.Location = New System.Drawing.Point(659, 36)
        Me.lblMachine.Name = "lblMachine"
        Me.lblMachine.Size = New System.Drawing.Size(77, 15)
        Me.lblMachine.TabIndex = 225
        Me.lblMachine.Text = "Machine Name"
        '
        'txtModuleName
        '
        Me.txtModuleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModuleName.Location = New System.Drawing.Point(359, 33)
        Me.txtModuleName.Name = "txtModuleName"
        Me.txtModuleName.Size = New System.Drawing.Size(134, 21)
        Me.txtModuleName.TabIndex = 222
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(566, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(87, 21)
        Me.dtpFromDate.TabIndex = 207
        '
        'txtMachine
        '
        Me.txtMachine.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMachine.Location = New System.Drawing.Point(742, 33)
        Me.txtMachine.Name = "txtMachine"
        Me.txtMachine.Size = New System.Drawing.Size(140, 21)
        Me.txtMachine.TabIndex = 222
        '
        'lblIPAddress
        '
        Me.lblIPAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIPAddress.Location = New System.Drawing.Point(276, 63)
        Me.lblIPAddress.Name = "lblIPAddress"
        Me.lblIPAddress.Size = New System.Drawing.Size(77, 15)
        Me.lblIPAddress.TabIndex = 223
        Me.lblIPAddress.Text = "IP Address"
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(499, 36)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(61, 15)
        Me.lblFromDate.TabIndex = 208
        Me.lblFromDate.Text = "From Date"
        '
        'dtpTodate
        '
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(566, 60)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(87, 21)
        Me.dtpTodate.TabIndex = 209
        '
        'lblTodate
        '
        Me.lblTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodate.Location = New System.Drawing.Point(499, 63)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(61, 15)
        Me.lblTodate.TabIndex = 210
        Me.lblTodate.Text = "To"
        '
        'txtIPAddress
        '
        Me.txtIPAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIPAddress.Location = New System.Drawing.Point(359, 60)
        Me.txtIPAddress.Name = "txtIPAddress"
        Me.txtIPAddress.Size = New System.Drawing.Size(134, 21)
        Me.txtIPAddress.TabIndex = 222
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(870, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 219
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(846, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 220
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(249, 60)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 87
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(8, 63)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(94, 15)
        Me.objlblCaption.TabIndex = 1
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilter
        '
        Me.cboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilter.DropDownWidth = 450
        Me.cboFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilter.FormattingEnabled = True
        Me.cboFilter.Location = New System.Drawing.Point(108, 60)
        Me.cboFilter.Name = "cboFilter"
        Me.cboFilter.Size = New System.Drawing.Size(135, 21)
        Me.cboFilter.TabIndex = 2
        '
        'objwbContent
        '
        Me.objwbContent.AllowNavigation = False
        Me.objwbContent.AllowWebBrowserDrop = False
        Me.objwbContent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objwbContent.IsWebBrowserContextMenuEnabled = False
        Me.objwbContent.Location = New System.Drawing.Point(0, 0)
        Me.objwbContent.MinimumSize = New System.Drawing.Size(20, 20)
        Me.objwbContent.Name = "objwbContent"
        Me.objwbContent.Size = New System.Drawing.Size(892, 233)
        Me.objwbContent.TabIndex = 11
        Me.objwbContent.WebBrowserShortcutsEnabled = False
        '
        'objpnlData
        '
        Me.objpnlData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objpnlData.Controls.Add(Me.objwbContent)
        Me.objpnlData.Location = New System.Drawing.Point(0, 281)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(894, 235)
        Me.objpnlData.TabIndex = 12
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(894, 55)
        Me.objFooter.TabIndex = 14
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(11, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(131, 30)
        Me.btnDelete.TabIndex = 120
        Me.btnDelete.Text = "&Delete permanently"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtSubject
        '
        Me.txtSubject.BackColor = System.Drawing.Color.White
        Me.txtSubject.Location = New System.Drawing.Point(61, 256)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.ReadOnly = True
        Me.txtSubject.Size = New System.Drawing.Size(830, 20)
        Me.txtSubject.TabIndex = 15
        '
        'lblSubject
        '
        Me.lblSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubject.Location = New System.Drawing.Point(4, 259)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(51, 15)
        Me.lblSubject.TabIndex = 209
        Me.lblSubject.Text = "Subject"
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToOrderColumns = True
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Location = New System.Drawing.Point(0, 90)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(893, 160)
        Me.dgvData.TabIndex = 210
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 95)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 211
        Me.objchkAll.UseVisualStyleBackColor = True
        Me.objchkAll.Visible = False
        '
        'frmATEmail_LogView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 572)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.lblSubject)
        Me.Controls.Add(Me.txtSubject)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.objpnlData)
        Me.Controls.Add(Me.gbGroupList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmATEmail_LogView"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View Email Log Tran"
        Me.gbGroupList.ResumeLayout(False)
        Me.gbGroupList.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbGroupList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents lblModuleName As System.Windows.Forms.Label
    Friend WithEvents lblMachine As System.Windows.Forms.Label
    Friend WithEvents txtModuleName As System.Windows.Forms.TextBox
    Friend WithEvents lblIPAddress As System.Windows.Forms.Label
    Friend WithEvents txtMachine As System.Windows.Forms.TextBox
    Friend WithEvents txtIPAddress As System.Windows.Forms.TextBox
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents objwbContent As System.Windows.Forms.WebBrowser
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblSubjectFilter As System.Windows.Forms.Label
    Friend WithEvents txtSubjectFilter As System.Windows.Forms.TextBox
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents cboViewType As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewType As System.Windows.Forms.Label
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmADMapHierarchyLevel
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmADMapHierarchyLevel))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbMapAttributeHierarchyLevel = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgcolhParentAllocation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhParentAllocationId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhHierarchyId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objgbMapAttributeHierarchyLevel.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbMapAttributeHierarchyLevel)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(757, 457)
        Me.pnlMain.TabIndex = 0
        '
        'objgbMapAttributeHierarchyLevel
        '
        Me.objgbMapAttributeHierarchyLevel.BorderColor = System.Drawing.Color.Black
        Me.objgbMapAttributeHierarchyLevel.Checked = False
        Me.objgbMapAttributeHierarchyLevel.CollapseAllExceptThis = False
        Me.objgbMapAttributeHierarchyLevel.CollapsedHoverImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.CollapsedNormalImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.CollapsedPressedImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.CollapseOnLoad = False
        Me.objgbMapAttributeHierarchyLevel.Controls.Add(Me.objpnlData)
        Me.objgbMapAttributeHierarchyLevel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbMapAttributeHierarchyLevel.ExpandedHoverImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.ExpandedNormalImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.ExpandedPressedImage = Nothing
        Me.objgbMapAttributeHierarchyLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbMapAttributeHierarchyLevel.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbMapAttributeHierarchyLevel.HeaderHeight = 25
        Me.objgbMapAttributeHierarchyLevel.HeaderMessage = ""
        Me.objgbMapAttributeHierarchyLevel.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbMapAttributeHierarchyLevel.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbMapAttributeHierarchyLevel.HeightOnCollapse = 0
        Me.objgbMapAttributeHierarchyLevel.LeftTextSpace = 0
        Me.objgbMapAttributeHierarchyLevel.Location = New System.Drawing.Point(0, 0)
        Me.objgbMapAttributeHierarchyLevel.Name = "objgbMapAttributeHierarchyLevel"
        Me.objgbMapAttributeHierarchyLevel.OpenHeight = 300
        Me.objgbMapAttributeHierarchyLevel.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbMapAttributeHierarchyLevel.ShowBorder = True
        Me.objgbMapAttributeHierarchyLevel.ShowCheckBox = False
        Me.objgbMapAttributeHierarchyLevel.ShowCollapseButton = False
        Me.objgbMapAttributeHierarchyLevel.ShowDefaultBorderColor = True
        Me.objgbMapAttributeHierarchyLevel.ShowDownButton = False
        Me.objgbMapAttributeHierarchyLevel.ShowHeader = True
        Me.objgbMapAttributeHierarchyLevel.Size = New System.Drawing.Size(757, 402)
        Me.objgbMapAttributeHierarchyLevel.TabIndex = 69
        Me.objgbMapAttributeHierarchyLevel.Temp = 0
        Me.objgbMapAttributeHierarchyLevel.Text = "Map Attribute Hierarchy Level"
        Me.objgbMapAttributeHierarchyLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.dgData)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(1, 25)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(755, 376)
        Me.objpnlData.TabIndex = 130
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhParentAllocation, Me.objdgcolhParentAllocationId, Me.objdgcolhHierarchyId})
        Me.dgData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgData.Location = New System.Drawing.Point(0, 0)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(755, 376)
        Me.dgData.TabIndex = 129
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 402)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(757, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(553, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(93, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(652, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgcolhParentAllocation
        '
        Me.dgcolhParentAllocation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhParentAllocation.HeaderText = ""
        Me.dgcolhParentAllocation.Name = "dgcolhParentAllocation"
        Me.dgcolhParentAllocation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhParentAllocationId
        '
        Me.objdgcolhParentAllocationId.HeaderText = "AllocationId"
        Me.objdgcolhParentAllocationId.Name = "objdgcolhParentAllocationId"
        Me.objdgcolhParentAllocationId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhParentAllocationId.Visible = False
        Me.objdgcolhParentAllocationId.Width = 5
        '
        'objdgcolhHierarchyId
        '
        Me.objdgcolhHierarchyId.HeaderText = "objdgcolhHierarchyId"
        Me.objdgcolhHierarchyId.Name = "objdgcolhHierarchyId"
        Me.objdgcolhHierarchyId.ReadOnly = True
        Me.objdgcolhHierarchyId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhHierarchyId.Visible = False
        '
        'frmADMapHierarchyLevel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(757, 457)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmADMapHierarchyLevel"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Map Attribute Hierarchy Level"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbMapAttributeHierarchyLevel.ResumeLayout(False)
        Me.objpnlData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objgbMapAttributeHierarchyLevel As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhParentAllocation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhParentAllocationId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhHierarchyId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

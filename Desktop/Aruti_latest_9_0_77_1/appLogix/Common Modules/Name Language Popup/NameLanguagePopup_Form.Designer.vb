<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NameLanguagePopup_Form
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NameLanguagePopup_Form))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbNameLanguage = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName2 = New System.Windows.Forms.Label
        Me.lblName1 = New System.Windows.Forms.Label
        Me.txtName1 = New eZee.TextBox.AlphanumericTextBox
        Me.txtName2 = New eZee.TextBox.AlphanumericTextBox
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbNameLanguage.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbNameLanguage)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(348, 185)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbNameLanguage
        '
        Me.gbNameLanguage.BorderColor = System.Drawing.Color.Black
        Me.gbNameLanguage.Checked = False
        Me.gbNameLanguage.CollapseAllExceptThis = False
        Me.gbNameLanguage.CollapsedHoverImage = Nothing
        Me.gbNameLanguage.CollapsedNormalImage = Nothing
        Me.gbNameLanguage.CollapsedPressedImage = Nothing
        Me.gbNameLanguage.CollapseOnLoad = False
        Me.gbNameLanguage.Controls.Add(Me.lblName)
        Me.gbNameLanguage.Controls.Add(Me.txtName)
        Me.gbNameLanguage.Controls.Add(Me.lblName2)
        Me.gbNameLanguage.Controls.Add(Me.lblName1)
        Me.gbNameLanguage.Controls.Add(Me.txtName1)
        Me.gbNameLanguage.Controls.Add(Me.txtName2)
        Me.gbNameLanguage.ExpandedHoverImage = Nothing
        Me.gbNameLanguage.ExpandedNormalImage = Nothing
        Me.gbNameLanguage.ExpandedPressedImage = Nothing
        Me.gbNameLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbNameLanguage.GradientColor = System.Drawing.Color.Transparent
        Me.gbNameLanguage.HeaderHeight = 25
        Me.gbNameLanguage.HeightOnCollapse = 0
        Me.gbNameLanguage.LeftTextSpace = 0
        Me.gbNameLanguage.Location = New System.Drawing.Point(9, 4)
        Me.gbNameLanguage.Name = "gbNameLanguage"
        Me.gbNameLanguage.OpenHeight = 119
        Me.gbNameLanguage.Padding = New System.Windows.Forms.Padding(3)
        Me.gbNameLanguage.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbNameLanguage.ShowBorder = True
        Me.gbNameLanguage.ShowCheckBox = False
        Me.gbNameLanguage.ShowCollapseButton = False
        Me.gbNameLanguage.ShowDefaultBorderColor = True
        Me.gbNameLanguage.ShowDownButton = False
        Me.gbNameLanguage.ShowHeader = True
        Me.gbNameLanguage.Size = New System.Drawing.Size(339, 119)
        Me.gbNameLanguage.TabIndex = 2
        Me.gbNameLanguage.Temp = 0
        Me.gbNameLanguage.Text = "Language"
        Me.gbNameLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(9, 36)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(83, 13)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(98, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(230, 21)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "name"
        '
        'lblName2
        '
        Me.lblName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName2.Location = New System.Drawing.Point(9, 90)
        Me.lblName2.Name = "lblName2"
        Me.lblName2.Size = New System.Drawing.Size(83, 13)
        Me.lblName2.TabIndex = 4
        Me.lblName2.Text = "Name 2"
        Me.lblName2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName1
        '
        Me.lblName1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName1.Location = New System.Drawing.Point(9, 63)
        Me.lblName1.Name = "lblName1"
        Me.lblName1.Size = New System.Drawing.Size(83, 13)
        Me.lblName1.TabIndex = 2
        Me.lblName1.Text = "Name 1"
        Me.lblName1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName1
        '
        Me.txtName1.Flags = 0
        Me.txtName1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName1.Location = New System.Drawing.Point(98, 59)
        Me.txtName1.Name = "txtName1"
        Me.txtName1.Size = New System.Drawing.Size(230, 21)
        Me.txtName1.TabIndex = 3
        Me.txtName1.Tag = "name"
        '
        'txtName2
        '
        Me.txtName2.Flags = 0
        Me.txtName2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName2.Location = New System.Drawing.Point(98, 86)
        Me.txtName2.Name = "txtName2"
        Me.txtName2.Size = New System.Drawing.Size(230, 21)
        Me.txtName2.TabIndex = 5
        Me.txtName2.Tag = "name"
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 130)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(348, 55)
        Me.objefFormFooter.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(250, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(154, 12)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&OK"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'NameLanguagePopup_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 185)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "NameLanguagePopup_Form"

        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Language"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbNameLanguage.ResumeLayout(False)
        Me.gbNameLanguage.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbNameLanguage As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName2 As System.Windows.Forms.Label
    Friend WithEvents lblName1 As System.Windows.Forms.Label
    Friend WithEvents txtName1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
End Class

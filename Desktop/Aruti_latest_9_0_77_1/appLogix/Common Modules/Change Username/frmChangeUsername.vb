﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmChangeUsername

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmChangeUsername"
    Private mblnCancel As Boolean = True
    Private objUser As clsUserAddEdit
    Private mintUserId As Integer = -1

#End Region

#Region " DisplayDialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtConfUName.BackColor = GUI.ColorComp
            txtNewUName.BackColor = GUI.ColorComp
            txtOldUName.BackColor = GUI.ColorComp
            txtPassword.BackColor = GUI.ColorComp
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUser._Userunkid = mintUserId
            objUser._Username = txtConfUName.Text.Trim
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function isValid() As Boolean
        Try
            If txtOldUName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Old Username is mandatory information. Please provide Old Username."), enMsgBoxStyle.Information)
                txtOldUName.Focus()
                Return False
            End If

            If txtNewUName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "New Username is mandatory information. Please provide New Username."), enMsgBoxStyle.Information)
                txtNewUName.Focus()
                Return False
            End If

            If txtConfUName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "New Username is mandatory information. Please provide New Username."), enMsgBoxStyle.Information)
                txtConfUName.Focus()
                Return False
            End If

            mintUserId = objUser.Return_UserId(txtOldUName.Text.Trim, "hrmsConfiguration", enLoginMode.USER)

            objUser._Userunkid = mintUserId

            If mintUserId > 0 Then
                If txtPassword.Text <> objUser._Password Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Password does not match. Please provide correct password."), enMsgBoxStyle.Information)
                    txtPassword.Focus()
                    Return False
                End If
            Else
                If txtPassword.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Password is mandatory information. Please provide Password."), enMsgBoxStyle.Information)
                    txtPassword.Focus()
                    Return False
                End If
            End If

            If mintUserId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Old Username does not exist in the system. Please provide correct username."), enMsgBoxStyle.Information)
                txtOldUName.Focus()
                Return False
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            mRegx = New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9 ]*$")

            If mRegx.IsMatch(txtOldUName.Text.Trim) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Special characters not allowed in username."), enMsgBoxStyle.Information)
                txtOldUName.Focus()
                Return False
            End If

            If mRegx.IsMatch(txtNewUName.Text.Trim) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Special characters not allowed in username."), enMsgBoxStyle.Information)
                txtNewUName.Focus()
                Return False
            End If

            If mRegx.IsMatch(txtConfUName.Text.Trim) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Special characters not allowed in username."), enMsgBoxStyle.Information)
                txtConfUName.Focus()
                Return False
            End If

            If txtPassword.Text <> objUser._Password Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Password does not match. Please provide correct password."), enMsgBoxStyle.Information)
                txtPassword.Focus()
                Return False
            End If

            Dim objPwd As New clsPassowdOptions
            If objPwd._IsUsrNameLengthSet = True Then
                If txtNewUName.Text.Trim.Length < objPwd._UsrNameLength Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Username cannot be less then ") & objPwd._UsrNameLength & Language.getMessage(mstrModuleName, 10, " character(s)."), enMsgBoxStyle.Information)
                    txtNewUName.Focus()
                    Return False
                End If
            End If

            If txtNewUName.Text <> txtConfUName.Text Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Username does not match. Please provide correct username."), enMsgBoxStyle.Information)
                txtConfUName.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "isValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmChangeUsername_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objUser = New clsUserAddEdit
            Call OtherSettings()
            Call SetColor()
            txtOldUName.Focus()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmChangeUsername_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmChangeUsername_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmChangeUsername_KeyDown", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If isValid() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objUser._FormName = mstrModuleName
            objUser._LoginEmployeeunkid = 0
            objUser._ClientIP = getIP()
            objUser._HostName = getHostName()
            objUser._FromWeb = False
            objUser._AuditUserId = User._Object._Userunkid
objUser._CompanyUnkid = Company._Object._Companyunkid
            objUser._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objUser.Update(True) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Username changed successfully. Please login with new username."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            Else
                eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbChangeUsername.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbChangeUsername.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbChangeUsername.Text = Language._Object.getCaption(Me.gbChangeUsername.Name, Me.gbChangeUsername.Text)
			Me.lblOldUname.Text = Language._Object.getCaption(Me.lblOldUname.Name, Me.lblOldUname.Text)
			Me.lblConfUName.Text = Language._Object.getCaption(Me.lblConfUName.Name, Me.lblConfUName.Text)
			Me.lblNewUsername.Text = Language._Object.getCaption(Me.lblNewUsername.Name, Me.lblNewUsername.Text)
			Me.lblPwd.Text = Language._Object.getCaption(Me.lblPwd.Name, Me.lblPwd.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Old Username is mandatory information. Please provide Old Username.")
			Language.setMessage(mstrModuleName, 2, "New Username is mandatory information. Please provide New Username.")
			Language.setMessage(mstrModuleName, 3, "Username does not match. Please provide correct username.")
			Language.setMessage(mstrModuleName, 4, "Password is mandatory information. Please provide Password.")
			Language.setMessage(mstrModuleName, 5, "Old Username does not exist in the system. Please provide correct username.")
			Language.setMessage(mstrModuleName, 6, "Special characters not allowed in username.")
			Language.setMessage(mstrModuleName, 7, "Password does not match. Please provide correct password.")
			Language.setMessage(mstrModuleName, 8, "Username changed successfully. Please login with new username.")
			Language.setMessage(mstrModuleName, 9, "Username cannot be less then")
			Language.setMessage(mstrModuleName, 10, " character(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
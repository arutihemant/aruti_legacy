#Region " Imports "
Imports eZeeCommonLib
Imports Microsoft.SqlServer
Imports Aruti.Data
Imports Aruti.Data.Language
#End Region

Public Class frmRestore

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmRestore"
    Dim mstrMsgSuccess As String = ""
    Dim mstrMsgCheckFile As String = ""
    Dim objBackup As clsBackup
    Dim mblnCancel As Boolean = True
    Dim WithEvents objDatabase As New eZeeDatabase
    'S.SANDEEP [16 FEB 2015] -- START
    Dim objBackupDatabase As New eZeeDatabase
    'S.SANDEEP [16 FEB 2015] -- END
    Dim blnIsFromConfig As Boolean = False

    'S.SANDEEP [16 FEB 2015] -- START
    Private mstrErrorMsg As String = String.Empty
    'S.SANDEEP [16 FEB 2015] -- END

    'S.SANDEEP [04 MAR 2015] -- START
    Private mblnIsHandlerRemoved As Boolean = False
    'S.SANDEEP [04 MAR 2015] -- END

#End Region

    'Sandeep [ 10 FEB 2011 ] -- Start
    '#Region " Display Dialog "

    '    Public Function displayDialog(ByVal blnConfiguration As Boolean) As Boolean
    '        Try

    '            blnIsFromConfig = blnConfiguration

    '            Me.ShowDialog()

    '            Return Not mblnCancel
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
    '        End Try
    '    End Function

    '#End Region
    'Sandeep [ 10 FEB 2011 ] -- End 

#Region " Private Function "
    Private Sub FillList()
        Dim strName As String = ""
        Dim dsList As DataSet = Nothing
        Dim lvItem As ListViewItem = Nothing
        Try

            'Sandeep [ 10 FEB 2011 ] -- Start
            'dsList = objBackup.getList(blnIsFromConfig, Company._Object._Companyunkid)
            dsList = objBackup.getList()
            'Sandeep [ 10 FEB 2011 ] -- End 
            lvRestore.Items.Clear()
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = eZeeDate.convertDate(dtRow.Item("backup_date").ToString).ToShortDateString
                lvItem.Tag = dtRow.Item("backupunkid")
                lvItem.SubItems.Add(dtRow.Item("backup_time"))
                lvItem.SubItems.Add(dtRow.Item("backup_path"))
                lvItem.SubItems.Add(dtRow.Item("UserName"))

                lvRestore.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvRestore.Items.Count > 13 Then
                colhBackupPath.Width = 333 - 18
            Else
                colhBackupPath.Width = 333
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events  "
    Private Sub frmRestore_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        ElseIf Asc(e.KeyChar) = 27 Then
            Call btnCancel.PerformClick()
        End If
    End Sub

    Private Sub frmRestore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            objBackup = New clsBackup
            Call Aruti.Data.Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call FillList()

            mstrMsgSuccess = Aruti.Data.Language.getMessage(mstrModuleName, 2, "Congratulations!! Database has been Restored successfully.") & vbCrLf & Aruti.Data.Language.getMessage(mstrModuleName, 3, "Database Restore will need to Restart Application.")
            mstrMsgCheckFile = Aruti.Data.Language.getMessage(mstrModuleName, 1, "Please Select backup file from folder, CD or Flash Drive.")

            If lvRestore.Items.Count > 0 Then lvRestore.Items(0).Selected = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRestore_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBackup.SetMessages()
            objfrm._Other_ModuleNames = "clsBackup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            ofdRestore.Filter = "DAT files(*.dat)|*.dat"
            If ofdRestore.ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtRestorePath.Text = ofdRestore.FileName
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnBrowse_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub


    'Sandeep [ 10 FEB 2011 ] -- Start
    Private Sub btnRestore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestore.Click
        Dim StrDataBaseName As String = String.Empty
        Dim StrBackUpPath As String = ""
        Dim strNewBKPath As String = String.Empty
        Try
            'S.SANDEEP [16 FEB 2015] -- START
            mstrErrorMsg = ""
            'S.SANDEEP [16 FEB 2015] -- END


            Me.Cursor = Cursors.WaitCursor
            Dim objAppSettings As New clsApplicationSettings
            If IO.Directory.Exists(objAppSettings._ApplicationPath & "DataBackup") = False Then
                IO.Directory.CreateDirectory(objAppSettings._ApplicationPath & "DataBackup")
            End If

            StrBackUpPath = objAppSettings._ApplicationPath & "DataBackup"

            If txtRestorePath.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Restore path cannot be blank. Please select the Restore path by browse button or clik on the list to select the restore path."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If System.IO.File.Exists(txtRestorePath.Text.Trim) Then
                Dim flInfo As New System.IO.FileInfo(txtRestorePath.Text.Trim)

                If flInfo.Name.StartsWith("hrmsConfiguration") Then
                    StrDataBaseName = flInfo.Name.Substring(0, flInfo.Name.IndexOf("_"))
                    If objBackup.Is_DatabasePresent(StrDataBaseName) Then
                        '< TAKING BACKUP OF EXISTING DATABASE : HRMSCONFIGURATION >
                        If IO.Directory.Exists(StrBackUpPath & "\" & StrDataBaseName) = False Then
                            IO.Directory.CreateDirectory(StrBackUpPath & "\" & StrDataBaseName)
                        End If
                        strNewBKPath = StrBackUpPath & "\" & StrDataBaseName

                        'S.SANDEEP [16 FEB 2015] -- START
                        'objDatabase.Backup(strNewBKPath)
                        objBackupDatabase.Backup(strNewBKPath, General_Settings._Object._ServerName)
                        'S.SANDEEP [16 FEB 2015] -- END

                        '< RESTORING THE SELECTED DATABASE >
                        objDatabase.Restore(txtRestorePath.Text.Trim)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Restore Failed, No such database exists. Please contact your Administrator."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Pinkal (18-Apr-2013) -- Start
                    'Enhancement : TRA Changes

                ElseIf flInfo.Name.StartsWith("arutiimages") Then
                    StrDataBaseName = flInfo.Name.Substring(0, flInfo.Name.IndexOf("_"))
                    If objBackup.Is_DatabasePresent(StrDataBaseName) Then
                        '< TAKING BACKUP OF EXISTING DATABASE : ARUTIIMAGES >
                        If IO.Directory.Exists(StrBackUpPath & "\" & StrDataBaseName) = False Then
                            IO.Directory.CreateDirectory(StrBackUpPath & "\" & StrDataBaseName)
                        End If
                        strNewBKPath = StrBackUpPath & "\" & StrDataBaseName
                        eZeeDatabase.change_database(StrDataBaseName)

                        'S.SANDEEP [16 FEB 2015] -- START
                        'objDatabase.Backup(strNewBKPath)
                        objBackupDatabase.Backup(strNewBKPath, General_Settings._Object._ServerName)
                        'S.SANDEEP [16 FEB 2015] -- END


                        '< RESTORING THE SELECTED DATABASE >
                        objDatabase.Restore(txtRestorePath.Text.Trim)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Restore Failed, No such database exists. Please contact your Administrator."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Pinkal (18-Apr-2013) -- End


                ElseIf flInfo.Name.StartsWith("tran_") Then
                    StrDataBaseName = flInfo.Name.Substring(0, flInfo.Name.LastIndexOf("_"))
                    StrDataBaseName = StrDataBaseName.Substring(0, StrDataBaseName.LastIndexOf("_"))
                    If objBackup.Is_DatabasePresent(StrDataBaseName) Then
                        If IO.Directory.Exists(StrBackUpPath & "\" & StrDataBaseName) = False Then
                            IO.Directory.CreateDirectory(StrBackUpPath & "\" & StrDataBaseName)
                        End If
                        strNewBKPath = StrBackUpPath & "\" & StrDataBaseName


                        eZeeDatabase.change_database(StrDataBaseName)
                        '< TAKING BACKUP OF EXISTING DATABASE : STRDATABASENAME >

                        'S.SANDEEP [16 FEB 2015] -- START
                        'objDatabase.Backup(strNewBKPath)
                        objBackupDatabase.Backup(strNewBKPath, General_Settings._Object._ServerName)
                        'S.SANDEEP [16 FEB 2015] -- END

                        '< RESTORING THE SELECTED DATABASE >
                        objDatabase.Restore(txtRestorePath.Text.Trim)
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Restore Failed, No such database exists. Please contact your Administrator."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If

                'S.SANDEEP [19 FEB 2015] -- START
                If mstrErrorMsg <> "" Then
                    'S.SANDEEP [04 MAR 2015] -- START
                    mblnIsHandlerRemoved = False
                    'S.SANDEEP [04 MAR 2015] -- END
                    eZeeMsgBox.Show(mstrErrorMsg, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'S.SANDEEP [19 FEB 2015] -- END


                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Database successfully restored. Please restart Application."), enMsgBoxStyle.Information)

                'Dim Dinfo As New System.IO.DirectoryInfo(objAppSettings._ApplicationPath)
                'Dim aryFi As IO.FileInfo() = Dinfo.GetFiles("*.exe")
                'Dim fi As IO.FileInfo
                'Dim StrFileName As String = ""
                'For Each fi In aryFi
                '    If FileVersionInfo.GetVersionInfo(objAppSettings._ApplicationPath & fi.Name).OriginalFilename = "ArutiConfiguration.exe" Then
                '        StrFileName = fi.Name
                '    End If
                '    Exit For
                'Next
                'Shell(objAppSettings._ApplicationPath & StrFileName, AppWinStyle.MaximizedFocus)
                blnIsIdealTimeSet = True

                Application.Exit()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrAccessLevelIds, 4, "Specified Path does not have any such file to restore. Either file is moved or deleted."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRestore_Click", mstrModuleName)
        Finally
            'S.SANDEEP [16 FEB 2015] -- START
            'eZeeDatabase.change_database("hrmsConfiguration")
            Try
                eZeeDatabase.change_database("hrmsConfiguration")
            Catch ex As Exception
                eZeeDatabase.change_database("hrmsConfiguration")
            End Try
            'S.SANDEEP [16 FEB 2015] -- END
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    'Private Sub btnRestore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestore.Click
    '    Try
    '        Dim objAppSettings As New clsApplicationSettings
    '        If IO.Directory.Exists(objAppSettings._ApplicationPath & "DataBackup") = False Then
    '            IO.Directory.CreateDirectory(objAppSettings._ApplicationPath & "DataBackup")
    '        End If

    '        objDatabase.Backup(objAppSettings._ApplicationPath & "DataBackup")


    '        If System.IO.File.Exists(txtRestorePath.Text) Then
    '            pbRestore.Maximum = 100
    '            pbRestore.Style = ProgressBarStyle.Blocks
    '            pbRestore.Value = 0
    '            pbRestore.Step = 10

    '            Me.Cursor = Cursors.WaitCursor

    '            objDatabase.Restore(txtRestorePath.Text)

    '            mblnCancel = False
    '            Me.Close()
    '        Else
    '            eZeeMsgBox.Show(mstrMsgCheckFile, enMsgBoxStyle.Information)
    '        End If

    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "btnRestore_Click", mstrModuleName)
    '    Finally
    '        Me.Cursor = Cursors.Default
    '    End Try
    'End Sub

    'Sandeep [ 10 FEB 2011 ] -- End 

#End Region

#Region " Other Control's Events "

    Private Sub lvRestore_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvRestore.MouseClick
        If lvRestore.SelectedItems.Count > 0 Then
            'Sandeep [ 10 FEB 2011 ] -- Start
            'txtRestorePath.Text = lvRestore.SelectedItems(0).SubItems(2).Text
            txtRestorePath.Text = lvRestore.SelectedItems(0).SubItems(colhBackupPath.Index).Text
            'Sandeep [ 10 FEB 2011 ] -- End 
        End If
    End Sub

    'Sandeep [ 10 FEB 2011 ] -- Start
    'Private Sub objDatabase_Completed(ByVal sender As Object, ByVal e As Microsoft.SqlServer.Management.Common.ServerMessageEventArgs) Handles objDatabase.Completed
    '    eZeeMsgBox.Show(mstrMsgSuccess, enMsgBoxStyle.Information)
    '    pbRestore.Value = 0
    'End Sub

    'Private Sub objDatabase_PercentComplete(ByVal sender As Object, ByVal e As Microsoft.SqlServer.Management.Smo.PercentCompleteEventArgs) Handles objDatabase.PercentComplete
    '    pbRestore.Value = e.Percent
    'End Sub
    'Sandeep [ 10 FEB 2011 ] -- End 

#End Region

    'S.SANDEEP [16 FEB 2015] -- START
#Region " Controls Events "

    Private Sub objDatabase_Progress(ByVal sender As Object, ByVal e As System.Data.SqlClient.SqlInfoMessageEventArgs) Handles objDatabase.Progress
        Try
            Application.DoEvents()
            For Each info As SqlClient.SqlError In e.Errors
                If info.Class > 10 Then
                    RemoveHandler objDatabase.Progress, AddressOf objDatabase_Progress
                    mstrErrorMsg &= info.Message & vbCrLf
                    'S.SANDEEP [04 MAR 2015] -- START
                    mblnIsHandlerRemoved = True
                    'S.SANDEEP [04 MAR 2015] -- END
                    Exit Sub
                End If
                If IsNumeric(info.Message.Split(CChar(" "))(0)) Then
                    pbProgress.Value = CInt(info.Message.Split(CChar(" "))(0))
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objDatabase_Progress", mstrModuleName)
        Finally
            'S.SANDEEP [04 MAR 2015] -- START
            'AddHandler objDatabase.Progress, AddressOf objDatabase_Progress
            If mblnIsHandlerRemoved = True Then
                AddHandler objDatabase.Progress, AddressOf objDatabase_Progress
            End If
            'S.SANDEEP [04 MAR 2015] -- END
        End Try
    End Sub

#End Region
    'S.SANDEEP [16 FEB 2015] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.btnBrowse.GradientBackColor = GUI._ButttonBackColor
            Me.btnBrowse.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnRestore.GradientBackColor = GUI._ButttonBackColor
            Me.btnRestore.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblRestorePath.Text = Language._Object.getCaption(Me.lblRestorePath.Name, Me.lblRestorePath.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhTime.Text = Language._Object.getCaption(CStr(Me.colhTime.Tag), Me.colhTime.Text)
            Me.colhBackupPath.Text = Language._Object.getCaption(CStr(Me.colhBackupPath.Tag), Me.colhBackupPath.Text)
            Me.colhUser.Text = Language._Object.getCaption(CStr(Me.colhUser.Tag), Me.colhUser.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnBrowse.Text = Language._Object.getCaption(Me.btnBrowse.Name, Me.btnBrowse.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnRestore.Text = Language._Object.getCaption(Me.btnRestore.Name, Me.btnRestore.Text)
            Me.pbProgress.Text = Language._Object.getCaption(Me.pbProgress.Name, Me.pbProgress.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrAccessLevelIds, 4, "Specified Path does not have any such file to restore. Either file is moved or deleted.")
            Language.setMessage(mstrModuleName, 1, "Please Select backup file from folder, CD or Flash Drive.")
            Language.setMessage(mstrModuleName, 2, "Congratulations!! Database has been Restored successfully.")
            Language.setMessage(mstrModuleName, 3, "Database Restore will need to Restart Application.")
            Language.setMessage(mstrModuleName, 4, "Restore path cannot be blank. Please select the Restore path by browse button or clik on the list to select the restore path.")
            Language.setMessage(mstrModuleName, 5, "Restore Failed, No such database exists. Please contact your Administrator.")
            Language.setMessage(mstrModuleName, 6, "Database successfully restored. Please restart Application.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
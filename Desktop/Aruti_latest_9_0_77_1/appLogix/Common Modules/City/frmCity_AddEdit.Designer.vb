﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCity_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCity_AddEdit))
        Me.txtCityName = New eZee.TextBox.AlphanumericTextBox
        Me.pnlCity = New System.Windows.Forms.Panel
        Me.gbCity = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddState = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.lblCityName = New System.Windows.Forms.Label
        Me.txtCityCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCityCode = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlCity.SuspendLayout()
        Me.gbCity.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCityName
        '
        Me.txtCityName.Flags = 0
        Me.txtCityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCityName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCityName.Location = New System.Drawing.Point(86, 113)
        Me.txtCityName.Name = "txtCityName"
        Me.txtCityName.Size = New System.Drawing.Size(185, 21)
        Me.txtCityName.TabIndex = 4
        '
        'pnlCity
        '
        Me.pnlCity.Controls.Add(Me.gbCity)
        Me.pnlCity.Controls.Add(Me.objFooter)
        Me.pnlCity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCity.Location = New System.Drawing.Point(0, 0)
        Me.pnlCity.Name = "pnlCity"
        Me.pnlCity.Size = New System.Drawing.Size(326, 215)
        Me.pnlCity.TabIndex = 1
        '
        'gbCity
        '
        Me.gbCity.BorderColor = System.Drawing.Color.Black
        Me.gbCity.Checked = False
        Me.gbCity.CollapseAllExceptThis = False
        Me.gbCity.CollapsedHoverImage = Nothing
        Me.gbCity.CollapsedNormalImage = Nothing
        Me.gbCity.CollapsedPressedImage = Nothing
        Me.gbCity.CollapseOnLoad = False
        Me.gbCity.Controls.Add(Me.objbtnAddState)
        Me.gbCity.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbCity.Controls.Add(Me.cboState)
        Me.gbCity.Controls.Add(Me.lblState)
        Me.gbCity.Controls.Add(Me.txtCityName)
        Me.gbCity.Controls.Add(Me.lblCityName)
        Me.gbCity.Controls.Add(Me.txtCityCode)
        Me.gbCity.Controls.Add(Me.lblCityCode)
        Me.gbCity.Controls.Add(Me.cboCountry)
        Me.gbCity.Controls.Add(Me.lblCountry)
        Me.gbCity.ExpandedHoverImage = Nothing
        Me.gbCity.ExpandedNormalImage = Nothing
        Me.gbCity.ExpandedPressedImage = Nothing
        Me.gbCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCity.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCity.HeaderHeight = 25
        Me.gbCity.HeightOnCollapse = 0
        Me.gbCity.LeftTextSpace = 0
        Me.gbCity.Location = New System.Drawing.Point(8, 9)
        Me.gbCity.Name = "gbCity"
        Me.gbCity.OpenHeight = 91
        Me.gbCity.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCity.ShowBorder = True
        Me.gbCity.ShowCheckBox = False
        Me.gbCity.ShowCollapseButton = False
        Me.gbCity.ShowDefaultBorderColor = True
        Me.gbCity.ShowDownButton = False
        Me.gbCity.ShowHeader = True
        Me.gbCity.Size = New System.Drawing.Size(309, 143)
        Me.gbCity.TabIndex = 12
        Me.gbCity.Temp = 0
        Me.gbCity.Text = "City"
        Me.gbCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddState
        '
        Me.objbtnAddState.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddState.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddState.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddState.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddState.BorderSelected = False
        Me.objbtnAddState.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddState.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Add
        Me.objbtnAddState.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddState.Location = New System.Drawing.Point(277, 59)
        Me.objbtnAddState.Name = "objbtnAddState"
        Me.objbtnAddState.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddState.TabIndex = 212
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Data.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(277, 113)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 5
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(86, 59)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(185, 21)
        Me.cboState.TabIndex = 2
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(8, 63)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(71, 14)
        Me.lblState.TabIndex = 209
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCityName
        '
        Me.lblCityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCityName.Location = New System.Drawing.Point(8, 117)
        Me.lblCityName.Name = "lblCityName"
        Me.lblCityName.Size = New System.Drawing.Size(71, 14)
        Me.lblCityName.TabIndex = 206
        Me.lblCityName.Text = "Name"
        Me.lblCityName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCityCode
        '
        Me.txtCityCode.Flags = 0
        Me.txtCityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCityCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCityCode.Location = New System.Drawing.Point(86, 86)
        Me.txtCityCode.Name = "txtCityCode"
        Me.txtCityCode.Size = New System.Drawing.Size(111, 21)
        Me.txtCityCode.TabIndex = 3
        '
        'lblCityCode
        '
        Me.lblCityCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCityCode.Location = New System.Drawing.Point(8, 90)
        Me.lblCityCode.Name = "lblCityCode"
        Me.lblCityCode.Size = New System.Drawing.Size(71, 14)
        Me.lblCityCode.TabIndex = 88
        Me.lblCityCode.Text = "Code"
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(86, 32)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(212, 21)
        Me.cboCountry.TabIndex = 1
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 36)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(71, 14)
        Me.lblCountry.TabIndex = 85
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 160)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(326, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(128, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(224, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCity_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 215)
        Me.Controls.Add(Me.pnlCity)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCity_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit City"
        Me.pnlCity.ResumeLayout(False)
        Me.gbCity.ResumeLayout(False)
        Me.gbCity.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCityName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlCity As System.Windows.Forms.Panel
    Friend WithEvents gbCity As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCityName As System.Windows.Forms.Label
    Friend WithEvents txtCityCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCityCode As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddState As eZee.Common.eZeeGradientButton
End Class

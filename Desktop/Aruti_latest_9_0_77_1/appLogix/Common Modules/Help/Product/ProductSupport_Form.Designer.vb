<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProductSupport
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProductSupport))
        Me.gbServiceAndSupport = New System.Windows.Forms.GroupBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblMsg1 = New System.Windows.Forms.Label
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.pnlBoxImage = New System.Windows.Forms.Panel
        Me.pnlResellerInfo = New System.Windows.Forms.Panel
        Me.objlblSupportEmail = New System.Windows.Forms.Label
        Me.objlblSupportPhone = New System.Windows.Forms.Label
        Me.objlblSalesEmail = New System.Windows.Forms.Label
        Me.objlblSalesPhone = New System.Windows.Forms.Label
        Me.lblEmailTitle = New System.Windows.Forms.Label
        Me.lblPhoneTitle = New System.Windows.Forms.Label
        Me.lblSupportInfo = New System.Windows.Forms.Label
        Me.lblSalesInfo = New System.Windows.Forms.Label
        Me.objlblOtherInfo = New System.Windows.Forms.Label
        Me.objlblCompany = New System.Windows.Forms.Label
        Me.lblLocalSalesANDSupport = New System.Windows.Forms.Label
        Me.objlblOEMOtherInfo = New System.Windows.Forms.Label
        Me.objlblOEMSupportEmail = New System.Windows.Forms.Label
        Me.objlblOEMSupportPhone = New System.Windows.Forms.Label
        Me.lblOEMSupportInfo = New System.Windows.Forms.Label
        Me.objlblOEMSalesEmail = New System.Windows.Forms.Label
        Me.lblOEMSalesEmailTitle = New System.Windows.Forms.Label
        Me.objlblOEmSalesPhone = New System.Windows.Forms.Label
        Me.lblOEMSalesPhoneTitle = New System.Windows.Forms.Label
        Me.lblOEMSalesInfo = New System.Windows.Forms.Label
        Me.objlblOEMCompany = New System.Windows.Forms.Label
        Me.lblMainBranch = New System.Windows.Forms.Label
        Me.gbServiceAndSupport.SuspendLayout()
        Me.pnl.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.pnlResellerInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbServiceAndSupport
        '
        Me.gbServiceAndSupport.Controls.Add(Me.Label16)
        Me.gbServiceAndSupport.Controls.Add(Me.Label15)
        Me.gbServiceAndSupport.Controls.Add(Me.Label14)
        Me.gbServiceAndSupport.Controls.Add(Me.Label13)
        Me.gbServiceAndSupport.Controls.Add(Me.Label12)
        Me.gbServiceAndSupport.Controls.Add(Me.LinkLabel2)
        Me.gbServiceAndSupport.Controls.Add(Me.Label11)
        Me.gbServiceAndSupport.Controls.Add(Me.Label10)
        Me.gbServiceAndSupport.Controls.Add(Me.Label9)
        Me.gbServiceAndSupport.Controls.Add(Me.Label8)
        Me.gbServiceAndSupport.Controls.Add(Me.Label7)
        Me.gbServiceAndSupport.Controls.Add(Me.LinkLabel1)
        Me.gbServiceAndSupport.Controls.Add(Me.Label6)
        Me.gbServiceAndSupport.Controls.Add(Me.Label5)
        Me.gbServiceAndSupport.Controls.Add(Me.Label4)
        Me.gbServiceAndSupport.Controls.Add(Me.Label3)
        Me.gbServiceAndSupport.Controls.Add(Me.Label2)
        Me.gbServiceAndSupport.Controls.Add(Me.Label1)
        Me.gbServiceAndSupport.Controls.Add(Me.lblMsg1)
        Me.gbServiceAndSupport.Location = New System.Drawing.Point(8, 351)
        Me.gbServiceAndSupport.Name = "gbServiceAndSupport"
        Me.gbServiceAndSupport.Size = New System.Drawing.Size(424, 365)
        Me.gbServiceAndSupport.TabIndex = 0
        Me.gbServiceAndSupport.TabStop = False
        Me.gbServiceAndSupport.Text = "Service and Support"
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.SaddleBrown
        Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label16.Location = New System.Drawing.Point(21, 311)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(9, 9)
        Me.Label16.TabIndex = 18
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.SaddleBrown
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Location = New System.Drawing.Point(21, 254)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(9, 9)
        Me.Label15.TabIndex = 17
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.SaddleBrown
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Location = New System.Drawing.Point(21, 165)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(9, 9)
        Me.Label14.TabIndex = 16
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.SaddleBrown
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Location = New System.Drawing.Point(21, 119)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(9, 9)
        Me.Label13.TabIndex = 15
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.SaddleBrown
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Location = New System.Drawing.Point(21, 75)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(9, 9)
        Me.Label12.TabIndex = 14
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Location = New System.Drawing.Point(42, 326)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(150, 13)
        Me.LinkLabel2.TabIndex = 13
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "support@eZeeFrontDesk.com"
        '
        'Label11
        '
        Me.Label11.ForeColor = System.Drawing.Color.Blue
        Me.Label11.Location = New System.Drawing.Point(166, 279)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(252, 19)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Call: +91 98251 26513"
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(42, 279)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(131, 19)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "For immediate assistance,"
        '
        'Label9
        '
        Me.Label9.ForeColor = System.Drawing.Color.Blue
        Me.Label9.Location = New System.Drawing.Point(142, 266)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(252, 19)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Fax Number: +91 98251 26513"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(42, 207)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 19)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Note:"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(42, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(305, 19)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Technical Support and FAQ online."
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(148, 162)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(204, 13)
        Me.LinkLabel1.TabIndex = 7
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "http://www.eZeeFrontDesk.com/support"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(42, 309)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(366, 20)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "You can also email your queries at"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(42, 252)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(366, 29)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Email your questions, comments, technical issues. We will respond to you as soon " & _
            "as possible."
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(82, 207)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(322, 45)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "You must have an Internet connection and a Web browser to access online help and " & _
            "support."
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(42, 162)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Point your browser to"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(42, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(366, 45)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Customer Service helps you with orders, upgrades, rebates and other non-technical" & _
            " issues."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(42, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(366, 45)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Technical Support helps you with installing, configuring, or troubleshooting your" & _
            " eZee product. "
        '
        'lblMsg1
        '
        Me.lblMsg1.Location = New System.Drawing.Point(16, 29)
        Me.lblMsg1.Name = "lblMsg1"
        Me.lblMsg1.Size = New System.Drawing.Size(392, 34)
        Me.lblMsg1.TabIndex = 0
        Me.lblMsg1.Text = "Technical and non-technical support for eZee products is available online through" & _
            " our Web site and through phone lines."
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(530, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "&OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'pnl
        '
        Me.pnl.BackColor = System.Drawing.Color.Transparent
        Me.pnl.Controls.Add(Me.objefFormFooter)
        Me.pnl.Controls.Add(Me.pnlBoxImage)
        Me.pnl.Controls.Add(Me.pnlResellerInfo)
        Me.pnl.Controls.Add(Me.objlblOEMOtherInfo)
        Me.pnl.Controls.Add(Me.objlblOEMSupportEmail)
        Me.pnl.Controls.Add(Me.objlblOEMSupportPhone)
        Me.pnl.Controls.Add(Me.lblOEMSupportInfo)
        Me.pnl.Controls.Add(Me.objlblOEMSalesEmail)
        Me.pnl.Controls.Add(Me.lblOEMSalesEmailTitle)
        Me.pnl.Controls.Add(Me.objlblOEmSalesPhone)
        Me.pnl.Controls.Add(Me.lblOEMSalesPhoneTitle)
        Me.pnl.Controls.Add(Me.lblOEMSalesInfo)
        Me.pnl.Controls.Add(Me.objlblOEMCompany)
        Me.pnl.Controls.Add(Me.lblMainBranch)
        Me.pnl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnl.Location = New System.Drawing.Point(0, 0)
        Me.pnl.Name = "pnl"
        Me.pnl.Size = New System.Drawing.Size(632, 369)
        Me.pnl.TabIndex = 6
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 313)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(632, 56)
        Me.objefFormFooter.TabIndex = 49
        '
        'pnlBoxImage
        '
        Me.pnlBoxImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlBoxImage.Location = New System.Drawing.Point(3, 8)
        Me.pnlBoxImage.Name = "pnlBoxImage"
        Me.pnlBoxImage.Size = New System.Drawing.Size(210, 303)
        Me.pnlBoxImage.TabIndex = 48
        '
        'pnlResellerInfo
        '
        Me.pnlResellerInfo.Controls.Add(Me.objlblSupportEmail)
        Me.pnlResellerInfo.Controls.Add(Me.objlblSupportPhone)
        Me.pnlResellerInfo.Controls.Add(Me.objlblSalesEmail)
        Me.pnlResellerInfo.Controls.Add(Me.objlblSalesPhone)
        Me.pnlResellerInfo.Controls.Add(Me.lblEmailTitle)
        Me.pnlResellerInfo.Controls.Add(Me.lblPhoneTitle)
        Me.pnlResellerInfo.Controls.Add(Me.lblSupportInfo)
        Me.pnlResellerInfo.Controls.Add(Me.lblSalesInfo)
        Me.pnlResellerInfo.Controls.Add(Me.objlblOtherInfo)
        Me.pnlResellerInfo.Controls.Add(Me.objlblCompany)
        Me.pnlResellerInfo.Controls.Add(Me.lblLocalSalesANDSupport)
        Me.pnlResellerInfo.Location = New System.Drawing.Point(219, 155)
        Me.pnlResellerInfo.Name = "pnlResellerInfo"
        Me.pnlResellerInfo.Size = New System.Drawing.Size(410, 157)
        Me.pnlResellerInfo.TabIndex = 47
        Me.pnlResellerInfo.Visible = False
        '
        'objlblSupportEmail
        '
        Me.objlblSupportEmail.BackColor = System.Drawing.Color.Transparent
        Me.objlblSupportEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSupportEmail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblSupportEmail.Location = New System.Drawing.Point(224, 132)
        Me.objlblSupportEmail.Name = "objlblSupportEmail"
        Me.objlblSupportEmail.Size = New System.Drawing.Size(180, 27)
        Me.objlblSupportEmail.TabIndex = 57
        Me.objlblSupportEmail.Text = "support@ezeefrontdesk.com"
        '
        'objlblSupportPhone
        '
        Me.objlblSupportPhone.BackColor = System.Drawing.Color.Transparent
        Me.objlblSupportPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSupportPhone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblSupportPhone.Location = New System.Drawing.Point(224, 115)
        Me.objlblSupportPhone.Name = "objlblSupportPhone"
        Me.objlblSupportPhone.Size = New System.Drawing.Size(180, 14)
        Me.objlblSupportPhone.TabIndex = 56
        Me.objlblSupportPhone.Text = "+91 98251 26513"
        '
        'objlblSalesEmail
        '
        Me.objlblSalesEmail.BackColor = System.Drawing.Color.Transparent
        Me.objlblSalesEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSalesEmail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblSalesEmail.Location = New System.Drawing.Point(57, 132)
        Me.objlblSalesEmail.Name = "objlblSalesEmail"
        Me.objlblSalesEmail.Size = New System.Drawing.Size(161, 27)
        Me.objlblSalesEmail.TabIndex = 55
        Me.objlblSalesEmail.Text = "sales@ezeefrontdesk.com"
        '
        'objlblSalesPhone
        '
        Me.objlblSalesPhone.BackColor = System.Drawing.Color.Transparent
        Me.objlblSalesPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSalesPhone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblSalesPhone.Location = New System.Drawing.Point(57, 115)
        Me.objlblSalesPhone.Name = "objlblSalesPhone"
        Me.objlblSalesPhone.Size = New System.Drawing.Size(161, 14)
        Me.objlblSalesPhone.TabIndex = 54
        Me.objlblSalesPhone.Text = "+91 98251 26513"
        '
        'lblEmailTitle
        '
        Me.lblEmailTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblEmailTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmailTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblEmailTitle.Location = New System.Drawing.Point(3, 132)
        Me.lblEmailTitle.Name = "lblEmailTitle"
        Me.lblEmailTitle.Size = New System.Drawing.Size(53, 14)
        Me.lblEmailTitle.TabIndex = 53
        Me.lblEmailTitle.Text = "Email:"
        '
        'lblPhoneTitle
        '
        Me.lblPhoneTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblPhoneTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhoneTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblPhoneTitle.Location = New System.Drawing.Point(3, 115)
        Me.lblPhoneTitle.Name = "lblPhoneTitle"
        Me.lblPhoneTitle.Size = New System.Drawing.Size(53, 14)
        Me.lblPhoneTitle.TabIndex = 52
        Me.lblPhoneTitle.Text = "Phone:"
        '
        'lblSupportInfo
        '
        Me.lblSupportInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblSupportInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupportInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblSupportInfo.Location = New System.Drawing.Point(224, 97)
        Me.lblSupportInfo.Name = "lblSupportInfo"
        Me.lblSupportInfo.Size = New System.Drawing.Size(180, 18)
        Me.lblSupportInfo.TabIndex = 51
        Me.lblSupportInfo.Text = "Support Information :"
        '
        'lblSalesInfo
        '
        Me.lblSalesInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblSalesInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalesInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblSalesInfo.Location = New System.Drawing.Point(20, 97)
        Me.lblSalesInfo.Name = "lblSalesInfo"
        Me.lblSalesInfo.Size = New System.Drawing.Size(159, 18)
        Me.lblSalesInfo.TabIndex = 50
        Me.lblSalesInfo.Text = "Sales Information :"
        '
        'objlblOtherInfo
        '
        Me.objlblOtherInfo.BackColor = System.Drawing.Color.Transparent
        Me.objlblOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOtherInfo.Location = New System.Drawing.Point(21, 44)
        Me.objlblOtherInfo.Name = "objlblOtherInfo"
        Me.objlblOtherInfo.Size = New System.Drawing.Size(380, 53)
        Me.objlblOtherInfo.TabIndex = 49
        Me.objlblOtherInfo.Text = "Vipul Kapoor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "D- 113 International Trade Center" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Majuragate Surat 395002 Gujarat " & _
            "India." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "www.ezeefrontdesk.com"
        '
        'objlblCompany
        '
        Me.objlblCompany.BackColor = System.Drawing.Color.Transparent
        Me.objlblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCompany.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblCompany.Location = New System.Drawing.Point(21, 29)
        Me.objlblCompany.Name = "objlblCompany"
        Me.objlblCompany.Size = New System.Drawing.Size(383, 15)
        Me.objlblCompany.TabIndex = 48
        Me.objlblCompany.Text = "eZee Technosys Pvt. Ltd."
        '
        'lblLocalSalesANDSupport
        '
        Me.lblLocalSalesANDSupport.BackColor = System.Drawing.Color.Transparent
        Me.lblLocalSalesANDSupport.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocalSalesANDSupport.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblLocalSalesANDSupport.Location = New System.Drawing.Point(3, 7)
        Me.lblLocalSalesANDSupport.Name = "lblLocalSalesANDSupport"
        Me.lblLocalSalesANDSupport.Size = New System.Drawing.Size(397, 20)
        Me.lblLocalSalesANDSupport.TabIndex = 47
        Me.lblLocalSalesANDSupport.Text = "Local Sales && Support:"
        '
        'objlblOEMOtherInfo
        '
        Me.objlblOEMOtherInfo.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEMOtherInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEMOtherInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEMOtherInfo.Location = New System.Drawing.Point(240, 42)
        Me.objlblOEMOtherInfo.Name = "objlblOEMOtherInfo"
        Me.objlblOEMOtherInfo.Size = New System.Drawing.Size(375, 53)
        Me.objlblOEMOtherInfo.TabIndex = 36
        Me.objlblOEMOtherInfo.Text = "Vipul Kapoor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "D- 113 International Trade Center" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Majuragate Surat 395002 Gujarat " & _
            "India." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "www.ezeefrontdesk.com"
        '
        'objlblOEMSupportEmail
        '
        Me.objlblOEMSupportEmail.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEMSupportEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEMSupportEmail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEMSupportEmail.Location = New System.Drawing.Point(443, 134)
        Me.objlblOEMSupportEmail.Name = "objlblOEMSupportEmail"
        Me.objlblOEMSupportEmail.Size = New System.Drawing.Size(180, 27)
        Me.objlblOEMSupportEmail.TabIndex = 35
        Me.objlblOEMSupportEmail.Text = "support@ezeefrontdesk.com"
        '
        'objlblOEMSupportPhone
        '
        Me.objlblOEMSupportPhone.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEMSupportPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEMSupportPhone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEMSupportPhone.Location = New System.Drawing.Point(443, 116)
        Me.objlblOEMSupportPhone.Name = "objlblOEMSupportPhone"
        Me.objlblOEMSupportPhone.Size = New System.Drawing.Size(180, 14)
        Me.objlblOEMSupportPhone.TabIndex = 33
        Me.objlblOEMSupportPhone.Text = "+91 98251 26513"
        '
        'lblOEMSupportInfo
        '
        Me.lblOEMSupportInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblOEMSupportInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOEMSupportInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOEMSupportInfo.Location = New System.Drawing.Point(443, 98)
        Me.lblOEMSupportInfo.Name = "lblOEMSupportInfo"
        Me.lblOEMSupportInfo.Size = New System.Drawing.Size(180, 17)
        Me.lblOEMSupportInfo.TabIndex = 31
        Me.lblOEMSupportInfo.Text = "Support Information :"
        '
        'objlblOEMSalesEmail
        '
        Me.objlblOEMSalesEmail.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEMSalesEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEMSalesEmail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEMSalesEmail.Location = New System.Drawing.Point(276, 134)
        Me.objlblOEMSalesEmail.Name = "objlblOEMSalesEmail"
        Me.objlblOEMSalesEmail.Size = New System.Drawing.Size(161, 27)
        Me.objlblOEMSalesEmail.TabIndex = 30
        Me.objlblOEMSalesEmail.Text = "sales@ezeefrontdesk.com"
        '
        'lblOEMSalesEmailTitle
        '
        Me.lblOEMSalesEmailTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblOEMSalesEmailTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOEMSalesEmailTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOEMSalesEmailTitle.Location = New System.Drawing.Point(219, 134)
        Me.lblOEMSalesEmailTitle.Name = "lblOEMSalesEmailTitle"
        Me.lblOEMSalesEmailTitle.Size = New System.Drawing.Size(56, 14)
        Me.lblOEMSalesEmailTitle.TabIndex = 29
        Me.lblOEMSalesEmailTitle.Text = "Email:"
        '
        'objlblOEmSalesPhone
        '
        Me.objlblOEmSalesPhone.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEmSalesPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEmSalesPhone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEmSalesPhone.Location = New System.Drawing.Point(276, 116)
        Me.objlblOEmSalesPhone.Name = "objlblOEmSalesPhone"
        Me.objlblOEmSalesPhone.Size = New System.Drawing.Size(161, 14)
        Me.objlblOEmSalesPhone.TabIndex = 28
        Me.objlblOEmSalesPhone.Text = "+91 98251 26513"
        '
        'lblOEMSalesPhoneTitle
        '
        Me.lblOEMSalesPhoneTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblOEMSalesPhoneTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOEMSalesPhoneTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOEMSalesPhoneTitle.Location = New System.Drawing.Point(219, 116)
        Me.lblOEMSalesPhoneTitle.Name = "lblOEMSalesPhoneTitle"
        Me.lblOEMSalesPhoneTitle.Size = New System.Drawing.Size(56, 14)
        Me.lblOEMSalesPhoneTitle.TabIndex = 27
        Me.lblOEMSalesPhoneTitle.Text = "Phone:"
        '
        'lblOEMSalesInfo
        '
        Me.lblOEMSalesInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblOEMSalesInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOEMSalesInfo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblOEMSalesInfo.Location = New System.Drawing.Point(239, 98)
        Me.lblOEMSalesInfo.Name = "lblOEMSalesInfo"
        Me.lblOEMSalesInfo.Size = New System.Drawing.Size(173, 17)
        Me.lblOEMSalesInfo.TabIndex = 25
        Me.lblOEMSalesInfo.Text = "Sales Information :"
        '
        'objlblOEMCompany
        '
        Me.objlblOEMCompany.BackColor = System.Drawing.Color.Transparent
        Me.objlblOEMCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOEMCompany.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objlblOEMCompany.Location = New System.Drawing.Point(240, 27)
        Me.objlblOEMCompany.Name = "objlblOEMCompany"
        Me.objlblOEMCompany.Size = New System.Drawing.Size(375, 15)
        Me.objlblOEMCompany.TabIndex = 17
        Me.objlblOEMCompany.Text = "eZee Technosys Pvt. Ltd."
        '
        'lblMainBranch
        '
        Me.lblMainBranch.BackColor = System.Drawing.Color.Transparent
        Me.lblMainBranch.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMainBranch.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblMainBranch.Location = New System.Drawing.Point(219, 9)
        Me.lblMainBranch.Name = "lblMainBranch"
        Me.lblMainBranch.Size = New System.Drawing.Size(197, 18)
        Me.lblMainBranch.TabIndex = 13
        Me.lblMainBranch.Text = "Head Office :"
        '
        'frmProductSupport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 369)
        Me.Controls.Add(Me.pnl)
        Me.Controls.Add(Me.gbServiceAndSupport)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProductSupport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Product Support"
        Me.gbServiceAndSupport.ResumeLayout(False)
        Me.gbServiceAndSupport.PerformLayout()
        Me.pnl.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.pnlResellerInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbServiceAndSupport As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblMsg1 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents pnl As System.Windows.Forms.Panel
    Friend WithEvents lblMainBranch As System.Windows.Forms.Label
    Friend WithEvents objlblOEMCompany As System.Windows.Forms.Label
    Friend WithEvents objlblOEMSalesEmail As System.Windows.Forms.Label
    Friend WithEvents lblOEMSalesEmailTitle As System.Windows.Forms.Label
    Friend WithEvents objlblOEmSalesPhone As System.Windows.Forms.Label
    Friend WithEvents lblOEMSalesPhoneTitle As System.Windows.Forms.Label
    Friend WithEvents lblOEMSalesInfo As System.Windows.Forms.Label
    Friend WithEvents objlblOEMSupportEmail As System.Windows.Forms.Label
    Friend WithEvents objlblOEMSupportPhone As System.Windows.Forms.Label
    Friend WithEvents lblOEMSupportInfo As System.Windows.Forms.Label
    Friend WithEvents objlblOEMOtherInfo As System.Windows.Forms.Label
    Friend WithEvents pnlResellerInfo As System.Windows.Forms.Panel
    Friend WithEvents objlblSupportEmail As System.Windows.Forms.Label
    Friend WithEvents objlblSupportPhone As System.Windows.Forms.Label
    Friend WithEvents objlblSalesEmail As System.Windows.Forms.Label
    Friend WithEvents objlblSalesPhone As System.Windows.Forms.Label
    Friend WithEvents lblEmailTitle As System.Windows.Forms.Label
    Friend WithEvents lblPhoneTitle As System.Windows.Forms.Label
    Friend WithEvents lblSupportInfo As System.Windows.Forms.Label
    Friend WithEvents lblSalesInfo As System.Windows.Forms.Label
    Friend WithEvents objlblOtherInfo As System.Windows.Forms.Label
    Friend WithEvents objlblCompany As System.Windows.Forms.Label
    Friend WithEvents lblLocalSalesANDSupport As System.Windows.Forms.Label
    Friend WithEvents pnlBoxImage As System.Windows.Forms.Panel
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
End Class

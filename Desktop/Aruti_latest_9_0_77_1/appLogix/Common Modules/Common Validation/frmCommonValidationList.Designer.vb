﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommonValidationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommonValidationList))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbValidationInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlYesNo = New eZee.Common.eZeeFooter
        Me.btnExport1 = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnYes = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNo = New eZee.Common.eZeeLightButton(Me.components)
        Me.objpnlOkCancel = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.gbValidationInformation.SuspendLayout()
        Me.objpnlYesNo.SuspendLayout()
        Me.objpnlOkCancel.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbValidationInformation
        '
        Me.gbValidationInformation.BorderColor = System.Drawing.Color.Black
        Me.gbValidationInformation.Checked = False
        Me.gbValidationInformation.CollapseAllExceptThis = False
        Me.gbValidationInformation.CollapsedHoverImage = Nothing
        Me.gbValidationInformation.CollapsedNormalImage = Nothing
        Me.gbValidationInformation.CollapsedPressedImage = Nothing
        Me.gbValidationInformation.CollapseOnLoad = False
        Me.gbValidationInformation.Controls.Add(Me.objpnlYesNo)
        Me.gbValidationInformation.Controls.Add(Me.objpnlOkCancel)
        Me.gbValidationInformation.Controls.Add(Me.Panel1)
        Me.gbValidationInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbValidationInformation.ExpandedHoverImage = Nothing
        Me.gbValidationInformation.ExpandedNormalImage = Nothing
        Me.gbValidationInformation.ExpandedPressedImage = Nothing
        Me.gbValidationInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbValidationInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbValidationInformation.HeaderHeight = 50
        Me.gbValidationInformation.HeaderMessage = ""
        Me.gbValidationInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbValidationInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbValidationInformation.HeightOnCollapse = 0
        Me.gbValidationInformation.LeftTextSpace = 0
        Me.gbValidationInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbValidationInformation.Name = "gbValidationInformation"
        Me.gbValidationInformation.OpenHeight = 300
        Me.gbValidationInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbValidationInformation.ShowBorder = True
        Me.gbValidationInformation.ShowCheckBox = False
        Me.gbValidationInformation.ShowCollapseButton = False
        Me.gbValidationInformation.ShowDefaultBorderColor = True
        Me.gbValidationInformation.ShowDownButton = False
        Me.gbValidationInformation.ShowHeader = True
        Me.gbValidationInformation.Size = New System.Drawing.Size(689, 420)
        Me.gbValidationInformation.TabIndex = 1
        Me.gbValidationInformation.Temp = 0
        Me.gbValidationInformation.Text = "  Warning!"
        Me.gbValidationInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlYesNo
        '
        Me.objpnlYesNo.BorderColor = System.Drawing.Color.Silver
        Me.objpnlYesNo.Controls.Add(Me.btnExport1)
        Me.objpnlYesNo.Controls.Add(Me.btnYes)
        Me.objpnlYesNo.Controls.Add(Me.btnNo)
        Me.objpnlYesNo.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objpnlYesNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlYesNo.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objpnlYesNo.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objpnlYesNo.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objpnlYesNo.Location = New System.Drawing.Point(0, 310)
        Me.objpnlYesNo.Name = "objpnlYesNo"
        Me.objpnlYesNo.Size = New System.Drawing.Size(689, 55)
        Me.objpnlYesNo.TabIndex = 217
        Me.objpnlYesNo.Visible = False
        '
        'btnExport1
        '
        Me.btnExport1.BackColor = System.Drawing.Color.White
        Me.btnExport1.BackgroundImage = CType(resources.GetObject("btnExport1.BackgroundImage"), System.Drawing.Image)
        Me.btnExport1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport1.BorderColor = System.Drawing.Color.Empty
        Me.btnExport1.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport1.FlatAppearance.BorderSize = 0
        Me.btnExport1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport1.ForeColor = System.Drawing.Color.Black
        Me.btnExport1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport1.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport1.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport1.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport1.Location = New System.Drawing.Point(12, 13)
        Me.btnExport1.Name = "btnExport1"
        Me.btnExport1.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport1.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport1.Size = New System.Drawing.Size(97, 30)
        Me.btnExport1.TabIndex = 3
        Me.btnExport1.Text = "&Export"
        Me.btnExport1.UseVisualStyleBackColor = True
        '
        'btnYes
        '
        Me.btnYes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnYes.BackColor = System.Drawing.Color.White
        Me.btnYes.BackgroundImage = CType(resources.GetObject("btnYes.BackgroundImage"), System.Drawing.Image)
        Me.btnYes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnYes.BorderColor = System.Drawing.Color.Empty
        Me.btnYes.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnYes.FlatAppearance.BorderSize = 0
        Me.btnYes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnYes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYes.ForeColor = System.Drawing.Color.Black
        Me.btnYes.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnYes.GradientForeColor = System.Drawing.Color.Black
        Me.btnYes.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnYes.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnYes.Location = New System.Drawing.Point(477, 13)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnYes.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnYes.Size = New System.Drawing.Size(97, 30)
        Me.btnYes.TabIndex = 0
        Me.btnYes.Text = "&Yes"
        Me.btnYes.UseVisualStyleBackColor = True
        '
        'btnNo
        '
        Me.btnNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNo.BackColor = System.Drawing.Color.White
        Me.btnNo.BackgroundImage = CType(resources.GetObject("btnNo.BackgroundImage"), System.Drawing.Image)
        Me.btnNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNo.BorderColor = System.Drawing.Color.Empty
        Me.btnNo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNo.FlatAppearance.BorderSize = 0
        Me.btnNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNo.ForeColor = System.Drawing.Color.Black
        Me.btnNo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNo.GradientForeColor = System.Drawing.Color.Black
        Me.btnNo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNo.Location = New System.Drawing.Point(580, 13)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNo.Size = New System.Drawing.Size(97, 30)
        Me.btnNo.TabIndex = 1
        Me.btnNo.Text = "&No"
        Me.btnNo.UseVisualStyleBackColor = True
        '
        'objpnlOkCancel
        '
        Me.objpnlOkCancel.BorderColor = System.Drawing.Color.Silver
        Me.objpnlOkCancel.Controls.Add(Me.btnExport)
        Me.objpnlOkCancel.Controls.Add(Me.btnOk)
        Me.objpnlOkCancel.Controls.Add(Me.btnClose)
        Me.objpnlOkCancel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objpnlOkCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlOkCancel.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objpnlOkCancel.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objpnlOkCancel.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objpnlOkCancel.Location = New System.Drawing.Point(0, 365)
        Me.objpnlOkCancel.Name = "objpnlOkCancel"
        Me.objpnlOkCancel.Size = New System.Drawing.Size(689, 55)
        Me.objpnlOkCancel.TabIndex = 215
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(12, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(477, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(580, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvList)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 55)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(665, 304)
        Me.Panel1.TabIndex = 219
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.dgvList.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(0, 0)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.Size = New System.Drawing.Size(665, 304)
        Me.dgvList.TabIndex = 216
        '
        'frmCommonValidationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(689, 420)
        Me.Controls.Add(Me.gbValidationInformation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommonValidationList"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Warning!"
        Me.gbValidationInformation.ResumeLayout(False)
        Me.objpnlYesNo.ResumeLayout(False)
        Me.objpnlOkCancel.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbValidationInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objpnlOkCancel As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents objpnlYesNo As eZee.Common.eZeeFooter
    Friend WithEvents btnYes As eZee.Common.eZeeLightButton
    Friend WithEvents btnNo As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport1 As eZee.Common.eZeeLightButton
End Class

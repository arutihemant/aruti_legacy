﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmUserConfigOptionLog

#Region " Private Variables "

    Private mstrModuleName As String = "frmUserConfigOptionLog"

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objMasterData As New clsMasterData
            dsList = objMasterData.GetConfigurationSettingModule(True, "AtConfig")
            With cboOperationBy
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsList.Tables(0)
            End With
         
            dsList = Nothing
            Dim objUser As New clsUserAddEdit
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsList = objUser.getComboList("List", True)

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUser.getNewComboList("List", , True)
            dsList = objUser.getNewComboList("List", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboUser
                .DisplayMember = "name"
                .ValueMember = "userunkid"
                .DataSource = dsList.Tables(0)
            End With

            dsList = Nothing
            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("Company", True)

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("companyunkid") = 0
            drRow("code") = Language.getMessage(mstrModuleName, 1, "Select")
            drRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Company")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim strSearching As String = ""

            If CInt(cboOperationBy.SelectedValue) > 0 Then
                strSearching = " AND atcfconfiguration.optiongroupid = " & CInt(cboOperationBy.SelectedValue) & " "
            End If

            If CInt(cboUser.SelectedValue) > 0 Then
                strSearching &= " AND atcfconfiguration.audituserunkid = " & CInt(cboUser.SelectedValue) & " "
            End If

            If CInt(cboCompany.SelectedValue) > 0 Then
                strSearching &= " AND atcfconfiguration.companyunkid = " & CInt(cboCompany.SelectedValue) & " "
            End If

            If dtpFromDate.Checked Then
                strSearching &= " AND Convert(CHAR(8),atcfconfiguration.auditdatetime,112) >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' "
            End If

            If dtpTodate.Checked Then
                strSearching &= " AND Convert(CHAR(8),atcfconfiguration.auditdatetime,112) <= '" & eZeeDate.convertDate(dtpTodate.Value.Date) & "' "
            End If

            If txtIPAddress.Text.Trim.Length > 0 Then
                strSearching &= " AND atcfconfiguration.ip = '" & txtIPAddress.Text.Trim & "'"
            End If

            If txtMachine.Text.Trim.Length > 0 Then
                strSearching &= " AND atcfconfiguration.machine_name = '" & txtMachine.Text.Trim & "'"
            End If


            Dim objAtConfigOption As New clsAtconfigoption
            Dim dsList As DataSet = objAtConfigOption.GetList("ATConfig", strSearching)
            dgvConfigOptionData.DataSource = dsList.Tables(0)
            GridSpecificColumnsVisible(False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub GridSpecificColumnsVisible(ByVal blnEnable As Boolean)
        Try
            If dgvConfigOptionData.Columns.Count > 0 Then
                dgvConfigOptionData.Columns("atoptionunkid").Visible = blnEnable
                dgvConfigOptionData.Columns("optiongroupid").Visible = blnEnable
                dgvConfigOptionData.Columns("configunkid").Visible = blnEnable
                dgvConfigOptionData.Columns("companyunkid").Visible = blnEnable
                dgvConfigOptionData.Columns("code").Visible = blnEnable
                dgvConfigOptionData.Columns("audituserunkid").Visible = blnEnable

                For i As Integer = 0 To dgvConfigOptionData.Columns.Count - 1
                    dgvConfigOptionData.Columns(i).SortMode = DataGridViewColumnSortMode.NotSortable
                Next

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GridSpecificColumnsVisible", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmUserConfigOptionLog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            Call FillCombo()
            Call btnReset_Click(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserConfigOptionLog_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsAtconfigoption.SetMessages()
            objfrm._Other_ModuleNames = "clsAtconfigoption"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Company is compulsory information.Please Select Company."), enMsgBoxStyle.Information)
                cboCompany.Select()
                Exit Sub
            End If
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboOperationBy.SelectedIndex = 0
            cboUser.SelectedIndex = 0
            cboCompany.SelectedIndex = 0
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpFromDate.Checked = False
            dtpTodate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTodate.Checked = False
            txtIPAddress.Text = ""
            txtMachine.Text = ""
            txtDescription.Text = ""
            dgvConfigOptionData.Rows.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            Dim dtReader As DataTable = CType(dgvConfigOptionData.DataSource, DataTable)

            If dtReader IsNot Nothing Then
                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='50%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 500, "Configuration Option Log Events") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 502, "Operation Mode : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboOperationBy.SelectedValue) > 0, cboOperationBy.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 503, "User : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboUser.SelectedValue) > 0, cboUser.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 504, "Company : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboCompany.SelectedValue) > 0, cboCompany.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 501, "From Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpFromDate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpFromDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 502, "To Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpTodate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpTodate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 507, "IP Address :") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & txtIPAddress.Text & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 509, "Machine Name :") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & txtMachine.Text & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count - 4 & "'  BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 505, "Detail List") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For i As Integer = 0 To dtReader.Columns.Count - 1

                    If dtReader.Columns(i).ColumnName = "atoptionunkid" Or dtReader.Columns(i).ColumnName = "optiongroupid" _
                       Or dtReader.Columns(i).ColumnName = "configunkid" Or dtReader.Columns(i).ColumnName = "companyunkid" _
                       Or dtReader.Columns(i).ColumnName = "code" Or dtReader.Columns(i).ColumnName = "audituserunkid" Then Continue For

                    If dtReader.Columns(i).ColumnName = "Description" Then
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN = '3'><FONT SIZE=2><B>" & dtReader.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
                    Else
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtReader.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
                    End If

                Next
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For j As Integer = 0 To dtReader.Rows.Count - 1
                    For i As Integer = 0 To dtReader.Columns.Count - 1

                        If dtReader.Columns(i).ColumnName = "atoptionunkid" Or dtReader.Columns(i).ColumnName = "optiongroupid" _
                        Or dtReader.Columns(i).ColumnName = "configunkid" Or dtReader.Columns(i).ColumnName = "companyunkid" _
                          Or dtReader.Columns(i).ColumnName = "code" Or dtReader.Columns(i).ColumnName = "audituserunkid" Then Continue For

                        If dtReader.Columns(i).ColumnName = "Description" Then
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' VALIGN = 'TOP' COLSPAN = '3'><FONT SIZE=2>" & dtReader.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                        Else
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & dtReader.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                        End If

                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim svDialog As New SaveFileDialog
                svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
                If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim strWriter As New IO.StreamWriter(fsFile)
                    With strWriter
                        .BaseStream.Seek(0, IO.SeekOrigin.End)
                        .WriteLine(strBuilder)
                        .Close()
                    End With
                    Diagnostics.Process.Start(svDialog.FileName)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboUser.ValueMember
                .DisplayMember = cboUser.DisplayMember
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgvConfigOptionData_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvConfigOptionData.SelectionChanged
        Try
            txtDescription.Text = ""
            If dgvConfigOptionData.SelectedRows.Count > 0 Then
                txtDescription.Text = dgvConfigOptionData.SelectedRows(0).Cells("Description").Value.ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvConfigOptionData_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbDetailList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDetailList.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbDetailList.Text = Language._Object.getCaption(Me.gbDetailList.Name, Me.gbDetailList.Text)
			Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
			Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.Name, Me.lblViewBy.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblMachine.Text = Language._Object.getCaption(Me.lblMachine.Name, Me.lblMachine.Text)
			Me.lblIPAddress.Text = Language._Object.getCaption(Me.lblIPAddress.Name, Me.lblIPAddress.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.LblDescription.Text = Language._Object.getCaption(Me.LblDescription.Name, Me.LblDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Company is compulsory information.Please Select Company.")
			Language.setMessage(mstrModuleName, 500, "Configuration Option Log Events")
			Language.setMessage(mstrModuleName, 501, "From Date :")
			Language.setMessage(mstrModuleName, 502, "Operation Mode :")
			Language.setMessage(mstrModuleName, 503, "User :")
			Language.setMessage(mstrModuleName, 504, "Company :")
			Language.setMessage(mstrModuleName, 505, "Detail List")
			Language.setMessage(mstrModuleName, 507, "IP Address :")
			Language.setMessage(mstrModuleName, 509, "Machine Name :")
			Language.setMessage(mstrModuleName, 502, "To Date :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMessageBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMessageBox))
        Me.LblMessage = New System.Windows.Forms.Label
        Me.PctImage = New System.Windows.Forms.PictureBox
        Me.PnlMain = New System.Windows.Forms.Panel
        Me.BtnOK = New System.Windows.Forms.Button
        CType(Me.PctImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'LblMessage
        '
        Me.LblMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblMessage.BackColor = System.Drawing.Color.White
        Me.LblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblMessage.Location = New System.Drawing.Point(75, 8)
        Me.LblMessage.Name = "LblMessage"
        Me.LblMessage.Size = New System.Drawing.Size(621, 63)
        Me.LblMessage.TabIndex = 1
        '
        'PctImage
        '
        Me.PctImage.BackColor = System.Drawing.Color.White
        Me.PctImage.Image = CType(resources.GetObject("PctImage.Image"), System.Drawing.Image)
        Me.PctImage.Location = New System.Drawing.Point(5, 3)
        Me.PctImage.Name = "PctImage"
        Me.PctImage.Size = New System.Drawing.Size(69, 69)
        Me.PctImage.TabIndex = 2
        Me.PctImage.TabStop = False
        '
        'PnlMain
        '
        Me.PnlMain.BackColor = System.Drawing.Color.White
        Me.PnlMain.Controls.Add(Me.LblMessage)
        Me.PnlMain.Controls.Add(Me.PctImage)
        Me.PnlMain.Location = New System.Drawing.Point(0, 0)
        Me.PnlMain.Name = "PnlMain"
        Me.PnlMain.Size = New System.Drawing.Size(699, 74)
        Me.PnlMain.TabIndex = 3
        '
        'BtnOK
        '
        Me.BtnOK.Location = New System.Drawing.Point(604, 87)
        Me.BtnOK.Name = "BtnOK"
        Me.BtnOK.Size = New System.Drawing.Size(86, 30)
        Me.BtnOK.TabIndex = 3
        Me.BtnOK.Text = "OK"
        Me.BtnOK.UseVisualStyleBackColor = True
        '
        'FrmMessageBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 124)
        Me.ControlBox = False
        Me.Controls.Add(Me.BtnOK)
        Me.Controls.Add(Me.PnlMain)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(721, 163)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(379, 163)
        Me.Name = "FrmMessageBox"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aruti"
        CType(Me.PctImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlMain.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents LblMessage As System.Windows.Forms.Label
    Public WithEvents PctImage As System.Windows.Forms.PictureBox
    Friend WithEvents PnlMain As System.Windows.Forms.Panel
    Friend WithEvents BtnOK As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPreviewDocuments
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.tblpnlView = New System.Windows.Forms.TableLayoutPanel
        Me.lvPreviewList = New eZee.Common.eZeeListView(Me.components)
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDocument = New System.Windows.Forms.ColumnHeader
        Me.colhFileName = New System.Windows.Forms.ColumnHeader
        Me.objcolhDocType = New System.Windows.Forms.ColumnHeader
        Me.objcolhFullPath = New System.Windows.Forms.ColumnHeader
        Me.pnlPreview = New System.Windows.Forms.Panel
        Me.picViewImage = New System.Windows.Forms.PictureBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.tblpnlView.SuspendLayout()
        Me.pnlPreview.SuspendLayout()
        CType(Me.picViewImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.tblpnlView)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(637, 569)
        Me.pnlMain.TabIndex = 0
        '
        'tblpnlView
        '
        Me.tblpnlView.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.tblpnlView.ColumnCount = 1
        Me.tblpnlView.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpnlView.Controls.Add(Me.lvPreviewList, 0, 1)
        Me.tblpnlView.Controls.Add(Me.pnlPreview, 0, 0)
        Me.tblpnlView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblpnlView.Location = New System.Drawing.Point(0, 0)
        Me.tblpnlView.Name = "tblpnlView"
        Me.tblpnlView.RowCount = 2
        Me.tblpnlView.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 391.0!))
        Me.tblpnlView.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpnlView.Size = New System.Drawing.Size(637, 569)
        Me.tblpnlView.TabIndex = 1
        '
        'lvPreviewList
        '
        Me.lvPreviewList.BackColorOnChecked = False
        Me.lvPreviewList.ColumnHeaders = Nothing
        Me.lvPreviewList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName, Me.colhDocument, Me.colhFileName, Me.objcolhDocType, Me.objcolhFullPath})
        Me.lvPreviewList.CompulsoryColumns = ""
        Me.lvPreviewList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPreviewList.FullRowSelect = True
        Me.lvPreviewList.GridLines = True
        Me.lvPreviewList.GroupingColumn = Nothing
        Me.lvPreviewList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPreviewList.HideSelection = False
        Me.lvPreviewList.Location = New System.Drawing.Point(5, 398)
        Me.lvPreviewList.MinColumnWidth = 50
        Me.lvPreviewList.MultiSelect = False
        Me.lvPreviewList.Name = "lvPreviewList"
        Me.lvPreviewList.OptionalColumns = ""
        Me.lvPreviewList.ShowMoreItem = False
        Me.lvPreviewList.ShowSaveItem = False
        Me.lvPreviewList.ShowSelectAll = True
        Me.lvPreviewList.ShowSizeAllColumnsToFit = True
        Me.lvPreviewList.Size = New System.Drawing.Size(627, 166)
        Me.lvPreviewList.Sortable = True
        Me.lvPreviewList.TabIndex = 1
        Me.lvPreviewList.UseCompatibleStateImageBehavior = False
        Me.lvPreviewList.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 90
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Employee/Applicant"
        Me.colhName.Width = 200
        '
        'colhDocument
        '
        Me.colhDocument.Tag = "colhDocument"
        Me.colhDocument.Text = "Document"
        Me.colhDocument.Width = 165
        '
        'colhFileName
        '
        Me.colhFileName.Tag = "colhFileName"
        Me.colhFileName.Text = "Filename"
        Me.colhFileName.Width = 180
        '
        'objcolhDocType
        '
        Me.objcolhDocType.Tag = "objcolhDocType"
        Me.objcolhDocType.Text = ""
        Me.objcolhDocType.Width = 0
        '
        'objcolhFullPath
        '
        Me.objcolhFullPath.Tag = "objcolhFullPath"
        Me.objcolhFullPath.Text = ""
        Me.objcolhFullPath.Width = 0
        '
        'pnlPreview
        '
        Me.pnlPreview.AutoScroll = True
        Me.pnlPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPreview.Controls.Add(Me.objlblCaption)
        Me.pnlPreview.Controls.Add(Me.picViewImage)
        Me.pnlPreview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPreview.Location = New System.Drawing.Point(5, 5)
        Me.pnlPreview.Name = "pnlPreview"
        Me.pnlPreview.Size = New System.Drawing.Size(627, 385)
        Me.pnlPreview.TabIndex = 2
        '
        'picViewImage
        '
        Me.picViewImage.Location = New System.Drawing.Point(3, 3)
        Me.picViewImage.Name = "picViewImage"
        Me.picViewImage.Size = New System.Drawing.Size(604, 377)
        Me.picViewImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picViewImage.TabIndex = 4
        Me.picViewImage.TabStop = False
        '
        'objlblCaption
        '
        Me.objlblCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Gray
        Me.objlblCaption.Location = New System.Drawing.Point(0, 0)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(625, 383)
        Me.objlblCaption.TabIndex = 6
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmPreviewDocuments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(637, 569)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPreviewDocuments"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Preview Documents"
        Me.pnlMain.ResumeLayout(False)
        Me.tblpnlView.ResumeLayout(False)
        Me.pnlPreview.ResumeLayout(False)
        Me.pnlPreview.PerformLayout()
        CType(Me.picViewImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents tblpnlView As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lvPreviewList As eZee.Common.eZeeListView
    Friend WithEvents pnlPreview As System.Windows.Forms.Panel
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDocument As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFileName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDocType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFullPath As System.Windows.Forms.ColumnHeader
    Friend WithEvents picViewImage As System.Windows.Forms.PictureBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
End Class

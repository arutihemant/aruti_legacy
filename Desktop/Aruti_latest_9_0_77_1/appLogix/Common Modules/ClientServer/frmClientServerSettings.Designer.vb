<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientServerSettings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClientServerSettings))
        Me.gbClientServerOption = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnInstructions = New eZee.Common.eZeeGradientButton
        Me.txtSQLPort = New eZee.TextBox.NumericTextBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblAutoUpdateServer = New System.Windows.Forms.Label
        Me.txtAutoUpdateServer = New eZee.TextBox.AlphanumericTextBox
        Me.txtServer = New eZee.TextBox.AlphanumericTextBox
        Me.lblWorkingMode = New System.Windows.Forms.Label
        Me.cboWorkingMode = New System.Windows.Forms.ComboBox
        Me.lblServer = New System.Windows.Forms.Label
        Me.lblMinute = New System.Windows.Forms.Label
        Me.lblRefreshInterval = New System.Windows.Forms.Label
        Me.nudRefreshInterval = New System.Windows.Forms.NumericUpDown
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbClientServerOption.SuspendLayout()
        CType(Me.nudRefreshInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbClientServerOption
        '
        Me.gbClientServerOption.BorderColor = System.Drawing.Color.Black
        Me.gbClientServerOption.Checked = False
        Me.gbClientServerOption.CollapseAllExceptThis = False
        Me.gbClientServerOption.CollapsedHoverImage = Nothing
        Me.gbClientServerOption.CollapsedNormalImage = Nothing
        Me.gbClientServerOption.CollapsedPressedImage = Nothing
        Me.gbClientServerOption.CollapseOnLoad = False
        Me.gbClientServerOption.Controls.Add(Me.objbtnInstructions)
        Me.gbClientServerOption.Controls.Add(Me.txtSQLPort)
        Me.gbClientServerOption.Controls.Add(Me.lblPort)
        Me.gbClientServerOption.Controls.Add(Me.lblAutoUpdateServer)
        Me.gbClientServerOption.Controls.Add(Me.txtAutoUpdateServer)
        Me.gbClientServerOption.Controls.Add(Me.txtServer)
        Me.gbClientServerOption.Controls.Add(Me.lblWorkingMode)
        Me.gbClientServerOption.Controls.Add(Me.cboWorkingMode)
        Me.gbClientServerOption.Controls.Add(Me.lblServer)
        Me.gbClientServerOption.Controls.Add(Me.lblMinute)
        Me.gbClientServerOption.Controls.Add(Me.lblRefreshInterval)
        Me.gbClientServerOption.Controls.Add(Me.nudRefreshInterval)
        Me.gbClientServerOption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbClientServerOption.ExpandedHoverImage = Nothing
        Me.gbClientServerOption.ExpandedNormalImage = Nothing
        Me.gbClientServerOption.ExpandedPressedImage = Nothing
        Me.gbClientServerOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClientServerOption.GradientColor = System.Drawing.Color.Transparent
        Me.gbClientServerOption.HeaderHeight = 25
        Me.gbClientServerOption.HeaderMessage = ""
        Me.gbClientServerOption.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbClientServerOption.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbClientServerOption.HeightOnCollapse = 0
        Me.gbClientServerOption.LeftTextSpace = 0
        Me.gbClientServerOption.Location = New System.Drawing.Point(0, 0)
        Me.gbClientServerOption.Name = "gbClientServerOption"
        Me.gbClientServerOption.OpenHeight = 100
        Me.gbClientServerOption.Padding = New System.Windows.Forms.Padding(3)
        Me.gbClientServerOption.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClientServerOption.ShowBorder = True
        Me.gbClientServerOption.ShowCheckBox = False
        Me.gbClientServerOption.ShowCollapseButton = False
        Me.gbClientServerOption.ShowDefaultBorderColor = True
        Me.gbClientServerOption.ShowDownButton = False
        Me.gbClientServerOption.ShowHeader = True
        Me.gbClientServerOption.Size = New System.Drawing.Size(375, 146)
        Me.gbClientServerOption.TabIndex = 1
        Me.gbClientServerOption.Temp = 0
        Me.gbClientServerOption.Text = "Client/Server settings"
        Me.gbClientServerOption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnInstructions
        '
        Me.objbtnInstructions.BackColor = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnInstructions.BorderSelected = False
        Me.objbtnInstructions.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnInstructions.Image = CType(resources.GetObject("objbtnInstructions.Image"), System.Drawing.Image)
        Me.objbtnInstructions.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnInstructions.Location = New System.Drawing.Point(337, 86)
        Me.objbtnInstructions.Name = "objbtnInstructions"
        Me.objbtnInstructions.Size = New System.Drawing.Size(21, 21)
        Me.objbtnInstructions.TabIndex = 26
        '
        'txtSQLPort
        '
        Me.txtSQLPort.AllowNegative = False
        Me.txtSQLPort.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSQLPort.DigitsInGroup = 0
        Me.txtSQLPort.Flags = 65536
        Me.txtSQLPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSQLPort.Location = New System.Drawing.Point(273, 86)
        Me.txtSQLPort.MaxDecimalPlaces = 0
        Me.txtSQLPort.MaxWholeDigits = 21
        Me.txtSQLPort.Name = "txtSQLPort"
        Me.txtSQLPort.Prefix = ""
        Me.txtSQLPort.RangeMax = 1.7976931348623157E+308
        Me.txtSQLPort.RangeMin = -1.7976931348623157E+308
        Me.txtSQLPort.Size = New System.Drawing.Size(58, 21)
        Me.txtSQLPort.TabIndex = 24
        Me.txtSQLPort.Text = "0"
        Me.txtSQLPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPort
        '
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPort.Location = New System.Drawing.Point(215, 88)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(52, 16)
        Me.lblPort.TabIndex = 11
        Me.lblPort.Text = "SQL Port"
        '
        'lblAutoUpdateServer
        '
        Me.lblAutoUpdateServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAutoUpdateServer.Location = New System.Drawing.Point(8, 115)
        Me.lblAutoUpdateServer.Name = "lblAutoUpdateServer"
        Me.lblAutoUpdateServer.Size = New System.Drawing.Size(113, 16)
        Me.lblAutoUpdateServer.TabIndex = 9
        Me.lblAutoUpdateServer.Text = "Auto Update Server"
        '
        'txtAutoUpdateServer
        '
        Me.txtAutoUpdateServer.Flags = 0
        Me.txtAutoUpdateServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAutoUpdateServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAutoUpdateServer.Location = New System.Drawing.Point(127, 113)
        Me.txtAutoUpdateServer.Name = "txtAutoUpdateServer"
        Me.txtAutoUpdateServer.Size = New System.Drawing.Size(233, 21)
        Me.txtAutoUpdateServer.TabIndex = 8
        '
        'txtServer
        '
        Me.txtServer.Flags = 0
        Me.txtServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServer.Location = New System.Drawing.Point(127, 59)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(233, 21)
        Me.txtServer.TabIndex = 3
        '
        'lblWorkingMode
        '
        Me.lblWorkingMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkingMode.Location = New System.Drawing.Point(8, 34)
        Me.lblWorkingMode.Name = "lblWorkingMode"
        Me.lblWorkingMode.Size = New System.Drawing.Size(113, 16)
        Me.lblWorkingMode.TabIndex = 0
        Me.lblWorkingMode.Text = "Working Mode"
        '
        'cboWorkingMode
        '
        Me.cboWorkingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkingMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkingMode.FormattingEnabled = True
        Me.cboWorkingMode.Items.AddRange(New Object() {"Client"})
        Me.cboWorkingMode.Location = New System.Drawing.Point(127, 32)
        Me.cboWorkingMode.Name = "cboWorkingMode"
        Me.cboWorkingMode.Size = New System.Drawing.Size(233, 21)
        Me.cboWorkingMode.TabIndex = 1
        '
        'lblServer
        '
        Me.lblServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServer.Location = New System.Drawing.Point(8, 61)
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(113, 16)
        Me.lblServer.TabIndex = 2
        Me.lblServer.Text = "Database Server"
        '
        'lblMinute
        '
        Me.lblMinute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinute.Location = New System.Drawing.Point(180, 88)
        Me.lblMinute.Name = "lblMinute"
        Me.lblMinute.Size = New System.Drawing.Size(29, 16)
        Me.lblMinute.TabIndex = 6
        Me.lblMinute.Text = "Mins"
        '
        'lblRefreshInterval
        '
        Me.lblRefreshInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefreshInterval.Location = New System.Drawing.Point(8, 88)
        Me.lblRefreshInterval.Name = "lblRefreshInterval"
        Me.lblRefreshInterval.Size = New System.Drawing.Size(113, 16)
        Me.lblRefreshInterval.TabIndex = 4
        Me.lblRefreshInterval.Text = "Refresh Interval"
        '
        'nudRefreshInterval
        '
        Me.nudRefreshInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudRefreshInterval.Location = New System.Drawing.Point(127, 86)
        Me.nudRefreshInterval.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudRefreshInterval.Name = "nudRefreshInterval"
        Me.nudRefreshInterval.Size = New System.Drawing.Size(47, 21)
        Me.nudRefreshInterval.TabIndex = 5
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnSave)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 146)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(375, 56)
        Me.objefFormFooter.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(167, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(101, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save && Close"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(274, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'frmClientServerSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(375, 202)
        Me.Controls.Add(Me.gbClientServerOption)
        Me.Controls.Add(Me.objefFormFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmClientServerSettings"
        Me.ShowIcon = False
        Me.ShowLanguageButton = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Client/Server Settings"
        Me.gbClientServerOption.ResumeLayout(False)
        Me.gbClientServerOption.PerformLayout()
        CType(Me.nudRefreshInterval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbClientServerOption As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblWorkingMode As System.Windows.Forms.Label
    Friend WithEvents cboWorkingMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblServer As System.Windows.Forms.Label
    Friend WithEvents lblMinute As System.Windows.Forms.Label
    Friend WithEvents lblRefreshInterval As System.Windows.Forms.Label
    Friend WithEvents nudRefreshInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblAutoUpdateServer As System.Windows.Forms.Label
    Friend WithEvents txtAutoUpdateServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents txtSQLPort As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnInstructions As eZee.Common.eZeeGradientButton
End Class

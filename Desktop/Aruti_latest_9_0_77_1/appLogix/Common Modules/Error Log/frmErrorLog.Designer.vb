﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmErrorLog
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmErrorLog))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvError = New System.Windows.Forms.DataGridView
        Me.objcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhErrorlogunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhErrorMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhError_Location = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhError_Date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhDBVersion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsEmailSent = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhIsFromWeb = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsESS = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhArchivedBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhArchivedDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhHost = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIP = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblTodate = New System.Windows.Forms.Label
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.chkOnlyEmailSent = New System.Windows.Forms.CheckBox
        Me.chkOnlyArchived = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSendEmail = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnArchive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.Panel1.SuspendLayout()
        CType(Me.dgvError, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.dgvError)
        Me.Panel1.Controls.Add(Me.gbFilterCriteria)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(839, 496)
        Me.Panel1.TabIndex = 0
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkSelectAll.Location = New System.Drawing.Point(21, 147)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(18, 24)
        Me.objchkSelectAll.TabIndex = 154
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvError
        '
        Me.dgvError.AllowUserToAddRows = False
        Me.dgvError.AllowUserToDeleteRows = False
        Me.dgvError.AllowUserToResizeRows = False
        Me.dgvError.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvError.BackgroundColor = System.Drawing.Color.White
        Me.dgvError.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvError.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheck, Me.objcolhErrorlogunkid, Me.colhErrorMessage, Me.colhError_Location, Me.colhError_Date, Me.colhDBVersion, Me.colhIsEmailSent, Me.colhIsFromWeb, Me.colhUserName, Me.colhIsESS, Me.colhArchivedBy, Me.colhArchivedDate, Me.colhHost, Me.colhIP})
        Me.dgvError.Location = New System.Drawing.Point(12, 135)
        Me.dgvError.Name = "dgvError"
        Me.dgvError.RowHeadersVisible = False
        Me.dgvError.Size = New System.Drawing.Size(815, 300)
        Me.dgvError.TabIndex = 153
        '
        'objcolhCheck
        '
        Me.objcolhCheck.HeaderText = ""
        Me.objcolhCheck.Name = "objcolhCheck"
        Me.objcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhCheck.Width = 30
        '
        'objcolhErrorlogunkid
        '
        Me.objcolhErrorlogunkid.HeaderText = ""
        Me.objcolhErrorlogunkid.Name = "objcolhErrorlogunkid"
        Me.objcolhErrorlogunkid.ReadOnly = True
        Me.objcolhErrorlogunkid.Visible = False
        '
        'colhErrorMessage
        '
        Me.colhErrorMessage.HeaderText = "Error Message"
        Me.colhErrorMessage.Name = "colhErrorMessage"
        Me.colhErrorMessage.ReadOnly = True
        Me.colhErrorMessage.Width = 200
        '
        'colhError_Location
        '
        Me.colhError_Location.HeaderText = "Error Location"
        Me.colhError_Location.Name = "colhError_Location"
        Me.colhError_Location.ReadOnly = True
        Me.colhError_Location.Width = 150
        '
        'colhError_Date
        '
        Me.colhError_Date.HeaderText = "Error Date"
        Me.colhError_Date.Name = "colhError_Date"
        Me.colhError_Date.ReadOnly = True
        Me.colhError_Date.Width = 80
        '
        'colhDBVersion
        '
        Me.colhDBVersion.HeaderText = "Database Version"
        Me.colhDBVersion.Name = "colhDBVersion"
        Me.colhDBVersion.ReadOnly = True
        Me.colhDBVersion.Width = 70
        '
        'colhIsEmailSent
        '
        Me.colhIsEmailSent.HeaderText = "Is Email Sent"
        Me.colhIsEmailSent.Name = "colhIsEmailSent"
        Me.colhIsEmailSent.ReadOnly = True
        Me.colhIsEmailSent.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhIsEmailSent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhIsEmailSent.Width = 40
        '
        'colhIsFromWeb
        '
        Me.colhIsFromWeb.HeaderText = "Is From Web"
        Me.colhIsFromWeb.Name = "colhIsFromWeb"
        Me.colhIsFromWeb.ReadOnly = True
        Me.colhIsFromWeb.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhIsFromWeb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhIsFromWeb.Width = 40
        '
        'colhUserName
        '
        Me.colhUserName.HeaderText = "Reported By"
        Me.colhUserName.Name = "colhUserName"
        Me.colhUserName.ReadOnly = True
        Me.colhUserName.Width = 80
        '
        'colhIsESS
        '
        Me.colhIsESS.HeaderText = "Is ESS"
        Me.colhIsESS.Name = "colhIsESS"
        Me.colhIsESS.ReadOnly = True
        Me.colhIsESS.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhIsESS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhIsESS.Width = 40
        '
        'colhArchivedBy
        '
        Me.colhArchivedBy.HeaderText = "Archived By"
        Me.colhArchivedBy.Name = "colhArchivedBy"
        Me.colhArchivedBy.ReadOnly = True
        Me.colhArchivedBy.Width = 80
        '
        'colhArchivedDate
        '
        Me.colhArchivedDate.HeaderText = "Archived Date"
        Me.colhArchivedDate.Name = "colhArchivedDate"
        Me.colhArchivedDate.ReadOnly = True
        Me.colhArchivedDate.Width = 80
        '
        'colhHost
        '
        Me.colhHost.HeaderText = "Host Name"
        Me.colhHost.Name = "colhHost"
        Me.colhHost.ReadOnly = True
        '
        'colhIP
        '
        Me.colhIP.HeaderText = "IP"
        Me.colhIP.Name = "colhIP"
        Me.colhIP.ReadOnly = True
        Me.colhIP.Width = 70
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblTodate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTodate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.chkOnlyEmailSent)
        Me.gbFilterCriteria.Controls.Add(Me.chkOnlyArchived)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(815, 65)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTodate
        '
        Me.lblTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTodate.Location = New System.Drawing.Point(212, 35)
        Me.lblTodate.Name = "lblTodate"
        Me.lblTodate.Size = New System.Drawing.Size(31, 15)
        Me.lblTodate.TabIndex = 214
        Me.lblTodate.Text = "To"
        '
        'dtpTodate
        '
        Me.dtpTodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(249, 32)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.ShowCheckBox = True
        Me.dtpTodate.Size = New System.Drawing.Size(104, 21)
        Me.dtpTodate.TabIndex = 213
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(102, 32)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpFromDate.TabIndex = 211
        '
        'lblFromDate
        '
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(11, 35)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(85, 15)
        Me.lblFromDate.TabIndex = 212
        Me.lblFromDate.Text = "From Date"
        '
        'chkOnlyEmailSent
        '
        Me.chkOnlyEmailSent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkOnlyEmailSent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOnlyEmailSent.Location = New System.Drawing.Point(640, 33)
        Me.chkOnlyEmailSent.Name = "chkOnlyEmailSent"
        Me.chkOnlyEmailSent.Size = New System.Drawing.Size(145, 24)
        Me.chkOnlyEmailSent.TabIndex = 1
        Me.chkOnlyEmailSent.Text = "Only Email Sent"
        Me.chkOnlyEmailSent.UseVisualStyleBackColor = True
        '
        'chkOnlyArchived
        '
        Me.chkOnlyArchived.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkOnlyArchived.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOnlyArchived.Location = New System.Drawing.Point(489, 33)
        Me.chkOnlyArchived.Name = "chkOnlyArchived"
        Me.chkOnlyArchived.Size = New System.Drawing.Size(145, 24)
        Me.chkOnlyArchived.TabIndex = 0
        Me.chkOnlyArchived.Text = "Only Archived"
        Me.chkOnlyArchived.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(786, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 146
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(763, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 145
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSendEmail)
        Me.objFooter.Controls.Add(Me.btnExport)
        Me.objFooter.Controls.Add(Me.btnArchive)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 441)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(839, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSendEmail
        '
        Me.btnSendEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSendEmail.BackColor = System.Drawing.Color.White
        Me.btnSendEmail.BackgroundImage = CType(resources.GetObject("btnSendEmail.BackgroundImage"), System.Drawing.Image)
        Me.btnSendEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSendEmail.BorderColor = System.Drawing.Color.Empty
        Me.btnSendEmail.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSendEmail.FlatAppearance.BorderSize = 0
        Me.btnSendEmail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSendEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSendEmail.ForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSendEmail.GradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmail.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.Location = New System.Drawing.Point(627, 13)
        Me.btnSendEmail.Name = "btnSendEmail"
        Me.btnSendEmail.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSendEmail.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSendEmail.Size = New System.Drawing.Size(97, 30)
        Me.btnSendEmail.TabIndex = 1
        Me.btnSendEmail.Text = "&Send Email"
        Me.btnSendEmail.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(12, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(105, 30)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "&Export List..."
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnArchive
        '
        Me.btnArchive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnArchive.BackColor = System.Drawing.Color.White
        Me.btnArchive.BackgroundImage = CType(resources.GetObject("btnArchive.BackgroundImage"), System.Drawing.Image)
        Me.btnArchive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnArchive.BorderColor = System.Drawing.Color.Empty
        Me.btnArchive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnArchive.FlatAppearance.BorderSize = 0
        Me.btnArchive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnArchive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnArchive.ForeColor = System.Drawing.Color.Black
        Me.btnArchive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnArchive.GradientForeColor = System.Drawing.Color.Black
        Me.btnArchive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnArchive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnArchive.Location = New System.Drawing.Point(524, 13)
        Me.btnArchive.Name = "btnArchive"
        Me.btnArchive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnArchive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnArchive.Size = New System.Drawing.Size(97, 30)
        Me.btnArchive.TabIndex = 0
        Me.btnArchive.Text = "&Archive"
        Me.btnArchive.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(730, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(839, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "System Error Log"
        '
        'frmErrorLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 496)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(845, 525)
        Me.Name = "frmErrorLog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "System Error Log"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvError, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Public WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSendEmail As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnArchive As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvError As System.Windows.Forms.DataGridView
    Friend WithEvents chkOnlyEmailSent As System.Windows.Forms.CheckBox
    Friend WithEvents chkOnlyArchived As System.Windows.Forms.CheckBox
    Friend WithEvents lblTodate As System.Windows.Forms.Label
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents objcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhErrorlogunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhErrorMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhError_Location As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhError_Date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhDBVersion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsEmailSent As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhIsFromWeb As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsESS As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhArchivedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhArchivedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhHost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
End Class

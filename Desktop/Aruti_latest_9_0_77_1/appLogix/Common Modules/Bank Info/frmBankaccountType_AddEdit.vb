﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmBankAccountType_AddEdit

    Private ReadOnly mstrModuleName As String = "frmBankAccountType_AddEdit"

    Private mblnCancel As Boolean = True

    Private objBankAccType As clsBankAccType

    Private menAction As enAction = enAction.ADD_ONE
    Private mintBankAccTypeUnkid As Integer = -1


#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBankAccTypeUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBankAccTypeUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtAlias.BackColor = GUI.ColorOptional
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objBankAccType._Accounttype_Code
        txtName.Text = objBankAccType._Accounttype_Name
        txtAlias.Text = objBankAccType._Accounttype_Alias
        txtDescription.Text = objBankAccType._Description
    End Sub

    Private Sub SetValue()
        objBankAccType._Accounttype_Code = txtCode.Text
        objBankAccType._Accounttype_Name = txtName.Text
        objBankAccType._Accounttype_Alias = txtAlias.Text
        objBankAccType._Description = txtDescription.Text

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmBankAccType_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBankAccType = Nothing
    End Sub

    Private Sub frmBankAccType_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmBankAccType_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmBankAccType_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBankAccType = New clsBankAccType
        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Call OtherSettings()
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objBankAccType._Accounttypeunkid = mintBankAccTypeUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankAccType_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankAccType.SetMessages()
            objfrm._Other_ModuleNames = "clsBankAccType"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region


#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Bank Account Type Code cannot be blank. Bank Account Type Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If


            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Bank Account Type Name cannot be blank. Bank Account Type Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBankAccType._FormName = mstrModuleName
            objBankAccType._LoginEmployeeunkid = 0
            objBankAccType._ClientIP = getIP()
            objBankAccType._HostName = getHostName()
            objBankAccType._FromWeb = False
            objBankAccType._AuditUserId = User._Object._Userunkid
objBankAccType._CompanyUnkid = Company._Object._Companyunkid
            objBankAccType._AuditDate = Now
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBankAccType.Update()
            Else
                blnFlag = objBankAccType.Insert()
            End If

            If blnFlag = False And objBankAccType._Message <> "" Then
                eZeeMsgBox.Show(objBankAccType._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBankAccType = Nothing
                    objBankAccType = New clsBankAccType
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintBankAccTypeUnkid = objBankAccType._Accounttypeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 
            Call objFrm.displayDialog(txtName.Text, objBankAccType._Accounttype_Name1, objBankAccType._Accounttype_Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbbankAccountTypeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbbankAccountTypeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbbankAccountTypeInfo.Text = Language._Object.getCaption(Me.gbbankAccountTypeInfo.Name, Me.gbbankAccountTypeInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Bank Account Type Code cannot be blank. Bank Account Type Code is required information.")
			Language.setMessage(mstrModuleName, 2, "Bank Account Type Name cannot be blank. Bank Account Type Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEFTCustomColumnsExport
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEFTCustomColumnsExport))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.chkTABDelimiter = New System.Windows.Forms.CheckBox
        Me.chkSaveAsTXT = New System.Windows.Forms.CheckBox
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.chkShowColumnHeader = New System.Windows.Forms.CheckBox
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkNAll = New System.Windows.Forms.CheckBox
        Me.elEFTCustomColumns = New eZee.Common.eZeeLine
        Me.lvEFTCustomColumns = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhNCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEFTCustomColumns = New System.Windows.Forms.ColumnHeader
        Me.txtDateFormat = New System.Windows.Forms.TextBox
        Me.lblDateFormat = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.lblDateFormat)
        Me.pnlMain.Controls.Add(Me.txtDateFormat)
        Me.pnlMain.Controls.Add(Me.chkTABDelimiter)
        Me.pnlMain.Controls.Add(Me.chkSaveAsTXT)
        Me.pnlMain.Controls.Add(Me.cboMembership)
        Me.pnlMain.Controls.Add(Me.lblMembership)
        Me.pnlMain.Controls.Add(Me.chkShowColumnHeader)
        Me.pnlMain.Controls.Add(Me.objbtnUp)
        Me.pnlMain.Controls.Add(Me.objbtnDown)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.objchkNAll)
        Me.pnlMain.Controls.Add(Me.elEFTCustomColumns)
        Me.pnlMain.Controls.Add(Me.lvEFTCustomColumns)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(348, 546)
        Me.pnlMain.TabIndex = 1
        '
        'chkTABDelimiter
        '
        Me.chkTABDelimiter.Location = New System.Drawing.Point(184, 468)
        Me.chkTABDelimiter.Name = "chkTABDelimiter"
        Me.chkTABDelimiter.Size = New System.Drawing.Size(142, 17)
        Me.chkTABDelimiter.TabIndex = 233
        Me.chkTABDelimiter.Text = "TAB Delimiter"
        Me.chkTABDelimiter.UseVisualStyleBackColor = True
        '
        'chkSaveAsTXT
        '
        Me.chkSaveAsTXT.Location = New System.Drawing.Point(11, 468)
        Me.chkSaveAsTXT.Name = "chkSaveAsTXT"
        Me.chkSaveAsTXT.Size = New System.Drawing.Size(142, 17)
        Me.chkSaveAsTXT.TabIndex = 232
        Me.chkSaveAsTXT.Text = "Save As TXT"
        Me.chkSaveAsTXT.UseVisualStyleBackColor = True
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 250
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(113, 415)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(180, 21)
        Me.cboMembership.TabIndex = 231
        '
        'lblMembership
        '
        Me.lblMembership.Location = New System.Drawing.Point(12, 419)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(95, 13)
        Me.lblMembership.TabIndex = 2
        Me.lblMembership.Text = "Membership"
        '
        'chkShowColumnHeader
        '
        Me.chkShowColumnHeader.Location = New System.Drawing.Point(12, 392)
        Me.chkShowColumnHeader.Name = "chkShowColumnHeader"
        Me.chkShowColumnHeader.Size = New System.Drawing.Size(281, 17)
        Me.chkShowColumnHeader.TabIndex = 230
        Me.chkShowColumnHeader.Text = "Show Column Header on Report"
        Me.chkShowColumnHeader.UseVisualStyleBackColor = True
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Data.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(300, 26)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 228
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.Aruti.Data.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(300, 57)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 229
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSaveSelection)
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 491)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(348, 55)
        Me.EZeeFooter1.TabIndex = 42
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(7, 13)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 36
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(146, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 35
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(242, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 34
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objchkNAll
        '
        Me.objchkNAll.AutoSize = True
        Me.objchkNAll.Location = New System.Drawing.Point(14, 31)
        Me.objchkNAll.Name = "objchkNAll"
        Me.objchkNAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkNAll.TabIndex = 41
        Me.objchkNAll.UseVisualStyleBackColor = True
        '
        'elEFTCustomColumns
        '
        Me.elEFTCustomColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEFTCustomColumns.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEFTCustomColumns.Location = New System.Drawing.Point(12, 6)
        Me.elEFTCustomColumns.Name = "elEFTCustomColumns"
        Me.elEFTCustomColumns.Size = New System.Drawing.Size(320, 17)
        Me.elEFTCustomColumns.TabIndex = 40
        Me.elEFTCustomColumns.Text = "Select Columns for EFT"
        Me.elEFTCustomColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvEFTCustomColumns
        '
        Me.lvEFTCustomColumns.BackColorOnChecked = False
        Me.lvEFTCustomColumns.CheckBoxes = True
        Me.lvEFTCustomColumns.ColumnHeaders = Nothing
        Me.lvEFTCustomColumns.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhNCheck, Me.colhEFTCustomColumns})
        Me.lvEFTCustomColumns.CompulsoryColumns = ""
        Me.lvEFTCustomColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEFTCustomColumns.FullRowSelect = True
        Me.lvEFTCustomColumns.GridLines = True
        Me.lvEFTCustomColumns.GroupingColumn = Nothing
        Me.lvEFTCustomColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEFTCustomColumns.HideSelection = False
        Me.lvEFTCustomColumns.Location = New System.Drawing.Point(7, 26)
        Me.lvEFTCustomColumns.MinColumnWidth = 50
        Me.lvEFTCustomColumns.MultiSelect = False
        Me.lvEFTCustomColumns.Name = "lvEFTCustomColumns"
        Me.lvEFTCustomColumns.OptionalColumns = ""
        Me.lvEFTCustomColumns.ShowMoreItem = False
        Me.lvEFTCustomColumns.ShowSaveItem = False
        Me.lvEFTCustomColumns.ShowSelectAll = True
        Me.lvEFTCustomColumns.ShowSizeAllColumnsToFit = True
        Me.lvEFTCustomColumns.Size = New System.Drawing.Size(286, 360)
        Me.lvEFTCustomColumns.Sortable = True
        Me.lvEFTCustomColumns.TabIndex = 39
        Me.lvEFTCustomColumns.UseCompatibleStateImageBehavior = False
        Me.lvEFTCustomColumns.View = System.Windows.Forms.View.Details
        '
        'objcolhNCheck
        '
        Me.objcolhNCheck.Tag = "objcolhNCheck"
        Me.objcolhNCheck.Text = ""
        Me.objcolhNCheck.Width = 25
        '
        'colhEFTCustomColumns
        '
        Me.colhEFTCustomColumns.Tag = "colhEFTCustomColumns"
        Me.colhEFTCustomColumns.Text = "EFT Custom Columns"
        Me.colhEFTCustomColumns.Width = 250
        '
        'txtDateFormat
        '
        Me.txtDateFormat.Location = New System.Drawing.Point(114, 442)
        Me.txtDateFormat.Name = "txtDateFormat"
        Me.txtDateFormat.Size = New System.Drawing.Size(100, 21)
        Me.txtDateFormat.TabIndex = 234
        '
        'lblDateFormat
        '
        Me.lblDateFormat.Location = New System.Drawing.Point(12, 445)
        Me.lblDateFormat.Name = "lblDateFormat"
        Me.lblDateFormat.Size = New System.Drawing.Size(95, 13)
        Me.lblDateFormat.TabIndex = 235
        Me.lblDateFormat.Text = "Date Format"
        '
        'frmEFTCustomColumnsExport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 546)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEFTCustomColumnsExport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "EFT Custom Columns Export"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objchkNAll As System.Windows.Forms.CheckBox
    Friend WithEvents elEFTCustomColumns As eZee.Common.eZeeLine
    Friend WithEvents lvEFTCustomColumns As eZee.Common.eZeeListView
    Friend WithEvents objcolhNCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEFTCustomColumns As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents chkShowColumnHeader As System.Windows.Forms.CheckBox
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents chkSaveAsTXT As System.Windows.Forms.CheckBox
    Friend WithEvents chkTABDelimiter As System.Windows.Forms.CheckBox
    Friend WithEvents txtDateFormat As System.Windows.Forms.TextBox
    Friend WithEvents lblDateFormat As System.Windows.Forms.Label
End Class

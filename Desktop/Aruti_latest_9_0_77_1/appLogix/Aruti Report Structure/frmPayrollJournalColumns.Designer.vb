﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollJournalColumns
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollJournalColumns))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.cboExportMode = New System.Windows.Forms.ComboBox
        Me.lblExportMode = New System.Windows.Forms.Label
        Me.chkTABDelimiter = New System.Windows.Forms.CheckBox
        Me.chkSaveAsTXT = New System.Windows.Forms.CheckBox
        Me.chkShowColumnHeader = New System.Windows.Forms.CheckBox
        Me.objbtnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnDown = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objchkNAll = New System.Windows.Forms.CheckBox
        Me.elPayrollJournalColumns = New eZee.Common.eZeeLine
        Me.lvPayrollJournalColumns = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhNCheck = New System.Windows.Forms.ColumnHeader
        Me.colhPayrollJournalColumns = New System.Windows.Forms.ColumnHeader
        Me.lblDateFormat = New System.Windows.Forms.Label
        Me.txtDateFormat = New System.Windows.Forms.TextBox
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.lblDateFormat)
        Me.pnlMain.Controls.Add(Me.txtDateFormat)
        Me.pnlMain.Controls.Add(Me.cboExportMode)
        Me.pnlMain.Controls.Add(Me.lblExportMode)
        Me.pnlMain.Controls.Add(Me.chkTABDelimiter)
        Me.pnlMain.Controls.Add(Me.chkSaveAsTXT)
        Me.pnlMain.Controls.Add(Me.chkShowColumnHeader)
        Me.pnlMain.Controls.Add(Me.objbtnUp)
        Me.pnlMain.Controls.Add(Me.objbtnDown)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.objchkNAll)
        Me.pnlMain.Controls.Add(Me.elPayrollJournalColumns)
        Me.pnlMain.Controls.Add(Me.lvPayrollJournalColumns)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(348, 554)
        Me.pnlMain.TabIndex = 2
        '
        'cboExportMode
        '
        Me.cboExportMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExportMode.DropDownWidth = 250
        Me.cboExportMode.FormattingEnabled = True
        Me.cboExportMode.Location = New System.Drawing.Point(113, 392)
        Me.cboExportMode.Name = "cboExportMode"
        Me.cboExportMode.Size = New System.Drawing.Size(180, 21)
        Me.cboExportMode.TabIndex = 235
        '
        'lblExportMode
        '
        Me.lblExportMode.Location = New System.Drawing.Point(7, 396)
        Me.lblExportMode.Name = "lblExportMode"
        Me.lblExportMode.Size = New System.Drawing.Size(100, 13)
        Me.lblExportMode.TabIndex = 234
        Me.lblExportMode.Text = "Export Mode"
        '
        'chkTABDelimiter
        '
        Me.chkTABDelimiter.Location = New System.Drawing.Point(180, 472)
        Me.chkTABDelimiter.Name = "chkTABDelimiter"
        Me.chkTABDelimiter.Size = New System.Drawing.Size(142, 17)
        Me.chkTABDelimiter.TabIndex = 233
        Me.chkTABDelimiter.Text = "TAB Delimiter"
        Me.chkTABDelimiter.UseVisualStyleBackColor = True
        '
        'chkSaveAsTXT
        '
        Me.chkSaveAsTXT.Location = New System.Drawing.Point(7, 472)
        Me.chkSaveAsTXT.Name = "chkSaveAsTXT"
        Me.chkSaveAsTXT.Size = New System.Drawing.Size(142, 17)
        Me.chkSaveAsTXT.TabIndex = 232
        Me.chkSaveAsTXT.Text = "Save As TXT"
        Me.chkSaveAsTXT.UseVisualStyleBackColor = True
        '
        'chkShowColumnHeader
        '
        Me.chkShowColumnHeader.Location = New System.Drawing.Point(7, 449)
        Me.chkShowColumnHeader.Name = "chkShowColumnHeader"
        Me.chkShowColumnHeader.Size = New System.Drawing.Size(281, 17)
        Me.chkShowColumnHeader.TabIndex = 230
        Me.chkShowColumnHeader.Text = "Show Column Header on Report"
        Me.chkShowColumnHeader.UseVisualStyleBackColor = True
        '
        'objbtnUp
        '
        Me.objbtnUp.BackColor = System.Drawing.Color.White
        Me.objbtnUp.BackgroundImage = CType(resources.GetObject("objbtnUp.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUp.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUp.FlatAppearance.BorderSize = 0
        Me.objbtnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUp.ForeColor = System.Drawing.Color.Black
        Me.objbtnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUp.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Image = Global.Aruti.Data.My.Resources.Resources.MoveUp_16
        Me.objbtnUp.Location = New System.Drawing.Point(300, 26)
        Me.objbtnUp.Name = "objbtnUp"
        Me.objbtnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUp.Size = New System.Drawing.Size(32, 30)
        Me.objbtnUp.TabIndex = 228
        Me.objbtnUp.UseVisualStyleBackColor = False
        '
        'objbtnDown
        '
        Me.objbtnDown.BackColor = System.Drawing.Color.White
        Me.objbtnDown.BackgroundImage = CType(resources.GetObject("objbtnDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDown.BorderColor = System.Drawing.Color.Empty
        Me.objbtnDown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnDown.FlatAppearance.BorderSize = 0
        Me.objbtnDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Image = Global.Aruti.Data.My.Resources.Resources.MoveDown_16
        Me.objbtnDown.Location = New System.Drawing.Point(300, 57)
        Me.objbtnDown.Name = "objbtnDown"
        Me.objbtnDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDown.Size = New System.Drawing.Size(32, 30)
        Me.objbtnDown.TabIndex = 229
        Me.objbtnDown.UseVisualStyleBackColor = False
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSaveSelection)
        Me.EZeeFooter1.Controls.Add(Me.btnOk)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 499)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(348, 55)
        Me.EZeeFooter1.TabIndex = 42
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(7, 13)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 36
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(146, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(90, 30)
        Me.btnOk.TabIndex = 35
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(242, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 34
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objchkNAll
        '
        Me.objchkNAll.AutoSize = True
        Me.objchkNAll.Location = New System.Drawing.Point(14, 31)
        Me.objchkNAll.Name = "objchkNAll"
        Me.objchkNAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkNAll.TabIndex = 41
        Me.objchkNAll.UseVisualStyleBackColor = True
        '
        'elPayrollJournalColumns
        '
        Me.elPayrollJournalColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPayrollJournalColumns.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPayrollJournalColumns.Location = New System.Drawing.Point(12, 6)
        Me.elPayrollJournalColumns.Name = "elPayrollJournalColumns"
        Me.elPayrollJournalColumns.Size = New System.Drawing.Size(320, 17)
        Me.elPayrollJournalColumns.TabIndex = 40
        Me.elPayrollJournalColumns.Text = "Select Columns for Payroll Journal"
        Me.elPayrollJournalColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvPayrollJournalColumns
        '
        Me.lvPayrollJournalColumns.BackColorOnChecked = False
        Me.lvPayrollJournalColumns.CheckBoxes = True
        Me.lvPayrollJournalColumns.ColumnHeaders = Nothing
        Me.lvPayrollJournalColumns.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhNCheck, Me.colhPayrollJournalColumns})
        Me.lvPayrollJournalColumns.CompulsoryColumns = ""
        Me.lvPayrollJournalColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPayrollJournalColumns.FullRowSelect = True
        Me.lvPayrollJournalColumns.GridLines = True
        Me.lvPayrollJournalColumns.GroupingColumn = Nothing
        Me.lvPayrollJournalColumns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPayrollJournalColumns.HideSelection = False
        Me.lvPayrollJournalColumns.Location = New System.Drawing.Point(7, 26)
        Me.lvPayrollJournalColumns.MinColumnWidth = 50
        Me.lvPayrollJournalColumns.MultiSelect = False
        Me.lvPayrollJournalColumns.Name = "lvPayrollJournalColumns"
        Me.lvPayrollJournalColumns.OptionalColumns = ""
        Me.lvPayrollJournalColumns.ShowMoreItem = False
        Me.lvPayrollJournalColumns.ShowSaveItem = False
        Me.lvPayrollJournalColumns.ShowSelectAll = True
        Me.lvPayrollJournalColumns.ShowSizeAllColumnsToFit = True
        Me.lvPayrollJournalColumns.Size = New System.Drawing.Size(286, 360)
        Me.lvPayrollJournalColumns.Sortable = True
        Me.lvPayrollJournalColumns.TabIndex = 39
        Me.lvPayrollJournalColumns.UseCompatibleStateImageBehavior = False
        Me.lvPayrollJournalColumns.View = System.Windows.Forms.View.Details
        '
        'objcolhNCheck
        '
        Me.objcolhNCheck.Tag = "objcolhNCheck"
        Me.objcolhNCheck.Text = ""
        Me.objcolhNCheck.Width = 25
        '
        'colhPayrollJournalColumns
        '
        Me.colhPayrollJournalColumns.Tag = "colhPayrollJournalColumns"
        Me.colhPayrollJournalColumns.Text = "Payroll Journal Columns"
        Me.colhPayrollJournalColumns.Width = 250
        '
        'lblDateFormat
        '
        Me.lblDateFormat.Location = New System.Drawing.Point(11, 425)
        Me.lblDateFormat.Name = "lblDateFormat"
        Me.lblDateFormat.Size = New System.Drawing.Size(95, 13)
        Me.lblDateFormat.TabIndex = 237
        Me.lblDateFormat.Text = "Date Format"
        '
        'txtDateFormat
        '
        Me.txtDateFormat.Location = New System.Drawing.Point(113, 422)
        Me.txtDateFormat.Name = "txtDateFormat"
        Me.txtDateFormat.Size = New System.Drawing.Size(100, 21)
        Me.txtDateFormat.TabIndex = 236
        '
        'frmPayrollJournalColumns
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 554)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollJournalColumns"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Journal Columns"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents chkTABDelimiter As System.Windows.Forms.CheckBox
    Friend WithEvents chkSaveAsTXT As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowColumnHeader As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnUp As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnDown As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objchkNAll As System.Windows.Forms.CheckBox
    Friend WithEvents elPayrollJournalColumns As eZee.Common.eZeeLine
    Friend WithEvents lvPayrollJournalColumns As eZee.Common.eZeeListView
    Friend WithEvents objcolhNCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPayrollJournalColumns As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboExportMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblExportMode As System.Windows.Forms.Label
    Friend WithEvents lblDateFormat As System.Windows.Forms.Label
    Friend WithEvents txtDateFormat As System.Windows.Forms.TextBox
End Class

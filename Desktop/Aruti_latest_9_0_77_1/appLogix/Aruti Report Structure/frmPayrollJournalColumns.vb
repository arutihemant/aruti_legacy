﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayrollJournalColumns

#Region " Private Variables "

    Private mstrModuleName As String = "frmPayrollJournalColumns"
    Private mblnCancel As Boolean = True
    Private mintReportId As Integer = -1
    Private mblnIsCrystalReport As Boolean = False
    Private mintReportTypeId As Integer = -1
    Private mintExportModeId As Integer = -1
    Private mstrPayrollJournalColumnsIds As String = String.Empty
    Private mblnShowColumnHeader As Boolean = False
    Private objUserDefRMode As clsUserDef_ReportMode
    Private marrPayrollJournalColumnIds As New ArrayList
    Private mblnSaveAsTXT As Boolean = False
    Private mblnTabDelimiter As Boolean = False
#End Region

#Region " Properties "

    Public Property _PayrollJournalColumnsIds() As String
        Get
            Return mstrPayrollJournalColumnsIds
        End Get
        Set(ByVal value As String)
            mstrPayrollJournalColumnsIds = value
        End Set
    End Property

    Public Property _PayrollJournalShowColumnHeader() As Boolean
        Get
            Return mblnShowColumnHeader
        End Get
        Set(ByVal value As Boolean)
            mblnShowColumnHeader = value
        End Set
    End Property

    Public ReadOnly Property _PayrollJournalExportModeId() As Integer
        Get
            Return mintExportModeId
        End Get
    End Property

    Public Property _PayrollJournalSaveAsTXT() As Boolean
        Get
            Return mblnSaveAsTXT
        End Get
        Set(ByVal value As Boolean)
            mblnSaveAsTXT = value
        End Set
    End Property

    Public Property _PayrollJournalTabDelimiter() As Boolean
        Get
            Return mblnTabDelimiter
        End Get
        Set(ByVal value As Boolean)
            mblnTabDelimiter = value
        End Set
    End Property

    'Hemant (13 Jul 2020) -- Start
    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
    Private mstrDateFormat As String = String.Empty
    Public Property _JVDateFormat() As String
        Get
            Return mstrDateFormat
        End Get
        Set(ByVal value As String)
            mstrDateFormat = value
        End Set
    End Property
    'Hemant (13 Jul 2020) -- End


#End Region

#Region " Dispaly Dialog "

    Public Function displayDialog(ByVal intReportUnkid As Integer, ByVal blnIsCrystalReport As Boolean, Optional ByVal intReportTypeUnkid As Integer = -1) As Boolean
        Try
            mintReportId = intReportUnkid
            mintReportTypeId = intReportTypeUnkid
            mblnIsCrystalReport = blnIsCrystalReport
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : displayDialog ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Enum "
    Private Enum enReportMode
        CUSTOM_COLUMNS = 0
        SHOW_REPORT_HEADER = 1
        EXPORT_MODE = 2
        SAVE_AS_TXT = 3
        TAB_DELIMITER = 4
        'Hemant (13 Jul 2020) -- Start
        'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
        DATE_FORMAT = 5
        'Hemant (13 Jul 2020) -- End
    End Enum
#End Region

#Region " Private Methods & Function "

    Private Sub GetValue()
        Dim dsList As DataSet
        Try
            marrPayrollJournalColumnIds.Clear()
            dsList = objUserDefRMode.GetList("List", mintReportId, CInt(cboExportMode.SelectedValue), mintReportTypeId)
            If dsList.Tables("List").Rows.Count > 0 Then
                For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                    If i = enReportMode.CUSTOM_COLUMNS Then 'Column order
                        marrPayrollJournalColumnIds.AddRange(dsList.Tables("List").Rows(i).Item("transactionheadid").ToString.Split(CChar(",")))

                    ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                        chkShowColumnHeader.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                    ElseIf i = enReportMode.EXPORT_MODE Then
                        'If dsList.Tables("List").Rows.Count > 2 Then
                        cboExportMode.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        'Else
                        '    cboExportMode.SelectedValue = 0
                        'End If

                    ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT
                        'If dsList.Tables("List").Rows.Count > 3 Then
                        chkSaveAsTXT.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        'End If

                    ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER
                        'If dsList.Tables("List").Rows.Count > 4 Then
                        chkTABDelimiter.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        'End If

                        'Hemant (13 Jul 2020) -- Start
                        'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                    ElseIf i = enReportMode.DATE_FORMAT Then 'Date Format
                        If dsList.Tables("List").Rows.Count > 5 Then
                            txtDateFormat.Text = CStr(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        Else
                            txtDateFormat.Text = "dd/MM/yyyy"
                        End If
                        'Hemant (13 Jul 2020) -- End

                    End If
                Next
            Else
                chkShowColumnHeader.Checked = False
                chkSaveAsTXT.Checked = False
                chkTABDelimiter.Checked = False
            End If

            Call FillList()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUserDefRMode._Reportmodeid = CInt(cboExportMode.SelectedValue)
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim dtTable As New DataTable
        Dim dsTicked As DataSet = Nothing
        Dim dsUnticked As DataSet = Nothing
        Dim lvItem As ListViewItem

        Try

            lvPayrollJournalColumns.Items.Clear()
            Dim dicTicked As Dictionary(Of Integer, DataRow) = Nothing

            If marrPayrollJournalColumnIds.Count > 0 Then
                dsTicked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport, "ID IN (" & String.Join(",", CType(marrPayrollJournalColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
                dicTicked = (From p In dsTicked.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("Id")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)

                dsUnticked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport, "ID NOT IN (" & String.Join(",", CType(marrPayrollJournalColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
            Else
                dsUnticked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport)
            End If


            If dsTicked IsNot Nothing Then

                Dim dtRow As DataRow = Nothing
                For Each id As Integer In CType(marrPayrollJournalColumnIds.Clone, ArrayList)
                    dtRow = dicTicked.Item(id)

                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = CInt(dtRow.Item("Id"))
                    lvItem.Checked = True

                    lvItem.SubItems.Add(dtRow.Item("NAME").ToString)

                    lvPayrollJournalColumns.Items.Add(lvItem)

                    lvItem = Nothing
                Next

            End If


            If dsUnticked IsNot Nothing Then

                For Each dtRow As DataRow In dsUnticked.Tables(0).Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.Tag = CInt(dtRow.Item("Id"))
                    If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                        If CInt(dtRow.Item("Id")) = enPayrollJournal_Columns.Debit OrElse CInt(dtRow.Item("Id")) = enPayrollJournal_Columns.Credit Then
                            lvItem.Checked = True
                        Else
                            lvItem.Checked = False
                        End If
                    Else
                        lvItem.Checked = False
                    End If

                    lvItem.SubItems.Add(dtRow.Item("NAME").ToString)

                    lvPayrollJournalColumns.Items.Add(lvItem)

                    lvItem = Nothing
                Next

            End If


            If lvPayrollJournalColumns.Items.Count > 18 Then
                colhPayrollJournalColumns.Width = 250 - 18
            Else
                colhPayrollJournalColumns.Width = 250
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            Select Case CInt(cboExportMode.SelectedValue)
                Case enPayrollJournal_Export_Mode.CSV, enPayrollJournal_Export_Mode.XLS, enPayrollJournal_Export_Mode.Crystal_Report
                    lvPayrollJournalColumns.Enabled = True
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetVisibility ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            Select Case CInt(cboExportMode.SelectedValue)
                Case enPayrollJournal_Export_Mode.CSV, enPayrollJournal_Export_Mode.XLS, enPayrollJournal_Export_Mode.Crystal_Report
                    If lvPayrollJournalColumns.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one column to export Payroll Journal report."), enMsgBoxStyle.Information)
                        lvPayrollJournalColumns.Focus()
                        Return False
                    ElseIf CInt(cboExportMode.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Export Mode."), enMsgBoxStyle.Information)
                        cboExportMode.Focus()
                        Return False
                    ElseIf CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report AndAlso lvPayrollJournalColumns.CheckedItems.Count < 4 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select atleast minimum 4 columns to export Payroll Journal report."), enMsgBoxStyle.Information)
                        lvPayrollJournalColumns.Focus()
                        Return False
                    End If
            End Select

            mstrPayrollJournalColumnsIds = String.Join(",", (From lv In lvPayrollJournalColumns.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            Dim strPayrollJournalColumnsId() As String = Split(mstrPayrollJournalColumnsIds, ",")
            If strPayrollJournalColumnsId.Contains(CInt(enPayrollJournal_Columns.Posting_Date).ToString) = True AndAlso txtDateFormat.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Date Format."), enMsgBoxStyle.Information)
                txtDateFormat.Focus()
                Return False
            End If
            'Hemant (13 Jul 2020) -- End
            mblnShowColumnHeader = chkShowColumnHeader.Checked
            mintExportModeId = CInt(cboExportMode.SelectedValue)
            mblnSaveAsTXT = chkSaveAsTXT.Checked
            mblnTabDelimiter = chkTABDelimiter.Checked
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            mstrDateFormat = txtDateFormat.Text
            'Hemant (13 Jul 2020) -- End

            Call SetValue()

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.GetComboListForPayrollJournalExportMode("ExportMode", False, mblnIsCrystalReport, Not mblnIsCrystalReport)
            With cboExportMode
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("ExportMode")
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPayrollJournalColumns_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objUserDefRMode = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPayrollJournalColumnst_FormClosed ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPayrollJournalColumns_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objUserDefRMode = New clsUserDef_ReportMode
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
           
            objUserDefRMode._Reportmodeid = CInt(cboExportMode.SelectedValue)
            objUserDefRMode._Reporttypeid = mintReportTypeId
            objUserDefRMode._Reportunkid = mintReportId
            Call SetVisibility()
            Call FillCombo()
            'Call GetValue()

            Call FillList()
            'Call GetValue()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : frmPayrollJournalColumns_Load ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPayrollJournalColumns_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMasterData.SetMessages()
            objfrm._Other_ModuleNames = "clsMasterData"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnClose_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If IsValidate() = False Then Exit Sub

            mblnCancel = False

            Me.Close()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnOk_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Try

            If IsValidate() = False Then Exit Sub

            For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()

                objUserDefRMode = New clsUserDef_ReportMode

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = mintReportId

                objUserDefRMode._Reporttypeid = mintReportTypeId

                objUserDefRMode._Reportmodeid = CInt(cboExportMode.SelectedValue)

                If i = enReportMode.CUSTOM_COLUMNS Then 'Custom Columns


                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrPayrollJournalColumnsIds

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnShowColumnHeader.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.EXPORT_MODE Then

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = CInt(cboExportMode.SelectedValue).ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnSaveAsTXT.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnTabDelimiter.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)

                    'Hemant (13 Jul 2020) -- Start
                    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                ElseIf i = enReportMode.DATE_FORMAT Then 'Date Format

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrDateFormat.ToString

                    intUnkid = objUserDefRMode.isExist(mintReportId, mintReportTypeId, CInt(cboExportMode.SelectedValue), i)
                    'Hemant (13 Jul 2020) -- End

                End If

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If


            Next


            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Information Successfully Saved."), enMsgBoxStyle.Information)

            mblnCancel = False

            Me.Close()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : btnSaveSelection_Click ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUp.Click
        Try
            If lvPayrollJournalColumns.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvPayrollJournalColumns.SelectedIndices.Item(0)
                If SelIndex = 0 Then Exit Sub
                Dim lvPrevItem As ListViewItem
                lvPrevItem = lvPayrollJournalColumns.Items(SelIndex - 1)
                lvPayrollJournalColumns.Items(SelIndex - 1) = CType(lvPayrollJournalColumns.Items(SelIndex).Clone, ListViewItem)
                lvPayrollJournalColumns.Items(SelIndex) = CType(lvPrevItem.Clone, ListViewItem)
                lvPayrollJournalColumns.Items(SelIndex - 1).Selected = True
                lvPayrollJournalColumns.Items(SelIndex - 1).EnsureVisible()
            End If
            lvPayrollJournalColumns.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDown.Click
        Try
            If lvPayrollJournalColumns.SelectedItems.Count > 0 Then
                Dim SelIndex As Integer = lvPayrollJournalColumns.SelectedIndices.Item(0)
                If SelIndex = lvPayrollJournalColumns.Items.Count - 1 Then Exit Sub
                Dim lvPrevItem As ListViewItem
                lvPrevItem = lvPayrollJournalColumns.Items(SelIndex + 1)
                lvPayrollJournalColumns.Items(SelIndex + 1) = CType(lvPayrollJournalColumns.Items(SelIndex).Clone, ListViewItem)
                lvPayrollJournalColumns.Items(SelIndex) = CType(lvPrevItem.Clone, ListViewItem)
                lvPayrollJournalColumns.Items(SelIndex + 1).Selected = True
                lvPayrollJournalColumns.Items(SelIndex + 1).EnsureVisible()
            End If
            lvPayrollJournalColumns.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub
#End Region

#Region " Combobox Events "
    Private Sub cboExportMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExportMode.SelectedIndexChanged
        Try
            chkShowColumnHeader.Visible = True
            If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.XLS OrElse CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                chkSaveAsTXT.Visible = False
                chkSaveAsTXT.Checked = False
                chkTABDelimiter.Visible = False
                chkTABDelimiter.Checked = False

                If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                    chkShowColumnHeader.Visible = False
                    chkShowColumnHeader.Checked = False
                End If
            Else
                chkSaveAsTXT.Visible = True
                chkTABDelimiter.Visible = True
            End If
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExportMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Contols "

    Private Sub objchkNAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNAll.CheckedChanged
        Try
            RemoveHandler lvPayrollJournalColumns.ItemChecked, AddressOf lvPayrollJournalColumns_ItemChecked
            For Each lvItem As ListViewItem In lvPayrollJournalColumns.Items
                lvItem.Checked = CBool(objchkNAll.CheckState)
            Next

            marrPayrollJournalColumnIds.Clear()
            marrPayrollJournalColumnIds.AddRange((From p In lvPayrollJournalColumns.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            If objchkNAll.Checked = True Then
                txtDateFormat.Enabled = True
            Else
                txtDateFormat.Text = "dd/MM/yyyy"
                txtDateFormat.Enabled = False
            End If
            'Hemant (13 Jul 2020) -- End

            AddHandler lvPayrollJournalColumns.ItemChecked, AddressOf lvPayrollJournalColumns_ItemChecked
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : objchkNAll_CheckedChanged ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPayrollJournalColumns_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvPayrollJournalColumns.ItemCheck
        Try
            If mblnIsCrystalReport = True Then
                If CInt(lvPayrollJournalColumns.Items(e.Index).Tag) = enPayrollJournal_Columns.Debit OrElse CInt(lvPayrollJournalColumns.Items(e.Index).Tag) = enPayrollJournal_Columns.Credit Then
                    e.NewValue = CheckState.Checked
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayrollJournalColumns_ItemCheck", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPayrollJournalColumns_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPayrollJournalColumns.ItemChecked
        Try
            RemoveHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
            If lvPayrollJournalColumns.CheckedItems.Count <= 0 Then
                objchkNAll.CheckState = CheckState.Unchecked
            ElseIf lvPayrollJournalColumns.CheckedItems.Count < lvPayrollJournalColumns.Items.Count Then
                objchkNAll.CheckState = CheckState.Indeterminate
            ElseIf lvPayrollJournalColumns.CheckedItems.Count = lvPayrollJournalColumns.Items.Count Then
                objchkNAll.CheckState = CheckState.Checked
            End If

            marrPayrollJournalColumnIds.Clear()
            marrPayrollJournalColumnIds.AddRange((From p In lvPayrollJournalColumns.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)

            'If CInt(e.Item.Tag) = enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO Then
            '    If e.Item.Checked = True Then
            '        cboMembership.Enabled = True
            '    Else
            '        cboMembership.SelectedValue = 0
            '        cboMembership.Enabled = False
            '    End If
            'End If

            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            If CInt(e.Item.Tag) = enPayrollJournal_Columns.Posting_Date Then
                If e.Item.Checked = True Then
                    txtDateFormat.Enabled = True
                Else
                    txtDateFormat.Text = "dd/MM/yyyy"
                    txtDateFormat.Enabled = False
                End If
            End If
            'Hemant (13 Jul 2020) -- End
            

            AddHandler objchkNAll.CheckedChanged, AddressOf objchkNAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayrollJournalColumns_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objbtnUp.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnUp.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnDown.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnDown.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.chkTABDelimiter.Text = Language._Object.getCaption(Me.chkTABDelimiter.Name, Me.chkTABDelimiter.Text)
			Me.chkSaveAsTXT.Text = Language._Object.getCaption(Me.chkSaveAsTXT.Name, Me.chkSaveAsTXT.Text)
			Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.Name, Me.chkShowColumnHeader.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.elPayrollJournalColumns.Text = Language._Object.getCaption(Me.elPayrollJournalColumns.Name, Me.elPayrollJournalColumns.Text)
			Me.colhPayrollJournalColumns.Text = Language._Object.getCaption(CStr(Me.colhPayrollJournalColumns.Tag), Me.colhPayrollJournalColumns.Text)
            Me.lblExportMode.Text = Language._Object.getCaption(Me.lblExportMode.Name, Me.lblExportMode.Text)
			Me.lblDateFormat.Text = Language._Object.getCaption(Me.lblDateFormat.Name, Me.lblDateFormat.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one column to export Payroll Journal report.")
			Language.setMessage(mstrModuleName, 2, "Information Successfully Saved.")
			Language.setMessage(mstrModuleName, 3, "Please Select Export Mode.")
			Language.setMessage(mstrModuleName, 4, "Please Select atleast minimum 4 columns to export Payroll Journal report.")
			Language.setMessage(mstrModuleName, 5, "Please Enter Date Format.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetDefaultView
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetDefaultView))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.rabPrint = New System.Windows.Forms.RadioButton
        Me.rabPreview = New System.Windows.Forms.RadioButton
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.SystemColors.Control
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.rabPrint)
        Me.pnlMain.Controls.Add(Me.rabPreview)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(261, 218)
        Me.pnlMain.TabIndex = 1
        '
        'objefFooter
        '
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnOK)
        Me.objefFooter.Controls.Add(Me.btnCancel)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.Color.Empty
        Me.objefFooter.GradientColor2 = System.Drawing.Color.Empty
        Me.objefFooter.Location = New System.Drawing.Point(0, 163)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(261, 55)
        Me.objefFooter.TabIndex = 5
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(65, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(90, 30)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "&Set"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(161, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'rabPrint
        '
        Me.rabPrint.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPrint.Image = Global.Aruti.Data.My.Resources.Resources.Print_48
        Me.rabPrint.Location = New System.Drawing.Point(28, 29)
        Me.rabPrint.Name = "rabPrint"
        Me.rabPrint.Size = New System.Drawing.Size(91, 110)
        Me.rabPrint.TabIndex = 0
        Me.rabPrint.Text = "&Print"
        Me.rabPrint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabPrint.UseVisualStyleBackColor = True
        '
        'rabPreview
        '
        Me.rabPreview.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPreview.Image = Global.Aruti.Data.My.Resources.Resources.PrintPreview_48
        Me.rabPreview.Location = New System.Drawing.Point(142, 29)
        Me.rabPreview.Name = "rabPreview"
        Me.rabPreview.Size = New System.Drawing.Size(91, 110)
        Me.rabPreview.TabIndex = 1
        Me.rabPreview.Text = "Print Previe&w"
        Me.rabPreview.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabPreview.UseVisualStyleBackColor = True
        '
        'frmSetDefaultView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(261, 218)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSetDefaultView"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Set Default View"
        Me.pnlMain.ResumeLayout(False)
        Me.objefFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents rabPrint As System.Windows.Forms.RadioButton
    Friend WithEvents rabPreview As System.Windows.Forms.RadioButton
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
End Class

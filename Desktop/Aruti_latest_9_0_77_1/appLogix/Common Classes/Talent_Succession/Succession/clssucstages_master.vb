﻿'************************************************************************************************************************************
'Class Name : clsscstages_master.vb
'Purpose    :
'Date       :30-Oct-2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clssucstages_master
    Private Shared ReadOnly mstrModuleName As String = "clsscstages_master"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStageunkid As Integer
    Private mstrStage_Name As String = String.Empty
    Private mblnIsdefault As Boolean
    Private mintFloworder As Integer
    Private mintApplyOnStageunkid As Integer
    Private mintApplyOnFloworder As Integer
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stageunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stageunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintStageunkid
        End Get
        Set(ByVal value As Integer)
            mintStageunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set stage_name
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stage_Name() As String
        Get
            Return mstrStage_Name
        End Get
        Set(ByVal value As String)
            mstrStage_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdefault
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isdefault() As Boolean
        Get
            Return mblnIsdefault
        End Get
        Set(ByVal value As Boolean)
            mblnIsdefault = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set floworder
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Floworder() As Integer
        Get
            Return mintFloworder
        End Get
        Set(ByVal value As Integer)
            mintFloworder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApplyOnstageunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ApplyOnStageunkid() As Integer
        Get
            Return mintApplyOnStageunkid
        End Get
        Set(ByVal value As Integer)
            mintApplyOnStageunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set floworder
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _ApplyOnFloworder() As Integer
        Get
            Return mintApplyOnFloworder
        End Get
        Set(ByVal value As Integer)
            mintApplyOnFloworder = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  stageunkid " & _
              ", stage_name " & _
              ", isdefault " & _
              ", floworder " & _
              ", ISNULL(default_name,'') AS default_name " & _
              ", isactive " & _
             "FROM " & mstrDatabaseName & "..sucstages_master " & _
             "WHERE stageunkid = @stageunkid "

            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStageunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStageunkid = CInt(dtRow.Item("stageunkid"))
                mstrStage_Name = dtRow.Item("stage_name").ToString
                mblnIsdefault = CBool(dtRow.Item("isdefault"))
                mintFloworder = CInt(dtRow.Item("floworder"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  stageunkid " & _
              ", ISNULL(stage_name, '') AS StageName " & _
              ", isdefault " & _
              ", floworder " & _
              ", ISNULL(default_name,'') AS default_name " & _
              ", sucstages_master.isactive " & _
             "FROM " & mstrDatabaseName & "..sucstages_master " & _
             " WHERE sucstages_master.isactive = 1 "

            strQ &= " ORDER BY floworder "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sucstages_master) </purpose>
    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal dtStages As DataTable = Nothing) As Boolean
        If isExist(mstrStage_Name, -1, objDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This stage is already defined. Please define new stage.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strDtFilter As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()

        If Not dtStages Is Nothing Then
        Select Case mintFloworder
            Case 1
                mintFloworder = mintApplyOnFloworder
                strDtFilter = "floworder >= " & mintApplyOnFloworder
            Case 2
                mintFloworder = mintApplyOnFloworder + 1
                strDtFilter = "floworder > " & mintApplyOnFloworder
            Case Else
                mintFloworder = mintApplyOnFloworder
                strDtFilter = "floworder >= " & mintApplyOnFloworder
        End Select
        End If

        Try
            objDataOperation.AddParameter("@stage_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStage_Name.ToString)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdefault.ToString)
            objDataOperation.AddParameter("@floworder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFloworder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            If mblnIsdefault Then
                objDataOperation.AddParameter("@default_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStage_Name)
            Else
                objDataOperation.AddParameter("@default_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
            End If


            strQ = "INSERT INTO " & mstrDatabaseName & "..sucstages_master ( " & _
              " stage_name " & _
              ", isdefault " & _
              ", floworder " & _
              ", default_name " & _
              ", isactive" & _
            ") VALUES (" & _
              " @stage_name " & _
              ", @isdefault " & _
              ", @floworder " & _
              ", @default_name " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStageunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Not dtStages Is Nothing Then
            For Each drRow As DataRow In dtStages.Select(strDtFilter)

                strQ = "UPDATE sucstages_master " & _
                " SET floworder = " & CInt(drRow.Item("floworder")) + 1 & _
                " WHERE stageunkid = " & drRow.Item("stageunkid")

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            End If

            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (sucstages_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrStage_Name, mintStageunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This stage is already defined. Please define new stage.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStageunkid.ToString)
            objDataOperation.AddParameter("@stage_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStage_Name.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..sucstages_master SET " & _
              " stage_name = @stage_name" & _
              ", isactive = @isactive " & _
            "WHERE stageunkid = @stageunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (sucstages_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal dtStages As DataTable = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Stageunkid(xDataOpr) = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..sucstages_master SET " & _
                   " isactive = 0 " & _
                   "WHERE stageunkid = @stageunkid "

            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Not dtStages Is Nothing Then
            For Each drRow As DataRow In dtStages.Select(" floworder > " & mintApplyOnFloworder)

                strQ = "UPDATE sucstages_master " & _
                " SET floworder = " & CInt(drRow.Item("floworder")) - 1 & _
                " WHERE stageunkid = " & drRow.Item("stageunkid")

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            End If


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strStage As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  stageunkid " & _
                   ", stage_name " & _
                   ", isdefault " & _
                   ", floworder " & _
                   ", isactive " & _
                   "FROM " & mstrDatabaseName & "..sucstages_master " & _
                   "WHERE isactive = 1 " & _
                   " AND stage_name = @stage_name "

            If intUnkid > 0 Then
                strQ &= " AND stageunkid <> @stageunkid"
            End If

            objDataOperation.AddParameter("@stage_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strStage)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO " & mstrDatabaseName & "..atsucstages_master ( " & _
                    "  tranguid " & _
                    ", stageunkid " & _
                    ", stage_name " & _
                    ", isdefault " & _
                    ", floworder " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @stageunkid " & _
                    ", @stage_name " & _
                    ", @isdefault " & _
                    ", @floworder " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStageunkid.ToString)
            objDataOperation.AddParameter("@stage_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStage_Name.ToString)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdefault.ToString)
            objDataOperation.AddParameter("@floworder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFloworder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function FlowOrder(ByVal strList As String, Optional ByVal blnSelect As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If blnSelect Then
                StrQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If

            StrQ &= "SELECT 1 AS Id, @BF AS Name UNION ALL " & _
                    "SELECT 2 AS Id, @AF AS Name "

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            objDataOperation.AddParameter("@BF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Before"))
            objDataOperation.AddParameter("@AF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "After"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FlowOrder; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Gajanan [17-Dec-2020] -- Start
    Public Function getComboList(ByVal strList As String, Optional ByVal blnSelect As Boolean = False, Optional ByVal blnExcludeMaxMinStage As Boolean = False) As DataSet
    'Gajanan [17-Dec-2020] -- End
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If blnSelect Then
                StrQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If
            StrQ &= "SELECT " & _
                    "  stageunkid AS Id " & _
                    ", stage_name AS Name " & _
                    "FROM " & mstrDatabaseName & "..sucstages_master " & _
                    "WHERE isactive = 1 "
    'Gajanan [17-Dec-2020] -- Start
            If blnExcludeMaxMinStage = True Then
                StrQ &= " AND floworder NOT IN (SELECT MAX(floworder) From sucstages_master)  "
                StrQ &= " AND floworder NOT IN (SELECT MIN(floworder) From sucstages_master)  "
            End If
    'Gajanan [17-Dec-2020] -- End

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FlowOrder; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Gajanan [17-Dec-2020] -- Start
    Public Sub Get_Min_Max_FlowOrder(ByRef intMinFlowOrder As Integer, ByRef intMaxFlowOrder As Integer, ByRef intMaxToLastFlowOrder As Integer)
    'Gajanan [17-Dec-2020] -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception : Dim dsList As DataSet = Nothing
        Dim objDataOperation As New clsDataOperation
        Try
    'Gajanan [17-Dec-2020] -- Add maxtolastorder

            StrQ = "SELECT " & _
                     " MIN(floworder) AS minfloworder " & _
                      "  ,MAX(floworder) as maxfloworder " & _
                      "  ,MAX(floworder)-1 as maxtolastorder " & _
                      "   FROM " & mstrDatabaseName & "..sucstages_master " & _
                      "   WHERE isactive = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intMinFlowOrder = Convert.ToInt32(dsList.Tables(0).Rows(0)("minfloworder"))
                intMaxFlowOrder = Convert.ToInt32(dsList.Tables(0).Rows(0)("maxfloworder"))
                'Gajanan [17-Dec-2020] -- Start
                intMaxToLastFlowOrder = Convert.ToInt32(dsList.Tables(0).Rows(0)("maxtolastorder"))
                'Gajanan [17-Dec-2020] -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Min_Max_FlowOrder; Module Name: " & mstrModuleName)
        End Try

    End Sub


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This stage is already defined. Please define new stage.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Before")
			Language.setMessage(mstrModuleName, 4, "After")
			Language.setMessage(mstrModuleName, 5, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

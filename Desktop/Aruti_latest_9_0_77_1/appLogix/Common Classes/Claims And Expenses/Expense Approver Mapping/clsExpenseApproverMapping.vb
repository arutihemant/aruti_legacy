﻿'************************************************************************************************************************************
'Class Name :clsExpenseApproverMapping.vb
'Purpose    :
'Date       :07-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports System

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExpenseApproverMapping
    Private Shared ReadOnly mstrModuleName As String = "clsExpenseApproverMapping"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintExMappingTranId As Integer
    Private mintExpenseTypeId As Integer
    Private mintExApproverUnkid As Integer
    Private mintUserId As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    Public Property _ExpenseTypeId() As Integer
        Get
            Return mintExpenseTypeId
        End Get
        Set(ByVal value As Integer)
            mintExpenseTypeId = value
        End Set
    End Property

    Public Property _ExApproverUnkid() As Integer
        Get
            Return mintExApproverUnkid
        End Get
        Set(ByVal value As Integer)
            mintExApproverUnkid = value
            Call Get_Data()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List")
            mdtTran.Columns.Add("isassigned", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("expensemappingunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("crapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("expenseunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("UoM", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("ischanged", System.Type.GetType("System.Boolean")).DefaultValue = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "

    Private Sub Get_Data()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "	 CASE WHEN cmapprover_expense_mapping.expenseunkid  = cmexpense_master.expenseunkid THEN CAST(1 AS bit) " & _
                   "		ELSE CAST(0 AS bit) END AS isassigned " & _
                   "	,cmexpense_master.expenseunkid " & _
                   "	,ISNULL(cmapprover_expense_mapping.expensemappingunkid,0) AS expensemappingunkid " & _
                   "	,ISNULL(cmapprover_expense_mapping.crapproverunkid,0) AS crapproverunkid " & _
                   "	,ISNULL(cmapprover_expense_mapping.isvoid,0) AS isvoid " & _
                   "	,ISNULL(cmapprover_expense_mapping.voiduserunkid,0) AS voiduserunkid " & _
                   "	,ISNULL(cmapprover_expense_mapping.voiddatetime,NULL) AS voiddatetime " & _
                   "	,ISNULL(cmapprover_expense_mapping.voidreason,'') AS voidreason " & _
                   "	,code " & _
                   "	,name " & _
                   "    , CASE WHEN uomunkid = 1 THEN @Qty " & _
                   "           WHEN uomunkid = 2 THEN @Amt END AS UoM " & _
                   "	,'' AS AUD " & _
                   "	,CAST(0 AS bit) AS ischanged " & _
                   "FROM cmexpense_master " & _
                   "	LEFT JOIN cmapprover_expense_mapping ON cmexpense_master.expenseunkid = cmapprover_expense_mapping.expenseunkid AND cmapprover_expense_mapping.isvoid = 0 " & _
                   "        AND cmapprover_expense_mapping.crapproverunkid = '" & mintExApproverUnkid & "' " & _
                   "	LEFT JOIN cmexpapprover_master ON cmapprover_expense_mapping.crapproverunkid = cmexpapprover_master.crapproverunkid " & _
                   "		AND cmapprover_expense_mapping.crapproverunkid = '" & mintExApproverUnkid & "' " & _
                   "WHERE cmexpense_master.expensetypeid = '" & mintExpenseTypeId & "' AND isactive = 1 "

            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO cmapprover_expense_mapping ( " & _
                                            "  crapproverunkid " & _
                                            ", expenseunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                        ") VALUES (" & _
                                            "  @crapproverunkid " & _
                                            ", @expenseunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                        "); SELECT @@identity"

                                objData.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExApproverUnkid)
                                objData.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                mintExMappingTranId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("crapproverunkid") > 0 Then
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmapprover_expense_mapping", "expensemappingunkid", mintExMappingTranId, 2, 1, , mintUserId) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                Else
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", mintExApproverUnkid, "cmapprover_expense_mapping", "expensemappingunkid", mintExMappingTranId, 2, 1, , mintUserId) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            Case "U"

                                StrQ = "UPDATE cmapprover_expense_mapping SET " & _
                                       "  crapproverunkid = @crapproverunkid" & _
                                       ", expenseunkid = @expenseunkid" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE expensemappingunkid = @expensemappingunkid "

                                objData.AddParameter("@expensemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expensemappingunkid"))
                                objData.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crapproverunkid"))
                                objData.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))


                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmapprover_expense_mapping", "expensemappingunkid", .Item("expensemappingunkid"), 2, 2, , mintUserId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END


                            Case "D"
                                If .Item("expensemappingunkid") > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmapprover_expense_mapping", "expensemappingunkid", .Item("expensemappingunkid"), 2, 3, , mintUserId) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                                StrQ = "UPDATE cmapprover_expense_mapping SET " & _
                                       "  isvoid = @isvoid " & _
                                       ", voiddatetime = @voiddatetime " & _
                                       ", voidreason = @voidreason " & _
                                       ", voiduserunkid = @voiduserunkid " & _
                                       "WHERE expensemappingunkid = @expensemappingunkid "

                                objData.AddParameter("@expensemappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expensemappingunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Delete", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsExpCommonMethods", 6, "Quantity")
			Language.setMessage("clsExpCommonMethods", 7, "Amount")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

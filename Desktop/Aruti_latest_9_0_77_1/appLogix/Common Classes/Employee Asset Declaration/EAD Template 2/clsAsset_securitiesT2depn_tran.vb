﻿

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsAsset_securitiesT2depn_tran
    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Private Const mstrModuleName = "clsAsset_businessdealT2depn_tran"
    Private Const mstrModuleName = "clsAsset_securitiesT2depn_tran"
    'Hemant (07 Dec 2018) -- End
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintassetsecuritiest2depntranunkid As Integer
    Private mintassetdeclarationt2unkid As Integer
    Private mintrelationshipunkid As Integer = 0
    Private mintsecuritiestypeunkid As Integer = 0
    Private mstrcertificate_no As String = String.Empty
    Private mintno_of_shares As Integer = 0
    Private mstrcompany_business_name As String = String.Empty
    Private mintcountryunkid As Integer = 0
    Private mintcurrencyunkid As Integer = 0
    Private mintbasecurrencyunkid As Integer = 0
    Private mdecbaseexchangerate As Decimal = 0
    Private mdecexchangerate As Decimal = 0
    Private mdecmarket_value As Decimal = 0
    Private mdecbasemarket_value As Decimal = 0
    Private mdtacquisition_date As DateTime
    Private mblnisfinalsaved As Boolean = False
    Private mdttransactiondate As DateTime
    Private mintuserunkid As Integer = 0
    Private mblnisvoid As Boolean = False
    Private mintvoiduserunkid As Integer = 0
    Private mdtvoiddatetime As DateTime
    Private mstrvoidreason As String = String.Empty

    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minClientIp As String = String.Empty
    Private minloginemployeeunkid As Integer
    Private mstrHostName As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = ""
    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
    Private mintDependentunkid As Integer
    'Hemant (03 Dec 2018) -- End
#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Assetsecuritiest2depntranunkid() As Integer
        Get
            Return mintassetsecuritiest2depntranunkid
        End Get
        Set(ByVal value As Integer)
            mintassetsecuritiest2depntranunkid = value
        End Set
    End Property

    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintassetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintassetdeclarationt2unkid = value
            Call GetData(xDatabase)
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set relationshipunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Relationshipunkid() As Integer
        Get
            Return mintRelationshipunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationshipunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set securitiestypeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Securitiestypeunkid() As Integer
        Get
            Return mintSecuritiestypeunkid
        End Get
        Set(ByVal value As Integer)
            mintSecuritiestypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set certificate_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Certificate_No() As String
        Get
            Return mstrCertificate_No
        End Get
        Set(ByVal value As String)
            mstrCertificate_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set no_of_shares
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _No_Of_Shares() As Integer
        Get
            Return mintNo_Of_Shares
        End Get
        Set(ByVal value As Integer)
            mintNo_Of_Shares = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_business_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company_Business_Name() As String
        Get
            Return mstrCompany_Business_Name
        End Get
        Set(ByVal value As String)
            mstrCompany_Business_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basecurrencyunkid() As Integer
        Get
            Return mintBasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecbaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecbaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Exchangerate() As Decimal
        Get
            Return mdecexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set market_value
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Market_Value() As Decimal
        Get
            Return mdecmarket_value
        End Get
        Set(ByVal value As Decimal)
            mdecmarket_value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basemarket_value
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basemarket_Value() As Decimal
        Get
            Return mdecbasemarket_value
        End Get
        Set(ByVal value As Decimal)
            mdecbasemarket_value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set acquisition_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Acquisition_Date() As Date
        Get
            Return mdtAcquisition_Date
        End Get
        Set(ByVal value As Date)
            mdtAcquisition_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Dependantunkid() As Integer
        Get
            Return mintDependentunkid
        End Get
        Set(ByVal value As Integer)
            mintDependentunkid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintuserunkid
        End Get
        Set(ByVal value As Integer)
            mintuserunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()

        'Hemant (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        'mdtTable = New DataTable("BusinessdealT2depn")
        mdtTable = New DataTable("Securitiesdepn")
        'Hemant (07 Dec 2018) -- End

        Try
            mdtTable.Columns.Add("assetsecuritiest2depntranunkid", System.Type.GetType("System.Int32")).DefaultValue = -2
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -2
            mdtTable.Columns.Add("relationshipunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("securitiestypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("certificate_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("no_of_shares", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("company_business_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("basecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("exchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("market_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("basemarket_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("acquisition_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            mdtTable.Columns.Add("dependantunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Hemant (03 Dec 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub GetData(ByVal xDatabase As String)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation

        mstrDatabaseName = xDatabase


        Try
            strQ = "SELECT " & _
              "  assetsecuritiest2depntranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", ISNULL(relationshipunkid, 0) as relationshipunkid" & _
              ", ISNULL(securitiestypeunkid, 0) as securitiestypeunkid" & _
              ", ISNULL(certificate_no, '') as certificate_no " & _
              ", ISNULL(no_of_shares, 0) as no_of_shares " & _
              ", ISNULL(company_business_name, 0) as company_business_name " & _
              ", ISNULL(countryunkid, 0) as countryunkid " & _
              ", ISNULL(currencyunkid, 0) as currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
              ", ISNULL(exchangerate, 0) as exchangerate " & _
              ", ISNULL(market_value, 0) as market_value " & _
              ", ISNULL(basemarket_value, 0) as basemarket_value " & _
              ", ISNULL(acquisition_date, Getdate()) AS acquisition_date  " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(dependantunkid, 0) as dependantunkid " & _
             "FROM " & mstrDatabaseName & "..hrasset_securitiesT2depn_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (03 Dec 2018) -- [dependantunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()

                drRow.Item("assetsecuritiest2depntranunkid") = CInt(dtRow.Item("assetsecuritiest2depntranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("relationshipunkid") = CInt(dtRow.Item("relationshipunkid"))
                drRow.Item("securitiestypeunkid") = CInt(dtRow.Item("securitiestypeunkid"))
                drRow.Item("certificate_no") = dtRow.Item("certificate_no").ToString
                drRow.Item("no_of_shares") = CInt(dtRow.Item("no_of_shares"))
                drRow.Item("company_business_name") = dtRow.Item("company_business_name").ToString
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("market_value") = CDec(dtRow.Item("market_value"))
                drRow.Item("basemarket_value") = CDec(dtRow.Item("basemarket_value"))
                If IsDBNull(dtRow.Item("acquisition_date")) = True Then
                    drRow.Item("acquisition_date") = DBNull.Value
                Else
                    drRow.Item("acquisition_date") = dtRow.Item("acquisition_date")
                End If
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    drRow.Item("voiddatetime") = DBNull.Value
                Else
                    drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                End If
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                drRow.Item("dependantunkid") = CInt(dtRow.Item("dependantunkid"))
                'Hemant (03 Dec 2018) -- End
                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End


        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        mintassetdeclarationt2unkid = intAssetdeclarationt2Unkid
        'Gajanan (14 Nov 2018) -- End
        mstrDatabaseName = xDatabase
        Try
            strQ = "SELECT " & _
              "  assetsecuritiest2depntranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", ISNULL(relationshipunkid, 0) as relationshipunkid" & _
              ", ISNULL(securitiestypeunkid, 0) as securitiestypeunkid" & _
              ", ISNULL(certificate_no, '') as certificate_no " & _
              ", ISNULL(no_of_shares, 0) as no_of_shares " & _
              ", ISNULL(company_business_name, 0) as company_business_name " & _
              ", ISNULL(countryunkid, 0) as countryunkid " & _
              ", ISNULL(currencyunkid, 0) as currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
              ", ISNULL(exchangerate, 0) as exchangerate " & _
              ", ISNULL(market_value, 0) as market_value " & _
              ", ISNULL(basemarket_value, 0) as basemarket_value " & _
              ", ISNULL(acquisition_date, Getdate()) AS acquisition_date  " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(dependantunkid, 0) as dependantunkid " & _
             "FROM " & mstrDatabaseName & "..hrasset_securitiesT2depn_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (03 Dec 2018) -- [dependantunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()

                drRow.Item("assetsecuritiest2depntranunkid") = CInt(dtRow.Item("assetsecuritiest2depntranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("relationshipunkid") = CInt(dtRow.Item("relationshipunkid"))
                drRow.Item("securitiestypeunkid") = CInt(dtRow.Item("securitiestypeunkid"))
                drRow.Item("certificate_no") = dtRow.Item("certificate_no").ToString
                drRow.Item("no_of_shares") = CInt(dtRow.Item("no_of_shares"))
                drRow.Item("company_business_name") = dtRow.Item("company_business_name").ToString
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("market_value") = CDec(dtRow.Item("market_value"))
                drRow.Item("basemarket_value") = CDec(dtRow.Item("basemarket_value"))
                If IsDBNull(dtRow.Item("acquisition_date")) = True Then
                    drRow.Item("acquisition_date") = DBNull.Value
                Else
                    drRow.Item("acquisition_date") = dtRow.Item("acquisition_date")
                End If
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    drRow.Item("voiddatetime") = DBNull.Value
                Else
                    drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                End If
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                drRow.Item("dependantunkid") = CInt(dtRow.Item("dependantunkid"))
                'Hemant (03 Dec 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Sub
    'Gajanan (14 Nov 2018) -- End

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetSecuritieDeptT2tranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
             "  assetsecuritiest2depntranunkid " & _
             ", assetdeclarationt2unkid " & _
             ", ISNULL(relationshipunkid, 0) as relationshipunkid" & _
             ", ISNULL(securitiestypeunkid, 0) as securitiestypeunkid" & _
             ", ISNULL(certificate_no, '') as certificate_no " & _
             ", ISNULL(no_of_shares, 0) as no_of_shares " & _
             ", ISNULL(company_business_name, 0) as company_business_name " & _
             ", ISNULL(countryunkid, 0) as countryunkid " & _
             ", ISNULL(currencyunkid, 0) as currencyunkid " & _
             ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
             ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
             ", ISNULL(exchangerate, 0) as exchangerate " & _
             ", ISNULL(market_value, 0) as market_value " & _
             ", ISNULL(basemarket_value, 0) as basemarket_value " & _
             ", ISNULL(acquisition_date, Getdate()) AS acquisition_date  " & _
             ", isfinalsaved " & _
             ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
             ", userunkid " & _
             ", isvoid " & _
             ", voiduserunkid " & _
             ", voiddatetime " & _
             ", voidreason " & _
             ", '' AS AUD " & _
             ", ISNULL(dependantunkid, 0) as dependantunkid " & _
            "FROM " & mstrDatabaseName & "..hrasset_securitiesT2depn_tran " & _
            "WHERE assetsecuritiest2depntranunkid = @assetsecuritiest2depntranunkid "

            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetSecuritieDeptT2tranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows

                mintassetsecuritiest2depntranunkid = CInt(dtRow.Item("assetsecuritiest2depntranunkid"))
                mintassetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mintrelationshipunkid = CInt(dtRow.Item("relationshipunkid"))
                mintsecuritiestypeunkid = CInt(dtRow.Item("securitiestypeunkid"))
                mstrcertificate_no = dtRow.Item("certificate_no").ToString
                mintno_of_shares = CInt(dtRow.Item("no_of_shares"))
                mstrcompany_business_name = dtRow.Item("company_business_name").ToString
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintcurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintbasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecbaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecexchangerate = CDec(dtRow.Item("exchangerate"))
                mdecmarket_value = CDec(dtRow.Item("market_value"))
                mdecbasemarket_value = CDec(dtRow.Item("basemarket_value"))
                If IsDBNull(dtRow.Item("acquisition_date")) = True Then
                    mdtacquisition_date = Nothing
                Else
                    mdtacquisition_date = dtRow.Item("acquisition_date")
                End If
                mblnisfinalsaved = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdttransactiondate = Nothing
                Else
                    mdttransactiondate = dtRow.Item("transactiondate")
                End If
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtvoiddatetime = Nothing
                Else
                    mdtvoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString

                mintDependentunkid = CInt(dtRow.Item("dependantunkid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByVal xCurrentDatetTime As DateTime) As Boolean
    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Gajanan (14 Nov 2018) -- End

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Gajanan (14 Nov 2018) -- End

            For Each dtRow As DataRow In mdtTable.Rows
                mintassetsecuritiest2depntranunkid = CInt(dtRow.Item("assetsecuritiest2depntranunkid"))
                mintassetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mintrelationshipunkid = CInt(dtRow.Item("relationshipunkid"))
                mintsecuritiestypeunkid = CInt(dtRow.Item("securitiestypeunkid"))
                mstrcertificate_no = dtRow.Item("certificate_no")
                mintno_of_shares = CInt(dtRow.Item("no_of_shares"))
                mstrcompany_business_name = dtRow.Item("company_business_name").ToString
                mintcountryunkid = dtRow.Item("countryunkid")
                mintcurrencyunkid = dtRow.Item("currencyunkid")
                mintbasecurrencyunkid = dtRow.Item("basecurrencyunkid")
                mdecbaseexchangerate = dtRow.Item("baseexchangerate")
                mdecexchangerate = dtRow.Item("exchangerate")
                'Hemant (26 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                'mdecbasemarket_value = dtRow.Item("market_value")
                mdecmarket_value = dtRow.Item("market_value")
                'Hemant (26 Nov 2018) -- End
                mdecbasemarket_value = dtRow.Item("basemarket_value")
                mdtacquisition_date = dtRow.Item("acquisition_date")
                mblnisfinalsaved = dtRow.Item("isfinalsaved")
                mdttransactiondate = dtRow.Item("transactiondate")
                mintuserunkid = dtRow.Item("userunkid")
                mblnisvoid = dtRow.Item("isvoid")
                mintvoiduserunkid = dtRow.Item("voiduserunkid")
                mdtvoiddatetime = Nothing
                mstrvoidreason = dtRow.Item("voidreason")
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                mintDependentunkid = CInt(dtRow.Item("dependantunkid"))
                'Hemant (03 Dec 2018) -- End
                If mintassetsecuritiest2depntranunkid <= 0 Then

                    blnChildTableChanged = True

                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End

                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintassetsecuritiest2depntranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintassetsecuritiest2depntranunkid.ToString
                    End If
                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next



            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetsecuritiest2depntranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintassetsecuritiest2depntranunkid = CInt(dtRow.Item("assetsecuritiest2depntranunkid"))
                    mintassetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                    mintrelationshipunkid = CInt(dtRow.Item("relationshipunkid"))
                    mintsecuritiestypeunkid = CInt(dtRow.Item("securitiestypeunkid"))
                    mstrcertificate_no = dtRow.Item("certificate_no")
                    mintno_of_shares = CInt(dtRow.Item("no_of_shares"))
                    mstrcompany_business_name = dtRow.Item("company_business_name").ToString
                    mintcountryunkid = dtRow.Item("countryunkid")
                    mintcurrencyunkid = dtRow.Item("currencyunkid")
                    mintbasecurrencyunkid = dtRow.Item("basecurrencyunkid")
                    mdecbaseexchangerate = dtRow.Item("baseexchangerate")
                    mdecexchangerate = dtRow.Item("exchangerate")
                    'Hemant (26 Nov 2018) -- Start
                    'Enhancement : Changes As per Rutta Request for UAT 
                    'mdecbasemarket_value = dtRow.Item("market_value")
                    mdecmarket_value = dtRow.Item("market_value")
                    'Hemant (26 Nov 2018) -- End
                    mdecbasemarket_value = dtRow.Item("basemarket_value")
                    mdtacquisition_date = dtRow.Item("acquisition_date")
                    mblnisfinalsaved = dtRow.Item("isfinalsaved")
                    mdttransactiondate = dtRow.Item("transactiondate")
                    mintuserunkid = dtRow.Item("userunkid")
                    mblnisvoid = dtRow.Item("isvoid")
                    mintvoiduserunkid = dtRow.Item("voiduserunkid")
                    mdtvoiddatetime = Nothing

                    'Hemant (03 Dec 2018) -- Start
                    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    mintDependentunkid = CInt(dtRow.Item("dependantunkid"))
                    'Hemant (03 Dec 2018) -- End

                    mstrvoidreason = dtRow.Item("voidreason")
                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Void(CInt(dtRow.Item("assetsecuritiest2depntranunkid")), mintuserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetsecuritiest2depntranunkid")), mintuserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing

            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End

        End Try
    End Function

    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2UnkID As Integer = 0, Optional ByVal dtAsonDate As Date = Nothing) As DataSet
        'Sohail (18 May 2019) - [dtAsonDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If dtAsonDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsonDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= "SELECT " & _
              "  hrasset_securitiesT2depn_tran.assetsecuritiest2depntranunkid " & _
              ", hrasset_securitiesT2depn_tran.assetdeclarationt2unkid " & _
              ", hrasset_securitiesT2depn_tran.relationshipunkid " & _
              ", ISNULL(relation.name, '') AS relationshipname " & _
              ", hrasset_securitiesT2depn_tran.securitiestypeunkid " & _
              ", ISNULL(securities.name, '') AS securitiestypename " & _
              ", hrasset_securitiesT2depn_tran.certificate_no " & _
              ", hrasset_securitiesT2depn_tran.no_of_shares " & _
              ", hrasset_securitiesT2depn_tran.company_business_name " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.countryunkid, 0) AS countryunkid " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.exchangerate, 0) AS exchangerate " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.market_value, 0) AS market_value " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.exchangerate, 0) AS exchangerate " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.basemarket_value, 0) AS basemarket_value " & _
              ", hrasset_securitiesT2depn_tran.acquisition_date " & _
              ", hrasset_securitiesT2depn_tran.isfinalsaved " & _
              ", hrasset_securitiesT2depn_tran.transactiondate " & _
              ", hrasset_securitiesT2depn_tran.userunkid " & _
              ", hrasset_securitiesT2depn_tran.isvoid " & _
              ", hrasset_securitiesT2depn_tran.voiduserunkid " & _
              ", hrasset_securitiesT2depn_tran.voiddatetime " & _
              ", hrasset_securitiesT2depn_tran.voidreason " & _
              ", ISNULL(hrasset_securitiesT2depn_tran.dependantunkid, 0) as dependantunkid " & _
              ", ISNULL(hrdependants_beneficiaries_tran.first_name,'') +  ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') +' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') as dependantname "

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_securitiesT2depn_tran.countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_securitiesT2depn_tran " & _
                        "LEFT JOIN cfcommon_master AS securities ON securities.masterunkid = hrasset_securitiesT2depn_tran.securitiestypeunkid AND securities.mastertype = " & CInt(clsCommon_Master.enCommonMaster.ASSET_SECURITIES) & " " & _
                        "LEFT JOIN cfcommon_master AS relation ON relation.masterunkid = hrasset_securitiesT2depn_tran.relationshipunkid AND relation.mastertype = " & CInt(clsCommon_Master.enCommonMaster.RELATIONS) & " " & _
                        "LEFT JOIN hrdependants_beneficiaries_tran ON dpndtbeneficetranunkid = hrasset_securitiesT2depn_tran.dependantunkid " & _
                        "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "WHERE ISNULL(hrasset_securitiesT2depn_tran.isvoid, 0 ) = 0 " & _
                    "AND #TableDepn.isactive = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
            'Sohail (07 Dec 2018) - [LEFT JOIN cfcommon_master AS securities, LEFT JOIN cfcommon_master AS relation, LEFT JOIN hrdependants_beneficiaries_tran]
            'Hemant (03 Dec 2018) -- [dependantunkid]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2UnkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            'End If
            If intAssetDeclarationt2UnkID > 0 Then
                strQ &= " AND hrasset_securitiesT2depn_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function Insert() As Boolean
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Gajanan (07 Dec 2018) - [objAD_MasterT2]
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End


        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintassetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintassetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintassetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetsecuritiest2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintrelationshipunkid)
            objDataOperation.AddParameter("@securitiestypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintsecuritiestypeunkid)
            objDataOperation.AddParameter("@certificate_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcertificate_no.ToString)
            objDataOperation.AddParameter("@no_of_shares", SqlDbType.Int, eZeeDataType.INT_SIZE, mintno_of_shares)
            objDataOperation.AddParameter("@company_business_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_business_name.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@market_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmarket_value)
            objDataOperation.AddParameter("@basemarket_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemarket_value)

            If mdtacquisition_date = Nothing Then
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtacquisition_date.ToString)
            End If

            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            'Hemant (03 Dec 2018) -- End

            strQ = "INSERT INTO hrasset_securitiesT2depn_tran ( " & _
                      " assetdeclarationt2unkid" & _
                      ", relationshipunkid" & _
                      ", securitiestypeunkid" & _
                      ", certificate_no" & _
                      ", no_of_shares " & _
                      ", company_business_name " & _
                      ", countryunkid " & _
                      ", currencyunkid " & _
                      ", basecurrencyunkid " & _
                      ", baseexchangerate " & _
                      ", exchangerate " & _
                      ", market_value " & _
                      ", basemarket_value " & _
                      ", acquisition_date " & _
                      ", isfinalsaved " & _
                      ", transactiondate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", dependantunkid " & _
                  ") VALUES (" & _
                      " @assetdeclarationt2unkid" & _
                      ", @relationshipunkid" & _
                      ", @securitiestypeunkid" & _
                      ", @certificate_no" & _
                      ", @no_of_shares " & _
                      ", @company_business_name " & _
                      ", @countryunkid " & _
                      ", @currencyunkid " & _
                      ", @basecurrencyunkid " & _
                      ", @baseexchangerate " & _
                      ", @exchangerate " & _
                      ", @market_value " & _
                      ", @basemarket_value " & _
                      ", @acquisition_date " & _
                      ", @isfinalsaved " & _
                      ", @transactiondate " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @dependantunkid " & _
            "); SELECT @@identity"

            'Hemant (03 Dec 2018) -- [dependantunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'mintassetdeclarationt2unkid = dsList.Tables(0).Rows(0).Item(0)
            mintassetsecuritiest2depntranunkid = dsList.Tables(0).Rows(0).Item(0)
            'Sohail (07 Dec 2018) -- End

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Gajanan (07 Dec 2018) -- Start
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Gajanan (07 Dec 2018) -- End
                Return False

            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintassetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintassetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintassetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetsecuritiest2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintrelationshipunkid)
            objDataOperation.AddParameter("@securitiestypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintsecuritiestypeunkid)
            objDataOperation.AddParameter("@certificate_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcertificate_no.ToString)
            objDataOperation.AddParameter("@no_of_shares", SqlDbType.Int, eZeeDataType.INT_SIZE, mintno_of_shares)
            objDataOperation.AddParameter("@company_business_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_business_name.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@market_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmarket_value)
            objDataOperation.AddParameter("@basemarket_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemarket_value)

            If mdtacquisition_date = Nothing Then
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtacquisition_date.ToString)
            End If

            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            'Hemant (03 Dec 2018) -- End

            strQ = "UPDATE hrasset_securitiesT2depn_tran SET " & _
                   " assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
                   ", relationshipunkid = @relationshipunkid" & _
                   ", securitiestypeunkid = @securitiestypeunkid " & _
                   ", certificate_no = @certificate_no " & _
                   ", no_of_shares= @no_of_shares" & _
                   ", company_business_name= @company_business_name " & _
                   ", countryunkid= @countryunkid " & _
                   ", currencyunkid= @currencyunkid " & _
                   ", basecurrencyunkid= @basecurrencyunkid " & _
                   ", exchangerate= @exchangerate " & _
                   ", market_value= @market_value " & _
                   ", basemarket_value= @basemarket_value " & _
                   ", transactiondate = @transactiondate " & _
                   ", isfinalsaved = @isfinalsaved " & _
                                     ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", dependantunkid = @dependantunkid " & _
                   ", acquisition_date = @acquisition_date " & _
                   "WHERE assetsecuritiest2depntranunkid = @assetsecuritiest2depntranunkid "

            'Hemant (07 Dec 2018) -- [acquisition_date]
            'Hemant (03 Dec 2018) -- [dependantunkid]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintassetsecuritiest2depntranunkid, objDataOperation) = False Then
                'Hemant (07 Feb 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'If Insert_AtTranLog(objDataOperation, 1) = False Then
                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Hemant (07 Feb 2019) -- End
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2


    'Public Function Void(ByVal intUnkid As Integer, _
    '                     ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
    Public Function Void(ByVal intUnkid As Integer, _
                            ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End

        Try
            strQ = "UPDATE hrasset_securitiesT2depn_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetsecuritiest2depntranunkid = @assetsecuritiest2depntranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If

            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function


    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2


    'Public Function VoidByAssetDeclarationt2UnkID(ByVal intAssetDeclarationt2UnkID As Integer, _
    '                                    ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer) As Boolean
    Public Function VoidByAssetDeclarationt2UnkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                   ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End

        Try

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_bankT2_tran", "assetbankt2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then

            '    Return False
            'End If
            strQ = "INSERT INTO athrasset_securitiesT2depn_tran( " & _
                       " assetsecuritiest2depntranunkid " & _
                       ", assetdeclarationt2unkid " & _
                       ", relationshipunkid " & _
                       ", securitiestypeunkid " & _
                       ", certificate_no " & _
                       ", no_of_shares " & _
                       ", company_business_name " & _
                       ", countryunkid " & _
                       ", currencyunkid " & _
                       ", basecurrencyunkid " & _
                       ", baseexchangerate " & _
                       ", exchangerate " & _
                       ", market_value " & _
                       ", basemarket_value " & _
                       ", acquisition_date " & _
                       ", isfinalsaved " & _
                       ", transactiondate " & _
                       ", auditdatetime " & _
                       ", AuditType " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", isweb " & _
                       ", dependantunkid " & _
                       " ) " & _
                   "SELECT " & _
                   " assetsecuritiest2depntranunkid " & _
                       ", assetdeclarationt2unkid " & _
                       ", relationshipunkid " & _
                       ", securitiestypeunkid " & _
                       ", certificate_no " & _
                       ", no_of_shares " & _
                       ", company_business_name " & _
                       ", countryunkid " & _
                       ", currencyunkid " & _
                       ", basecurrencyunkid " & _
                       ", baseexchangerate " & _
                       ", exchangerate " & _
                       ", market_value " & _
                       ", basemarket_value " & _
                       ", acquisition_date " & _
                       ", isfinalsaved " & _
                       ", transactiondate " & _
                       ", GETDATE() " & _
                       ", 3 " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       ", @isweb " & _
                       ", dependantunkid " & _
                       "  FROM hrasset_securitiesT2depn_tran " & _
                       "  WHERE isvoid = 0 " & _
                       "  AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            'Hemant (03 Dec 2018) -- [dependantunkid]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE hrasset_securitiesT2depn_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO athrasset_securitiesT2depn_tran( " & _
                       " assetsecuritiest2depntranunkid " & _
                       ", assetdeclarationt2unkid " & _
                       ", relationshipunkid " & _
                       ", securitiestypeunkid " & _
                       ", certificate_no " & _
                       ", no_of_shares " & _
                       ", company_business_name " & _
                       ", countryunkid " & _
                       ", currencyunkid " & _
                       ", basecurrencyunkid " & _
                       ", baseexchangerate " & _
                       ", exchangerate " & _
                       ", market_value " & _
                       ", basemarket_value " & _
                       ", acquisition_date " & _
                       ", isfinalsaved " & _
                       ", transactiondate " & _
                       ", auditdatetime " & _
                       ", AuditType " & _
                       ", audituserunkid " & _
                       ", loginemployeeunkid " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", isweb " & _
                       ", dependantunkid " & _
                  ") VALUES (" & _
                       " @assetsecuritiest2depntranunkid " & _
                       ", @assetdeclarationt2unkid " & _
                       ",@relationshipunkid " & _
                       ", @securitiestypeunkid " & _
                       ", @certificate_no " & _
                       ", @no_of_shares " & _
                       ", @company_business_name " & _
                       ", @countryunkid " & _
                       ", @currencyunkid " & _
                       ", @basecurrencyunkid " & _
                       ", @baseexchangerate " & _
                       ", @exchangerate " & _
                       ", @market_value " & _
                       ", @basemarket_value " & _
                       ", @acquisition_date " & _
                       ", @isfinalsaved " & _
                       ", @transactiondate " & _
                       ", GETDATE() " & _
                       ", @AuditType " & _
                       ", @audituserunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       ", @isweb " & _
                       ", @dependantunkid " & _
                       " ) "

            'Hemant (03 Dec 2018) -- [dependantunkid]
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetsecuritiest2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintrelationshipunkid)
            objDataOperation.AddParameter("@securitiestypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintsecuritiestypeunkid)
            objDataOperation.AddParameter("@certificate_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcertificate_no.ToString)
            objDataOperation.AddParameter("@no_of_shares", SqlDbType.Int, eZeeDataType.INT_SIZE, mintno_of_shares)
            objDataOperation.AddParameter("@company_business_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_business_name.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@market_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmarket_value)
            objDataOperation.AddParameter("@basemarket_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemarket_value)

            If mdtacquisition_date = Nothing Then
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtacquisition_date.ToString)
            End If

            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            'Hemant (03 Dec 2018) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                extForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw extForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_securitiesT2depn_tran where assetsecuritiest2depntranunkid = @assetsecuritiest2depntranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_securitiesT2depn_tran where assetsecuritiest2depntranunkid = @assetsecuritiest2depntranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetsecuritiest2depntranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                If IsDBNull(dr("dependantunkid")) = True Then
                    Return False
                    'Hemant (03 Dec 2018) -- End
                ElseIf dr("securitiestypeunkid").ToString() = mintsecuritiestypeunkid AndAlso dr("certificate_no").ToString() = mstrcertificate_no AndAlso _
                   dr("company_business_name").ToString() = mstrcompany_business_name AndAlso dr("no_of_shares").ToString() = mintno_of_shares AndAlso _
                      dr("countryunkid").ToString() = mintcountryunkid AndAlso CDate(dr("acquisition_date")) = mdtacquisition_date _
                       AndAlso dr("dependantunkid").ToString() = mintDependentunkid Then
                    'Hemant (03 Dec 2018) -- [dependantunkid]
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class






﻿'************************************************************************************************************************************
'Class Name : clsUserAddEdit.vb
'Purpose    : Report Ability Level
'Date       : 18-Aug-2015
'Written By : Nilay Mistry
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsReport_role_mapping_tran

    Private ReadOnly mstrModuleName As String = "clsReport_role_mapping_tran"

#Region "Private Variables"
    Private iobjDataOperation As clsDataOperation
#End Region

#Region "Properties"

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private xDataOpr As clsDataOperation = Nothing
    Public Property _xDataOpr() As clsDataOperation
        Get
            Return xDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            iobjDataOperation = value
        End Set
    End Property
#End Region

#Region "Public Methods"

    Public Function GetListByRoleAndCompany(ByVal intcompanyunkid As Integer, ByVal intRoleunkid As Integer) As DataTable
        Dim dtTable As New DataTable
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim strQ As String = ""
        Dim objDataOperation As clsDataOperation

        If iobjDataOperation IsNot Nothing Then
            objDataOperation = iobjDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                        "  mappingtranunkid " & _
                        " ,roleunkid " & _
                        " ,reportunkid " & _
                        " ,companyunkid " & _
                    " FROM cfreport_role_mapping_tran " & _
                    " WHERE roleunkid = '" & intRoleunkid & "' AND companyunkid = '" & intcompanyunkid & "' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables("List").Copy

        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: GetListByRoleAndCompany; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    Public Function GetReportAbilityLevelList(ByVal intcompanyunkid As Integer, ByVal intUserUnkid As Integer, ByVal strDatabaseName As String) As DataTable

        Dim objDataOperation As clsDataOperation
        Dim exForce As Exception

        Dim dtTable As New DataTable
        Dim dsReportList As New DataSet
        Dim objReport = New clsArutiReportClass

        Try

            dsReportList = objReport.getReportListGroupedByCategory(strDatabaseName, intUserUnkid, intcompanyunkid, False)
            dtTable = dsReportList.Tables(0).Copy

            dtTable.Columns("DisplayName").ColumnName = "Reports"
            dtTable.Columns("reportunkid").ColumnName = "objreportunkid"
            dtTable.Columns("reportcategoryunkid").ColumnName = "objreportcategoryunkid"

            dtTable.Columns.Remove("CategoryName")
            dtTable.Columns.Remove("sort_key")
            dtTable.Columns.Remove("AUD")
            dtTable.Columns.Remove("IsRcheck")
            dtTable.Columns.Remove("reportprivilegeunkid")

            Dim dsRoleList As New DataSet
            Dim objUserRole As New clsUserRole_Master
            dsRoleList = objUserRole.GetList("Role", True)
            Dim dtCol As DataColumn
            Dim dtAssignedReport As DataTable
            For i As Integer = 0 To dsRoleList.Tables("Role").Rows.Count - 1
                dtCol = New DataColumn()
                dtCol.ColumnName = dsRoleList.Tables("Role").Rows(i).Item("name").ToString
                dtCol.ExtendedProperties.Add(dsRoleList.Tables("Role").Rows(i).Item("name"), dsRoleList.Tables("Role").Rows(i).Item("roleunkid"))
                dtCol.DataType = System.Type.GetType("System.Boolean")
                dtCol.DefaultValue = False
                dtTable.Columns.Add(dtCol)

                dtCol = New DataColumn()
                dtCol.ColumnName = "objAUD" & dsRoleList.Tables("Role").Rows(i).Item("name").ToString
                dtCol.ExtendedProperties.Add(dtCol.ColumnName, dsRoleList.Tables("Role").Rows(i).Item("roleunkid"))
                dtCol.DataType = System.Type.GetType("System.String")
                dtCol.DefaultValue = ""
                dtTable.Columns.Add(dtCol)

            Next

            dtCol = New DataColumn("objisReportCategory")
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtTable.Columns.Add(dtCol)

            'S.SANDEEP [21-SEP-2018] -- START
            dtCol = New DataColumn("objReportGrp")
            dtCol.Caption = Language.getMessage(mstrModuleName, 100, "Report Group")
            dtCol.DataType = System.Type.GetType("System.String")
            dtTable.Columns.Add(dtCol)

            dtCol = New DataColumn("objReportName")
            dtCol.Caption = Language.getMessage(mstrModuleName, 101, "Report Name")
            dtCol.DataType = System.Type.GetType("System.String")
            dtTable.Columns.Add(dtCol)
            'S.SANDEEP [21-SEP-2018] -- END

            For i As Integer = 0 To dsReportList.Tables(0).Rows.Count - 1
                If CInt(dsReportList.Tables(0).Rows(i).Item("reportunkid")) <= 0 Then
                    dtTable.Rows(i).Item("objisReportCategory") = True
                Else
                    dtTable.Rows(i).Item("objisReportCategory") = False
                End If
                dtTable.Rows(i)("objReportGrp") = dsReportList.Tables(0).Rows(i)("CategoryName")
                dtTable.Rows(i)("objReportName") = dsReportList.Tables(0).Rows(i)("DisplayName").ToString.Trim()
            Next

            For Each xCol As DataColumn In dtTable.Columns
                If xCol.ExtendedProperties.Count > 0 Then
                    Dim StrColName As String = ""
                    StrColName = xCol.ColumnName
                    If StrColName.StartsWith("objAUD") Then Continue For

                    dtAssignedReport = GetListByRoleAndCompany(intcompanyunkid, xCol.ExtendedProperties(StrColName))


                    If dtAssignedReport.Rows.Count > 0 Then

                        Dim rows = (From A In dtTable.AsEnumerable() Join B In dtAssignedReport.AsEnumerable() _
                                    On A.Item("objreportunkid") Equals B.Item("reportunkid") Select (A))

                        For Each ro As DataRow In rows
                            ro.Item(StrColName) = True
                        Next

                        Dim grprows = (From T In dtTable.AsEnumerable().Where(Function(y) y.Field(Of Boolean)("objisReportCategory") = True) Select (T))
                        For Each ro As DataRow In grprows
                            Dim iGrpUnkid As Integer = 0
                            iGrpUnkid = ro.Item("objreportcategoryunkid")
                            Dim xCount = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(StrColName) = True And x.Field(Of Integer)("objreportcategoryunkid") = iGrpUnkid).Count()
                            If xCount > 0 Then
                                ro.Item(StrColName) = True
                            End If
                        Next
                    End If



                End If
            Next

            dtTable.Columns("objisReportCategory").SetOrdinal(dtTable.Columns(dtTable.Columns.Count - 1).Ordinal)
            dtTable.Columns("objreportunkid").SetOrdinal(dtTable.Columns("objisReportCategory").Ordinal)
            dtTable.Columns("objreportcategoryunkid").SetOrdinal(dtTable.Columns("objreportunkid").Ordinal)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReportAbilityLevelList; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If iobjDataOperation Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dtTable
    End Function

    Public Function ExportReportAbilityLevel(ByVal dtReportAbilityList As DataTable) As System.Text.StringBuilder

        Dim strBuilder As New System.Text.StringBuilder
        Dim intIsReportCatColOrdinal As Integer = CInt(dtReportAbilityList.Columns("objisReportCategory").Ordinal)

        Try
            strBuilder.Append("<HTML>" & vbCrLf)
            strBuilder.Append("<BODY><FONT FACE=VERDANA FONT SIZE=2>" & vbCrLf)
            strBuilder.Append("<TABLE BORDER=1 BORDERCOLOR =BLACK WIDTH='100%'>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
            For i As Integer = 0 To dtReportAbilityList.Columns.Count - 1

                If dtReportAbilityList.Columns(i).ColumnName.StartsWith("obj") Then Continue For
                If i = 0 Then
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR='Gray'><FONT FONT COLOR='White' SIZE=3>" & dtReportAbilityList.Columns(i).ColumnName.ToString & "</FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' BGCOLOR='Gray'><FONT FONT COLOR='White' SIZE=3>" & dtReportAbilityList.Columns(i).ColumnName.ToString & "</FONT></TD>" & vbCrLf)
                End If
            Next

            For i As Integer = 0 To dtReportAbilityList.Rows.Count - 1
                strBuilder.Append(" <TR WIDTH='100%'> " & vbCrLf)
                For k As Integer = 0 To dtReportAbilityList.Columns.Count - 1
                    If dtReportAbilityList.Columns(k).ColumnName.StartsWith("obj") Then Continue For
                    If k = 0 Then
                        If CBool(dtReportAbilityList.Rows(i).Item(intIsReportCatColOrdinal)) = True Then
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR = 'TEAL' COLSPAN='" & dtReportAbilityList.Columns.Count - (dtReportAbilityList.Columns.Count - intIsReportCatColOrdinal) & "'><FONT SIZE=2 COLOR = 'WHITE'><B>" & CStr(dtReportAbilityList.Rows(i).Item(k).ToString) & "</B></FONT></TD>" & vbCrLf)
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> &nbsp;&nbsp;&nbsp;&nbsp; " & CStr(dtReportAbilityList.Rows(i).Item(k).ToString) & "</FONT></TD>" & vbCrLf)
                        End If
                    Else
                        If CBool(dtReportAbilityList.Rows(i).Item(intIsReportCatColOrdinal)) = False Then
                            If CBool(dtReportAbilityList.Rows(i).Item(k)) = True Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' ><FONT SIZE=2>" & "YES" & "</FONT></TD>" & vbCrLf)
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN = 'CENTER' ><FONT SIZE=2>" & "NO" & "</FONT></TD>" & vbCrLf)
                            End If
                        End If
                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append("</TR>" & vbCrLf)
            strBuilder.Append("</TABLE>" & vbCrLf)
            strBuilder.Append("</BODY>" & vbCrLf)
            strBuilder.Append("</HTML>" & vbCrLf)
        Catch ex As Exception
            Throw New Exception(ex.Message & "Procedure Name: ExportReportAbilityLevel; Module Name: " & mstrModuleName)
        End Try
        Return strBuilder
    End Function

    Public Function SaveReportAbilityLevel(ByVal StrDataBaseName As String, _
                                           ByVal intRoleId As Integer, _
                                           ByVal mReportIdsDictionary As Dictionary(Of Integer, String), _
                                           ByVal intCompanyId As Integer) As Boolean
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = ""
        Dim objDataOperation As clsDataOperation

        If iobjDataOperation IsNot Nothing Then
            objDataOperation = iobjDataOperation
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            If mReportIdsDictionary.Keys.Count > 0 Then
                Dim dsUsr As New DataSet

                StrQ = "SELECT userunkid,roleunkid FROM " & StrDataBaseName & "..cfuser_master WHERE isactive = 1 AND roleunkid = '" & intRoleId & "' AND userunkid <> 1"

                dsUsr = objDataOperation.ExecQuery(StrQ, "List")

                Dim dUserList = From usr In dsUsr.Tables(0).AsEnumerable() Select usr

                '*********************************************** INSERT REPORT PRIVILEGE FOR USER BASED ON ROLE ********** START
                For Each iUsr In dUserList

                    For Each iReportId As Integer In mReportIdsDictionary.Keys

                        Select Case mReportIdsDictionary(iReportId)

                            Case "A"

                                If objDataOperation.RecordCount("SELECT userunkid,reportunkid FROM " & StrDataBaseName & "..cfuser_reportprivilege WHERE userunkid = '" & iUsr.Item("userunkid") & "' AND reportunkid = '" & iReportId & "' AND companyunkid = '" & intCompanyId & "'") <= 0 Then

                                    StrQ = "INSERT INTO " & StrDataBaseName & "..cfuser_reportprivilege (userunkid,reportunkid,companyunkid,isfavorite) " & _
                                           "SELECT TOP 1 '" & iUsr.Item("userunkid") & "','" & iReportId & "','" & intCompanyId & "',CAST(0 AS BIT) ; SELECT @@identity "

                                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        Dim objCommonATLog As New clsCommonATLog
                                        objCommonATLog._FormName = mstrFormName
                                        objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                        objCommonATLog._ClientIP = mstrClientIP
                                        objCommonATLog._HostName = mstrHostName
                                        objCommonATLog._FromWeb = mblnIsWeb
                                        objCommonATLog._AuditUserId = mintAuditUserId
                                        objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                        objCommonATLog._AuditDate = mdtAuditDate
                                        'S.SANDEEP [28-May-2018] -- END

                                        If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cfuser_reportprivilege", "reportprivilegeunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If

                                        'S.SANDEEP [28-May-2018] -- START
                                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                                        objCommonATLog = Nothing
                                        'S.SANDEEP [28-May-2018] -- END

                                    End If

                                End If

                            Case "D"

                                StrQ = "SELECT reportprivilegeunkid FROM " & StrDataBaseName & "..cfuser_reportprivilege WHERE userunkid = '" & iUsr.Item("userunkid") & "' AND reportunkid = '" & iReportId & "' AND companyunkid = '" & intCompanyId & "'"

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If dsList.Tables(0).Rows.Count > 0 Then

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cfuser_reportprivilege", "reportprivilegeunkid", dsList.Tables(0).Rows(0)("reportprivilegeunkid"), True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END


                                    StrQ = "DELETE FROM " & StrDataBaseName & "..cfuser_reportprivilege WHERE reportprivilegeunkid = '" & dsList.Tables(0).Rows(0)("reportprivilegeunkid") & "' "

                                    objDataOperation.ExecNonQuery(StrQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                        End Select

                    Next

                Next
                '*********************************************** INSERT REPORT PRIVILEGE FOR USER BASED ON ROLE ********** END


                '*********************************************** INSERT REPORT MAPPING WITH ROLE ********** START
                For Each iReportId As Integer In mReportIdsDictionary.Keys

                    Select Case mReportIdsDictionary(iReportId)

                        Case "A"

                            If objDataOperation.RecordCount("SELECT mappingtranunkid FROM " & StrDataBaseName & "..cfreport_role_mapping_tran WHERE roleunkid = '" & intRoleId & "' AND reportunkid = '" & iReportId & "' AND companyunkid = '" & intCompanyId & "'") <= 0 Then

                                StrQ = "INSERT INTO " & StrDataBaseName & "..cfreport_role_mapping_tran (roleunkid,reportunkid,companyunkid) " & _
                                       "SELECT '" & intRoleId & "','" & iReportId & "','" & intCompanyId & "' ;SELECT @@identity "

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If dsList.Tables("List").Rows(0)(0).ToString.Trim.Length > 0 Then
                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    Dim objCommonATLog As New clsCommonATLog
                                    objCommonATLog._FormName = mstrFormName
                                    objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                    objCommonATLog._ClientIP = mstrClientIP
                                    objCommonATLog._HostName = mstrHostName
                                    objCommonATLog._FromWeb = mblnIsWeb
                                    objCommonATLog._AuditUserId = mintAuditUserId
                                    objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                    objCommonATLog._AuditDate = mdtAuditDate
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objCommonATLog.Insert_AtLog(objDataOperation, 1, "cfreport_role_mapping_tran", "mappingtranunkid", dsList.Tables("List").Rows(0)(0), True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objCommonATLog = Nothing
                                    'S.SANDEEP [28-May-2018] -- END

                                End If

                            End If

                        Case "D"

                            StrQ = "SELECT mappingtranunkid FROM " & StrDataBaseName & "..cfreport_role_mapping_tran WHERE roleunkid = '" & intRoleId & "' AND reportunkid = '" & iReportId & "' AND companyunkid = '" & intCompanyId & "'"

                            dsList = objDataOperation.ExecQuery(StrQ, "List")

                            If dsList.Tables(0).Rows.Count > 0 Then

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                Dim objCommonATLog As New clsCommonATLog
                                objCommonATLog._FormName = mstrFormName
                                objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                objCommonATLog._ClientIP = mstrClientIP
                                objCommonATLog._HostName = mstrHostName
                                objCommonATLog._FromWeb = mblnIsWeb
                                objCommonATLog._AuditUserId = mintAuditUserId
                                objCommonATLog._CompanyUnkid = mintCompanyUnkid
                                objCommonATLog._AuditDate = mdtAuditDate
                                'S.SANDEEP [28-May-2018] -- END

                                If objCommonATLog.Insert_AtLog(objDataOperation, 3, "cfreport_role_mapping_tran", "mappingtranunkid", dsList.Tables(0).Rows(0)("mappingtranunkid"), True) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objCommonATLog = Nothing
                                'S.SANDEEP [28-May-2018] -- END

                                'S.SANDEEP [15-NOV-2018] -- START
                                'StrQ = "DELETE FROM " & StrDataBaseName & "..cfreport_role_mapping_tran WHERE reportprivilegeunkid = '" & dsList.Tables(0).Rows(0)("mappingtranunkid") & "' "
                                StrQ = "DELETE FROM " & StrDataBaseName & "..cfreport_role_mapping_tran WHERE mappingtranunkid = '" & dsList.Tables(0).Rows(0)("mappingtranunkid") & "' "
                                'S.SANDEEP [15-NOV-2018] -- END

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                    End Select

                Next
                '*********************************************** INSERT REPORT MAPPING WITH ROLE ********** END

                'S.SANDEEP [18 Jan 2016] -- START
                'If iobjDataOperation Is Nothing Then
                '    objDataOperation.ReleaseTransaction(True)
                'End If
                'S.SANDEEP [18 Jan 2016] -- END

            End If

            'S.SANDEEP [18 Jan 2016] -- START
                If iobjDataOperation Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            'S.SANDEEP [18 Jan 2016] -- END

        Catch ex As Exception
            'S.SANDEEP [18 Jan 2016] -- START
            If iobjDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [18 Jan 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: SaveReportAbilityLevel; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

End Class

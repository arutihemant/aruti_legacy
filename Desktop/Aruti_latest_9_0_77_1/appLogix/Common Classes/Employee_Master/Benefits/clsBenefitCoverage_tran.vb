﻿'************************************************************************************************************************************
'Class Name : clsBenefitCoverage_tran.vb
'Purpose    :
'Date       :27/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsBenefitCoverage_tran
    Private Shared ReadOnly mstrModuleName As String = "clsBenefitCoverage_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBenefitcoverageunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintBenefitgroupunkid As Integer
    Private mintBenefitplanunkid As Integer
    'Vimal (01 Nov 2010) -- Start 
    'Private mintValue_Basis As Integer
    'Private mdblPercentage As Double
    'Private mdblAmount As Double
    'Private mdtStart_Date As Date
    'Private mdtEnd_Date As Date
    'Vimal (01 Nov 2010) -- End
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mintTranheadunkid As Integer
    Private mintGroupAssignBenefitGroupId As Integer
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Vimal (01 Nov 2010) -- Start 
    Private intOldTranhedunkid As Integer = -1
    'Anjan (11 May 2011)-Start
    'Private mdblAmount As Double
    Private mdecAmount As Decimal

    'Vimal (01 Nov 2010) -- End


#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitcoverageunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Benefitcoverageunkid() As Integer
        Get
            Return mintBenefitcoverageunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitcoverageunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Benefitgroupunkid() As Integer
        Get
            Return mintBenefitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplanunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Benefitplanunkid() As Integer
        Get
            Return mintBenefitplanunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitplanunkid = Value
        End Set
    End Property
    'Vimal (01 Nov 2010) -- Start 
    '' <summary>
    '' Purpose: Get or Set value_basis
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    'Public Property _Value_Basis() As Integer
    '    Get
    '        Return mintValue_Basis
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintValue_Basis = Value
    '    End Set
    'End Property

    '' <summary>
    '' Purpose: Get or Set percentage
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    'Public Property _Percentage() As Double
    '    Get
    '        Return mdblPercentage
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblPercentage = Value
    '    End Set
    'End Property

    '' <summary>
    '' Purpose: Get or Set amount
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    'Public Property _Amount() As Double
    '    Get
    '        Return mdblAmount
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblAmount = Value
    '    End Set
    'End Property

    '' <summary>
    '' Purpose: Get or Set start_date
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    'Public Property _Start_Date() As Date
    '    Get
    '        Return mdtStart_Date
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtStart_Date = Value
    '    End Set
    'End Property

    '' <summary>
    '' Purpose: Get or Set end_date
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    'Public Property _End_Date() As Date
    '    Get
    '        Return mdtEnd_Date
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtEnd_Date = Value
    '    End Set
    'End Property
    'Vimal (01 Nov 2010) -- End

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sandeep [ 21 Aug 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Vimal (01 Nov 2010) -- Start 

    'Anjan (11 May 2011)-Start
    'Public Property _Amount() As Double
    Public Property _Amount() As Decimal
        'Anjan (11 May 2011)-End 
        Get
            Return mdecAmount
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End 
            mdecAmount = value
        End Set
    End Property
    'Vimal (01 Nov 2010) -- End




#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sandeep [ 21 Aug 2010 ] -- Start
            'strQ = "SELECT " & _
            '  "  benefitcoverageunkid " & _
            '  ", employeeunkid " & _
            '  ", benefitgroupunkid " & _
            '  ", benefitplanunkid " & _
            '  ", value_basis " & _
            '  ", percentage " & _
            '  ", amount " & _
            '  ", start_date " & _
            '  ", end_date " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' "FROM hremp_benefit_coverage " & _
            ' "WHERE benefitcoverageunkid = @benefitcoverageunkid "


            'Vimal (01 Nov 2010) -- Start 
            ' strQ = "SELECT " & _
            ' "  benefitcoverageunkid " & _
            ' ", employeeunkid " & _
            ' ", benefitgroupunkid " & _
            ' ", benefitplanunkid " & _
            ' ", value_basis " & _
            ' ", percentage " & _
            ' ", amount " & _
            ' ", start_date " & _
            ' ", end_date " & _
            ' ", userunkid " & _
            ' ", isvoid " & _
            ' ", voiduserunkid " & _
            ' ", voiddatetime " & _
            ' ", voidreason " & _
            '         ", ISNULL(tranheadunkid,0) As tranheadunkid " & _
            '"FROM hremp_benefit_coverage " & _
            '"WHERE benefitcoverageunkid = @benefitcoverageunkid "

            strQ = "SELECT " & _
              "  benefitcoverageunkid " & _
              ", employeeunkid " & _
              ", benefitgroupunkid " & _
              ", benefitplanunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                      ", ISNULL(tranheadunkid,0) As tranheadunkid " & _
             "FROM hremp_benefit_coverage " & _
             "WHERE benefitcoverageunkid = @benefitcoverageunkid "
            'Vimal (01 Nov 2010) -- End




            'Sandeep [ 21 Aug 2010 ] -- End 

            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitcoverageunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBenefitcoverageunkid = CInt(dtRow.Item("benefitcoverageunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintBenefitgroupunkid = CInt(dtRow.Item("benefitgroupunkid"))
                mintBenefitplanunkid = CInt(dtRow.Item("benefitplanunkid"))
                'Vimal (01 Nov 2010) -- Start 
                'mintValue_Basis = CInt(dtRow.Item("value_basis"))
                'mdblPercentage = cdec(dtRow.Item("percentage"))

                'Anjan (11 May 2011)-Start
                'mdblAmount = cdec(dtRow.Item("amount"))
                'mdecAmount = CDec(dtRow.Item("amount"))
                'Anjan (11 May 2011)-End
                'mdtStart_Date = dtRow.Item("start_date")
                'If IsDBNull(dtRow.Item("end_date")) Then
                '    mdtEnd_Date = Nothing
                'Else
                '    mdtEnd_Date = dtRow.Item("end_date")
                'End If
                'Vimal (01 Nov 2010) -- End

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))

                'Vimal (01 Nov 2010) -- Start 
                intOldTranhedunkid = mintTranheadunkid
                'Vimal (01 Nov 2010) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intEmpID As Integer = 0, _
                            Optional ByVal intBenefitPlanID As Integer = 0, _
                            Optional ByVal intTranHeadID As Integer = 0, _
                            Optional ByVal strFilterString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet 'Sandeep (14-Sep-2018) -- Start {mblnAddApprovalCondition} -- End

        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition) 'Sandeep (14-Sep-2018) -- Start {mblnAddApprovalCondition} -- End
            'Pinkal (28-Dec-2015) -- End
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
                       "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
                       "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
                       "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
                       "	,hremp_benefit_coverage.userunkid " & _
                       "	,hremp_benefit_coverage.isvoid " & _
                       "	,hremp_benefit_coverage.voiduserunkid " & _
                       "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
                       "	,hremp_benefit_coverage.voidreason " & _
                       "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
                       "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
                       "    ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                       "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
                       "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId " & _
                       "    ,ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                       "    ,ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
                       "    ,ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                       "    ,ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
                       "    ,ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
                       "    ,ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
                       "    ,ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
                       "    ,ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
                       "    ,ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
                       "    ,ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
                       "    ,ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
                       "    ,ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
                       "    ,ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
                       "FROM hremp_benefit_coverage " & _
                       "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
                       "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
                       "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid "

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

              
                StrQ &= " WHERE 1 = 1 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry & " "
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                If intEmpID > 0 Then
                    StrQ &= "AND hremp_benefit_coverage.employeeunkid = @employeeunkid "
                    objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID.ToString)
                End If

                If intBenefitPlanID > 0 Then
                    StrQ &= "AND hrbenefitplan_master.benefitplanunkid = @benefitplanunkid "
                    objDo.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitPlanID.ToString)
                End If

                If intTranHeadID > 0 Then
                    StrQ &= "AND hremp_benefit_coverage.tranheadunkid = @tranheadunkid "
                    objDo.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID.ToString)
                End If

                If strFilterString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilterString
                End If

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal intEmpID As Integer = 0, _
    '                        Optional ByVal intBenefitPlanID As Integer = 0, _
    '                        Optional ByVal intTranHeadID As Integer = 0, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END


    '    'Public Function GetList(ByVal strTableName As String, Optional ByVal intEmpID As Integer = 0, Optional ByVal intBenefitPlanID As Integer = 0, Optional ByVal intTranHeadID As Integer = 0) As DataSet 'Sohail (03 Nov 2010)
    '    'Public Function GetList(ByVal strTableName As String) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'Sandeep [ 21 Aug 2010 ] -- Start
    '        'strQ = "SELECT " & _
    '        '        "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '        '        "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '        '        "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '        '        "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '        '        "	,hremp_benefit_coverage.value_basis " & _
    '        '        "	,hremp_benefit_coverage.percentage AS BPercent " & _
    '        '        "	,hremp_benefit_coverage.amount AS BAmount " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.start_date,112) AS StDate " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.end_date,112) AS EdDate " & _
    '        '        "	,hremp_benefit_coverage.userunkid " & _
    '        '        "	,hremp_benefit_coverage.isvoid " & _
    '        '        "	,hremp_benefit_coverage.voiduserunkid " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '        '        "	,hremp_benefit_coverage.voidreason " & _
    '        '        "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '        '        "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '        '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '        "FROM hremp_benefit_coverage " & _
    '        '        "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '        "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "

    '        'Vimal (01 Nov 2010) -- Start 
    '        'strQ = "SELECT " & _
    '        '       "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '        '       "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '        '       "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '        '       "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '        '       "	,hremp_benefit_coverage.value_basis " & _
    '        '       "	,hremp_benefit_coverage.percentage AS BPercent " & _
    '        '       "	,hremp_benefit_coverage.amount AS BAmount " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.start_date,112) AS StDate " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.end_date,112) AS EdDate " & _
    '        '       "	,hremp_benefit_coverage.userunkid " & _
    '        '       "	,hremp_benefit_coverage.isvoid " & _
    '        '       "	,hremp_benefit_coverage.voiduserunkid " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '        '       "	,hremp_benefit_coverage.voidreason " & _
    '        '       "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '        '       "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '        '       "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '      "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
    '        '      "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId " & _
    '        '       "FROM hremp_benefit_coverage " & _
    '        '      "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '       "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '       "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "

    '        'Sohail (03 Nov 2010) -- Start
    '        'Changes : EmpID, benefitplanID, tranheadID parameters added.
    '        'strQ = "SELECT " & _
    '        '        "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '        '        "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '        '        "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '        '        "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '        '        "	,hremp_benefit_coverage.userunkid " & _
    '        '        "	,hremp_benefit_coverage.isvoid " & _
    '        '        "	,hremp_benefit_coverage.voiduserunkid " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '        '        "	,hremp_benefit_coverage.voidreason " & _
    '        '        "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '        '        "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '        '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '       "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
    '        '       "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId " & _
    '        '        "FROM hremp_benefit_coverage " & _
    '        '       "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '        "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "



    '        strQ = "SELECT  hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '                "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '                "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '                "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '                "	,hremp_benefit_coverage.userunkid " & _
    '                "	,hremp_benefit_coverage.isvoid " & _
    '                "	,hremp_benefit_coverage.voiduserunkid " & _
    '                "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '                "	,hremp_benefit_coverage.voidreason " & _
    '                "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '                "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                        "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                        "+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '               "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
    '               "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId "

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid "
    '        'Anjan (21 Nov 2011)-End 



    '        'S.SANDEEP [ 23 JAN 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '        'strQ &= " FROM hremp_benefit_coverage " & _
    '        '                    "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '                    "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '                    "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid " & _
    '        '                                                         "AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '                    "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                    " WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "
    '        strQ &= " FROM hremp_benefit_coverage " & _
    '               "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '               "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid " & _
    '                                                     "AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '                "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '                            " WHERE 1 = 1 "
    '        'S.SANDEEP [ 23 JAN 2012 ] -- END





    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END
    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END




    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If


    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Anjan (24 Jun 2011)-End 





    '        If intEmpID > 0 Then
    '            strQ &= "AND hremp_benefit_coverage.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID.ToString)
    '        End If
    '        If intBenefitPlanID > 0 Then
    '            strQ &= "AND hrbenefitplan_master.benefitplanunkid = @benefitplanunkid "
    '            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitPlanID.ToString)
    '        End If
    '        If intTranHeadID > 0 Then
    '            strQ &= "AND hremp_benefit_coverage.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID.ToString)
    '        End If
    '        'Sohail (03 Nov 2010) -- End
    '        'Vimal (01 Nov 2010) -- End


    '        'Sandeep [ 21 Aug 2010 ] -- End 
    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END


    'Public Function GetList(ByVal strTableName As String) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'Sandeep [ 21 Aug 2010 ] -- Start
    '        'strQ = "SELECT " & _
    '        '        "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '        '        "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '        '        "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '        '        "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '        '        "	,hremp_benefit_coverage.value_basis " & _
    '        '        "	,hremp_benefit_coverage.percentage AS BPercent " & _
    '        '        "	,hremp_benefit_coverage.amount AS BAmount " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.start_date,112) AS StDate " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.end_date,112) AS EdDate " & _
    '        '        "	,hremp_benefit_coverage.userunkid " & _
    '        '        "	,hremp_benefit_coverage.isvoid " & _
    '        '        "	,hremp_benefit_coverage.voiduserunkid " & _
    '        '        "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '        '        "	,hremp_benefit_coverage.voidreason " & _
    '        '        "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '        '        "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '        '        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '        "FROM hremp_benefit_coverage " & _
    '        '        "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '        "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "

    '        'Vimal (01 Nov 2010) -- Start 
    '        'strQ = "SELECT " & _
    '        '       "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '        '       "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '        '       "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '        '       "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '        '       "	,hremp_benefit_coverage.value_basis " & _
    '        '       "	,hremp_benefit_coverage.percentage AS BPercent " & _
    '        '       "	,hremp_benefit_coverage.amount AS BAmount " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.start_date,112) AS StDate " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.end_date,112) AS EdDate " & _
    '        '       "	,hremp_benefit_coverage.userunkid " & _
    '        '       "	,hremp_benefit_coverage.isvoid " & _
    '        '       "	,hremp_benefit_coverage.voiduserunkid " & _
    '        '       "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '        '       "	,hremp_benefit_coverage.voidreason " & _
    '        '       "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '        '       "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '        '       "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '      "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
    '        '      "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId " & _
    '        '       "FROM hremp_benefit_coverage " & _
    '        '      "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '        '       "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '        '       "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "

    '        strQ = "SELECT " & _
    '                "	 hremp_benefit_coverage.benefitcoverageunkid AS BCoverageId " & _
    '                "	,hremp_benefit_coverage.employeeunkid AS EmpId " & _
    '                "	,hremp_benefit_coverage.benefitgroupunkid AS BGroupId " & _
    '                "	,hremp_benefit_coverage.benefitplanunkid AS BPlanId " & _
    '                "	,hremp_benefit_coverage.userunkid " & _
    '                "	,hremp_benefit_coverage.isvoid " & _
    '                "	,hremp_benefit_coverage.voiduserunkid " & _
    '                "	,CONVERT(CHAR(8),hremp_benefit_coverage.voiddatetime,112)AS voiddatetime " & _
    '                "	,hremp_benefit_coverage.voidreason " & _
    '                "	,ISNULL(hrbenefitplan_master.benefitplanname,'') AS BPlanName " & _
    '                "	,ISNULL(cfcommon_master.name,'') AS BGroupName " & _
    '                "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '               "	,ISNULL(prtranhead_master.trnheadname,'') AS TranHead " & _
    '               "    ,hremp_benefit_coverage.tranheadunkid AS TranHeadId " & _
    '                "FROM hremp_benefit_coverage " & _
    '               "	LEFT JOIN prtranhead_master ON hremp_benefit_coverage.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "	LEFT JOIN hrbenefitplan_master ON hremp_benefit_coverage.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
    '                "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_benefit_coverage.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
    '                "	LEFT JOIN hremployee_master ON hremp_benefit_coverage.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE ISNULL(hremp_benefit_coverage.isvoid,0) = 0 "


    '        'Vimal (01 Nov 2010) -- End



    '        'Sandeep [ 21 Aug 2010 ] -- End 
    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_benefit_coverage) </purpose>
    Public Function Insert(ByVal strDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal dtEDTable As DataTable _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           ) As Boolean 'Sohail (23 Jan 2012) - [dtEDTable], Sohail (23 Apr 2012) - [strAllowToApproveEarningDeduction]
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        If isExist(mintEmployeeunkid, mintBenefitplanunkid, mintTranheadunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot assign same benefit plan or transaction head to same employee. Please assign new benefit plan or transaction head.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)
            'Vimal (01 Nov 2010) -- Start 
            'objDataOperation.AddParameter("@value_basis", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvalue_basis.ToString)
            'objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblpercentage.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblamount.ToString)
            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblamount)
            'Anjan (11 May 2011)-End 



            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            'If mdtEnd_Date = Nothing Then
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            'End If
            'Vimal (01 Nov 2010) -- End

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            'strQ = "INSERT INTO hremp_benefit_coverage ( " & _
            '              "  employeeunkid " & _
            '              ", benefitgroupunkid " & _
            '              ", benefitplanunkid " & _
            '              ", value_basis " & _
            '              ", percentage " & _
            '              ", amount " & _
            '              ", start_date " & _
            '              ", end_date " & _
            '              ", userunkid " & _
            '              ", isvoid " & _
            '              ", voiduserunkid " & _
            '              ", voiddatetime " & _
            '              ", voidreason" & _
            '          ") VALUES (" & _
            '              "  @employeeunkid " & _
            '              ", @benefitgroupunkid " & _
            '              ", @benefitplanunkid " & _
            '              ", @value_basis " & _
            '              ", @percentage " & _
            '              ", @amount " & _
            '              ", @start_date " & _
            '              ", @end_date " & _
            '              ", @userunkid " & _
            '              ", @isvoid " & _
            '              ", @voiduserunkid " & _
            '              ", @voiddatetime " & _
            '              ", @voidreason" & _
            '            "); SELECT @@identity"

            'Vimal (01 Nov 2010) -- Start 
            'strQ = "INSERT INTO hremp_benefit_coverage ( " & _
            '              "  employeeunkid " & _
            '              ", benefitgroupunkid " & _
            '              ", benefitplanunkid " & _
            '              ", value_basis " & _
            '              ", percentage " & _
            '              ", amount " & _
            '              ", start_date " & _
            '              ", end_date " & _
            '              ", userunkid " & _
            '              ", isvoid " & _
            '              ", voiduserunkid " & _
            '              ", voiddatetime " & _
            '              ", voidreason" & _
            '              ", tranheadunkid" & _
            '          ") VALUES (" & _
            '              "  @employeeunkid " & _
            '              ", @benefitgroupunkid " & _
            '              ", @benefitplanunkid " & _
            '              ", @value_basis " & _
            '              ", @percentage " & _
            '              ", @amount " & _
            '              ", @start_date " & _
            '              ", @end_date " & _
            '              ", @userunkid " & _
            '              ", @isvoid " & _
            '              ", @voiduserunkid " & _
            '              ", @voiddatetime " & _
            '              ", @voidreason" & _
            '              ", @tranheadunkid" & _
            '            "); SELECT @@identity"

            strQ = "INSERT INTO hremp_benefit_coverage ( " & _
                          "  employeeunkid " & _
                          ", benefitgroupunkid " & _
                          ", benefitplanunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                          ", tranheadunkid" & _
                      ") VALUES (" & _
                          "  @employeeunkid " & _
                          ", @benefitgroupunkid " & _
                          ", @benefitplanunkid " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                          ", @tranheadunkid" & _
                        "); SELECT @@identity"

            'Vimal (01 Nov 2010) -- End


            'Sandeep [ 21 Aug 2010 ] -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBenefitcoverageunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hremp_benefit_coverage", "benefitcoverageunkid", mintBenefitcoverageunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END




            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            ''Vimal (01 Nov 2010) -- Start 
            'Dim objEd As New clsEarningDeduction
            'Dim intEDUnkID As Integer = objEd.GetEDUnkID(mintEmployeeunkid, mintTranheadunkid)

            'Dim objTranHead As New clsTransactionHead
            'objTranHead._Tranheadunkid = mintTranheadunkid
            'If intEDUnkID > 0 Then
            '    objEd._Edunkid = intEDUnkID
            'End If

            'objEd._Tranheadunkid = mintTranheadunkid
            'objEd._Employeeunkid = mintEmployeeunkid
            'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
            'objEd._Typeof_Id = objTranHead._Typeof_id
            'objEd._Calctype_Id = objTranHead._Calctype_Id
            'objEd._Formula = objTranHead._Formula
            'objEd._FormulaId = objTranHead._Formulaid
            'objEd._Userunkid = objEd._Userunkid
            'objEd._Amount = mdecAmount
            'If intEDUnkID > 0 Then
            '    'Sohail (11 Nov 2010) -- Start
            '    'objEd.Update()
            '    objEd.Update(True)
            '    'Sohail (11 Nov 2010) -- End
            'Else
            '    'Sohail (14 Jun 2011) -- Start
            '    objEd._Isapproved = False
            '    objEd._Approveruserunkid = -1
            '    'Sohail (14 Jun 2011) -- End
            '    'Sohail (11 Nov 2010) -- Start
            '    'objEd.Insert()
            '    objEd.Insert(True)
            '    'Sohail (11 Nov 2010) -- End
            'End If
            If dtEDTable IsNot Nothing AndAlso dtEDTable.Rows.Count > 0 Then
                Dim objEd As New clsEarningDeduction
                With objEd
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                      'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                'If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid.ToString, dtEDTable, True, xUserUnkid, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, False, False, blnApplyUserAccessFilter, strFilerString) = False Then
                If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid.ToString, dtEDTable, True, xUserUnkid, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, , , blnApplyUserAccessFilter, strFilerString) = False Then
                    'Sohail (17 Sep 2019) -- End
                    'Sohail (21 Aug 2015) -- End
                    'If objEd.InsertAllByDataTable(mintEmployeeunkid.ToString, dtEDTable, True, True) = False Then
                    'Sohail (23 Apr 2012) -- End
                    Return False
                End If
            End If
            'Sohail (23 Jan 2012) -- End

            objDataOperation.ReleaseTransaction(True)
            'Vimal (01 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT - Overload funcion created as  Issue of bind transaction from employee master from aruti self service using new web commonlib.
    Public Function Insert(ByVal strDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal objDataOperation As clsDataOperation _
                           , ByVal dtEDTable As DataTable _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           ) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, blnApplyUserAccessFilter, strFilerString]

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        If isExist(mintEmployeeunkid, mintBenefitplanunkid, mintTranheadunkid, , objDataOperation) Then
            'If isExist(mintEmployeeunkid, mintBenefitplanunkid, mintTranheadunkid) Then
            'Sohail (23 Apr 2012) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot assign same benefit plan or transaction head to same employee. Please assign new benefit plan or transaction head.")

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation.ReleaseTransaction(False)
            If objDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [19 OCT 2016] -- END

            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            strQ = "INSERT INTO hremp_benefit_coverage ( " & _
                          "  employeeunkid " & _
                          ", benefitgroupunkid " & _
                          ", benefitplanunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                          ", tranheadunkid" & _
                      ") VALUES (" & _
                          "  @employeeunkid " & _
                          ", @benefitgroupunkid " & _
                          ", @benefitplanunkid " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                          ", @tranheadunkid" & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBenefitcoverageunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hremp_benefit_coverage", "benefitcoverageunkid", mintBenefitcoverageunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            If dtEDTable IsNot Nothing AndAlso dtEDTable.Rows.Count > 0 Then
                Dim objEd As New clsEarningDeduction
                With objEd
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid.ToString, dtEDTable, True, xUserUnkid, True, dtCurrentDateAndTime, , mintUserunkid, blnAllowToApproveEarningDeduction, blnApplyUserAccessFilter, strFilerString) = False Then
                    'Sohail (21 Aug 2015) -- End
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation.ReleaseTransaction(False)
            If objDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [19 OCT 2016] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            If objDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END

        End Try
    End Function
    'Sohail (23 Apr 2012) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_benefit_coverage) </purpose>
    Public Function Update(ByVal strDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As DateTime _
                                     , ByVal xPeriodEnd As DateTime _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal dtEDTable As DataTable _
                                     , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                     , ByVal dtCurrentDateAndTime As Date _
                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                     , Optional ByVal strFilerString As String = "" _
                                     ) As Boolean 'Sohail (23 Jan 2012) - [dtEDTable]
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        If isExist(mintEmployeeunkid, mintBenefitplanunkid, mintTranheadunkid, mintBenefitcoverageunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot assign same benefit plan or transaction head to same employee. Please assign new benefit plan or transaction head.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitcoverageunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)

            'Vimal (01 Nov 2010) -- Start 
            'objDataOperation.AddParameter("@value_basis", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvalue_basis.ToString)
            'objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblpercentage.ToString)


            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblamount.ToString)
            'objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblamount)
            'Anjan (11 May 2011)-End 


            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            'If mdtEnd_Date = Nothing Then
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            'End If
            'Vimal (01 Nov 2010) -- End

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Sandeep [ 21 Aug 2010 ] -- Start
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            'strQ = "UPDATE hremp_benefit_coverage SET " & _
            '         "  employeeunkid = @employeeunkid" & _
            '         ", benefitgroupunkid = @benefitgroupunkid" & _
            '         ", benefitplanunkid = @benefitplanunkid" & _
            '         ", value_basis = @value_basis" & _
            '         ", percentage = @percentage" & _
            '         ", amount = @amount" & _
            '         ", start_date = @start_date" & _
            '         ", end_date = @end_date" & _
            '         ", userunkid = @userunkid" & _
            '         ", isvoid = @isvoid" & _
            '         ", voiduserunkid = @voiduserunkid" & _
            '         ", voiddatetime = @voiddatetime" & _
            '         ", voidreason = @voidreason " & _
            '        "WHERE benefitcoverageunkid = @benefitcoverageunkid "

            'Vimal (01 Nov 2010) -- Start 
            'strQ = "UPDATE hremp_benefit_coverage SET " & _
            '            "  employeeunkid = @employeeunkid" & _
            '            ", benefitgroupunkid = @benefitgroupunkid" & _
            '            ", benefitplanunkid = @benefitplanunkid" & _
            '            ", value_basis = @value_basis" & _
            '            ", percentage = @percentage" & _
            '            ", amount = @amount" & _
            '            ", start_date = @start_date" & _
            '            ", end_date = @end_date" & _
            '            ", userunkid = @userunkid" & _
            '            ", isvoid = @isvoid" & _
            '            ", voiduserunkid = @voiduserunkid" & _
            '            ", voiddatetime = @voiddatetime" & _
            '            ", voidreason = @voidreason " & _
            '       ", tranheadunkid = @tranheadunkid " & _
            '          "WHERE benefitcoverageunkid = @benefitcoverageunkid "

            strQ = "UPDATE hremp_benefit_coverage SET " & _
                          "  employeeunkid = @employeeunkid" & _
                          ", benefitgroupunkid = @benefitgroupunkid" & _
                          ", benefitplanunkid = @benefitplanunkid" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                     ", tranheadunkid = @tranheadunkid " & _
                        "WHERE benefitcoverageunkid = @benefitcoverageunkid "
            'Vimal (01 Nov 2010) -- End

            'Sandeep [ 21 Aug 2010 ] -- End 

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hremp_benefit_coverage", mintBenefitcoverageunkid, "benefitcoverageunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hremp_benefit_coverage", "benefitcoverageunkid", mintBenefitcoverageunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            ''Vimal (01 Nov 2010) -- Start 
            'Dim objEd As New clsEarningDeduction
            'Dim intEDUnkID As Integer = objEd.GetEDUnkID(mintEmployeeunkid, intOldTranhedunkid)

            'Dim objTranHead As New clsTransactionHead
            'objTranHead._Tranheadunkid = mintTranheadunkid
            'If intEDUnkID > 0 Then
            '    objEd._Edunkid = intEDUnkID
            'End If

            'objEd._Tranheadunkid = mintTranheadunkid
            'objEd._Employeeunkid = mintEmployeeunkid
            'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
            'objEd._Typeof_Id = objTranHead._Typeof_id
            'objEd._Calctype_Id = objTranHead._Calctype_Id
            'objEd._Formula = objTranHead._Formula
            'objEd._FormulaId = objTranHead._Formulaid
            'objEd._Userunkid = objEd._Userunkid
            'objEd._Amount = mdecAmount
            'If intEDUnkID > 0 Then
            '    'Sohail (11 Nov 2010) -- Start
            '    'objEd.Update()
            '    objEd.Update(True)
            '    'Sohail (11 Nov 2010) -- End
            'Else
            '    'Sohail (14 Jun 2011) -- Start
            '    objEd._Isapproved = False
            '    objEd._Approveruserunkid = -1
            '    'Sohail (14 Jun 2011) -- End
            '    'Sohail (11 Nov 2010) -- Start
            '    'objEd.Insert()
            '    objEd.Insert(True)
            '    'Sohail (11 Nov 2010) -- End
            'End If
            If dtEDTable IsNot Nothing AndAlso dtEDTable.Rows.Count > 0 Then
                Dim objEd As New clsEarningDeduction
                With objEd
                    ._FormName = mstrFormName
                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FromWeb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                    ._AuditDate = mdtAuditDate
                End With
                If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeunkid.ToString, dtEDTable, True, xUserUnkid, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, , , blnApplyUserAccessFilter, strFilerString) = False Then

                    If objEd._Message <> "" Then
                        Throw New Exception(objEd._Message)
                    End If

                    'Pinkal (13-Mar-2018) -- End

                    'Sohail (21 Aug 2015) -- End
                    Return False
                End If
            End If
            'Sohail (23 Jan 2012) -- End

            objDataOperation.ReleaseTransaction(True)
            'Vimal (01 Nov 2010) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function



    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (hremp_benefit_coverage) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_benefit_coverage) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intEmpId As Integer _
                           , Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean 'Sohail (23 Apr 2012) - [objDataOperation]
        'S.SANDEEP [ 23 JAN 2012 ] -- END

        'Sandeep | 24 JAN 2011 | -- START
        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        If isUsed(intUnkid, intEmpId, objDataOperation) Then
            'If isUsed(intUnkid, intEmpId) Then
            'Sohail (23 Apr 2012) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this benefit. Reason : This benefit is already used in some transactions.")
            Return False
        End If
        'Sandeep | 24 JAN 2011 | -- END 



        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End



        Try
            strQ = "UPDATE hremp_benefit_coverage SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                        "WHERE benefitcoverageunkid = @benefitcoverageunkid "

            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hremp_benefit_coverage", "benefitcoverageunkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            objDataOperation.ReleaseTransaction(True)

            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT - Overload funcion created as  Issue of bind transaction from employee master from aruti self service using new web commonlib.
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer, ByVal intEmpId As Integer) As Boolean

        If isUsed(intUnkid, intEmpId, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this benefit. Reason : This benefit is already used in some transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremp_benefit_coverage SET " & _
                          "  isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                        "WHERE benefitcoverageunkid = @benefitcoverageunkid "

            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "hremp_benefit_coverage", "benefitcoverageunkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (23 Apr 2012) -- End



    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal intEmpId As Integer _
                           , Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean 'Sohail (23 Apr 2012) - [objDataOperation]
        'S.SANDEEP [ 23 JAN 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'objDataOperation = New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (23 Apr 2012) -- End

        Try
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
            'strQ = "SELECT " & _
            '       " edunkid " & _
            '       "FROM prearningdeduction_master " & _
            '       "WHERE tranheadunkid IN (SELECT tranheadunkid FROM hremp_benefit_coverage WHERE benefitcoverageunkid = @benefitcoverageunkid) " & _
            '       " AND prearningdeduction_master.isvoid = 0 "

            'S.SANDEEP [ 23 JAN 2012 ] -- END
            strQ = "SELECT " & _
                   " edunkid " & _
                   "FROM prearningdeduction_master " & _
                   " JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "WHERE tranheadunkid IN (SELECT tranheadunkid FROM hremp_benefit_coverage WHERE benefitcoverageunkid = @benefitcoverageunkid) " & _
                   " AND prearningdeduction_master.isvoid = 0 AND prearningdeduction_master.employeeunkid = @EmpId " & _
                   " AND cfcommon_period_tran.statusid = 1 "
            'Sandeep | 24 JAN 2011 | -- END 

            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 21 Aug 2010 ] -- Start
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal intEmpId As Integer, ByVal intBenefitId As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '                      "  benefitcoverageunkid " & _
    '                      ", employeeunkid " & _
    '                      ", benefitgroupunkid " & _
    '                      ", benefitplanunkid " & _
    '                      ", value_basis " & _
    '                      ", percentage " & _
    '                      ", amount " & _
    '                      ", start_date " & _
    '                      ", end_date " & _
    '                      ", userunkid " & _
    '                      ", isvoid " & _
    '                      ", voiduserunkid " & _
    '                      ", voiddatetime " & _
    '                      ", voidreason " & _
    '                      ", tranheadunkid " & _
    '                "FROM hremp_benefit_coverage " & _
    '                "WHERE employeeunkid = @EmpId " & _
    '                "AND benefitplanunkid = @BenefitId " & _
    '                "AND ISNULL(isvoid,0) = 0"

    '        If intUnkid > 0 Then
    '            strQ &= " AND benefitcoverageunkid <> @benefitcoverageunkid"
    '        End If

    '        objDataOperation.AddParameter("@EmpId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpId)
    '        objDataOperation.AddParameter("@BenefitId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intBenefitId)
    '        objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 

    'Vimal (01 Nov 2010) -- Start 
    'Public Function isExist(ByVal intEmpId As Integer, ByVal intBenefitId As String,  ByVal intTranHeadId As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal intEmpId As Integer, ByVal intBenefitId As String, Optional ByVal intTranHeadId As Integer = -1, Optional ByVal intUnkid As Integer = -1 _
                            , Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean 'Sohail (23 Apr 2012) - [objDataOperation]
        'Vimal (01 Nov 2010) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (23 Apr 2012) -- Start
        'TRA - ENHANCEMENT
        'objDataOperation = New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Sohail (23 Apr 2012) -- End

        Try
            'Vimal (01 Nov 2010) -- Start 
            'strQ = "SELECT " & _
            '            "  benefitcoverageunkid " & _
            '            ", employeeunkid " & _
            '            ", benefitgroupunkid " & _
            '            ", benefitplanunkid " & _
            '            ", value_basis " & _
            '            ", percentage " & _
            '            ", amount " & _
            '            ", start_date " & _
            '            ", end_date " & _
            '            ", userunkid " & _
            '            ", isvoid " & _
            '            ", voiduserunkid " & _
            '            ", voiddatetime " & _
            '            ", voidreason " & _
            '            ", tranheadunkid " & _
            '      "FROM hremp_benefit_coverage " & _
            '      "WHERE employeeunkid = @EmpId " & _
            '      "AND benefitplanunkid = @BenefitId " & _
            '      "AND tranheadunkid = @TranHeadId " & _
            '      "AND ISNULL(isvoid,0) = 0"

            strQ = "SELECT " & _
                          "  benefitcoverageunkid " & _
                          ", employeeunkid " & _
                          ", benefitgroupunkid " & _
                          ", benefitplanunkid " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason " & _
                          ", tranheadunkid " & _
                    "FROM hremp_benefit_coverage " & _
                    "WHERE employeeunkid = @EmpId " & _
                  "AND (benefitplanunkid = @BenefitId " & _
                  "OR tranheadunkid = @TranHeadId) " & _
                    "AND ISNULL(isvoid,0) = 0"

            'Vimal (01 Nov 2010) -- End


            If intUnkid > 0 Then
                strQ &= " AND benefitcoverageunkid <> @benefitcoverageunkid"
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpId)
            objDataOperation.AddParameter("@BenefitId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intBenefitId)
            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (19 May 2012) -- Start
    'TRA - ENHANCEMENT

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function AssignGroupBenefit(ByVal strIds As String, ByVal mdtTable As DataTable, ByVal blnCopyPreviousEDSlab As Boolean _
    '                                       , ByVal blnOverwritePreviousEDSlabHeads As Boolean) As Boolean
    '    '                              'Sohail (26 Jul 2012) - [blnOverwritePreviousEDSlabHeads]
    '    'Public Function AssignGroupBenefit(ByVal strIds As String, ByVal mdtTable As DataTable) As Boolean
    '    'Sohail (19 May 2012) -- End

    Public Function AssignGroupBenefit(ByVal strIds As String, _
                                       ByVal mdtTable As DataTable, _
                                       ByVal blnCopyPreviousEDSlab As Boolean, _
                                       ByVal blnOverwritePreviousEDSlabHeads As Boolean, _
                                       ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xPeriodStart As DateTime, _
                                       ByVal xPeriodEnd As DateTime, _
                                       ByVal xUserModeSetting As String, _
                                       ByVal xOnlyApproved As Boolean, _
                                       ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                       ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                       ByVal dtCurrentDateTime As Date, _
                                       ByVal blnOverWriteEDHeadIfExist As Boolean _
                                       ) As Boolean
        'Sohail (19 Mar 2016) - [blnOverWriteEDHeadIfExist]
        'Sohail (21 Aug 2015) - [blnAllowToApproveEarningDeduction]
        'S.SANDEEP [04 JUN 2015] -- END


        Dim i As Integer = 0
        Dim strEmployeeIds() As String = Nothing
        'Vimal (01 Nov 2010) -- Start 
        Dim objDataOperation As clsDataOperation
        'Vimal (01 Nov 2010) -- End
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            If strIds.Length <= 0 Then Exit Function

            strEmployeeIds = strIds.Split(",")

            For i = 0 To strEmployeeIds.Length - 1
                For j As Integer = 0 To mdtTable.Rows.Count - 1
                    With mdtTable.Rows(j)
                        'Vimal (01 Nov 2010) -- Start 
                        'If IsBenefitPresent(objDataOperation, strEmployeeIds(i), mdtTable.Rows(j)("benefitplanunkid")) = True Then
                        If IsBenefitPresent(objDataOperation, strEmployeeIds(i), mdtTable.Rows(j)("benefitplanunkid"), mdtTable.Rows(j)("tranheadunkid")) = True Then
                            'If mdtTable.Rows(j)("actionid") = 2 Then
                            'Sohail (23 Jan 2012) -- Start
                            'TRA - ENHANCEMENT
                            If mdtTable.Rows(j)("actionid") = 1 OrElse mdtTable.Rows(j)("actionid") = 0 Then
                                'If mdtTable.Rows(j)("actionid") = 1 Then
                                'Sohail (23 Jan 2012) -- End
                                'Vimal (01 Nov 2010) -- End
                                If Not IsDBNull(.Item("AUD")) Then
                                    Select Case .Item("AUD")
                                        Case "A"

                                            'Vimal (01 Nov 2010) -- Start

                                            'S.SANDEEP [04 JUN 2015] -- START
                                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                            'Dim ds As DataSet = GetList("Benefit", strEmployeeIds(i), mdtTable.Rows(j)("benefitplanunkid"))
                                            Dim ds As DataSet = GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Benefit", True, strEmployeeIds(i), mdtTable.Rows(j)("benefitplanunkid"))
                                            'S.SANDEEP [04 JUN 2015] -- END

                                            If ds.Tables("Benefit").Rows.Count > 0 Then
                                                intOldTranhedunkid = ds.Tables("Benefit").Rows(0)("TranHeadId")
                                            Else
                                                intOldTranhedunkid = 0
                                            End If
                                            'Vimal (01 Nov 2010) -- End
                                            If mdtTable.Rows(j)("actionid") = 1 Then 'Sohail (23 Jan 2012)
                                                Call UpdateBenefitGroup(objDataOperation, strEmployeeIds(i), mdtTable.Rows(j))
                                            End If 'Sohail (23 Jan 2012)
                                            'Vimal (01 Nov 2010) -- Start 
                                            'Dim objEd As New clsEarningDeduction
                                            'Dim intEDUnkID As Integer = objEd.GetEDUnkID(strEmployeeIds(i), mdtTable.Rows(j)("tranheadunkid"))
                                            'Dim objTranHead As New clsTransactionHead
                                            'objTranHead._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                            'objEd._Edunkid = strEmployeeIds(i)
                                            'objEd._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                            'objEd._Employeeunkid = strEmployeeIds(i)
                                            'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                                            'objEd._Typeof_Id = objTranHead._Typeof_id
                                            'objEd._Calctype_Id = objTranHead._Calctype_Id
                                            'objEd._Formula = objTranHead._Formula
                                            'objEd._FormulaId = objTranHead._Formulaid
                                            'objEd._Userunkid = objEd._Userunkid
                                            'objEd.Update()

                                            Dim objEd As New clsEarningDeduction
                                            'Dim intEDUnkID As Integer 'Sohail (19 May 2012)
                                            'Sohail (23 Jan 2012) -- Start
                                            'TRA - ENHANCEMENT
                                            'If objEd.isExist(strEmployeeIds(i), intOldTranhedunkid, , CInt(mdtTable.Rows(j)("periodunkid"))) = True Then
                                            'intEDUnkID = objEd.GetEDUnkID(strEmployeeIds(i), intOldTranhedunkid)
                                            'Else
                                            'intEDUnkID = objEd.GetEDUnkID(strEmployeeIds(i), mdtTable.Rows(j)("tranheadunkid"))
                                            'Sohail (19 May 2012) -- Start
                                            'TRA - ENHANCEMENT
                                            'intEDUnkID = objEd.GetEDUnkID(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), CInt(mdtTable.Rows(j)("periodunkid")))
                                            ''End If
                                            ''Sohail (23 Jan 2012) -- End

                                            'Dim objTranHead As New clsTransactionHead
                                            'objTranHead._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                            'If intEDUnkID > 0 Then
                                            '    objEd._Edunkid = intEDUnkID
                                            '    'Sohail (23 Jan 2012) -- Start
                                            '    'TRA - ENHANCEMENT
                                            'Else
                                            '    objEd._CostCenterUnkID = 0
                                            '    objEd._MedicalRefNo = ""
                                            '    'Sohail (23 Jan 2012) -- End
                                            'End If
                                            'objEd._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                            'objEd._Employeeunkid = strEmployeeIds(i)
                                            'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                                            'objEd._Typeof_Id = objTranHead._Typeof_id
                                            'objEd._Calctype_Id = objTranHead._Calctype_Id
                                            'objEd._Formula = objTranHead._Formula
                                            'objEd._FormulaId = objTranHead._Formulaid
                                            'objEd._Userunkid = objEd._Userunkid
                                            'objEd._Amount = mdtTable.Rows(j)("amount")
                                            ''Sohail (23 Jan 2012) -- Start
                                            ''TRA - ENHANCEMENT
                                            'objEd._Periodunkid = CInt(mdtTable.Rows(j)("periodunkid"))
                                            ''Sohail (23 Jan 2012) -- End
                                            'If intEDUnkID > 0 Then
                                            '    'Sohail (11 Nov 2010) -- Start
                                            '    'objEd.Update()
                                            '    'Sohail (23 Jan 2012) -- Start
                                            '    'TRA - ENHANCEMENT
                                            '    'objEd.Update(True)
                                            '    If mdtTable.Rows(j)("actionid") = 1 Then
                                            'objEd.Update(True)
                                            '    End If
                                            '    'Sohail (23 Jan 2012) -- End
                                            '    'Sohail (11 Nov 2010) -- End
                                            'Else
                                            '    'Sohail (14 Jun 2011) -- Start
                                            '    'Sohail (23 Jan 2012) -- Start
                                            '    'TRA - ENHANCEMENT
                                            '    'objEd._Isapproved = False
                                            '    'objEd._Approveruserunkid = -1
                                            '    If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                                            '        objEd._Isapproved = True
                                            '        objEd._Approveruserunkid = User._Object._Userunkid
                                            '    Else
                                            'objEd._Isapproved = False
                                            'objEd._Approveruserunkid = -1
                                            '    End If
                                            '    'Sohail (23 Jan 2012) -- End
                                            '    'Sohail (14 Jun 2011) -- End
                                            '    'Sohail (11 Nov 2010) -- Start
                                            '    'objEd.Insert()
                                            '    objEd.Insert(True)
                                            '    'Sohail (11 Nov 2010) -- End
                                            'End If
                                            'Sohail (26 Jul 2012) -- Start
                                            'TRA - ENHANCEMENT
                                            'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", True, blnCopyPreviousEDSlab) = False Then
                                            'Sohail (09 Nov 2013) -- Start
                                            'TRA - ENHANCEMENT
                                            'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            'Sohail (21 Aug 2015) -- Start
                                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                            'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            'Sohail (19 Mar 2016) -- Start
                                            'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
                                            'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            'Sohail (01 Mar 2018) -- Start
                                            'Internal Issue - ARUTI-30 : In Assign Benift Group In You Copy Previous ED Slab Option Data Not Copy Perfectly ,Discription And Steps Are Given In Image in 70.1.
                                            'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            Dim blnCopyPrevSlab As Boolean = blnCopyPreviousEDSlab
                                            If Not (j = mdtTable.Rows.Count - 1) Then
                                                blnCopyPrevSlab = False
                                            End If

                                            With objEd
                                                ._FormName = mstrFormName
                                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                                ._ClientIP = mstrClientIP
                                                ._HostName = mstrHostName
                                                ._FromWeb = mblnIsWeb
                                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                ._AuditDate = mdtAuditDate
                                            End With

                                            'Sohail (15 Dec 2018) -- Start
                                            'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                                            'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPrevSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateTime, True, blnCopyPrevSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                                'Sohail (15 Dec 2018) -- End
                                                'Sohail (01 Mar 2018) -- End
                                                'Sohail (19 Mar 2016) -- End
                                                'Sohail (21 Aug 2015) -- End
                                                'Sohail (09 Nov 2013) -- End
                                                'Sohail (26 Jul 2012) -- End
                                                objDataOperation.ReleaseTransaction(False)
                                                Throw New Exception(objEd._Message)
                                            End If
                                            'Sohail (19 May 2012) -- End

                                            Dim k As Integer = 0
                                            Dim strPeriodIds As String = mdtTable.Rows(j)("exemptperiodid")
                                            Dim strExemPeriodIds() As String = Nothing
                                            strExemPeriodIds = strPeriodIds.Split(",")
                                            For k = 0 To strExemPeriodIds.Length - 1
                                                If strExemPeriodIds(k) = "" Then Continue For
                                                Dim objExempt As New clsemployee_exemption_Tran
                                                Dim objPeriod As New clscommom_period_Tran
                                                objExempt._Employeeunkid = strEmployeeIds(i)
                                                objExempt._Periodunkid = CInt(strExemPeriodIds(k))
                                                'Sohail (21 Aug 2015) -- Start
                                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                                'objPeriod._Periodunkid = objExempt._Periodunkid
                                                objPeriod._Periodunkid(xDatabaseName) = objExempt._Periodunkid
                                                'Sohail (21 Aug 2015) -- End
                                                objExempt._Yearunkid = objPeriod._Yearunkid
                                                objExempt._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                                objExempt._Userunkid = User._Object._Userunkid
                                                With objExempt
                                                    ._FormName = mstrFormName
                                                    ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                                    ._ClientIP = mstrClientIP
                                                    ._HostName = mstrHostName
                                                    ._FromWeb = mblnIsWeb
                                                    ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                    ._AuditDate = mdtAuditDate
                                                End With                                               
                                                'Sohail (21 Aug 2015) -- Start
                                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                                'objExempt.InsertByPeriod(strEmployeeIds(i), strExemPeriodIds(k))
                                                objExempt.InsertByPeriod(strEmployeeIds(i), strExemPeriodIds(k), dtCurrentDateTime)
                                                'Sohail (21 Aug 2015) -- End
                                            Next
                                            'Vimal (01 Nov 2010) -- End
                                    End Select
                                End If
                            End If
                        Else
                            'Vimal (01 Nov 2010) -- Start 
                            'If mdtTable.Rows(j)("actionid") = 3 Then
                            'If mdtTable.Rows(j)("actionid") = 0 Then
                            'Vimal (01 Nov 2010) -- End
                            If Not IsDBNull(.Item("AUD")) Then
                                Select Case .Item("AUD")
                                    Case "A"
                                        Call InsertBenefitGroup(objDataOperation, strEmployeeIds(i), mdtTable.Rows(j))
                                        'Vimal (01 Nov 2010) -- Start 
                                        'Dim objEd As New clsEarningDeduction
                                        'Dim objTranHead As New clsTransactionHead
                                        'objTranHead._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")

                                        'objEd._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                        'objEd._Employeeunkid = strEmployeeIds(i)
                                        'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                                        'objEd._Typeof_Id = objTranHead._Typeof_id
                                        'objEd._Calctype_Id = objTranHead._Calctype_Id
                                        'objEd._Formula = objTranHead._Formula
                                        'objEd._FormulaId = objTranHead._Formulaid
                                        'objEd._Userunkid = User._Object._Userunkid
                                        'objEd.Insert()

                                        Dim objEd As New clsEarningDeduction
                                        'Sohail (23 Jan 2012) -- Start
                                        'TRA - ENHANCEMENT
                                        'Dim intEDUnkID As Integer = objEd.GetEDUnkID(strEmployeeIds(i), mdtTable.Rows(j)("tranheadunkid"))
                                        'Sohail (19 May 2012) -- Start
                                        'TRA - ENHANCEMENT
                                        'Dim intEDUnkID As Integer = objEd.GetEDUnkID(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), CInt(mdtTable.Rows(j)("periodunkid")))
                                        ''Sohail (23 Jan 2012) -- End
                                        'Dim objTranHead As New clsTransactionHead
                                        'objTranHead._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                        'If intEDUnkID > 0 Then
                                        '    objEd._Edunkid = intEDUnkID
                                        '    'Sohail (23 Jan 2012) -- Start
                                        '    'TRA - ENHANCEMENT
                                        'Else
                                        '    objEd._CostCenterUnkID = 0
                                        '    objEd._MedicalRefNo = ""
                                        '    'Sohail (23 Jan 2012) -- End
                                        'End If
                                        'objEd._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                        'objEd._Employeeunkid = strEmployeeIds(i)
                                        'objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                                        'objEd._Typeof_Id = objTranHead._Typeof_id
                                        'objEd._Calctype_Id = objTranHead._Calctype_Id
                                        'objEd._Formula = objTranHead._Formula
                                        'objEd._FormulaId = objTranHead._Formulaid
                                        'objEd._Userunkid = objEd._Userunkid
                                        'objEd._Amount = mdtTable.Rows(j)("amount")
                                        ''Sohail (23 Jan 2012) -- Start
                                        ''TRA - ENHANCEMENT
                                        'objEd._Periodunkid = CInt(mdtTable.Rows(j)("periodunkid"))
                                        ''Sohail (23 Jan 2012) -- End
                                        'If intEDUnkID > 0 Then
                                        '    'Sohail (11 Nov 2010) -- Start
                                        '    'objEd.Update()
                                        '    objEd.Update(True)
                                        '    'Sohail (11 Nov 2010) -- End
                                        'Else
                                        '    'Sohail (14 Jun 2011) -- Start
                                        '    'Sohail (23 Jan 2012) -- Start
                                        '    'TRA - ENHANCEMENT
                                        '    'objEd._Isapproved = False
                                        '    'objEd._Approveruserunkid = -1
                                        '    If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                                        '        objEd._Isapproved = True
                                        '        objEd._Approveruserunkid = User._Object._Userunkid
                                        '    Else
                                        'objEd._Isapproved = False
                                        'objEd._Approveruserunkid = -1
                                        '    End If
                                        '    'Sohail (23 Jan 2012) -- End

                                        '    'Sohail (14 Jun 2011) -- End
                                        '    'Sohail (11 Nov 2010) -- Start
                                        '    'objEd.Insert()
                                        '    'Sohail (23 Jan 2012) -- Start
                                        '    'TRA - ENHANCEMENT
                                        '    'objEd.Insert(True)
                                        '    objEd.Insert(True, True)
                                        '    'Sohail (23 Jan 2012) -- End
                                        '    'Sohail (11 Nov 2010) -- End
                                        'End If
                                        'Sohail (26 Jul 2012) -- Start
                                        'TRA - ENHANCEMENT
                                        'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", True, blnCopyPreviousEDSlab) = False Then
                                        'Sohail (09 Nov 2013) -- Start
                                        'TRA - ENHANCEMENT
                                        'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        'If objEd.InsertAllByEmployeeList(strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                        'Sohail (19 Mar 2016) -- Start
                                        'Enhancement - 58.1 - Allow to map all types of heads with Benefit Plan in Benefit allocation.
                                        'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), CBool(mdtTable.Rows(j)("actionid")), False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                        'Sohail (01 Mar 2018) -- Start
                                        'Internal Issue - ARUTI-30 : In Assign Benift Group In You Copy Previous ED Slab Option Data Not Copy Perfectly ,Discription And Steps Are Given In Image in 70.1.
                                        'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPreviousEDSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                        Dim blnCopyPrevSlab As Boolean = blnCopyPreviousEDSlab
                                        If Not (j = mdtTable.Rows.Count - 1) Then
                                            blnCopyPrevSlab = False
                                        End If
                                        With objEd
                                            ._FormName = mstrFormName
                                            ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                            ._ClientIP = mstrClientIP
                                            ._HostName = mstrHostName
                                            ._FromWeb = mblnIsWeb
                                            ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                            ._AuditDate = mdtAuditDate
                                        End With
                                        'Sohail (15 Dec 2018) -- Start
                                        'HJFMRI Enhancement - Copy Selected Period ED Slab option on Global Assign ED screen in 75.1.
                                        'If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, dtCurrentDateTime, True, blnCopyPrevSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                        If objEd.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, strEmployeeIds(i), CInt(mdtTable.Rows(j)("tranheadunkid")), 0, 0, -1, CDec(mdtTable.Rows(j)("amount")), blnOverWriteEDHeadIfExist, False, CInt(mdtTable.Rows(j)("periodunkid")), "", Nothing, Nothing, blnAllowToApproveEarningDeduction, objDataOperation, dtCurrentDateTime, True, blnCopyPrevSlab, blnOverwritePreviousEDSlabHeads) = False Then
                                            'Sohail (15 Dec 2018) -- End
                                            'Sohail (01 Mar 2018) -- End
                                            'Sohail (19 Mar 2016) -- End
                                            'Sohail (21 Aug 2015) -- End
                                            'Sohail (09 Nov 2013) -- End
                                            'Sohail (26 Jul 2012) -- End
                                            objDataOperation.ReleaseTransaction(False)
                                            Throw New Exception(objEd._Message)
                                        End If
                                        'Sohail (19 May 2012) -- End

                                        Dim k As Integer = 0
                                        Dim strPeriodIds As String = mdtTable.Rows(j)("exemptperiodid")
                                        Dim strExemPeriodIds() As String = Nothing
                                        strExemPeriodIds = strPeriodIds.Split(",")
                                        For k = 0 To strExemPeriodIds.Length - 1
                                            If strExemPeriodIds(k) = "" Then Continue For
                                            Dim objExempt As New clsemployee_exemption_Tran
                                            Dim objPeriod As New clscommom_period_Tran
                                            objExempt._Employeeunkid = strEmployeeIds(i)
                                            objExempt._Periodunkid = strExemPeriodIds(k)
                                            'Sohail (21 Aug 2015) -- Start
                                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                            'objPeriod._Periodunkid = objExempt._Periodunkid
                                            objPeriod._Periodunkid(xDatabaseName) = objExempt._Periodunkid
                                            'Sohail (21 Aug 2015) -- End
                                            objExempt._Yearunkid = objPeriod._Yearunkid
                                            objExempt._Tranheadunkid = mdtTable.Rows(j)("tranheadunkid")
                                            objExempt._Userunkid = User._Object._Userunkid
                                            With objExempt
                                                ._FormName = mstrFormName
                                                ._LoginEmployeeUnkid = mintLoginEmployeeunkid
                                                ._ClientIP = mstrClientIP
                                                ._HostName = mstrHostName
                                                ._FromWeb = mblnIsWeb
                                                ._AuditUserId = mintAuditUserId
._CompanyUnkid = mintCompanyUnkid
                                                ._AuditDate = mdtAuditDate
                                            End With
                                            objExempt.InsertByPeriod(strEmployeeIds(i), strExemPeriodIds(k), dtCurrentDateTime)
                                            'Sohail (21 Aug 2015) -- End
                                        Next
                                        'Vimal (01 Nov 2010) -- End
                                End Select
                            End If
                            'End If
                        End If
                    End With
                Next
            Next
            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "AssignGroupBenefit", mstrModuleName)
        End Try
    End Function

    'Vimal (01 Nov 2010) -- Start 
    'Private Function IsBenefitPresent(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal intBenefitId As Integer) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As DataSet = Nothing
    '    Dim exForce As Exception
    '    Try
    '        StrQ = "SELECT " & _
    '                "   employeeunkid " & _
    '                "  ,benefitplanunkid " & _
    '                "  ,benefitcoverageunkid " & _
    '                "FROM hremp_benefit_coverage " & _
    '                "WHERE ISNULL(hremp_benefit_coverage.isvoid,0)= 0 " & _
    '                "   AND hremp_benefit_coverage.employeeunkid = @EmpId " & _
    '                "   AND hremp_benefit_coverage.benefitplanunkid = @BenefitId "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
    '        objDataOperation.AddParameter("@BenefitId", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitId.ToString)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            mintGroupAssignBenefitGroupId = dsList.Tables(0).Rows(0)("benefitcoverageunkid")
    '            Return True
    '        Else
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "IsBenefitPresent", mstrModuleName)
    '    End Try
    'End Function

    Private Function IsBenefitPresent(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal intBenefitId As Integer, ByVal intTranHeadId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            StrQ = "SELECT " & _
                    "   employeeunkid " & _
                    "  ,benefitplanunkid " & _
                    "  ,benefitcoverageunkid " & _
                    "  ,tranheadunkid " & _
                    "FROM hremp_benefit_coverage " & _
                    "WHERE ISNULL(hremp_benefit_coverage.isvoid,0)= 0 " & _
                    "   AND hremp_benefit_coverage.employeeunkid = @EmpId " & _
                    "   AND (hremp_benefit_coverage.benefitplanunkid = @BenefitId " & _
                    "   OR hremp_benefit_coverage.tranheadunkid = @TranHeadId) "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@BenefitId", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitId.ToString)
            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintGroupAssignBenefitGroupId = dsList.Tables(0).Rows(0)("benefitcoverageunkid")
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsBenefitPresent", mstrModuleName)
        End Try
    End Function
    'Vimal (01 Nov 2010) -- End




    Private Function InsertBenefitGroup(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal dtRow As DataRow) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            'Vimal (01 Nov 2010) -- Start 
            'StrQ = "INSERT INTO hremp_benefit_coverage ( " & _
            '           "  employeeunkid " & _
            '           ", benefitgroupunkid " & _
            '           ", benefitplanunkid " & _
            '           ", value_basis " & _
            '           ", percentage " & _
            '           ", amount " & _
            '           ", start_date " & _
            '           ", end_date " & _
            '           ", userunkid " & _
            '           ", isvoid " & _
            '           ", voiduserunkid " & _
            '           ", voiddatetime " & _
            '           ", voidreason" & _
            '           ", tranheadunkid" & _
            '       ") VALUES (" & _
            '           "  @employeeunkid " & _
            '           ", @benefitgroupunkid " & _
            '           ", @benefitplanunkid " & _
            '           ", @value_basis " & _
            '           ", @percentage " & _
            '           ", @amount " & _
            '           ", @start_date " & _
            '           ", @end_date " & _
            '           ", @userunkid " & _
            '           ", @isvoid " & _
            '           ", @voiduserunkid " & _
            '           ", @voiddatetime " & _
            '           ", @voidreason" & _
            '           ", @tranheadunkid" & _
            '        "); SELECT @@identity"

            StrQ = "INSERT INTO hremp_benefit_coverage ( " & _
                        "  employeeunkid " & _
                        ", benefitgroupunkid " & _
                        ", benefitplanunkid " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                        ", tranheadunkid" & _
                    ") VALUES (" & _
                        "  @employeeunkid " & _
                        ", @benefitgroupunkid " & _
                        ", @benefitplanunkid " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                        ", @tranheadunkid" & _
                     "); SELECT @@identity"
            'Vimal (01 Nov 2010) -- End




            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("benefitgroupunkid").ToString)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("benefitplanunkid").ToString)

            'Vimal (01 Nov 2010) -- Start 
            'objDataOperation.AddParameter("@value_basis", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("valueid").ToString)
            'objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dtRow.Item("percent").ToString)
            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dtRow.Item("amount").ToString)
            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtRow.Item("startdate").ToString)
            'If IsDBNull(dtRow.Item("enddate")) Then
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtRow.Item("enddate").ToString)
            'End If
            'Vimal (01 Nov 2010) -- End
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("tranheadunkid").ToString)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'Call objDataOperation.ExecNonQuery(StrQ)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then mintBenefitcoverageunkid = dsList.Tables(0).Rows(0)(0)

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "hremp_benefit_coverage", "benefitcoverageunkid", mintBenefitcoverageunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertBenefitGroup", mstrModuleName)
        End Try
    End Function

    Private Function UpdateBenefitGroup(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal dtRow As DataRow) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            'Vimal (01 Nov 2010) -- Start 
            'StrQ = "UPDATE hremp_benefit_coverage SET " & _
            '        "  employeeunkid = @employeeunkid" & _
            '        ", benefitgroupunkid = @benefitgroupunkid" & _
            '        ", benefitplanunkid = @benefitplanunkid" & _
            '        ", value_basis = @value_basis" & _
            '        ", percentage = @percentage" & _
            '        ", amount = @amount" & _
            '        ", start_date = @start_date" & _
            '        ", end_date = @end_date" & _
            '        ", userunkid = @userunkid" & _
            '        ", isvoid = @isvoid" & _
            '        ", voiduserunkid = @voiduserunkid" & _
            '        ", voiddatetime = @voiddatetime" & _
            '        ", voidreason = @voidreason " & _
            '        ", tranheadunkid = @tranheadunkid " & _
            '       "WHERE benefitcoverageunkid = @benefitcoverageunkid "

            StrQ = "UPDATE hremp_benefit_coverage SET " & _
                     "  employeeunkid = @employeeunkid" & _
                     ", benefitgroupunkid = @benefitgroupunkid" & _
                     ", benefitplanunkid = @benefitplanunkid" & _
                     ", userunkid = @userunkid" & _
                     ", isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
                     ", tranheadunkid = @tranheadunkid " & _
                    "WHERE benefitcoverageunkid = @benefitcoverageunkid "
            'Vimal (01 Nov 2010) -- End



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@benefitcoverageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupAssignBenefitGroupId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("benefitgroupunkid").ToString)
            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("benefitplanunkid").ToString)

            'Vimal (01 Nov 2010) -- Start 
            'objDataOperation.AddParameter("@value_basis", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("valueid").ToString)
            'objDataOperation.AddParameter("@percentage", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dtRow.Item("percent").ToString)
            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, dtRow.Item("amount").ToString)
            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtRow.Item("startdate").tostring)
            'If dtRow.Item("enddate") = Nothing Then
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            'Else
            '    objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtRow.Item("enddate").ToString)
            'End If
            'Vimal (01 Nov 2010) -- End
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("tranheadunkid").ToString)

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "hremp_benefit_coverage", mintBenefitcoverageunkid, "benefitcoverageunkid", 2) Then

                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "hremp_benefit_coverage", "benefitcoverageunkid", mintBenefitcoverageunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END



            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateBenefitGroup", mstrModuleName)
        End Try
    End Function
    'Sandeep [ 21 Aug 2010 ] -- End 


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot assign same benefit plan or transaction head to same employee. Please assign new benefit plan or transaction head.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot delete this benefit. Reason : This benefit is already used in some transactions.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿Imports eZeeCommonLib

Public Class clsDataBaseUpdate
    'S.SANDEEP [ 09 AUG 2011 ] -- START
    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
    Private Shared mblnIsLabel1 As Boolean = False
    'S.SANDEEP [ 09 AUG 2011 ] -- End

    Public Shared Function DataUpdate() As Boolean
        Dim strFile As String = ""
        Dim objappsettings As New clsApplicationSettings
        Try
            Dim strDatabaseVersion As String = ""
            Using objDataOperation As New clsDataOperation
                strDatabaseVersion = objDataOperation.getVersion()
                Dim objDBVer() As String
                objDBVer = Split(strDatabaseVersion, ".")
                If acore32.core.Database_Version_Major <> CInt(Val(objDBVer(0))) Then
                    'S.SANDEEP [ 09 AUG 2011 ] -- START
                    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
                    mblnIsLabel1 = True
                    'S.SANDEEP [ 09 AUG 2011 ] -- End
                    GoTo Label_01
                ElseIf acore32.core.Database_Version_Minor <> CInt(Val(objDBVer(1))) Then
                    'S.SANDEEP [ 09 AUG 2011 ] -- START
                    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
                    mblnIsLabel1 = True
                    'S.SANDEEP [ 09 AUG 2011 ] -- End
                    GoTo Label_01
                ElseIf acore32.core.Database_Version_Build <> CInt(Val(objDBVer(2))) Then
                    'S.SANDEEP [ 09 AUG 2011 ] -- START
                    'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
                    mblnIsLabel1 = True
                    'S.SANDEEP [ 09 AUG 2011 ] -- End
                    GoTo Label_01
                ElseIf acore32.core.Database_Version_Revision <> CInt(Val(objDBVer(3))) Then
                    GoTo Label_02
                Else
                    GoTo label_03
                End If


            End Using
Label_01:
            strFile = objappsettings._ApplicationPath & "Aruti_SQL.exe"
            'If System.IO.File.Exists(strFile) Then
            '    Call Shell(strFile, AppWinStyle.NormalFocus, True)
            '    If Not CheckUpdate() Then
            '        Return False
            '    End If
            'Else
            '    'Issue # OEM
            '    eZeeMsgBox.Show("Error 35XSMO9 : System file not found. Please Contact Aruti Support", enMsgBoxStyle.Critical)
            '    Return False
            'End If
            'S.SANDEEP [ 09 AUG 2011 ] -- START
            'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
            'If System.IO.File.Exists(strFile) Then
                'If General_Settings._Object._WorkingMode.ToUpper <> enWorkingMode.Client.ToString.ToUpper Then ' for smo to run on server only and not on client machine.
                    'Call Shell(strFile, AppWinStyle.NormalFocus, True)
                'End If
                'If Not CheckUpdate() Then
                    'Return False
                'End If
            'Else
                'Issue # OEM
                'eZeeMsgBox.Show("Error 35XSMO9 : System file not found. Please Contact Aruti Support", enMsgBoxStyle.Critical)
                'Return False
            'End If
            'S.SANDEEP [ 09 AUG 2011 ] -- End


            'Anjan [16 September 2014] -- Start
            'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
            'Reason: for issue in aruti.ini WorkingMode set as 2 then its not updating client machine so now checking is set on registry IsClient=1
            'If General_Settings._Object._WorkingMode.ToUpper <> enWorkingMode.Client.ToString.ToUpper Then
            If AppSettings._Object._IsClient = 0 Then
                'Anjan [16 September 2014] -- End


                If System.IO.File.Exists(strFile) Then
                    Call Shell(strFile, AppWinStyle.NormalFocus, True)
                    If Not CheckUpdate() Then
                        Return False
                    End If
                Else
                    eZeeMsgBox.Show("Error 35XSMO9 : System file not found. Please Contact Aruti Support", enMsgBoxStyle.Critical)
                    Return False
                End If
                'Anjan [16 September 2014] -- Start
                'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
                'Reason: for issue in aruti.ini WorkingMode set as 2 then its not updating client machine so now checking is set on registry IsClient=1
                'ElseIf General_Settings._Object._WorkingMode.ToUpper = enWorkingMode.Client.ToString.ToUpper Then
            ElseIf AppSettings._Object._IsClient = 1 Then
                 'Anjan [16 September 2014] -- End
                If Not CheckUpdate() Then
                    Return False
                End If
            End If
Label_02:

            'S.SANDEEP [ 09 AUG 2011 ] -- START
            'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE

            If mblnIsLabel1 = False Then                'To prevent if there is change in Major or Minor version coz it will run for label01: 


                'Anjan [16 September 2014] -- Start
                'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
                'Reason: for issue in aruti.ini WorkingMode set as 2 then its not updating client machine so now checking is set on registry IsClient=1
                'If General_Settings._Object._WorkingMode.ToString.ToUpper = enWorkingMode.Client.ToString.ToUpper Then
                If AppSettings._Object._IsClient = 1 Then
                    'Anjan [16 September 2014] -- End

                    'strFile = objappsettings._ApplicationPath & "Aruti_SQL.exe"

                    'If System.IO.File.Exists(strFile) Then
                    'Call Shell(strFile, AppWinStyle.NormalFocus, True)
                    If Not CheckUpdate() Then
                        Return False
                    End If
                    'Else
                    '    eZeeMsgBox.Show("Error 35XSMO9 : System file not found. Please Contact Aruti Support", enMsgBoxStyle.Critical)
                    '    Return False
                    'End If
                End If
            End If
            'S.SANDEEP [ 09 AUG 2011 ] -- End
            'Anjan [16 September 2014] -- Start
            'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
            'Reason: for issue of client was updating server version is client is on different version.
            If AppSettings._Object._IsClient = 0 Then
            If Not UpdateVersion() Then
                Return False
            End If
            End If
            'Anjan [16 September 2014] -- End

Label_03:
            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function CheckUpdate() As Boolean
        Dim objDataOperation As New clsDataOperation
        Dim blnResult As Boolean = False
        Try
            If IO.File.Exists(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat") = True Then
                IO.File.Delete(My.Computer.FileSystem.SpecialDirectories.Temp & "\ArutiTemp.bat")
            End If

            Dim strVersion As String = ""
            strVersion = objDataOperation.getVersion()
            Dim objDBVer As String()
            objDBVer = Split(strVersion, ".")

            If acore32.core.Database_Version_Major = CInt(Val(objDBVer(0))) And _
               acore32.core.Database_Version_Minor = CInt(Val(objDBVer(1))) And _
               acore32.core.Database_Version_Build = CInt(Val(objDBVer(2))) And _
               acore32.core.Database_Version_Revision = CInt(Val(objDBVer(3))) Then

                Return True

            Else


                '    'eZeeMsgBox.Show("Error 42XSMO8: Invalid System file Version. please Contact eZee Support Team.", enMsgBoxStyle.Critical)
                '    'Issue # OEM
                '    eZeeMsgBox.Show("Your Database Version and Application Version are not matching. " & vbCrLf & vbCrLf & _
                '                    "Reason: This normally happens if you have installed Application on more than 1 computer " & vbCrLf & _
                '                    "and have not run Service Pack on all computers. " & vbCrLf & _
                '                    "If you have already done that, please run Aruti Configuration on your Server and this problem will be resolved. " & vbCrLf & vbCrLf & _
                '                    "If you are not sure please contact Support Center (42XSMO8)", enMsgBoxStyle.Critical)
                '    Return False
                'End If

                'S.SANDEEP [ 09 AUG 2011 ] -- START
                'ENHANCEMENT : AUTO BACKUP & AUTO UPDATE
                ''eZeeMsgBox.Show("Error 42XSMO8: Invalid System file Version. please Contact eZee Support Team.", enMsgBoxStyle.Critical)
                ''Issue # OEM
                'eZeeMsgBox.Show("Your Database Version and Application Version are not matching. " & vbCrLf & vbCrLf & _
                '                "Reason: This normally happens if you have installed Application on more than 1 computer " & vbCrLf & _
                '                "and have not run Service Pack on all computers. " & vbCrLf & _
                '                "If you have already done that, please run Aruti Configuration on your Server and this problem will be resolved. " & vbCrLf & vbCrLf & _
                '                "If you are not sure please contact Support Center (42XSMO8)", enMsgBoxStyle.Critical)
                'If Not AppSettings._Object._IsAutoUpdateEnabled Then
                '    eZeeMsgBox.Show("Your Database Version and Application Version are not matching. " & vbCrLf & vbCrLf & _
                '                "Reason: This normally happens if you have installed Application on more than 1 computer " & vbCrLf & _
                '                "and have not run Service Pack on all computers. " & vbCrLf & _
                '                "If you have already done that, please run Aruti Configuration on your Server and this problem will be resolved. " & vbCrLf & vbCrLf & _
                '                "If you are not sure please contact Support Center (42XSMO8)", enMsgBoxStyle.Critical)
                '    Return False
                'Else

                'Anjan [16 September 2014] -- Start
                'ENHANCEMENT : Implementing autoupdate issue where Workingmode is set as 2 in aruti.ini.
                'Reason: for issue in aruti.ini WorkingMode set as 2 then its not updating client machine so now checking is set on registry IsClient=1
                'If General_Settings._Object._WorkingMode.ToString.ToUpper = enWorkingMode.Client.ToString.ToUpper  Then


                If AppSettings._Object._IsClient = 1 Then

                    'Anjan [17 November 2014] -- Start
                    'ENHANCEMENT : included to check if database version is lower than acore Client machine version.For autoupdate as revision will be always lower on database server(9.0.53.1) and acore will be higher(9.0.53.2)
                    If CInt(acore32.core.Version.Replace(".", "")) > CInt(strVersion.Replace(".", "")) Then
                        Return True
                    End If
                    'Anjan [17 November 2014] -- End

                    'Anjan [16 September 2014] -- End
                    'Dim strSetupFilePath As String = "\\" & General_Settings._Object._ServerName & "\Updates\ClientSP.exe"
                    'If Not System.IO.File.Exists(strSetupFilePath) Then
                    '    'S.SANDEEP [ 09 APRIL 2012 ] -- START
                    '    'ENHANCEMENT : TRA CHANGES
                    '    eZeeMsgBox.Show("Sorry, You cannot update the application. Reason specified Client SP could not be found on Server or login credentials are needed to access this Client SP." & vbCrLf & _
                    '                    "Please contact system or network administrator.", enMsgBoxStyle.Information)
                    '    Return False
                    '    'Else
                    '    '    'S.SANDEEP [ 09 APRIL 2012 ] -- END
                    '    '    strSetupFilePath = ""
                    '    '    Dim strLink As String = clsDataBaseUpdate.getNewVersionLink
                    '    '    If strLink.Trim <> "" Then
                    '    '        Dim frm As New frmDownloader
                    '    '        frm._URL = strLink
                    '    '        frm.ShowDialog()
                    '    '        If frm._Downloaded Then
                    '    '            strSetupFilePath = frm._DownloadFilePath
                    '    '        End If
                    '    '    End If
                    'End If

                    'If strSetupFilePath <> "" Then
                    '    Dim strApplicationExe As String = AppSettings._Object._ApplicationPath
                    '    Select Case gApplicationType
                    '        Case enArutiApplicatinType.Aruti_Configuration
                    '            strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiConfiguration.exe")
                    '        Case enArutiApplicatinType.Aruti_Payroll
                    '            strApplicationExe = System.IO.Path.Combine(strApplicationExe, "Aruti.exe")
                    '        Case enArutiApplicatinType.Aruti_TimeSheet
                    '            strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiTimeSheet.exe")
                    '    End Select
                    '    Shell(AppSettings._Object._ApplicationPath & "ArutiUpdate.exe " & """" & strSetupFilePath & """ """ & strApplicationExe & """")
                    'Else
                    '    eZeeMsgBox.Show("Your Database Version and Application Version are not matching. " & vbCrLf & vbCrLf & _
                    '                    "Reason: This normally happens if you have installed Application on more than one computer " & vbCrLf & _
                    '                    "and have not run Service Pack on all computers. " & vbCrLf & _
                    '                    "If you have already done that, please run Aruti Configuration on your Server and this problem will be resolved. " & vbCrLf & vbCrLf & _
                    '                    "If you are not sure please contact Support Center (42XSMO8)", enMsgBoxStyle.Critical)
                    '    Return False
                    'End If

                    'S.SANDEEP [ 10 NOV 2014 ] -- START
                    'Dim strSetupFilePath As String = "\\" & General_Settings._Object._ServerName & "\Updates\Client.cab"
                    Dim strSetupFilePath As String = String.Empty
                    If General_Settings._Object._AutoUpdate_Server.Trim.Length <= 0 Then
                        strSetupFilePath = "\\" & General_Settings._Object._ServerName & "\Updates\Client.cab"
                    Else
                        strSetupFilePath = "\\" & General_Settings._Object._AutoUpdate_Server & "\Updates\Client.cab"
                    End If
                    'S.SANDEEP [ 10 NOV 2014 ] -- END

                    If Not System.IO.File.Exists(strSetupFilePath) Then
                        eZeeMsgBox.Show("Sorry, You cannot update the application. Reason specified Cab File could not be found on Server or login credentials are needed to access this Cab File." & vbCrLf & _
                                        "Please contact system/network administrator OR system Administrator to update your version manually.", enMsgBoxStyle.Information)
                        Return False
                    End If

                    If strSetupFilePath <> "" Then
                        'Sandeep [15 Feb 2014] -- Start
                        'ENHANCEMENT : Requested by Rutta
                        'MsgBox("We are updating your Aruti Application version. Please wait for a while.")
                        'Shell(AppSettings._Object._ApplicationPath & "ArutiUpdate.exe " & """" & strSetupFilePath & """", AppWinStyle.NormalFocus, True)
                        'MsgBox("Update complete. Please Start Aruti Application.")
                        eZeeMsgBox.Show("We are updating your Aruti Application version. Please wait for a while.", enMsgBoxStyle.Information)
                        Dim strApplicationExe As String = AppSettings._Object._ApplicationPath
                        Select Case gApplicationType
                            Case enArutiApplicatinType.Aruti_Configuration
                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiConfiguration.exe")
                            Case enArutiApplicatinType.Aruti_Payroll
                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "Aruti.exe")
                            Case enArutiApplicatinType.Aruti_TimeSheet
                                strApplicationExe = System.IO.Path.Combine(strApplicationExe, "ArutiTimeSheet.exe")
                        End Select
                        Shell(AppSettings._Object._ApplicationPath & "ArutiUpdate.exe " & """" & strSetupFilePath & """ """ & strApplicationExe & """", AppWinStyle.NormalFocus, True)
                        'Sandeep [15 Feb 2014 ] -- End
                    Else
                        eZeeMsgBox.Show("Your Database Version and Application Version are not matching. " & vbCrLf & vbCrLf & _
                                        "Reason: This normally happens if you have installed Application on more than one computer " & vbCrLf & _
                                        "and have not run Service Pack on all computers. " & vbCrLf & _
                                        "If you have already done that, please run Aruti Configuration on your Server and this problem will be resolved. " & vbCrLf & vbCrLf & _
                                        "If you are not sure please contact Support Center (42XSMO8)", enMsgBoxStyle.Critical)
                        Return False
                    End If
                Else
                    Return True
                End If
            End If
            'S.SANDEEP [ 09 AUG 2011 ] -- END 

            'End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function UpdateVersion() As Boolean
        Dim strVersion As String = String.Empty

        strVersion = _
         acore32.core.Database_Version_Major.ToString & "." & _
         acore32.core.Database_Version_Minor.ToString & "." & _
         acore32.core.Database_Version_Build.ToString & "." & _
         Format(acore32.core.Database_Version_Revision, "00#").ToString

        Try

            Using DataOperation As New eZeeCommonLib.clsDataOperation
                Dim strQ As String = ""

                Try
                    strQ = "ALTER PROCEDURE [dbo].[version] AS SELECT '" & strVersion & "' AS version "

                    DataOperation.ExecNonQuery(strQ)

                    If DataOperation.ErrorMessage <> "" Then
                        Throw New Exception(DataOperation.ErrorMessage)
                    End If
                Catch ex As Exception
                    strQ = "CREATE PROCEDURE [dbo].[version] AS SELECT '" & strVersion & "' AS version "
                    Dim objDataOp As New clsDataOperation
                    objDataOp.ExecNonQuery(strQ)
                    If objDataOp.ErrorMessage <> "" Then
                        Throw New Exception(objDataOp.ErrorMessage)
                    End If
                    objDataOp = Nothing
                End Try

            End Using

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function getNewVersionLink() As String
        Try
            Dim strLink As String = ""
            If isInternetAvailable() Then
                Dim webClient As New System.Net.WebClient()
                webClient.DownloadFile("http://www.ezeecentrix.com/download/Aruti/Updates/Version.txt", System.IO.Path.GetTempPath() & "\version.txt")
                Dim strData As String = String.Empty
                strData = System.IO.File.ReadAllText(System.IO.Path.GetTempPath() & "\version.txt")
                Dim strRec() As String
                strRec = strData.Split("|")

                Dim strVersion As String = strRec(0)

                strLink = strRec(1)

                System.IO.File.Delete(System.IO.Path.GetTempPath() & "\version.txt")

                Dim objDBVer() As String
                objDBVer = Split(strVersion, ".")

                If acore32.core.Database_Version_Major = CInt(Val(objDBVer(0))) And _
                    acore32.core.Database_Version_Minor = CInt(Val(objDBVer(1))) And _
                    acore32.core.Database_Version_Build = CInt(Val(objDBVer(2))) And _
                    acore32.core.Database_Version_Revision = CInt(Val(objDBVer(3))) Then
                    strLink = ""
                End If

                Return strLink
            Else
                Return ""
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function isInternetAvailable() As Boolean
        Try
            Dim blnFlag As Boolean = False
            Dim wReq As System.Net.WebRequest
            wReq = System.Net.WebRequest.Create("http://www.aruti.com/")

            Dim wRes As System.Net.WebResponse
            wRes = wReq.GetResponse
            blnFlag = True

            Return blnFlag
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function isAMCPaid() As Boolean
        Try
            Dim blnAMCPaid As Boolean = False

            Dim obj As System.Net.HttpWebRequest
            Dim objHotel As New clsGroup_Master(True)
            Dim strURL As String = "http://www.ezeesupport.com/ws/xml_service.php?xml=<ROOT><AUTH_CODE>ezee123</AUTH_CODE><PROPERTY>" & objHotel._Groupname & "</PROPERTY><CITY>" & objHotel._City & "</CITY></ROOT>"
            obj = System.Net.HttpWebRequest.Create(strURL)
            obj.ContentType = "text/xml"
            obj.Method = "GET"
            Dim obj1 As System.Net.HttpWebResponse
            obj1 = CType(obj.GetResponse, System.Net.HttpWebResponse)

            Dim os1 As System.IO.Stream = obj1.GetResponseStream()

            Dim _Answer As IO.StreamReader = New IO.StreamReader(os1)
            Dim strRSString As String = _Answer.ReadToEnd().ToString

            Dim xd As New Xml.XmlDocument()
            xd.LoadXml(strRSString)
            Dim root As Xml.XmlElement = xd("ROOT")
            If root("ERROR").InnerText = "" Then
                If root("AMC").InnerText = "1" Then
                    blnAMCPaid = True
                Else
                    blnAMCPaid = False
                End If
            Else
                blnAMCPaid = False
            End If
            Return blnAMCPaid
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class

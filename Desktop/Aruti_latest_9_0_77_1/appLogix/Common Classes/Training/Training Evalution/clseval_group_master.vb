﻿'************************************************************************************************************************************
'Class Name : clseval_group_master.vb
'Purpose    :
'Date       :08-07-2021
'Written By :gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: gajanan
''' </summary>
Public Class clseval_group_master
    Private Shared ReadOnly mstrModuleName As String = "clseval_group_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCategoryid As Integer
    Private mstrCategoryname As String = String.Empty
    Private mintFeedbackmode As Integer
    Private mintFeedbackNoDaysAfterTrainingAttended As Integer
    Private mblnIsnotifyforfeedback As Boolean
    Private mintNotifyBeforeNoDaysForFeedback As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    Enum enFeedBack
        PRETRAINING = 1
        POSTTRAINING = 2
        DAYSAFTERTRAINING = 3
    End Enum

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryid
    ''' Modify By: gajanan
    ''' </summary>
    Public Property _Categoryid() As Integer
        Get
            Return mintCategoryid
        End Get
        Set(ByVal value As Integer)
            mintCategoryid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryname
    ''' Modify By: gajanan
    ''' </summary>
    Public Property _Categoryname() As String
        Get
            Return mstrCategoryname
        End Get
        Set(ByVal value As String)
            mstrCategoryname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set feedbackmode
    ''' Modify By: gajanan
    ''' </summary>
    Public Property _Feedbackmode() As Integer
        Get
            Return mintFeedbackmode
        End Get
        Set(ByVal value As Integer)
            mintFeedbackmode = value
        End Set
    End Property

    Public Property _FeedbackNodaysAfterTrainingAttended() As Integer
        Get
            Return mintFeedbackNoDaysAfterTrainingAttended
        End Get
        Set(ByVal value As Integer)
            mintFeedbackNoDaysAfterTrainingAttended = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isnotifyforfeedback
    ''' Modify By: gajanan
    ''' </summary>
    Public Property _Isnotifyforfeedback() As Boolean
        Get
            Return mblnIsnotifyforfeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsnotifyforfeedback = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set noofdaysforfeedback
    ''' Modify By: gajanan
    ''' </summary>
    Public Property _NotifyBeforeNodaysForFeedback() As Integer
        Get
            Return mintNotifyBeforeNoDaysForFeedback
        End Get
        Set(ByVal value As Integer)
            mintNotifyBeforeNoDaysForFeedback = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  categoryid " & _
              ", categoryname " & _
              ", feedbackmode " & _
              ", feedbacknodaysaftertrainingattended " & _
              ", isnotifyforfeedback " & _
              ", notifybeforenodaysforfeedback " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM treval_group_master " & _
            "WHERE categoryid = @categoryid "

            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCategoryid = CInt(dtRow.Item("categoryid"))
                mstrCategoryname = dtRow.Item("categoryname").ToString
                mintFeedbackmode = CInt(dtRow.Item("feedbackmode"))
                mintFeedbackNoDaysAfterTrainingAttended = CInt(dtRow.Item("feedbacknodaysaftertrainingattended"))
                mblnIsnotifyforfeedback = CBool(dtRow.Item("isnotifyforfeedback"))
                mintNotifyBeforeNoDaysForFeedback = CInt(dtRow.Item("notifybeforenodaysforfeedback"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intFeedbackmodeId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  categoryid " & _
              ", categoryname " & _
              ", feedbackmode as feedbackmodeid " & _
              ",CASE " & _
                    "WHEN ISNULL(feedbackmode, 0) = " & CInt(enFeedBack.PRETRAINING) & " THEN @PRETRAINING " & _
                    "WHEN ISNULL(feedbackmode, 0) = " & CInt(enFeedBack.POSTTRAINING) & " THEN @POSTTRAINING " & _
                    "WHEN ISNULL(feedbackmode, 0) = " & CInt(enFeedBack.DAYSAFTERTRAINING) & " THEN @DAYSAFTERTRAINING " & _
                    "ELSE '' " & _
              "END AS feedbackmode " & _
              ", isnotifyforfeedback " & _
              ",CASE " & _
                    "WHEN ISNULL(feedbackmode, 0) = " & CInt(enFeedBack.DAYSAFTERTRAINING) & " THEN feedbacknodaysaftertrainingattended " & _
                    "ELSE 0 " & _
              "END AS feedbacknodaysaftertrainingattended " & _
              ", isnotifyforfeedback " & _
              ",CASE " & _
                    "WHEN ISNULL(isnotifyforfeedback, 0) > 0 THEN notifybeforenodaysforfeedback " & _
                    "ELSE 0 " & _
              "END AS notifybeforenodaysforfeedback " & _
              ", isvoid " & _
             "FROM treval_group_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If intFeedbackmodeId > 0 Then
                strQ &= " AND feedbackmode = @feedbackmodeid "
                objDataOperation.AddParameter("@feedbackmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFeedbackmodeId)
            End If

            objDataOperation.AddParameter("@PRETRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pre-Training"))
            objDataOperation.AddParameter("@POSTTRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Post-Training"))
            objDataOperation.AddParameter("@DAYSAFTERTRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Days after training attended."))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetCategoryComboList(ByVal strTableName As String, Optional ByVal blnSelect As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnSelect Then
                strQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If

            strQ &= " SELECT " & _
              "  categoryid as Id " & _
              ", categoryname AS Name " & _
             "FROM treval_group_master WHERE isvoid = 0 "

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCategoryComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (treval_group_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrCategoryname, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname.ToString)
            objDataOperation.AddParameter("@feedbackmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackmode.ToString)
            objDataOperation.AddParameter("@feedbacknodaysaftertrainingattended", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackNoDaysAfterTrainingAttended.ToString)
            objDataOperation.AddParameter("@isnotifyforfeedback", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnotifyforfeedback.ToString)
            objDataOperation.AddParameter("@notifybeforenodaysforfeedback", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNotifyBeforeNoDaysForFeedback.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO treval_group_master ( " & _
              "  categoryname " & _
              ", feedbackmode " & _
              ", feedbacknodaysaftertrainingattended " & _
              ", isnotifyforfeedback " & _
              ", notifybeforenodaysforfeedback " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
            ") VALUES (" & _
              "  @categoryname " & _
              ", @feedbackmode " & _
              ", @feedbacknodaysaftertrainingattended " & _
              ", @isnotifyforfeedback " & _
              ", @notifybeforenodaysforfeedback " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategoryid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintCategoryid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (treval_group_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        If isExist(mstrCategoryname, mintCategoryid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryid.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname.ToString)
            objDataOperation.AddParameter("@feedbackmode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackmode.ToString)
            objDataOperation.AddParameter("@feedbacknodaysaftertrainingattended", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackNoDaysAfterTrainingAttended.ToString)
            objDataOperation.AddParameter("@isnotifyforfeedback", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnotifyforfeedback.ToString)
            objDataOperation.AddParameter("@notifybeforenodaysforfeedback", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNotifyBeforeNoDaysForFeedback.ToString)

            strQ = "UPDATE treval_group_master SET " & _
              "  categoryname = @categoryname" & _
              ", feedbackmode = @feedbackmode" & _
              ", feedbacknodaysaftertrainingattended = @feedbacknodaysaftertrainingattended " & _
              ", isnotifyforfeedback = @isnotifyforfeedback" & _
              ", notifybeforenodaysforfeedback = @notifybeforenodaysforfeedback " & _
            "WHERE categoryid = @categoryid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintCategoryid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (treval_group_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE treval_group_master SET " & _
                   " isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
            "WHERE categoryid = @categoryid "

            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"treval_question_master", "treval_subquestion_master"}

            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each value As String In Tables
                Select Case value
                    Case "treval_question_master"
                        strQ = "SELECT categoryid FROM " & value & " WHERE isvoid = 0 AND categoryid = @categoryid "

                    Case "treval_subquestion_master"
                        strQ = "SELECT categoryid FROM " & value & " WHERE isvoid = 0 AND categoryid = @categoryid "

                    Case Else
                End Select

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  categoryid " & _
              ", categoryname " & _
             "FROM treval_group_master " & _
             "WHERE  isvoid = 0 AND categoryname = @categoryname "

            If intUnkid > 0 Then
                strQ &= " AND categoryid <> @categoryid"
            End If

            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO attreval_group_master ( " & _
                    "  tranguid " & _
                    ", categoryid " & _
                    ", categoryname " & _
                    ", feedbackmode " & _
                    ", feedbacknodaysaftertrainingattended " & _
                    ", isnotifyforfeedback " & _
                    ", notifybeforenodaysforfeedback " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", hostname " & _
                    ", isweb" & _
                  ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", categoryid " & _
                    ", categoryname " & _
                    ", feedbackmode " & _
                    ", feedbacknodaysaftertrainingattended " & _
                    ", isnotifyforfeedback " & _
                    ", notifybeforenodaysforfeedback " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @hostname " & _
                    ", @isweb" & _
                    "  From treval_group_master WHERE 1=1 AND treval_group_master.categoryid = @categoryid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Sorry, This category is already defined. Please define new category.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
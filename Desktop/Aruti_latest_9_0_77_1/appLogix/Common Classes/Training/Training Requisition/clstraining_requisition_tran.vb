﻿'************************************************************************************************************************************
'Class Name : clstraining_requisition_tran.vb
'Purpose    :
'Date       :15-Oct-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clstraining_requisition_tran

#Region " Private Variables "

    Private Const mstrModuleName As String = "clstraining_requisition_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintTrainingRequisitionMasterId As Integer = -1
    Private mdtTran As DataTable
    Private mintRequsitionTranId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintLoginEmployeeId As Integer = 0

#End Region

#Region " Properties "

    Public Property _TrainingRequisitionMasterId(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTrainingRequisitionMasterId
        End Get
        Set(ByVal value As Integer)
            mintTrainingRequisitionMasterId = value
            Get_Requisition_Tran(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeId() As Integer
        Set(ByVal value As Integer)
            mintLoginEmployeeId = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("Training_Requisition")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("requisitiontranunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("requisitionmasterunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingmodeunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resourcemasterunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("item_description")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainingmode")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resourcetype")
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Method(s) "

    Private Sub Get_Requisition_Tran(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     TRT.requisitiontranunkid " & _
                   "    ,TRT.requisitionmasterunkid " & _
                   "    ,TRT.trainingmodeunkid " & _
                   "    ,TRT.resourcemasterunkid " & _
                   "    ,TRT.item_description " & _
                   "    ,TRT.isvoid " & _
                   "    ,TRT.voiddatetime " & _
                   "    ,TRT.voiduserunkid " & _
                   "    ,TRT.voidreason " & _
                   "    ,'' AS AUD " & _
                   "    ,ISNULL(TM.name,'') AS trainingmode " & _
                   "    ,ISNULL(RT.name,'') AS resourcetype " & _
                   "FROM hrtraining_requisition_tran AS TRT " & _
                   "    LEFT JOIN cfcommon_master AS TM ON TM.masterunkid = TRT.trainingmodeunkid AND TM.isactive = 1 " & _
                   "    LEFT JOIN cfcommon_master AS RT ON RT.masterunkid = TRT.resourcemasterunkid AND RT.isactive = 1 " & _
                   "WHERE TRT.isvoid = 0 AND TRT.requisitionmasterunkid = @requisitionmasterunkid AND TRT.isvoid = 0 "

            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequisitionMasterId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For Each row As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(row)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Requisition_Tran; Module Name: " & mstrModuleName)
        Finally
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_TrainingRequisition(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD").ToString.ToUpper()
                            Case "A"
                                strQ = "INSERT INTO hrtraining_requisition_tran ( " & _
                                       "  requisitionmasterunkid " & _
                                       ", trainingmodeunkid " & _
                                       ", resourcemasterunkid " & _
                                       ", item_description " & _
                                       ", isvoid " & _
                                       ", voiddatetime " & _
                                       ", voiduserunkid " & _
                                       ", voidreason" & _
                                       ") VALUES (" & _
                                       "  @requisitionmasterunkid " & _
                                       ", @trainingmodeunkid " & _
                                       ", @resourcemasterunkid " & _
                                       ", @item_description " & _
                                       ", @isvoid " & _
                                       ", @voiddatetime " & _
                                       ", @voiduserunkid " & _
                                       ", @voidreason" & _
                                       "); SELECT @@identity"

                                objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingRequisitionMasterId.ToString)
                                objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingmodeunkid").ToString)
                                objDataOperation.AddParameter("@resourcemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcemasterunkid").ToString)
                                objDataOperation.AddParameter("@item_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("item_description").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintRequsitionTranId = dsList.Tables(0).Rows(0).Item(0)

                                If InsertAuditTrailTrainingRequisitionTran(objDataOperation, 1, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If mdtTran.Columns.Contains("requisitiontranunkid") Then
                                    .Item("requisitiontranunkid") = mintRequsitionTranId
                                End If

                                If mdtTran.Columns.Contains("requisitionmasterunkid") Then
                                    .Item("requisitionmasterunkid") = mintTrainingRequisitionMasterId
                                End If

                            Case "U"

                                strQ = "UPDATE hrtraining_requisition_tran SET " & _
                                       "  requisitionmasterunkid = @requisitionmasterunkid" & _
                                       ", trainingmodeunkid = @trainingmodeunkid" & _
                                       ", resourcemasterunkid = @resourcemasterunkid" & _
                                       ", item_description = @item_description" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE requisitiontranunkid = @requisitiontranunkid "

                                objDataOperation.AddParameter("@requisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitiontranunkid").ToString)
                                objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitionmasterunkid").ToString)
                                objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingmodeunkid").ToString)
                                objDataOperation.AddParameter("@resourcemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resourcemasterunkid").ToString)
                                objDataOperation.AddParameter("@item_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("item_description").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertAuditTrailTrainingRequisitionTran(objDataOperation, 2, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = "UPDATE hrtraining_requisition_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE requisitiontranunkid = @requisitiontranunkid "

                                objDataOperation.AddParameter("@requisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("requisitiontranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertAuditTrailTrainingRequisitionTran(objDataOperation, 3, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_TrainingRequisition; Module Name: " & mstrModuleName)
        Finally
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Function InsertAuditTrailTrainingRequisitionTran(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer, ByVal drow As DataRow) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrtraining_requisition_tran ( " & _
                   "  tranguid " & _
                   ", requisitiontranunkid " & _
                   ", requisitionmasterunkid " & _
                   ", trainingmodeunkid " & _
                   ", resourcemasterunkid " & _
                   ", item_description " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", auditdatetime " & _
                   ", ip " & _
                   ", hostname " & _
                   ", form_name " & _
                   ", isweb " & _
                   ", loginemoployeeunkid " & _
                   ") VALUES ( " & _
                   "  @tranguid " & _
                   ", @requisitiontranunkid " & _
                   ", @requisitionmasterunkid " & _
                   ", @trainingmodeunkid " & _
                   ", @resourcemasterunkid " & _
                   ", @item_description " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @auditdatetime " & _
                   ", @ip " & _
                   ", @hostname " & _
                   ", @form_name " & _
                   ", @isweb " & _
                   ", @loginemoployeeunkid " & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@requisitionmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintTrainingRequisitionMasterId <= 0, drow.Item("requisitionmasterunkid").ToString, mintTrainingRequisitionMasterId).ToString())
            objDataOperation.AddParameter("@requisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintRequsitionTranId <= 0, drow.Item("requisitiontranunkid").ToString, mintRequsitionTranId).ToString())
            objDataOperation.AddParameter("@trainingmodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drow.Item("trainingmodeunkid").ToString)
            objDataOperation.AddParameter("@resourcemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drow.Item("resourcemasterunkid").ToString)
            objDataOperation.AddParameter("@item_description", SqlDbType.NText, eZeeDataType.NAME_SIZE, drow.Item("item_description").ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@loginemoployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeId)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailTrainingRequisitionTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class

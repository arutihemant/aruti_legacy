﻿'************************************************************************************************************************************
'Class Name : clsLnloan_scheme.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsLoan_Scheme
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Scheme"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoanschemeunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty

    'Anjan (11 May 2011)-Start
    'Private mdblMinnetsalary As Double
    Private mdecMinnetsalary As Decimal
    'Anjan (11 May 2011)-End 


    Private mdblMinhoursworked As Double
    Private mintLastnoofmonths As Integer
    'Private mblnIsinclude_Parttime As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
Private mintAuditUserId As Integer = 0
Public WriteOnly Property _AuditUserId() As Boolean 
Set(ByVal value As Integer) 
mintAuditUserId = value 
End Set 
End Property 
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set minnetsalary
    '''' Modify By: Anjan
    '''' </summary>

    'Anjan (11 May 2011)-Start
    'Public Property _Minnetsalary() As Double
    Public Property _Minnetsalary() As Decimal
        'Anjan (11 May 2011)-End 

        Get
            Return mdecMinnetsalary
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End
            mdecMinnetsalary = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set minhoursworked
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Minhoursworked() As Double
        Get
            Return mdblMinhoursworked
        End Get
        Set(ByVal value As Double)
            mdblMinhoursworked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastnoofmonths
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Lastnoofmonths() As Integer
        Get
            Return mintLastnoofmonths
        End Get
        Set(ByVal value As Integer)
            mintLastnoofmonths = Value
        End Set
    End Property

    'Public Property _Isinclude_Parttime() As Boolean
    '    Get
    '        Return mblnIsinclude_Parttime
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsinclude_Parttime = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loanschemeunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", minnetsalary " & _
              ", isactive " & _
             "FROM lnloan_scheme_master " & _
             "WHERE loanschemeunkid = @loanschemeunkid "

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintLoanschemeUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintloanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString

                'Anjan (11 May 2011)-Start
                'mdecMinnetsalary = CDbl(dtRow.Item("minnetsalary"))
                mdecMinnetsalary = CDec(dtRow.Item("minnetsalary"))
                'Anjan (11 May 2011)-End 


                'mblnIsinclude_Parttime = CBool(dtRow.Item("isinclude_parttime"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loanschemeunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", minnetsalary " & _
              ", isactive " & _
              "FROM lnloan_scheme_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_scheme_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, ) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 


            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            

            strQ = "INSERT INTO lnloan_scheme_master ( " & _
              "  code " & _
              ", name " & _
              ", description " & _
              ", minnetsalary " & _
              ", isactive " & _
            ") VALUES (" & _
              "  @code " & _
              ", @name " & _
              ", @description " & _
              ", @minnetsalary " & _
              ", @isactive " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanschemeunkid = dsList.Tables(0).Rows(0).Item(0)

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_scheme_master) </purpose>
    Public Function Update() As Boolean
        If isExist(, mstrName, mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        If isExist(mstrCode, , mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloanschemeunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)


            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 



            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            
            strQ = "UPDATE lnloan_scheme_master SET " & _
              "  code = @code " & _
              ", name = @name " & _
              ", description = @description " & _
              ", minnetsalary = @minnetsalary " & _
              ", isactive = @isactive " & _
             "WHERE loanschemeunkid = @loanschemeunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_scheme_master", mintLoanschemeunkid, "loanschemeunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)



            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_scheme_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            'strQ = "DELETE FROM lnloan_scheme_master " & _
            '"WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "UPDATE lnloan_scheme_master " & _
                   " SET isactive = 0 " & _
                   "WHERE loanschemeunkid = @loanschemeunkid "

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
objCommonATLog._WebClientIP = mstrClientIP 
objCommonATLog._WebHostName = mstrHostName 
objCommonATLog._FromWeb = mblnIsWeb 
objCommonATLog._AuditUserId = mintAuditUserId 
objCommonATLog._CompanyUnkid = mintCompanyUnkid
objCommonATLog._AuditDate = mdtAuditDate 
'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_scheme_master", "loanschemeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objCommonATLog = Nothing 
'S.SANDEEP [28-May-2018] -- END
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('loanschemeunkid') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "lnloan_scheme_master" Then Continue For
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            'Sohail (14 Nov 2011) -- Start
            If blnIsUsed = False Then
                strQ = "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_employee " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_costcenter " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                blnIsUsed = dsList.Tables("List").Rows.Count > 0
            End If
            'Sohail (14 Nov 2011) -- End

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loanschemeunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", minnetsalary " & _
              ", isactive " & _
            "FROM lnloan_scheme_master " & _
             "WHERE 1 = 1"

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= " AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND loanschemeunkid <> @loanschemeunkid"
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                      Optional ByVal intLanguageID As Integer = -1) As DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then
                strQ = "SELECT 0 AS loanschemeunkid " & _
                          ",  ' ' + @Select AS name " & _
                          ",  ' ' as Code " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            End If

            strQ &= "SELECT loanschemeunkid " & _
                        ", name AS name " & _
                        ", code as Code " & _
                     "FROM lnloan_scheme_master " & _
                     "WHERE isactive = 1 " & _
                     "ORDER BY  name "

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Sohail (14 Nov 2011) -- Start
    Public Function GetLoanSchemeIDUsedInJV(ByVal enAccConfigType As enAccountConfigType) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            Dim strFilter As String = "WHERE isactive=1 " & _
                                      "AND transactiontype_Id = @transactiontype_Id "

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration " & _
                        strFilter

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration_employee " & _
                        strFilter

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration_costcenter " & _
                        strFilter
            End If

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetLoanSchemeIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            Dim strFilter As String = "WHERE isactive=1 " & _
                                      "AND transactiontype_Id = @transactiontype_Id "

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration_costcenter " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT tranheadunkid FROM praccount_configuration_employee " & _
                        strFilter

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT tranheadunkid FROM praccount_configuration_costcenter " & _
                        strFilter

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strQ = "SELECT DISTINCT tranheadunkid FROM praccount_configuration " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT tranheadunkid FROM praccount_configuration_employee " & _
                        strFilter
            End If

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (14 Nov 2011) -- End


    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanSchemeUnkid(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = " SELECT " & _
                      "  loanschemeunkid " & _
                      "  FROM lnloan_scheme_master " & _
                      " WHERE name = @name  AND isactive = 1 "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("loanschemeunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 29 DEC 2011 ] -- END




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
			Language.setMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

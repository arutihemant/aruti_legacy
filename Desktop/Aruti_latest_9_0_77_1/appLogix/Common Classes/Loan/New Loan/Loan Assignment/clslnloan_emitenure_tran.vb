﻿'************************************************************************************************************************************
'Class Name : clslnloan_emitenure_tran.vb
'Purpose    :
'Date       :04-Mar-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clslnloan_emitenure_tran
    Private Shared ReadOnly mstrModuleName As String = "clslnloan_emitenure_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLnemitranunkid As Integer = 0
    Private mintLoanadvancetranunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmi_Tenure As Integer = 0
    Private mdecEmi_Amount As Decimal = 0
    Private mintLoan_Duration As Integer = Nothing
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mdtEnd_Date As Date = Nothing
    Private xDataOpr As clsDataOperation
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebIP As String = ""
    'Private mstrWebHost As String = ""

    Private mdecExchange_rate As Decimal = 0
    Private mdecBasecurrency_amount As Decimal = 0
    'Nilay (20-Nov-2015) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mblnisdefault As Boolean = False
    Private mintapproverunkid As Integer = 0
    'Nilay (20-Nov-2015) -- End

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private mdecPrincipal_amount As Decimal = 0
    Private mdecInterest_amount As Decimal = 0
    'Sohail (15 Dec 2015) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Private mstrIdentifyGuid As String = String.Empty
    'Nilay (13-Sept-2016) -- End

#End Region

#Region " Properties "

    'S.SANDEEP [28-May-2018] -- START 
    'ISSUE/ENHANCEMENT : {Audit Trails} 
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mblnIsWeb As Boolean = False
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintLoginEmployeeunkid As Integer = 0
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Private mintVoidlogingemployeeunkid As Integer = 0
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnemitranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Lnemitranunkid() As Integer
        Get
            Return mintLnemitranunkid
        End Get
        Set(ByVal value As Integer)
            mintLnemitranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanadvancetranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loanadvancetranunkid() As Integer
        Get
            Return mintLoanadvancetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanadvancetranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
   
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_tenure
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Emi_Tenure() As Integer
        Get
            Return mintEmi_Tenure
        End Get
        Set(ByVal value As Integer)
            mintEmi_Tenure = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emi_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Emi_Amount() As Decimal
        Get
            Return mdecEmi_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecEmi_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loan_duration
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loan_Duration() As Integer
        Get
            Return mintLoan_Duration
        End Get
        Set(ByVal value As Integer)
            mintLoan_Duration = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

    'Public WriteOnly Property _WebFormName() As String
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebIP() As String
    '    Set(ByVal value As String)
    '        mstrWebIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHost() As String
    '    Set(ByVal value As String)
    '        mstrWebHost = value
    '    End Set
    'End Property

    Public Property _Exchange_rate() As Decimal
        Get
            Return mdecExchange_rate
        End Get
        Set(ByVal value As Decimal)
            mdecExchange_rate = value
        End Set
    End Property

    Public Property _Basecurrency_amount() As Decimal
        Get
            Return mdecBasecurrency_amount
        End Get
        Set(ByVal value As Decimal)
            mdecBasecurrency_amount = value
        End Set
    End Property

     'Nilay (20-Nov-2015) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Public Property _IsDefault() As Boolean
        Get
            Return mblnisdefault
        End Get
        Set(ByVal value As Boolean)
            mblnisdefault = value
        End Set
    End Property

    Public Property _Approverunkid() As Integer
        Get
            Return mintapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintapproverunkid = value
        End Set
    End Property
    'Nilay (20-Nov-2015) -- End

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Public Property _Principal_amount() As Decimal
        Get
            Return mdecPrincipal_amount
        End Get
        Set(ByVal value As Decimal)
            mdecPrincipal_amount = value
        End Set
    End Property

    Public Property _Interest_amount() As Decimal
        Get
            Return mdecInterest_amount
        End Get
        Set(ByVal value As Decimal)
            mdecInterest_amount = value
        End Set
    End Property
    'Sohail (15 Dec 2015) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Public Property _Identify_Guid() As String
        Get
            Return mstrIdentifyGuid
        End Get
        Set(ByVal value As String)
            mstrIdentifyGuid = value
        End Set
    End Property
    'Nilay (13-Sept-2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                    "  lnemitranunkid " & _
                    ", loanadvancetranunkid " & _
                    ", periodunkid " & _
                    ", effectivedate " & _
                    ", emi_tenure " & _
                    ", emi_amount " & _
                    ", loan_duration " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                    ", end_date " & _
                    ", ISNULL(exchange_rate,0) AS exchange_rate " & _
                    ", ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
                    ", ISNULL(isdefault, 0) AS isdefault " & _
                    ", ISNULL(approverunkid, -1) AS approverunkid " & _
                    ", ISNULL(principal_amount,0) AS principal_amount " & _
                    ", ISNULL(interest_amount,0) AS interest_amount " & _
                    ", ISNULL(identify_guid,'') AS identify_guid " & _
                   "FROM lnloan_emitenure_tran " & _
                   "WHERE lnemitranunkid = @lnemitranunkid "
            'Nilay (13-Sept-2016) -- [identify_guid]
            'Nilay (20-Nov-2015) -- [isdefault], [approverunkid] -- ADDED
            'Sohail (15 Dec 2015) - [principal_amount, interest_amount]

            objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLnemitranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLnemitranunkid = CInt(dtRow.Item("lnemitranunkid"))
                mintLoanadvancetranunkid = CInt(dtRow.Item("loanadvancetranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmi_Tenure = CInt(dtRow.Item("emi_tenure"))
                mdecEmi_Amount = dtRow.Item("emi_amount")
                mintLoan_Duration = CInt(dtRow.Item("loan_duration"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdtEnd_Date = dtRow.Item("end_date")
                mdecExchange_rate = CDec(dtRow.Item("exchange_rate"))
                mdecBasecurrency_amount = CDec(dtRow.Item("basecurrency_amount"))
                'Nilay (20-Nov-2015) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mblnisdefault = CBool(dtRow.Item("isdefault"))
                mintapproverunkid = CInt(dtRow.Item("approverunkid"))
                'Nilay (20-Nov-2015) -- End
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                mdecPrincipal_amount = CDec(dtRow.Item("principal_amount"))
                mdecInterest_amount = CDec(dtRow.Item("interest_amount"))
                'Sohail (15 Dec 2015) -- End

                'Nilay (13-Sept-2016) -- Start
                ''Enhancement : Enable Default Parameter Edit & other fixes
                mstrIdentifyGuid = dtRow.Item("identify_guid").ToString
                'Nilay (13-Sept-2016) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intLoanAdvUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                    "  lnloan_emitenure_tran.lnemitranunkid " & _
                    ", lnloan_emitenure_tran.loanadvancetranunkid " & _
                    ", lnloan_emitenure_tran.periodunkid " & _
                    ", lnloan_emitenure_tran.effectivedate " & _
                    ", lnloan_emitenure_tran.emi_tenure " & _
                    ", lnloan_emitenure_tran.emi_amount " & _
                    ", lnloan_emitenure_tran.loan_duration " & _
                    ", lnloan_emitenure_tran.userunkid " & _
                    ", lnloan_emitenure_tran.isvoid " & _
                    ", lnloan_emitenure_tran.voiduserunkid " & _
                    ", lnloan_emitenure_tran.voiddatetime " & _
                    ", lnloan_emitenure_tran.voidreason " & _
                    ", lnloan_emitenure_tran.end_date " & _
                    ", CONVERT(CHAR(8),effectivedate,112) AS ddate " & _
                    ", cfcommon_period_tran.period_name AS dperiod " & _
                    ", cfcommon_period_tran.statusid AS pstatusid " & _
                    ", ISNULL(exchange_rate,0) AS exchange_rate " & _
                    ", ISNULL(principal_amount,0) AS principal_amount " & _
                    ", ISNULL(basecurrency_amount,0) AS basecurrency_amount " & _
                    ", ISNULL(interest_amount,0) AS interest_amount " & _
                    ", lnloan_emitenure_tran.isdefault " & _
                    ", lnloan_emitenure_tran.approverunkid " & _
                    ", lnloan_emitenure_tran.identify_guid " & _
                    "FROM lnloan_emitenure_tran " & _
                    " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
                    " WHERE lnloan_emitenure_tran.isvoid = 0 AND cfcommon_period_tran.isactive = 1 "
            'Nilay (13-Sept-2016) -- [isdefault,approverunkid,identify_guid]

            'Sohail (15 Dec 2015) - [basecurrency_amount, interest_amount]

            strQ &= " AND lnloan_emitenure_tran.loanadvancetranunkid = '" & intLoanAdvUnkid & "' "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each row As DataRow In dsList.Tables(0).Rows
                If row.Item("ddate").ToString <> "" Then
                    row.Item("ddate") = eZeeDate.convertDate(row.Item("ddate").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Public Function GetLastEMI(ByVal strTableName As String, ByVal intLoanAdvanceUnkid As Integer, ByVal dtAsOnDate As Date) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 Jan 2019) -- Start
        'Issue - 74.1 - Bind Transaction issue.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        'Sohail (18 Jan 2019) -- End
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    lnloan_emitenure_tran.lnemitranunkid  " & _
                                      ", lnloan_emitenure_tran.loanadvancetranunkid " & _
                                      ", lnloan_emitenure_tran.periodunkid " & _
                                      ", CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) AS effectivedate " & _
                                      ", lnloan_emitenure_tran.emi_tenure " & _
                                      ", lnloan_emitenure_tran.emi_amount " & _
                                      ", lnloan_emitenure_tran.loan_duration " & _
                                      ", CONVERT(CHAR(8), lnloan_emitenure_tran.end_date, 112) AS end_date " & _
                                      ", lnloan_emitenure_tran.exchange_rate " & _
                                      ", lnloan_emitenure_tran.basecurrency_amount " & _
                                      ", lnloan_emitenure_tran.principal_amount " & _
                                      ", lnloan_emitenure_tran.interest_amount " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY lnloan_emitenure_tran.loanadvancetranunkid ORDER BY lnloan_emitenure_tran.effectivedate DESC, lnloan_emitenure_tran.lnemitranunkid DESC ) AS ROW_NO " & _
                              "FROM      lnloan_emitenure_tran " & _
                              "WHERE     lnloan_emitenure_tran.isvoid = 0 " & _
                                        "AND lnloan_emitenure_tran.loanadvancetranunkid = @loanadvancetranunkid " & _
                                        "AND CONVERT(CHAR(8), lnloan_emitenure_tran.effectivedate, 112) <= @effectivedate " & _
                            ") AS A " & _
                    "WHERE   A.ROW_NO = 1 "

            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvanceUnkid)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastEMI; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function
    'Sohail (15 Dec 2015) -- End

    ' <summary>
    ' Modify By: Sandeep Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> INSERT INTO Database Table (lnloan_emitenure_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Insert(ByVal blnIncludeBalanceTran As Boolean, ByVal intYearUnkId As Integer) As Boolean
    Public Function Insert(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal blnIncludeBalanceTran As Boolean) As Boolean
        'Nilay (25-Mar-2016) -- End

        If isExist(mdtEffectivedate, mintLoanadvancetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Loan Installment Information is already defined for the selected date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objLoanAdvance As New clsLoan_Advance
            Dim dsBalance As New DataSet
            If blnIncludeBalanceTran = True Then
                'Nilay (25-Mar-2016) -- Start
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True)
                'Hemant (24 Jan 2019) -- Start
                'Issue - 74.1 - Bind Transaction issue.
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                '                                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                '                                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True)
                dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                                     xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                                     "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , , , , True, , , , , , , , , , , , , objDataOperation)
                'Hemant (24 Jan 2019) -- End
                
                'Nilay (25-Mar-2016) -- End
            End If
            'Sohail (07 May 2015) -- End
            'Hemant (25 Mar 2019) -- Start
            objDataOperation.ClearParameters()
            'Hemant (25 Mar 2019) -- End
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectivedate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            'Nilay (13-Sept-2016) -- End
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            'Nilay (20-Nov-2015) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisdefault.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverunkid.ToString)
            'Nilay (20-Nov-2015) -- End
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipal_amount)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_amount)
            'Sohail (15 Dec 2015) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            objDataOperation.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentifyGuid)
            'Nilay (13-Sept-2016) -- End

            strQ = "INSERT INTO lnloan_emitenure_tran ( " & _
              "  loanadvancetranunkid " & _
              ", periodunkid " & _
              ", effectivedate " & _
              ", emi_tenure " & _
              ", emi_amount " & _
              ", loan_duration " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", end_date" & _
              ", exchange_rate " & _
              ", basecurrency_amount " & _
              ", isdefault " & _
              ", approverunkid " & _
              ", principal_amount " & _
              ", interest_amount " & _
              ", identify_guid " & _
            ") VALUES (" & _
              "  @loanadvancetranunkid " & _
              ", @periodunkid " & _
              ", @effectivedate " & _
              ", @emi_tenure " & _
              ", @emi_amount " & _
              ", @loan_duration " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @end_date" & _
              ", @exchange_rate " & _
              ", @basecurrency_amount " & _
              ", @isdefault " & _
              ", @approverunkid " & _
              ", @principal_amount " & _
              ", @interest_amount " & _
              ", @identify_guid " & _
            "); SELECT @@identity"
            'Sohail (15 Dec 2015) - [principal_amount, interest_amount]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLnemitranunkid = dsList.Tables(0).Rows(0).Item(0)

            If blnIncludeBalanceTran = True Then
                'Dim objLoanAdvance As New clsLoan_Advance
                'Dim dsBalance As New DataSet
                'dsBalance = objLoanAdvance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid)
                If dsBalance.Tables(0).Rows.Count > 0 Then
                    Dim intPeriodID As Integer = 0

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), 0, FinancialYear._Object._YearUnkid)
                    'Hemant (24 Jan 2019) -- Start
                    'Issue - 74.1 - Bind Transaction issue.
                    'intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0)
                    intPeriodID = (New clsMasterData).getCurrentPeriodID(enModuleReference.Payroll, mdtEffectivedate.AddDays(-1), xYearUnkid, 0, , , objDataOperation)
                    'Hemant (24 Jan 2019) -- End
                    'S.SANDEEP [04 JUN 2015] -- END

                    If intPeriodID <= 0 Then intPeriodID = CInt(dsBalance.Tables(0).Rows(0).Item("periodunkid"))

                    '*** Do not change bf_amount (keep amount same as previous bf_amount amount) as it will be used in Interest Amount column for Simple interesr loan calculation
                    strQ = "INSERT  INTO lnloan_balance_tran " & _
                            "( loanschemeunkid  " & _
                            ", transaction_periodunkid " & _
                            ", transactiondate " & _
                            ", periodunkid " & _
                            ", end_date " & _
                            ", loanadvancetranunkid " & _
                            ", employeeunkid " & _
                            ", payrollprocesstranunkid " & _
                            ", paymenttranunkid " & _
                            ", bf_amount " & _
                            ", bf_amountpaidcurrency " & _
                            ", amount " & _
                            ", amountpaidcurrency " & _
                            ", cf_amount " & _
                            ", cf_amountpaidcurrency " & _
                            ", userunkid " & _
                            ", isvoid " & _
                            ", voiduserunkid " & _
                            ", voiddatetime " & _
                            ", voidreason " & _
                            ", lninteresttranunkid " & _
                            ", lnemitranunkid " & _
                            ", lntopuptranunkid " & _
                            ", days " & _
                            ", bf_balance " & _
                            ", bf_balancepaidcurrency " & _
                            ", principal_amount " & _
                            ", principal_amountpaidcurrency " & _
                            ", topup_amount " & _
                            ", topup_amountpaidcurrency " & _
                            ", repayment_amount " & _
                            ", repayment_amountpaidcurrency " & _
                            ", interest_rate " & _
                            ", interest_amount " & _
                            ", interest_amountpaidcurrency " & _
                            ", cf_balance " & _
                            ", cf_balancepaidcurrency " & _
                            ", isonhold " & _
                            ", isreceipt  " & _
                            ", nexteffective_days " & _
                            ", nexteffective_months " & _
                            ", exchange_rate " & _
                            ", isbrought_forward " & _
                            ", loanstatustranunkid " & _
                            ") " & _
                    "VALUES  ( " & dsBalance.Tables(0).Rows(0).Item("loanschemeunkid") & "  " & _
                            ", " & mintPeriodunkid & " " & _
                            ", '" & Format(mdtEffectivedate, "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & intPeriodID & " " & _
                            ", '" & Format(mdtEffectivedate.AddDays(-1), "yyyyMMdd HH:mm:ss") & "' " & _
                            ", " & mintLoanadvancetranunkid & " " & _
                            ", " & dsBalance.Tables(0).Rows(0).Item("employeeunkid") & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_Amount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BF_AmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPMTAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                            ", " & mintUserunkid & " " & _
                            ", 0 " & _
                            ", -1 " & _
                            ", NULL " & _
                            ", '' " & _
                            ", 0  " & _
                            ", " & mintLnemitranunkid & " " & _
                            ", -1 " & _
                            ", " & CInt(dsBalance.Tables(0).Rows(0).Item("DaysDiff")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("BalanceAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency")) & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", 0 " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("interest_rate")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmount")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("TotInterestAmountPaidCurrency")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalance")) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("LastProjectedBalancePaidCurrency")) & " " & _
                            ", " & CInt(Int(dsBalance.Tables(0).Rows(0).Item("isonhold"))) & " " & _
                            ", 0 " & _
                            ", " & DateDiff(DateInterval.Day, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("loaneffectivedate").ToString), mdtEnd_Date.AddDays(1)) - DateDiff(DateInterval.Day, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("loaneffectivedate").ToString), mdtEffectivedate) & " " & _
                            ", " & DateDiff(DateInterval.Month, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("loaneffectivedate").ToString), mdtEnd_Date.AddDays(1)) - DateDiff(DateInterval.Month, eZeeDate.convertDate(dsBalance.Tables(0).Rows(0).Item("loaneffectivedate").ToString), mdtEffectivedate) & " " & _
                            ", " & CDec(dsBalance.Tables(0).Rows(0).Item("exchange_rate")) & " " & _
                            ", 0 " & _
                            ", 0 " & _
                            ") "
                    'Sohail (15 Dec 2015) - [loanstatustranunkid,nexteffective_months]

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    'Hemant (30 Aug 2019) -- Start
                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                    objLoanAdvance._DataOpr = objDataOperation
                    'Hemant (30 Aug 2019) -- End
                    objLoanAdvance._Loanadvancetranunkid = mintLoanadvancetranunkid
                    objLoanAdvance._Balance_Amount = objLoanAdvance._Balance_Amount - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                    objLoanAdvance._Balance_AmountPaidCurrency = objLoanAdvance._Balance_AmountPaidCurrency - CDec(dsBalance.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                    objLoanAdvance._ClientIP = mstrClientIP
                    objLoanAdvance._FormName = mstrFormName
                    objLoanAdvance._HostName = mstrHostName
                    objLoanAdvance._AuditDate = mdtAuditDate
                    objLoanAdvance._AuditUserId = mintAuditUserId
objLoanAdvance._CompanyUnkid = mintCompanyUnkid
                    objLoanAdvance._FromWeb = mblnIsWeb
                    objLoanAdvance._LoginEmployeeUnkid = mintLoginEmployeeunkid

                    'Nilay (05-May-2016) -- Start
                    'If objLoanAdvance.Update(, objDataOperation) = False Then
                    If objLoanAdvance.Update(objDataOperation) = False Then
                        'Nilay (05-May-2016) -- End

                        exForce = New Exception(objLoanAdvance._Message)
                        Throw exForce
                    End If
                    'Sohail (07 May 2015) -- End

                End If
                objLoanAdvance = Nothing
            End If

            If InsertAuditEMI_Tran(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_emitenure_tran) </purpose>
    Public Function Update() As Boolean
        If isExist(mdtEffectivedate, mintLoanadvancetranunkid, mintLnemitranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Loan Installment Information is already defined for the selected date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLnemitranunkid.ToString)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DateTime.Parse(mdtEffectivedate & " " & Format(Now, "hh:mm:ss tt")))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            'Nilay (13-Sept-2016) -- End
            objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString)
            objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            objDataOperation.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            objDataOperation.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipal_amount)
            objDataOperation.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_amount)
            'Sohail (15 Dec 2015) -- End

            strQ = "UPDATE lnloan_emitenure_tran SET " & _
                    "  loanadvancetranunkid = @loanadvancetranunkid" & _
                    ", periodunkid = @periodunkid" & _
                    ", effectivedate = @effectivedate" & _
                    ", emi_tenure = @emi_tenure" & _
                    ", emi_amount = @emi_amount" & _
                    ", loan_duration = @loan_duration" & _
                    ", userunkid = @userunkid" & _
                    ", isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason" & _
                    ", end_date = @end_date " & _
                    ", exchange_rate = @exchange_rate " & _
                    ", basecurrency_amount = @basecurrency_amount " & _
                    ", principal_amount = @principal_amount " & _
                    ", interest_amount = @interest_amount " & _
                   "WHERE lnemitranunkid = @lnemitranunkid "
            'Sohail (15 Dec 2015) - [principal_amount, interest_amount]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditEMI_Tran(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ' <summary>
    ' Modify By: Sandeep Sharma
    ' </summary>
    ' <returns>Boolean</returns>
    ' <purpose> Delete Database Table (lnloan_emitenure_tran) </purpose>

    'Nilay (25-Mar-2016) -- Start
    'Public Function Delete(ByVal intUnkid As Integer, ByVal blnIncludeBalanceTran As Boolean) As Boolean
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="xDatabaseName"></param>
    ''' <param name="xUserUnkid"></param>
    ''' <param name="xYearUnkid"></param>
    ''' <param name="xCompanyUnkid"></param>
    ''' <param name="xPeriodStart"></param>
    ''' <param name="xPeriodEnd"></param>
    ''' <param name="xUserModeSetting"></param>
    ''' <param name="xOnlyApproved"></param>
    ''' <param name="intUnkid"></param>
    ''' <param name="blnIncludeBalanceTran"></param>
    ''' <param name="blnIsFromLoanAdvanceList">Pass True only when from LoanAdvanceList Else False</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Delete(ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As Date, _
                           ByVal xPeriodEnd As Date, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal intUnkid As Integer, _
                           ByVal blnIncludeBalanceTran As Boolean, _
                           ByVal blnIsFromLoanAdvanceList As Boolean) As Boolean
        'Nilay (20-Sept-2016) -- [blnIsFromLoanAdvanceList]
        'Nilay (25-Mar-2016) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = ""
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this Installment Info. Reason : It is linked with loan balance transaction.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            If blnIncludeBalanceTran = True Then
                Dim objLnAdv As New clsLoan_Advance
                Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanadvancetranunkid)
                Dim strMsg As String = String.Empty
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) = intUnkid) Then
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 3, " Process Payroll.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage(mstrModuleName, 4, " Loan Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage(mstrModuleName, 5, " Receipt Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 6, " Loan Installment Change.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 7, " Loan Topup Added.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 8, " Loan Rate Change.")
                        End If
                    End If
                End If
                objLnAdv = Nothing
                If strMsg.Trim.Length > 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 9, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg
                    If xDataOpr Is Nothing Then
                        objDataOperation.ReleaseTransaction(False)
                    End If
                    Return False
                End If
            End If

            strQ = "UPDATE lnloan_emitenure_tran SET " & _
                    "  isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                    "WHERE lnemitranunkid = @lnemitranunkid "

            objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'Dim objlnOtherOpTran As New clsloanotherop_approval_tran
            'objlnOtherOpTran._Isvoid = mblnIsvoid
            'objlnOtherOpTran._Voiduserunkid = mintVoiduserunkid
            'objlnOtherOpTran._VoidDateTime = mdtVoiddatetime
            'objlnOtherOpTran._VoidReason = mstrVoidreason
            'If objlnOtherOpTran.Delete(mstrIdentifyGuid, objDataOperation) = False Then
            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            'Nilay (20-Sept-2016) -- End
            'Nilay (13-Sept-2016) -- End

            If blnIncludeBalanceTran = True Then

                objDataOperation.ClearParameters()

                strQ = "UPDATE lnloan_balance_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                       "WHERE lnemitranunkid = @lnemitranunkid "

                objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            _Lnemitranunkid = intUnkid

            If InsertAuditEMI_Tran(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            If blnIsFromLoanAdvanceList = False Then
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objLoan_Advance As New clsLoan_Advance
            Dim dsLoan As DataSet = Nothing
            'Nilay (25-Mar-2016) -- Start
            'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", mdtEffectivedate, "", mintLoanadvancetranunkid, , False, , , True)
            dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, _
                                                               xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, _
                                                               "List", mdtEffectivedate, "", mintLoanadvancetranunkid, , False, , , True)
            'Nilay (25-Mar-2016) -- End

            If dsLoan.Tables(0).Rows.Count > 0 Then
                    objLoan_Advance._DataOpr = objDataOperation 'Sohail (16 Oct 2019) 
                objLoan_Advance._Loanadvancetranunkid = mintLoanadvancetranunkid
                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                objLoan_Advance._Balance_AmountPaidCurrency = objLoan_Advance._Balance_AmountPaidCurrency + CDec(dsLoan.Tables(0).Rows(0).Item("TotPrincipalAmountPaidCurrency"))
                    objLoan_Advance._ClientIP = mstrClientIP
                    objLoan_Advance._FormName = mstrFormName
                    objLoan_Advance._HostName = mstrHostName
                    objLoan_Advance._AuditDate = mdtAuditDate
                    objLoan_Advance._AuditUserId = mintAuditUserId
objLoan_Advance._CompanyUnkid = mintCompanyUnkid
                    objLoan_Advance._FromWeb = mblnIsWeb
                    objLoan_Advance._LoginEmployeeUnkid = mintLoginEmployeeunkid

                'Nilay (05-May-2016) -- Start
                'If objLoan_Advance.Update(, objDataOperation) = False Then
                If objLoan_Advance.Update(objDataOperation) = False Then
                    'Nilay (05-May-2016) -- End

                    exForce = New Exception(objLoan_Advance._Message)
                    Throw exForce
                End If
            End If
            'Sohail (07 May 2015) -- End
            End If
            'Nilay (20-Sept-2016) -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        Try
            strQ = "SELECT 1 FROM lnloan_balance_tran WHERE isvoid = 0 AND lnemitranunkid = @lnemitranunkid "

            objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtEffDate As Date, ByVal intLoanAdvUnkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef retintUnkId As Integer = 0) As Boolean
        'Sohail (15 Dec 2015) - [retintUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            retintUnkId = 0 'Sohail (15 Dec 2015)

            strQ = "SELECT " & _
              "  lnemitranunkid " & _
              ", loanadvancetranunkid " & _
              ", periodunkid " & _
              ", effectivedate " & _
              ", emi_tenure " & _
              ", emi_amount " & _
              ", loan_duration " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", end_date " & _
             "FROM lnloan_emitenure_tran " & _
             "WHERE isvoid = 0 " & _
             "AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate " & _
             "AND loanadvancetranunkid = @loanadvancetranunkid "

            If intUnkid > 0 Then
                strQ &= " AND lnemitranunkid <> @lnemitranunkid"
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffDate).ToString)
            objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanAdvUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            If dsList.Tables(0).Rows.Count > 0 Then
                retintUnkId = CInt(dsList.Tables(0).Rows(0).Item("lnemitranunkid"))
            End If
            'Sohail (15 Dec 2015) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function InsertAuditEMI_Tran(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atlnloan_emitenure_tran ( " & _
                    "  lnemitranunkid " & _
                    ", loanadvancetranunkid " & _
                    ", periodunkid " & _
                    ", effectivedate " & _
                    ", emi_tenure " & _
                    ", emi_amount " & _
                    ", loan_duration " & _
                    ", end_date " & _
                    ", exchange_rate " & _
                    ", basecurrency_amount " & _
                    ", principal_amount " & _
                    ", interest_amount " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", form_name " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    ", isweb" & _
                    ", isdefault " & _
                    ", approverunkid " & _
                    ", identify_guid " & _
                    ") VALUES (" & _
                    "  @lnemitranunkid " & _
                    ", @loanadvancetranunkid " & _
                    ", @periodunkid " & _
                    ", @effectivedate " & _
                    ", @emi_tenure " & _
                    ", @emi_amount " & _
                    ", @loan_duration " & _
                    ", @end_date " & _
                    ", @exchange_rate " & _
                    ", @basecurrency_amount " & _
                    ", @principal_amount " & _
                    ", @interest_amount " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", @auditdatetime " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @form_name " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    " " & _
                    ", @isweb" & _
                    ", @isdefault " & _
                    ", @approverunkid " & _
                    ", @identify_guid " & _
                    ") "
            'Nilay (13-Sept-2016) -- [identify_guid]

            'Nilay (03-Feb-2016) -- Start
            'ENHANCEMENT - Add isdefault & approverunkid for Loan Parameters' approval process
            '[isdefault],[approverunkid]
            'Nilay (03-Feb-2016) -- End

            xDataOpr.ClearParameters()
            xDataOpr.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLnemitranunkid.ToString)
            xDataOpr.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)
            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            xDataOpr.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            xDataOpr.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmi_Tenure.ToString)
            xDataOpr.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_Amount.ToString)
            xDataOpr.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoan_Duration.ToString)
            xDataOpr.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            xDataOpr.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_rate)
            xDataOpr.AddParameter("@basecurrency_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBasecurrency_amount)
            xDataOpr.AddParameter("@principal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPrincipal_amount)
            xDataOpr.AddParameter("@interest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterest_amount)
            xDataOpr.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisdefault.ToString)
            xDataOpr.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverunkid.ToString)

            xDataOpr.AddParameter("@identify_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentifyGuid)
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditEMI_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Loan Installment Information is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 3, " Process Payroll.")
            Language.setMessage(mstrModuleName, 4, " Loan Payment List.")
            Language.setMessage(mstrModuleName, 5, " Receipt Payment List.")
            Language.setMessage(mstrModuleName, 6, " Loan Installment Change.")
            Language.setMessage(mstrModuleName, 7, " Loan Topup Added.")
            Language.setMessage(mstrModuleName, 8, " Loan Rate Change.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of")
            Language.setMessage(mstrModuleName, 10, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clslnloan_emitenure_tran
'    Private Const mstrModuleName = "clslnloan_emitenure_tran"
'    Dim mstrMessage As String = ""

'#Region " Private variables "

'    Private mintLnemitranunkid As Integer
'    Private mintLoanadvancetranunkid As Integer
'    Private mdtTran As DataTable
'    Private mstrWebFormName As String = String.Empty
'    Private mstrWebIP As String = ""
'    Private mstrWebHost As String = ""

'#End Region

'#Region " Properties "

'    Public Property _Loanadvancetranunkid() As Integer
'        Get
'            Return mintLoanadvancetranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintLoanadvancetranunkid = value
'            Call GetEmiTran()
'        End Set
'    End Property

'    Public Property _DataTable() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property

'    Public Property _WebFormName() As String
'        Get
'            Return mstrWebFormName
'        End Get
'        Set(ByVal value As String)
'            mstrWebFormName = value
'        End Set
'    End Property

'    Public Property _WebIP() As String
'        Get
'            Return mstrWebIP
'        End Get
'        Set(ByVal value As String)
'            mstrWebIP = value
'        End Set
'    End Property

'    Public Property _WebHost() As String
'        Get
'            Return mstrWebHost
'        End Get
'        Set(ByVal value As String)
'            mstrWebHost = value
'        End Set
'    End Property

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        Try
'            mdtTran = New DataTable("emitran")

'            mdtTran.Columns.Add("lnemitranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("loanadvancetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("effectivedate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
'            mdtTran.Columns.Add("emi_tenure", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("emi_amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
'            mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
'            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
'            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = 0
'            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("loan_duration", System.Type.GetType("System.Int32")).DefaultValue = 0

'            '=========================================== DISPLAY & CHECKING PURPOSE
'            mdtTran.Columns.Add("dperiod", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("ddate", System.Type.GetType("System.String")).DefaultValue = ""
'            mdtTran.Columns.Add("pstatusid", System.Type.GetType("System.Int32")).DefaultValue = 0
'            '=========================================== DISPLAY & CHECKING PURPOSE

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private/Public Methods "

'    Private Sub GetEmiTran()
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            StrQ = "SELECT " & _
'                   "  lnloan_emitenure_tran.lnemitranunkid " & _
'                   ", lnloan_emitenure_tran.loanadvancetranunkid " & _
'                   ", lnloan_emitenure_tran.periodunkid " & _
'                   ", lnloan_emitenure_tran.effectivedate " & _
'                   ", lnloan_emitenure_tran.emi_tenure " & _
'                   ", lnloan_emitenure_tran.emi_amount " & _
'                   ", lnloan_emitenure_tran.userunkid " & _
'                   ", lnloan_emitenure_tran.isvoid " & _
'                   ", lnloan_emitenure_tran.voiduserunkid " & _
'                   ", lnloan_emitenure_tran.voiddatetime " & _
'                   ", lnloan_emitenure_tran.voidreason " & _
'                   ", CONVERT(CHAR(8),effectivedate,112) AS ddate " & _
'                   ", cfcommon_period_tran.period_name AS dperiod " & _
'                   ", cfcommon_period_tran.statusid AS pstatusid " & _
'                   ", lnloan_emitenure_tran.loan_duration " & _
'                   "FROM lnloan_emitenure_tran " & _
'                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = lnloan_emitenure_tran.periodunkid " & _
'                   "WHERE lnloan_emitenure_tran.loanadvancetranunkid = @loanadvancetranunkid " & _
'                   "    AND lnloan_emitenure_tran.isvoid = 0 AND cfcommon_period_tran.isactive = 1 "

'            objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            mdtTran.Rows.Clear()

'            For Each xRow As DataRow In dsList.Tables(0).Rows
'                If xRow.Item("ddate").ToString.Trim.Length > 0 Then
'                    xRow.Item("ddate") = eZeeDate.convertDate(xRow.Item("ddate").ToString).ToShortDateString
'                End If
'                mdtTran.ImportRow(xRow)
'            Next

'            If mdtTran.Rows.Count <= 0 Then
'                mdtTran.Rows.Add(mdtTran.NewRow)
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetEmiTran", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Public Function InsertUpdateDelete_EMI_Tran(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnIncludeBalanceTran As Boolean = False, Optional ByVal intUserId As Integer = 0) As Boolean
'        Dim i As Integer
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Dim objDataOperation As clsDataOperation

'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'        Else
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'        End If
'        Try
'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"
'                                If .Item("periodunkid") <= 0 Then Continue For

'                                strQ = "INSERT INTO lnloan_emitenure_tran ( " & _
'                                           "  loanadvancetranunkid " & _
'                                           ", periodunkid " & _
'                                           ", effectivedate " & _
'                                           ", emi_tenure " & _
'                                           ", emi_amount " & _
'                                           ", loan_duration " & _
'                                           ", userunkid " & _
'                                           ", isvoid " & _
'                                           ", voiduserunkid " & _
'                                           ", voiddatetime " & _
'                                           ", voidreason" & _
'                                       ") VALUES (" & _
'                                           "  @loanadvancetranunkid " & _
'                                           ", @periodunkid " & _
'                                           ", @effectivedate " & _
'                                           ", @emi_tenure " & _
'                                           ", @emi_amount " & _
'                                           ", @loan_duration " & _
'                                           ", @userunkid " & _
'                                           ", @isvoid " & _
'                                           ", @voiduserunkid " & _
'                                           ", @voiddatetime " & _
'                                           ", @voidreason" & _
'                                       "); SELECT @@identity"

'                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanadvancetranunkid)
'                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
'                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))
'                                objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("emi_tenure"))
'                                objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("emi_amount"))
'                                objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loan_duration"))
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                dsList = objDataOperation.ExecQuery(strQ, "List")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                mintLnemitranunkid = dsList.Tables(0).Rows(0).Item(0)

'                                If InsertAuditEMI_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.ADD, mintLnemitranunkid) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "U"
'                                If .Item("periodunkid") <= 0 Then Continue For

'                                strQ = "UPDATE lnloan_emitenure_tran SET " & _
'                                       "  loanadvancetranunkid = @loanadvancetranunkid" & _
'                                       ", periodunkid = @periodunkid" & _
'                                       ", effectivedate = @effectivedate" & _
'                                       ", emi_tenure = @emi_tenure" & _
'                                       ", emi_amount = @emi_amount" & _
'                                       ", loan_duration = @loan_duration " & _
'                                       ", userunkid = @userunkid" & _
'                                       ", isvoid = @isvoid" & _
'                                       ", voiduserunkid = @voiduserunkid" & _
'                                       ", voiddatetime = @voiddatetime" & _
'                                       ", voidreason = @voidreason " & _
'                                       "WHERE lnemitranunkid = @lnemitranunkid "

'                                objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lnemitranunkid"))
'                                objDataOperation.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loanadvancetranunkid"))
'                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid"))
'                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))
'                                objDataOperation.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("emi_tenure"))
'                                objDataOperation.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("emi_amount"))
'                                objDataOperation.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loan_duration"))
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                If InsertAuditEMI_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.EDIT, .Item("lnemitranunkid")) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "D"

'                                If .Item("periodunkid") <= 0 Then Continue For

'                                strQ = "UPDATE lnloan_emitenure_tran SET " & _
'                                       "  isvoid = @isvoid" & _
'                                       ", voiduserunkid = @voiduserunkid" & _
'                                       ", voiddatetime = @voiddatetime" & _
'                                       ", voidreason = @voidreason " & _
'                                       "WHERE lnemitranunkid = @lnemitranunkid "

'                                objDataOperation.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lnemitranunkid"))
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                If InsertAuditEMI_Tran(objDataOperation, mdtTran.Rows(i), enAuditType.DELETE, .Item("lnemitranunkid")) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                        End Select
'                    End If
'                End With
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_EMI_Tran", mstrModuleName)
'            Return False
'        Finally
'        End Try
'        Return True
'    End Function

'    Private Function InsertAuditEMI_Tran(ByVal xDataOpr As clsDataOperation, _
'                                                        ByVal xRow As DataRow, _
'                                                        ByVal xAuditType As enAuditType, _
'                                                        ByVal xUnkid As Integer) As Boolean
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception = Nothing
'        Try
'            strQ = "INSERT INTO atlnloan_emitenure_tran ( " & _
'                    "  lnemitranunkid " & _
'                    ", loanadvancetranunkid " & _
'                    ", periodunkid " & _
'                    ", effectivedate " & _
'                    ", emi_tenure " & _
'                    ", emi_amount " & _
'                    ", loan_duration " & _
'                    ", audittype " & _
'                    ", audituserunkid " & _
'                    ", auditdatetime " & _
'                    ", ip " & _
'                    ", machine_name " & _
'                    ", form_name " & _
'                    ", module_name1 " & _
'                    ", module_name2 " & _
'                    ", module_name3 " & _
'                    ", module_name4 " & _
'                    ", module_name5 " & _
'                    ", isweb" & _
'                    ") VALUES (" & _
'                    "  @lnemitranunkid " & _
'                    ", @loanadvancetranunkid " & _
'                    ", @periodunkid " & _
'                    ", @effectivedate " & _
'                    ", @emi_tenure " & _
'                    ", @emi_amount " & _
'                    ", @loan_duration " & _
'                    ", @audittype " & _
'                    ", @audituserunkid " & _
'                    ", @auditdatetime " & _
'                    ", @ip " & _
'                    ", @machine_name " & _
'                    ", @form_name " & _
'                    ", @module_name1 " & _
'                    ", @module_name2 " & _
'                    ", @module_name3 " & _
'                    ", @module_name4 " & _
'                    ", @module_name5 " & _
'                    ", @isweb" & _
'                    ") "

'            xDataOpr.ClearParameters()
'            xDataOpr.AddParameter("@lnemitranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid)
'            xDataOpr.AddParameter("@loanadvancetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("loanadvancetranunkid"))
'            xDataOpr.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("periodunkid"))
'            xDataOpr.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xRow.Item("effectivedate"))
'            xDataOpr.AddParameter("@emi_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("emi_tenure"))
'            xDataOpr.AddParameter("@emi_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xRow.Item("emi_amount"))
'            xDataOpr.AddParameter("@loan_duration", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("loan_duration"))
'            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
'            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRow.Item("userunkid"))
'            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

'            If mstrWebIP.ToString().Trim.Length <= 0 Then
'                mstrWebIP = getIP()
'            End If
'            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

'            If mstrWebHost.ToString().Length <= 0 Then
'                mstrWebHost = getHostName()
'            End If
'            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

'            If mstrWebFormName.Trim.Length <= 0 Then
'                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
'                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(0, Boolean).ToString)
'                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
'            Else
'                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
'                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CType(1, Boolean).ToString)
'                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
'            End If
'            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
'            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
'            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
'            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

'            xDataOpr.ExecNonQuery(strQ)

'            If xDataOpr.ErrorMessage <> "" Then
'                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditEMI_Tran; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'        End Try
'        Return True
'    End Function

'#End Region

'End Class

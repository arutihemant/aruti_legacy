﻿'************************************************************************************************************************************
'Class Name : clsapprover_scheme_mapping.vb
'Purpose    :
'Date       :4/15/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsapprover_scheme_mapping
    Private Const mstrModuleName = "clsapprover_scheme_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLoanschememappingunkid As Integer
    Private mintApproverunkid As Integer
    Private mintLoanschemeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtLoanScheme As DataTable = Nothing
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty

#End Region

#Region "Constructor"

    Public Sub New()

        mdtLoanScheme = New DataTable("List")
        mdtLoanScheme.Columns.Add("ischecked", Type.GetType("System.Boolean"))
        mdtLoanScheme.Columns.Add("Loanschememappingunkid", Type.GetType("System.Int32"))
        mdtLoanScheme.Columns.Add("approverunkid", Type.GetType("System.Int32"))
        mdtLoanScheme.Columns.Add("loanschemeunkid", Type.GetType("System.Int32"))
        mdtLoanScheme.Columns.Add("code", Type.GetType("System.String"))
        mdtLoanScheme.Columns.Add("name", Type.GetType("System.String"))

    End Sub

#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set loanschememappingunkid
    '' Modify By: Pinkal
    '' </summary>

    'Nilay (07-Feb-2016) -- Start
    'Public Property _Loanschememappingunkid() As Integer
    Public Property _Loanschememappingunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        'Nilay (07-Feb-2016) -- End
        Get
            Return mintLoanschememappingunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschememappingunkid = value
            'Nilay (07-Feb-2016) -- Start
            'Call GetData()
            Call GetData(objDataOp)
            'Nilay (07-Feb-2016) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtLoanScheme
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _dtLoanScheme() As DataTable
        Get
            Return mdtLoanScheme
        End Get
        Set(ByVal value As DataTable)
            mdtLoanScheme = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (07-Feb-2016) -- Start
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        'Nilay (07-Feb-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'Dim objDataOperation As New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Nilay (07-Feb-2016) -- End

        Try
            'Nilay (05-May-2016) -- Start
            objDataOperation.ClearParameters()
            'Nilay (05-May-2016) -- End

            strQ = "SELECT " & _
                      "  loanschememappingunkid " & _
                      ", approverunkid " & _
                      ", loanschemeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM lnapprover_scheme_mapping " & _
                     "WHERE loanschememappingunkid = @loanschememappingunkid "

            objDataOperation.AddParameter("@loanschememappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschememappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanschememappingunkid = CInt(dtRow.Item("loanschememappingunkid"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Nilay (07-Feb-2016) -- Start
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverunkid As Integer = -1, _
                            Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        'Nilay (07-Feb-2016) -- [objDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'Dim objDataOperation As New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        'Nilay (07-Feb-2016) -- End

        Try
            strQ = "SELECT " & _
                      "  lnapprover_scheme_mapping.loanschememappingunkid " & _
                      ", lnapprover_scheme_mapping.approverunkid " & _
                      ", lnapprover_scheme_mapping.loanschemeunkid " & _
                      ", ISNULL(lnloan_scheme_master.code,'') as code " & _
                      ", ISNULL(lnloan_scheme_master.name,'') as name " & _
                      ", lnapprover_scheme_mapping.userunkid " & _
                      ", lnapprover_scheme_mapping.isvoid " & _
                      ", lnapprover_scheme_mapping.voiduserunkid " & _
                      ", lnapprover_scheme_mapping.voiddatetime " & _
                      ", lnapprover_scheme_mapping.voidreason " & _
                      " FROM lnapprover_scheme_mapping " & _
                      " LEFT JOIN lnloan_scheme_master on lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid "

            If blnOnlyActive Then
                strQ &= " WHERE lnapprover_scheme_mapping.isvoid = 0 "
            End If

            If intApproverunkid > 0 Then
                strQ &= " AND lnapprover_scheme_mapping.approverunkid = " & intApproverunkid
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Nilay (07-Feb-2016) -- Start
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnapprover_scheme_mapping) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try

            If _dtLoanScheme IsNot Nothing AndAlso _dtLoanScheme.Rows.Count > 0 Then

                For Each dr As DataRow In _dtLoanScheme.Rows
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing

                    If isExist(mintApproverunkid, CInt(dr("loanschemeunkid")), objDataOperation) Then
                        If CBool(dr("ischecked")) = False Then
                            mblnIsvoid = True
                            mintVoiduserunkid = mintUserunkid
                            mdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            Delete(objDataOperation, dr)
                        End If
                        Continue For
                    End If

                    If CBool(dr("ischecked")) Then

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                        objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("loanschemeunkid")).ToString)
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                        strQ = "INSERT INTO lnapprover_scheme_mapping ( " & _
                                    "  approverunkid " & _
                                    ", loanschemeunkid " & _
                                    ", userunkid " & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime " & _
                                    ", voidreason" & _
                                  ") VALUES (" & _
                                    "  @approverunkid " & _
                                    ", @loanschemeunkid " & _
                                    ", @userunkid " & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime " & _
                                    ", @voidreason" & _
                                  "); SELECT @@identity"


                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintLoanschememappingunkid = dsList.Tables(0).Rows(0).Item(0)

                        If InsertATLoanSchemeMapping(objDOperation, 1, CInt(dr("loanschemeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnapprover_scheme_mapping) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            If dr IsNot Nothing Then

                strQ = " UPDATE lnapprover_scheme_mapping set " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime=@voiddatetime  " & _
                          ", voidreason=@voidreason  " & _
                          " WHERE loanschemeunkid = @loanschemeunkid  AND approverunkid = @approverunkid AND isvoid = 0"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("loanschemeunkid").ToString)
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintLoanschememappingunkid = CInt(dr("loanschememappingunkid"))
                If InsertATLoanSchemeMapping(objDataOperation, 3, CInt(dr("loanschemeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnapprover_scheme_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "DELETE FROM lnapprover_scheme_mapping " & _
            "WHERE loanschememappingunkid = @loanschememappingunkid "

            objDataOperation.AddParameter("@loanschememappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intappreoverunkid As Integer, ByVal intloanschemeId As Integer, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  loanschememappingunkid " & _
                      ", approverunkid " & _
                      ", loanschemeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      "FROM lnapprover_scheme_mapping " & _
                      " WHERE approverunkid = @approverunkid " & _
                      " AND loanschemeunkid = @loanschemeunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappreoverunkid)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intloanschemeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertATLoanSchemeMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intSchemeID As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanschememappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschememappingunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeID.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atlnapprover_scheme_mapping ( " & _
                                "  loanschememappingunkid " & _
                                ", approverunkid " & _
                                ", loanschemeunkid " & _
                                 ",audittype " & _
                                ",audituserunkid " & _
                                ",auditdatetime " & _
                                ",ip " & _
                                ",machine_name " & _
                                ",form_name " & _
                                ",isweb " & _
                              ") VALUES (" & _
                                "  @loanschememappingunkid " & _
                                ", @approverunkid " & _
                                ", @loanschemeunkid " & _
                                ", @audittype " & _
                                ", @audituserunkid " & _
                                ", @auditdatetime " & _
                                ", @ip " & _
                                ", @machine_name " & _
                                ", @form_name " & _
                                ", @isweb " & _
                              "); SELECT @@identity"



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATLoanSchemeMapping , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanSchemeMappingUnkId(ByVal intappreoverunkid As Integer, ByVal intloanSchemeId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  loanschememappingunkid " & _
                    ", approverunkid " & _
                    ", loanschemeunkid " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                   "FROM lnapprover_scheme_mapping " & _
                   "WHERE approverunkid = @approverunkid " & _
                   "AND loanschemeunkid = @loanschemeunkid AND isvoid = 0 "


            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappreoverunkid)
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intloanSchemeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("loanschememappingunkid"))
            Else
                Return -1
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeMappingUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanSchemeForMapping(ByVal intApproveID As Integer) As DataTable
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLoanScheme As New clsLoan_Scheme
            dsFill = objLoanScheme.getComboList(False, "List")
            For Each dr As DataRow In dsFill.Tables(0).Rows

                Dim drRow As DataRow = mdtLoanScheme.NewRow
                drRow("Loanschememappingunkid") = GetLoanSchemeMappingUnkId(intApproveID, CInt(dr("loanschemeunkid")))
                drRow("ischecked") = isExist(intApproveID, CInt(dr("loanschemeunkid")))
                drRow("approverunkid") = intApproveID
                drRow("loanschemeunkid") = dr("loanschemeunkid").ToString()
                drRow("code") = dr("Code").ToString()
                drRow("name") = dr("name").ToString()
                mdtLoanScheme.Rows.Add(drRow)
            Next
            Return mdtLoanScheme
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoanSchemeForMapping", mstrModuleName)
        End Try
        Return Nothing
    End Function

End Class
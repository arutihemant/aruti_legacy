﻿'************************************************************************************************************************************
'Class Name : clsBankEdi_master.vb
'Purpose    :
'Date       :08/09/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBankEdi_master
    Private Shared ReadOnly mstrModuleName As String = "clsBankEdi_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBankediunkid As Integer
    Private mstrEdicode As String = String.Empty
    Private mstrEdiname As String = String.Empty
    Private mstrHeaderid As String = String.Empty
    Private mstrFiletype As String = String.Empty
    Private mdtProcessdate As Date
    Private mstrHeaderclearcode As String = String.Empty
    Private mstrFileid As String = String.Empty
    Private mstrHeadercompname As String = String.Empty
    Private mstrSenderid As String = String.Empty
    Private mstrReceiver As String = String.Empty
    Private mstrWorkcode As String = String.Empty
    Private mstrVersion As String = String.Empty
    Private mstrTrailerid As String = String.Empty
    Private mstrTrailerclearcode As String = String.Empty
    Private mstrPaymentid As String = String.Empty
    Private mstrSalarycode As String = String.Empty
    Private mstrProcessref As String = String.Empty
    Private mstrContracode As String = String.Empty
    Private mdtPaydate As Date
    Private mstrDatacompname As String = String.Empty
    Private mintTranheadunkid As Integer
    Private mintCurrencyunkid As Integer
    Private mintBankgroupunkid As Integer
    Private mintBranchunkid As Integer
    Private mstrBankaccno As String = String.Empty
    Private mintAccounttypeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrEdiname1 As String = String.Empty
    Private mstrEdiname2 As String = String.Empty
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bankediunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Bankediunkid() As Integer
        Get
            Return mintBankediunkid
        End Get
        Set(ByVal value As Integer)
            mintBankediunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set edicode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Edicode() As String
        Get
            Return mstrEdicode
        End Get
        Set(ByVal value As String)
            mstrEdicode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ediname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ediname() As String
        Get
            Return mstrEdiname
        End Get
        Set(ByVal value As String)
            mstrEdiname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set headerid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Headerid() As String
        Get
            Return mstrHeaderid
        End Get
        Set(ByVal value As String)
            mstrHeaderid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set filetype
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Filetype() As String
        Get
            Return mstrFiletype
        End Get
        Set(ByVal value As String)
            mstrFiletype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Processdate() As Date
        Get
            Return mdtProcessdate
        End Get
        Set(ByVal value As Date)
            mdtProcessdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set headerclearcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Headerclearcode() As String
        Get
            Return mstrHeaderclearcode
        End Get
        Set(ByVal value As String)
            mstrHeaderclearcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fileid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Fileid() As String
        Get
            Return mstrFileid
        End Get
        Set(ByVal value As String)
            mstrFileid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set headercompname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Headercompname() As String
        Get
            Return mstrHeadercompname
        End Get
        Set(ByVal value As String)
            mstrHeadercompname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set senderid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Senderid() As String
        Get
            Return mstrSenderid
        End Get
        Set(ByVal value As String)
            mstrSenderid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set receiver
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Receiver() As String
        Get
            Return mstrReceiver
        End Get
        Set(ByVal value As String)
            mstrReceiver = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Workcode() As String
        Get
            Return mstrWorkcode
        End Get
        Set(ByVal value As String)
            mstrWorkcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set version
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Version() As String
        Get
            Return mstrVersion
        End Get
        Set(ByVal value As String)
            mstrVersion = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trailerid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trailerid() As String
        Get
            Return mstrTrailerid
        End Get
        Set(ByVal value As String)
            mstrTrailerid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trailerclearcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Trailerclearcode() As String
        Get
            Return mstrTrailerclearcode
        End Get
        Set(ByVal value As String)
            mstrTrailerclearcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paymentid() As String
        Get
            Return mstrPaymentid
        End Get
        Set(ByVal value As String)
            mstrPaymentid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set salarycode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Salarycode() As String
        Get
            Return mstrSalarycode
        End Get
        Set(ByVal value As String)
            mstrSalarycode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processref
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Processref() As String
        Get
            Return mstrProcessref
        End Get
        Set(ByVal value As String)
            mstrProcessref = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contracode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Contracode() As String
        Get
            Return mstrContracode
        End Get
        Set(ByVal value As String)
            mstrContracode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paydate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Paydate() As Date
        Get
            Return mdtPaydate
        End Get
        Set(ByVal value As Date)
            mdtPaydate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datacompname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Datacompname() As String
        Get
            Return mstrDatacompname
        End Get
        Set(ByVal value As String)
            mstrDatacompname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bankgroupunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Bankgroupunkid() As Integer
        Get
            Return mintBankgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBankgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bankaccno
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Bankaccno() As String
        Get
            Return mstrBankaccno
        End Get
        Set(ByVal value As String)
            mstrBankaccno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttypeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accounttypeunkid() As Integer
        Get
            Return mintAccounttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintAccounttypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ediname1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ediname1() As String
        Get
            Return mstrEdiname1
        End Get
        Set(ByVal value As String)
            mstrEdiname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ediname2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ediname2() As String
        Get
            Return mstrEdiname2
        End Get
        Set(ByVal value As String)
            mstrEdiname2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  bankediunkid " & _
              ", edicode " & _
              ", ediname " & _
              ", headerid " & _
              ", filetype " & _
              ", processdate " & _
              ", headerclearcode " & _
              ", fileid " & _
              ", headercompname " & _
              ", senderid " & _
              ", receiver " & _
              ", workcode " & _
              ", version " & _
              ", trailerid " & _
              ", trailerclearcode " & _
              ", paymentid " & _
              ", salarycode " & _
              ", processref " & _
              ", contracode " & _
              ", paydate " & _
              ", datacompname " & _
              ", tranheadunkid " & _
              ", currencyunkid " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bankaccno " & _
              ", accounttypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ediname1 " & _
              ", ediname2 " & _
              ", voidreason " & _
             "FROM prbankedi_master " & _
             "WHERE bankediunkid = @bankediunkid "

            objDataOperation.AddParameter("@bankediunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBankediUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbankediunkid = CInt(dtRow.Item("bankediunkid"))
                mstredicode = dtRow.Item("edicode").ToString
                mstrediname = dtRow.Item("ediname").ToString
                mstrheaderid = dtRow.Item("headerid").ToString
                mstrfiletype = dtRow.Item("filetype").ToString
                mdtprocessdate = dtRow.Item("processdate")
                mstrheaderclearcode = dtRow.Item("headerclearcode").ToString
                mstrfileid = dtRow.Item("fileid").ToString
                mstrheadercompname = dtRow.Item("headercompname").ToString
                mstrsenderid = dtRow.Item("senderid").ToString
                mstrreceiver = dtRow.Item("receiver").ToString
                mstrworkcode = dtRow.Item("workcode").ToString
                mstrversion = dtRow.Item("version").ToString
                mstrtrailerid = dtRow.Item("trailerid").ToString
                mstrtrailerclearcode = dtRow.Item("trailerclearcode").ToString
                mstrpaymentid = dtRow.Item("paymentid").ToString
                mstrsalarycode = dtRow.Item("salarycode").ToString
                mstrprocessref = dtRow.Item("processref").ToString
                mstrcontracode = dtRow.Item("contracode").ToString
                mdtpaydate = dtRow.Item("paydate")
                mstrdatacompname = dtRow.Item("datacompname").ToString
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintcurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintbankgroupunkid = CInt(dtRow.Item("bankgroupunkid"))
                mintbranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrbankaccno = dtRow.Item("bankaccno").ToString
                mintaccounttypeunkid = CInt(dtRow.Item("accounttypeunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrediname1 = dtRow.Item("ediname1").ToString
                mstrediname2 = dtRow.Item("ediname2").ToString
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  bankediunkid " & _
              ", edicode " & _
              ", ediname " & _
              ", headerid " & _
              ", filetype " & _
              ", convert(char(8),processdate,112) AS processdate " & _
              ", headerclearcode " & _
              ", fileid " & _
              ", headercompname " & _
              ", senderid " & _
              ", receiver " & _
              ", workcode " & _
              ", version " & _
              ", trailerid " & _
              ", trailerclearcode " & _
              ", paymentid " & _
              ", salarycode " & _
              ", processref " & _
              ", contracode " & _
              ", convert(char(8),paydate,112) AS paydate " & _
              ", datacompname " & _
              ", tranheadunkid " & _
              ", currencyunkid " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bankaccno " & _
              ", accounttypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ediname1 " & _
              ", ediname2 " & _
              ", voidreason " & _
             "FROM prbankedi_master " & _
             "WHERE ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prbankedi_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrEdicode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Bank EDI Code is already defined. Please define new Bank EDI Code.")
            Return False
        End If
        If isExist("", mstrEdiname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Bank EDI Name is already defined. Please define new Bank EDI Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@edicode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstredicode.ToString)
            objDataOperation.AddParameter("@ediname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrediname.ToString)
            objDataOperation.AddParameter("@headerid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrheaderid.ToString)
            objDataOperation.AddParameter("@filetype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFiletype.ToString)
            objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdate)
            objDataOperation.AddParameter("@headerclearcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHeaderclearcode.ToString)
            objDataOperation.AddParameter("@fileid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfileid.ToString)
            objDataOperation.AddParameter("@headercompname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrheadercompname.ToString)
            objDataOperation.AddParameter("@senderid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrsenderid.ToString)
            objDataOperation.AddParameter("@receiver", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreceiver.ToString)
            objDataOperation.AddParameter("@workcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrworkcode.ToString)
            objDataOperation.AddParameter("@version", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrversion.ToString)
            objDataOperation.AddParameter("@trailerid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtrailerid.ToString)
            objDataOperation.AddParameter("@trailerclearcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtrailerclearcode.ToString)
            objDataOperation.AddParameter("@paymentid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaymentid.ToString)
            objDataOperation.AddParameter("@salarycode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrsalarycode.ToString)
            objDataOperation.AddParameter("@processref", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrprocessref.ToString)
            objDataOperation.AddParameter("@contracode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContracode.ToString)
            objDataOperation.AddParameter("@paydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaydate)
            objDataOperation.AddParameter("@datacompname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatacompname.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbankgroupunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbranchunkid.ToString)
            objDataOperation.AddParameter("@bankaccno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbankaccno.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccounttypeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@ediname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEdiname1.ToString)
            objDataOperation.AddParameter("@ediname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrediname2.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "INSERT INTO prbankedi_master ( " & _
              "  edicode " & _
              ", ediname " & _
              ", headerid " & _
              ", filetype " & _
              ", processdate " & _
              ", headerclearcode " & _
              ", fileid " & _
              ", headercompname " & _
              ", senderid " & _
              ", receiver " & _
              ", workcode " & _
              ", version " & _
              ", trailerid " & _
              ", trailerclearcode " & _
              ", paymentid " & _
              ", salarycode " & _
              ", processref " & _
              ", contracode " & _
              ", paydate " & _
              ", datacompname " & _
              ", tranheadunkid " & _
              ", currencyunkid " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bankaccno " & _
              ", accounttypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ediname1 " & _
              ", ediname2 " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @edicode " & _
              ", @ediname " & _
              ", @headerid " & _
              ", @filetype " & _
              ", @processdate " & _
              ", @headerclearcode " & _
              ", @fileid " & _
              ", @headercompname " & _
              ", @senderid " & _
              ", @receiver " & _
              ", @workcode " & _
              ", @version " & _
              ", @trailerid " & _
              ", @trailerclearcode " & _
              ", @paymentid " & _
              ", @salarycode " & _
              ", @processref " & _
              ", @contracode " & _
              ", @paydate " & _
              ", @datacompname " & _
              ", @tranheadunkid " & _
              ", @currencyunkid " & _
              ", @bankgroupunkid " & _
              ", @branchunkid " & _
              ", @bankaccno " & _
              ", @accounttypeunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @ediname1 " & _
              ", @ediname2 " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBankediUnkId = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prbankedi_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrEdicode, "", mintBankediunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Bank EDI Code is already defined. Please define new Bank EDI Code.")
            Return False
        End If
        If isExist("", mstrEdiname, mintBankediunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Bank EDI Name is already defined. Please define new Bank EDI Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@bankediunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbankediunkid.ToString)
            objDataOperation.AddParameter("@edicode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstredicode.ToString)
            objDataOperation.AddParameter("@ediname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrediname.ToString)
            objDataOperation.AddParameter("@headerid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrheaderid.ToString)
            objDataOperation.AddParameter("@filetype", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfiletype.ToString)
            objDataOperation.AddParameter("@processdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdate)
            objDataOperation.AddParameter("@headerclearcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHeaderclearcode.ToString)
            objDataOperation.AddParameter("@fileid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfileid.ToString)
            objDataOperation.AddParameter("@headercompname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrheadercompname.ToString)
            objDataOperation.AddParameter("@senderid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrsenderid.ToString)
            objDataOperation.AddParameter("@receiver", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreceiver.ToString)
            objDataOperation.AddParameter("@workcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrworkcode.ToString)
            objDataOperation.AddParameter("@version", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrversion.ToString)
            objDataOperation.AddParameter("@trailerid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtrailerid.ToString)
            objDataOperation.AddParameter("@trailerclearcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtrailerclearcode.ToString)
            objDataOperation.AddParameter("@paymentid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrpaymentid.ToString)
            objDataOperation.AddParameter("@salarycode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrsalarycode.ToString)
            objDataOperation.AddParameter("@processref", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrprocessref.ToString)
            objDataOperation.AddParameter("@contracode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontracode.ToString)
            objDataOperation.AddParameter("@paydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPaydate)
            objDataOperation.AddParameter("@datacompname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatacompname.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbankgroupunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbranchunkid.ToString)
            objDataOperation.AddParameter("@bankaccno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbankaccno.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccounttypeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@ediname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEdiname1.ToString)
            objDataOperation.AddParameter("@ediname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrediname2.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "UPDATE prbankedi_master SET " & _
              "  edicode = @edicode" & _
              ", ediname = @ediname" & _
              ", headerid = @headerid" & _
              ", filetype = @filetype" & _
              ", processdate = @processdate" & _
              ", headerclearcode = @headerclearcode" & _
              ", fileid = @fileid" & _
              ", headercompname = @headercompname" & _
              ", senderid = @senderid" & _
              ", receiver = @receiver" & _
              ", workcode = @workcode" & _
              ", version = @version" & _
              ", trailerid = @trailerid" & _
              ", trailerclearcode = @trailerclearcode" & _
              ", paymentid = @paymentid" & _
              ", salarycode = @salarycode" & _
              ", processref = @processref" & _
              ", contracode = @contracode" & _
              ", paydate = @paydate" & _
              ", datacompname = @datacompname" & _
              ", tranheadunkid = @tranheadunkid" & _
              ", currencyunkid = @currencyunkid" & _
              ", bankgroupunkid = @bankgroupunkid" & _
              ", branchunkid = @branchunkid" & _
              ", bankaccno = @bankaccno" & _
              ", accounttypeunkid = @accounttypeunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", ediname1 = @ediname1" & _
              ", ediname2 = @ediname2" & _
              ", voidreason = @voidreason " & _
            "WHERE bankediunkid = @bankediunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prbankedi_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'StrQ = "DELETE FROM prbankedi_master " & _
            '"WHERE bankediunkid = @bankediunkid "
            strQ = "UPDATE prbankedi_master SET " & _
                        " isvoid = 1" & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE bankediunkid = @bankediunkid "

            objDataOperation.AddParameter("@bankediunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@bankediunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  bankediunkid " & _
              ", edicode " & _
              ", ediname " & _
              ", headerid " & _
              ", filetype " & _
              ", processdate " & _
              ", headerclearcode " & _
              ", fileid " & _
              ", headercompname " & _
              ", senderid " & _
              ", receiver " & _
              ", workcode " & _
              ", version " & _
              ", trailerid " & _
              ", trailerclearcode " & _
              ", paymentid " & _
              ", salarycode " & _
              ", processref " & _
              ", contracode " & _
              ", paydate " & _
              ", datacompname " & _
              ", tranheadunkid " & _
              ", currencyunkid " & _
              ", bankgroupunkid " & _
              ", branchunkid " & _
              ", bankaccno " & _
              ", accounttypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", ediname1 " & _
              ", ediname2 " & _
              ", voidreason " & _
             "FROM prbankedi_master " & _
             " WHERE ISNULL(isvoid,0) = 0 " 'Sohail (16 Oct 2010)

            If strCode.Trim <> "" Then
                strQ += " AND edicode = @code"
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If
            If strName.Trim <> "" Then
                strQ += " AND ediname = @name"
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If
            If intUnkid > 0 Then
                strQ &= " AND bankediunkid <> @bankediunkid"
                objDataOperation.AddParameter("@bankediunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

#Region " Message "
    '1, "This Bank EDI Code is already defined. Please define new Bank EDI Code."
    '2, "This Bank EDI Name is already defined. Please define new Bank EDI Name."
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Bank EDI Code is already defined. Please define new Bank EDI Code.")
			Language.setMessage(mstrModuleName, 2, "This Bank EDI Name is already defined. Please define new Bank EDI Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
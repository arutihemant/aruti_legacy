﻿Imports System.Runtime.InteropServices

Public Class clsZKAccessControl

    <DllImport("plcommpro.dll", EntryPoint:="Connect")> _
Public Shared Function Connect(ByVal Parameters As String) As IntPtr
    End Function
    <DllImport("plcommpro.dll", EntryPoint:="PullLastError")> _
    Public Shared Function PullLastError() As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="Disconnect")> _
Public Shared Sub Disconnect(ByVal h As IntPtr)
    End Sub

    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceParam")> _
Public Shared Function GetDeviceParam(ByVal h As IntPtr, ByRef buffer As Byte, ByVal buffersize As Integer, ByVal itemvalues As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="SetDeviceParam")> _
     Public Shared Function SetDeviceParam(ByVal h As IntPtr, ByVal itemvalues As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="ControlDevice")> _
      Public Shared Function ControlDevice(ByVal h As IntPtr, ByVal operationid As Integer, ByVal param1 As Integer, ByVal param2 As Integer, ByVal param3 As Integer, ByVal param4 As Integer, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceDataCount")> _
      Public Shared Function GetDeviceDataCount(ByVal h As IntPtr, ByVal tablename As String, ByVal filter As String, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceData")> _
    Public Shared Function GetDeviceData(ByVal h As IntPtr, ByRef buffer As Byte, ByVal buffersize As Integer, ByVal tablename As String, ByVal filename As String, ByVal filter As String, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="SetDeviceData")> _
      Public Shared Function SetDeviceData(ByVal h As IntPtr, ByVal tablename As String, ByVal data As String, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="GetRTLog")> _
   Public Shared Function GetRTLog(ByVal h As Integer, ByRef buffer As Byte, ByVal buffersize As Integer) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="SearchDevice")> _
    Public Shared Function SearchDevice(ByVal commtype As String, ByVal address As String, ByRef buffer As Byte) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="ModifyIPAddress")> _
       Public Shared Function ModifyIPAddress(ByVal commtype As String, ByVal address As String, ByVal buffer As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="GetDeviceFileData")> _
   Public Shared Function GetDeviceFileData(ByVal h As IntPtr, ByRef buffer As Byte, ByRef buffersize As Integer, ByVal filename As String, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="SetDeviceFileData")> _
        Public Shared Function SetDeviceFileData(ByVal h As IntPtr, ByVal filename As String, ByRef buffer As Byte, ByVal buffersize As Integer, ByVal options As String) As Integer
    End Function

    <DllImport("plcommpro.dll", EntryPoint:="DeleteDeviceData")> _
      Public Shared Function DeleteDeviceData(ByVal h As IntPtr, ByVal tablename As String, ByVal data As String, ByVal options As String) As Integer
    End Function

End Class

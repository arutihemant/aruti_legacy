﻿'************************************************************************************************************************************
'Class Name : clsNetConnection.vb
'Purpose    : To Chech Connection State
'Date       : 13 Jul 2011
'Written By : Sohail Patel
'Modified   : 
'************************************************************************************************************************************

Public Class clsNetConnectivity

#Region " Private Variables "

    Private strConnectionState As String
    Private blnIsConnected As Boolean
    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpSFlags As Int32, ByVal dwReserved As Int32) As Boolean

    Public Enum InetConnState
        Modem = &H1
        Lan = &H2
        Proxy = &H4
        Ras = &H10
        Offline = &H20
        Configured = &H40
    End Enum

#End Region

#Region " Public Properties "

    Public ReadOnly Property _Conected() As Boolean
        Get
            blnIsConnected = CheckInetConnection()
            Return blnIsConnected
        End Get
    End Property

    Public ReadOnly Property _ConnectionStatus() As String
        Get
            If blnIsConnected = False Then CheckInetConnection()
            Return strConnectionState
        End Get
    End Property
#End Region

#Region " Private Methods "

    Private Function CheckInetConnection() As Boolean

        Dim lngFlags As Long

        If InternetGetConnectedState(lngFlags, 0) Then
            ' True

            If lngFlags And InetConnState.Lan Then
                strConnectionState = "LAN."
            ElseIf lngFlags And InetConnState.Modem Then
                strConnectionState = "Modem."
            ElseIf lngFlags And InetConnState.Configured Then
                strConnectionState = "Configured."
            ElseIf lngFlags And InetConnState.Proxy Then
                strConnectionState = "Proxy"
            ElseIf lngFlags And InetConnState.Ras Then
                strConnectionState = "RAS."
            ElseIf lngFlags And InetConnState.Offline Then
                strConnectionState = "Offline."
            End If
            Return True
        Else
            ' False
            strConnectionState = "Not Connected."
        End If

    End Function

#End Region

End Class

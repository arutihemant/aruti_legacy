﻿

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsOT_Penalty

    Private Shared ReadOnly mstrModuleName As String = "clsOT_Penalty"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "

    Private mbintPenaltyunkid As Int64
    Private mintUptomins As Integer
    Private mintOt1_Penalty As Integer
    Private mintOt2_Penalty As Integer
    Private mintOt3_Penalty As Integer
    Private mintOt4_Penalty As Integer
    Private mintPolicyunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtOTPenalty As DataTable = Nothing
    'Private mstrWebFormName As String = String.Empty
    'Private mstrWebClientIP As String = String.Empty
    'Private mstrWebHostName As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()

        mdtOTPenalty = New DataTable("Penalty")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("Penaltyunkid")
            dCol.DataType = System.Type.GetType("System.Int64")
            dCol.DefaultValue = -1
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Uptomins")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Uptominsinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)


            dCol = New DataColumn("Ot1_Penalty")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot1_Penaltyinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot2_Penalty")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot2_Penaltyinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot3_Penalty")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot3_Penaltyinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot4_Penalty")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("Ot4_Penaltyinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("policyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtOTPenalty.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtOTPenalty.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 


    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Penaltyunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Int64
        Get
            Return mbintPenaltyunkid
        End Get
        Set(ByVal value As Int64)
            mbintPenaltyunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set uptomins
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Uptomins() As Integer
        Get
            Return mintUptomins
        End Get
        Set(ByVal value As Integer)
            mintUptomins = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot1_penalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot1_Penalty() As Integer
        Get
            Return mintOt1_Penalty
        End Get
        Set(ByVal value As Integer)
            mintOt1_Penalty = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot2_penalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot2_Penalty() As Integer
        Get
            Return mintOt2_Penalty
        End Get
        Set(ByVal value As Integer)
            mintOt2_Penalty = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot3_penalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot3_Penalty() As Integer
        Get
            Return mintOt3_Penalty
        End Get
        Set(ByVal value As Integer)
            mintOt3_Penalty = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot4_penalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ot4_Penalty() As Integer
        Get
            Return mintOt4_Penalty
        End Get
        Set(ByVal value As Integer)
            mintOt4_Penalty = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyunkid() As Integer
        Get
            Return mintPolicyunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtOTPenalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtOTPenalty() As DataTable
        Get
            Return mdtOTPenalty
        End Get
        Set(ByVal value As DataTable)
            mdtOTPenalty = value
        End Set
    End Property

    'Public Property _WebFormName() As String
    '    Get
    '        Return mstrWebFormName
    '    End Get
    '    Set(ByVal value As String)
    '        mstrWebFormName = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebClientIP() As String
    '    Set(ByVal value As String)
    '        mstrWebClientIP = value
    '    End Set
    'End Property

    'Public WriteOnly Property _WebHostName() As String
    '    Set(ByVal value As String)
    '        mstrWebHostName = value
    '    End Set
    'End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  penaltyunkid " & _
                      ", ISNULL(uptomins,0) AS uptomins " & _
                      ", ISNULL(ot1_penalty,0) AS ot1_penalty " & _
                      ", ISNULL(ot2_penalty,0) AS ot2_penalty " & _
                      ", ISNULL(ot3_penalty,0) AS  ot3_penalty " & _
                      ", ISNULL(ot4_penalty,0) AS  ot4_penalty " & _
                      ", policyunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM tnaot_penalty " & _
                     "WHERE penaltyunkid = @penaltyunkid "

            objDataOperation.AddParameter("@penaltyunkid", SqlDbType.BigInt, mbintPenaltyunkid.ToString.Length, mbintPenaltyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mbintPenaltyunkid = Convert.ToInt64(dtRow.Item("penaltyunkid"))
                mintUptomins = CInt(dtRow.Item("uptomins"))
                mintOt1_Penalty = CInt(dtRow.Item("ot1_penalty"))
                mintOt2_Penalty = CInt(dtRow.Item("ot2_penalty"))
                mintOt3_Penalty = CInt(dtRow.Item("ot3_penalty"))
                mintOt4_Penalty = CInt(dtRow.Item("ot4_penalty"))
                mintPolicyunkid = CInt(dtRow.Item("policyunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal xPolicyunkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  penaltyunkid " & _
                      ", 0 AS uptomins " & _
                      ", ISNULL(uptomins,0) AS  uptominsinsec " & _
                      ", 0 AS ot1_penalty " & _
                      ", ISNULL(ot1_penalty,0) AS ot1_penaltyinsec " & _
                      ", 0 AS ot2_penalty " & _
                      ", ISNULL(ot2_penalty,0) AS ot2_penaltyinsec " & _
                      ", 0 AS ot3_penalty " & _
                      ", ISNULL(ot3_penalty,0) AS ot3_penaltyinsec " & _
                      ", 0 AS ot4_penalty " & _
                      ", ISNULL(ot4_penalty,0) AS ot4_penaltyinsec " & _
                      ", policyunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ' ' AS AUD " & _
                      ", ' ' AS GUID " & _
                      " FROM tnaot_penalty "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If xPolicyunkid <> 0 Then
                strQ &= " AND policyunkid = @policyunkid"
                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPolicyunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                mdtOTPenalty = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertOTPenalty(ByVal objDataOperation As clsDataOperation, ByVal dtPenalty As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If dtPenalty Is Nothing OrElse dtPenalty.Rows.Count <= 0 Then Return True

            For i = 0 To dtPenalty.Rows.Count - 1
                With dtPenalty.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                          Select .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO tnaot_penalty ( " & _
                                          "  uptomins " & _
                                          ", ot1_penalty " & _
                                          ", ot2_penalty " & _
                                          ", ot3_penalty " & _
                                          ", ot4_penalty " & _
                                          ", policyunkid " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason " & _
                                      ") VALUES (" & _
                                          " @uptomins " & _
                                          ", @ot1_penalty " & _
                                          ", @ot2_penalty " & _
                                          ", @ot3_penalty " & _
                                          ", @ot4_penalty " & _
                                          ", @policyunkid " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason " & _
                                      "); SELECT @@identity"

                                objDataOperation.AddParameter("@uptomins", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("uptominsinsec"))
                                objDataOperation.AddParameter("@ot1_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot1_penaltyinsec"))
                                objDataOperation.AddParameter("@ot2_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot2_penaltyinsec"))
                                objDataOperation.AddParameter("@ot3_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot3_penaltyinsec"))
                                objDataOperation.AddParameter("@ot4_penalty", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot4_penaltyinsec"))
                                objDataOperation.AddParameter("@policyunkid", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintPolicyunkid)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Penaltyunkid(objDataOperation) = Convert.ToInt64(dsList.Tables(0).Rows(0).Item(0))

                                If InsertATOTPenalty(objDataOperation, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"

                                strQ = " UPDATE  tnaot_penalty SET " & _
                                          " uptomins = @uptomins  " & _
                                          ", ot1_penalty = @ot1_penalty " & _
                                          ", ot2_penalty = @ot2_penalty " & _
                                          ", ot3_penalty = @ot3_penalty " & _
                                          ", ot4_penalty = @ot4_penalty  " & _
                                          ", policyunkid = @policyunkid " & _
                                          " WHERE penaltyunkid = @penaltyunkid "

                                objDataOperation.AddParameter("@penaltyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("penaltyunkid"))
                                objDataOperation.AddParameter("@uptomins", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("uptominsinsec"))
                                objDataOperation.AddParameter("@ot1_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot1_penaltyinsec"))
                                objDataOperation.AddParameter("@ot2_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot2_penaltyinsec"))
                                objDataOperation.AddParameter("@ot3_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ot3_penaltyinsec"))
                                objDataOperation.AddParameter("@ot4_penalty", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot4_penaltyinsec"))
                                objDataOperation.AddParameter("@policyunkid", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintPolicyunkid)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Penaltyunkid(objDataOperation) = Convert.ToInt64(.Item("penaltyunkid"))

                                If InsertATOTPenalty(objDataOperation, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"

                                strQ = " UPDATE  tnaot_penalty SET " & _
                                          " isvoid  = @isvoid " & _
                                          ", voiduserunkid = @voiduserunkid " & _
                                          ", voidreason = @voidreason " & _
                                          ", voiddatetime = Getdate() " & _
                                          " WHERE penaltyunkid = @penaltyunkid "

                                objDataOperation.AddParameter("@penaltyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("penaltyunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                          
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Penaltyunkid(objDataOperation) = Convert.ToInt64(.Item("penaltyunkid"))

                                If InsertATOTPenalty(objDataOperation, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If
                End With
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertOTPenalty; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertATOTPenalty(ByVal objDataOperation As clsDataOperation,  ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO attnaot_penalty ( " & _
                        " penaltyunkid " & _
                        ", uptomins " & _
                        ", ot1_penalty " & _
                        ", ot2_penalty " & _
                        ", ot3_penalty " & _
                        ", ot4_penalty " & _
                        ", policyunkid " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name " & _
                        ", form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", isweb " & _
                  ") VALUES (" & _
                        "  @penaltyunkid " & _
                        ", @uptomins " & _
                        ", @ot1_penalty " & _
                        ", @ot2_penalty " & _
                        ", @ot3_penalty " & _
                        ", @ot4_penalty " & _
                        ", @policyunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", Getdate() " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        " " & _
                        ", @isweb ); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@penaltyunkid", SqlDbType.BigInt, eZeeDataType.INT_SIZE, mbintPenaltyunkid)
            objDataOperation.AddParameter("@uptomins", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUptomins)
            objDataOperation.AddParameter("@ot1_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOt1_Penalty)
            objDataOperation.AddParameter("@ot2_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOt2_Penalty)
            objDataOperation.AddParameter("@ot3_penalty", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOt3_Penalty)
            objDataOperation.AddParameter("@ot4_penalty", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintOt4_Penalty)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintPolicyunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATOTPenalty; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

End Class

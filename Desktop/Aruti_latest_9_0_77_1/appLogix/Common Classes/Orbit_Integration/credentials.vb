﻿Public Class credentials
    Private _username As String = String.Empty
    Private _password As String = String.Empty
    Private _agentUsername As String = String.Empty

    Public Property username() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Public Property password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property

    Public Property agentUsername() As String
        Get
            Return _agentUsername
        End Get
        Set(ByVal value As String)
            _agentUsername = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal _username As String, ByVal _password As String, ByVal _agentUsername As String)
        username = _username
        password = _password
        agentUsername = _agentUsername
    End Sub
End Class

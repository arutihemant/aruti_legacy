﻿Public Class clsdataDictionaries
    Private _referenceNo As String = String.Empty
    Private _credentials As New credentials
    Private _dataDictionaries As String = String.Empty

    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property

    Public Property dataDictionaries() As String
        Get
            Return _dataDictionaries
        End Get
        Set(ByVal value As String)
            _dataDictionaries = value
        End Set
    End Property
End Class

Public Class DictionariesValue
    Private _fieldName As String = String.Empty
    Private _key As String = String.Empty
    Private _value As String = String.Empty

    Public Property fieldName() As String
        Get
            Return _fieldName
        End Get
        Set(ByVal value As String)
            _fieldName = value
        End Set
    End Property

    Public Property key() As String
        Get
            Return _key
        End Get
        Set(ByVal value As String)
            _key = value
        End Set
    End Property

    Public Property value() As String
        Get
            Return _value
        End Get
        Set(ByVal value As String)
            _value = value
        End Set
    End Property

End Class
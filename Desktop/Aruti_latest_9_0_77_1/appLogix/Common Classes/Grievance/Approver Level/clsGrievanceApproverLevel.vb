﻿Imports eZeeCommonLib

Public Class clsGrievanceApproverLevel
    Private Shared ReadOnly mstrModuleName As String = "clsGrievanceApproverLevel"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintPriority As Integer
    Private mintApprovalSetting As Integer

    Private minAuditUserid As Integer
    Private minAuditDate As DateTime
    Private minClientIp As String
    Private minloginemployeeunkid As Integer
    Private mstrHostName As String
    Private mstrFormName As String
    Private blnIsFromWeb As Boolean

    Dim objDoOpseration As clsDataOperation
    Dim objclsConfigOptions As clsConfigOptions
#End Region


#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _ApprLevelunkid(Optional ByVal objDoOpseration As clsDataOperation = Nothing) As Integer
        Get
            Return mintApprLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintApprLevelunkid = value
            Call GetData(objDoOpseration)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApprovalSetting
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _ApprovalSetting() As Integer
        Get
            Return mintApprovalSetting
        End Get
        Set(ByVal value As Integer)
            mintApprovalSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public Property _loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Sub GetData(Optional ByVal objDoOpseration As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOpseration Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOpseration
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
              "  apprlevelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", approvalsettingid " & _
              ", isactive " & _
             "FROM greapproverlevel_master " & _
             "WHERE apprlevelunkid = @apprlevelunkid "

            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprLevelunkid.ToString())

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintApprLevelunkid = CInt(dtRow.Item("apprlevelunkid"))
                mstrLevelcode = dtRow.Item("levelcode").ToString
                mstrLevelname = dtRow.Item("levelname").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = dtRow.Item("priority")
                If dtRow.Item("approvalsettingid") IsNot DBNull.Value Then mintApprovalSetting = dtRow.Item("approvalsettingid")
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOpseration Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal ApprSetting As Integer = -1, Optional ByVal mblnblank As Boolean = False, Optional ByVal strfilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            If mblnblank = True Then
                strQ = "SELECT " & _
                       "  0 AS apprlevelunkid " & _
                       ", 0 AS levelcode " & _
                       ", '' AS levelname " & _
                       ", 0 AS priority " & _
                       ", 0 AS approvalsettingid " & _
                       ",0 AS isactive " & _
                       ", '' AS ApprovalSetting " & _
                       " UNION ALL "
            End If

            strQ += "SELECT " & _
             "  apprlevelunkid " & _
             ", levelcode " & _
             ", levelname " & _
             ", priority " & _
             ", approvalsettingid " & _
             ", isactive " & _
             ", CASE WHEN  approvalsettingid =" & Convert.ToInt32(enGrievanceApproval.UserAcess) & " THEN @UserAccessSetting " & _
             "       WHEN  approvalsettingid =" & Convert.ToInt32(enGrievanceApproval.ApproverEmpMapping) & " THEN @ApproverEmpMapping " & _
             " End AS ApprovalSetting " & _
             " FROM greapproverlevel_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            If ApprSetting > 0 Then
                strQ &= " And approvalsettingid =@approvalsettingid  "
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApprSetting)
            End If

            If strfilter <> "" Then
                strQ += "And " + strfilter
            End If

            objDataOperation.AddParameter("@UserAccessSetting", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Based On User Access"))
            objDataOperation.AddParameter("@ApproverEmpMapping", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Approver And Employee Mapping"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvapproverlevel_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mintApprovalSetting, mstrLevelcode, "", -1, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist(mintApprovalSetting, "", mstrLevelname, -1, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isExist(mintApprovalSetting, "", "", mintPriority, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try

            strQ = "INSERT INTO greapproverlevel_master ( " & _
              "  levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", approvalsettingid " & _
              ", isactive " & _
            ") VALUES ( " & _
              "  @levelcode " & _
              ", @levelname " & _
              ", @priority " & _
              ", @approvalsettingid " & _
              ", @isactive " & _
            "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApprLevelunkid = dsList.Tables(0).Rows(0).Item(0)



            If Insert_AtTranLog(objDataOperation, 1, mintApprLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception

            objDataOperation.ReleaseTransaction(False)

            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvapproverlevel_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mintApprovalSetting, mstrLevelcode, "", -1, mintApprLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist(mintApprovalSetting, "", mstrLevelname, -1, mintApprLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isExist(mintApprovalSetting, "", "", mintPriority, mintApprLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprLevelunkid)


            strQ = "UPDATE greapproverlevel_master SET " & _
              "  levelcode = @levelcode " & _
              ", levelname = @levelname " & _
              ", priority  = @priority  " & _
              ", approvalsettingid = @approvalsettingid " & _
              ", isactive = @isactive " & _
            " WHERE apprlevelunkid = @apprlevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If IsTableDataUpdate(mintApprLevelunkid, objDataOperation) = False Then
                If Insert_AtTranLog(objDataOperation, 2, mintApprLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception

            objDataOperation.ReleaseTransaction(False)

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update greapproverlevel_master set isactive = 0 " & _
            "WHERE apprlevelunkid = @apprlevelunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _ApprLevelunkid(objDataOperation) = intUnkid

            If Insert_AtTranLog(objDataOperation, 3, mintApprLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strq As String = ""
        Dim exforce As Exception
        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strq = "select isnull(apprlevelunkid ,0) from greapprover_master where apprlevelunkid =@apprlevelunkid"
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strq)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exforce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exforce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exforce = Nothing
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strApprovalSetting As Integer, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intPriority As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  apprlevelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", approvalsettingid " & _
              ", isactive " & _
             "FROM greapproverlevel_master " & _
             "WHERE isactive = 1 "

            If strCode.Length > 0 Then
                strQ &= " AND levelcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND levelname = @name "
            End If
            If intPriority > -1 Then
                strQ &= " AND priority = @priority "
            End If

            If intUnkid > 0 Then
                strQ &= " AND apprlevelunkid <> @apprlevelunkid"
            End If

            strQ &= " AND approvalsettingid = @approvalsettingid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal intApprovalSettingId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as apprlevelunkid, ' ' +  @name  as name, 0 AS priority UNION "
            End If
            strQ &= "SELECT apprlevelunkid, levelname as name, priority AS priority " & _
                    "FROM greapproverlevel_master " & _
                    "WHERE isactive = 1 and approvalsettingid = @approvalsettingid ORDER BY priority "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovalSettingId)

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer, ByVal intUnkid As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO atgreapproverlevel_master ( " & _
             "  row_guid " & _
             ", apprlevelunkid " & _
             ", levelcode " & _
             ", levelname " & _
             ", priority " & _
             ", approvalSettingid " & _
             ", auditdatetime " & _
             ", audittype " & _
             ", audituserunkid " & _
             ", loginemployeeunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
             "  @row_guid " & _
             ", @apprlevelunkid " & _
             ", @levelcode " & _
             ", @levelname " & _
             ", @priority " & _
             ", @approvalSettingid " & _
             ", GETDATE() " & _
             ", @audittype " & _
             ", @audituserunkid " & _
             ", @loginemployeeunkid " & _
             ", @ip " & _
             ", @host " & _
             ", @form_name " & _
             ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDoOps.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprLevelunkid)
            objDoOps.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLevelcode)
            objDoOps.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLevelname)
            objDoOps.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDoOps.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If


            strQ = "select TOP 1 * from atgreapproverlevel_master where apprlevelunkid = @apprlevelunkid and approvalsettingid = @approvalsettingid and audittype = 2 ORDER BY auditdatetime DESC"
            objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode)
            'objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname)
            'objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintPriority)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("levelcode").ToString() = mstrLevelcode AndAlso dr("levelname").ToString() = mstrLevelname AndAlso _
                   dr("priority").ToString() = mintPriority AndAlso dr("approvalSettingid").ToString() = mintApprovalSetting Then
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
			Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
			Language.setMessage(mstrModuleName, 4, "Based On User Access")
			Language.setMessage(mstrModuleName, 5, "Approver And Employee Mapping")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

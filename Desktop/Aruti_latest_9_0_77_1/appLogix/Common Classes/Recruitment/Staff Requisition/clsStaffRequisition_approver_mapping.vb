﻿'************************************************************************************************************************************
'Class Name : clsStaffRequisition_approver_mapping.vb
'Purpose    :
'Date       :19-May-2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsStaffRequisition_approver_mapping

    Private Shared ReadOnly mstrModuleName As String = "clsStaffRequisition_approver_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintStaffRequisitionapproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mintUserapproverunkid As Integer
    Private mintAllocationId As Integer
    Private mintAllocationUnkId As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrAllocationUnkIDs As String = String.Empty
    Private mdtTable As DataTable     'Hemant (03 Sep 2019)
#End Region

#Region " Properties "
'S.SANDEEP [28-May-2018] -- START 
'ISSUE/ENHANCEMENT : {Audit Trails} 
Private mstrFormName As String = String.Empty
Public WriteOnly Property _FormName() As String 
Set(ByVal value As String) 
mstrFormName = value 
End Set 
End Property 
 
Private mstrClientIP As String = ""
Public WriteOnly Property _ClientIP() As String 
Set(ByVal value As String) 
mstrClientIP = value 
End Set 
End Property 
 
Private mstrHostName As String = ""
Public WriteOnly Property _HostName() As String 
Set(ByVal value As String) 
mstrHostName = value 
End Set 
End Property 
 
Private mblnIsWeb As Boolean = False
Public WriteOnly Property _FromWeb() As Boolean 
Set(ByVal value As Boolean) 
mblnIsWeb = value 
End Set 
End Property 
 
    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
 
Private mdtAuditDate As DateTime = Now
Public WriteOnly Property _AuditDate() As DateTime 
Set(ByVal value As DateTime) 
mdtAuditDate = value 
End Set 
End Property 
 
Private xDataOpr As clsDataOperation = Nothing
Public Property _xDataOpr() As clsDataOperation 
Get 
Return xDataOpr 
End Get 
Set(ByVal value As clsDataOperation) 
xDataOpr = value 
End Set 
End Property 
 
Private mintLoginEmployeeunkid As Integer = 0
Public Property _LoginEmployeeunkid() As Integer 
Get 
Return mintLoginEmployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintLoginEmployeeunkid = value 
End Set 
End Property 
 
Private mintVoidlogingemployeeunkid As Integer=0
Public Property _Voidloginemployeeunkid() As Integer 
Get 
Return mintVoidlogingemployeeunkid 
End Get 
Set(ByVal value As Integer) 
mintVoidlogingemployeeunkid = value 
End Set 
End Property 
'S.SANDEEP [28-May-2018] -- END 

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set StaffRequisitionapproverunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _StaffRequisitionapproverunkid() As Integer
        Get
            Return mintStaffRequisitionapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintStaffRequisitionapproverunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userapproverunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userapproverunkid() As Integer
        Get
            Return mintUserapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintUserapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AllocationId
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _AllocationId() As Integer
        Get
            Return mintAllocationId
        End Get
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AllocationUnkId
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _AllocationUnkId() As Integer
        Get
            Return mintAllocationUnkId
        End Get
        Set(ByVal value As Integer)
            mintAllocationUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _AllocationUnkIDs() As String
        Set(ByVal value As String)
            mstrAllocationUnkIDs = value
        End Set
    End Property

    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property
    'Hemant (03 Sep 2019) -- End
#End Region

#Region " Constructor "
    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
    Public Sub New()
        mdtTable = New DataTable("StaffRequisitionApprover")

        Try
            mdtTable.Columns.Add("staffrequisitionapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("levelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("userapproverunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("allocationid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("allocationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
    'Hemant (03 Sep 2019) -- End
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  staffrequisitionapproverunkid " & _
                  ", levelunkid " & _
                  ", userapproverunkid " & _
                  ", allocationid " & _
                  ", allocationunkid " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason " & _
             "FROM rcstaffrequisition_approver_mapping " & _
             "WHERE staffrequisitionapproverunkid = @staffrequisitionapproverunkid "

            objDataOperation.AddParameter("@staffrequisitionapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffRequisitionapproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStaffRequisitionapproverunkid = CInt(dtRow.Item("staffrequisitionapproverunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintUserapproverunkid = CInt(dtRow.Item("userapproverunkid"))
                mintAllocationId = CInt(dtRow.Item("allocationid"))
                mintAllocationUnkId = CInt(dtRow.Item("allocationunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal intStaffReqById As Integer = 0 _
                            , Optional ByVal intAllocationUnkId As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal strOrderBy As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  rcstaffrequisition_approver_mapping.staffrequisitionapproverunkid " & _
                          ", rcstaffrequisition_approver_mapping.levelunkid " & _
                          ", rcstaffrequisitionlevel_master.levelcode " & _
                          ", rcstaffrequisitionlevel_master.levelname AS approver_level " & _
                          ", rcstaffrequisitionlevel_master.levelname1 " & _
                          ", rcstaffrequisitionlevel_master.levelname2 " & _
                          ", rcstaffrequisitionlevel_master.priority " & _
                          ", rcstaffrequisition_approver_mapping.userapproverunkid " & _
                          ", rcstaffrequisition_approver_mapping.allocationid " & _
                          ", rcstaffrequisition_approver_mapping.allocationunkid " & _
                          ", ISNULL(hrstation_master.name, '') AS Branch " & _
                          ", ISNULL(hrdepartment_group_master.name, '') AS DepartmentGroup " & _
                          ", ISNULL(hrdepartment_master.name, '') AS Department " & _
                          ", ISNULL(hrsectiongroup_master.name, '') AS SectionGroup " & _
                          ", ISNULL(hrsection_master.name, '') AS Section " & _
                          ", ISNULL(hrunitgroup_master.name, '') AS UnitGroup " & _
                          ", ISNULL(hrunit_master.name, '') AS Unit " & _
                          ", ISNULL(hrteam_master.name, '') AS Team " & _
                          ", ISNULL(hrjobgroup_master.name, '') AS JobGroup " & _
                          ", ISNULL(hrjob_master.job_name, '') AS Job " & _
                          ", ISNULL(hrclassgroup_master.name, '') AS ClassGroup " & _
                          ", ISNULL(hrclasses_master.name, '') AS Class " & _
                          ", rcstaffrequisition_approver_mapping.userunkid " & _
                          ", hrmsConfiguration..cfuser_master.username AS approver " & _
                          ", rcstaffrequisition_approver_mapping.isvoid " & _
                          ", rcstaffrequisition_approver_mapping.voiduserunkid " & _
                          ", rcstaffrequisition_approver_mapping.voiddatetime " & _
                          ", rcstaffrequisition_approver_mapping.voidreason " & _
                    "FROM    rcstaffrequisition_approver_mapping " & _
                            " JOIN hrmsConfiguration..cfuser_master ON rcstaffrequisition_approver_mapping.userapproverunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                            " LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                            " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                            " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                            " LEFT JOIN hrstation_master on hrstation_master.stationunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                            " LEFT JOIN hrdepartment_group_master on hrdepartment_group_master.deptgroupunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                            " LEFT JOIN hrsection_master on hrsection_master.sectionunkid = rcstaffrequisition_approver_mapping.allocationunkid  " & _
                            " LEFT JOIN hrunit_master on hrunit_master.unitunkid =rcstaffrequisition_approver_mapping.allocationunkid   " & _
                            " LEFT JOIN hrjobgroup_master on hrjobgroup_master.jobgroupunkid = rcstaffrequisition_approver_mapping.allocationunkid  " & _
                            " LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = rcstaffrequisition_approver_mapping.allocationunkid  " & _
                            " LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = rcstaffrequisition_approver_mapping.allocationunkid   " & _
                            " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = rcstaffrequisition_approver_mapping.allocationunkid " & _
                            " LEFT JOIN hrunitgroup_master ON rcstaffrequisition_approver_mapping.allocationunkid = hrunitgroup_master.unitgroupunkid " & _
                            " LEFT JOIN hrsectiongroup_master ON rcstaffrequisition_approver_mapping.allocationunkid = hrsectiongroup_master.sectiongroupunkid " & _
                    "WHERE   ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                            "AND rcstaffrequisitionlevel_master.isactive = 1 "

            If intApproverUserUnkId > 0 Then
                strQ &= " AND userapproverunkid = @userapproverunkid "
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            End If

            If intStaffReqById > 0 Then
                strQ &= " AND allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcstaffrequisition_approver_mapping) </purpose>
    Public Function Insert(Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintUserapproverunkid, mintAllocationId, mintAllocationUnkId, , objDataOp) Then
            'Hemant (03 Sep 2019) -- [objDataOp]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()   'Hemant (03 Sep 2019)
        End If


        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO rcstaffrequisition_approver_mapping ( " & _
              "  levelunkid " & _
              ", userapproverunkid " & _
              ", allocationid " & _
              ", allocationunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @levelunkid " & _
              ", @userapproverunkid " & _
              ", @allocationid " & _
              ", @allocationunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStaffRequisitionapproverunkid = dsList.Tables(0).Rows(0).Item(0)
'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
Dim objCommonATLog AS New clsCommonATLog 
objCommonATLog._FormName = mstrFormName 
objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid 
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 1, "rcstaffrequisition_approver_mapping", "staffrequisitionapproverunkid", mintStaffRequisitionapproverunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True

        Catch ex As Exception
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            'objDataOperation.ReleaseTransaction(False)
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (03 Sep 2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (03 Sep 2019) -- End
        End Try
    End Function

    Public Function InsertAll() As Boolean
        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim arrID As String()

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            arrID = mstrAllocationUnkIDs.Split(",")

            For i As Integer = 0 To arrID.Length - 1
                mintAllocationUnkId = CInt(arrID(i))

                If Insert(objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcstaffrequisition_approver_mapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintUserapproverunkid, mintAllocationId, mintAllocationUnkId, mintStaffRequisitionapproverunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@staffrequisitionapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStaffRequisitionapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserapproverunkid.ToString)
            objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationId.ToString)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcstaffrequisition_approver_mapping SET " & _
              "  levelunkid = @levelunkid" & _
              ", userapproverunkid = @userapproverunkid" & _
              ", allocationid = @allocationid " & _
              ", allocationunkid = @allocationunkid " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE staffrequisitionapproverunkid = @staffrequisitionapproverunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.IsTableDataUpdate("atcommon_log", "rcstaffrequisition_approver_mapping", mintStaffRequisitionapproverunkid, "staffrequisitionapproverunkid", 2) Then
                If objCommonATLog.Insert_AtLog(objDataOperation, 2, "rcstaffrequisition_approver_mapping", "levelunkid", mintLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommonATLog = Nothing
            'S.SANDEEP [28-May-2018] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcstaffrequisition_approver_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (03 Sep 2019) -- [objDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (03 Sep 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If
        'Hemant (03 Sep 2019) -- End

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE rcstaffrequisition_approver_mapping SET " & _
                   "  isvoid = @isvoid" & _
                   " ,voiduserunkid = @voiduserunkid" & _
                   " ,voiddatetime = @voiddatetime" & _
                   " ,voidreason = @voidreason " & _
                   "WHERE staffrequisitionapproverunkid = @staffrequisitionapproverunkid "

            objDataOperation.AddParameter("@staffrequisitionapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            Dim objCommonATLog As New clsCommonATLog
            objCommonATLog._FormName = mstrFormName
            objCommonATLog._LoginEmployeeUnkid = mintLoginEmployeeunkid
            objCommonATLog._ClientIP = mstrClientIP
            objCommonATLog._HostName = mstrHostName
            objCommonATLog._FromWeb = mblnIsWeb
            objCommonATLog._AuditUserId = mintAuditUserId
objCommonATLog._CompanyUnkid = mintCompanyUnkid
            objCommonATLog._AuditDate = mdtAuditDate
            'S.SANDEEP [28-May-2018] -- END

            If objCommonATLog.Insert_AtLog(objDataOperation, 3, "rcstaffrequisition_approver_mapping", "staffrequisitionapproverunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            'objDataOperation.ReleaseTransaction(True)
            If objDataOp Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Hemant (03 Sep 2019) -- End           

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False) 'Hemant (03 Sep 2019)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (03 Sep 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intApproverUserUnkID As Integer, _
                           Optional ByVal intStaffReqById As Integer = 0, _
                           Optional ByVal intAllocationUnkId As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT rcstaffrequisition_approval_tran.staffrequisitiontranunkid " & _
                    "FROM   rcstaffrequisition_approval_tran " & _
                    "LEFT JOIN rcstaffrequisition_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                    "WHERE ISNULL(rcstaffrequisition_approval_tran.isvoid, 0) = 0 " & _
                    "AND ISNULL(rcstaffrequisition_tran.isvoid, 0) = 0 " & _
                    "AND rcstaffrequisition_approval_tran.userunkid = @userunkid "

            If intStaffReqById > 0 Then
                strQ &= " AND rcstaffrequisition_tran.staffrequisitionbyid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_tran.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApproverUserId As Integer, ByVal intAllocationID As Integer, ByVal intAllocationUnkId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (03 Sep 2019) -- [objDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (03 Sep 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
        'objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        Else
        objDataOperation = New clsDataOperation
        End If
        'Hemant (03 Sep 2019) -- End

        Try
            strQ = "SELECT " & _
                   "  staffrequisitionapproverunkid " & _
                   " ,levelunkid " & _
                   " ,userapproverunkid " & _
                   ", allocationid " & _
                   ", allocationunkid " & _
                   " ,userunkid " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   "FROM rcstaffrequisition_approver_mapping " & _
                   "WHERE ISNULL(isvoid, 0) = 0 " & _
                   " AND userapproverunkid = @userapproverunkid " & _
                   " AND allocationid = @allocationid " & _
                   " AND allocationunkid = @allocationunkid "

            If intUnkid > 0 Then
                strQ &= " AND staffrequisitionapproverunkid <> @staffrequisitionapproverunkid"
            End If

            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intApproverUserId)
            objDataOperation.AddParameter("@allocationid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAllocationID)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intAllocationUnkId)
            objDataOperation.AddParameter("@staffrequisitionapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
            'objDataOperation = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (03 Sep 2019) -- End
        End Try
    End Function

    Public Function GetApproverUnkIDs(Optional ByVal intStaffReqById As Integer = 0 _
                                      , Optional ByVal intAllocationUnkId As Integer = 0 _
                                      , Optional ByVal intPririty As Integer = -1 _
                                      , Optional ByVal intLevelUnkId As Integer = 0 _
                                      , Optional ByVal strLevelCode As String = "" _
                                      ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     rcstaffrequisition_approver_mapping " & _
                                                    "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 "

            If intStaffReqById > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            If intPririty > -1 Then
                strQ &= " AND rcstaffrequisitionlevel_master.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPririty)
            End If

            If intLevelUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.levelunkid  = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkId)
            End If

            If strLevelCode.Trim <> "" Then
                strQ &= " AND rcstaffrequisitionlevel_master.levelcode = @levelcode "
                objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelCode)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetPublishStaffRequisitionUserUnkIDs() As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            '*** 354 = AllowtoAddVacancy

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userunkid AS NVARCHAR(50)) " & _
                                           "FROM     hrmsConfiguration..cfuser_privilege " & _
                                           "WHERE    privilegeunkid = 354 " & _
                                         "FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPublishStaffRequisitionUserUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetLowerApproverUnkIDs(ByVal intCurrLevelPririty As Integer _
                                           , Optional ByVal intStaffReqById As Integer = 0 _
                                           , Optional ByVal intAllocationUnkId As Integer = 0 _
                                           ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     rcstaffrequisition_approver_mapping " & _
                                                    "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 " & _
                                                    "AND rcstaffrequisitionlevel_master.priority < @priority "

            If intStaffReqById > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPririty)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLowerApproverUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetCurrentLevelPriority(ByVal intCurrLevelPririty As Integer _
                                            , Optional ByVal intStaffReqById As Integer = 0 _
                                            , Optional ByVal intAllocationUnkId As Integer = 0) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     rcstaffrequisition_approver_mapping " & _
                                                    "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 " & _
                                                    "AND rcstaffrequisitionlevel_master.priority = @priority "

            If intStaffReqById > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPririty)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentLevelPriority; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetCancelStaffRequisitionUserUnkIDs(Optional ByVal intStaffReqById As Integer = 0 _
                                                        , Optional ByVal intAllocationUnkId As Integer = 0 _
                                                        , Optional ByVal intExcludeUserUnkId As Integer = 0 _
                                                        ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     rcstaffrequisition_approver_mapping " & _
                                                    "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisitionlevel_master.levelunkid = rcstaffrequisition_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(rcstaffrequisitionlevel_master.isactive, 1) = 1 "

            If intStaffReqById > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationid = @allocationid "
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStaffReqById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            If intExcludeUserUnkId > 0 Then
                strQ &= " AND rcstaffrequisition_approver_mapping.userapproverunkid <> @userapproverunkid "
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExcludeUserUnkId)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0)("ApproverIDs")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCancelStaffRequisitionUserUnkIDs; Module Name: " & mstrModuleName)
        End Try
        Return ""
    End Function

    'Hemant (02 Jul 2019) -- Start
    'ENHANCEMENT :  ZRA minimum Requirements
    Public Function GetOnlyMappedAllocatonList(ByVal strTableName As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim dsAllocation As DataSet
        Dim objMaster As New clsMasterData
        dsAllocation = objMaster.GetEAllocation_Notification("Allocation")
        Dim dicAllocation As Dictionary(Of Integer, String) = (From p In dsAllocation.Tables("Allocation") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT DISTINCT	allocationid as Id "
            strQ &= ", CASE allocationid "
            For Each pair In dicAllocation
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " WHEN 0  THEN 'Employee' "
            strQ &= " END AS Name "
            strQ &= "FROM    rcstaffrequisition_approver_mapping " & _
                           "WHERE   ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocatonMappedList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetLastMappedAllocatonRow(ByVal strTableName As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT TOP 1 * "
            strQ &= "FROM    rcstaffrequisition_approver_mapping " & _
                           "WHERE   ISNULL(rcstaffrequisition_approver_mapping.isvoid, 0) = 0 " & _
                           " ORDER BY 1 DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastMappedAllocatonRow; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Hemant (02 Jul 2019) -- End


    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 1 : On Staff requisition approver mapping screen, provide option to edit the approvers. Currently, you can only delete and create afresh.)
    Public Function InsertUpdateDelete(ByVal mdtTable As DataTable, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try
            If mdtTable IsNot Nothing Then
                For Each dtRow As DataRow In mdtTable.Rows
                    mintStaffRequisitionapproverunkid = CInt(dtRow.Item("StaffRequisitionapproverunkid"))
                    mintLevelunkid = CInt(dtRow.Item("Levelunkid"))
                    mintUserapproverunkid = CInt(dtRow.Item("Userapproverunkid"))
                    mintAllocationId = CInt(dtRow.Item("AllocationId"))
                    mintAllocationUnkId = CInt(dtRow.Item("AllocationUnkId"))
                    mintUserunkid = CInt(dtRow.Item("userunkid"))
                    mblnIsvoid = CBool(dtRow.Item("isvoid"))
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                        mdtVoiddatetime = dtRow.Item("voiddatetime")
                    Else
                        mdtVoiddatetime = Nothing
                    End If
                    mstrVoidreason = dtRow.Item("voidreason").ToString

                    Select Case dtRow.Item("AUD").ToString.ToUpper
                        Case "A"
                            If Insert(objDataOperation) = False Then
                                Return False
                            End If

                        Case "D"
                            If Delete(mintStaffRequisitionapproverunkid, objDataOperation) = False Then
                                Return False
                            End If

                    End Select
                Next
            End If

            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Hemant (03 Sep 2019) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This approver is already mapped with some level.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

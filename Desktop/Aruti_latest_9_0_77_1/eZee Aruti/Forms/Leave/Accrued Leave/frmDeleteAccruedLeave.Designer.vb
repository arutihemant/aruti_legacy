﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeleteAccruedLeave
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeleteAccruedLeave))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objLine2 = New eZee.Common.eZeeLine
        Me.pnlDeleteByBatch = New System.Windows.Forms.Panel
        Me.cboBatchNo = New System.Windows.Forms.ComboBox
        Me.lblBatchNo = New System.Windows.Forms.Label
        Me.pnlDeleteByLeave = New System.Windows.Forms.Panel
        Me.cboLeaveCode = New System.Windows.Forms.ComboBox
        Me.lblLeaveCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.pnlDeleteByBatch.SuspendLayout()
        Me.pnlDeleteByLeave.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objLine2)
        Me.pnlMainInfo.Controls.Add(Me.pnlDeleteByBatch)
        Me.pnlMainInfo.Controls.Add(Me.pnlDeleteByLeave)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(288, 135)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objLine2
        '
        Me.objLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine2.Location = New System.Drawing.Point(13, 36)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(264, 13)
        Me.objLine2.TabIndex = 6
        Me.objLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlDeleteByBatch
        '
        Me.pnlDeleteByBatch.Controls.Add(Me.cboBatchNo)
        Me.pnlDeleteByBatch.Controls.Add(Me.lblBatchNo)
        Me.pnlDeleteByBatch.Location = New System.Drawing.Point(13, 51)
        Me.pnlDeleteByBatch.Name = "pnlDeleteByBatch"
        Me.pnlDeleteByBatch.Size = New System.Drawing.Size(263, 21)
        Me.pnlDeleteByBatch.TabIndex = 4
        '
        'cboBatchNo
        '
        Me.cboBatchNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatchNo.FormattingEnabled = True
        Me.cboBatchNo.Items.AddRange(New Object() {"Maternitiy Leave", "Annual Leave", "Sick Leave"})
        Me.cboBatchNo.Location = New System.Drawing.Point(85, 0)
        Me.cboBatchNo.Name = "cboBatchNo"
        Me.cboBatchNo.Size = New System.Drawing.Size(176, 21)
        Me.cboBatchNo.TabIndex = 3
        '
        'lblBatchNo
        '
        Me.lblBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchNo.Location = New System.Drawing.Point(3, 3)
        Me.lblBatchNo.Name = "lblBatchNo"
        Me.lblBatchNo.Size = New System.Drawing.Size(76, 15)
        Me.lblBatchNo.TabIndex = 271
        Me.lblBatchNo.Text = "Batch No."
        '
        'pnlDeleteByLeave
        '
        Me.pnlDeleteByLeave.Controls.Add(Me.cboLeaveCode)
        Me.pnlDeleteByLeave.Controls.Add(Me.lblLeaveCode)
        Me.pnlDeleteByLeave.Location = New System.Drawing.Point(13, 12)
        Me.pnlDeleteByLeave.Name = "pnlDeleteByLeave"
        Me.pnlDeleteByLeave.Size = New System.Drawing.Size(263, 21)
        Me.pnlDeleteByLeave.TabIndex = 4
        '
        'cboLeaveCode
        '
        Me.cboLeaveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveCode.FormattingEnabled = True
        Me.cboLeaveCode.Items.AddRange(New Object() {"Maternitiy Leave", "Annual Leave", "Sick Leave"})
        Me.cboLeaveCode.Location = New System.Drawing.Point(85, 0)
        Me.cboLeaveCode.Name = "cboLeaveCode"
        Me.cboLeaveCode.Size = New System.Drawing.Size(176, 21)
        Me.cboLeaveCode.TabIndex = 2
        '
        'lblLeaveCode
        '
        Me.lblLeaveCode.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveCode.Location = New System.Drawing.Point(3, 3)
        Me.lblLeaveCode.Name = "lblLeaveCode"
        Me.lblLeaveCode.Size = New System.Drawing.Size(78, 15)
        Me.lblLeaveCode.TabIndex = 230
        Me.lblLeaveCode.Text = "Leave Code"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 80)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(288, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(90, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 4
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(186, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmDeleteAccruedLeave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(288, 135)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDeleteAccruedLeave"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlDeleteByBatch.ResumeLayout(False)
        Me.pnlDeleteByLeave.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents pnlDeleteByBatch As System.Windows.Forms.Panel
    Friend WithEvents pnlDeleteByLeave As System.Windows.Forms.Panel
    Friend WithEvents cboLeaveCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveCode As System.Windows.Forms.Label
    Friend WithEvents objLine2 As eZee.Common.eZeeLine
    Friend WithEvents lblBatchNo As System.Windows.Forms.Label
    Friend WithEvents cboBatchNo As System.Windows.Forms.ComboBox
End Class

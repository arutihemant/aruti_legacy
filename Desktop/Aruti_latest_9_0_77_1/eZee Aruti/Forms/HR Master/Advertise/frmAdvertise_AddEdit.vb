﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

'Last Message Index = 4

Public Class frmAdvertise_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAdvertise_AddEdit"
    Private mblnCancel As Boolean = True
    Private objAdvertisemaster As clsAdvertise_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAdvertiseMasterUnkid As Integer = -1
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strEmailExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'Dim strWebsiteExpression As String = "^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$"
    'Dim strWebsiteExpression As String = "(http|ftp|https)://([\w-]+\.)+(/[\w- ./?%&=]*)?"


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintAdvertiseMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintAdvertiseMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmAdvertise_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAdvertisemaster = New clsAdvertise_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objAdvertisemaster._Advertiseunkid = mintAdvertiseMasterUnkid
            End If
            FillAdvertiseCategory()
            FillCombo()
            GetValue()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvertise_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertise_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvertise_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertise_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdvertise_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAdvertise_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAdvertisemaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAdvertise_master.SetMessages()
            objfrm._Other_ModuleNames = "clsAdvertise_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        Try
            FillZipcode()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCity_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'S.SANDEEP [ 11 MAR 2014 ] -- START
        'Dim EmailExpression As New RegularExpressions.Regex(strEmailExpression)
        Dim EmailExpression As New RegularExpressions.Regex(iEmailRegxExpression)
        'S.SANDEEP [ 11 MAR 2014 ] -- END


        'Dim WebSiteExpression As New RegularExpressions.Regex(strWebsiteExpression)
        Try
            If CInt(cboCategory.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Advertise Category is compulsory information. Please Select Advertise Category."), enMsgBoxStyle.Information)
                cboCategory.Focus()
                Exit Sub
            ElseIf Trim(txtCompany.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Advertise Company Name cannot be blank. Advertise Company Name is required information."), enMsgBoxStyle.Information)
                txtCompany.Focus()
                Exit Sub
                'ElseIf CInt(cboCountry.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Country is compulsory information.Please Select Country."), enMsgBoxStyle.Information)
                '    cboCountry.Select()
                '    Exit Sub
                'ElseIf CInt(cboState.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "State is compulsory information.Please Select State."), enMsgBoxStyle.Information)
                '    cboState.Select()
                '    Exit Sub
                'ElseIf CInt(cboCity.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "City is compulsory information.Please Select City."), enMsgBoxStyle.Information)
                '    cboCity.Select()
                '    Exit Sub
                'ElseIf CInt(cboZipcode.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Pincode is compulsory information.Please Select Pincode."), enMsgBoxStyle.Information)
                '    cboZipcode.Select()
                '    Exit Sub
            ElseIf txtEmail.Text.Trim.Length > 0 Then
                If EmailExpression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email."), enMsgBoxStyle.Information)
                    txtEmail.Focus()
                    Exit Sub
                End If
            ElseIf txtwebsite.Text.Trim.Length > 0 Then
                'Dim str As String = "http://" & txtwebsite.Text.Trim
                'If WebSiteExpression.IsMatch(str) = False Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Website.Please Enter Valid Website"), enMsgBoxStyle.Information)
                '    txtwebsite.Focus()
                '    Exit Sub
                'End If
            End If
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAdvertisemaster._FormName = mstrModuleName
            objAdvertisemaster._LoginEmployeeunkid = 0
            objAdvertisemaster._ClientIP = getIP()
            objAdvertisemaster._HostName = getHostName()
            objAdvertisemaster._FromWeb = False
            objAdvertisemaster._AuditUserId = User._Object._Userunkid
objAdvertisemaster._CompanyUnkid = Company._Object._Companyunkid
            objAdvertisemaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAdvertisemaster.Update()
            Else
                blnFlag = objAdvertisemaster.Insert()
            End If

            If blnFlag = False And objAdvertisemaster._Message <> "" Then
                eZeeMsgBox.Show(objAdvertisemaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAdvertisemaster = Nothing
                    objAdvertisemaster = New clsAdvertise_master
                    Call GetValue()
                    cboCategory.Select()
                Else
                    mintAdvertiseMasterUnkid = objAdvertisemaster._Advertiseunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCategory.Click
        Try
            Dim objfrmCommonMaster As New frmCommonMaster

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonMaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            objfrmCommonMaster.displayDialog(-1, clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, enAction.ADD_ONE)
            FillAdvertiseCategory()
            cboCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboCategory.BackColor = GUI.ColorComp
            txtCompany.BackColor = GUI.ColorComp
            txtaddress1.BackColor = GUI.ColorOptional
            txtaddress2.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            cboZipcode.BackColor = GUI.ColorOptional
            txtPhone.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtwebsite.BackColor = GUI.ColorOptional
            txtcontactno.BackColor = GUI.ColorOptional
            txtcontactperson.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCategory.SelectedValue = objAdvertisemaster._Categoryunkid
            txtCompany.Text = objAdvertisemaster._Company
            txtaddress1.Text = objAdvertisemaster._Address1
            txtaddress2.Text = objAdvertisemaster._Address2
            cboCountry.SelectedValue = objAdvertisemaster._Countryunkid
            cboState.SelectedValue = objAdvertisemaster._Stateunkid
            cboCity.SelectedValue = objAdvertisemaster._Cityunkid
            cboZipcode.SelectedValue = objAdvertisemaster._Pincodeunkid
            txtPhone.Text = objAdvertisemaster._Phone
            txtFax.Text = objAdvertisemaster._Fax
            txtEmail.Text = objAdvertisemaster._Email
            txtwebsite.Text = objAdvertisemaster._Website
            txtcontactno.Text = objAdvertisemaster._Contactno
            txtcontactperson.Text = objAdvertisemaster._Contactperson
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAdvertisemaster._Categoryunkid = CInt(cboCategory.SelectedValue)
            objAdvertisemaster._Company = txtCompany.Text.Trim
            objAdvertisemaster._Address1 = txtaddress1.Text.Trim
            objAdvertisemaster._Address2 = txtaddress2.Text.Trim
            objAdvertisemaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objAdvertisemaster._Stateunkid = CInt(cboState.SelectedValue)
            objAdvertisemaster._Cityunkid = CInt(cboCity.SelectedValue)
            objAdvertisemaster._Pincodeunkid = CInt(cboZipcode.SelectedValue)
            objAdvertisemaster._Phone = txtPhone.Text.Trim
            objAdvertisemaster._Fax = txtFax.Text.Trim
            objAdvertisemaster._Email = txtEmail.Text.Trim
            objAdvertisemaster._Website = txtwebsite.Text.Trim
            objAdvertisemaster._Contactno = txtcontactno.Text.Trim
            objAdvertisemaster._Contactperson = txtcontactperson.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillAdvertiseCategory()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsAdvertiseCategory As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "Advertise")
            cboCategory.ValueMember = "masterunkid"
            cboCategory.DisplayMember = "name"
            cboCategory.DataSource = dsAdvertiseCategory.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAdvertiseCategory", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    Private Sub FillZipcode()
        Dim dsZipcode As DataSet = Nothing
        Try
            Dim objZipcodemaster As New clszipcode_master
            If CInt(cboCity.SelectedValue) > 0 Then
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, False, CInt(cboCity.SelectedValue))
            Else
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, True)
            End If
            cboZipcode.ValueMember = "zipcodeunkid"
            cboZipcode.DisplayMember = "zipcode_no"
            cboZipcode.DataSource = dsZipcode.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillZipcode", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddCategory.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSkill.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSkill.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSkill.Text = Language._Object.getCaption(Me.gbSkill.Name, Me.gbSkill.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblwebsite.Text = Language._Object.getCaption(Me.lblwebsite.Name, Me.lblwebsite.Text)
			Me.lblcontactperson.Text = Language._Object.getCaption(Me.lblcontactperson.Name, Me.lblcontactperson.Text)
			Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.Name, Me.lblContactNo.Text)
			Me.lblPincode.Text = Language._Object.getCaption(Me.lblPincode.Name, Me.lblPincode.Text)
			Me.lblstate.Text = Language._Object.getCaption(Me.lblstate.Name, Me.lblstate.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblPhone.Text = Language._Object.getCaption(Me.lblPhone.Name, Me.lblPhone.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Advertise Category is compulsory information. Please Select Advertise Category.")
			Language.setMessage(mstrModuleName, 2, "Advertise Company Name cannot be blank. Advertise Company Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
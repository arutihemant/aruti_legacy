﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQualificationCourse_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQualificationCourse_AddEdit))
        Me.pnlQualification = New System.Windows.Forms.Panel
        Me.gbSkill = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.ChkSyncRecruitment = New System.Windows.Forms.CheckBox
        Me.objbtnAddResultGrp = New eZee.Common.eZeeGradientButton
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGrp = New System.Windows.Forms.Label
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeHeader1 = New eZee.Common.eZeeHeader
        Me.pnlQualification.SuspendLayout()
        Me.gbSkill.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlQualification
        '
        Me.pnlQualification.Controls.Add(Me.gbSkill)
        Me.pnlQualification.Controls.Add(Me.objFooter)
        Me.pnlQualification.Controls.Add(Me.EZeeHeader1)
        Me.pnlQualification.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlQualification.Location = New System.Drawing.Point(0, 0)
        Me.pnlQualification.Name = "pnlQualification"
        Me.pnlQualification.Size = New System.Drawing.Size(434, 340)
        Me.pnlQualification.TabIndex = 2
        '
        'gbSkill
        '
        Me.gbSkill.BorderColor = System.Drawing.Color.Black
        Me.gbSkill.Checked = False
        Me.gbSkill.CollapseAllExceptThis = False
        Me.gbSkill.CollapsedHoverImage = Nothing
        Me.gbSkill.CollapsedNormalImage = Nothing
        Me.gbSkill.CollapsedPressedImage = Nothing
        Me.gbSkill.CollapseOnLoad = False
        Me.gbSkill.Controls.Add(Me.ChkSyncRecruitment)
        Me.gbSkill.Controls.Add(Me.objbtnAddResultGrp)
        Me.gbSkill.Controls.Add(Me.cboResultGroup)
        Me.gbSkill.Controls.Add(Me.lblResultGrp)
        Me.gbSkill.Controls.Add(Me.objbtnAddGroup)
        Me.gbSkill.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbSkill.Controls.Add(Me.txtDescription)
        Me.gbSkill.Controls.Add(Me.lblDescription)
        Me.gbSkill.Controls.Add(Me.txtName)
        Me.gbSkill.Controls.Add(Me.lblName)
        Me.gbSkill.Controls.Add(Me.txtCode)
        Me.gbSkill.Controls.Add(Me.lblCode)
        Me.gbSkill.Controls.Add(Me.cboGroup)
        Me.gbSkill.Controls.Add(Me.lblGroup)
        Me.gbSkill.ExpandedHoverImage = Nothing
        Me.gbSkill.ExpandedNormalImage = Nothing
        Me.gbSkill.ExpandedPressedImage = Nothing
        Me.gbSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkill.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkill.HeaderHeight = 25
        Me.gbSkill.HeaderMessage = ""
        Me.gbSkill.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkill.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkill.HeightOnCollapse = 0
        Me.gbSkill.LeftTextSpace = 0
        Me.gbSkill.Location = New System.Drawing.Point(12, 67)
        Me.gbSkill.Name = "gbSkill"
        Me.gbSkill.OpenHeight = 300
        Me.gbSkill.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkill.ShowBorder = True
        Me.gbSkill.ShowCheckBox = False
        Me.gbSkill.ShowCollapseButton = False
        Me.gbSkill.ShowDefaultBorderColor = True
        Me.gbSkill.ShowDownButton = False
        Me.gbSkill.ShowHeader = True
        Me.gbSkill.Size = New System.Drawing.Size(408, 211)
        Me.gbSkill.TabIndex = 2
        Me.gbSkill.Temp = 0
        Me.gbSkill.Text = "Qualification / Course"
        Me.gbSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ChkSyncRecruitment
        '
        Me.ChkSyncRecruitment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkSyncRecruitment.Location = New System.Drawing.Point(252, 90)
        Me.ChkSyncRecruitment.Name = "ChkSyncRecruitment"
        Me.ChkSyncRecruitment.Size = New System.Drawing.Size(146, 17)
        Me.ChkSyncRecruitment.TabIndex = 4
        Me.ChkSyncRecruitment.Text = "Sync with Recruitment"
        Me.ChkSyncRecruitment.UseVisualStyleBackColor = True
        '
        'objbtnAddResultGrp
        '
        Me.objbtnAddResultGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResultGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResultGrp.BorderSelected = False
        Me.objbtnAddResultGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResultGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResultGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResultGrp.Location = New System.Drawing.Point(252, 60)
        Me.objbtnAddResultGrp.Name = "objbtnAddResultGrp"
        Me.objbtnAddResultGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResultGrp.TabIndex = 105
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(87, 60)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(159, 21)
        Me.cboResultGroup.TabIndex = 2
        '
        'lblResultGrp
        '
        Me.lblResultGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGrp.Location = New System.Drawing.Point(8, 64)
        Me.lblResultGrp.Name = "lblResultGrp"
        Me.lblResultGrp.Size = New System.Drawing.Size(73, 13)
        Me.lblResultGrp.TabIndex = 104
        Me.lblResultGrp.Text = "Result Group"
        Me.lblResultGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(252, 33)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 101
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(379, 114)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(87, 142)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(311, 60)
        Me.txtDescription.TabIndex = 6
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 144)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(73, 13)
        Me.lblDescription.TabIndex = 99
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(87, 114)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(286, 21)
        Me.txtName.TabIndex = 5
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 119)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(73, 13)
        Me.lblName.TabIndex = 97
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(87, 87)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(159, 21)
        Me.txtCode.TabIndex = 3
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 91)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(73, 13)
        Me.lblCode.TabIndex = 95
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(87, 33)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(159, 21)
        Me.cboGroup.TabIndex = 1
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(8, 37)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(73, 13)
        Me.lblGroup.TabIndex = 93
        Me.lblGroup.Text = "Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 285)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(434, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(327, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(227, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'EZeeHeader1
        '
        Me.EZeeHeader1.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader1.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader1.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader1.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader1.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader1.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader1.Icon = Nothing
        Me.EZeeHeader1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader1.Message = ""
        Me.EZeeHeader1.Name = "EZeeHeader1"
        Me.EZeeHeader1.Size = New System.Drawing.Size(434, 60)
        Me.EZeeHeader1.TabIndex = 0
        Me.EZeeHeader1.Title = "Qualification / Course Information"
        '
        'frmQualificationCourse_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(434, 340)
        Me.Controls.Add(Me.pnlQualification)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQualificationCourse_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Qualification / Course"
        Me.pnlQualification.ResumeLayout(False)
        Me.gbSkill.ResumeLayout(False)
        Me.gbSkill.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlQualification As System.Windows.Forms.Panel
    Friend WithEvents gbSkill As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeHeader1 As eZee.Common.eZeeHeader
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddResultGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGrp As System.Windows.Forms.Label
    Friend WithEvents ChkSyncRecruitment As System.Windows.Forms.CheckBox
End Class

﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 4

Public Class frmQualificationCourse_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmQualificationCourse_AddEdit"
    Private mblnCancel As Boolean = True
    Private objQualificationMaster As clsqualification_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintQualificationMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintQualificationMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintQualificationMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmQualificationCourse_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objQualificationMaster = New clsqualification_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objQualificationMaster._Qualificationunkid = mintQualificationMasterUnkid
            End If
            FillCombo()
            GetValue()
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationCourse_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationCourse_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationCourse_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationCourse_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationCourse_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationCourse_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objQualificationMaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsqualification_master.SetMessages()
            objfrm._Other_ModuleNames = "clsqualification_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Qualification Group is compulsory information.Please Select Qualification Group."), enMsgBoxStyle.Information)
                cboGroup.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Qualification Code cannot be blank. Qualification Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Qualification Name cannot be blank. Qualification Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
            ElseIf CInt(cboResultGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Result Group is compulsory information.Please Select Result Group."), enMsgBoxStyle.Information)
                cboResultGroup.Focus()
                Exit Sub
                'Pinkal (12-Oct-2011) -- End


            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objQualificationMaster._FormName = mstrModuleName
            objQualificationMaster._LoginEmployeeunkid = 0
            objQualificationMaster._ClientIP = getIP()
            objQualificationMaster._HostName = getHostName()
            objQualificationMaster._FromWeb = False
            objQualificationMaster._AuditUserId = User._Object._Userunkid
objQualificationMaster._CompanyUnkid = Company._Object._Companyunkid
            objQualificationMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objQualificationMaster.Update()
            Else
                blnFlag = objQualificationMaster.Insert()
            End If

            If blnFlag = False And objQualificationMaster._Message <> "" Then
                eZeeMsgBox.Show(objQualificationMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objQualificationMaster = Nothing
                    objQualificationMaster = New clsqualification_master
                    Call GetValue()
                    cboGroup.Select()
                Else
                    mintQualificationMasterUnkid = objQualificationMaster._Qualificationgroupunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrmLangPopup As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrmLangPopup.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmLangPopup.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrmLangPopup)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call objFrmLangPopup.displayDialog(txtName.Text, objQualificationMaster._Qualificationname1, objQualificationMaster._Qualificationname2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddResultGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddResultGrp.Click
        Try
            Dim objfrmCommonMaster As New frmCommonMaster

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonMaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            objfrmCommonMaster.displayDialog(-1, clsCommon_Master.enCommonMaster.RESULT_GROUP, enAction.ADD_ONE)
            FillCombo()
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddResultGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim objfrmCommonMaster As New frmCommonMaster

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmCommonMaster.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmCommonMaster.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmCommonMaster)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            objfrmCommonMaster.displayDialog(-1, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
            FillCombo()
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorOptional
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            cboGroup.BackColor = GUI.ColorComp
            cboResultGroup.BackColor = GUI.ColorComp
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objQualificationMaster._Qualificationcode
            txtName.Text = objQualificationMaster._Qualificationname
            cboGroup.SelectedValue = objQualificationMaster._Qualificationgroupunkid
            txtDescription.Text = objQualificationMaster._Description

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            If menAction = enAction.EDIT_ONE Then
                If objQualificationMaster._ResultGroupunkid > 0 Then
                    cboGroup.Enabled = False
                    cboResultGroup.Enabled = False
                    objbtnAddGroup.Enabled = False
                    objbtnAddResultGrp.Enabled = False
                    cboResultGroup.SelectedValue = objQualificationMaster._ResultGroupunkid
                Else
                    cboGroup_SelectedIndexChanged(New Object(), New EventArgs())
                    If CInt(cboResultGroup.SelectedValue) > 0 Then
                        cboResultGroup.Enabled = False
                    Else
                        cboResultGroup.Enabled = True
                    End If
                End If

            Else
                cboResultGroup.SelectedValue = objQualificationMaster._ResultGroupunkid
            End If

            'Pinkal (12-Oct-2011) -- End


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes
            ChkSyncRecruitment.Checked = objQualificationMaster._IsSyncWithRecruitment
            'Pinkal (22-MAY-2012) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objQualificationMaster._Qualificationcode = txtCode.Text.Trim
            objQualificationMaster._Qualificationname = txtName.Text.Trim
            objQualificationMaster._Qualificationgroupunkid = CInt(cboGroup.SelectedValue)
            objQualificationMaster._Description = txtDescription.Text.Trim


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objQualificationMaster._ResultGroupunkid = CInt(cboResultGroup.SelectedValue)
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (22-MAY-2012) -- Start
            'Enhancement : TRA Changes
            objQualificationMaster._IsSyncWithRecruitment = ChkSyncRecruitment.Checked
            'Pinkal (22-MAY-2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsQualificationCategory As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Qualiication List")
            cboGroup.ValueMember = "masterunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsQualificationCategory.Tables(0)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            Dim dsResultGroup As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGroup")
            cboResultGroup.ValueMember = "masterunkid"
            cboResultGroup.DisplayMember = "name"
            cboResultGroup.DataSource = dsResultGroup.Tables(0)
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                ChkSyncRecruitment.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)
                ChkSyncRecruitment.Checked = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes

#Region "DropDown's Event"

    Private Sub cboGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedIndexChanged
        Try
            Dim mintResultGroupunkid As Integer = objQualificationMaster.GetResultGroupFromQualifcationGroup(CInt(cboGroup.SelectedValue))
            If mintResultGroupunkid > 0 Then
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = False
                objbtnAddResultGrp.Enabled = False
            Else
                cboResultGroup.SelectedValue = mintResultGroupunkid
                cboResultGroup.Enabled = True
                objbtnAddResultGrp.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (07-Jan-2012) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSkill.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSkill.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSkill.Text = Language._Object.getCaption(Me.gbSkill.Name, Me.gbSkill.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.lblResultGrp.Text = Language._Object.getCaption(Me.lblResultGrp.Name, Me.lblResultGrp.Text)
			Me.ChkSyncRecruitment.Text = Language._Object.getCaption(Me.ChkSyncRecruitment.Name, Me.ChkSyncRecruitment.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Qualification Group is compulsory information.Please Select Qualification Group.")
			Language.setMessage(mstrModuleName, 2, "Qualification Code cannot be blank. Qualification Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Qualification Name cannot be blank. Qualification Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Result Group is compulsory information.Please Select Result Group.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
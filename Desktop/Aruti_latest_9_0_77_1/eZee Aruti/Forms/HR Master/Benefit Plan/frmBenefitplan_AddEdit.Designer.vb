﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBenefitplan_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBenefitplan_AddEdit))
        Me.txtBenefitplanName = New eZee.TextBox.AlphanumericTextBox
        Me.pnlBenefitPlan = New System.Windows.Forms.Panel
        Me.gbBenefitPlan = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblBenefitplanName = New System.Windows.Forms.Label
        Me.txtBenefitplancode = New eZee.TextBox.AlphanumericTextBox
        Me.lblBenefitplancode = New System.Windows.Forms.Label
        Me.cboBenefitGroup = New System.Windows.Forms.ComboBox
        Me.lblBenefitGroup = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlBenefitPlan.SuspendLayout()
        Me.gbBenefitPlan.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtBenefitplanName
        '
        Me.txtBenefitplanName.Flags = 0
        Me.txtBenefitplanName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitplanName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBenefitplanName.Location = New System.Drawing.Point(97, 85)
        Me.txtBenefitplanName.Name = "txtBenefitplanName"
        Me.txtBenefitplanName.Size = New System.Drawing.Size(188, 20)
        Me.txtBenefitplanName.TabIndex = 3
        '
        'pnlBenefitPlan
        '
        Me.pnlBenefitPlan.Controls.Add(Me.gbBenefitPlan)
        Me.pnlBenefitPlan.Controls.Add(Me.objFooter)
        Me.pnlBenefitPlan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBenefitPlan.Location = New System.Drawing.Point(0, 0)
        Me.pnlBenefitPlan.Name = "pnlBenefitPlan"
        Me.pnlBenefitPlan.Size = New System.Drawing.Size(337, 244)
        Me.pnlBenefitPlan.TabIndex = 1
        '
        'gbBenefitPlan
        '
        Me.gbBenefitPlan.BorderColor = System.Drawing.Color.Black
        Me.gbBenefitPlan.Checked = False
        Me.gbBenefitPlan.CollapseAllExceptThis = False
        Me.gbBenefitPlan.CollapsedHoverImage = Nothing
        Me.gbBenefitPlan.CollapsedNormalImage = Nothing
        Me.gbBenefitPlan.CollapsedPressedImage = Nothing
        Me.gbBenefitPlan.CollapseOnLoad = False
        Me.gbBenefitPlan.Controls.Add(Me.objbtnAddGroup)
        Me.gbBenefitPlan.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbBenefitPlan.Controls.Add(Me.txtDescription)
        Me.gbBenefitPlan.Controls.Add(Me.lblDescription)
        Me.gbBenefitPlan.Controls.Add(Me.txtBenefitplanName)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitplanName)
        Me.gbBenefitPlan.Controls.Add(Me.txtBenefitplancode)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitplancode)
        Me.gbBenefitPlan.Controls.Add(Me.cboBenefitGroup)
        Me.gbBenefitPlan.Controls.Add(Me.lblBenefitGroup)
        Me.gbBenefitPlan.ExpandedHoverImage = Nothing
        Me.gbBenefitPlan.ExpandedNormalImage = Nothing
        Me.gbBenefitPlan.ExpandedPressedImage = Nothing
        Me.gbBenefitPlan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBenefitPlan.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBenefitPlan.HeaderHeight = 25
        Me.gbBenefitPlan.HeightOnCollapse = 0
        Me.gbBenefitPlan.LeftTextSpace = 0
        Me.gbBenefitPlan.Location = New System.Drawing.Point(9, 8)
        Me.gbBenefitPlan.Name = "gbBenefitPlan"
        Me.gbBenefitPlan.OpenHeight = 91
        Me.gbBenefitPlan.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBenefitPlan.ShowBorder = True
        Me.gbBenefitPlan.ShowCheckBox = False
        Me.gbBenefitPlan.ShowCollapseButton = False
        Me.gbBenefitPlan.ShowDefaultBorderColor = True
        Me.gbBenefitPlan.ShowDownButton = False
        Me.gbBenefitPlan.ShowHeader = True
        Me.gbBenefitPlan.Size = New System.Drawing.Size(319, 174)
        Me.gbBenefitPlan.TabIndex = 12
        Me.gbBenefitPlan.Temp = 0
        Me.gbBenefitPlan.Text = "Benefit Plan"
        Me.gbBenefitPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(291, 32)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 211
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(288, 87)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(97, 113)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(212, 53)
        Me.txtDescription.TabIndex = 5
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 115)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(83, 14)
        Me.lblDescription.TabIndex = 209
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBenefitplanName
        '
        Me.lblBenefitplanName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitplanName.Location = New System.Drawing.Point(8, 88)
        Me.lblBenefitplanName.Name = "lblBenefitplanName"
        Me.lblBenefitplanName.Size = New System.Drawing.Size(83, 14)
        Me.lblBenefitplanName.TabIndex = 206
        Me.lblBenefitplanName.Text = "Name"
        Me.lblBenefitplanName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBenefitplancode
        '
        Me.txtBenefitplancode.Flags = 0
        Me.txtBenefitplancode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenefitplancode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBenefitplancode.Location = New System.Drawing.Point(97, 59)
        Me.txtBenefitplancode.Name = "txtBenefitplancode"
        Me.txtBenefitplancode.Size = New System.Drawing.Size(111, 20)
        Me.txtBenefitplancode.TabIndex = 2
        '
        'lblBenefitplancode
        '
        Me.lblBenefitplancode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitplancode.Location = New System.Drawing.Point(8, 62)
        Me.lblBenefitplancode.Name = "lblBenefitplancode"
        Me.lblBenefitplancode.Size = New System.Drawing.Size(83, 14)
        Me.lblBenefitplancode.TabIndex = 88
        Me.lblBenefitplancode.Text = "Code"
        '
        'cboBenefitGroup
        '
        Me.cboBenefitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBenefitGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBenefitGroup.FormattingEnabled = True
        Me.cboBenefitGroup.Location = New System.Drawing.Point(97, 32)
        Me.cboBenefitGroup.Name = "cboBenefitGroup"
        Me.cboBenefitGroup.Size = New System.Drawing.Size(188, 21)
        Me.cboBenefitGroup.TabIndex = 1
        '
        'lblBenefitGroup
        '
        Me.lblBenefitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenefitGroup.Location = New System.Drawing.Point(8, 35)
        Me.lblBenefitGroup.Name = "lblBenefitGroup"
        Me.lblBenefitGroup.Size = New System.Drawing.Size(83, 14)
        Me.lblBenefitGroup.TabIndex = 85
        Me.lblBenefitGroup.Text = "Benefit Group"
        Me.lblBenefitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 189)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(337, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(141, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(237, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmBenefitplan_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(337, 244)
        Me.Controls.Add(Me.pnlBenefitPlan)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBenefitplan_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Benefit Plan"
        Me.pnlBenefitPlan.ResumeLayout(False)
        Me.gbBenefitPlan.ResumeLayout(False)
        Me.gbBenefitPlan.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtBenefitplanName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlBenefitPlan As System.Windows.Forms.Panel
    Friend WithEvents gbBenefitPlan As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblBenefitplanName As System.Windows.Forms.Label
    Friend WithEvents txtBenefitplancode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBenefitplancode As System.Windows.Forms.Label
    Friend WithEvents cboBenefitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenefitGroup As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
End Class

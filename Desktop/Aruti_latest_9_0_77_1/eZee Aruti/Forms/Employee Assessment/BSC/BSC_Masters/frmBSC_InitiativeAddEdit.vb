﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmBSC_InitiativeAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBSC_InitiativeAddEdit"
    Private mblnCancel As Boolean = True
    Private objIntiativeMaster As clsinitiative_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintInitiativeUnkid As Integer = -1
    Private mintEmployeeUnkid As Integer = 0
    Private mintObjectiveUnkid As Integer = 0
    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [ 30 MAY 2014 ] -- START
    'Dim objWSetting As New clsWeight_Setting(True)
    Dim objWSetting As clsWeight_Setting
    'S.SANDEEP [ 30 MAY 2014 ] -- END

    Dim mDicWeight As New Dictionary(Of Integer, String)
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintInitiativeUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintInitiativeUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Get_General_Weightage()
        Try
            Dim mDecTotal, mDecAssign As Decimal
            mDecTotal = 0 : mDecAssign = 0
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON AndAlso objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                'S.SANDEEP [ 02 NOV 2013 ] -- START
                'objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD4, mintInitiativeUnkid, mDecAssign, mDecTotal, , CInt(cboYear.SelectedValue))
                objWSetting.Get_Weight_BasedOn(enAllocation.GENERAL, enWeight_Types.WEIGHT_FIELD4, mintInitiativeUnkid, mDecAssign, mDecTotal, , CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                'S.SANDEEP [ 02 NOV 2013 ] -- END
                If mDicWeight.ContainsKey(enAllocation.GENERAL) = False Then
                    mDicWeight.Add(enAllocation.GENERAL, mDecTotal.ToString & "|" & mDecAssign.ToString)
                Else
                    mDicWeight(enAllocation.GENERAL) = mDecTotal.ToString & "|" & mDecAssign.ToString
                End If

                If menAction = enAction.EDIT_ONE Then
                    If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                        If mDecTotal - txtWeight.Decimal <= 0 Then
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                        Else
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecTotal - txtWeight.Decimal) & vbCrLf
                        End If
                        objlblCaption.Text &= Language.getMessage(mstrModuleName, 9, "Total Weightage : ") & CStr(mDecTotal)
                    End If
                Else
                    objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_General_Weightage", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboObjective.BackColor = GUI.ColorOptional
            'cboTargets.BackColor = GUI.ColorComp
            cboObjective.BackColor = GUI.ColorComp
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                cboEmployee.BackColor = GUI.ColorComp
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtWeight.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objIntiativeMaster._Code
            txtName.Text = objIntiativeMaster._Name
            txtDescription.Text = objIntiativeMaster._Description
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtWeight.Decimal = objIntiativeMaster._Weight
            'S.SANDEEP [ 28 DEC 2012 ] -- END
            Dim objObjective As New clsObjective_Master
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                If objIntiativeMaster._Objectiveunkid > 0 Then
                    objObjective._Objectiveunkid = objIntiativeMaster._Objectiveunkid
                    cboEmployee.SelectedValue = objObjective._EmployeeId
                Else
                    Dim objTarget As New clstarget_master
                    Dim objKPI As New clsKPI_Master
                    objTarget._Targetunkid = objIntiativeMaster._Targetunkid
                    If objTarget._Kpiunkid > 0 Then
                        objKPI._Kpiunkid = objTarget._Kpiunkid
                        objObjective._Objectiveunkid = objKPI._Objectiveunkid
                        cboEmployee.SelectedValue = objObjective._EmployeeId
                    Else
                        objObjective._Objectiveunkid = objTarget._Objectiveunkid
                        cboEmployee.SelectedValue = objObjective._EmployeeId
                    End If
                End If

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
            Else
                Call Get_General_Weightage()
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboYear.SelectedValue = objObjective._Yearunkid
            cboPeriod.SelectedValue = objObjective._Periodunkid
            If menAction = enAction.EDIT_ONE Then
                cboYear.Enabled = False : cboPeriod.Enabled = False
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            cboObjective.SelectedValue = objIntiativeMaster._Objectiveunkid

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboTarget.SelectedValue = objIntiativeMaster._Targetunkid
            'S.SANDEEP [ 12 JUNE 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objIntiativeMaster._Code = txtCode.Text
            objIntiativeMaster._Name = txtName.Text
            objIntiativeMaster._Description = txtDescription.Text
            objIntiativeMaster._Objectiveunkid = CInt(cboObjective.SelectedValue)
            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                objIntiativeMaster._EmployeeunkId = CInt(cboEmployee.SelectedValue)
            End If
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIntiativeMaster._Targetunkid = CInt(cboTarget.SelectedValue)
            'S.SANDEEP [ 12 JUNE 2012 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIntiativeMaster._Weight = txtWeight.Decimal
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objMaster As New clsMasterData
        'S.SANDEEP [ 28 DEC 2012 ] -- END
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If ConfigParameter._Object._IsBSC_ByEmployee Then
                Dim objEmployee As New clsEmployee_Master
                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                'End If
                ''Sohail (23 Nov 2012) -- End
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsList.Tables(0)
                'End With

                'S.SANDEEP [04 JUN 2015] -- END

                
            Else
                Dim objObjective As New clsObjective_Master
                dsList = objObjective.getComboList("List", True)
                With cboObjective
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                End With

                'S.SANDEEP [ 12 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objTarget As New clstarget_master
                dsList = objTarget.getComboList("List", True)
                With cboTarget
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                End With
                'S.SANDEEP [ 12 JUNE 2012 ] -- END
            End If
            'Pinkal (20-Jan-2012) -- End


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objMaster.getComboListPAYYEAR("List", True, , , , True)
            dsList = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "List", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is required information. Please select employee."), enMsgBoxStyle.Information)
                    cboEmployee.Select()
                    Return False
                End If

            End If
            'Pinkal (20-Jan-2012) -- End


            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboTargets.SelectedValue) <= 0 AndAlso CInt(cboObjective.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Targets or Objective are required information. Please select Targets or Objective."), enMsgBoxStyle.Information) '?1
            '    cboObjective.Focus()
            '    Return False
            'End If
            If CInt(cboObjective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Objective is required information. Please select Objective."), enMsgBoxStyle.Information) '?1
                cboObjective.Focus()
                Return False
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Return False
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Name cannot be blank. Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Return False
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    If CInt(cboTarget.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Target is mandatory information. Please select Target to continue."), enMsgBoxStyle.Information)
                        cboTarget.Focus()
                        Return False
                    End If
            End Select
            If txtWeight.Visible = True Then
                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Weight is mandatory information. Please give weigth in order to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If
                If objWSetting._Weight_Optionid <> enWeight_Options.WEIGHT_BASED_ON Then
                    If CInt(cboTarget.SelectedValue) > 0 Then
                        If mDicWeight.ContainsKey(CInt(cboTarget.SelectedValue)) = True Then
                            If CDec(mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(1))) Then
                                If CDec(mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(1)) > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(cboTarget.SelectedValue)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                End If
                                txtWeight.Focus()
                                Return False
                            End If
                        End If
                    End If
                Else
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        If CInt(cboEmployee.SelectedValue) > 0 Then
                            If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = True Then
                                If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))) Then
                                    If CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1)) <> 100 Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(cboEmployee.SelectedValue)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                                    End If
                                    txtWeight.Focus()
                                    Return False
                                End If
                            End If
                        End If
                    Else
                        If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) < (txtWeight.Decimal + CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))) Then
                            If CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) > 0 AndAlso CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1)) <> 100 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & CStr(CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0)) - CDec(mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(1))), enMsgBoxStyle.Information)
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you can set this weight as it exceeds the total of ") & mDicWeight(CInt(enAllocation.GENERAL)).Split(CChar("|"))(0).ToString, enMsgBoxStyle.Information)
                            End If
                            txtWeight.Focus()
                            Return False
                        End If
                    End If
                End If
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SHARMA [ 22 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If menAction <> enAction.EDIT_ONE Then

                'S.SANDEEP [ 10 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Dim objSTran As New clsobjective_status_tran
                If ConfigParameter._Object._IsBSC_ByEmployee Then
                    If objSTran.isStatusExists(CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry You cannot save this Initiative." & vbCrLf & _
                                                                                "Reason : There are some Initiative(s). Which are final saved/submitted for approval for the selected period."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                objSTran = Nothing
                'S.SANDEEP [ 10 APR 2013 ] -- END

                If objWSetting.Is_FinalSaved(mintObjectiveUnkid, _
                                  CInt(cboEmployee.SelectedValue), _
                                  CInt(cboYear.SelectedValue), _
                                  CInt(cboPeriod.SelectedValue), enWeight_Types.WEIGHT_FIELD4) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot save Initiative for the selected period. Reason : Some Initiative are final saved for the selected period."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'S.SHARMA [ 22 JAN 2013 ] -- END


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Public Sub OpenAddEdit_Form()
        Try
            Dim frm As New frmBSC_ObjectiveAddEdit
            Dim intRefId As Integer = -1

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objObjective As New clsObjective_Master
                dsList = objObjective.getComboList("List", True)
                With cboObjective
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objObjective = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "OpenAddEdit_Form", mstrModuleName)
        End Try
    End Sub
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            Select Case objWSetting._Weight_Optionid
                Case enWeight_Options.WEIGHT_EACH_ITEM
                    txtWeight.Visible = True
                    objlblCaption.Visible = True
                    lblWeight.Visible = True
                Case enWeight_Options.WEIGHT_BASED_ON
                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD4
                            txtWeight.Visible = True
                            objlblCaption.Visible = True
                            lblWeight.Visible = True
                        Case Else
                            txtWeight.Visible = False
                            objlblCaption.Visible = False
                            lblWeight.Visible = False
                    End Select
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmBSC_InitiativeAddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objIntiativeMaster = Nothing
    End Sub

    Private Sub frmBSC_InitiativeAddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_InitiativeAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_InitiativeAddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_InitiativeAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_InitiativeAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objIntiativeMaster = New clsinitiative_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            Dim p As New Point
            p.X = 3
            If ConfigParameter._Object._IsBSC_ByEmployee = False Then
                p.Y = 30
                pnlinitiatives.Location = p
                Me.Size = New Size(421, 367)
                cboObjective.Select()
            Else
                p.Y = 55
                pnlinitiatives.Location = p
                Me.Size = New Size(421, 394)
                cboEmployee.Select()
            End If

            'Pinkal (20-Jan-2012) -- End


            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objIntiativeMaster._Initiativeunkid = mintInitiativeUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
            End If

            Call GetValue()

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_InitiativeAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsObjective_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsObjective_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objIntiativeMaster.Update()
            Else
                blnFlag = objIntiativeMaster.Insert()
            End If

            If blnFlag = False And objIntiativeMaster._Message <> "" Then
                eZeeMsgBox.Show(objIntiativeMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objIntiativeMaster = Nothing
                    objIntiativeMaster = New clsinitiative_master
                    Call GetValue()

                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    If ConfigParameter._Object._IsBSC_ByEmployee Then
                        cboEmployee.Select()
                    Else
                        cboObjective.Select()
                    End If
                    'Pinkal (20-Jan-2012) -- End

                    cboEmployee.SelectedValue = mintEmployeeUnkid
                    cboObjective.SelectedValue = mintObjectiveUnkid

                Else
                    mintInitiativeUnkid = objIntiativeMaster._Initiativeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtName.Text, objIntiativeMaster._Name1, objIntiativeMaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddObjective.Click
        Try
            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsBSC_ByEmployee Then

                If ConfigParameter._Object._IsBSCObjectiveSaved = False Then

                    If eZeeMsgBox.Show(Language.getMessage("frmBSC_ObjectiveList", 4, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        OpenAddEdit_Form()
                    Else
                        Exit Sub
                    End If

                Else
                    OpenAddEdit_Form()
                End If

            Else
                OpenAddEdit_Form()
            End If

            'Pinkal (20-Jan-2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddObjective_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddTargets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmBSC_TargetAddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objTargets As New clstarget_master
                dsList = objTargets.getComboList("List", True)
                With cboObjective
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objTargets = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTargets_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (20-Jan-2012) -- End

    'S.SANDEEP [ 12 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearchObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchObjective.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboObjective.ValueMember
                .DisplayMember = cboObjective.DisplayMember
                .DataSource = CType(cboObjective.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboObjective.SelectedValue = frm.SelectedValue
                cboObjective.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTarget_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTarget.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboTarget.ValueMember
                .DisplayMember = cboTarget.DisplayMember
                .DataSource = CType(cboTarget.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboTarget.SelectedValue = frm.SelectedValue
                cboTarget.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTarget_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddTarget_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTarget.Click
        Try
            Dim frm As New frmBSC_TargetAddEdit
            Dim intRefId As Integer = -1
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objTarget As New clstarget_master
                dsList = objTarget.getComboList("List", True, CInt(cboObjective.SelectedValue), , CInt(cboEmployee.SelectedValue))
                With cboTarget
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objTarget = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTarget_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 12 JUNE 2012 ] -- END

#End Region

#Region "ComboBox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'S.SANDEEP [ 30 MAY 2014 ] -- START
            objWSetting = New clsWeight_Setting(CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [ 30 MAY 2014 ] -- END
            Dim objObjective As New clsObjective_Master
            Dim dsList As DataSet = objObjective.getComboList("List", True, , CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            cboObjective.DisplayMember = "name"
            cboObjective.ValueMember = "id"
            cboObjective.DataSource = dsList.Tables("List")
            'If CType(sender, ComboBox).Name.ToUpper = "CBOEMPLOYEE" Then
            If CInt(cboEmployee.SelectedValue) > 0 Then
                mintEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_BASED_ON
                        Select Case objWSetting._Weight_Typeid
                            Case enWeight_Types.WEIGHT_FIELD4
                                Dim mDecTotal, mDecAssign As Decimal
                                mDecTotal = 0 : mDecAssign = 0
                                If ConfigParameter._Object._IsBSC_ByEmployee Then
                                    'S.SANDEEP [ 02 NOV 2013 ] -- START
                                    'objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD4, mintInitiativeUnkid, mDecAssign, mDecTotal, mintEmployeeUnkid, CInt(cboYear.SelectedValue))
                                    objWSetting.Get_Weight_BasedOn(enAllocation.EMPLOYEE, enWeight_Types.WEIGHT_FIELD4, mintInitiativeUnkid, mDecAssign, mDecTotal, mintEmployeeUnkid, CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
                                    'S.SANDEEP [ 02 NOV 2013 ] -- END
                                    If mDicWeight.ContainsKey(CInt(cboEmployee.SelectedValue)) = False Then
                                        mDicWeight.Add(CInt(cboEmployee.SelectedValue), mDecTotal.ToString & "|" & mDecAssign.ToString)
                                    Else
                                        mDicWeight(CInt(cboEmployee.SelectedValue)) = mDecTotal.ToString & "|" & mDecAssign.ToString
                                    End If
                                    If menAction = enAction.EDIT_ONE Then
                                        If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                                            If mDecTotal - txtWeight.Decimal <= 0 Then
                                                objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                                            Else
                                                objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecAssign) & vbCrLf
                                            End If
                                            objlblCaption.Text &= Language.getMessage(mstrModuleName, 9, "Total Weightage : ") & CStr(mDecTotal)
                                        End If
                                    Else
                                        objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                                    End If
                                End If
                        End Select
                End Select
                'S.SANDEEP [ 28 DEC 2012 ] -- END
            End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboObjective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObjective.SelectedIndexChanged
        Try
            If CInt(cboObjective.SelectedValue) > 0 Then
                mintObjectiveUnkid = CInt(cboObjective.SelectedValue)
            End If

            'S.SANDEEP [ 12 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objTarget As New clstarget_master
            Dim dsList As New DataSet
            dsList = objTarget.getComboList("List", True, CInt(cboObjective.SelectedValue), , CInt(cboEmployee.SelectedValue))
            With cboTarget
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            'S.SANDEEP [ 12 JUNE 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboObjective_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboTarget_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTarget.SelectedIndexChanged
        Try
            If CInt(cboTarget.SelectedValue) > 0 Then
                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_EACH_ITEM
                        Dim mDecTotal, mDecAssign As Decimal
                        mDecTotal = 0 : mDecAssign = 0
                        objWSetting.Get_Each_Item_Weight(enWeight_Types.WEIGHT_FIELD3, CInt(cboTarget.SelectedValue), mintInitiativeUnkid, mDecAssign, mDecTotal)
                        If mDecTotal = 0 AndAlso mDecAssign = 0 Then
                            Dim objTarget As New clstarget_master
                            objTarget._Targetunkid = CInt(cboTarget.SelectedValue)
                            mDecTotal = objTarget._Weight
                        End If
                        If mDicWeight.ContainsKey(CInt(cboTarget.SelectedValue)) = False Then
                            mDicWeight.Add(CInt(cboTarget.SelectedValue), mDecTotal.ToString & "|" & mDecAssign.ToString)
                        Else
                            mDicWeight(CInt(cboTarget.SelectedValue)) = mDecTotal.ToString & "|" & mDecAssign.ToString
                        End If
                        If menAction = enAction.EDIT_ONE Then
                            If txtWeight.Visible = True AndAlso txtWeight.Decimal > 0 Then
                                If mDecTotal - txtWeight.Decimal <= 0 Then
                                    objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecTotal) & vbCrLf
                                Else
                                    objlblCaption.Text = Language.getMessage(mstrModuleName, 10, "Total Assigned Weightage : ") & CStr(mDecTotal - txtWeight.Decimal) & vbCrLf
                                End If
                                objlblCaption.Text &= Language.getMessage(mstrModuleName, 9, "Total Target Weightage : ") & CStr(mDecTotal)
                            End If
                        Else
                            objlblCaption.Text = Language.getMessage(mstrModuleName, 8, "Total Remaining Weightage : ") & CStr(mDecTotal - mDecAssign)
                        End If
                End Select
            Else
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then objlblCaption.Text = ""
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboTarget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Tab Events "

    Private Sub tabpDescription_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpDescription.Enter
        Try
            objbtnOtherLanguage.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpDescription_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tabpInitiavtive_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabpInitiavtive.Enter
        Try
            objbtnOtherLanguage.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tabpInitiavtive_Enter", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbInitiativeAction.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInitiativeAction.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbInitiativeAction.Text = Language._Object.getCaption(Me.gbInitiativeAction.Name, Me.gbInitiativeAction.Text)
			Me.lblAssessmentItemCode.Text = Language._Object.getCaption(Me.lblAssessmentItemCode.Name, Me.lblAssessmentItemCode.Text)
			Me.lblObjective.Text = Language._Object.getCaption(Me.lblObjective.Name, Me.lblObjective.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.tabpInitiavtive.Text = Language._Object.getCaption(Me.tabpInitiavtive.Name, Me.tabpInitiavtive.Text)
			Me.tabpDescription.Text = Language._Object.getCaption(Me.tabpDescription.Name, Me.tabpDescription.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is required information. Please select employee.")
			Language.setMessage(mstrModuleName, 2, "Objective is required information. Please select Objective.")
			Language.setMessage(mstrModuleName, 3, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 4, "Name cannot be blank. Name is required information.")
			Language.setMessage("frmBSC_ObjectiveList", 4, "As per your configuration setting Balanced Score Card Obective is set to base on employee. " & vbCrLf & " If you want to continue with the same setting please select 'Yes' or you can change the settings to 'general'." & vbCrLf & "Aruti Configuration -> Options -> General -> 'Balance Score Card Evaluation Settings'." & vbCrLf & " Do You wish to continue ?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class
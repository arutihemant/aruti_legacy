﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentCaptions

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentCaptions"
    Private objFieldMaster As clsAssess_Field_Master
    Private mdtData As DataTable = Nothing

#End Region

#Region " Private Methods "

    Private Sub Set_Grid_Data()
        Try
            mdtData = objFieldMaster.Get_Fields_Details()
            dgvData.AutoGenerateColumns = False
            objdgcolhIsUsed.DataPropertyName = "isused"
            dgcolhCaption.DataPropertyName = "fieldcaption"
            dgcolhInformation.DataPropertyName = "isinformational"
            dgcolhParent.DataPropertyName = "isparent"
            dgvData.DataSource = mdtData
            Call Grid_Controls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Grid_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Grid_Controls()
        Try
            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CBool(dgvRow.Cells(objdgcolhIsUsed.Index).Value) = True AndAlso dgvRow.Index = 0 Then
                    dgvRow.Cells(dgcolhParent.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhInformation.Index).ReadOnly = True
                    dgvRow.Cells(objdgcolhIsUsed.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhCaption.Index).Style.BackColor = Color.LightGreen
                ElseIf CBool(dgvRow.Cells(objdgcolhIsUsed.Index).Value) = False AndAlso dgvRow.Index > 0 Then
                    dgvRow.Cells(dgcolhParent.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhInformation.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhCaption.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhCaption.Index).Style.BackColor = Color.Khaki
                ElseIf CBool(dgvRow.Cells(objdgcolhIsUsed.Index).Value) = True AndAlso dgvRow.Index > 0 Then
                    dgvRow.Cells(dgcolhParent.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhInformation.Index).ReadOnly = False
                    dgvRow.Cells(objdgcolhIsUsed.Index).ReadOnly = False
                    dgvRow.Cells(dgcolhCaption.Index).Style.BackColor = Color.LightGreen
                End If
                If dgvRow.Index > 0 Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhParent.Index).ColumnIndex, dgvRow.Cells(dgcolhParent.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.Cells(dgcolhParent.Index).ReadOnly = True
                End If

                If dgvRow.Index <= 4 Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhInformation.Index).ColumnIndex, dgvRow.Cells(dgcolhInformation.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.Cells(dgcolhInformation.Index).ReadOnly = True
                End If

                If dgvRow.Index > 4 Then
                    dgvRow.Cells(dgcolhInformation.Index).Value = True
                    dgvRow.Cells(dgcolhInformation.Index).ReadOnly = True
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Grid_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAssessmentCaptions_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objFieldMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentCaptions_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessmentCaptions_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objFieldMaster = New clsAssess_Field_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call Set_Grid_Data()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnSave.Enabled = User._Object.Privilege._AllowtoSaveBSCTitles
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentCaptions_AddEdit_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If mdtData IsNot Nothing Then
                Dim dRow As DataRow() = mdtData.Select("isused=True and ISNULL(fieldcaption,'') = ''")
                If dRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Field Caption cannot be blank. Field Caption is mandatory information."), enMsgBoxStyle.Information)
                    Dim iRowIdx As Integer = mdtData.Rows.IndexOf(dRow(0))
                    dgvData.Rows(iRowIdx).Cells(dgcolhCaption.Index).Selected = True
                    Exit Sub
                End If

                dRow = mdtData.Select("isused=true and isinformational=false")
                If dRow.Length > 5 Then 'MAX FIVE FIELDS TO BE LINKED
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot make more than 5 fields as not informational."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim blnIsUsed As Boolean = False
                dRow = mdtData.Select("isused=false AND AUD = 'U'")
                If dRow.Length > 0 Then
                    For index As Integer = 0 To dRow.Length - 1
                        If objFieldMaster.isUsed(CInt(dRow(index).Item("fieldunkid"))) Then
                            dRow(index).Item("isused") = True
                            dRow(index).Item("AUD") = ""
                            dRow(index).AcceptChanges()
                            blnIsUsed = True
                        End If
                    Next
                    mdtData.AcceptChanges()
                    If blnIsUsed = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Some of the captions are used in transaction you cannot make it unused."), enMsgBoxStyle.Information)
                    End If
                End If


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objFieldMaster._FormName = mstrModuleName
                objFieldMaster._LoginEmployeeunkid = 0
                objFieldMaster._ClientIP = getIP()
                objFieldMaster._HostName = getHostName()
                objFieldMaster._FromWeb = False
                objFieldMaster._AuditUserId = User._Object._Userunkid
objFieldMaster._CompanyUnkid = Company._Object._Companyunkid
                objFieldMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                blnFlag = objFieldMaster.InsertUpdateField_Caption(User._Object._Userunkid, mdtData)

                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Field Caption(s) successfully saved."), enMsgBoxStyle.Information)
                    Me.Close()
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Problem in saving Field Caption(s)."), enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Other Control Events "

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhCaption.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            If e.RowIndex = -1 Then Exit Sub
            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            Select Case CInt(e.ColumnIndex)
                Case objdgcolhIsUsed.Index
                    If CBool(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                        If e.RowIndex > 0 Then
                            Dim idx As Integer = e.RowIndex - 1
                            If CBool(dgvData.Rows(idx).Cells(e.ColumnIndex).Value) = False AndAlso _
                            CBool(dgvData.Rows(e.RowIndex).Cells(dgcolhInformation.Index).Value) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot leave previous field as unused."), enMsgBoxStyle.Information)
                                dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = False
                                Exit Sub
                            End If
                        End If
                        dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).ReadOnly = False : dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Style.BackColor = Color.LightGreen
                        If e.RowIndex > 4 Then
                            'dgvData.Rows(e.RowIndex).Cells(dgcolhInformation.Index).ReadOnly = False
                        End If
                        dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Selected = True
                    ElseIf CBool(dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                        dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).ReadOnly = True : dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Style.BackColor = Color.Khaki
                        dgvData.Rows(e.RowIndex).Cells(dgcolhInformation.Index).ReadOnly = True
                        dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Value = ""
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex = -1 Then Exit Sub

            'If Me.dgvData.IsCurrentCellDirty Then
            '    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            'End If
            If CInt(e.ColumnIndex) > 0 Then
                If objFieldMaster.isExist(dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Value.ToString.Trim, CInt(mdtData.Rows(e.RowIndex).Item("fieldunkid"))) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry this caption is already defined. Please define new caption."), enMsgBoxStyle.Information)
                    dgvData.Rows(e.RowIndex).Cells(dgcolhCaption.Index).Value = ""
                    Exit Sub
                End If
                If mdtData IsNot Nothing Then
                    If CInt(mdtData.Rows(e.RowIndex).Item("fieldunkid")) > 0 Then
                        mdtData.Rows(e.RowIndex).Item("AUD") = "U"
                    Else
                        mdtData.Rows(e.RowIndex).Item("AUD") = "A"
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.dgcolhCaption.HeaderText = Language._Object.getCaption(Me.dgcolhCaption.Name, Me.dgcolhCaption.HeaderText)
			Me.dgcolhParent.HeaderText = Language._Object.getCaption(Me.dgcolhParent.Name, Me.dgcolhParent.HeaderText)
			Me.dgcolhInformation.HeaderText = Language._Object.getCaption(Me.dgcolhInformation.Name, Me.dgcolhInformation.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Field Caption cannot be blank. Field Caption is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot make more than 5 fields as not informational.")
			Language.setMessage(mstrModuleName, 3, "Field Caption(s) successfully saved.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Problem in saving Field Caption(s).")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot leave previous field as unused.")
			Language.setMessage(mstrModuleName, 6, "Sorry this caption is already defined. Please define new caption.")
			Language.setMessage(mstrModuleName, 7, "Sorry, Some of the captions are used in transaction you cannot make it unused.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
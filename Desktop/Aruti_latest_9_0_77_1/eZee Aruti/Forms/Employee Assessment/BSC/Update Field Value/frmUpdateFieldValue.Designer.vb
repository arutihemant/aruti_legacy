﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdateFieldValue
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdateFieldValue))
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.cboCalculationType = New System.Windows.Forms.ComboBox
        Me.lblCalcType = New System.Windows.Forms.Label
        Me.txtTotalPercentage = New eZee.TextBox.NumericTextBox
        Me.lblCaption4 = New System.Windows.Forms.Label
        Me.txtNewPercentage = New eZee.TextBox.NumericTextBox
        Me.lblCaption3 = New System.Windows.Forms.Label
        Me.txtTotalValue = New eZee.TextBox.NumericTextBox
        Me.lblCaption2 = New System.Windows.Forms.Label
        Me.cboChangeBy = New System.Windows.Forms.ComboBox
        Me.lblChangeBy = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblGoals = New System.Windows.Forms.Label
        Me.txtGoals = New System.Windows.Forms.TextBox
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPercent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAccomplishedStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAccomplishedStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.objeline1 = New eZee.Common.eZeeLine
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.txtPeriod = New System.Windows.Forms.TextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.txtEmployeeName = New System.Windows.Forms.TextBox
        Me.dtpChangeDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtNewValue = New eZee.TextBox.NumericTextBox
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlnkGoalType = New System.Windows.Forms.LinkLabel
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblOwner = New System.Windows.Forms.Label
        Me.txtOwner = New System.Windows.Forms.TextBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkIncludeDisapproved = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.chkIncludeDisapproved)
        Me.pnlMain.Controls.Add(Me.cboCalculationType)
        Me.pnlMain.Controls.Add(Me.lblCalcType)
        Me.pnlMain.Controls.Add(Me.txtTotalPercentage)
        Me.pnlMain.Controls.Add(Me.lblCaption4)
        Me.pnlMain.Controls.Add(Me.txtNewPercentage)
        Me.pnlMain.Controls.Add(Me.lblCaption3)
        Me.pnlMain.Controls.Add(Me.txtTotalValue)
        Me.pnlMain.Controls.Add(Me.lblCaption2)
        Me.pnlMain.Controls.Add(Me.cboChangeBy)
        Me.pnlMain.Controls.Add(Me.lblChangeBy)
        Me.pnlMain.Controls.Add(Me.lblEmployee)
        Me.pnlMain.Controls.Add(Me.lblGoals)
        Me.pnlMain.Controls.Add(Me.txtGoals)
        Me.pnlMain.Controls.Add(Me.dgvHistory)
        Me.pnlMain.Controls.Add(Me.btnSave)
        Me.pnlMain.Controls.Add(Me.objeline1)
        Me.pnlMain.Controls.Add(Me.lblRemark)
        Me.pnlMain.Controls.Add(Me.txtRemark)
        Me.pnlMain.Controls.Add(Me.txtPeriod)
        Me.pnlMain.Controls.Add(Me.lblPeriod)
        Me.pnlMain.Controls.Add(Me.txtEmployeeName)
        Me.pnlMain.Controls.Add(Me.dtpChangeDate)
        Me.pnlMain.Controls.Add(Me.lblDate)
        Me.pnlMain.Controls.Add(Me.txtNewValue)
        Me.pnlMain.Controls.Add(Me.lblCaption1)
        Me.pnlMain.Controls.Add(Me.cboStatus)
        Me.pnlMain.Controls.Add(Me.lblStatus)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.lblOwner)
        Me.pnlMain.Controls.Add(Me.txtOwner)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(695, 526)
        Me.pnlMain.TabIndex = 0
        '
        'cboCalculationType
        '
        Me.cboCalculationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCalculationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCalculationType.FormattingEnabled = True
        Me.cboCalculationType.Location = New System.Drawing.Point(137, 147)
        Me.cboCalculationType.Name = "cboCalculationType"
        Me.cboCalculationType.Size = New System.Drawing.Size(249, 21)
        Me.cboCalculationType.TabIndex = 514
        '
        'lblCalcType
        '
        Me.lblCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcType.Location = New System.Drawing.Point(9, 149)
        Me.lblCalcType.Name = "lblCalcType"
        Me.lblCalcType.Size = New System.Drawing.Size(122, 17)
        Me.lblCalcType.TabIndex = 515
        Me.lblCalcType.Text = "Calculation Mode"
        Me.lblCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTotalPercentage
        '
        Me.txtTotalPercentage.AllowNegative = False
        Me.txtTotalPercentage.BackColor = System.Drawing.Color.White
        Me.txtTotalPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtTotalPercentage.DigitsInGroup = 0
        Me.txtTotalPercentage.Flags = 65536
        Me.txtTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPercentage.Location = New System.Drawing.Point(137, 230)
        Me.txtTotalPercentage.MaxDecimalPlaces = 2
        Me.txtTotalPercentage.MaxWholeDigits = 9
        Me.txtTotalPercentage.Name = "txtTotalPercentage"
        Me.txtTotalPercentage.Prefix = ""
        Me.txtTotalPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPercentage.Size = New System.Drawing.Size(116, 21)
        Me.txtTotalPercentage.TabIndex = 513
        Me.txtTotalPercentage.Text = "0.00"
        Me.txtTotalPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCaption4
        '
        Me.lblCaption4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption4.Location = New System.Drawing.Point(9, 232)
        Me.lblCaption4.Name = "lblCaption4"
        Me.lblCaption4.Size = New System.Drawing.Size(122, 17)
        Me.lblCaption4.TabIndex = 512
        Me.lblCaption4.Text = "Total Percentage"
        Me.lblCaption4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNewPercentage
        '
        Me.txtNewPercentage.AllowNegative = False
        Me.txtNewPercentage.BackColor = System.Drawing.Color.White
        Me.txtNewPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtNewPercentage.DigitsInGroup = 0
        Me.txtNewPercentage.Flags = 65536
        Me.txtNewPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewPercentage.Location = New System.Drawing.Point(137, 176)
        Me.txtNewPercentage.MaxDecimalPlaces = 2
        Me.txtNewPercentage.MaxWholeDigits = 9
        Me.txtNewPercentage.Name = "txtNewPercentage"
        Me.txtNewPercentage.Prefix = ""
        Me.txtNewPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtNewPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtNewPercentage.Size = New System.Drawing.Size(116, 21)
        Me.txtNewPercentage.TabIndex = 511
        Me.txtNewPercentage.Text = "0.00"
        Me.txtNewPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCaption3
        '
        Me.lblCaption3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption3.Location = New System.Drawing.Point(9, 178)
        Me.lblCaption3.Name = "lblCaption3"
        Me.lblCaption3.Size = New System.Drawing.Size(122, 17)
        Me.lblCaption3.TabIndex = 510
        Me.lblCaption3.Text = "New Percentage"
        Me.lblCaption3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTotalValue
        '
        Me.txtTotalValue.AllowNegative = False
        Me.txtTotalValue.BackColor = System.Drawing.Color.White
        Me.txtTotalValue.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtTotalValue.DigitsInGroup = 0
        Me.txtTotalValue.Flags = 65536
        Me.txtTotalValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalValue.Location = New System.Drawing.Point(137, 258)
        Me.txtTotalValue.MaxDecimalPlaces = 2
        Me.txtTotalValue.MaxWholeDigits = 9
        Me.txtTotalValue.Name = "txtTotalValue"
        Me.txtTotalValue.Prefix = ""
        Me.txtTotalValue.RangeMax = 1.7976931348623157E+308
        Me.txtTotalValue.RangeMin = -1.7976931348623157E+308
        Me.txtTotalValue.Size = New System.Drawing.Size(116, 21)
        Me.txtTotalValue.TabIndex = 509
        Me.txtTotalValue.Text = "0.00"
        Me.txtTotalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCaption2
        '
        Me.lblCaption2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption2.Location = New System.Drawing.Point(9, 260)
        Me.lblCaption2.Name = "lblCaption2"
        Me.lblCaption2.Size = New System.Drawing.Size(122, 17)
        Me.lblCaption2.TabIndex = 508
        Me.lblCaption2.Text = "Total Value"
        Me.lblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChangeBy
        '
        Me.cboChangeBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeBy.FormattingEnabled = True
        Me.cboChangeBy.Location = New System.Drawing.Point(137, 120)
        Me.cboChangeBy.Name = "cboChangeBy"
        Me.cboChangeBy.Size = New System.Drawing.Size(249, 21)
        Me.cboChangeBy.TabIndex = 506
        '
        'lblChangeBy
        '
        Me.lblChangeBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeBy.Location = New System.Drawing.Point(9, 122)
        Me.lblChangeBy.Name = "lblChangeBy"
        Me.lblChangeBy.Size = New System.Drawing.Size(122, 17)
        Me.lblChangeBy.TabIndex = 507
        Me.lblChangeBy.Text = "Change By"
        Me.lblChangeBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 41)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(122, 17)
        Me.lblEmployee.TabIndex = 496
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGoals
        '
        Me.lblGoals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoals.Location = New System.Drawing.Point(392, 15)
        Me.lblGoals.Name = "lblGoals"
        Me.lblGoals.Size = New System.Drawing.Size(291, 17)
        Me.lblGoals.TabIndex = 505
        Me.lblGoals.Text = "Goal"
        Me.lblGoals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGoals
        '
        Me.txtGoals.BackColor = System.Drawing.Color.White
        Me.txtGoals.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGoals.Location = New System.Drawing.Point(392, 39)
        Me.txtGoals.Multiline = True
        Me.txtGoals.Name = "txtGoals"
        Me.txtGoals.ReadOnly = True
        Me.txtGoals.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtGoals.Size = New System.Drawing.Size(292, 102)
        Me.txtGoals.TabIndex = 504
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhDate, Me.dgcolhPercent, Me.dgcolhLastValue, Me.dgcolhStatus, Me.dgcolhRemark, Me.dgcolhAccomplishedStatus, Me.objdgcolhUnkid, Me.objdgcolhAccomplishedStatusId})
        Me.dgvHistory.Location = New System.Drawing.Point(12, 306)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHistory.Size = New System.Drawing.Size(672, 163)
        Me.dgvHistory.TabIndex = 503
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.NullValue = Nothing
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle9
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Visible = False
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.NullValue = Nothing
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle10
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhDate
        '
        Me.dgcolhDate.HeaderText = "Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhDate.Width = 90
        '
        'dgcolhPercent
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhPercent.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhPercent.HeaderText = "% Completed"
        Me.dgcolhPercent.Name = "dgcolhPercent"
        Me.dgcolhPercent.ReadOnly = True
        Me.dgcolhPercent.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhPercent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhPercent.Width = 90
        '
        'dgcolhLastValue
        '
        Me.dgcolhLastValue.HeaderText = "Last Value"
        Me.dgcolhLastValue.Name = "dgcolhLastValue"
        Me.dgcolhLastValue.ReadOnly = True
        Me.dgcolhLastValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhStatus.Width = 120
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'dgcolhAccomplishedStatus
        '
        Me.dgcolhAccomplishedStatus.HeaderText = "Approval Status"
        Me.dgcolhAccomplishedStatus.Name = "dgcolhAccomplishedStatus"
        Me.dgcolhAccomplishedStatus.ReadOnly = True
        '
        'objdgcolhUnkid
        '
        Me.objdgcolhUnkid.HeaderText = "objdgcolhUnkid"
        Me.objdgcolhUnkid.Name = "objdgcolhUnkid"
        Me.objdgcolhUnkid.ReadOnly = True
        Me.objdgcolhUnkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhUnkid.Visible = False
        '
        'objdgcolhAccomplishedStatusId
        '
        Me.objdgcolhAccomplishedStatusId.HeaderText = "objcolhAccomplishedStatusId"
        Me.objdgcolhAccomplishedStatusId.Name = "objdgcolhAccomplishedStatusId"
        Me.objdgcolhAccomplishedStatusId.ReadOnly = True
        Me.objdgcolhAccomplishedStatusId.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(590, 254)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 502
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'objeline1
        '
        Me.objeline1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objeline1.Location = New System.Drawing.Point(259, 256)
        Me.objeline1.Name = "objeline1"
        Me.objeline1.Size = New System.Drawing.Size(325, 26)
        Me.objeline1.TabIndex = 501
        Me.objeline1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(392, 149)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(291, 17)
        Me.lblRemark.TabIndex = 500
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.BackColor = System.Drawing.Color.White
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(392, 174)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(292, 77)
        Me.txtRemark.TabIndex = 499
        '
        'txtPeriod
        '
        Me.txtPeriod.BackColor = System.Drawing.Color.White
        Me.txtPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriod.Location = New System.Drawing.Point(137, 12)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.ReadOnly = True
        Me.txtPeriod.Size = New System.Drawing.Size(249, 21)
        Me.txtPeriod.TabIndex = 498
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 14)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(122, 17)
        Me.lblPeriod.TabIndex = 497
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.BackColor = System.Drawing.Color.White
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.Location = New System.Drawing.Point(137, 39)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmployeeName.Size = New System.Drawing.Size(249, 21)
        Me.txtEmployeeName.TabIndex = 495
        '
        'dtpChangeDate
        '
        Me.dtpChangeDate.Checked = False
        Me.dtpChangeDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpChangeDate.Location = New System.Drawing.Point(137, 66)
        Me.dtpChangeDate.Name = "dtpChangeDate"
        Me.dtpChangeDate.ShowCheckBox = True
        Me.dtpChangeDate.Size = New System.Drawing.Size(116, 21)
        Me.dtpChangeDate.TabIndex = 494
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(9, 68)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(122, 17)
        Me.lblDate.TabIndex = 493
        Me.lblDate.Text = "Change Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNewValue
        '
        Me.txtNewValue.AllowNegative = False
        Me.txtNewValue.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtNewValue.DigitsInGroup = 0
        Me.txtNewValue.Flags = 65536
        Me.txtNewValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNewValue.Location = New System.Drawing.Point(137, 203)
        Me.txtNewValue.MaxDecimalPlaces = 2
        Me.txtNewValue.MaxWholeDigits = 9
        Me.txtNewValue.Name = "txtNewValue"
        Me.txtNewValue.Prefix = ""
        Me.txtNewValue.RangeMax = 1.7976931348623157E+308
        Me.txtNewValue.RangeMin = -1.7976931348623157E+308
        Me.txtNewValue.Size = New System.Drawing.Size(116, 21)
        Me.txtNewValue.TabIndex = 492
        Me.txtNewValue.Text = "0.00"
        Me.txtNewValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCaption1
        '
        Me.lblCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption1.Location = New System.Drawing.Point(9, 205)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(122, 17)
        Me.lblCaption1.TabIndex = 491
        Me.lblCaption1.Text = "New Value"
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(137, 93)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(249, 21)
        Me.cboStatus.TabIndex = 489
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(9, 95)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(122, 17)
        Me.lblStatus.TabIndex = 490
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlnkGoalType)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 471)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(695, 55)
        Me.objFooter.TabIndex = 447
        '
        'objlnkGoalType
        '
        Me.objlnkGoalType.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkGoalType.Location = New System.Drawing.Point(32, 20)
        Me.objlnkGoalType.Name = "objlnkGoalType"
        Me.objlnkGoalType.Size = New System.Drawing.Size(474, 16)
        Me.objlnkGoalType.TabIndex = 1
        Me.objlnkGoalType.TabStop = True
        Me.objlnkGoalType.Text = "#"
        Me.objlnkGoalType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(589, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblOwner
        '
        Me.lblOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwner.Location = New System.Drawing.Point(9, 41)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(122, 17)
        Me.lblOwner.TabIndex = 498
        Me.lblOwner.Text = "Owner"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOwner
        '
        Me.txtOwner.BackColor = System.Drawing.Color.White
        Me.txtOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOwner.Location = New System.Drawing.Point(137, 39)
        Me.txtOwner.Name = "txtOwner"
        Me.txtOwner.ReadOnly = True
        Me.txtOwner.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOwner.Size = New System.Drawing.Size(249, 21)
        Me.txtOwner.TabIndex = 497
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn2.HeaderText = "% Completed"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn2.Width = 90
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhUnkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhUnkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhAccomplishedStatusId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'chkIncludeDisapproved
        '
        Me.chkIncludeDisapproved.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkIncludeDisapproved.Location = New System.Drawing.Point(428, 287)
        Me.chkIncludeDisapproved.Name = "chkIncludeDisapproved"
        Me.chkIncludeDisapproved.Size = New System.Drawing.Size(255, 15)
        Me.chkIncludeDisapproved.TabIndex = 516
        Me.chkIncludeDisapproved.Text = "Include Disapproved data in the below list"
        Me.chkIncludeDisapproved.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkIncludeDisapproved.UseVisualStyleBackColor = True
        '
        'frmUpdateFieldValue
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(695, 526)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUpdateFieldValue"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Update Progress"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpChangeDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtNewValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeName As System.Windows.Forms.TextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objeline1 As eZee.Common.eZeeLine
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents lblGoals As System.Windows.Forms.Label
    Friend WithEvents txtGoals As System.Windows.Forms.TextBox
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents txtOwner As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCaption2 As System.Windows.Forms.Label
    Friend WithEvents cboChangeBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblChangeBy As System.Windows.Forms.Label
    Friend WithEvents txtNewPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCaption3 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlnkGoalType As System.Windows.Forms.LinkLabel
    Friend WithEvents txtTotalPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCaption4 As System.Windows.Forms.Label
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPercent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAccomplishedStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAccomplishedStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCalculationType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCalcType As System.Windows.Forms.Label
    Friend WithEvents chkIncludeDisapproved As System.Windows.Forms.CheckBox
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpFieldsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpFieldsList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchPerspective = New eZee.Common.eZeeGradientButton
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.objbtnSearchEmp = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboFieldValue5 = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objlblField5 = New System.Windows.Forms.Label
        Me.cboFieldValue4 = New System.Windows.Forms.ComboBox
        Me.objbtnSearchField5 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchField4 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchField3 = New eZee.Common.eZeeGradientButton
        Me.objlblField4 = New System.Windows.Forms.Label
        Me.objbtnSearchField2 = New eZee.Common.eZeeGradientButton
        Me.objlblField3 = New System.Windows.Forms.Label
        Me.objlblField1 = New System.Windows.Forms.Label
        Me.cboFieldValue3 = New System.Windows.Forms.ComboBox
        Me.cboFieldValue2 = New System.Windows.Forms.ComboBox
        Me.objlblField2 = New System.Windows.Forms.Label
        Me.objbtnSearchField1 = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboFieldValue1 = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSubmitForGoalAccomplished = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSubmitApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproveSubmitted = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlockFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuGlobalOperation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidGoals = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdatePercentage = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportBSC_Planning = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintBSCForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAppRejGoalAccomplished = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportGoals = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalVoidGoals = New System.Windows.Forms.ToolStripMenuItem
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.lblCaption2 = New System.Windows.Forms.Label
        Me.objpnl2 = New System.Windows.Forms.Panel
        Me.objpnl1 = New System.Windows.Forms.Panel
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuUnlockEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMain.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objspc1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(919, 543)
        Me.pnlMain.TabIndex = 1
        '
        'objspc1
        '
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.IsSplitterFixed = True
        Me.objspc1.Location = New System.Drawing.Point(0, 0)
        Me.objspc1.Margin = New System.Windows.Forms.Padding(0)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.dgvData)
        Me.objspc1.Size = New System.Drawing.Size(919, 488)
        Me.objspc1.SplitterDistance = 168
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 7
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.cboPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.lblPerspective)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmp)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue5)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField5)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField5)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField3)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField4)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField3)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField1)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue3)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(919, 168)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPerspective
        '
        Me.objbtnSearchPerspective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPerspective.BorderSelected = False
        Me.objbtnSearchPerspective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPerspective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPerspective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPerspective.Location = New System.Drawing.Point(266, 52)
        Me.objbtnSearchPerspective.Name = "objbtnSearchPerspective"
        Me.objbtnSearchPerspective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPerspective.TabIndex = 486
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.DropDownWidth = 250
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(12, 52)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(248, 21)
        Me.cboPerspective.TabIndex = 484
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(12, 34)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(248, 15)
        Me.lblPerspective.TabIndex = 483
        Me.lblPerspective.Text = "Perspective"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmp
        '
        Me.objbtnSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmp.BorderSelected = False
        Me.objbtnSearchEmp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmp.Location = New System.Drawing.Point(266, 94)
        Me.objbtnSearchEmp.Name = "objbtnSearchEmp"
        Me.objbtnSearchEmp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmp.TabIndex = 481
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 76)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(248, 15)
        Me.lblEmployee.TabIndex = 479
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(12, 94)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(248, 21)
        Me.cboEmployee.TabIndex = 480
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 118)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(248, 15)
        Me.lblPeriod.TabIndex = 476
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue5
        '
        Me.cboFieldValue5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue5.DropDownWidth = 250
        Me.cboFieldValue5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue5.FormattingEnabled = True
        Me.cboFieldValue5.Location = New System.Drawing.Point(615, 94)
        Me.cboFieldValue5.Name = "cboFieldValue5"
        Me.cboFieldValue5.Size = New System.Drawing.Size(248, 21)
        Me.cboFieldValue5.TabIndex = 467
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(615, 136)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(248, 21)
        Me.cboStatus.TabIndex = 71
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(12, 136)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(248, 21)
        Me.cboPeriod.TabIndex = 477
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(615, 118)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(248, 15)
        Me.lblStatus.TabIndex = 72
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblField5
        '
        Me.objlblField5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField5.Location = New System.Drawing.Point(615, 76)
        Me.objlblField5.Name = "objlblField5"
        Me.objlblField5.Size = New System.Drawing.Size(248, 15)
        Me.objlblField5.TabIndex = 468
        Me.objlblField5.Text = "#Caption"
        Me.objlblField5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue4
        '
        Me.cboFieldValue4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue4.DropDownWidth = 250
        Me.cboFieldValue4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue4.FormattingEnabled = True
        Me.cboFieldValue4.Location = New System.Drawing.Point(615, 52)
        Me.cboFieldValue4.Name = "cboFieldValue4"
        Me.cboFieldValue4.Size = New System.Drawing.Size(248, 21)
        Me.cboFieldValue4.TabIndex = 464
        '
        'objbtnSearchField5
        '
        Me.objbtnSearchField5.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField5.BorderSelected = False
        Me.objbtnSearchField5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField5.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField5.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField5.Location = New System.Drawing.Point(869, 94)
        Me.objbtnSearchField5.Name = "objbtnSearchField5"
        Me.objbtnSearchField5.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField5.TabIndex = 469
        '
        'objbtnSearchField4
        '
        Me.objbtnSearchField4.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField4.BorderSelected = False
        Me.objbtnSearchField4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField4.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField4.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField4.Location = New System.Drawing.Point(869, 52)
        Me.objbtnSearchField4.Name = "objbtnSearchField4"
        Me.objbtnSearchField4.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField4.TabIndex = 466
        '
        'objbtnSearchField3
        '
        Me.objbtnSearchField3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField3.BorderSelected = False
        Me.objbtnSearchField3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField3.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField3.Location = New System.Drawing.Point(568, 136)
        Me.objbtnSearchField3.Name = "objbtnSearchField3"
        Me.objbtnSearchField3.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField3.TabIndex = 466
        '
        'objlblField4
        '
        Me.objlblField4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField4.Location = New System.Drawing.Point(615, 33)
        Me.objlblField4.Name = "objlblField4"
        Me.objlblField4.Size = New System.Drawing.Size(248, 15)
        Me.objlblField4.TabIndex = 465
        Me.objlblField4.Text = "#Caption"
        Me.objlblField4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField2
        '
        Me.objbtnSearchField2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField2.BorderSelected = False
        Me.objbtnSearchField2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField2.Location = New System.Drawing.Point(568, 94)
        Me.objbtnSearchField2.Name = "objbtnSearchField2"
        Me.objbtnSearchField2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField2.TabIndex = 466
        '
        'objlblField3
        '
        Me.objlblField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField3.Location = New System.Drawing.Point(314, 118)
        Me.objlblField3.Name = "objlblField3"
        Me.objlblField3.Size = New System.Drawing.Size(248, 15)
        Me.objlblField3.TabIndex = 465
        Me.objlblField3.Text = "#Caption"
        Me.objlblField3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblField1
        '
        Me.objlblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField1.Location = New System.Drawing.Point(314, 33)
        Me.objlblField1.Name = "objlblField1"
        Me.objlblField1.Size = New System.Drawing.Size(248, 15)
        Me.objlblField1.TabIndex = 465
        Me.objlblField1.Text = "#Caption"
        Me.objlblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue3
        '
        Me.cboFieldValue3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue3.DropDownWidth = 250
        Me.cboFieldValue3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue3.FormattingEnabled = True
        Me.cboFieldValue3.Location = New System.Drawing.Point(314, 136)
        Me.cboFieldValue3.Name = "cboFieldValue3"
        Me.cboFieldValue3.Size = New System.Drawing.Size(248, 21)
        Me.cboFieldValue3.TabIndex = 464
        '
        'cboFieldValue2
        '
        Me.cboFieldValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue2.DropDownWidth = 250
        Me.cboFieldValue2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue2.FormattingEnabled = True
        Me.cboFieldValue2.Location = New System.Drawing.Point(314, 94)
        Me.cboFieldValue2.Name = "cboFieldValue2"
        Me.cboFieldValue2.Size = New System.Drawing.Size(248, 21)
        Me.cboFieldValue2.TabIndex = 464
        '
        'objlblField2
        '
        Me.objlblField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField2.Location = New System.Drawing.Point(314, 76)
        Me.objlblField2.Name = "objlblField2"
        Me.objlblField2.Size = New System.Drawing.Size(248, 15)
        Me.objlblField2.TabIndex = 465
        Me.objlblField2.Text = "#Caption"
        Me.objlblField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField1
        '
        Me.objbtnSearchField1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField1.BorderSelected = False
        Me.objbtnSearchField1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField1.Location = New System.Drawing.Point(568, 52)
        Me.objbtnSearchField1.Name = "objbtnSearchField1"
        Me.objbtnSearchField1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField1.TabIndex = 466
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(892, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'cboFieldValue1
        '
        Me.cboFieldValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue1.DropDownWidth = 250
        Me.cboFieldValue1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue1.FormattingEnabled = True
        Me.cboFieldValue1.Location = New System.Drawing.Point(314, 52)
        Me.cboFieldValue1.Name = "cboFieldValue1"
        Me.cboFieldValue1.Size = New System.Drawing.Size(248, 21)
        Me.cboFieldValue1.TabIndex = 464
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(869, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(919, 318)
        Me.dgvData.TabIndex = 5
        '
        'objdgcolhAdd
        '
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSubmitForGoalAccomplished)
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.lblCaption1)
        Me.objFooter.Controls.Add(Me.lblCaption2)
        Me.objFooter.Controls.Add(Me.objpnl2)
        Me.objFooter.Controls.Add(Me.objpnl1)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 488)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(919, 55)
        Me.objFooter.TabIndex = 5
        '
        'btnSubmitForGoalAccomplished
        '
        Me.btnSubmitForGoalAccomplished.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSubmitForGoalAccomplished.BackColor = System.Drawing.Color.White
        Me.btnSubmitForGoalAccomplished.BackgroundImage = CType(resources.GetObject("btnSubmitForGoalAccomplished.BackgroundImage"), System.Drawing.Image)
        Me.btnSubmitForGoalAccomplished.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSubmitForGoalAccomplished.BorderColor = System.Drawing.Color.Empty
        Me.btnSubmitForGoalAccomplished.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSubmitForGoalAccomplished.FlatAppearance.BorderSize = 0
        Me.btnSubmitForGoalAccomplished.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmitForGoalAccomplished.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmitForGoalAccomplished.ForeColor = System.Drawing.Color.Black
        Me.btnSubmitForGoalAccomplished.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSubmitForGoalAccomplished.GradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForGoalAccomplished.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForGoalAccomplished.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForGoalAccomplished.Location = New System.Drawing.Point(618, 13)
        Me.btnSubmitForGoalAccomplished.Name = "btnSubmitForGoalAccomplished"
        Me.btnSubmitForGoalAccomplished.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSubmitForGoalAccomplished.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSubmitForGoalAccomplished.Size = New System.Drawing.Size(186, 30)
        Me.btnSubmitForGoalAccomplished.TabIndex = 136
        Me.btnSubmitForGoalAccomplished.Text = "&Submit For Goal Accomplished"
        Me.btnSubmitForGoalAccomplished.UseVisualStyleBackColor = True
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(591, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(213, 13)
        Me.objlblTotalWeight.TabIndex = 135
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(97, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 134
        Me.btnOperations.Text = "Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSubmitApproval, Me.mnuApproveSubmitted, Me.mnuFinalSave, Me.mnuUnlockFinalSave, Me.objSep2, Me.mnuGlobalOperation, Me.mnuGlobalAssign, Me.mnuVoidGoals, Me.mnuUpdatePercentage, Me.mnuGetFileFormat, Me.mnuImportBSC_Planning, Me.objSep1, Me.mnuPrintBSCForm, Me.mnuAppRejGoalAccomplished, Me.mnuImportGoals, Me.mnuGlobalVoidGoals, Me.mnuUnlockEmployee})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(244, 368)
        '
        'mnuSubmitApproval
        '
        Me.mnuSubmitApproval.Name = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Size = New System.Drawing.Size(243, 22)
        Me.mnuSubmitApproval.Tag = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Text = "S&ubmit for Approval"
        Me.mnuSubmitApproval.Visible = False
        '
        'mnuApproveSubmitted
        '
        Me.mnuApproveSubmitted.Name = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Size = New System.Drawing.Size(243, 22)
        Me.mnuApproveSubmitted.Tag = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Text = "&Approve/Reject BSC Plan"
        Me.mnuApproveSubmitted.Visible = False
        '
        'mnuFinalSave
        '
        Me.mnuFinalSave.Name = "mnuFinalSave"
        Me.mnuFinalSave.Size = New System.Drawing.Size(243, 22)
        Me.mnuFinalSave.Tag = "mnuFinalSave"
        Me.mnuFinalSave.Text = "&Final Approved"
        Me.mnuFinalSave.Visible = False
        '
        'mnuUnlockFinalSave
        '
        Me.mnuUnlockFinalSave.Name = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Size = New System.Drawing.Size(243, 22)
        Me.mnuUnlockFinalSave.Tag = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Text = "&Unlock Final Approved"
        Me.mnuUnlockFinalSave.Visible = False
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(240, 6)
        Me.objSep2.Tag = "objSep2"
        Me.objSep2.Visible = False
        '
        'mnuGlobalOperation
        '
        Me.mnuGlobalOperation.Name = "mnuGlobalOperation"
        Me.mnuGlobalOperation.Size = New System.Drawing.Size(243, 22)
        Me.mnuGlobalOperation.Text = "Global Operation"
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(243, 22)
        Me.mnuGlobalAssign.Tag = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Text = "Global Assign Goals"
        '
        'mnuVoidGoals
        '
        Me.mnuVoidGoals.Name = "mnuVoidGoals"
        Me.mnuVoidGoals.Size = New System.Drawing.Size(243, 22)
        Me.mnuVoidGoals.Tag = "mnuVoidGoals"
        Me.mnuVoidGoals.Text = "Void Employee Goals"
        '
        'mnuUpdatePercentage
        '
        Me.mnuUpdatePercentage.Name = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Size = New System.Drawing.Size(243, 22)
        Me.mnuUpdatePercentage.Tag = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Text = "Update Percentage Completed"
        Me.mnuUpdatePercentage.Visible = False
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(243, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get &File Format"
        Me.mnuGetFileFormat.Visible = False
        '
        'mnuImportBSC_Planning
        '
        Me.mnuImportBSC_Planning.Name = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Size = New System.Drawing.Size(243, 22)
        Me.mnuImportBSC_Planning.Tag = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Text = "Import BSC Planning"
        Me.mnuImportBSC_Planning.Visible = False
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(240, 6)
        Me.objSep1.Tag = "objSep1"
        Me.objSep1.Visible = False
        '
        'mnuPrintBSCForm
        '
        Me.mnuPrintBSCForm.Name = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Size = New System.Drawing.Size(243, 22)
        Me.mnuPrintBSCForm.Tag = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Text = "&Print BSC Form"
        Me.mnuPrintBSCForm.Visible = False
        '
        'mnuAppRejGoalAccomplished
        '
        Me.mnuAppRejGoalAccomplished.Name = "mnuAppRejGoalAccomplished"
        Me.mnuAppRejGoalAccomplished.Size = New System.Drawing.Size(243, 22)
        Me.mnuAppRejGoalAccomplished.Tag = "mnuAppRejGoalAccomplished"
        Me.mnuAppRejGoalAccomplished.Text = "App/Rej Goals Accomplishment"
        '
        'mnuImportGoals
        '
        Me.mnuImportGoals.Name = "mnuImportGoals"
        Me.mnuImportGoals.Size = New System.Drawing.Size(243, 22)
        Me.mnuImportGoals.Tag = "mnuImportGoals"
        Me.mnuImportGoals.Text = "Import Goals"
        '
        'mnuGlobalVoidGoals
        '
        Me.mnuGlobalVoidGoals.Name = "mnuGlobalVoidGoals"
        Me.mnuGlobalVoidGoals.Size = New System.Drawing.Size(243, 22)
        Me.mnuGlobalVoidGoals.Tag = "mnuGlobalVoidGoals"
        Me.mnuGlobalVoidGoals.Text = "Global Void Goals"
        '
        'lblCaption1
        '
        Me.lblCaption1.Location = New System.Drawing.Point(166, 10)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption1.TabIndex = 132
        Me.lblCaption1.Text = "Submitted for Approval(s)."
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption2
        '
        Me.lblCaption2.Location = New System.Drawing.Point(166, 31)
        Me.lblCaption2.Name = "lblCaption2"
        Me.lblCaption2.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption2.TabIndex = 133
        Me.lblCaption2.Text = "Finally Approved"
        Me.lblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl2
        '
        Me.objpnl2.BackColor = System.Drawing.Color.Blue
        Me.objpnl2.Location = New System.Drawing.Point(125, 31)
        Me.objpnl2.Name = "objpnl2"
        Me.objpnl2.Size = New System.Drawing.Size(35, 17)
        Me.objpnl2.TabIndex = 131
        '
        'objpnl1
        '
        Me.objpnl1.BackColor = System.Drawing.Color.Green
        Me.objpnl1.Location = New System.Drawing.Point(125, 10)
        Me.objpnl1.Name = "objpnl1"
        Me.objpnl1.Size = New System.Drawing.Size(35, 17)
        Me.objpnl1.TabIndex = 130
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(810, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'mnuUnlockEmployee
        '
        Me.mnuUnlockEmployee.Name = "mnuUnlockEmployee"
        Me.mnuUnlockEmployee.Size = New System.Drawing.Size(243, 22)
        Me.mnuUnlockEmployee.Tag = "mnuUnlockEmployee"
        Me.mnuUnlockEmployee.Text = "Unlock Employee"
        '
        'frmEmpFieldsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 543)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpFieldsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Goals List"
        Me.pnlMain.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue5 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField5 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue4 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchField5 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchField4 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchField3 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField4 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField3 As System.Windows.Forms.Label
    Friend WithEvents objlblField1 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFieldValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField2 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField1 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboFieldValue1 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchEmp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption2 As System.Windows.Forms.Label
    Friend WithEvents objpnl2 As System.Windows.Forms.Panel
    Friend WithEvents objpnl1 As System.Windows.Forms.Panel
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSubmitApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproveSubmitted As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlockFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportBSC_Planning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPrintBSCForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdatePercentage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVoidGoals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents mnuAppRejGoalAccomplished As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalOperation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSubmitForGoalAccomplished As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchPerspective As eZee.Common.eZeeGradientButton
    Friend WithEvents mnuImportGoals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalVoidGoals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlockEmployee As System.Windows.Forms.ToolStripMenuItem
End Class

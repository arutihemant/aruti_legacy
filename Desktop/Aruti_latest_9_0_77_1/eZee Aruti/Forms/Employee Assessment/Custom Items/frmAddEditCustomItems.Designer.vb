﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddEditCustomItems
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddEditCustomItems))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbCustomItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkCompletedTraining = New System.Windows.Forms.CheckBox
        Me.radReviewer = New System.Windows.Forms.RadioButton
        Me.radAssessor = New System.Windows.Forms.RadioButton
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.elItemAccess = New eZee.Common.eZeeLine
        Me.cboItemType = New System.Windows.Forms.ComboBox
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblItemType = New System.Windows.Forms.Label
        Me.lblCustomItem = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchHeader = New eZee.Common.eZeeGradientButton
        Me.cboCHeader = New System.Windows.Forms.ComboBox
        Me.lblCustomHeader = New System.Windows.Forms.Label
        Me.pnlIsDefualtEntry = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblNotes = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbCustomItems.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbCustomItems)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(572, 314)
        Me.pnlMain.TabIndex = 0
        '
        'gbCustomItems
        '
        Me.gbCustomItems.BorderColor = System.Drawing.Color.Black
        Me.gbCustomItems.Checked = False
        Me.gbCustomItems.CollapseAllExceptThis = False
        Me.gbCustomItems.CollapsedHoverImage = Nothing
        Me.gbCustomItems.CollapsedNormalImage = Nothing
        Me.gbCustomItems.CollapsedPressedImage = Nothing
        Me.gbCustomItems.CollapseOnLoad = False
        Me.gbCustomItems.Controls.Add(Me.chkCompletedTraining)
        Me.gbCustomItems.Controls.Add(Me.radReviewer)
        Me.gbCustomItems.Controls.Add(Me.radAssessor)
        Me.gbCustomItems.Controls.Add(Me.radEmployee)
        Me.gbCustomItems.Controls.Add(Me.elItemAccess)
        Me.gbCustomItems.Controls.Add(Me.cboItemType)
        Me.gbCustomItems.Controls.Add(Me.cboMode)
        Me.gbCustomItems.Controls.Add(Me.lblItemType)
        Me.gbCustomItems.Controls.Add(Me.lblCustomItem)
        Me.gbCustomItems.Controls.Add(Me.txtName)
        Me.gbCustomItems.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbCustomItems.Controls.Add(Me.lblPeriod)
        Me.gbCustomItems.Controls.Add(Me.cboPeriod)
        Me.gbCustomItems.Controls.Add(Me.objbtnSearchHeader)
        Me.gbCustomItems.Controls.Add(Me.cboCHeader)
        Me.gbCustomItems.Controls.Add(Me.lblCustomHeader)
        Me.gbCustomItems.Controls.Add(Me.pnlIsDefualtEntry)
        Me.gbCustomItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCustomItems.ExpandedHoverImage = Nothing
        Me.gbCustomItems.ExpandedNormalImage = Nothing
        Me.gbCustomItems.ExpandedPressedImage = Nothing
        Me.gbCustomItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCustomItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCustomItems.HeaderHeight = 25
        Me.gbCustomItems.HeaderMessage = ""
        Me.gbCustomItems.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCustomItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCustomItems.HeightOnCollapse = 0
        Me.gbCustomItems.LeftTextSpace = 0
        Me.gbCustomItems.Location = New System.Drawing.Point(0, 0)
        Me.gbCustomItems.Name = "gbCustomItems"
        Me.gbCustomItems.OpenHeight = 119
        Me.gbCustomItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCustomItems.ShowBorder = True
        Me.gbCustomItems.ShowCheckBox = False
        Me.gbCustomItems.ShowCollapseButton = False
        Me.gbCustomItems.ShowDefaultBorderColor = True
        Me.gbCustomItems.ShowDownButton = False
        Me.gbCustomItems.ShowHeader = True
        Me.gbCustomItems.Size = New System.Drawing.Size(572, 259)
        Me.gbCustomItems.TabIndex = 4
        Me.gbCustomItems.Temp = 0
        Me.gbCustomItems.Text = "Custom Items"
        Me.gbCustomItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCompletedTraining
        '
        Me.chkCompletedTraining.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCompletedTraining.Location = New System.Drawing.Point(365, 131)
        Me.chkCompletedTraining.Name = "chkCompletedTraining"
        Me.chkCompletedTraining.Size = New System.Drawing.Size(168, 17)
        Me.chkCompletedTraining.TabIndex = 0
        Me.chkCompletedTraining.Text = "Fill Only Completed Training"
        Me.chkCompletedTraining.UseVisualStyleBackColor = True
        '
        'radReviewer
        '
        Me.radReviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radReviewer.Location = New System.Drawing.Point(272, 130)
        Me.radReviewer.Name = "radReviewer"
        Me.radReviewer.Size = New System.Drawing.Size(87, 17)
        Me.radReviewer.TabIndex = 261
        Me.radReviewer.TabStop = True
        Me.radReviewer.Text = "Reviewer"
        Me.radReviewer.UseVisualStyleBackColor = True
        '
        'radAssessor
        '
        Me.radAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAssessor.Location = New System.Drawing.Point(178, 130)
        Me.radAssessor.Name = "radAssessor"
        Me.radAssessor.Size = New System.Drawing.Size(88, 17)
        Me.radAssessor.TabIndex = 260
        Me.radAssessor.TabStop = True
        Me.radAssessor.Text = "Assessor"
        Me.radAssessor.UseVisualStyleBackColor = True
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(103, 130)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(69, 17)
        Me.radEmployee.TabIndex = 259
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "All"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'elItemAccess
        '
        Me.elItemAccess.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elItemAccess.Location = New System.Drawing.Point(12, 112)
        Me.elItemAccess.Name = "elItemAccess"
        Me.elItemAccess.Size = New System.Drawing.Size(521, 16)
        Me.elItemAccess.TabIndex = 254
        Me.elItemAccess.Text = "Item Applicable to"
        Me.elItemAccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboItemType
        '
        Me.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboItemType.DropDownWidth = 450
        Me.cboItemType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboItemType.FormattingEnabled = True
        Me.cboItemType.Location = New System.Drawing.Point(103, 87)
        Me.cboItemType.Name = "cboItemType"
        Me.cboItemType.Size = New System.Drawing.Size(187, 21)
        Me.cboItemType.TabIndex = 252
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.DropDownWidth = 450
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(296, 87)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(237, 21)
        Me.cboMode.TabIndex = 250
        '
        'lblItemType
        '
        Me.lblItemType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemType.Location = New System.Drawing.Point(12, 89)
        Me.lblItemType.Name = "lblItemType"
        Me.lblItemType.Size = New System.Drawing.Size(85, 17)
        Me.lblItemType.TabIndex = 248
        Me.lblItemType.Text = "Item Type"
        Me.lblItemType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCustomItem
        '
        Me.lblCustomItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomItem.Location = New System.Drawing.Point(12, 156)
        Me.lblCustomItem.Name = "lblCustomItem"
        Me.lblCustomItem.Size = New System.Drawing.Size(85, 17)
        Me.lblCustomItem.TabIndex = 243
        Me.lblCustomItem.Text = "Custom Item"
        Me.lblCustomItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39)}
        Me.txtName.Location = New System.Drawing.Point(103, 156)
        Me.txtName.Multiline = True
        Me.txtName.Name = "txtName"
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtName.Size = New System.Drawing.Size(430, 95)
        Me.txtName.TabIndex = 242
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(296, 33)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 238
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(85, 17)
        Me.lblPeriod.TabIndex = 236
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 450
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(103, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(187, 21)
        Me.cboPeriod.TabIndex = 235
        '
        'objbtnSearchHeader
        '
        Me.objbtnSearchHeader.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchHeader.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeader.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchHeader.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchHeader.BorderSelected = False
        Me.objbtnSearchHeader.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchHeader.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchHeader.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchHeader.Location = New System.Drawing.Point(539, 60)
        Me.objbtnSearchHeader.Name = "objbtnSearchHeader"
        Me.objbtnSearchHeader.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchHeader.TabIndex = 234
        '
        'cboCHeader
        '
        Me.cboCHeader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCHeader.DropDownWidth = 800
        Me.cboCHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCHeader.FormattingEnabled = True
        Me.cboCHeader.Location = New System.Drawing.Point(103, 60)
        Me.cboCHeader.Name = "cboCHeader"
        Me.cboCHeader.Size = New System.Drawing.Size(430, 21)
        Me.cboCHeader.TabIndex = 231
        '
        'lblCustomHeader
        '
        Me.lblCustomHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomHeader.Location = New System.Drawing.Point(12, 62)
        Me.lblCustomHeader.Name = "lblCustomHeader"
        Me.lblCustomHeader.Size = New System.Drawing.Size(85, 17)
        Me.lblCustomHeader.TabIndex = 232
        Me.lblCustomHeader.Text = "Custom Header"
        Me.lblCustomHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlIsDefualtEntry
        '
        Me.pnlIsDefualtEntry.Location = New System.Drawing.Point(2, 26)
        Me.pnlIsDefualtEntry.Name = "pnlIsDefualtEntry"
        Me.pnlIsDefualtEntry.Size = New System.Drawing.Size(567, 126)
        Me.pnlIsDefualtEntry.TabIndex = 263
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblNotes)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 259)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(572, 55)
        Me.objFooter.TabIndex = 3
        '
        'objlblNotes
        '
        Me.objlblNotes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNotes.ForeColor = System.Drawing.Color.Maroon
        Me.objlblNotes.Location = New System.Drawing.Point(12, 13)
        Me.objlblNotes.Name = "objlblNotes"
        Me.objlblNotes.Size = New System.Drawing.Size(342, 30)
        Me.objlblNotes.TabIndex = 9
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(360, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(463, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAddEditCustomItems
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 314)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddEditCustomItems"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Custom Items"
        Me.pnlMain.ResumeLayout(False)
        Me.gbCustomItems.ResumeLayout(False)
        Me.gbCustomItems.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbCustomItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCustomItem As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchHeader As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCHeader As System.Windows.Forms.ComboBox
    Friend WithEvents lblCustomHeader As System.Windows.Forms.Label
    Friend WithEvents lblItemType As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents cboItemType As System.Windows.Forms.ComboBox
    Friend WithEvents elItemAccess As eZee.Common.eZeeLine
    Friend WithEvents radReviewer As System.Windows.Forms.RadioButton
    Friend WithEvents radAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents objlblNotes As System.Windows.Forms.Label
    Friend WithEvents pnlIsDefualtEntry As System.Windows.Forms.Panel
    Friend WithEvents chkCompletedTraining As System.Windows.Forms.CheckBox
End Class

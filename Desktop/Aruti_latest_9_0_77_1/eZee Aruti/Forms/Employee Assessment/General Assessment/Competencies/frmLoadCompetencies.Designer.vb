﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoadCompetencies
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoadCompetencies))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objSPC1 = New System.Windows.Forms.SplitContainer
        Me.gbAssignment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchGroup = New eZee.Common.eZeeGradientButton
        Me.lblComptenceGroup = New System.Windows.Forms.Label
        Me.cboComptenceGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchScore = New eZee.Common.eZeeGradientButton
        Me.cboScore = New System.Windows.Forms.ComboBox
        Me.lblScoreScale = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhItems = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objSearchHeading = New eZee.Common.eZeeHeading
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.radGroup = New System.Windows.Forms.RadioButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.radItems = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblSearchBy = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlnkData = New System.Windows.Forms.LinkLabel
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuExpandGroups = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCollapseGroups = New System.Windows.Forms.ToolStripMenuItem
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        Me.objSPC1.Panel1.SuspendLayout()
        Me.objSPC1.Panel2.SuspendLayout()
        Me.objSPC1.SuspendLayout()
        Me.gbAssignment.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objSearchHeading.SuspendLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objSPC1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(944, 517)
        Me.Panel1.TabIndex = 0
        '
        'objSPC1
        '
        Me.objSPC1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSPC1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSPC1.IsSplitterFixed = True
        Me.objSPC1.Location = New System.Drawing.Point(0, 0)
        Me.objSPC1.Name = "objSPC1"
        Me.objSPC1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSPC1.Panel1
        '
        Me.objSPC1.Panel1.Controls.Add(Me.gbAssignment)
        '
        'objSPC1.Panel2
        '
        Me.objSPC1.Panel2.Controls.Add(Me.objchkAll)
        Me.objSPC1.Panel2.Controls.Add(Me.dgvData)
        Me.objSPC1.Panel2.Controls.Add(Me.objSearchHeading)
        Me.objSPC1.Size = New System.Drawing.Size(944, 517)
        Me.objSPC1.SplitterDistance = 74
        Me.objSPC1.SplitterWidth = 2
        Me.objSPC1.TabIndex = 0
        '
        'gbAssignment
        '
        Me.gbAssignment.BorderColor = System.Drawing.Color.Black
        Me.gbAssignment.Checked = False
        Me.gbAssignment.CollapseAllExceptThis = False
        Me.gbAssignment.CollapsedHoverImage = Nothing
        Me.gbAssignment.CollapsedNormalImage = Nothing
        Me.gbAssignment.CollapsedPressedImage = Nothing
        Me.gbAssignment.CollapseOnLoad = False
        Me.gbAssignment.Controls.Add(Me.cboComptenceGroup)
        Me.gbAssignment.Controls.Add(Me.objbtnSearchGroup)
        Me.gbAssignment.Controls.Add(Me.lblComptenceGroup)
        Me.gbAssignment.Controls.Add(Me.objbtnSearchScore)
        Me.gbAssignment.Controls.Add(Me.cboScore)
        Me.gbAssignment.Controls.Add(Me.lblScoreScale)
        Me.gbAssignment.Controls.Add(Me.lblPeriod)
        Me.gbAssignment.Controls.Add(Me.cboPeriod)
        Me.gbAssignment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssignment.ExpandedHoverImage = Nothing
        Me.gbAssignment.ExpandedNormalImage = Nothing
        Me.gbAssignment.ExpandedPressedImage = Nothing
        Me.gbAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignment.HeaderHeight = 25
        Me.gbAssignment.HeaderMessage = ""
        Me.gbAssignment.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssignment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignment.HeightOnCollapse = 0
        Me.gbAssignment.LeftTextSpace = 0
        Me.gbAssignment.Location = New System.Drawing.Point(0, 0)
        Me.gbAssignment.Name = "gbAssignment"
        Me.gbAssignment.OpenHeight = 300
        Me.gbAssignment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignment.ShowBorder = True
        Me.gbAssignment.ShowCheckBox = False
        Me.gbAssignment.ShowCollapseButton = False
        Me.gbAssignment.ShowDefaultBorderColor = True
        Me.gbAssignment.ShowDownButton = False
        Me.gbAssignment.ShowHeader = True
        Me.gbAssignment.Size = New System.Drawing.Size(944, 74)
        Me.gbAssignment.TabIndex = 0
        Me.gbAssignment.Temp = 0
        Me.gbAssignment.Text = "Assignment Information"
        Me.gbAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGroup
        '
        Me.objbtnSearchGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGroup.BorderSelected = False
        Me.objbtnSearchGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGroup.Location = New System.Drawing.Point(911, 39)
        Me.objbtnSearchGroup.Name = "objbtnSearchGroup"
        Me.objbtnSearchGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGroup.TabIndex = 358
        '
        'lblComptenceGroup
        '
        Me.lblComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblComptenceGroup.Location = New System.Drawing.Point(600, 41)
        Me.lblComptenceGroup.Name = "lblComptenceGroup"
        Me.lblComptenceGroup.Size = New System.Drawing.Size(108, 17)
        Me.lblComptenceGroup.TabIndex = 357
        Me.lblComptenceGroup.Text = "Competence Group"
        Me.lblComptenceGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboComptenceGroup
        '
        Me.cboComptenceGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboComptenceGroup.DropDownWidth = 200
        Me.cboComptenceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboComptenceGroup.FormattingEnabled = True
        Me.cboComptenceGroup.Location = New System.Drawing.Point(705, 39)
        Me.cboComptenceGroup.Name = "cboComptenceGroup"
        Me.cboComptenceGroup.Size = New System.Drawing.Size(203, 21)
        Me.cboComptenceGroup.TabIndex = 356
        '
        'objbtnSearchScore
        '
        Me.objbtnSearchScore.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScore.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScore.BorderSelected = False
        Me.objbtnSearchScore.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScore.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScore.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScore.Location = New System.Drawing.Point(573, 39)
        Me.objbtnSearchScore.Name = "objbtnSearchScore"
        Me.objbtnSearchScore.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchScore.TabIndex = 354
        '
        'cboScore
        '
        Me.cboScore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScore.FormattingEnabled = True
        Me.cboScore.Location = New System.Drawing.Point(378, 39)
        Me.cboScore.Name = "cboScore"
        Me.cboScore.Size = New System.Drawing.Size(189, 21)
        Me.cboScore.TabIndex = 352
        '
        'lblScoreScale
        '
        Me.lblScoreScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScoreScale.Location = New System.Drawing.Point(264, 41)
        Me.lblScoreScale.Name = "lblScoreScale"
        Me.lblScoreScale.Size = New System.Drawing.Size(108, 17)
        Me.lblScoreScale.TabIndex = 353
        Me.lblScoreScale.Text = "Score/Scale Group"
        Me.lblScoreScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 41)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(92, 17)
        Me.lblPeriod.TabIndex = 350
        Me.lblPeriod.Text = "Period to Assign"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(110, 39)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(148, 21)
        Me.cboPeriod.TabIndex = 351
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(32, 35)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 2
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhAllCheck, Me.dgcolhItems, Me.objdgcolhIsGrp, Me.objdgcolhGrpName})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 29)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(944, 412)
        Me.dgvData.TabIndex = 1
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhAllCheck
        '
        Me.objdgcolhAllCheck.HeaderText = ""
        Me.objdgcolhAllCheck.Name = "objdgcolhAllCheck"
        Me.objdgcolhAllCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAllCheck.Width = 25
        '
        'dgcolhItems
        '
        Me.dgcolhItems.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhItems.HeaderText = "Competencies"
        Me.dgcolhItems.Name = "dgcolhItems"
        Me.dgcolhItems.ReadOnly = True
        Me.dgcolhItems.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpName
        '
        Me.objdgcolhGrpName.HeaderText = "objdgcolhGrpName"
        Me.objdgcolhGrpName.Name = "objdgcolhGrpName"
        Me.objdgcolhGrpName.Visible = False
        '
        'objSearchHeading
        '
        Me.objSearchHeading.BorderColor = System.Drawing.Color.Black
        Me.objSearchHeading.Controls.Add(Me.txtSearch)
        Me.objSearchHeading.Controls.Add(Me.radGroup)
        Me.objSearchHeading.Controls.Add(Me.objbtnSearch)
        Me.objSearchHeading.Controls.Add(Me.radItems)
        Me.objSearchHeading.Controls.Add(Me.objbtnReset)
        Me.objSearchHeading.Controls.Add(Me.lblSearchBy)
        Me.objSearchHeading.Dock = System.Windows.Forms.DockStyle.Top
        Me.objSearchHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSearchHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objSearchHeading.Location = New System.Drawing.Point(0, 0)
        Me.objSearchHeading.Name = "objSearchHeading"
        Me.objSearchHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objSearchHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objSearchHeading.ShowDefaultBorderColor = True
        Me.objSearchHeading.Size = New System.Drawing.Size(944, 29)
        Me.objSearchHeading.TabIndex = 11
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(365, 4)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(523, 21)
        Me.txtSearch.TabIndex = 354
        '
        'radGroup
        '
        Me.radGroup.BackColor = System.Drawing.Color.Transparent
        Me.radGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radGroup.Location = New System.Drawing.Point(131, 6)
        Me.radGroup.Name = "radGroup"
        Me.radGroup.Size = New System.Drawing.Size(104, 17)
        Me.radGroup.TabIndex = 352
        Me.radGroup.TabStop = True
        Me.radGroup.Text = "Category"
        Me.radGroup.UseVisualStyleBackColor = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(894, 3)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 9
        Me.objbtnSearch.TabStop = False
        '
        'radItems
        '
        Me.radItems.BackColor = System.Drawing.Color.Transparent
        Me.radItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radItems.Location = New System.Drawing.Point(241, 6)
        Me.radItems.Name = "radItems"
        Me.radItems.Size = New System.Drawing.Size(118, 17)
        Me.radItems.TabIndex = 353
        Me.radItems.TabStop = True
        Me.radItems.Text = "Competencies"
        Me.radItems.UseVisualStyleBackColor = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(917, 3)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 10
        Me.objbtnReset.TabStop = False
        '
        'lblSearchBy
        '
        Me.lblSearchBy.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchBy.Location = New System.Drawing.Point(12, 6)
        Me.lblSearchBy.Name = "lblSearchBy"
        Me.lblSearchBy.Size = New System.Drawing.Size(112, 17)
        Me.lblSearchBy.TabIndex = 351
        Me.lblSearchBy.Text = "Search By"
        Me.lblSearchBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlnkData)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnAssign)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 55)
        Me.objFooter.TabIndex = 7
        '
        'objlnkData
        '
        Me.objlnkData.AutoSize = True
        Me.objlnkData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkData.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkData.Location = New System.Drawing.Point(373, 22)
        Me.objlnkData.Name = "objlnkData"
        Me.objlnkData.Size = New System.Drawing.Size(0, 13)
        Me.objlnkData.TabIndex = 9
        Me.objlnkData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objlnkData.Visible = False
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(13, 12)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(111, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperation.TabIndex = 8
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExpandGroups, Me.mnuCollapseGroups})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(152, 48)
        '
        'mnuExpandGroups
        '
        Me.mnuExpandGroups.Name = "mnuExpandGroups"
        Me.mnuExpandGroups.Size = New System.Drawing.Size(151, 22)
        Me.mnuExpandGroups.Tag = "mnuExpandGroups"
        Me.mnuExpandGroups.Text = "Expand Groups"
        '
        'mnuCollapseGroups
        '
        Me.mnuCollapseGroups.Name = "mnuCollapseGroups"
        Me.mnuCollapseGroups.Size = New System.Drawing.Size(151, 22)
        Me.mnuCollapseGroups.Tag = "mnuCollapseGroups"
        Me.mnuCollapseGroups.Text = "Collapse Groups"
        '
        'btnAssign
        '
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(732, 13)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(97, 30)
        Me.btnAssign.TabIndex = 6
        Me.btnAssign.Text = "&Assign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Competencies"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhGrpName"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhGrpName"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'frmLoadCompetencies
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 572)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoadCompetencies"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Competencies"
        Me.Panel1.ResumeLayout(False)
        Me.objSPC1.Panel1.ResumeLayout(False)
        Me.objSPC1.Panel2.ResumeLayout(False)
        Me.objSPC1.Panel2.PerformLayout()
        Me.objSPC1.ResumeLayout(False)
        Me.gbAssignment.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objSearchHeading.ResumeLayout(False)
        Me.objSearchHeading.PerformLayout()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.cmnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objSPC1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbAssignment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchScore As eZee.Common.eZeeGradientButton
    Friend WithEvents cboScore As System.Windows.Forms.ComboBox
    Friend WithEvents lblScoreScale As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuExpandGroups As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCollapseGroups As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSearchHeading As eZee.Common.eZeeHeading
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents radGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radItems As System.Windows.Forms.RadioButton
    Friend WithEvents lblSearchBy As System.Windows.Forms.Label
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhItems As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlnkData As System.Windows.Forms.LinkLabel
    Friend WithEvents lblComptenceGroup As System.Windows.Forms.Label
    Friend WithEvents cboComptenceGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGroup As eZee.Common.eZeeGradientButton
End Class

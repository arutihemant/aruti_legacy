﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGroup_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroup_AddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.gbAllocation = New System.Windows.Forms.GroupBox
        Me.radTeam = New System.Windows.Forms.RadioButton
        Me.radUnitGroup = New System.Windows.Forms.RadioButton
        Me.radSectionGroup = New System.Windows.Forms.RadioButton
        Me.radDepartmentGroup = New System.Windows.Forms.RadioButton
        Me.radJobs = New System.Windows.Forms.RadioButton
        Me.radSection = New System.Windows.Forms.RadioButton
        Me.radJobGroup = New System.Windows.Forms.RadioButton
        Me.radDepartment = New System.Windows.Forms.RadioButton
        Me.radUnit = New System.Windows.Forms.RadioButton
        Me.radBranch = New System.Windows.Forms.RadioButton
        Me.objlblCaption = New System.Windows.Forms.RichTextBox
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAssessmentGroup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objlblValue = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.elMode = New eZee.Common.eZeeLine
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.radAllocation = New System.Windows.Forms.RadioButton
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.pnlData = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.lblCode = New System.Windows.Forms.Label
        Me.pnlAssessmentGroup = New System.Windows.Forms.Panel
        Me.objFooter.SuspendLayout()
        Me.gbAllocation.SuspendLayout()
        Me.gbAssessmentGroup.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAssessmentGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.gbAllocation)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 271)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(642, 55)
        Me.objFooter.TabIndex = 21
        '
        'gbAllocation
        '
        Me.gbAllocation.BackColor = System.Drawing.SystemColors.Control
        Me.gbAllocation.Controls.Add(Me.radTeam)
        Me.gbAllocation.Controls.Add(Me.objlblCaption)
        Me.gbAllocation.Controls.Add(Me.radUnitGroup)
        Me.gbAllocation.Controls.Add(Me.radSectionGroup)
        Me.gbAllocation.Controls.Add(Me.radDepartmentGroup)
        Me.gbAllocation.Controls.Add(Me.radJobs)
        Me.gbAllocation.Controls.Add(Me.radSection)
        Me.gbAllocation.Controls.Add(Me.radJobGroup)
        Me.gbAllocation.Controls.Add(Me.radDepartment)
        Me.gbAllocation.Controls.Add(Me.radUnit)
        Me.gbAllocation.Controls.Add(Me.radBranch)
        Me.gbAllocation.Enabled = False
        Me.gbAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocation.Location = New System.Drawing.Point(11, 13)
        Me.gbAllocation.Name = "gbAllocation"
        Me.gbAllocation.Size = New System.Drawing.Size(54, 26)
        Me.gbAllocation.TabIndex = 33
        Me.gbAllocation.TabStop = False
        Me.gbAllocation.Text = "Allocations"
        Me.gbAllocation.Visible = False
        '
        'radTeam
        '
        Me.radTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radTeam.Location = New System.Drawing.Point(173, 72)
        Me.radTeam.Name = "radTeam"
        Me.radTeam.Size = New System.Drawing.Size(140, 17)
        Me.radTeam.TabIndex = 8
        Me.radTeam.TabStop = True
        Me.radTeam.Text = "Team"
        Me.radTeam.UseVisualStyleBackColor = True
        '
        'radUnitGroup
        '
        Me.radUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUnitGroup.Location = New System.Drawing.Point(173, 26)
        Me.radUnitGroup.Name = "radUnitGroup"
        Me.radUnitGroup.Size = New System.Drawing.Size(140, 17)
        Me.radUnitGroup.TabIndex = 7
        Me.radUnitGroup.TabStop = True
        Me.radUnitGroup.Text = "Unit Group"
        Me.radUnitGroup.UseVisualStyleBackColor = True
        '
        'radSectionGroup
        '
        Me.radSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSectionGroup.Location = New System.Drawing.Point(27, 95)
        Me.radSectionGroup.Name = "radSectionGroup"
        Me.radSectionGroup.Size = New System.Drawing.Size(140, 17)
        Me.radSectionGroup.TabIndex = 6
        Me.radSectionGroup.TabStop = True
        Me.radSectionGroup.Text = "Section Group"
        Me.radSectionGroup.UseVisualStyleBackColor = True
        '
        'radDepartmentGroup
        '
        Me.radDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepartmentGroup.Location = New System.Drawing.Point(27, 49)
        Me.radDepartmentGroup.Name = "radDepartmentGroup"
        Me.radDepartmentGroup.Size = New System.Drawing.Size(140, 17)
        Me.radDepartmentGroup.TabIndex = 5
        Me.radDepartmentGroup.TabStop = True
        Me.radDepartmentGroup.Text = "Department Group"
        Me.radDepartmentGroup.UseVisualStyleBackColor = True
        '
        'radJobs
        '
        Me.radJobs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobs.Location = New System.Drawing.Point(173, 118)
        Me.radJobs.Name = "radJobs"
        Me.radJobs.Size = New System.Drawing.Size(140, 17)
        Me.radJobs.TabIndex = 4
        Me.radJobs.TabStop = True
        Me.radJobs.Text = "Job"
        Me.radJobs.UseVisualStyleBackColor = True
        '
        'radSection
        '
        Me.radSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSection.Location = New System.Drawing.Point(27, 118)
        Me.radSection.Name = "radSection"
        Me.radSection.Size = New System.Drawing.Size(140, 17)
        Me.radSection.TabIndex = 3
        Me.radSection.TabStop = True
        Me.radSection.Text = "Section"
        Me.radSection.UseVisualStyleBackColor = True
        '
        'radJobGroup
        '
        Me.radJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobGroup.Location = New System.Drawing.Point(173, 95)
        Me.radJobGroup.Name = "radJobGroup"
        Me.radJobGroup.Size = New System.Drawing.Size(140, 17)
        Me.radJobGroup.TabIndex = 2
        Me.radJobGroup.TabStop = True
        Me.radJobGroup.Text = "Job Group"
        Me.radJobGroup.UseVisualStyleBackColor = True
        '
        'radDepartment
        '
        Me.radDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepartment.Location = New System.Drawing.Point(27, 72)
        Me.radDepartment.Name = "radDepartment"
        Me.radDepartment.Size = New System.Drawing.Size(140, 17)
        Me.radDepartment.TabIndex = 1
        Me.radDepartment.TabStop = True
        Me.radDepartment.Text = "Department"
        Me.radDepartment.UseVisualStyleBackColor = True
        '
        'radUnit
        '
        Me.radUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUnit.Location = New System.Drawing.Point(173, 49)
        Me.radUnit.Name = "radUnit"
        Me.radUnit.Size = New System.Drawing.Size(140, 17)
        Me.radUnit.TabIndex = 0
        Me.radUnit.TabStop = True
        Me.radUnit.Text = "Unit"
        Me.radUnit.UseVisualStyleBackColor = True
        '
        'radBranch
        '
        Me.radBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radBranch.Location = New System.Drawing.Point(27, 26)
        Me.radBranch.Name = "radBranch"
        Me.radBranch.Size = New System.Drawing.Size(140, 17)
        Me.radBranch.TabIndex = 0
        Me.radBranch.TabStop = True
        Me.radBranch.Text = "Branch"
        Me.radBranch.UseVisualStyleBackColor = True
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.SystemColors.Control
        Me.objlblCaption.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.objlblCaption.DetectUrls = False
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Red
        Me.objlblCaption.Location = New System.Drawing.Point(195, 9)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.ReadOnly = True
        Me.objlblCaption.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.objlblCaption.ShortcutsEnabled = False
        Me.objlblCaption.Size = New System.Drawing.Size(41, 12)
        Me.objlblCaption.TabIndex = 12
        Me.objlblCaption.Text = ""
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(434, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(537, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 11
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbAssessmentGroup
        '
        Me.gbAssessmentGroup.BorderColor = System.Drawing.Color.Black
        Me.gbAssessmentGroup.Checked = False
        Me.gbAssessmentGroup.CollapseAllExceptThis = False
        Me.gbAssessmentGroup.CollapsedHoverImage = Nothing
        Me.gbAssessmentGroup.CollapsedNormalImage = Nothing
        Me.gbAssessmentGroup.CollapsedPressedImage = Nothing
        Me.gbAssessmentGroup.CollapseOnLoad = False
        Me.gbAssessmentGroup.Controls.Add(Me.lnkAllocation)
        Me.gbAssessmentGroup.Controls.Add(Me.objAlloacationReset)
        Me.gbAssessmentGroup.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbAssessmentGroup.Controls.Add(Me.objlblValue)
        Me.gbAssessmentGroup.Controls.Add(Me.cboMode)
        Me.gbAssessmentGroup.Controls.Add(Me.elMode)
        Me.gbAssessmentGroup.Controls.Add(Me.radEmployee)
        Me.gbAssessmentGroup.Controls.Add(Me.radAllocation)
        Me.gbAssessmentGroup.Controls.Add(Me.lblWeight)
        Me.gbAssessmentGroup.Controls.Add(Me.txtWeight)
        Me.gbAssessmentGroup.Controls.Add(Me.pnlData)
        Me.gbAssessmentGroup.Controls.Add(Me.txtSearch)
        Me.gbAssessmentGroup.Controls.Add(Me.objLine1)
        Me.gbAssessmentGroup.Controls.Add(Me.txtDescription)
        Me.gbAssessmentGroup.Controls.Add(Me.txtName)
        Me.gbAssessmentGroup.Controls.Add(Me.txtCode)
        Me.gbAssessmentGroup.Controls.Add(Me.lblDescription)
        Me.gbAssessmentGroup.Controls.Add(Me.lblName)
        Me.gbAssessmentGroup.Controls.Add(Me.lblCode)
        Me.gbAssessmentGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssessmentGroup.ExpandedHoverImage = Nothing
        Me.gbAssessmentGroup.ExpandedNormalImage = Nothing
        Me.gbAssessmentGroup.ExpandedPressedImage = Nothing
        Me.gbAssessmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssessmentGroup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssessmentGroup.HeaderHeight = 25
        Me.gbAssessmentGroup.HeaderMessage = ""
        Me.gbAssessmentGroup.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAssessmentGroup.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssessmentGroup.HeightOnCollapse = 0
        Me.gbAssessmentGroup.LeftTextSpace = 0
        Me.gbAssessmentGroup.Location = New System.Drawing.Point(0, 0)
        Me.gbAssessmentGroup.Name = "gbAssessmentGroup"
        Me.gbAssessmentGroup.OpenHeight = 168
        Me.gbAssessmentGroup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssessmentGroup.ShowBorder = True
        Me.gbAssessmentGroup.ShowCheckBox = False
        Me.gbAssessmentGroup.ShowCollapseButton = False
        Me.gbAssessmentGroup.ShowDefaultBorderColor = True
        Me.gbAssessmentGroup.ShowDownButton = False
        Me.gbAssessmentGroup.ShowHeader = True
        Me.gbAssessmentGroup.Size = New System.Drawing.Size(642, 271)
        Me.gbAssessmentGroup.TabIndex = 2
        Me.gbAssessmentGroup.Temp = 0
        Me.gbAssessmentGroup.Text = "Assessment Group Info"
        Me.gbAssessmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(244, 36)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 239
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(328, 33)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 240
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(328, 78)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 235
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.Location = New System.Drawing.Point(8, 80)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(68, 16)
        Me.objlblValue.TabIndex = 45
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(82, 78)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(240, 21)
        Me.cboMode.TabIndex = 44
        '
        'elMode
        '
        Me.elMode.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elMode.Location = New System.Drawing.Point(11, 35)
        Me.elMode.Name = "elMode"
        Me.elMode.Size = New System.Drawing.Size(227, 17)
        Me.elMode.TabIndex = 43
        Me.elMode.Text = "Mode"
        Me.elMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(206, 55)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(116, 17)
        Me.radEmployee.TabIndex = 42
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'radAllocation
        '
        Me.radAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAllocation.Location = New System.Drawing.Point(82, 55)
        Me.radAllocation.Name = "radAllocation"
        Me.radAllocation.Size = New System.Drawing.Size(118, 17)
        Me.radAllocation.TabIndex = 42
        Me.radAllocation.TabStop = True
        Me.radAllocation.Text = "Allocation"
        Me.radAllocation.UseVisualStyleBackColor = True
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(192, 108)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(74, 15)
        Me.lblWeight.TabIndex = 6
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(272, 105)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(50, 21)
        Me.txtWeight.TabIndex = 7
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.objchkAll)
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(368, 60)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(263, 204)
        Me.pnlData.TabIndex = 38
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 5)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 37
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhName, Me.objdgcolhUnkid})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(263, 204)
        Me.dgvData.TabIndex = 36
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhName
        '
        Me.dgcolhName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhName.HeaderText = "Name"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        Me.dgcolhName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhUnkid
        '
        Me.objdgcolhUnkid.HeaderText = "objdgcolhUnkid"
        Me.objdgcolhUnkid.Name = "objdgcolhUnkid"
        Me.objdgcolhUnkid.ReadOnly = True
        Me.objdgcolhUnkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhUnkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhUnkid.Visible = False
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearch.Location = New System.Drawing.Point(368, 35)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(263, 21)
        Me.txtSearch.TabIndex = 37
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(355, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(7, 247)
        Me.objLine1.TabIndex = 11
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(82, 159)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(240, 105)
        Me.txtDescription.TabIndex = 3
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(82, 132)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(240, 21)
        Me.txtName.TabIndex = 2
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(82, 105)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(104, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 159)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(68, 16)
        Me.lblDescription.TabIndex = 4
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 134)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 107)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(68, 16)
        Me.lblCode.TabIndex = 1
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAssessmentGroup
        '
        Me.pnlAssessmentGroup.Controls.Add(Me.gbAssessmentGroup)
        Me.pnlAssessmentGroup.Controls.Add(Me.objFooter)
        Me.pnlAssessmentGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAssessmentGroup.Location = New System.Drawing.Point(0, 0)
        Me.pnlAssessmentGroup.Name = "pnlAssessmentGroup"
        Me.pnlAssessmentGroup.Size = New System.Drawing.Size(642, 326)
        Me.pnlAssessmentGroup.TabIndex = 0
        '
        'frmGroup_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 326)
        Me.Controls.Add(Me.pnlAssessmentGroup)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGroup_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assessment Group"
        Me.objFooter.ResumeLayout(False)
        Me.gbAllocation.ResumeLayout(False)
        Me.gbAssessmentGroup.ResumeLayout(False)
        Me.gbAssessmentGroup.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAssessmentGroup.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAssessmentGroup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents pnlAssessmentGroup As System.Windows.Forms.Panel
    Friend WithEvents gbAllocation As System.Windows.Forms.GroupBox
    Friend WithEvents radJobs As System.Windows.Forms.RadioButton
    Friend WithEvents radSection As System.Windows.Forms.RadioButton
    Friend WithEvents radJobGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartment As System.Windows.Forms.RadioButton
    Friend WithEvents radUnit As System.Windows.Forms.RadioButton
    Friend WithEvents radBranch As System.Windows.Forms.RadioButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radTeam As System.Windows.Forms.RadioButton
    Friend WithEvents radUnitGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radSectionGroup As System.Windows.Forms.RadioButton
    Friend WithEvents radDepartmentGroup As System.Windows.Forms.RadioButton
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents objlblCaption As System.Windows.Forms.RichTextBox
    Friend WithEvents elMode As eZee.Common.eZeeLine
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents radAllocation As System.Windows.Forms.RadioButton
    Friend WithEvents objlblValue As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
End Class

﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessSubItemList

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssessSubItemList"
    Private objSubItemmaster As clsassess_subitem_master

#End Region

#Region "Form's Event"

    Private Sub frmAssessSubItemList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objSubItemmaster = New clsassess_subitem_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            Call FillCombo()

            If lvSubItemList.Items.Count > 0 Then lvSubItemList.Items(0).Selected = True

            lvSubItemList.Select()

            lvSubItemList.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessSubItemList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessSubItemList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvSubItemList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessSubItemList_KeyUp", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessSubItemList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objSubItemmaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessSubItemList_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessSubItemList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_subitem_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_subitem_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAssessItem As New clsassess_item_master
        'S.SANDEEP [ 04 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objAssessGrp As New clsassess_group_master
        'S.SANDEEP [ 04 FEB 2012 ] -- END
        Try
            dsCombo = objAssessItem.getListForCombo("List", True)
            With cboAssessmentItem
                .ValueMember = "assessitemunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsCombo = objAssessGrp.getListForCombo("List", True)
            With cboAssessGroup
                .ValueMember = "assessgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 04 FEB 2012 ] -- END


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
            End With
            'S.SANDEEP [ 28 DEC 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objAssessItem = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Dim dtTable As DataTable
        Try

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewAssessmentSubItemList = True Then    'Pinkal (09-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END



            dsList = objSubItemmaster.GetList("List")
            lvSubItemList.Items.Clear()

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboAssessGroup.SelectedValue) > 0 Then
                StrSearch &= "AND assessgroupunkid = '" & CInt(cboAssessGroup.SelectedValue) & "' "
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            If CInt(cboAssessmentItem.SelectedValue) > 0 Then
                StrSearch &= "AND assessitemunkid = '" & CInt(cboAssessmentItem.SelectedValue) & "' "
            End If

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    StrSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), StrSearch, "assessitem", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("List"), "", "assessitem", DataViewRowState.CurrentRows).ToTable
            End If

            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("assessitem").ToString
                lvItem.SubItems.Add(dtRow.Item("code").ToString)
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems.Add(dtRow.Item("description").ToString)

                lvItem.Tag = dtRow.Item("assess_subitemunkid")

                lvSubItemList.Items.Add(lvItem)
            Next

            If lvSubItemList.Items.Count > 2 Then
                colhDescription.Width = 200 - 20
            Else
                colhDescription.Width = 200
            End If

            lvSubItemList.GroupingColumn = objcolhAssessmentItem
            lvSubItemList.DisplayGroups(True)

            lvSubItemList.GridLines = False

            '            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AllowToAddAssessmentSubItems
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditAssessmentSubItems
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAssessmentSubItems
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region
    
#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmAssess_SubItemAddEdit
            
            If User._Object._Isrighttoleft = True Then
                ObjFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjFrm)
            End If

            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvSubItemList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvSubItemList.Select()
                Exit Sub
            End If
            Dim frm As New frmAssess_SubItemAddEdit
            Try
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If


                If frm.displayDialog(CInt(lvSubItemList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call Fill_List()
                End If
                frm = Nothing
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSubItemList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvSubItemList.Select()
            Exit Sub
        End If
        If objSubItemmaster.isUsed(CInt(lvSubItemList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use."), enMsgBoxStyle.Information) '?2
            lvSubItemList.Select()
            Exit Sub
        End If
        Try

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objSubItemmaster.Delete(CInt(lvSubItemList.SelectedItems(0).Tag))
                lvSubItemList.SelectedItems(0).Remove()

                If lvSubItemList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvSubItemList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAssessmentItem.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchItems.Click
        Dim frm As New frmCommonSearch
        Try
            If CType(cboAssessmentItem.DataSource, DataTable) IsNot Nothing Then
            With frm
                .ValueMember = cboAssessmentItem.ValueMember
                .DisplayMember = cboAssessmentItem.DisplayMember
                .DataSource = CType(cboAssessmentItem.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboAssessmentItem.SelectedValue = frm.SelectedValue
                cboAssessmentItem.Focus()
            End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchItems_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSearchAssessGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchAssessGrp.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboAssessGroup.ValueMember
                .DisplayMember = cboAssessGroup.DisplayMember
                .DataSource = CType(cboAssessGroup.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboAssessGroup.SelectedValue = frm.SelectedValue
                cboAssessGroup.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub cboAssessGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessGroup.SelectedIndexChanged
    '    Dim dsCombo As New DataSet
    '    Dim objAssessItem As New clsassess_item_master
    '    Try
    '        If CInt(cboAssessGroup.SelectedValue) > 0 Then
    '            dsCombo = objAssessItem.getListForCombo("List", True, CInt(cboAssessGroup.SelectedValue))
    '            With cboAssessmentItem
    '                .ValueMember = "assessitemunkid"
    '                .DisplayMember = "name"
    '                .DataSource = dsCombo.Tables("List")
    '                .SelectedValue = 0
    '            End With
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboAssessGroup_SelectedIndexChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboAssessGroup.SelectedIndexChanged
        Dim objAssessItem As New clsassess_item_master
        Dim dsCombo As DataSet
        Try
            dsCombo = objAssessItem.getListForCombo("AssessItem", True, CInt(cboAssessGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
                With cboAssessmentItem
                    .ValueMember = "assessitemunkid"
                    .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AssessItem")
                    .SelectedValue = 0
                End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhSubItemCode.Text = Language._Object.getCaption(CStr(Me.colhSubItemCode.Tag), Me.colhSubItemCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAssessmentItem.Text = Language._Object.getCaption(Me.lblAssessmentItem.Name, Me.lblAssessmentItem.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnSearchAssessGrp.Text = Language._Object.getCaption(Me.btnSearchAssessGrp.Name, Me.btnSearchAssessGrp.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Assess Item from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assess Item. Reason: This Assess Item is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Assess Item?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
    
End Class
﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTabular_Evaluation

#Region " Private Variables "

    Private mstrModuleName As String = "frmTabular_Evaluation"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objAssessMaster As clsassess_analysis_master
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintEmplId As Integer = 0
    Private mintYearId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode

    Private objAnalysisTran As clsassess_analysis_tran
    Private objRemarkTran As clsassess_remarks_tran

    Private mdicGroupWiseTotal As New Dictionary(Of Integer, Decimal)
    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)

    Private mdtEvaluation As DataTable
    Private mdtRemarks As DataTable
    Private dsTabularData As New DataSet
    Private dsItemList As New DataSet

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
        Try
            mintAssessAnalysisUnkid = intUnkid
            menAction = eAction
            menAssess = eAssess
            Me.ShowDialog()
            intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboAssessor.Enabled = False
                lblAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                tabcEvaluation.TabPages.Remove(tabpPersonalDevelopment)
                tabcEvaluation.TabPages.Remove(tabpReviewerComments)
                lnkViewAssessments.Visible = False
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.Visible = True
                lblAssessor.Visible = True
                objbtnSearchAssessor.Enabled = True
                tabcEvaluation.TabPages.Remove(tabpImporvementArea)
                tabcEvaluation.TabPages.Remove(tabpReviewerComments)
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
                lnkViewAssessments.Visible = False
                radInternalAssessor.Checked = True
                radInternalAssessor.Enabled = True : radExternalAssessor.Enabled = True
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Visible = False
                lblReviewer.Visible = True
                lblReviewer.Visible = True
                cboReviewer.Visible = True
                objbtnSearchReviewer.Visible = True
                lnkViewAssessments.Visible = True
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Enabled = False : radExternalAssessor.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboAssessGroup.BackColor = GUI.ColorComp
            cboAssessor.BackColor = GUI.ColorComp
            txtI_Action.BackColor = GUI.ColorOptional
            txtI_OtherTraining.BackColor = GUI.ColorOptional
            txtI_Support.BackColor = GUI.ColorOptional
            txtP_Action.BackColor = GUI.ColorOptional
            txtP_Development.BackColor = GUI.ColorOptional
            txtP_OtherTraining.BackColor = GUI.ColorOptional
            txtP_Support.BackColor = GUI.ColorOptional
            txtRemark1.BackColor = GUI.ColorOptional
            txtRemark2.BackColor = GUI.ColorOptional
            txtI_Improvement.BackColor = GUI.ColorOptional
            cboReviewer.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objAssessGroup As New clsassess_group_master
        Dim objCMaster As New clsCommon_Master
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                'End If
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsCombos.Tables(0)
                '    .SelectedValue = 0
                'End With
                'S.SANDEEP [04 JUN 2015] -- END
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dsCombos = objAssessMaster.getAssessorComboList("Assessor", True, True)
                With cboReviewer
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objMaster.getComboListPAYYEAR("Year", True, , , , True)
            dsCombos = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Year")
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboITraining
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboPTraining
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    cboEmployee.SelectedValue = objAssessMaster._Selfemployeeunkid
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If objAssessMaster._Ext_Assessorunkid > 0 Then
                        radExternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objAssessMaster._Ext_Assessorunkid
                    Else
                        radInternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objAssessMaster._Assessormasterunkid
                    End If
                    cboEmployee.SelectedValue = objAssessMaster._Assessedemployeeunkid
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    cboReviewer.SelectedValue = objAssessMaster._Assessormasterunkid
                    cboEmployee.SelectedValue = objAssessMaster._Assessedemployeeunkid
            End Select
            cboYear.SelectedValue = objAssessMaster._Yearunkid
            cboPeriod.SelectedValue = objAssessMaster._Periodunkid
            cboAssessGroup.SelectedValue = objAssessMaster._Assessgroupunkid
            If objAssessMaster._Assessmentdate <> Nothing Then
                dtpAssessdate.Value = objAssessMaster._Assessmentdate
            End If
            txtRemark1.Text = objAssessMaster._Reviewer_Remark1
            txtRemark2.Text = objAssessMaster._Reviewer_Remark2
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
                objAssessMaster._Assessedemployeeunkid = -1
                objAssessMaster._Assessormasterunkid = -1
                objAssessMaster._Assessoremployeeunkid = -1
                objAssessMaster._Reviewerunkid = -1
                objAssessMaster._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = -1
                objAssessMaster._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)

                If radInternalAssessor.Checked = True Then
                    objAssessMaster._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                    Dim intEmployeeId As Integer = -1
                    intEmployeeId = objAssessMaster.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                    objAssessMaster._Assessoremployeeunkid = intEmployeeId
                ElseIf radExternalAssessor.Checked = True Then
                    objAssessMaster._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                    objAssessMaster._Assessormasterunkid = -1
                    objAssessMaster._Assessoremployeeunkid = -1
                End If

                objAssessMaster._Reviewerunkid = -1
                objAssessMaster._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                objAssessMaster._Selfemployeeunkid = -1
                objAssessMaster._Assessoremployeeunkid = -1
                objAssessMaster._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                objAssessMaster._Reviewerunkid = User._Object._Userunkid
                objAssessMaster._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                objAssessMaster._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                Dim intEmployeeId As Integer = -1
                intEmployeeId = objAssessMaster.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                objAssessMaster._Reviewerunkid = intEmployeeId
            End If
            objAssessMaster._Assessmentdate = dtpAssessdate.Value
            objAssessMaster._Assessgroupunkid = CInt(cboAssessGroup.SelectedValue)
            objAssessMaster._Yearunkid = CInt(cboYear.SelectedValue)
            objAssessMaster._Periodunkid = CInt(cboPeriod.SelectedValue)
            objAssessMaster._Userunkid = User._Object._Userunkid
            objAssessMaster._Reviewer_Remark1 = txtRemark1.Text
            objAssessMaster._Reviewer_Remark2 = txtRemark2.Text
            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objAssessMaster._Committeddatetime <> Nothing Then
                objAssessMaster._Committeddatetime = objAssessMaster._Committeddatetime
            Else
                objAssessMaster._Committeddatetime = Nothing
            End If
            'S.SANDEEP [ 14 JUNE 2012 ] -- END
            If menAction = enAction.ADD_CONTINUE Then
                objAssessMaster._Isvoid = False
                objAssessMaster._Voiddatetime = Nothing
                objAssessMaster._Voidreason = ""
                objAssessMaster._Voiduserunkid = -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub DoOperation(ByVal blnOperation As Boolean)
        Try
            cboAssessGroup.Enabled = blnOperation
            cboAssessor.Enabled = blnOperation
            cboEmployee.Enabled = blnOperation
            cboPeriod.Enabled = blnOperation
            cboYear.Enabled = blnOperation
            dtpAssessdate.Enabled = blnOperation
            radExternalAssessor.Enabled = blnOperation
            radInternalAssessor.Enabled = blnOperation
            objbtnSearchGroup.Enabled = blnOperation
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEvaluationList()
        Try
            dsTabularData = objAssessMaster.GetTabularData(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessGroup.SelectedValue))

            Dim iRGrp() As DataRow = dsTabularData.Tables(0).Select("RGrpId > 0")
            Dim iResultGrpId As Integer = 0
            If iRGrp.Length > 0 Then
                iResultGrpId = CInt(iRGrp(0).Item("RGrpId"))
            End If
            dgvData.AutoGenerateColumns = False
            dgcolhItems.DataPropertyName = "Items"
            dgcolhRemark.DataPropertyName = "Remark"
            dgcolhResult.DataPropertyName = "Result"
            dgcolhScoreGuide.DataPropertyName = "ScoreGuide"
            dgcolhWeight.DataPropertyName = "Weight"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhiEdit.DataPropertyName = "iEdit"
            objdgcolhSubItemId.DataPropertyName = "assesssubitemunkid"

            dgcolhWeight.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhWeight.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            Dim dsCombo As New DataSet
            dsCombo = (New clsresult_master).getComboList("List", True, iResultGrpId)
            dgcolhScore.ValueMember = "Id"
            dgcolhScore.DisplayMember = "Name"
            dgcolhScore.DataSource = dsCombo.Tables(0)
            dgcolhScore.DataPropertyName = "Id"
            dgvData.DataSource = dsTabularData.Tables(0)

            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, 2, dgvRow.Cells.Count - 1, Color.Gray, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.ReadOnly = True
                End If

                If CInt(dgvRow.Cells(objdgcolhiEdit.Index).Value.ToString) <= 0 AndAlso CInt(dgvRow.Cells(objdgcolhSubItemId.Index).Value) <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhScoreGuide.Index).ColumnIndex, dgvRow.Cells(dgcolhScoreGuide.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhScore.Index).ColumnIndex, dgvRow.Cells(dgcolhScore.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.Cells(dgcolhScoreGuide.Index).ReadOnly = True
                    dgvRow.ReadOnly = True
                ElseIf CInt(dgvRow.Cells(objdgcolhiEdit.Index).Value.ToString) > 0 AndAlso CInt(dgvRow.Cells(objdgcolhSubItemId.Index).Value) <= 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhScoreGuide.Index).ColumnIndex, dgvRow.Cells(dgcolhScoreGuide.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.Cells(dgcolhScoreGuide.Index).ReadOnly = True
                ElseIf CInt(dgvRow.Cells(objdgcolhiEdit.Index).Value.ToString) > 0 AndAlso CInt(dgvRow.Cells(objdgcolhSubItemId.Index).Value) > 0 AndAlso CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = False Then
                    pCell.MakeMerge(dgvData, dgvRow.Index, dgvRow.Cells(dgcolhWeight.Index).ColumnIndex, dgvRow.Cells(dgcolhWeight.Index).ColumnIndex, Color.White, Color.White, Nothing, "", picStayView.Image)
                    dgvRow.Cells(dgcolhWeight.Index).ReadOnly = True
                End If

                If menAction <> enAction.EDIT_ONE Then
                    dgvRow.Cells(dgcolhScore.Index).Value = 0
                Else
                    Dim dtmp() As DataRow = Nothing
                    dtmp = mdtEvaluation.Select("assess_subitemunkid = '" & dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assesssubitemunkid").ToString & "' AND AUD <> 'D' ")
                    If dtmp.Length > 0 Then
                        RemoveHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged
                        Dim objResult As New clsresult_master
                        objResult._Resultunkid = CInt(dtmp(0).Item("resultunkid"))
                        dgvRow.Cells(dgcolhScore.Index).Value = CInt(dtmp(0).Item("resultunkid"))
                        If ConfigParameter._Object._ConsiderItemWeightAsNumber Then
                            If IsNumeric(objResult._Resultname) Then
                                dgvRow.Cells(dgcolhResult.Index).Value = CDbl(dgvRow.Cells(dgcolhWeight.Index).Value) * CDbl(objResult._Resultname)
                            End If
                        Else
                            dgvRow.Cells(dgcolhResult.Index).Value = objResult._Resultname
                        End If
                        dgvRow.Cells(dgcolhRemark.Index).Value = dtmp(0).Item("remark")
                        objResult = Nothing

                        Dim iRIdx As Integer = -1
                        dtmp = dsTabularData.Tables(0).Select("assessitemunkid = '" & CInt(dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assessitemunkid")) & "' AND assesssubitemunkid = 0")
                        If dtmp.Length > 0 Then
                            iRIdx = dsTabularData.Tables(0).Rows.IndexOf(dtmp(0))
                            dgvData.Rows(iRIdx).Cells(dgcolhResult.Index).Value = mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assessitemunkid")))
                        End If

                        AddHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged
                    Else
                        dgvRow.Cells(dgcolhScore.Index).Value = 0
                    End If
                End If

                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    If dgvRow.Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhCollaps.Index).Value = "-"
                        dgvRow.Cells(objdgcolhCollaps.Index).Style.ForeColor = Color.White
                    End If
                    dgvRow.DefaultCellStyle.BackColor = Color.Gray
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.Gray
                    dgvRow.DefaultCellStyle.ForeColor = Color.White
                Else
                    dgvRow.Cells(objdgcolhCollaps.Index).Value = ""
                End If

                Dim objItem As New clsassess_item_master
                objItem._Assessitemunkid = CInt(dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assessitemunkid"))
                If objItem._Assessitemunkid > 0 Then
                    If mdicItemWeight.ContainsKey(CInt(dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assessitemunkid"))) = False Then
                        mdicItemWeight.Add(CInt(dsTabularData.Tables(0).Rows(dgvRow.Index).Item("assessitemunkid")), objItem._Weight)
                    End If
                End If
                objItem = Nothing
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEvaluationList", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False
            ElseIf CInt(cboYear.SelectedValue) <= 0 AndAlso menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Select()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 AndAlso menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(cboAssessGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Assessment Group is compulsory information.Please Select Assessment Group."), enMsgBoxStyle.Information)
                cboAssessGroup.Select()
                Return False
            ElseIf CInt(cboAssessor.SelectedValue) <= 0 AndAlso menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
                cboAssessor.Select()
                Return False
            ElseIf CInt(cboReviewer.SelectedValue) <= 0 AndAlso menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
                cboReviewer.Focus()
                Return False
            End If

            Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", CInt(cboYear.SelectedValue))
            If dsYr.Tables("List").Rows.Count > 0 Then
                If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
                   dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            Else
                If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
                   dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & FinancialYear._Object._Database_End_Date.Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Evaluated_Data(ByVal iResultId As Integer)
        Try
            Dim dRow As DataRow = Nothing
            Dim dtTemp() As DataRow = mdtEvaluation.Select("assessitemunkid = '" & CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")) & "' AND assess_subitemunkid = '" & CInt(dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value) & "' AND AUD <> 'D' ")
            If dtTemp.Length <= 0 Then
                dRow = mdtEvaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("assessitemunkid") = dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")
                dRow.Item("assess_subitemunkid") = dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value
                dRow.Item("resultunkid") = iResultId
                dRow.Item("remark") = dgvData.CurrentRow.Cells(dgcolhRemark.Index).Value.ToString
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                mdtEvaluation.Rows.Add(dRow)
            Else
                dtTemp(0).Item("analysistranunkid") = dtTemp(0).Item("analysistranunkid")
                dtTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dtTemp(0).Item("assessitemunkid") = dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")
                dtTemp(0).Item("assess_subitemunkid") = dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value
                dtTemp(0).Item("resultunkid") = iResultId
                dtTemp(0).Item("remark") = dgvData.CurrentRow.Cells(dgcolhRemark.Index).Value.ToString
                If IsDBNull(dtTemp(0).Item("AUD")) Or CStr(dtTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dtTemp(0).Item("AUD") = "U"
                End If
                dtTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dtTemp(0).Item("isvoid") = False
                dtTemp(0).Item("voiduserunkid") = -1
                dtTemp(0).Item("voiddatetime") = DBNull.Value
                dtTemp(0).Item("voidreason") = ""
                dtTemp(0).AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Evaluated_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            Dim iItemCount = objAssessMaster.GetItems_BaseGroup(CInt(cboAssessGroup.SelectedValue))
            Dim dtView As DataView = mdtEvaluation.DefaultView

            If iItemCount <> dtView.ToTable(True, "assessitemunkid").Rows.Count Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all items in the selected group."), enMsgBoxStyle.Information)
                End If
                Return False
            End If

            If dsItemList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsItemList.Tables(0).Rows
                    Dim dtTemp() As DataRow = mdtEvaluation.Select("assessitemunkid = '" & CInt(dRow.Item("Id")) & "'")
                    If dtTemp.Length > 0 AndAlso CInt(dtTemp(0)("assess_subitemunkid")) <> 0 Then
                        If dtTemp.Length <> CInt(dRow.Item("Cnt")) Then
                            If blnFlag = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all subitems in some item(s)."), enMsgBoxStyle.Information)
                            End If
                            Return False
                        End If
                    End If
                Next
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub FillImprovementList(ByVal ListView As eZee.Common.eZeeListView)
        Try

            ListView.Items.Clear()
            ListView.GridLines = False
            Dim dtTable As DataTable = Nothing
            Select Case ListView.Name.ToUpper
                Case "LVIMPROVEMENT"
                    dtTable = New DataView(mdtRemarks, "isimporvement = true", "", DataViewRowState.CurrentRows).ToTable
                Case "LVPERSONALDEVELOP"
                    dtTable = New DataView(mdtRemarks, "isimporvement = false", "", DataViewRowState.CurrentRows).ToTable
            End Select

            Dim lvItem As New ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("major_area").ToString
                    lvItem.SubItems.Add(dtRow.Item("activity").ToString)
                    lvItem.SubItems.Add(dtRow.Item("support_required").ToString)
                    lvItem.SubItems.Add(dtRow.Item("course_name").ToString)
                    lvItem.SubItems.Add(CDate(dtRow.Item("timeframe_date")).ToShortDateString)
                    lvItem.SubItems.Add(dtRow.Item("other_training").ToString)
                    lvItem.SubItems.Add(dtRow.Item("coursemasterunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("remarkstranunkid")

                    ListView.Items.Add(lvItem)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillImprovementList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetImprovement(ByVal blnFlag As Boolean)
        Try
            If blnFlag = True Then
                txtI_Action.Text = ""
                txtI_Improvement.Text = ""
                txtI_OtherTraining.Text = ""
                txtI_Support.Text = ""
                cboITraining.SelectedValue = 0
            Else
                txtP_Action.Text = ""
                txtP_Development.Text = ""
                txtP_OtherTraining.Text = ""
                txtP_Support.Text = ""
                cboPTraining.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetImprovement", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmTabular_Evaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAssessMaster = New clsassess_analysis_master
        objAnalysisTran = New clsassess_analysis_tran
        objRemarkTran = New clsassess_remarks_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objAssessMaster._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                cboAssessGroup.Enabled = False
                radExternalAssessor.Enabled = False
                radInternalAssessor.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
                objbtnSearchGroup.Enabled = False
                cboYear.Enabled = False : cboPeriod.Enabled = False
            End If
            Call GetValue()
            objAnalysisTran._ConsiderWeightAsNumber = ConfigParameter._Object._ConsiderItemWeightAsNumber
            objAnalysisTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtEvaluation = objAnalysisTran._DataTable

            mdicGroupWiseTotal = objAnalysisTran._GrpWiseTotal
            mdicItemWeight = objAnalysisTran._ItemWeight

            If menAction = enAction.EDIT_ONE Then
                Call FillEvaluationList()
            End If

            objRemarkTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtRemarks = objRemarkTran._DataTable
            Call FillImprovementList(lvImprovement)
            Call FillImprovementList(lvPersonalDevelop)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTabular_Evaluation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTabular_Evaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTabular_Evaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTabular_Evaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTabular_Evaluation_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTabular_Evaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objAssessMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_analysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_analysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
        Dim blnFlag As Boolean = False
        Try
            Dim dtmp() As DataRow = mdtEvaluation.Select("AUD <> 'D'")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Please assess atleast one item in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call SetValue()
            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSAVECOMMIT"
                    If ConfigParameter._Object._IsAllowFinalSave = False Then
                        If isAll_Assessed() = False Then Exit Sub
                        If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                            If lvImprovement.Items.Count <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot do Save Commit Operation as you have not provided any improvement."), enMsgBoxStyle.Information)
                                tabcEvaluation.SelectedTab = tabpImporvementArea
                                Exit Sub
                            End If
                        End If
                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
                        If isAll_Assessed(False) = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    End If
                    objAssessMaster._Iscommitted = True
                    objAssessMaster._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
            End Select

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssessMaster.Update(mdtEvaluation, mdtRemarks)
            Else
                blnFlag = objAssessMaster.Insert(mdtEvaluation, mdtRemarks)
            End If

            If blnFlag = False And objAssessMaster._Message <> "" Then
                eZeeMsgBox.Show(objAssessMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objAssessMaster._Iscommitted = True Then
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , "", CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 22 OCT 2013 ] -- START {CInt(cboAssessGroup.SelectedValue)} -- END
                            objAssessMaster.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                                                               CInt(cboEmployee.SelectedValue), _
                                                               CInt(cboPeriod.SelectedValue), _
                                                               CInt(cboYear.SelectedValue), _
                                                              ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , "", CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , cboAssessor.Text, CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 22 OCT 2013 ] -- START {CInt(cboAssessGroup.SelectedValue)} -- END)
                            'objAssessMaster.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , cboAssessor.Text, CInt(cboAssessGroup.SelectedValue), , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objAssessMaster.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                            '                                   CInt(cboEmployee.SelectedValue), _
                            '                                   CInt(cboPeriod.SelectedValue), _
                            '                                   CInt(cboYear.SelectedValue), _
                            '                                   ConfigParameter._Object._IsCompanyNeedReviewer, , , cboReviewer.Text, , , enLogin_Mode.DESKTOP, 0)
                            objAssessMaster.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                               CInt(cboEmployee.SelectedValue), _
                                                               CInt(cboPeriod.SelectedValue), _
                                                               CInt(cboYear.SelectedValue), _
                                                              ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboReviewer.Text, , , enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                    End Select
                End If
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAssessMaster = Nothing
                    objAssessMaster = New clsassess_analysis_master
                    Call GetValue()
                    Call DoOperation(True)
                    mdtEvaluation.Rows.Clear()
                    mdicGroupWiseTotal.Clear()
                    mdicItemWeight.Clear()
                    Call FillEvaluationList()
                    mdtRemarks.Rows.Clear()
                    Call FillImprovementList(lvImprovement)
                    Call FillImprovementList(lvPersonalDevelop)
                    Call SetVisibility()
                Else
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboAssessGroup.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call FillEvaluationList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboEmployee.SelectedValue = 0
            Else
                cboAssessor.SelectedValue = 0
                cboReviewer.SelectedValue = 0
            End If
            cboYear.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboAssessGroup.SelectedValue = 0
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Imporvement Area "

    Private Sub btnAddImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddImprovement.Click
        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND isimporvement = 1 ")


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = True ")
            Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = True AND AUD <> 'D' ")
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'Pinkal (06-Feb-2012) -- End



            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If txtI_Improvement.Text.Trim.Length <= 0 And _
               txtI_Action.Text.Trim.Length <= 0 And _
               txtI_OtherTraining.Text.Trim.Length <= 0 And _
               txtI_Support.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please write some remark in order to add into list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dRow As DataRow = mdtRemarks.NewRow

            dRow.Item("remarkstranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("coursemasterunkid") = cboITraining.SelectedValue
            dRow.Item("timeframe_date") = dtpITimeframe.Value
            dRow.Item("major_area") = txtI_Improvement.Text
            dRow.Item("activity") = txtI_Action.Text
            dRow.Item("support_required") = txtI_Support.Text
            dRow.Item("other_training") = txtI_OtherTraining.Text
            dRow.Item("isimporvement") = True
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"

            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dRow.Item("course_name") = cboITraining.Text
            If CInt(cboITraining.SelectedValue) > 0 Then
                dRow.Item("course_name") = cboITraining.Text
            Else
                dRow.Item("course_name") = ""
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtRemarks.Rows.Add(dRow)

            Call FillImprovementList(lvImprovement)

            Call ResetImprovement(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvImprovement.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvImprovement.SelectedItems(0).Tag) & "'")
                End If


                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "' AND AUD <> 'D'")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END



                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("remarkstranunkid") = lvImprovement.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("coursemasterunkid") = cboITraining.SelectedValue
                        .Item("timeframe_date") = dtpITimeframe.Value
                        .Item("major_area") = txtI_Improvement.Text
                        .Item("activity") = txtI_Action.Text
                        .Item("support_required") = txtI_Support.Text
                        .Item("other_training") = txtI_OtherTraining.Text
                        .Item("isimporvement") = True
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If


                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        '.Item("course_name") = cboITraining.Text

                        If CInt(cboITraining.SelectedValue) > 0 Then
                            .Item("course_name") = cboITraining.Text
                        Else
                            .Item("course_name") = ""
                        End If


                        'Pinkal (06-Feb-2012) -- End


                        .Item("GUID") = Guid.NewGuid.ToString
                        .AcceptChanges()
                    End With



                    Call FillImprovementList(lvImprovement)
                End If
                Call ResetImprovement(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteImprovement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvImprovement.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvImprovement.SelectedItems(0).SubItems(objcolhI_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvImprovement.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If CInt(lvImprovement.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    Call FillImprovementList(lvImprovement)
                End If
                Call ResetImprovement(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvImprovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvImprovement.Click
        Try
            If lvImprovement.SelectedItems.Count > 0 Then
                cboITraining.SelectedValue = 0
                txtI_Action.Text = lvImprovement.SelectedItems(0).SubItems(colhI_ActivityReq.Index).Text
                txtI_Improvement.Text = lvImprovement.SelectedItems(0).Text
                txtI_OtherTraining.Text = lvImprovement.SelectedItems(0).SubItems(colhI_OtherTraning.Index).Text
                txtI_Support.Text = lvImprovement.SelectedItems(0).SubItems(colhI_Support.Index).Text
                cboITraining.SelectedValue = lvImprovement.SelectedItems(0).SubItems(objcolhI_CourseId.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Personal Developement "

    Private Sub btnAddPersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPersonal.Click
        Try


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND isimporvement = 0 ")


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = False ")
            Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboPTraining.SelectedValue) & "' AND coursemasterunkid > 0 AND isimporvement = False AND AUD <> 'D' ")
            'S.SANDEEP [ 05 MARCH 2012 ] -- END


            'Pinkal (06-Feb-2012) -- End



            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If txtP_Development.Text.Trim.Length <= 0 And _
               txtP_Action.Text.Trim.Length <= 0 And _
               txtP_OtherTraining.Text.Trim.Length <= 0 And _
               txtP_Support.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please write some remark in order to add into list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dRow As DataRow = mdtRemarks.NewRow

            dRow.Item("remarkstranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("coursemasterunkid") = cboPTraining.SelectedValue
            dRow.Item("timeframe_date") = dtpPTimeframe.Value
            dRow.Item("major_area") = txtP_Development.Text
            dRow.Item("activity") = txtP_Action.Text
            dRow.Item("support_required") = txtP_Support.Text
            dRow.Item("other_training") = txtP_OtherTraining.Text
            dRow.Item("isimporvement") = False
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dRow.Item("course_name") = cboPTraining.Text
            If CInt(cboPTraining.SelectedValue) > 0 Then
                dRow.Item("course_name") = cboPTraining.Text
            Else
                dRow.Item("course_name") = ""
            End If
            'S.SANDEEP [ 23 JAN 2012 ] -- END
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtRemarks.Rows.Add(dRow)

            Call FillImprovementList(lvPersonalDevelop)

            Call ResetImprovement(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddPersonal_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditPersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditPersonal.Click
        Try

            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvPersonalDevelop.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvPersonalDevelop.SelectedItems(0).Tag) & "'")
                End If

                'S.SANDEEP [ 05 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Dim dtTemp() As DataRow = mdtRemarks.Select("coursemasterunkid = '" & CInt(cboITraining.SelectedValue) & "' AND GUID <> '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "' AND AUD <> 'D'")
                'S.SANDEEP [ 05 MARCH 2012 ] -- END

                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot add same training again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("remarkstranunkid") = lvPersonalDevelop.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("coursemasterunkid") = cboPTraining.SelectedValue
                        .Item("timeframe_date") = dtpPTimeframe.Value
                        .Item("major_area") = txtP_Development.Text
                        .Item("activity") = txtP_Action.Text
                        .Item("support_required") = txtP_Support.Text
                        .Item("other_training") = txtP_OtherTraining.Text
                        .Item("isimporvement") = False
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If


                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        '.Item("course_name") = cboPTraining.Text

                        If CInt(cboPTraining.SelectedValue) > 0 Then
                            .Item("course_name") = cboPTraining.Text
                        Else
                            .Item("course_name") = ""
                        End If
                        'Pinkal (06-Feb-2012) -- End


                        .Item("GUID") = Guid.NewGuid.ToString
                        .AcceptChanges()
                    End With
                    Call FillImprovementList(lvPersonalDevelop)
                End If
                Call ResetImprovement(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditPersonal_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeletePersonal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePersonal.Click
        Try
            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvPersonalDevelop.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtRemarks.Select("GUID = '" & lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_GUID.Index).Text & "'")
                Else
                    drTemp = mdtRemarks.Select("remarkstranunkid = '" & CInt(lvPersonalDevelop.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If CInt(lvPersonalDevelop.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    Call FillImprovementList(lvPersonalDevelop)
                End If
                Call ResetImprovement(False)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteImprovement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPersonalDevelop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvPersonalDevelop.Click
        Try
            If lvPersonalDevelop.SelectedItems.Count > 0 Then
                cboPTraining.SelectedValue = 0
                txtP_Action.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_ActivityReq.Index).Text
                txtP_Development.Text = lvPersonalDevelop.SelectedItems(0).Text
                txtP_OtherTraining.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_OtherTraning.Index).Text
                txtP_Support.Text = lvPersonalDevelop.SelectedItems(0).SubItems(colhP_Support.Index).Text
                cboPTraining.SelectedValue = lvPersonalDevelop.SelectedItems(0).SubItems(objcolhP_CourseId.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPersonalDevelop", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet

                If radInternalAssessor.Checked = True Then
                    dsList = objAssessMaster.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                ElseIf radExternalAssessor.Checked = True Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If

                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            If CInt(cboYear.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "APeriod", True, 1)
                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "APeriod", True, 1)
                'Sohail (21 Aug 2015) -- End
                With cboPeriod
                    .ValueMember = "periodunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("APeriod")
                    .SelectedValue = 0
                End With
            Else
                cboPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboEmployee.DataSource IsNot Nothing Then
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                Select Case menAssess
                    Case enAssessmentMode.SELF_ASSESSMENT
                        frm.CodeMember = "employeecode"
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        frm.CodeMember = "Code"
                End Select
                frm.DataSource = CType(cboEmployee.DataSource, DataTable)
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim dsCombo As New DataSet : Dim objAssessGroup As New clsassess_group_master
                dsCombo = objAssessGroup.getListForCombo(CInt(cboEmployee.SelectedValue), "AssessGroup", True)
                With cboAssessGroup
                    .ValueMember = "assessgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("AssessGroup")
                End With
            Else
                cboAssessGroup.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If cboAssessGroup.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboAssessGroup.ValueMember
                    .DisplayMember = cboAssessGroup.DisplayMember
                    .DataSource = CType(cboAssessGroup.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboAssessGroup.SelectedValue = frm.SelectedValue
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkViewAssessments_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkViewAssessments.LinkClicked
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboAssessGroup.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim frm As New frmCommonAssess_View
            frm.displayDialog(cboEmployee.Text, _
                              cboAssessGroup.Text, _
                              CInt(cboEmployee.SelectedValue), _
                              CInt(cboAssessGroup.SelectedValue), _
                              CInt(cboYear.SelectedValue), _
                              CInt(cboPeriod.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewAssessments_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value Is "-" Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            ElseIf e.ColumnIndex = dgvData.Rows(e.RowIndex).Cells(dgcolhScoreGuide.Index).ColumnIndex Then
                If CInt(dsTabularData.Tables(0).Rows(e.RowIndex).Item("assesssubitemunkid")) > 0 Then
                    Dim mScoreGuide As String = String.Empty
                    Dim objSItem As New clsassess_subitem_master
                    objSItem._Assess_Subitemunkid = CInt(dsTabularData.Tables(0).Rows(e.RowIndex).Item("assesssubitemunkid"))
                    mScoreGuide = objSItem._Description
                    objSItem = Nothing
                    If mScoreGuide <> "" Then
                        Dim frm As New frmScore_Guide
                        frm._Score_Guide = mScoreGuide
                        frm.ShowDialog()
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, No score guide is defined for the selected subitem in description."), enMsgBoxStyle.Information)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellEnter
        Try
            If e.ColumnIndex = dgcolhScore.Index Or e.ColumnIndex = dgcolhRemark.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.ColumnIndex = dgcolhRemark.Index Then
                If dgvData.Rows(e.RowIndex).Cells(dgcolhRemark.Index).Value.ToString.Trim.Length > 0 Then
                    Call Evaluated_Data(Convert.ToInt32(dgvData.CurrentRow.Cells(dgcolhScore.Index).Value))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
        Dim cbo As ComboBox
        Try
            If dgvData.CurrentCell.ColumnIndex = dgcolhScore.Index AndAlso TypeOf e.Control Is ComboBox Then
                cbo = CType(e.Control, ComboBox)
                RemoveHandler cbo.SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
                AddHandler cbo.SelectedIndexChanged, AddressOf cbo_SelectedIndexChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub cbo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cbobox As ComboBox
        cbobox = CType(sender, ComboBox)
        If cbobox.SelectedValue IsNot Nothing AndAlso TypeOf (cbobox.SelectedValue) Is Integer AndAlso CInt(cbobox.SelectedIndex) > 0 Then
            If TypeOf (cbobox.SelectedValue) Is Integer Then
                If CDec(cbobox.Text) > CDec(dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set more result value than assigned weight."), enMsgBoxStyle.Information)
                    cbobox.SelectedValue = 0
                Else
                    If Validation() = False Then Exit Sub

                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            If objAssessMaster.isExist(menAssess, _
                                                       CInt(cboEmployee.SelectedValue), _
                                                       CInt(cboYear.SelectedValue), _
                                                       CInt(cboPeriod.SelectedValue), _
                                                       CInt(cboAssessGroup.SelectedValue), _
                                                       , , , _
                                                       mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
								cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If


                            If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               CInt(dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value), _
                                               , , _
                                               mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                                If objAssessMaster.isExist(enAssessmentMode.SELF_ASSESSMENT, _
                                                       CInt(cboEmployee.SelectedValue), _
                                                       CInt(cboYear.SelectedValue), _
                                                       CInt(cboPeriod.SelectedValue), _
                                                       CInt(cboAssessGroup.SelectedValue), _
                                                       , , , _
                                                      -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                    Exit Sub
                                End If
                            End If
                            If objAssessMaster.isExist(menAssess, _
                                                       CInt(cboEmployee.SelectedValue), _
                                                       CInt(cboYear.SelectedValue), _
                                                       CInt(cboPeriod.SelectedValue), _
                                                       CInt(cboAssessGroup.SelectedValue), _
                                                       , CInt(cboAssessor.SelectedValue), , _
                                                       mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If

                            If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               CInt(dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value), _
                                               CInt(cboAssessor.SelectedValue), _
                                               , _
                                               mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If
                        Case enAssessmentMode.REVIEWER_ASSESSMENT

                            If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                                If objAssessMaster.isExist(enAssessmentMode.SELF_ASSESSMENT, _
                                                       CInt(cboEmployee.SelectedValue), _
                                                       CInt(cboYear.SelectedValue), _
                                                       CInt(cboPeriod.SelectedValue), _
                                                       CInt(cboAssessGroup.SelectedValue), _
                                                       , , , _
                                                      -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                    Exit Sub
                                End If

                                If objAssessMaster.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                        CInt(cboEmployee.SelectedValue), _
                                                        CInt(cboYear.SelectedValue), _
                                                        CInt(cboPeriod.SelectedValue), _
                                                        CInt(cboAssessGroup.SelectedValue), _
                                                        , , , _
                                                        -1) = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), enMsgBoxStyle.Information)
                                    cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                    Exit Sub
                                End If
                            End If
                            If objAssessMaster.isExist(menAssess, _
                                                       CInt(cboEmployee.SelectedValue), _
                                                       CInt(cboYear.SelectedValue), _
                                                       CInt(cboPeriod.SelectedValue), _
                                                       CInt(cboAssessGroup.SelectedValue), _
                                                       , , CInt(cboReviewer.SelectedValue), _
                                                       mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, You have already assessed some items of particular group. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If

                            If objAssessMaster.isExist(menAssess, _
                                               CInt(cboEmployee.SelectedValue), _
                                               CInt(cboYear.SelectedValue), _
                                               CInt(cboPeriod.SelectedValue), _
                                               CInt(cboAssessGroup.SelectedValue), _
                                               CInt(dgvData.CurrentRow.Cells(objdgcolhSubItemId.Index).Value), _
                                               , CInt(cboReviewer.SelectedValue), _
                                               mintAssessAnalysisUnkid) = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected subitem assessment is already present for this period. Please define new assessment."), enMsgBoxStyle.Information)
                                cbobox.SelectedValue = 0 'S.SANDEEP [ 19 AUG 2014 ] -- START -- END
                                Exit Sub
                            End If
                    End Select


                    'S.SANDEEP [ 19 AUG 2014 ] -- START
                    Dim iDecResult As Decimal = 0
                    Decimal.TryParse(CStr(dgvData.CurrentRow.Cells(dgcolhResult.Index).Value), iDecResult)
                    'S.SANDEEP [ 19 AUG 2014 ] -- END


                    If mdicGroupWiseTotal.ContainsKey(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) Then
                        'S.SANDEEP [ 19 AUG 2014 ] -- START
                        'mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) - CDec(dgvData.CurrentRow.Cells(dgcolhResult.Index).Value))
                        mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) - CDec(iDecResult))
                        'S.SANDEEP [ 19 AUG 2014 ] -- END

                        If (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) + CDec(cbobox.Text)) > mdicItemWeight(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            dgvData.CurrentRow.Cells(dgcolhResult.Index).Value = cbobox.SelectedValue
                            cbobox.SelectedValue = 0
                            Exit Sub
                        End If

                        If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                            If dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value.ToString.Trim.Length > 0 Then
                                mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) + (CDec(cbobox.Text) * CDec(dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value)))
                            Else
                                mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) + (CDec(cbobox.Text) * CDec(1)))
                            End If
                        Else
                            'If dgvData.CurrentRow.Cells(dgcolhResult.Index).Value.ToString <> "" Then
                            '    mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) - CDec(dgvData.CurrentRow.Cells(dgcolhResult.Index).Value))
                            'End If
                            mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) = (mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid"))) + CDec(cbobox.Text))
                        End If
                    Else
                        If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                            If dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value.ToString.Trim.Length > 0 Then
                                mdicGroupWiseTotal.Add(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")), (CDec(cbobox.Text) * CDec(dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value)))
                            Else
                                mdicGroupWiseTotal.Add(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")), (CDec(cbobox.Text) * CDec(1)))
                            End If
                        Else
                            mdicGroupWiseTotal.Add(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")), CDec(cbobox.Text))
                        End If
                    End If
                    If ConfigParameter._Object._ConsiderItemWeightAsNumber = True Then
                        dgvData.CurrentRow.Cells(dgcolhResult.Index).Value = (CDec(cbobox.Text) * CDec(dgvData.CurrentRow.Cells(dgcolhWeight.Index).Value))
                    Else
                        dgvData.CurrentRow.Cells(dgcolhResult.Index).Value = (CDec(cbobox.Text) * CDec(1))
                    End If
                    Dim iRIdx As Integer = -1
                    Dim dtmp() As DataRow = dsTabularData.Tables(0).Select("assessitemunkid = '" & CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")) & "' AND assesssubitemunkid = 0")
                    If dtmp.Length > 0 Then
                        iRIdx = dsTabularData.Tables(0).Rows.IndexOf(dtmp(0))
                        dgvData.Rows(iRIdx).Cells(dgcolhResult.Index).Value = mdicGroupWiseTotal(CInt(dsTabularData.Tables(0).Rows(dgvData.CurrentRow.Index).Item("assessitemunkid")))
                    End If
                    Call Evaluated_Data(CInt(cbobox.SelectedValue))
                End If
            End If
        End If
    End Sub

    Private Sub cboAssessGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAssessGroup.SelectedIndexChanged
        Try
            If CInt(cboAssessGroup.SelectedValue) > 0 Then
                Dim objAssessItem As New clsassess_item_master
                dsItemList = objAssessMaster.GetSubItemCount(CInt(cboAssessGroup.SelectedValue), , CInt(cboPeriod.SelectedValue))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchPTraning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPTraning.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboPTraining.ValueMember
                .DisplayMember = cboPTraining.DisplayMember
                .DataSource = CType(cboPTraining.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboPTraining.SelectedValue = frm.SelectedValue
                cboPTraining.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPTraning_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchITraning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchITraning.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboITraining.ValueMember
                .DisplayMember = cboITraining.DisplayMember
                .DataSource = CType(cboITraining.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboITraining.SelectedValue = frm.SelectedValue
                cboITraining.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchITraning_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
        Try
            If radInternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim dsCombos As DataSet = objAssessMaster.getAssessorComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
        Try
            If radExternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objAssessMaster.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("REmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbAssessmentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAssessmentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbImprovement.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbImprovement.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPersonalDevelopment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPersonalDevelopment.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbComments.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbComments.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSaveCommit.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveCommit.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSaveCommit.Text = Language._Object.getCaption(Me.btnSaveCommit.Name, Me.btnSaveCommit.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpCompetenceEvaluation.Text = Language._Object.getCaption(Me.tabpCompetenceEvaluation.Name, Me.tabpCompetenceEvaluation.Text)
            Me.gbAssessmentInfo.Text = Language._Object.getCaption(Me.gbAssessmentInfo.Name, Me.gbAssessmentInfo.Text)
            Me.radExternalAssessor.Text = Language._Object.getCaption(Me.radExternalAssessor.Name, Me.radExternalAssessor.Text)
            Me.radInternalAssessor.Text = Language._Object.getCaption(Me.radInternalAssessor.Name, Me.radInternalAssessor.Text)
            Me.lblAssessDate.Text = Language._Object.getCaption(Me.lblAssessDate.Name, Me.lblAssessDate.Text)
            Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
            Me.lblAssessGroup.Text = Language._Object.getCaption(Me.lblAssessGroup.Name, Me.lblAssessGroup.Text)
            Me.tabpImporvementArea.Text = Language._Object.getCaption(Me.tabpImporvementArea.Name, Me.tabpImporvementArea.Text)
            Me.gbImprovement.Text = Language._Object.getCaption(Me.gbImprovement.Name, Me.gbImprovement.Text)
            Me.lblILearningObjective.Text = Language._Object.getCaption(Me.lblILearningObjective.Name, Me.lblILearningObjective.Text)
            Me.lblITime.Text = Language._Object.getCaption(Me.lblITime.Name, Me.lblITime.Text)
            Me.tabpImprovement.Text = Language._Object.getCaption(Me.tabpImprovement.Name, Me.tabpImprovement.Text)
            Me.tapbActionRequired.Text = Language._Object.getCaption(Me.tapbActionRequired.Name, Me.tapbActionRequired.Text)
            Me.tabpSupportRequired.Text = Language._Object.getCaption(Me.tabpSupportRequired.Name, Me.tabpSupportRequired.Text)
            Me.tabpOtherTraining.Text = Language._Object.getCaption(Me.tabpOtherTraining.Name, Me.tabpOtherTraining.Text)
            Me.tabpPersonalDevelopment.Text = Language._Object.getCaption(Me.tabpPersonalDevelopment.Name, Me.tabpPersonalDevelopment.Text)
            Me.gbPersonalDevelopment.Text = Language._Object.getCaption(Me.gbPersonalDevelopment.Name, Me.gbPersonalDevelopment.Text)
            Me.lblPLearningObjective.Text = Language._Object.getCaption(Me.lblPLearningObjective.Name, Me.lblPLearningObjective.Text)
            Me.lblPTime.Text = Language._Object.getCaption(Me.lblPTime.Name, Me.lblPTime.Text)
            Me.tabpDevelopment.Text = Language._Object.getCaption(Me.tabpDevelopment.Name, Me.tabpDevelopment.Text)
            Me.tabpPAction.Text = Language._Object.getCaption(Me.tabpPAction.Name, Me.tabpPAction.Text)
            Me.tabpPSupport.Text = Language._Object.getCaption(Me.tabpPSupport.Name, Me.tabpPSupport.Text)
            Me.tabpPOtherTraining.Text = Language._Object.getCaption(Me.tabpPOtherTraining.Name, Me.tabpPOtherTraining.Text)
            Me.tabpReviewerComments.Text = Language._Object.getCaption(Me.tabpReviewerComments.Name, Me.tabpReviewerComments.Text)
            Me.gbComments.Text = Language._Object.getCaption(Me.gbComments.Name, Me.gbComments.Text)
            Me.elRemark1.Text = Language._Object.getCaption(Me.elRemark1.Name, Me.elRemark1.Text)
            Me.elRemark2.Text = Language._Object.getCaption(Me.elRemark2.Name, Me.elRemark2.Text)
            Me.lnkViewAssessments.Text = Language._Object.getCaption(Me.lnkViewAssessments.Name, Me.lnkViewAssessments.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.dgcolhItems.HeaderText = Language._Object.getCaption(Me.dgcolhItems.Name, Me.dgcolhItems.HeaderText)
            Me.dgcolhWeight.HeaderText = Language._Object.getCaption(Me.dgcolhWeight.Name, Me.dgcolhWeight.HeaderText)
            Me.dgcolhScoreGuide.HeaderText = Language._Object.getCaption(Me.dgcolhScoreGuide.Name, Me.dgcolhScoreGuide.HeaderText)
            Me.dgcolhScore.HeaderText = Language._Object.getCaption(Me.dgcolhScore.Name, Me.dgcolhScore.HeaderText)
            Me.dgcolhResult.HeaderText = Language._Object.getCaption(Me.dgcolhResult.Name, Me.dgcolhResult.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 22, "Please set following information [Employee,Year,Period and Group] to view assessment.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalInterviewAnalysis
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalInterviewAnalysis))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.chkIncomplete = New System.Windows.Forms.CheckBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.lblBatch = New System.Windows.Forms.Label
        Me.lblInterviewType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboBatch = New System.Windows.Forms.ComboBox
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacanyType = New System.Windows.Forms.Label
        Me.gbBasicInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnInterviewer = New eZee.Common.eZeeGradientButton
        Me.cboInterviewer = New System.Windows.Forms.ComboBox
        Me.lblInterviewer = New System.Windows.Forms.Label
        Me.objbtnAddResult = New eZee.Common.eZeeGradientButton
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblScore = New System.Windows.Forms.Label
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvData = New Aruti.Data.GroupByGrid
        Me.objchkSelectAll_Budget = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBasicInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncomplete)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.chkComplete)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatch)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBatch)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacanyType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 4)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 92
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(335, 153)
        Me.gbFilterCriteria.TabIndex = 42
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(300, 63)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 49
        '
        'chkIncomplete
        '
        Me.chkIncomplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncomplete.Location = New System.Drawing.Point(3, 313)
        Me.chkIncomplete.Name = "chkIncomplete"
        Me.chkIncomplete.Size = New System.Drawing.Size(15, 17)
        Me.chkIncomplete.TabIndex = 42
        Me.chkIncomplete.Text = "Incomplete"
        Me.chkIncomplete.UseVisualStyleBackColor = True
        Me.chkIncomplete.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(309, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 32
        Me.objbtnReset.TabStop = False
        '
        'chkComplete
        '
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.Location = New System.Drawing.Point(3, 312)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(15, 17)
        Me.chkComplete.TabIndex = 41
        Me.chkComplete.Text = "Complete"
        Me.chkComplete.UseVisualStyleBackColor = True
        Me.chkComplete.Visible = False
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(92, 63)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(202, 21)
        Me.cboVacancy.TabIndex = 44
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(92, 120)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(202, 21)
        Me.cboInterviewType.TabIndex = 18
        '
        'lblBatch
        '
        Me.lblBatch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatch.Location = New System.Drawing.Point(8, 92)
        Me.lblBatch.Name = "lblBatch"
        Me.lblBatch.Size = New System.Drawing.Size(78, 16)
        Me.lblBatch.TabIndex = 47
        Me.lblBatch.Text = "Batch"
        Me.lblBatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterviewType
        '
        Me.lblInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewType.Location = New System.Drawing.Point(5, 122)
        Me.lblInterviewType.Name = "lblInterviewType"
        Me.lblInterviewType.Size = New System.Drawing.Size(81, 16)
        Me.lblInterviewType.TabIndex = 17
        Me.lblInterviewType.Text = "Interview Type"
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(5, 65)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(81, 16)
        Me.lblVacancy.TabIndex = 43
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(286, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 31
        Me.objbtnSearch.TabStop = False
        '
        'cboBatch
        '
        Me.cboBatch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatch.DropDownWidth = 300
        Me.cboBatch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatch.FormattingEnabled = True
        Me.cboBatch.Location = New System.Drawing.Point(92, 90)
        Me.cboBatch.Name = "cboBatch"
        Me.cboBatch.Size = New System.Drawing.Size(107, 21)
        Me.cboBatch.TabIndex = 48
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 300
        Me.cboVacancyType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(92, 33)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(107, 21)
        Me.cboVacancyType.TabIndex = 46
        '
        'lblVacanyType
        '
        Me.lblVacanyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacanyType.Location = New System.Drawing.Point(8, 35)
        Me.lblVacanyType.Name = "lblVacanyType"
        Me.lblVacanyType.Size = New System.Drawing.Size(78, 16)
        Me.lblVacanyType.TabIndex = 45
        Me.lblVacanyType.Text = "Vacancy Type"
        Me.lblVacanyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicInfo
        '
        Me.gbBasicInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBasicInfo.Checked = False
        Me.gbBasicInfo.CollapseAllExceptThis = False
        Me.gbBasicInfo.CollapsedHoverImage = Nothing
        Me.gbBasicInfo.CollapsedNormalImage = Nothing
        Me.gbBasicInfo.CollapsedPressedImage = Nothing
        Me.gbBasicInfo.CollapseOnLoad = False
        Me.gbBasicInfo.Controls.Add(Me.objbtnInterviewer)
        Me.gbBasicInfo.Controls.Add(Me.cboInterviewer)
        Me.gbBasicInfo.Controls.Add(Me.lblInterviewer)
        Me.gbBasicInfo.Controls.Add(Me.objbtnAddResult)
        Me.gbBasicInfo.Controls.Add(Me.cboResult)
        Me.gbBasicInfo.Controls.Add(Me.txtRemark)
        Me.gbBasicInfo.Controls.Add(Me.lblRemark)
        Me.gbBasicInfo.Controls.Add(Me.lblScore)
        Me.gbBasicInfo.Controls.Add(Me.btnApply)
        Me.gbBasicInfo.Controls.Add(Me.radApplyToAll)
        Me.gbBasicInfo.Controls.Add(Me.radApplyToChecked)
        Me.gbBasicInfo.ExpandedHoverImage = Nothing
        Me.gbBasicInfo.ExpandedNormalImage = Nothing
        Me.gbBasicInfo.ExpandedPressedImage = Nothing
        Me.gbBasicInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicInfo.HeaderHeight = 25
        Me.gbBasicInfo.HeaderMessage = ""
        Me.gbBasicInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBasicInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicInfo.HeightOnCollapse = 0
        Me.gbBasicInfo.LeftTextSpace = 0
        Me.gbBasicInfo.Location = New System.Drawing.Point(344, 4)
        Me.gbBasicInfo.Name = "gbBasicInfo"
        Me.gbBasicInfo.OpenHeight = 300
        Me.gbBasicInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicInfo.ShowBorder = True
        Me.gbBasicInfo.ShowCheckBox = False
        Me.gbBasicInfo.ShowCollapseButton = False
        Me.gbBasicInfo.ShowDefaultBorderColor = True
        Me.gbBasicInfo.ShowDownButton = False
        Me.gbBasicInfo.ShowHeader = True
        Me.gbBasicInfo.Size = New System.Drawing.Size(511, 153)
        Me.gbBasicInfo.TabIndex = 44
        Me.gbBasicInfo.Temp = 0
        Me.gbBasicInfo.Text = "Basic Information"
        Me.gbBasicInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnInterviewer
        '
        Me.objbtnInterviewer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnInterviewer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnInterviewer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnInterviewer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnInterviewer.BorderSelected = False
        Me.objbtnInterviewer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnInterviewer.Image = CType(resources.GetObject("objbtnInterviewer.Image"), System.Drawing.Image)
        Me.objbtnInterviewer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnInterviewer.Location = New System.Drawing.Point(278, 40)
        Me.objbtnInterviewer.Name = "objbtnInterviewer"
        Me.objbtnInterviewer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnInterviewer.TabIndex = 230
        '
        'cboInterviewer
        '
        Me.cboInterviewer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewer.FormattingEnabled = True
        Me.cboInterviewer.Location = New System.Drawing.Point(99, 36)
        Me.cboInterviewer.Name = "cboInterviewer"
        Me.cboInterviewer.Size = New System.Drawing.Size(173, 21)
        Me.cboInterviewer.TabIndex = 229
        '
        'lblInterviewer
        '
        Me.lblInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewer.Location = New System.Drawing.Point(24, 42)
        Me.lblInterviewer.Name = "lblInterviewer"
        Me.lblInterviewer.Size = New System.Drawing.Size(81, 15)
        Me.lblInterviewer.TabIndex = 228
        Me.lblInterviewer.Text = "Interviewer"
        Me.lblInterviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddResult
        '
        Me.objbtnAddResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResult.BorderSelected = False
        Me.objbtnAddResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResult.Location = New System.Drawing.Point(278, 64)
        Me.objbtnAddResult.Name = "objbtnAddResult"
        Me.objbtnAddResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResult.TabIndex = 226
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(99, 64)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(173, 21)
        Me.cboResult.TabIndex = 225
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(373, 36)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(131, 49)
        Me.txtRemark.TabIndex = 224
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(305, 36)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(63, 15)
        Me.lblRemark.TabIndex = 223
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScore
        '
        Me.lblScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.Location = New System.Drawing.Point(24, 67)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(69, 15)
        Me.lblScore.TabIndex = 222
        Me.lblScore.Text = "Score"
        Me.lblScore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(373, 94)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(89, 30)
        Me.btnApply.TabIndex = 9
        Me.btnApply.Text = "Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'radApplyToAll
        '
        Me.radApplyToAll.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToAll.Location = New System.Drawing.Point(264, 101)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(93, 17)
        Me.radApplyToAll.TabIndex = 5
        Me.radApplyToAll.TabStop = True
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = False
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.BackColor = System.Drawing.Color.Transparent
        Me.radApplyToChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplyToChecked.Location = New System.Drawing.Point(141, 101)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(116, 17)
        Me.radApplyToChecked.TabIndex = 4
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 467)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(860, 55)
        Me.objFooter.TabIndex = 45
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(641, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 126
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(744, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 125
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Interview Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 246
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Analysis Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Reviewer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Score"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhAppBatchTranId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhAnalysisId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhVacancyId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhApplicantId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhSortId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvData.IgnoreFirstColumn = False
        Me.dgvData.Location = New System.Drawing.Point(3, 163)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(852, 298)
        Me.dgvData.TabIndex = 194
        '
        'objchkSelectAll_Budget
        '
        Me.objchkSelectAll_Budget.AutoSize = True
        Me.objchkSelectAll_Budget.Location = New System.Drawing.Point(10, 171)
        Me.objchkSelectAll_Budget.Name = "objchkSelectAll_Budget"
        Me.objchkSelectAll_Budget.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll_Budget.TabIndex = 195
        Me.objchkSelectAll_Budget.UseVisualStyleBackColor = True
        Me.objchkSelectAll_Budget.Visible = False
        '
        'frmGlobalInterviewAnalysis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 522)
        Me.Controls.Add(Me.objchkSelectAll_Budget)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbBasicInfo)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalInterviewAnalysis"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Interview Analysis"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBasicInfo.ResumeLayout(False)
        Me.gbBasicInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents chkIncomplete As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents lblBatch As System.Windows.Forms.Label
    Friend WithEvents lblInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboBatch As System.Windows.Forms.ComboBox
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacanyType As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbBasicInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAddResult As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblScore As System.Windows.Forms.Label
    Friend WithEvents dgvData As Aruti.Data.GroupByGrid
    Friend WithEvents objchkSelectAll_Budget As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnInterviewer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboInterviewer As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterviewer As System.Windows.Forms.Label
End Class

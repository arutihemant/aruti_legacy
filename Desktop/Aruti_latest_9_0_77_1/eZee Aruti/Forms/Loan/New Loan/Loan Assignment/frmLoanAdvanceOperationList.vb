﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLoanAdvanceOperationList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceOperationList"
    Private objLnAdv As clsLoan_Advance
    Private mintLoanAdvanceUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private mintRefTranUnkid As Integer = 0
    Private mdtLoanEffectiveDate As String = ""
    Private objLoanRate As clslnloan_interest_tran
    Private objLoanEMI As clslnloan_emitenure_tran
    Private objLoanTopup As clslnloan_topup_tran
    Private mintEmployeeId As Integer = 0
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private objlnOpApprovaltran As clsloanotherop_approval_tran
    Private mstrFilter As String = String.Empty
    'Nilay (01-Apr-2016) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Private mdtList As DataTable
    'Nilay (13-Sept-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal strEmpName As String, _
                                  ByVal strLnScheme As String, _
                                  ByVal strLnVocNo As String, ByVal intLoanAdvanceUnkid As Integer) As Boolean
        Try
            txtEmployeeName.Text = strEmpName
            txtLoanScheme.Text = strLnScheme
            txtVocNo.Text = strLnVocNo
            mintLoanAdvanceUnkid = intLoanAdvanceUnkid

            Dim objLnAdv As New clsLoan_Advance
            objLnAdv._Loanadvancetranunkid = intLoanAdvanceUnkid

            mintEmployeeId = objLnAdv._Employeeunkid

            mdtLoanEffectiveDate = eZeeDate.convertDate(objLnAdv._Effective_Date).ToString

            objlblEffDate.Text = Language.getMessage(mstrModuleName, 8, "Loan Start Date :") & " " & objLnAdv._Effective_Date.ToShortDateString

            Call EnableDisable_Edit_Button(mintLoanAdvanceUnkid)

            objLnAdv = Nothing

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Dim objLoanApplication As New clsProcess_pending_loan
        Try
            dsCombo = objLoanApplication.GetLoan_Status("Status", False, False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Status")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Try
            dsList = objLnAdv.GetLoanOperationList(mintLoanAdvanceUnkid, "List")
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            mdtList = dsList.Tables("List")
            Call EnableDisable_Edit_Button()
            'Nilay (13-Sept-2016) -- End

            lvData.Items.Clear()

            For Each drow As DataRow In dsList.Tables(0).Rows
                Dim lvitem As New ListViewItem

                lvitem.Text = drow.Item("EffDate").ToString
                lvitem.SubItems.Add(drow.Item("Rate").ToString)
                lvitem.SubItems.Add(drow.Item("NoOfInstallment").ToString)
                lvitem.SubItems.Add(drow.Item("InstallmentAmount").ToString)
                lvitem.SubItems.Add(drow.Item("TopupAmount").ToString)
                lvitem.SubItems.Add(drow.Item("PeriodName").ToString)
                lvitem.SubItems(objcolhPeriod.Index).Tag = drow.Item("periodunkid").ToString
                lvitem.SubItems.Add(drow.Item("IdType").ToString)
                lvitem.SubItems.Add(drow.Item("statusid").ToString)
                lvitem.SubItems.Add(drow.Item("OprType").ToString)

                'Nilay (15-Dec-2015) -- Start
                'If mdtLoanEffectiveDate = drow.Item("oDate").ToString Then
                '    lvitem.ForeColor = Color.Blue
                'End If
                If mdtLoanEffectiveDate = drow.Item("oDate").ToString AndAlso CInt(drow.Item("IdType")) <> enParameterMode.LN_TOPUP Then
                    lvitem.ForeColor = Color.Blue
                End If
                'Nilay (15-Dec-2015) -- End

                If CInt(drow.Item("statusid").ToString) = enStatusType.Close Then
                    lvitem.ForeColor = Color.Gray
                End If
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                lvitem.SubItems.Add(drow.Item("isdefault").ToString)
                lvitem.SubItems.Add(drow.Item("approverunkid").ToString)
                lvitem.SubItems.Add(drow.Item("approver").ToString)
                'Nilay (01-Apr-2016) -- End

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                lvitem.SubItems.Add(drow.Item("identify_guid").ToString)
                'Nilay (13-Sept-2016) -- End

                lvitem.Tag = drow.Item("TabUnkid").ToString

                lvData.Items.Add(lvitem)
            Next

            lvData.GroupingColumn = objcolhPeriod
            lvData.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub FillPendingOpList()
        Try
            Dim dsList As DataSet = Nothing
            objlnOpApprovaltran = New clsloanotherop_approval_tran

            If CInt(cboStatus.SelectedValue) > 0 Then
                mstrFilter = " lnloanotherop_approval_tran.final_status = " & CInt(cboStatus.SelectedValue)
            End If

            dsList = objlnOpApprovaltran.getPendingOpList(mintLoanAdvanceUnkid, "List", mstrFilter)

            Dim mintloanadvancetranceunkid As Integer = 0
            Dim mintLoanOperationtypeId As Integer = 0
            Dim mstrIdentifyGuid As String = String.Empty
            Dim dtList As DataTable = Nothing
            Dim mstrStatus As String = String.Empty

            If dsList.Tables("List") IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                For Each drRow As DataRow In dsList.Tables("List").Rows
                    If CInt(drRow("lnotheroptranunkid")) <= 0 Then Continue For
                    mstrStatus = ""
                    If mintloanadvancetranceunkid <> CInt(drRow("loanadvancetranunkid")) OrElse mintLoanOperationtypeId <> CInt(drRow("lnoperationtypeid")) _
                                    OrElse mstrIdentifyGuid <> CStr(drRow("identify_guid")) Then
                        dtList = New DataView(dsList.Tables("List"), "employeeunkid= " & CInt(drRow("employeeunkid")) & _
                                              " AND loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & _
                                              " AND lnoperationtypeid = " & CInt(drRow("lnoperationtypeid")) & _
                                              " AND identify_guid = '" & CStr(drRow("identify_guid")) & "' ", "", DataViewRowState.CurrentRows).ToTable
                        mintloanadvancetranceunkid = CInt(drRow("loanadvancetranunkid"))
                        mintLoanOperationtypeId = CInt(drRow("lnoperationtypeid"))
                        mstrIdentifyGuid = CStr(drRow("identify_guid"))
                    End If
                    If dtList IsNot Nothing AndAlso dtList.Rows.Count > 0 Then
                        Dim dR As DataRow() = dtList.Select("priority >= " & CInt(drRow("priority")))
                        If dR.Length > 0 Then
                            For i As Integer = 0 To dR.Length - 1
                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStatus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For
                                ElseIf CInt(drRow("statusunkid")) = 1 Then
                                    If CInt(dR(i)("statusunkid")) = 2 Then
                                        mstrStatus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & dR(i)("ApproverName").ToString()
                                        Exit For
                                    ElseIf CInt(dR(i)("statusunkid")) = 3 Then
                                        mstrStatus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & dR(i)("ApproverName").ToString()
                                        Exit For
                                    End If
                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStatus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & dR(i)("ApproverName").ToString()
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If mstrStatus <> "" Then
                        drRow("Status") = mstrStatus.Trim
                    End If
                Next
            End If

            Dim dtTable As New DataTable
            If chkMyApprovals.Checked Then
                dtTable = New DataView(dsList.Tables(0), "MappedUserId = " & User._Object._Userunkid & " OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            dgPendingOp.AutoGenerateColumns = False
            dgcolhPeriodName.DataPropertyName = "PeriodName"
            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhEffectiveDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern

            dgcolhRate.DataPropertyName = "interest_rate"
            dgcolhRate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRate.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRate.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhInstlNo.DataPropertyName = "emi_tenure"
            dgcolhInstlNo.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstlNo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhInstlAmount.DataPropertyName = "emi_amount"
            dgcolhInstlAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstlAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstlAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhTopupAmount.DataPropertyName = "topup_amount"
            dgcolhTopupAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhTopupAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhTopupAmount.DefaultCellStyle.Format = GUI.fmtCurrency

            dgcolhOperation.DataPropertyName = "Operation"
            dgcolhApprover.DataPropertyName = "Approver"
            dgcolhStatus.DataPropertyName = "Status"
            dgcolhRemark.DataPropertyName = "remark"
            objdgcolhisgrp.DataPropertyName = "isgrp"
            objdgcolhloanOpTypeId.DataPropertyName = "lnoperationtypeid"
            objdgcolhmappeduserid.DataPropertyName = "MappedUserId"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            objdgcolhlnapproverunkid.DataPropertyName = "approvertranunkid"
            objdgcolhApproverEmpId.DataPropertyName = "approverempunkid"
            objdgcolhPriority.DataPropertyName = "priority"
            objdgcolhlnotheroptranunkid.DataPropertyName = "lnotheroptranunkid"
            objdgcolhIdentifyGuid.DataPropertyName = "identify_guid"
            objdgcolhStatusunkid.DataPropertyName = "statusunkid"
            objdgcolhLoanAdvanceTranunkid.DataPropertyName = "loanadvancetranunkid"
            'dgPendingOp.DataSource = dsList.Tables("List")
            dgPendingOp.DataSource = dtTable

            Dim mCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgPendingOp.Rows
                If CBool(dgvRow.Cells(objdgcolhisgrp.Index).Value) = True Then
                    dgvRow.ReadOnly = True
                    mCell.MakeMerge(dgPendingOp, dgvRow.Index, 0, dgvRow.Cells.Count - 1, Color.Gray, Color.White, _
                                    dgvRow.Cells(dgcolhPeriodName.Index).Value.ToString, Nothing, Nothing)
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPendingOpList", mstrModuleName)
        End Try
    End Sub

    Private Sub setGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgPendingOp.RowCount - 1
                If CBool(dgPendingOp.Rows(i).Cells(objdgcolhisgrp.Index).Value) = True Then
                    dgPendingOp.Rows(i).DefaultCellStyle = dgvcsHeader
                    dgPendingOp.Rows(i).Cells(dgcolhRate.Index).Style.ForeColor = Color.Gray
                    dgPendingOp.Rows(i).Cells(dgcolhRate.Index).Style.BackColor = Color.Gray
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setGridColor", mstrModuleName)
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- End


    Private Function IsEditAllowed(ByVal xColumnToMatch As String, _
                                   ByVal xColumnUnkid As Integer) As Boolean
        Try
            Dim objLnAdv As New clsLoan_Advance
            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
            Dim strMsg As String = String.Empty
            If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
                If ds.Tables("List").Rows.Count > 0 Then
                    If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 3, " Process Payroll.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 4, " Loan Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 5, " Receipt Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 6, " Loan Installment Change.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 7, " Loan Topup Added.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                        strMsg = Language.getMessage("clslnloan_interest_tran", 8, " Loan Rate Change.")
                    End If
                End If
            End If
            objLnAdv = Nothing
            If strMsg.Trim.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot edit this transaction. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsEditAllowed", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    'Private Sub EnableDisable_Edit_Button(ByVal intLnAdvId As Integer)
    Private Sub EnableDisable_Edit_Button(Optional ByVal intLnAdvId As Integer = 0)
        'Nilay (13-Sept-2016) -- End
        Try
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            If mdtList IsNot Nothing Then
                Dim dRow As DataRow() = mdtList.Select("isdefault=0")
                If dRow.Length > 0 Then
                    btnEdit.Enabled = False
                Else
                    btnEdit.Enabled = True
                End If
            End If
            'Nilay (13-Sept-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'Dim objLnAdv As New clsLoan_Advance
            'Dim dsList As New DataSet
            'dsList = objLnAdv.GetLastLoanBalance("List", intLnAdvId)
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    btnEdit.Enabled = False
            'Else
            '    btnEdit.Enabled = True
            'End If
            'objLnAdv = Nothing
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisable_Edit_Button", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmLoanAdvanceOperationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLnAdv = New clsLoan_Advance : objLoanRate = New clslnloan_interest_tran
        objLoanEMI = New clslnloan_emitenure_tran : objLoanTopup = New clslnloan_topup_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_List()
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            FillCombo()
            Call FillPendingOpList()
            tbOperationList.SelectedTab = tbPending
            'btnChangeStatus.Enabled = False
            'chkMyApprovals.Visible = False

            Call tbOperationList_SelectedIndexChanged(Nothing, Nothing)
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceOperationList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmLoanAdvanceOperationList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmLoanAdvanceOperationList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            Dim mstrFilter As String = ""
            Dim dsList As DataSet = Nothing
            objlnOpApprovaltran = New clsloanotherop_approval_tran

            Dim frm As New frmLoanParameter
            'frm._AllowChangeStatus = True
            ''frm.ShowDialog()

            If dgPendingOp.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Select atleast one record for further operation."), enMsgBoxStyle.Information)
                dgPendingOp.Select()
                Exit Sub
            End If

            If CBool(dgPendingOp.SelectedRows(0).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub

            If CBool(objlnOpApprovaltran.IsAllowedPendingOpChangeStatus(CStr(dgPendingOp.SelectedRows(0).Cells(objdgcolhIdentifyGuid.Index).Value), _
                                                                        CDate(dgPendingOp.SelectedRows(0).Cells(dgcolhEffectiveDate.Index).Value), _
                                                                        CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhLoanAdvanceTranunkid.Index).Value), _
                                                                        CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value)) = False) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot change status. Reason: Previous operation is not final approved yet."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Userunkid <> CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhmappeduserid.Index).Value) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "You cannot change status of this operation. Reason: You are logged in into another user login."), enMsgBoxStyle.Information)
                dgPendingOp.Select()
                Exit Sub
            End If

            If CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = 2 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information)
                dgPendingOp.Select()
                Exit Sub
            ElseIf CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhStatusunkid.Index).Value) = 3 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information)
                dgPendingOp.Select()
                Exit Sub
            End If

            mstrFilter &= " lnloanotherop_approval_tran.approverempunkid <> " & CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhApproverEmpId.Index).Value)

            dsList = objlnOpApprovaltran.getOtherOpApprovaltranList("List", _
                                                                    CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value), _
                                                                    CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value), _
                                                                    CStr(dgPendingOp.SelectedRows(0).Cells(objdgcolhIdentifyGuid.Index).Value), _
                                                                    mstrFilter)

            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

                    If CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhPriority.Index).Value) > CInt(dsList.Tables("List").Rows(i)("priority")) Then

                        Dim dtList As DataTable = New DataView(dsList.Tables("List"), "lnlevelunkid = " & CInt(dsList.Tables("List").Rows(i)("lnlevelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                        If dtList.Rows.Count > 0 Then Continue For

                        If CInt(dsList.Tables("List").Rows(i)("statusunkid")) = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                    ElseIf CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhPriority.Index).Value) <= CInt(dsList.Tables("List").Rows(i)("priority")) Then

                        If CInt(dsList.Tables("List").Rows(i)("statusunkid")) = 2 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), enMsgBoxStyle.Information)
                            Exit Sub
                        ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If frm.displayDialog(CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhlnotheroptranunkid.Index).Value), _
            '                   mintLoanAdvanceUnkid, enAction.EDIT_ONE, False, True, _
            '                   CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value), ) Then

            If frm.displayDialog(CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhlnotheroptranunkid.Index).Value), _
                           mintLoanAdvanceUnkid, enAction.EDIT_ONE, True, _
                           CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value)) Then
                'Nilay (01-Apr-2016) -- End


                Call FillPendingOpList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillPendingOpList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmLoanParameter
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If frm.displayDialog(-1, mintLoanAdvanceUnkid, enAction.ADD_ONE) Then
            If frm.displayDialog(-1, mintLoanAdvanceUnkid, enAction.ADD_ONE, False) Then
                'Nilay (01-Apr-2016) -- End
                Call Fill_List()
            End If
            Call EnableDisable_Edit_Button(mintLoanAdvanceUnkid)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmLoanParameter
        Dim blnFlag As Boolean = False 'Nilay (01-Apr-2016)
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'Select Case CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text)
            '    Case enParameterMode.LN_RATE
            '        If IsEditAllowed("lninteresttranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
            '    Case enParameterMode.LN_EMI
            '        If IsEditAllowed("lnemitranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
            '    Case enParameterMode.LN_TOPUP
            '        If IsEditAllowed("lntopuptranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
            'End Select

            'If frm.displayDialog(CInt(lvData.SelectedItems(0).Tag), mintLoanAdvanceUnkid, enAction.EDIT_ONE, _
            '                     CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text), False) Then
            '   Call Fill_List()
            'End If

            If tbOperationList.SelectedTab Is tbApproved Then

                If lvData.SelectedItems.Count < 1 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select one record to perform further operation."), enMsgBoxStyle.Information)
                    lvData.Select()
                    Exit Sub
                End If

            Select Case CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text)
                Case enParameterMode.LN_RATE
                    If IsEditAllowed("lninteresttranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
                Case enParameterMode.LN_EMI
                    If IsEditAllowed("lnemitranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
                Case enParameterMode.LN_TOPUP
                    If IsEditAllowed("lntopuptranunkid", CInt(lvData.SelectedItems(0).Tag)) = False Then Exit Sub
            End Select

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                Dim dsList As DataSet = Nothing
                objlnOpApprovaltran = New clsloanotherop_approval_tran

                mstrFilter = ""
                mstrFilter = " lnloanotherop_approval_tran.final_status = 1 AND lnloanotherop_approval_tran.statusunkid = 2 AND lnloanotherop_approval_tran.final_approved = 0 "

                dsList = objlnOpApprovaltran.getPendingOpList(mintLoanAdvanceUnkid, "List", mstrFilter)
                Dim dRow As DataRow() = dsList.Tables("List").Select("isgrp=0")
                If dRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot edit default operation. Reason: Some of the pending operation transaction(s) in approval process."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Nilay (13-Sept-2016) -- End

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'If frm.displayDialog(CInt(lvData.SelectedItems(0).Tag), mintLoanAdvanceUnkid, enAction.EDIT_ONE, True, False, _
                '         CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text), False) Then
                If frm.displayDialog(CInt(lvData.SelectedItems(0).Tag), _
                                     mintLoanAdvanceUnkid, _
                                     enAction.EDIT_ONE, _
                                     False, _
                     CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text)) Then
                    'Nilay (01-Apr-2016) -- End
                Call Fill_List()
            End If

            ElseIf tbOperationList.SelectedTab Is tbPending Then

                If CBool(dgPendingOp.SelectedRows(0).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub
                If dgPendingOp.SelectedRows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select one record to perform further operation."), enMsgBoxStyle.Information)
                    dgPendingOp.Select()
                    Exit Sub
                End If

                blnFlag = objlnOpApprovaltran.IsAllowEditPendingListParameters(mintLoanAdvanceUnkid, _
                                                                     CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value), _
                                                                     mintEmployeeId)
                If blnFlag = False AndAlso objlnOpApprovaltran._Message <> "" Then
                    eZeeMsgBox.Show(objlnOpApprovaltran._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'If frm.displayDialog(CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhlnotheroptranunkid.Index).Value), _
                '                     mintLoanAdvanceUnkid, enAction.EDIT_ONE, False, False, _
                '                     CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value), False) Then

                If frm.displayDialog(CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhlnotheroptranunkid.Index).Value), _
                                     mintLoanAdvanceUnkid, enAction.EDIT_ONE, False, _
                                 CInt(dgPendingOp.SelectedRows(0).Cells(objdgcolhloanOpTypeId.Index).Value)) Then
                    'Nilay (01-Apr-2016) -- End


                    Call FillPendingOpList()
                End If
            End If

            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim frm As New frmReasonSelection
        Try
            If lvData.SelectedItems.Count <= 0 Then Exit Sub


            Dim objTnaLeaveTran As New clsTnALeaveTran
            Dim objPeriod As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(lvData.SelectedItems(0).SubItems(objcolhPeriod.Index).Tag)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvData.SelectedItems(0).SubItems(objcolhPeriod.Index).Tag)
            'Nilay (10-Oct-2015) -- End

            If objTnaLeaveTran.IsPayrollProcessDone(CInt(lvData.SelectedItems(0).SubItems(objcolhPeriod.Index).Tag), _
                                                    mintEmployeeId.ToString, objPeriod._End_Date) Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot delete this entry. Reason : Payroll is already processed for this selected employee and period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot delete this entry. Reason : Payroll is already processed for this selected employee and period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                objPeriod = Nothing
                objTnaLeaveTran = Nothing
                Exit Sub
            End If
            objTnaLeaveTran = Nothing
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'objPeriod = Nothing
            'Nilay (01-Apr-2016) -- End

            Dim blnFlag As Boolean = False
            Dim strVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(enVoidCategoryType.LOAN, strVoidReason)

            If strVoidReason.Trim.Length > 0 Then
                Select Case CInt(lvData.SelectedItems(0).SubItems(objcolhIdType.Index).Text)
                    Case enParameterMode.LN_RATE
                        objLoanRate._Loanadvancetranunkid = mintLoanAdvanceUnkid
                        objLoanRate._Isvoid = True
                        objLoanRate._Voiduserunkid = User._Object._Userunkid
                        objLoanRate._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objLoanRate._Voidreason = strVoidReason

                        With objLoanRate
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        'Nilay (25-Mar-2016) -- Start
                        'blnFlag = objLoanRate.Delete(CInt(lvData.SelectedItems(0).Tag), True)
                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'blnFlag = objLoanRate.Delete(FinancialYear._Object._DatabaseName, _
                        '                             User._Object._Userunkid, _
                        '                             FinancialYear._Object._YearUnkid, _
                        '                             Company._Object._Companyunkid, _
                        '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                             ConfigParameter._Object._UserAccessModeSetting, _
                        '                             True, CInt(lvData.SelectedItems(0).Tag), True)
                        blnFlag = objLoanRate.Delete(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     objPeriod._Start_Date, _
                                                     objPeriod._End_Date, _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, CInt(lvData.SelectedItems(0).Tag), _
                                                    True, False)
                        'Nilay (20-Sept-2016) -- End

                        'Nilay (01-Apr-2016) -- End

                        'Nilay (25-Mar-2016) -- End

                        If blnFlag = False AndAlso objLoanRate._Message <> "" Then
                            eZeeMsgBox.Show(objLoanRate._Message, enMsgBoxStyle.Information)
                            Exit Select
                        End If
                    Case enParameterMode.LN_EMI
                        objLoanEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
                        objLoanEMI._Isvoid = True
                        objLoanEMI._Voiduserunkid = User._Object._Userunkid
                        objLoanEMI._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objLoanEMI._Voidreason = strVoidReason

                        With objLoanEMI
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        'Nilay (25-Mar-2016) -- Start
                        'blnFlag = objLoanEMI.Delete(CInt(lvData.SelectedItems(0).Tag), True)

                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations
                        'blnFlag = objLoanEMI.Delete(FinancialYear._Object._DatabaseName, _
                        '                            User._Object._Userunkid, _
                        '                            FinancialYear._Object._YearUnkid, _
                        '                            Company._Object._Companyunkid, _
                        '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                            ConfigParameter._Object._UserAccessModeSetting, _
                        '                            True, CInt(lvData.SelectedItems(0).Tag), True)

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'blnFlag = objLoanEMI.Delete(FinancialYear._Object._DatabaseName, _
                        '                            User._Object._Userunkid, _
                        '                            FinancialYear._Object._YearUnkid, _
                        '                            Company._Object._Companyunkid, _
                        '                            objPeriod._Start_Date, _
                        '                            objPeriod._End_Date, _
                        '                            ConfigParameter._Object._UserAccessModeSetting, _
                        '                            True, CInt(lvData.SelectedItems(0).Tag), True)
                        blnFlag = objLoanEMI.Delete(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    objPeriod._Start_Date, _
                                                    objPeriod._End_Date, _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, CInt(lvData.SelectedItems(0).Tag), _
                                                    True, False)
                        'Nilay (20-Sept-2016) -- End

                        'Nilay (01-Apr-2016) -- End

                        'Nilay (25-Mar-2016) -- End

                        If blnFlag = False AndAlso objLoanEMI._Message <> "" Then
                            eZeeMsgBox.Show(objLoanEMI._Message, enMsgBoxStyle.Information)
                            Exit Select
                        End If

                    Case enParameterMode.LN_TOPUP
                        objLoanTopup._Loanadvancetranunkid = mintLoanAdvanceUnkid
                        objLoanTopup._Isvoid = True
                        objLoanTopup._Voiduserunkid = User._Object._Userunkid
                        objLoanTopup._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objLoanTopup._Voidreason = strVoidReason

                        With objLoanTopup
                            ._FormName = mstrModuleName
                            ._Loginemployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        'Nilay (25-Mar-2016) -- Start
                        'blnFlag = objLoanTopup.Delete(CInt(lvData.SelectedItems(0).Tag), True)

                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations
                        'blnFlag = objLoanTopup.Delete(FinancialYear._Object._DatabaseName, _
                        '                              User._Object._Userunkid, _
                        '                              FinancialYear._Object._YearUnkid, _
                        '                              Company._Object._Companyunkid, _
                        '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                        '                              ConfigParameter._Object._UserAccessModeSetting, _
                        '                              True, _
                        '                              CInt(lvData.SelectedItems(0).Tag), True)

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'blnFlag = objLoanTopup.Delete(FinancialYear._Object._DatabaseName, _
                        '                             User._Object._Userunkid, _
                        '                             FinancialYear._Object._YearUnkid, _
                        '                             Company._Object._Companyunkid, _
                        '                             objPeriod._Start_Date, _
                        '                             objPeriod._End_Date, _
                        '                             ConfigParameter._Object._UserAccessModeSetting, _
                        '                             True, _
                        '                             CInt(lvData.SelectedItems(0).Tag), True)
                        blnFlag = objLoanTopup.Delete(FinancialYear._Object._DatabaseName, _
                                                      User._Object._Userunkid, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      objPeriod._Start_Date, _
                                                      objPeriod._End_Date, _
                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, _
                                                     CInt(lvData.SelectedItems(0).Tag), _
                                                     True, False)
                        'Nilay (20-Sept-2016) -- End

                        'Nilay (01-Apr-2016) -- End

                        'Nilay (25-Mar-2016) -- End

                        If blnFlag = False AndAlso objLoanTopup._Message <> "" Then
                            eZeeMsgBox.Show(objLoanTopup._Message, enMsgBoxStyle.Information)
                            Exit Select
                        End If

                End Select
            End If

            If blnFlag = True Then
                Call Fill_List()
            End If

            Call EnableDisable_Edit_Button(mintLoanAdvanceUnkid)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
#Region "CheckBox Event"

    Private Sub chkMyApprovals_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            FillPendingOpList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged_1", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (01-Apr-2016) -- End


#Region " Listview Events "

    Private Sub lvData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvData.SelectedIndexChanged
        Try
            Dim lstview As ListView = Nothing

            lstview = CType(sender, ListView)

            If lstview.SelectedItems.Count > 0 Then
                If lstview.SelectedItems(0).ForeColor = Color.Blue Or lstview.SelectedItems(0).ForeColor = Color.Gray Then
                    btnDelete.Enabled = False
                Else
                    btnDelete.Enabled = True
                End If
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                Call EnableDisable_Edit_Button()
                'Nilay (13-Sept-2016) -- End
            Else
                btnDelete.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvData_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
#Region "Tab Events"

    Private Sub tbOperationList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbOperationList.SelectedIndexChanged
        Try
            If tbOperationList.SelectedTab Is tbApproved Then
                btnNew.Enabled = True
                btnEdit.Enabled = False
                btnChangeStatus.Enabled = False
                pnlPendingControls.Enabled = False
                Call Fill_List()
            ElseIf tbOperationList.SelectedTab Is tbPending Then
                btnNew.Enabled = False
                btnEdit.Enabled = False
                btnDelete.Enabled = False
                btnChangeStatus.Enabled = True
                chkMyApprovals.Checked = True
                pnlPendingControls.Enabled = True
                cboStatus.SelectedIndex = 0
                Call FillPendingOpList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tbOperationList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    
#End Region
    'Nilay (01-Apr-2016) -- End

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
#Region "DataGrid Events"

    Private Sub dgPendingOp_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgPendingOp.CellFormatting
        Try
            If CBool(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhisgrp.Index).Value) = True Then
                If e.ColumnIndex <> dgcolhPeriodName.Index Then
                    e.Value = ""
                End If
            Else
                Select Case CInt(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhloanOpTypeId.Index).Value)
                    Case enParameterMode.LN_RATE
                        Select Case e.ColumnIndex
                            Case dgcolhInstlNo.Index
                                e.Value = ""
                            Case dgcolhInstlAmount.Index
                                e.Value = ""
                            Case dgcolhTopupAmount.Index
                                e.Value = ""
                        End Select

                    Case enParameterMode.LN_EMI
                        Select Case e.ColumnIndex
                            Case dgcolhRate.Index
                                e.Value = ""
                            Case dgcolhTopupAmount.Index
                                e.Value = ""
                        End Select

                    Case enParameterMode.LN_TOPUP
                        Select Case e.ColumnIndex
                            Case dgcolhRate.Index
                                e.Value = ""
                            Case dgcolhInstlNo.Index
                                e.Value = ""
                            Case dgcolhInstlAmount.Index
                                e.Value = ""
                        End Select

                End Select

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPendingOp_CellFormatting", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPendingOp_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgPendingOp.DataBindingComplete
        Try
            Call setGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPendingOp_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPendingOp_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgPendingOp.RowPostPaint
        Try
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If e.RowIndex <= -1 Then Exit Sub
            'If CBool(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub
            ''If CBool(dgPendingOp.Rows(e.RowIndex - 1).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub
            'If e.RowIndex = dgPendingOp.Rows.Count - 1 Then Exit Sub
            'If CInt(dgPendingOp.Rows(e.RowIndex + 1).Cells(objdgcolhloanOpTypeId.Index).Value) <> CInt(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhloanOpTypeId.Index).Value) Then
            '    'e.Graphics.DrawLine(Pens.Gray, e.RowBounds.Left, e.RowBounds.Bottom - 1, e.RowBounds.Right, e.RowBounds.Bottom - 1)
            '    If CStr(dgPendingOp.Rows(e.RowIndex + 1).Cells(objdgcolhIdentifyGuid.Index).Value) <> CStr(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhIdentifyGuid.Index).Value) Then
            '        e.Graphics.DrawLine(Pens.Gray, e.RowBounds.Left, e.RowBounds.Bottom - 1, e.RowBounds.Right, e.RowBounds.Bottom - 1)
            '    End If
            'End If

            If e.RowIndex <= -1 Then Exit Sub
            If CBool(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub
            'If CBool(dgPendingOp.Rows(e.RowIndex - 1).Cells(objdgcolhisgrp.Index).Value) = True Then Exit Sub
            If e.RowIndex = dgPendingOp.Rows.Count - 1 Then Exit Sub
            If CStr(dgPendingOp.Rows(e.RowIndex + 1).Cells(objdgcolhIdentifyGuid.Index).Value) <> CStr(dgPendingOp.Rows(e.RowIndex).Cells(objdgcolhIdentifyGuid.Index).Value) Then
                e.Graphics.DrawLine(Pens.Gray, e.RowBounds.Left, e.RowBounds.Bottom - 1, e.RowBounds.Right, e.RowBounds.Bottom - 1)
            End If
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPendingOp_RowPostPaint", mstrModuleName)
        End Try
    End Sub

    Private Sub dgPendingOp_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgPendingOp.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgPendingOp_DataError", mstrModuleName)
        End Try
    End Sub

#End Region
    'Nilay (01-Apr-2016) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblVocNo.Text = Language._Object.getCaption(Me.lblVocNo.Name, Me.lblVocNo.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.colhEffectiveDate.Text = Language._Object.getCaption(CStr(Me.colhEffectiveDate.Tag), Me.colhEffectiveDate.Text)
            Me.colhRate.Text = Language._Object.getCaption(CStr(Me.colhRate.Tag), Me.colhRate.Text)
            Me.colhINSTLNo.Text = Language._Object.getCaption(CStr(Me.colhINSTLNo.Tag), Me.colhINSTLNo.Text)
            Me.colhINSTLAmt.Text = Language._Object.getCaption(CStr(Me.colhINSTLAmt.Tag), Me.colhINSTLAmt.Text)
            Me.colhTopup.Text = Language._Object.getCaption(CStr(Me.colhTopup.Tag), Me.colhTopup.Text)
			Me.colhChangeType.Text = Language._Object.getCaption(CStr(Me.colhChangeType.Tag), Me.colhChangeType.Text)
            Me.tbApproved.Text = Language._Object.getCaption(Me.tbApproved.Name, Me.tbApproved.Text)
            Me.tbPending.Text = Language._Object.getCaption(Me.tbPending.Name, Me.tbPending.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.dgcolhPeriodName.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodName.Name, Me.dgcolhPeriodName.HeaderText)
            Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
            Me.dgcolhOperation.HeaderText = Language._Object.getCaption(Me.dgcolhOperation.Name, Me.dgcolhOperation.HeaderText)
            Me.dgcolhRate.HeaderText = Language._Object.getCaption(Me.dgcolhRate.Name, Me.dgcolhRate.HeaderText)
            Me.dgcolhInstlNo.HeaderText = Language._Object.getCaption(Me.dgcolhInstlNo.Name, Me.dgcolhInstlNo.HeaderText)
            Me.dgcolhInstlAmount.HeaderText = Language._Object.getCaption(Me.dgcolhInstlAmount.Name, Me.dgcolhInstlAmount.HeaderText)
            Me.dgcolhTopupAmount.HeaderText = Language._Object.getCaption(Me.dgcolhTopupAmount.Name, Me.dgcolhTopupAmount.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage("clslnloan_interest_tran", 3, " Process Payroll.")
			Language.setMessage("clslnloan_interest_tran", 4, " Loan Payment List.")
			Language.setMessage("clslnloan_interest_tran", 5, " Receipt Payment List.")
			Language.setMessage("clslnloan_interest_tran", 6, " Loan Installment Change.")
			Language.setMessage("clslnloan_interest_tran", 7, " Loan Topup Added.")
            Language.setMessage("clslnloan_interest_tran", 8, " Loan Rate Change.")
			Language.setMessage(mstrModuleName, 8, "Loan Start Date :")
			Language.setMessage(mstrModuleName, 9, "Sorry, You cannot edit this transaction. Please delete last transaction from the screen of")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot delete this entry. Reason : Payroll is already processed for this selected employee and period.")
            Language.setMessage(mstrModuleName, 11, "Approved By :-")
            Language.setMessage(mstrModuleName, 12, "Rejected By :-")
            Language.setMessage(mstrModuleName, 13, "Select atleast one record for further operation.")
            Language.setMessage(mstrModuleName, 14, "You cannot change status of this operation. Reason: You are logged in into another user login.")
            Language.setMessage(mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Language.setMessage(mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
            Language.setMessage(mstrModuleName, 17, "Sorry, you cannot change status. Reason: Previous operation is not final approved yet.")
            Language.setMessage(mstrModuleName, 18, "Please select one record to perform further operation.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot edit default operation. Reason: Some of the pending operation transaction(s) in approval process.")
			Language.setMessage(mstrModuleName, 20, "Do you want to void Payroll?")
			Language.setMessage("clslnloan_interest_tran", 8, " Loan Rate Change.")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'Public Class frmLoanAdvanceOperationList1

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceOperationList"
'    Private objlnInterest As clslnloan_interest_tran
'    Private objlnEMI As clslnloan_emitenure_tran
'    Private objlnTopUp As clslnloan_topup_tran
'    Private mdtInterest As DataTable
'    Private mdtEMI As DataTable
'    Private mdtTopup As DataTable
'    Private mintLoanAdvanceUnkid As Integer = -1
'    Private mblnCancel As Boolean = True
'    Private mintRefTranUnkid As Integer = 0
'    Private mdtLoanEffectiveDate As String = ""

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByVal strEmpName As String, _
'                                  ByVal strLnScheme As String, _
'                                  ByVal strLnVocNo As String, ByVal intLoanAdvanceUnkid As Integer) As Boolean
'        Try
'            txtEmployeeName.Text = strEmpName
'            txtLoanScheme.Text = strLnScheme
'            txtVocNo.Text = strLnVocNo
'            mintLoanAdvanceUnkid = intLoanAdvanceUnkid

'            Dim objLnAdv As New clsLoan_Advance
'            objLnAdv._Loanadvancetranunkid = intLoanAdvanceUnkid
'            mdtLoanEffectiveDate = eZeeDate.convertDate(objLnAdv._Effective_Date).ToString
'            objLnAdv = Nothing

'            Call FillCombo()
'            'Call Form_SetUp()

'            Me.ShowDialog()

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'#Region " Form's Events "

'    Private Sub frmLoanAdvanceOperationList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objlnEMI = New clslnloan_emitenure_tran : objlnTopUp = New clslnloan_topup_tran : objlnInterest = New clslnloan_interest_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceOperationList_Load", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub frmLoanAdvanceOperationList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
'        If e.KeyCode = Keys.Delete Then
'            Call btnDelete.PerformClick()
'        End If
'    End Sub

'    Private Sub frmLoanAdvanceOperationList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        If Asc(e.KeyChar) = 27 Then
'            Me.Close()
'        End If
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsLoan_Advance.SetMessages()
'            objfrm._Other_ModuleNames = "clsLoan_Advance"
'            objfrm.displayDialog(Me)


'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Try
'            With cboParameterType
'                .Items.Clear()
'                .Items.Add(Language.getMessage(mstrModuleName, 1, "Loan Interest Rate"))
'                .Items.Add(Language.getMessage(mstrModuleName, 2, "Loan EMI Information"))
'                .Items.Add(Language.getMessage(mstrModuleName, 3, "Loan Topup Information"))
'                .SelectedIndex = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Setup_Form_View()
'        Try
'            Select Case (cboParameterType.SelectedIndex) + 1
'                Case enParameterMode.LN_RATE
'                    lvEMI.Visible = False : lvTopUp.Visible = False : lvRate.Visible = True
'                    lvRate.Dock = DockStyle.Fill
'                    lvRate.BringToFront()
'                Case enParameterMode.LN_EMI
'                    lvEMI.Visible = True : lvTopUp.Visible = False : lvRate.Visible = False
'                    lvEMI.Dock = DockStyle.Fill
'                    lvEMI.BringToFront()
'                Case enParameterMode.LN_TOPUP
'                    lvEMI.Visible = False : lvTopUp.Visible = True : lvRate.Visible = False
'                    lvTopUp.Dock = DockStyle.Fill
'                    lvTopUp.BringToFront()
'            End Select

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Setup_Form_View", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Rate_List()
'        Try
'            objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'            mdtInterest = objlnInterest._DataTable
'            lvRate.Items.Clear()
'            For Each row As DataRow In mdtInterest.Rows
'                If row.Item("AUD").ToString = "D" Then Continue For

'                If CInt(row.Item("loanadvancetranunkid")) > 0 Then
'                    Dim lvItem As New ListViewItem

'                    lvItem.Text = row.Item("ddate").ToString
'                    lvItem.SubItems.Add(row.Item("interest_rate").ToString)
'                    lvItem.SubItems.Add(row.Item("dperiod").ToString)
'                    lvItem.SubItems(objcolhRPeriod.Index).Tag = row.Item("pstatusid").ToString
'                    lvItem.SubItems.Add(row.Item("periodunkid").ToString)
'                    If mdtLoanEffectiveDate = eZeeDate.convertDate(CDate(row.Item("ddate"))).ToString Then
'                        lvItem.ForeColor = Color.Blue
'                    End If
'                    lvItem.Tag = row.Item("lninteresttranunkid").ToString

'                    lvRate.Items.Add(lvItem)

'                End If

'            Next
'            lvRate.GroupingColumn = objcolhRPeriod
'            lvRate.DisplayGroups(True)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_Rate_List", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_EMI_List()
'        Try
'            objlnEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
'            mdtEMI = objlnEMI._DataTable
'            lvEMI.Items.Clear()
'            For Each row As DataRow In mdtEMI.Rows
'                If row.Item("AUD").ToString = "D" Then Continue For
'                If CInt(row.Item("loanadvancetranunkid")) > 0 Then
'                    Dim lvItem As New ListViewItem

'                    lvItem.Text = row.Item("ddate").ToString
'                    lvItem.SubItems.Add(row.Item("emi_tenure").ToString)
'                    lvItem.SubItems.Add(row.Item("loan_duration").ToString)
'                    lvItem.SubItems.Add(row.Item("dperiod").ToString)
'                    lvItem.SubItems(objcolhRPeriod.Index).Tag = row.Item("pstatusid").ToString

'                    If mdtLoanEffectiveDate = eZeeDate.convertDate(CDate(row.Item("ddate"))).ToString Then
'                        lvItem.ForeColor = Color.Blue
'                    End If

'                    lvItem.Tag = row.Item("lnemitranunkid").ToString

'                    lvEMI.Items.Add(lvItem)
'                End If
'            Next

'            lvEMI.GroupingColumn = objcolhEPeriod
'            lvEMI.DisplayGroups(True)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_EMI_List", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_TopUp_List()
'        Try
'            objlnTopUp._Loanadvancetranunkid = mintLoanAdvanceUnkid
'            mdtTopup = objlnTopUp._DataTable
'            lvTopUp.Items.Clear()
'            For Each row As DataRow In mdtTopup.Rows
'                If row.Item("AUD").ToString = "D" Then Continue For

'                If CInt(row.Item("loanadvancetranunkid")) > 0 Then
'                    Dim lvItem As New ListViewItem

'                    lvItem.Text = row.Item("ddate").ToString
'                    lvItem.SubItems.Add(Format(CDec(row.Item("topup_amount")), GUI.fmtCurrency))
'                    lvItem.SubItems.Add(row.Item("dperiod").ToString)
'                    lvItem.SubItems(objcolhRPeriod.Index).Tag = row.Item("pstatusid").ToString

'                    If mdtLoanEffectiveDate = eZeeDate.convertDate(CDate(row.Item("ddate"))).ToString Then
'                        lvItem.ForeColor = Color.Blue
'                    End If

'                    lvItem.Tag = row.Item("lntopuptranunkid").ToString

'                    lvTopUp.Items.Add(lvItem)
'                End If
'            Next
'            lvTopUp.GroupingColumn = objcolhTPeriod
'            lvTopUp.DisplayGroups(True)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_TopUp_List", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function IsEditAllowed(ByVal xColumnToMatch As String, _
'                                   ByVal xColumnUnkid As Integer) As Boolean
'        Try
'            Dim objLnAdv As New clsLoan_Advance
'            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceUnkid)
'            Dim strMsg As String = String.Empty
'            If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
'                If ds.Tables("List").Rows.Count > 0 Then
'                    If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 1, " Process Payroll.")
'                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 2, " Loan Payment List.")
'                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 3, " Receipt Payment List.")
'                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 4, " Loan Operation (EMI).")
'                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 5, " Loan Operation (Topup).")
'                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
'                        strMsg = Language.getMessage("clslnloan_interest_tran", 6, " Loan Operation (Rate).")
'                    End If
'                End If
'            End If
'            objLnAdv = Nothing
'            If strMsg.Trim.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot edit this transaction. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
'                Return False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsEditAllowed", mstrModuleName)
'        Finally
'        End Try
'        Return True
'    End Function

'#End Region

'#Region " Button's Events "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Dim frm As New frmLoanParameter
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If frm.displayDialog(-1, CType((CInt(cboParameterType.SelectedIndex) + 1), enParameterMode), mintLoanAdvanceUnkid, enAction.ADD_ONE) Then
'                Select Case (cboParameterType.SelectedIndex) + 1
'                    Case enParameterMode.LN_RATE
'                        Call Fill_Rate_List()
'                    Case enParameterMode.LN_EMI
'                        Call Fill_EMI_List()
'                    Case enParameterMode.LN_TOPUP
'                        Call Fill_TopUp_List()
'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        Dim frm As New frmLoanParameter
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            Select Case (cboParameterType.SelectedIndex) + 1
'                Case enParameterMode.LN_RATE
'                    If IsEditAllowed("lninteresttranunkid", CInt(lvRate.SelectedItems(0).Tag)) = False Then Exit Sub
'                    If frm.displayDialog(CInt(lvRate.SelectedItems(0).Tag), CType((CInt(cboParameterType.SelectedIndex) + 1), enParameterMode), mintLoanAdvanceUnkid, enAction.EDIT_ONE, False) Then
'                        Call Fill_Rate_List()
'                    End If
'                Case enParameterMode.LN_EMI
'                    If IsEditAllowed("lnemitranunkid", CInt(lvEMI.SelectedItems(0).Tag)) = False Then Exit Sub
'                    If frm.displayDialog(CInt(lvEMI.SelectedItems(0).Tag), CType((CInt(cboParameterType.SelectedIndex) + 1), enParameterMode), mintLoanAdvanceUnkid, enAction.EDIT_ONE, False) Then
'                        Call Fill_EMI_List()
'                    End If
'                Case enParameterMode.LN_TOPUP
'                    If IsEditAllowed("lntopuptranunkid", CInt(lvTopUp.SelectedItems(0).Tag)) = False Then Exit Sub
'                    If frm.displayDialog(CInt(lvTopUp.SelectedItems(0).Tag), CType((CInt(cboParameterType.SelectedIndex) + 1), enParameterMode), mintLoanAdvanceUnkid, enAction.EDIT_ONE, False) Then
'                        Call Fill_TopUp_List()
'                    End If
'            End Select

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Dim frm As New frmReasonSelection
'        Try
'            Dim blnFlag As Boolean = False
'            Dim strVoidReason As String = String.Empty

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.displayDialog(enVoidCategoryType.LOAN, strVoidReason)

'            If strVoidReason.Trim.Length > 0 Then

'                Select Case (cboParameterType.SelectedIndex) + 1
'                    Case enParameterMode.LN_RATE

'                        objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
'                        mdtInterest = objlnInterest._DataTable : mdtInterest.Rows.Clear()

'                        Dim dRow As DataRow = mdtInterest.NewRow()
'                        dRow.Item("periodunkid") = lvRate.SelectedItems(0).SubItems(objcolhPeriodId.Index).Text
'                        dRow.Item("lninteresttranunkid") = lvRate.SelectedItems(0).Tag
'                        dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
'                        dRow.Item("isvoid") = True
'                        dRow.Item("voiduserunkid") = User._Object._Userunkid
'                        dRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
'                        dRow.Item("voidreason") = strVoidReason
'                        dRow.Item("AUD") = "D"
'                        mdtInterest.Rows.Add(dRow)

'                        blnFlag = objlnInterest.InsertUpdateDelete_InerestRate_Tran(, True)

'                        If blnFlag = False AndAlso objlnInterest._Message <> "" Then
'                            eZeeMsgBox.Show(objlnInterest._Message, enMsgBoxStyle.Information)
'                        Else
'                            Call Fill_Rate_List()
'                        End If

'                    Case enParameterMode.LN_EMI
'                    Case enParameterMode.LN_TOPUP
'                End Select

'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Select Case (cboParameterType.SelectedIndex) + 1
'                Case enParameterMode.LN_RATE : Call Fill_Rate_List()
'                Case enParameterMode.LN_EMI : Call Fill_EMI_List()
'                Case enParameterMode.LN_TOPUP : Call Fill_TopUp_List()
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            Select Case (cboParameterType.SelectedIndex) + 1
'                Case enParameterMode.LN_RATE : lvRate.Items.Clear()
'                Case enParameterMode.LN_EMI : lvEMI.Items.Clear()
'                Case enParameterMode.LN_TOPUP : lvTopUp.Items.Clear()
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Combo Event "

'    Private Sub cboParameterType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboParameterType.SelectedIndexChanged
'        Try
'            Call Setup_Form_View()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboParameterType_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " List View Events "

'    Private Sub lvRate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvRate.SelectedIndexChanged, lvEMI.SelectedIndexChanged, lvTopUp.SelectedIndexChanged
'        Try
'            Dim lstview As ListView = Nothing

'            lstview = CType(sender, ListView)

'            If lstview.SelectedItems.Count > 0 Then
'                If lstview.SelectedItems(0).ForeColor = Color.Blue Then
'                    btnDelete.Enabled = False
'                Else
'                    btnDelete.Enabled = True
'                End If
'            Else
'                btnDelete.Enabled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvRate_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'    '<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'    Private Sub OtherSettings()
'        Try
'            Me.SuspendLayout()

'            Call SetLanguage()

'            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
'            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

'            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
'            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

'            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
'            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

'            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
'            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


'            Me.ResumeLayout()
'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetLanguage()
'        Try
'            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

'            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
'            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
'            Me.lblVocNo.Text = Language._Object.getCaption(Me.lblVocNo.Name, Me.lblVocNo.Text)
'            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
'            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
'            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
'            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
'            Me.lblLoanOperation.Text = Language._Object.getCaption(Me.lblLoanOperation.Name, Me.lblLoanOperation.Text)
'            Me.colhIEffDate.Text = Language._Object.getCaption(CStr(Me.colhIEffDate.Tag), Me.colhIEffDate.Text)
'            Me.colhIRate.Text = Language._Object.getCaption(CStr(Me.colhIRate.Tag), Me.colhIRate.Text)
'            Me.colhTEffDate.Text = Language._Object.getCaption(CStr(Me.colhTEffDate.Tag), Me.colhTEffDate.Text)
'            Me.colhTopup.Text = Language._Object.getCaption(CStr(Me.colhTopup.Tag), Me.colhTopup.Text)
'            Me.colhEEffDate.Text = Language._Object.getCaption(CStr(Me.colhEEffDate.Tag), Me.colhEEffDate.Text)
'            Me.colhNoOfInstallment.Text = Language._Object.getCaption(CStr(Me.colhNoOfInstallment.Tag), Me.colhNoOfInstallment.Text)
'            Me.colhDuration.Text = Language._Object.getCaption(CStr(Me.colhDuration.Tag), Me.colhDuration.Text)

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
'        End Try
'    End Sub


'    Private Sub SetMessages()
'        Try
'            Language.setMessage(mstrModuleName, 1, "Loan Interest Rate")
'            Language.setMessage(mstrModuleName, 2, "Loan EMI Information")
'            Language.setMessage(mstrModuleName, 3, "Loan Topup Information")

'        Catch Ex As Exception
'            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
'        End Try
'    End Sub
'#End Region 'Language & UI Settings
'    '</Language>

'End Class
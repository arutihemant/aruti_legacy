﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanAdvanceList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanAdvanceList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvLoanAdvance = New eZee.Common.eZeeListView(Me.components)
        Me.colhVocNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhLoanCode = New System.Windows.Forms.ColumnHeader
        Me.colhLoanScheme = New System.Windows.Forms.ColumnHeader
        Me.colhLoan_Advance = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhInstallments = New System.Windows.Forms.ColumnHeader
        Me.colhBalance = New System.Windows.Forms.ColumnHeader
        Me.colhPaidLoan = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhIsLoan = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.txtVocNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblVocNo = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchTrnHead = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnPreview = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPayment = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuReceived = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuViewApprovalForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuChangeStatus = New System.Windows.Forms.ToolStripMenuItem
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvLoanAdvance)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(797, 443)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvLoanAdvance
        '
        Me.lvLoanAdvance.BackColorOnChecked = True
        Me.lvLoanAdvance.ColumnHeaders = Nothing
        Me.lvLoanAdvance.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhVocNo, Me.colhEmpCode, Me.colhEmployee, Me.colhLoanCode, Me.colhLoanScheme, Me.colhLoan_Advance, Me.colhAmount, Me.colhInstallments, Me.colhBalance, Me.colhPaidLoan, Me.colhPeriod, Me.colhStatus, Me.colhIsLoan, Me.objcolhStatusUnkid, Me.objcolhemployeeunkid})
        Me.lvLoanAdvance.CompulsoryColumns = ""
        Me.lvLoanAdvance.FullRowSelect = True
        Me.lvLoanAdvance.GridLines = True
        Me.lvLoanAdvance.GroupingColumn = Nothing
        Me.lvLoanAdvance.HideSelection = False
        Me.lvLoanAdvance.Location = New System.Drawing.Point(9, 166)
        Me.lvLoanAdvance.MinColumnWidth = 50
        Me.lvLoanAdvance.MultiSelect = False
        Me.lvLoanAdvance.Name = "lvLoanAdvance"
        Me.lvLoanAdvance.OptionalColumns = ""
        Me.lvLoanAdvance.ShowMoreItem = False
        Me.lvLoanAdvance.ShowSaveItem = False
        Me.lvLoanAdvance.ShowSelectAll = True
        Me.lvLoanAdvance.ShowSizeAllColumnsToFit = True
        Me.lvLoanAdvance.Size = New System.Drawing.Size(773, 216)
        Me.lvLoanAdvance.Sortable = False
        Me.lvLoanAdvance.TabIndex = 4
        Me.lvLoanAdvance.UseCompatibleStateImageBehavior = False
        Me.lvLoanAdvance.View = System.Windows.Forms.View.Details
        '
        'colhVocNo
        '
        Me.colhVocNo.Tag = "colhVocNo"
        Me.colhVocNo.Text = "Voc #"
        Me.colhVocNo.Width = 100
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Employee Code"
        Me.colhEmpCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 170
        '
        'colhLoanCode
        '
        Me.colhLoanCode.Tag = "colhLoanCode"
        Me.colhLoanCode.Text = "Loan Code"
        Me.colhLoanCode.Width = 90
        '
        'colhLoanScheme
        '
        Me.colhLoanScheme.Tag = "colhLoanScheme"
        Me.colhLoanScheme.Text = "Loan Scheme"
        Me.colhLoanScheme.Width = 110
        '
        'colhLoan_Advance
        '
        Me.colhLoan_Advance.Tag = "colhLoan_Advance"
        Me.colhLoan_Advance.Text = "Loan / Advance"
        Me.colhLoan_Advance.Width = 95
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 95
        '
        'colhInstallments
        '
        Me.colhInstallments.Tag = "colhInstallments"
        Me.colhInstallments.Text = "No. Of Installments"
        Me.colhInstallments.Width = 80
        '
        'colhBalance
        '
        Me.colhBalance.Tag = "colhBalance"
        Me.colhBalance.Text = "Current Balance"
        Me.colhBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBalance.Width = 100
        '
        'colhPaidLoan
        '
        Me.colhPaidLoan.Tag = "colhPaidLoan"
        Me.colhPaidLoan.Text = "Paid Loan"
        Me.colhPaidLoan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhPaidLoan.Width = 100
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 95
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'colhIsLoan
        '
        Me.colhIsLoan.Tag = "colhIsLoan"
        Me.colhIsLoan.Text = ""
        Me.colhIsLoan.Width = 0
        '
        'objcolhStatusUnkid
        '
        Me.objcolhStatusUnkid.Tag = "objcolhStatusUnkid"
        Me.objcolhStatusUnkid.Text = ""
        Me.objcolhStatusUnkid.Width = 0
        '
        'objcolhemployeeunkid
        '
        Me.objcolhemployeeunkid.Tag = "objcolhemployeeunkid"
        Me.objcolhemployeeunkid.Text = ""
        Me.objcolhemployeeunkid.Width = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(797, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Loan / Advance List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objLoanSchemeSearch)
        Me.gbFilterCriteria.Controls.Add(Me.txtVocNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmount)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmount)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblVocNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(776, 94)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(563, 33)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objLoanSchemeSearch.TabIndex = 5
        '
        'txtVocNo
        '
        Me.txtVocNo.Flags = 0
        Me.txtVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVocNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVocNo.Location = New System.Drawing.Point(656, 33)
        Me.txtVocNo.Name = "txtVocNo"
        Me.txtVocNo.Size = New System.Drawing.Size(106, 21)
        Me.txtVocNo.TabIndex = 7
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(584, 63)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(54, 15)
        Me.lblAmount.TabIndex = 14
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(656, 60)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(106, 21)
        Me.txtAmount.TabIndex = 15
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.DropDownWidth = 150
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(92, 60)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(111, 21)
        Me.cboLoanAdvance.TabIndex = 9
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(78, 15)
        Me.lblLoanAdvance.TabIndex = 8
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 100
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(263, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(85, 21)
        Me.cboStatus.TabIndex = 11
        '
        'lblVocNo
        '
        Me.lblVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVocNo.Location = New System.Drawing.Point(584, 35)
        Me.lblVocNo.Name = "lblVocNo"
        Me.lblVocNo.Size = New System.Drawing.Size(66, 16)
        Me.lblVocNo.TabIndex = 6
        Me.lblVocNo.Text = "Voucher No."
        Me.lblVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(209, 62)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(50, 16)
        Me.lblStatus.TabIndex = 10
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(380, 36)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(70, 15)
        Me.lblLoanScheme.TabIndex = 3
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(452, 33)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(107, 21)
        Me.cboLoanScheme.TabIndex = 4
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(452, 60)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(107, 21)
        Me.cboPayPeriod.TabIndex = 13
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(360, 62)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(85, 16)
        Me.lblPayPeriod.TabIndex = 12
        Me.lblPayPeriod.Text = "Assigned Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTrnHead
        '
        Me.objbtnSearchTrnHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrnHead.BorderSelected = False
        Me.objbtnSearchTrnHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrnHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrnHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrnHead.Location = New System.Drawing.Point(354, 33)
        Me.objbtnSearchTrnHead.Name = "objbtnSearchTrnHead"
        Me.objbtnSearchTrnHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrnHead.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(92, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(256, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(66, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(749, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(726, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnPreview)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 388)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(797, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.BackColor = System.Drawing.Color.White
        Me.btnPreview.BackgroundImage = CType(resources.GetObject("btnPreview.BackgroundImage"), System.Drawing.Image)
        Me.btnPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPreview.BorderColor = System.Drawing.Color.Empty
        Me.btnPreview.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.Color.Black
        Me.btnPreview.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPreview.GradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Location = New System.Drawing.Point(116, 13)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Size = New System.Drawing.Size(105, 30)
        Me.btnPreview.TabIndex = 6
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.ContextMenuStrip = Me.mnuOperations
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(98, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 5
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayment, Me.mnuReceived, Me.objSep1, Me.mnuViewApprovalForm, Me.mnuChangeStatus})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(188, 98)
        '
        'mnuPayment
        '
        Me.mnuPayment.Name = "mnuPayment"
        Me.mnuPayment.Size = New System.Drawing.Size(187, 22)
        Me.mnuPayment.Text = "&Payment"
        '
        'mnuReceived
        '
        Me.mnuReceived.Name = "mnuReceived"
        Me.mnuReceived.Size = New System.Drawing.Size(187, 22)
        Me.mnuReceived.Text = "&Received"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(184, 6)
        '
        'mnuViewApprovalForm
        '
        Me.mnuViewApprovalForm.Name = "mnuViewApprovalForm"
        Me.mnuViewApprovalForm.Size = New System.Drawing.Size(187, 22)
        Me.mnuViewApprovalForm.Tag = "mnuViewApprovalForm"
        Me.mnuViewApprovalForm.Text = "&View Approved Form"
        '
        'mnuChangeStatus
        '
        Me.mnuChangeStatus.Name = "mnuChangeStatus"
        Me.mnuChangeStatus.Size = New System.Drawing.Size(187, 22)
        Me.mnuChangeStatus.Tag = "mnuChangeStatus"
        Me.mnuChangeStatus.Text = "Global Change Status"
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(371, 13)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(105, 30)
        Me.btnChangeStatus.TabIndex = 4
        Me.btnChangeStatus.Text = "&Change Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(585, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(482, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(482, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        Me.btnNew.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(688, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmLoanAdvanceList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 443)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanAdvanceList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan / Advance List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTrnHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents lvLoanAdvance As eZee.Common.eZeeListView
    Friend WithEvents colhVocNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanScheme As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoan_Advance As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtVocNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVocNo As System.Windows.Forms.Label
    Friend WithEvents colhIsLoan As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReceived As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhStatusUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents btnPreview As eZee.Common.eZeeLightButton
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuViewApprovalForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhemployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInstallments As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuChangeStatus As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhBalance As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPaidLoan As System.Windows.Forms.ColumnHeader
End Class

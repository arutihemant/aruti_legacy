﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApprover_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApprover_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApproversInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSearchApprover = New System.Windows.Forms.TextBox
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.objChkECheck = New System.Windows.Forms.CheckBox
        Me.objChkACheck = New System.Windows.Forms.CheckBox
        Me.objbtnUnassign = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvApprover = New System.Windows.Forms.ListView
        Me.colhACode = New System.Windows.Forms.ColumnHeader
        Me.objcolhAppTranId = New System.Windows.Forms.ColumnHeader
        Me.lvEmployee = New System.Windows.Forms.ListView
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApproversInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApproversInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(666, 429)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbApproversInfo
        '
        Me.gbApproversInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproversInfo.Checked = False
        Me.gbApproversInfo.CollapseAllExceptThis = False
        Me.gbApproversInfo.CollapsedHoverImage = Nothing
        Me.gbApproversInfo.CollapsedNormalImage = Nothing
        Me.gbApproversInfo.CollapsedPressedImage = Nothing
        Me.gbApproversInfo.CollapseOnLoad = False
        Me.gbApproversInfo.Controls.Add(Me.txtSearchApprover)
        Me.gbApproversInfo.Controls.Add(Me.txtSearchEmp)
        Me.gbApproversInfo.Controls.Add(Me.objChkECheck)
        Me.gbApproversInfo.Controls.Add(Me.objChkACheck)
        Me.gbApproversInfo.Controls.Add(Me.objbtnUnassign)
        Me.gbApproversInfo.Controls.Add(Me.lvApprover)
        Me.gbApproversInfo.Controls.Add(Me.lvEmployee)
        Me.gbApproversInfo.Controls.Add(Me.objbtnAssign)
        Me.gbApproversInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproversInfo.ExpandedHoverImage = Nothing
        Me.gbApproversInfo.ExpandedNormalImage = Nothing
        Me.gbApproversInfo.ExpandedPressedImage = Nothing
        Me.gbApproversInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproversInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproversInfo.HeaderHeight = 25
        Me.gbApproversInfo.HeaderMessage = ""
        Me.gbApproversInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproversInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproversInfo.HeightOnCollapse = 0
        Me.gbApproversInfo.LeftTextSpace = 0
        Me.gbApproversInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbApproversInfo.Name = "gbApproversInfo"
        Me.gbApproversInfo.OpenHeight = 300
        Me.gbApproversInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproversInfo.ShowBorder = True
        Me.gbApproversInfo.ShowCheckBox = False
        Me.gbApproversInfo.ShowCollapseButton = False
        Me.gbApproversInfo.ShowDefaultBorderColor = True
        Me.gbApproversInfo.ShowDownButton = False
        Me.gbApproversInfo.ShowHeader = True
        Me.gbApproversInfo.Size = New System.Drawing.Size(666, 374)
        Me.gbApproversInfo.TabIndex = 0
        Me.gbApproversInfo.Temp = 0
        Me.gbApproversInfo.Text = "Approvers Info"
        Me.gbApproversInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearchApprover
        '
        Me.txtSearchApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchApprover.Location = New System.Drawing.Point(360, 30)
        Me.txtSearchApprover.Name = "txtSearchApprover"
        Me.txtSearchApprover.Size = New System.Drawing.Size(304, 21)
        Me.txtSearchApprover.TabIndex = 110
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(2, 30)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(304, 21)
        Me.txtSearchEmp.TabIndex = 109
        '
        'objChkECheck
        '
        Me.objChkECheck.AutoSize = True
        Me.objChkECheck.Location = New System.Drawing.Point(285, 62)
        Me.objChkECheck.Name = "objChkECheck"
        Me.objChkECheck.Size = New System.Drawing.Size(15, 14)
        Me.objChkECheck.TabIndex = 1
        Me.objChkECheck.UseVisualStyleBackColor = True
        '
        'objChkACheck
        '
        Me.objChkACheck.AutoSize = True
        Me.objChkACheck.Location = New System.Drawing.Point(644, 62)
        Me.objChkACheck.Name = "objChkACheck"
        Me.objChkACheck.Size = New System.Drawing.Size(15, 14)
        Me.objChkACheck.TabIndex = 2
        Me.objChkACheck.UseVisualStyleBackColor = True
        '
        'objbtnUnassign
        '
        Me.objbtnUnassign.BackColor = System.Drawing.Color.White
        Me.objbtnUnassign.BackgroundImage = CType(resources.GetObject("objbtnUnassign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnassign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnassign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnassign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnassign.FlatAppearance.BorderSize = 0
        Me.objbtnUnassign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnassign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnassign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnassign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnUnassign.Location = New System.Drawing.Point(312, 209)
        Me.objbtnUnassign.Name = "objbtnUnassign"
        Me.objbtnUnassign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnassign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnassign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnassign.TabIndex = 107
        Me.objbtnUnassign.UseVisualStyleBackColor = True
        '
        'lvApprover
        '
        Me.lvApprover.CheckBoxes = True
        Me.lvApprover.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhACode, Me.objcolhAppTranId})
        Me.lvApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvApprover.FullRowSelect = True
        Me.lvApprover.HideSelection = False
        Me.lvApprover.Location = New System.Drawing.Point(360, 57)
        Me.lvApprover.Name = "lvApprover"
        Me.lvApprover.Size = New System.Drawing.Size(304, 315)
        Me.lvApprover.TabIndex = 105
        Me.lvApprover.UseCompatibleStateImageBehavior = False
        Me.lvApprover.View = System.Windows.Forms.View.Details
        '
        'colhACode
        '
        Me.colhACode.Tag = "colhACode"
        Me.colhACode.Text = "Approver"
        Me.colhACode.Width = 300
        '
        'objcolhAppTranId
        '
        Me.objcolhAppTranId.Tag = "objcolhAppTranId"
        Me.objcolhAppTranId.Text = ""
        Me.objcolhAppTranId.Width = 0
        '
        'lvEmployee
        '
        Me.lvEmployee.CheckBoxes = True
        Me.lvEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode})
        Me.lvEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvEmployee.FullRowSelect = True
        Me.lvEmployee.HideSelection = False
        Me.lvEmployee.Location = New System.Drawing.Point(2, 57)
        Me.lvEmployee.Name = "lvEmployee"
        Me.lvEmployee.Size = New System.Drawing.Size(304, 315)
        Me.lvEmployee.TabIndex = 104
        Me.lvEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEmployee.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Employee"
        Me.colhCode.Width = 300
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnAssign.Location = New System.Drawing.Point(312, 163)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 106
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 374)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(666, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(454, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(557, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmLoanApprover_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(666, 429)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApprover_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan Approver"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApproversInfo.ResumeLayout(False)
        Me.gbApproversInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbApproversInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUnassign As eZee.Common.eZeeLightButton
    Friend WithEvents lvApprover As System.Windows.Forms.ListView
    Friend WithEvents lvEmployee As System.Windows.Forms.ListView
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhACode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkECheck As System.Windows.Forms.CheckBox
    Friend WithEvents objChkACheck As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhAppTranId As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtSearchApprover As System.Windows.Forms.TextBox
    Friend WithEvents txtSearchEmp As System.Windows.Forms.TextBox
End Class

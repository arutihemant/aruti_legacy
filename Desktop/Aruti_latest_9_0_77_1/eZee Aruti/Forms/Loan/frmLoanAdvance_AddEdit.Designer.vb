﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanAdvance_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim colhAmount As System.Windows.Forms.ColumnHeader
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanAdvance_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabcLoanAdvance = New System.Windows.Forms.TabControl
        Me.tabpLoanAdvance = New System.Windows.Forms.TabPage
        Me.gbLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLoanInfo = New System.Windows.Forms.Panel
        Me.lblDuration = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.gbEMIAmount = New System.Windows.Forms.GroupBox
        Me.txtNoOfEMI = New eZee.TextBox.NumericTextBox
        Me.lblNoOfInstallments = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.EZeeStraightLine3 = New eZee.Common.eZeeStraightLine
        Me.objbtnAddLoanScheme = New eZee.Common.eZeeGradientButton
        Me.gbEMIMonth = New System.Windows.Forms.GroupBox
        Me.txtEMIMonthsAmt = New eZee.TextBox.NumericTextBox
        Me.lblEMIMonthsAmt = New System.Windows.Forms.Label
        Me.txtEMIMonths = New eZee.TextBox.NumericTextBox
        Me.lblNoOfInstallment = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.radReduceBalInterest = New System.Windows.Forms.RadioButton
        Me.radCompoundInterest = New System.Windows.Forms.RadioButton
        Me.radSimpleInterest = New System.Windows.Forms.RadioButton
        Me.lnInterestCalcType = New eZee.Common.eZeeLine
        Me.lblLoanSchedule = New System.Windows.Forms.Label
        Me.cboLoanScheduleBy = New System.Windows.Forms.ComboBox
        Me.lnInstallmentInfo = New eZee.Common.eZeeLine
        Me.txtNetAmount = New eZee.TextBox.NumericTextBox
        Me.txtInterestAmt = New eZee.TextBox.NumericTextBox
        Me.lblInterestAmt = New System.Windows.Forms.Label
        Me.txtLoanInterest = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.lblLoanAmt = New System.Windows.Forms.Label
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.lblNetAmount = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.gbAdvanceAmountInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.txtAdvanceAmt = New eZee.TextBox.NumericTextBox
        Me.lblAdvanceAmt = New System.Windows.Forms.Label
        Me.gbLoanAdvanceInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.txtApprovedBy = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeStraightLine4 = New eZee.Common.eZeeStraightLine
        Me.lblPurpose = New System.Windows.Forms.Label
        Me.txtPurpose = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblApprovedBy = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtVoucherNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.tabpHostory = New System.Windows.Forms.TabPage
        Me.gbLoanAdvanceHistory = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLoanAdvanceHistory = New System.Windows.Forms.Panel
        Me.lvLoanHistory = New eZee.Common.eZeeListView(Me.components)
        Me.colhLoanScheme = New System.Windows.Forms.ColumnHeader
        Me.colhVoucherNo = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhNetAmount = New System.Windows.Forms.ColumnHeader
        Me.colhRepayment = New System.Windows.Forms.ColumnHeader
        Me.colhBalanceAmt = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.lblHistoryBalanceAmt = New System.Windows.Forms.Label
        Me.txtHistoryBalanceAmt = New eZee.TextBox.NumericTextBox
        Me.lblHistoryNetAMount = New System.Windows.Forms.Label
        Me.txtHistoryNetAmt = New eZee.TextBox.NumericTextBox
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lblHistoryRepaymentAmt = New System.Windows.Forms.Label
        Me.txtHistoryRepaymentAmt = New eZee.TextBox.NumericTextBox
        Me.lblHistoryAmount = New System.Windows.Forms.Label
        Me.txtHistoryAmount = New eZee.TextBox.NumericTextBox
        colhAmount = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.tabcLoanAdvance.SuspendLayout()
        Me.tabpLoanAdvance.SuspendLayout()
        Me.gbLoanInfo.SuspendLayout()
        Me.pnlLoanInfo.SuspendLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEMIAmount.SuspendLayout()
        Me.gbEMIMonth.SuspendLayout()
        Me.gbAdvanceAmountInfo.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbLoanAdvanceInfo.SuspendLayout()
        Me.tabpHostory.SuspendLayout()
        Me.gbLoanAdvanceHistory.SuspendLayout()
        Me.pnlLoanAdvanceHistory.SuspendLayout()
        Me.SuspendLayout()
        '
        'colhAmount
        '
        colhAmount.Tag = "colhAmount"
        colhAmount.Text = "Amount"
        colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        colhAmount.Width = 100
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.tabcLoanAdvance)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(818, 494)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 439)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(818, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(606, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(709, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'tabcLoanAdvance
        '
        Me.tabcLoanAdvance.Controls.Add(Me.tabpLoanAdvance)
        Me.tabcLoanAdvance.Controls.Add(Me.tabpHostory)
        Me.tabcLoanAdvance.Location = New System.Drawing.Point(12, 12)
        Me.tabcLoanAdvance.Name = "tabcLoanAdvance"
        Me.tabcLoanAdvance.SelectedIndex = 0
        Me.tabcLoanAdvance.Size = New System.Drawing.Size(795, 421)
        Me.tabcLoanAdvance.TabIndex = 0
        '
        'tabpLoanAdvance
        '
        Me.tabpLoanAdvance.Controls.Add(Me.gbLoanInfo)
        Me.tabpLoanAdvance.Controls.Add(Me.gbAdvanceAmountInfo)
        Me.tabpLoanAdvance.Controls.Add(Me.gbLoanAdvanceInfo)
        Me.tabpLoanAdvance.Location = New System.Drawing.Point(4, 22)
        Me.tabpLoanAdvance.Name = "tabpLoanAdvance"
        Me.tabpLoanAdvance.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpLoanAdvance.Size = New System.Drawing.Size(787, 395)
        Me.tabpLoanAdvance.TabIndex = 0
        Me.tabpLoanAdvance.Text = "Loan / Advance Info"
        Me.tabpLoanAdvance.UseVisualStyleBackColor = True
        '
        'gbLoanInfo
        '
        Me.gbLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanInfo.Checked = True
        Me.gbLoanInfo.CollapseAllExceptThis = False
        Me.gbLoanInfo.CollapsedHoverImage = Nothing
        Me.gbLoanInfo.CollapsedNormalImage = Nothing
        Me.gbLoanInfo.CollapsedPressedImage = Nothing
        Me.gbLoanInfo.CollapseOnLoad = False
        Me.gbLoanInfo.Controls.Add(Me.pnlLoanInfo)
        Me.gbLoanInfo.ExpandedHoverImage = Nothing
        Me.gbLoanInfo.ExpandedNormalImage = Nothing
        Me.gbLoanInfo.ExpandedPressedImage = Nothing
        Me.gbLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanInfo.HeaderHeight = 25
        Me.gbLoanInfo.HeaderMessage = ""
        Me.gbLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanInfo.HeightOnCollapse = 0
        Me.gbLoanInfo.LeftTextSpace = 0
        Me.gbLoanInfo.Location = New System.Drawing.Point(3, 151)
        Me.gbLoanInfo.Name = "gbLoanInfo"
        Me.gbLoanInfo.OpenHeight = 300
        Me.gbLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanInfo.ShowBorder = True
        Me.gbLoanInfo.ShowCheckBox = True
        Me.gbLoanInfo.ShowCollapseButton = False
        Me.gbLoanInfo.ShowDefaultBorderColor = True
        Me.gbLoanInfo.ShowDownButton = False
        Me.gbLoanInfo.ShowHeader = True
        Me.gbLoanInfo.Size = New System.Drawing.Size(781, 174)
        Me.gbLoanInfo.TabIndex = 1
        Me.gbLoanInfo.Temp = 0
        Me.gbLoanInfo.Text = "Loan Information"
        Me.gbLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanInfo
        '
        Me.pnlLoanInfo.AutoScroll = True
        Me.pnlLoanInfo.Controls.Add(Me.lblDuration)
        Me.pnlLoanInfo.Controls.Add(Me.nudDuration)
        Me.pnlLoanInfo.Controls.Add(Me.gbEMIAmount)
        Me.pnlLoanInfo.Controls.Add(Me.EZeeStraightLine3)
        Me.pnlLoanInfo.Controls.Add(Me.objbtnAddLoanScheme)
        Me.pnlLoanInfo.Controls.Add(Me.gbEMIMonth)
        Me.pnlLoanInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.pnlLoanInfo.Controls.Add(Me.radReduceBalInterest)
        Me.pnlLoanInfo.Controls.Add(Me.radCompoundInterest)
        Me.pnlLoanInfo.Controls.Add(Me.radSimpleInterest)
        Me.pnlLoanInfo.Controls.Add(Me.lnInterestCalcType)
        Me.pnlLoanInfo.Controls.Add(Me.lblLoanSchedule)
        Me.pnlLoanInfo.Controls.Add(Me.cboLoanScheduleBy)
        Me.pnlLoanInfo.Controls.Add(Me.lnInstallmentInfo)
        Me.pnlLoanInfo.Controls.Add(Me.txtNetAmount)
        Me.pnlLoanInfo.Controls.Add(Me.txtInterestAmt)
        Me.pnlLoanInfo.Controls.Add(Me.lblInterestAmt)
        Me.pnlLoanInfo.Controls.Add(Me.txtLoanInterest)
        Me.pnlLoanInfo.Controls.Add(Me.lblLoanInterest)
        Me.pnlLoanInfo.Controls.Add(Me.lblLoanAmt)
        Me.pnlLoanInfo.Controls.Add(Me.txtLoanAmt)
        Me.pnlLoanInfo.Controls.Add(Me.lblNetAmount)
        Me.pnlLoanInfo.Controls.Add(Me.cboLoanScheme)
        Me.pnlLoanInfo.Controls.Add(Me.lblLoanScheme)
        Me.pnlLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLoanInfo.Location = New System.Drawing.Point(2, 26)
        Me.pnlLoanInfo.Name = "pnlLoanInfo"
        Me.pnlLoanInfo.Size = New System.Drawing.Size(777, 146)
        Me.pnlLoanInfo.TabIndex = 289
        '
        'lblDuration
        '
        Me.lblDuration.Location = New System.Drawing.Point(204, 38)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(113, 15)
        Me.lblDuration.TabIndex = 312
        Me.lblDuration.Text = "Duration (In Months)"
        '
        'nudDuration
        '
        Me.nudDuration.Location = New System.Drawing.Point(204, 62)
        Me.nudDuration.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(69, 21)
        Me.nudDuration.TabIndex = 311
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'gbEMIAmount
        '
        Me.gbEMIAmount.Controls.Add(Me.txtNoOfEMI)
        Me.gbEMIAmount.Controls.Add(Me.lblNoOfInstallments)
        Me.gbEMIAmount.Controls.Add(Me.txtInstallmentAmt)
        Me.gbEMIAmount.Controls.Add(Me.lblEMIAmount)
        Me.gbEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEMIAmount.Location = New System.Drawing.Point(498, 67)
        Me.gbEMIAmount.Name = "gbEMIAmount"
        Me.gbEMIAmount.Size = New System.Drawing.Size(250, 70)
        Me.gbEMIAmount.TabIndex = 304
        Me.gbEMIAmount.TabStop = False
        Me.gbEMIAmount.Visible = False
        '
        'txtNoOfEMI
        '
        Me.txtNoOfEMI.AllowNegative = True
        Me.txtNoOfEMI.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNoOfEMI.DigitsInGroup = 0
        Me.txtNoOfEMI.Enabled = False
        Me.txtNoOfEMI.Flags = 0
        Me.txtNoOfEMI.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNoOfEMI.Location = New System.Drawing.Point(107, 42)
        Me.txtNoOfEMI.MaxDecimalPlaces = 6
        Me.txtNoOfEMI.MaxWholeDigits = 21
        Me.txtNoOfEMI.Name = "txtNoOfEMI"
        Me.txtNoOfEMI.Prefix = ""
        Me.txtNoOfEMI.RangeMax = 1.7976931348623157E+308
        Me.txtNoOfEMI.RangeMin = -1.7976931348623157E+308
        Me.txtNoOfEMI.Size = New System.Drawing.Size(116, 21)
        Me.txtNoOfEMI.TabIndex = 23
        Me.txtNoOfEMI.Text = "0"
        Me.txtNoOfEMI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNoOfInstallments
        '
        Me.lblNoOfInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInstallments.Location = New System.Drawing.Point(1, 44)
        Me.lblNoOfInstallments.Name = "lblNoOfInstallments"
        Me.lblNoOfInstallments.Size = New System.Drawing.Size(104, 16)
        Me.lblNoOfInstallments.TabIndex = 263
        Me.lblNoOfInstallments.Text = "No. of Installments"
        Me.lblNoOfInstallments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 0
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(107, 15)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(116, 21)
        Me.txtInstallmentAmt.TabIndex = 22
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(1, 17)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(104, 16)
        Me.lblEMIAmount.TabIndex = 261
        Me.lblEMIAmount.Text = "Installment Amt."
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine3
        '
        Me.EZeeStraightLine3.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeStraightLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine3.Location = New System.Drawing.Point(279, 56)
        Me.EZeeStraightLine3.Name = "EZeeStraightLine3"
        Me.EZeeStraightLine3.Size = New System.Drawing.Size(14, 81)
        Me.EZeeStraightLine3.TabIndex = 310
        Me.EZeeStraightLine3.Text = "EZeeStraightLine3"
        '
        'objbtnAddLoanScheme
        '
        Me.objbtnAddLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLoanScheme.BorderSelected = False
        Me.objbtnAddLoanScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLoanScheme.Location = New System.Drawing.Point(323, 8)
        Me.objbtnAddLoanScheme.Name = "objbtnAddLoanScheme"
        Me.objbtnAddLoanScheme.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLoanScheme.TabIndex = 309
        Me.objbtnAddLoanScheme.Visible = False
        '
        'gbEMIMonth
        '
        Me.gbEMIMonth.Controls.Add(Me.txtEMIMonthsAmt)
        Me.gbEMIMonth.Controls.Add(Me.lblEMIMonthsAmt)
        Me.gbEMIMonth.Controls.Add(Me.txtEMIMonths)
        Me.gbEMIMonth.Controls.Add(Me.lblNoOfInstallment)
        Me.gbEMIMonth.Location = New System.Drawing.Point(498, 67)
        Me.gbEMIMonth.Name = "gbEMIMonth"
        Me.gbEMIMonth.Size = New System.Drawing.Size(250, 70)
        Me.gbEMIMonth.TabIndex = 307
        Me.gbEMIMonth.TabStop = False
        Me.gbEMIMonth.Visible = False
        '
        'txtEMIMonthsAmt
        '
        Me.txtEMIMonthsAmt.AllowNegative = True
        Me.txtEMIMonthsAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIMonthsAmt.DigitsInGroup = 0
        Me.txtEMIMonthsAmt.Enabled = False
        Me.txtEMIMonthsAmt.Flags = 0
        Me.txtEMIMonthsAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIMonthsAmt.Location = New System.Drawing.Point(107, 42)
        Me.txtEMIMonthsAmt.MaxDecimalPlaces = 6
        Me.txtEMIMonthsAmt.MaxWholeDigits = 21
        Me.txtEMIMonthsAmt.Name = "txtEMIMonthsAmt"
        Me.txtEMIMonthsAmt.Prefix = ""
        Me.txtEMIMonthsAmt.RangeMax = 1.7976931348623157E+308
        Me.txtEMIMonthsAmt.RangeMin = -1.7976931348623157E+308
        Me.txtEMIMonthsAmt.Size = New System.Drawing.Size(116, 21)
        Me.txtEMIMonthsAmt.TabIndex = 1
        Me.txtEMIMonthsAmt.Text = "0"
        Me.txtEMIMonthsAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIMonthsAmt
        '
        Me.lblEMIMonthsAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIMonthsAmt.Location = New System.Drawing.Point(6, 47)
        Me.lblEMIMonthsAmt.Name = "lblEMIMonthsAmt"
        Me.lblEMIMonthsAmt.Size = New System.Drawing.Size(98, 16)
        Me.lblEMIMonthsAmt.TabIndex = 263
        Me.lblEMIMonthsAmt.Text = "Installment Amt."
        Me.lblEMIMonthsAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEMIMonths
        '
        Me.txtEMIMonths.AllowNegative = True
        Me.txtEMIMonths.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIMonths.DigitsInGroup = 0
        Me.txtEMIMonths.Flags = 0
        Me.txtEMIMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIMonths.Location = New System.Drawing.Point(107, 15)
        Me.txtEMIMonths.MaxDecimalPlaces = 6
        Me.txtEMIMonths.MaxWholeDigits = 21
        Me.txtEMIMonths.Name = "txtEMIMonths"
        Me.txtEMIMonths.Prefix = ""
        Me.txtEMIMonths.RangeMax = 1.7976931348623157E+308
        Me.txtEMIMonths.RangeMin = -1.7976931348623157E+308
        Me.txtEMIMonths.Size = New System.Drawing.Size(116, 21)
        Me.txtEMIMonths.TabIndex = 0
        Me.txtEMIMonths.Text = "0"
        Me.txtEMIMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNoOfInstallment
        '
        Me.lblNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInstallment.Location = New System.Drawing.Point(6, 17)
        Me.lblNoOfInstallment.Name = "lblNoOfInstallment"
        Me.lblNoOfInstallment.Size = New System.Drawing.Size(98, 16)
        Me.lblNoOfInstallment.TabIndex = 261
        Me.lblNoOfInstallment.Text = "No. of Installments"
        Me.lblNoOfInstallment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(463, 8)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(13, 129)
        Me.EZeeStraightLine2.TabIndex = 308
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'radReduceBalInterest
        '
        Me.radReduceBalInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radReduceBalInterest.Location = New System.Drawing.Point(313, 116)
        Me.radReduceBalInterest.Name = "radReduceBalInterest"
        Me.radReduceBalInterest.Size = New System.Drawing.Size(145, 17)
        Me.radReduceBalInterest.TabIndex = 7
        Me.radReduceBalInterest.Text = "Reducing Balance"
        Me.radReduceBalInterest.UseVisualStyleBackColor = True
        '
        'radCompoundInterest
        '
        Me.radCompoundInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCompoundInterest.Location = New System.Drawing.Point(313, 93)
        Me.radCompoundInterest.Name = "radCompoundInterest"
        Me.radCompoundInterest.Size = New System.Drawing.Size(145, 17)
        Me.radCompoundInterest.TabIndex = 6
        Me.radCompoundInterest.Text = "Compound"
        Me.radCompoundInterest.UseVisualStyleBackColor = True
        '
        'radSimpleInterest
        '
        Me.radSimpleInterest.Checked = True
        Me.radSimpleInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSimpleInterest.Location = New System.Drawing.Point(313, 70)
        Me.radSimpleInterest.Name = "radSimpleInterest"
        Me.radSimpleInterest.Size = New System.Drawing.Size(145, 17)
        Me.radSimpleInterest.TabIndex = 5
        Me.radSimpleInterest.TabStop = True
        Me.radSimpleInterest.Text = "Simple"
        Me.radSimpleInterest.UseVisualStyleBackColor = True
        '
        'lnInterestCalcType
        '
        Me.lnInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnInterestCalcType.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnInterestCalcType.Location = New System.Drawing.Point(299, 53)
        Me.lnInterestCalcType.Name = "lnInterestCalcType"
        Me.lnInterestCalcType.Size = New System.Drawing.Size(149, 17)
        Me.lnInterestCalcType.TabIndex = 300
        Me.lnInterestCalcType.Text = "Interest Calc. Type"
        '
        'lblLoanSchedule
        '
        Me.lblLoanSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchedule.Location = New System.Drawing.Point(513, 42)
        Me.lblLoanSchedule.Name = "lblLoanSchedule"
        Me.lblLoanSchedule.Size = New System.Drawing.Size(89, 15)
        Me.lblLoanSchedule.TabIndex = 299
        Me.lblLoanSchedule.Text = "Loan Schedule By"
        Me.lblLoanSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheduleBy
        '
        Me.cboLoanScheduleBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheduleBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheduleBy.FormattingEnabled = True
        Me.cboLoanScheduleBy.Items.AddRange(New Object() {"Weekly", "Fortnightly", "Monthly", "Half Yearly", "Yearly"})
        Me.cboLoanScheduleBy.Location = New System.Drawing.Point(605, 39)
        Me.cboLoanScheduleBy.Name = "cboLoanScheduleBy"
        Me.cboLoanScheduleBy.Size = New System.Drawing.Size(116, 21)
        Me.cboLoanScheduleBy.TabIndex = 8
        '
        'lnInstallmentInfo
        '
        Me.lnInstallmentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnInstallmentInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnInstallmentInfo.Location = New System.Drawing.Point(495, 11)
        Me.lnInstallmentInfo.Name = "lnInstallmentInfo"
        Me.lnInstallmentInfo.Size = New System.Drawing.Size(262, 20)
        Me.lnInstallmentInfo.TabIndex = 297
        Me.lnInstallmentInfo.Text = "Monthly Installment Information"
        '
        'txtNetAmount
        '
        Me.txtNetAmount.AllowNegative = True
        Me.txtNetAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNetAmount.DigitsInGroup = 0
        Me.txtNetAmount.Flags = 0
        Me.txtNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetAmount.Location = New System.Drawing.Point(97, 116)
        Me.txtNetAmount.MaxDecimalPlaces = 6
        Me.txtNetAmount.MaxWholeDigits = 21
        Me.txtNetAmount.Name = "txtNetAmount"
        Me.txtNetAmount.Prefix = ""
        Me.txtNetAmount.RangeMax = 1.7976931348623157E+308
        Me.txtNetAmount.RangeMin = -1.7976931348623157E+308
        Me.txtNetAmount.ReadOnly = True
        Me.txtNetAmount.Size = New System.Drawing.Size(101, 21)
        Me.txtNetAmount.TabIndex = 4
        Me.txtNetAmount.Text = "0"
        Me.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterestAmt
        '
        Me.txtInterestAmt.AllowNegative = True
        Me.txtInterestAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestAmt.DigitsInGroup = 0
        Me.txtInterestAmt.Flags = 0
        Me.txtInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestAmt.Location = New System.Drawing.Point(97, 89)
        Me.txtInterestAmt.MaxDecimalPlaces = 6
        Me.txtInterestAmt.MaxWholeDigits = 21
        Me.txtInterestAmt.Name = "txtInterestAmt"
        Me.txtInterestAmt.Prefix = ""
        Me.txtInterestAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInterestAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInterestAmt.ReadOnly = True
        Me.txtInterestAmt.Size = New System.Drawing.Size(101, 21)
        Me.txtInterestAmt.TabIndex = 3
        Me.txtInterestAmt.Text = "0"
        Me.txtInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestAmt
        '
        Me.lblInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestAmt.Location = New System.Drawing.Point(11, 92)
        Me.lblInterestAmt.Name = "lblInterestAmt"
        Me.lblInterestAmt.Size = New System.Drawing.Size(80, 15)
        Me.lblInterestAmt.TabIndex = 294
        Me.lblInterestAmt.Text = "Interest Amount"
        Me.lblInterestAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanInterest
        '
        Me.txtLoanInterest.AllowNegative = True
        Me.txtLoanInterest.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanInterest.DigitsInGroup = 0
        Me.txtLoanInterest.Flags = 0
        Me.txtLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanInterest.Location = New System.Drawing.Point(97, 62)
        Me.txtLoanInterest.MaxDecimalPlaces = 6
        Me.txtLoanInterest.MaxWholeDigits = 21
        Me.txtLoanInterest.Name = "txtLoanInterest"
        Me.txtLoanInterest.Prefix = ""
        Me.txtLoanInterest.RangeMax = 1.7976931348623157E+308
        Me.txtLoanInterest.RangeMin = -1.7976931348623157E+308
        Me.txtLoanInterest.Size = New System.Drawing.Size(101, 21)
        Me.txtLoanInterest.TabIndex = 2
        Me.txtLoanInterest.Text = "0"
        Me.txtLoanInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(11, 65)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(80, 15)
        Me.lblLoanInterest.TabIndex = 292
        Me.lblLoanInterest.Text = "Interest (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanAmt
        '
        Me.lblLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmt.Location = New System.Drawing.Point(11, 38)
        Me.lblLoanAmt.Name = "lblLoanAmt"
        Me.lblLoanAmt.Size = New System.Drawing.Size(80, 15)
        Me.lblLoanAmt.TabIndex = 290
        Me.lblLoanAmt.Text = "Loan Amount"
        Me.lblLoanAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = True
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 0
        Me.txtLoanAmt.Flags = 0
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(97, 35)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.ReadOnly = True
        Me.txtLoanAmt.Size = New System.Drawing.Size(101, 21)
        Me.txtLoanAmt.TabIndex = 1
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNetAmount
        '
        Me.lblNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetAmount.Location = New System.Drawing.Point(11, 119)
        Me.lblNetAmount.Name = "lblNetAmount"
        Me.lblNetAmount.Size = New System.Drawing.Size(80, 15)
        Me.lblNetAmount.TabIndex = 289
        Me.lblNetAmount.Text = "Net Amount"
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.Enabled = False
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(97, 8)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(220, 21)
        Me.cboLoanScheme.TabIndex = 0
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(11, 11)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(80, 15)
        Me.lblLoanScheme.TabIndex = 287
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'gbAdvanceAmountInfo
        '
        Me.gbAdvanceAmountInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.Checked = False
        Me.gbAdvanceAmountInfo.CollapseAllExceptThis = False
        Me.gbAdvanceAmountInfo.CollapsedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.CollapseOnLoad = False
        Me.gbAdvanceAmountInfo.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbAdvanceAmountInfo.ExpandedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAdvanceAmountInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAdvanceAmountInfo.HeaderHeight = 25
        Me.gbAdvanceAmountInfo.HeaderMessage = ""
        Me.gbAdvanceAmountInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAdvanceAmountInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.HeightOnCollapse = 0
        Me.gbAdvanceAmountInfo.LeftTextSpace = 0
        Me.gbAdvanceAmountInfo.Location = New System.Drawing.Point(3, 331)
        Me.gbAdvanceAmountInfo.Name = "gbAdvanceAmountInfo"
        Me.gbAdvanceAmountInfo.OpenHeight = 300
        Me.gbAdvanceAmountInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAdvanceAmountInfo.ShowBorder = True
        Me.gbAdvanceAmountInfo.ShowCheckBox = True
        Me.gbAdvanceAmountInfo.ShowCollapseButton = False
        Me.gbAdvanceAmountInfo.ShowDefaultBorderColor = True
        Me.gbAdvanceAmountInfo.ShowDownButton = False
        Me.gbAdvanceAmountInfo.ShowHeader = True
        Me.gbAdvanceAmountInfo.Size = New System.Drawing.Size(781, 61)
        Me.gbAdvanceAmountInfo.TabIndex = 2
        Me.gbAdvanceAmountInfo.Temp = 0
        Me.gbAdvanceAmountInfo.Text = "Advance Amount Information"
        Me.gbAdvanceAmountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.txtAdvanceAmt)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblAdvanceAmt)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(777, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'txtAdvanceAmt
        '
        Me.txtAdvanceAmt.AllowNegative = True
        Me.txtAdvanceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAdvanceAmt.DigitsInGroup = 0
        Me.txtAdvanceAmt.Flags = 0
        Me.txtAdvanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdvanceAmt.Location = New System.Drawing.Point(97, 6)
        Me.txtAdvanceAmt.MaxDecimalPlaces = 6
        Me.txtAdvanceAmt.MaxWholeDigits = 21
        Me.txtAdvanceAmt.Name = "txtAdvanceAmt"
        Me.txtAdvanceAmt.Prefix = ""
        Me.txtAdvanceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtAdvanceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtAdvanceAmt.ReadOnly = True
        Me.txtAdvanceAmt.Size = New System.Drawing.Size(140, 21)
        Me.txtAdvanceAmt.TabIndex = 1
        Me.txtAdvanceAmt.Text = "0"
        Me.txtAdvanceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAdvanceAmt
        '
        Me.lblAdvanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvanceAmt.Location = New System.Drawing.Point(8, 8)
        Me.lblAdvanceAmt.Name = "lblAdvanceAmt"
        Me.lblAdvanceAmt.Size = New System.Drawing.Size(83, 16)
        Me.lblAdvanceAmt.TabIndex = 0
        Me.lblAdvanceAmt.Text = "Adv. Amount"
        Me.lblAdvanceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbLoanAdvanceInfo
        '
        Me.gbLoanAdvanceInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceInfo.Checked = False
        Me.gbLoanAdvanceInfo.CollapseAllExceptThis = False
        Me.gbLoanAdvanceInfo.CollapsedHoverImage = Nothing
        Me.gbLoanAdvanceInfo.CollapsedNormalImage = Nothing
        Me.gbLoanAdvanceInfo.CollapsedPressedImage = Nothing
        Me.gbLoanAdvanceInfo.CollapseOnLoad = False
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboDeductionPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblDeductionPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtApprovedBy)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.EZeeStraightLine4)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblPurpose)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtPurpose)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboEmpName)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblApprovedBy)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblEmpName)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.dtpDate)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtVoucherNo)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblEffectiveDate)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbLoanAdvanceInfo.ExpandedHoverImage = Nothing
        Me.gbLoanAdvanceInfo.ExpandedNormalImage = Nothing
        Me.gbLoanAdvanceInfo.ExpandedPressedImage = Nothing
        Me.gbLoanAdvanceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanAdvanceInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanAdvanceInfo.HeaderHeight = 25
        Me.gbLoanAdvanceInfo.HeaderMessage = ""
        Me.gbLoanAdvanceInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanAdvanceInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceInfo.HeightOnCollapse = 0
        Me.gbLoanAdvanceInfo.LeftTextSpace = 0
        Me.gbLoanAdvanceInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbLoanAdvanceInfo.Name = "gbLoanAdvanceInfo"
        Me.gbLoanAdvanceInfo.OpenHeight = 300
        Me.gbLoanAdvanceInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanAdvanceInfo.ShowBorder = True
        Me.gbLoanAdvanceInfo.ShowCheckBox = False
        Me.gbLoanAdvanceInfo.ShowCollapseButton = False
        Me.gbLoanAdvanceInfo.ShowDefaultBorderColor = True
        Me.gbLoanAdvanceInfo.ShowDownButton = False
        Me.gbLoanAdvanceInfo.ShowHeader = True
        Me.gbLoanAdvanceInfo.Size = New System.Drawing.Size(781, 142)
        Me.gbLoanAdvanceInfo.TabIndex = 0
        Me.gbLoanAdvanceInfo.Temp = 0
        Me.gbLoanAdvanceInfo.Text = "Loan / Advance General Info"
        Me.gbLoanAdvanceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(500, 114)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(118, 21)
        Me.cboDeductionPeriod.TabIndex = 315
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(405, 117)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(90, 15)
        Me.lblDeductionPeriod.TabIndex = 316
        Me.lblDeductionPeriod.Text = "Deduction Period"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApprovedBy
        '
        Me.txtApprovedBy.Flags = 0
        Me.txtApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprovedBy.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprovedBy.Location = New System.Drawing.Point(99, 114)
        Me.txtApprovedBy.Name = "txtApprovedBy"
        Me.txtApprovedBy.ReadOnly = True
        Me.txtApprovedBy.Size = New System.Drawing.Size(253, 21)
        Me.txtApprovedBy.TabIndex = 313
        '
        'EZeeStraightLine4
        '
        Me.EZeeStraightLine4.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeStraightLine4.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine4.Location = New System.Drawing.Point(385, 33)
        Me.EZeeStraightLine4.Name = "EZeeStraightLine4"
        Me.EZeeStraightLine4.Size = New System.Drawing.Size(14, 102)
        Me.EZeeStraightLine4.TabIndex = 311
        Me.EZeeStraightLine4.Text = "EZeeStraightLine4"
        '
        'lblPurpose
        '
        Me.lblPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPurpose.Location = New System.Drawing.Point(405, 36)
        Me.lblPurpose.Name = "lblPurpose"
        Me.lblPurpose.Size = New System.Drawing.Size(89, 15)
        Me.lblPurpose.TabIndex = 303
        Me.lblPurpose.Text = "Loan Purpose"
        '
        'txtPurpose
        '
        Me.txtPurpose.Flags = 0
        Me.txtPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPurpose.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPurpose.Location = New System.Drawing.Point(500, 33)
        Me.txtPurpose.Multiline = True
        Me.txtPurpose.Name = "txtPurpose"
        Me.txtPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPurpose.Size = New System.Drawing.Size(259, 75)
        Me.txtPurpose.TabIndex = 6
        '
        'cboEmpName
        '
        Me.cboEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpName.Enabled = False
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(99, 87)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(253, 21)
        Me.cboEmpName.TabIndex = 4
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(99, 60)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(101, 21)
        Me.cboPayPeriod.TabIndex = 3
        '
        'lblApprovedBy
        '
        Me.lblApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedBy.Location = New System.Drawing.Point(8, 117)
        Me.lblApprovedBy.Name = "lblApprovedBy"
        Me.lblApprovedBy.Size = New System.Drawing.Size(85, 15)
        Me.lblApprovedBy.TabIndex = 230
        Me.lblApprovedBy.Text = "Approved By"
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(85, 15)
        Me.lblPeriod.TabIndex = 274
        Me.lblPeriod.Text = "Assign Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 90)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(85, 15)
        Me.lblEmpName.TabIndex = 229
        Me.lblEmpName.Text = "Employee"
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(264, 33)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(88, 21)
        Me.dtpDate.TabIndex = 1
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(358, 87)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 93
        Me.objbtnSearchEmployee.Visible = False
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.Flags = 0
        Me.txtVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucherNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucherNo.Location = New System.Drawing.Point(99, 33)
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.Size = New System.Drawing.Size(101, 21)
        Me.txtVoucherNo.TabIndex = 0
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(206, 36)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(52, 15)
        Me.lblEffectiveDate.TabIndex = 2
        Me.lblEffectiveDate.Text = "Date"
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(8, 36)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(85, 15)
        Me.lblVoucherNo.TabIndex = 1
        Me.lblVoucherNo.Text = "Voucher No."
        '
        'tabpHostory
        '
        Me.tabpHostory.Controls.Add(Me.gbLoanAdvanceHistory)
        Me.tabpHostory.Location = New System.Drawing.Point(4, 22)
        Me.tabpHostory.Name = "tabpHostory"
        Me.tabpHostory.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpHostory.Size = New System.Drawing.Size(787, 395)
        Me.tabpHostory.TabIndex = 1
        Me.tabpHostory.Text = "Employee Loan / Advance History"
        Me.tabpHostory.UseVisualStyleBackColor = True
        '
        'gbLoanAdvanceHistory
        '
        Me.gbLoanAdvanceHistory.BorderColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceHistory.Checked = False
        Me.gbLoanAdvanceHistory.CollapseAllExceptThis = False
        Me.gbLoanAdvanceHistory.CollapsedHoverImage = Nothing
        Me.gbLoanAdvanceHistory.CollapsedNormalImage = Nothing
        Me.gbLoanAdvanceHistory.CollapsedPressedImage = Nothing
        Me.gbLoanAdvanceHistory.CollapseOnLoad = False
        Me.gbLoanAdvanceHistory.Controls.Add(Me.pnlLoanAdvanceHistory)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.lblHistoryBalanceAmt)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.txtHistoryBalanceAmt)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.lblHistoryNetAMount)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.txtHistoryNetAmt)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.EZeeStraightLine1)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.lblHistoryRepaymentAmt)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.txtHistoryRepaymentAmt)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.lblHistoryAmount)
        Me.gbLoanAdvanceHistory.Controls.Add(Me.txtHistoryAmount)
        Me.gbLoanAdvanceHistory.ExpandedHoverImage = Nothing
        Me.gbLoanAdvanceHistory.ExpandedNormalImage = Nothing
        Me.gbLoanAdvanceHistory.ExpandedPressedImage = Nothing
        Me.gbLoanAdvanceHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanAdvanceHistory.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanAdvanceHistory.HeaderHeight = 25
        Me.gbLoanAdvanceHistory.HeaderMessage = ""
        Me.gbLoanAdvanceHistory.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanAdvanceHistory.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceHistory.HeightOnCollapse = 0
        Me.gbLoanAdvanceHistory.LeftTextSpace = 0
        Me.gbLoanAdvanceHistory.Location = New System.Drawing.Point(3, 3)
        Me.gbLoanAdvanceHistory.Name = "gbLoanAdvanceHistory"
        Me.gbLoanAdvanceHistory.OpenHeight = 300
        Me.gbLoanAdvanceHistory.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanAdvanceHistory.ShowBorder = True
        Me.gbLoanAdvanceHistory.ShowCheckBox = False
        Me.gbLoanAdvanceHistory.ShowCollapseButton = False
        Me.gbLoanAdvanceHistory.ShowDefaultBorderColor = True
        Me.gbLoanAdvanceHistory.ShowDownButton = False
        Me.gbLoanAdvanceHistory.ShowHeader = True
        Me.gbLoanAdvanceHistory.Size = New System.Drawing.Size(781, 388)
        Me.gbLoanAdvanceHistory.TabIndex = 268
        Me.gbLoanAdvanceHistory.Temp = 0
        Me.gbLoanAdvanceHistory.Text = "Loan / Advance History"
        Me.gbLoanAdvanceHistory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanAdvanceHistory
        '
        Me.pnlLoanAdvanceHistory.Controls.Add(Me.lvLoanHistory)
        Me.pnlLoanAdvanceHistory.Location = New System.Drawing.Point(3, 26)
        Me.pnlLoanAdvanceHistory.Name = "pnlLoanAdvanceHistory"
        Me.pnlLoanAdvanceHistory.Size = New System.Drawing.Size(775, 290)
        Me.pnlLoanAdvanceHistory.TabIndex = 301
        '
        'lvLoanHistory
        '
        Me.lvLoanHistory.BackColorOnChecked = True
        Me.lvLoanHistory.ColumnHeaders = Nothing
        Me.lvLoanHistory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLoanScheme, Me.colhVoucherNo, Me.colhDate, colhAmount, Me.colhNetAmount, Me.colhRepayment, Me.colhBalanceAmt, Me.colhStatus})
        Me.lvLoanHistory.CompulsoryColumns = ""
        Me.lvLoanHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvLoanHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLoanHistory.FullRowSelect = True
        Me.lvLoanHistory.GridLines = True
        Me.lvLoanHistory.GroupingColumn = Nothing
        Me.lvLoanHistory.HideSelection = False
        Me.lvLoanHistory.Location = New System.Drawing.Point(0, 0)
        Me.lvLoanHistory.MinColumnWidth = 50
        Me.lvLoanHistory.MultiSelect = False
        Me.lvLoanHistory.Name = "lvLoanHistory"
        Me.lvLoanHistory.OptionalColumns = ""
        Me.lvLoanHistory.ShowMoreItem = False
        Me.lvLoanHistory.ShowSaveItem = False
        Me.lvLoanHistory.ShowSelectAll = True
        Me.lvLoanHistory.ShowSizeAllColumnsToFit = True
        Me.lvLoanHistory.Size = New System.Drawing.Size(775, 290)
        Me.lvLoanHistory.Sortable = True
        Me.lvLoanHistory.TabIndex = 25
        Me.lvLoanHistory.UseCompatibleStateImageBehavior = False
        Me.lvLoanHistory.View = System.Windows.Forms.View.Details
        '
        'colhLoanScheme
        '
        Me.colhLoanScheme.Tag = "colhLoanScheme"
        Me.colhLoanScheme.Text = "Loan Scheme"
        Me.colhLoanScheme.Width = 110
        '
        'colhVoucherNo
        '
        Me.colhVoucherNo.Tag = "colhVoucherNo"
        Me.colhVoucherNo.Text = "Voucher #"
        Me.colhVoucherNo.Width = 80
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 80
        '
        'colhNetAmount
        '
        Me.colhNetAmount.Tag = "colhNetAmount"
        Me.colhNetAmount.Text = "Net Amount"
        Me.colhNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhNetAmount.Width = 100
        '
        'colhRepayment
        '
        Me.colhRepayment.Tag = "colhRepayment"
        Me.colhRepayment.Text = "Repayment"
        Me.colhRepayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhRepayment.Width = 100
        '
        'colhBalanceAmt
        '
        Me.colhBalanceAmt.Tag = "colhBalanceAmt"
        Me.colhBalanceAmt.Text = "Balance Amount"
        Me.colhBalanceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhBalanceAmt.Width = 100
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'lblHistoryBalanceAmt
        '
        Me.lblHistoryBalanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHistoryBalanceAmt.Location = New System.Drawing.Point(556, 360)
        Me.lblHistoryBalanceAmt.Name = "lblHistoryBalanceAmt"
        Me.lblHistoryBalanceAmt.Size = New System.Drawing.Size(99, 16)
        Me.lblHistoryBalanceAmt.TabIndex = 299
        Me.lblHistoryBalanceAmt.Text = "Balance Amount"
        Me.lblHistoryBalanceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHistoryBalanceAmt
        '
        Me.txtHistoryBalanceAmt.AllowNegative = True
        Me.txtHistoryBalanceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtHistoryBalanceAmt.DigitsInGroup = 0
        Me.txtHistoryBalanceAmt.Flags = 0
        Me.txtHistoryBalanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHistoryBalanceAmt.Location = New System.Drawing.Point(661, 358)
        Me.txtHistoryBalanceAmt.MaxDecimalPlaces = 6
        Me.txtHistoryBalanceAmt.MaxWholeDigits = 21
        Me.txtHistoryBalanceAmt.Name = "txtHistoryBalanceAmt"
        Me.txtHistoryBalanceAmt.Prefix = ""
        Me.txtHistoryBalanceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtHistoryBalanceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtHistoryBalanceAmt.ReadOnly = True
        Me.txtHistoryBalanceAmt.Size = New System.Drawing.Size(114, 21)
        Me.txtHistoryBalanceAmt.TabIndex = 28
        Me.txtHistoryBalanceAmt.Text = "0"
        Me.txtHistoryBalanceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHistoryNetAMount
        '
        Me.lblHistoryNetAMount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHistoryNetAMount.Location = New System.Drawing.Point(317, 360)
        Me.lblHistoryNetAMount.Name = "lblHistoryNetAMount"
        Me.lblHistoryNetAMount.Size = New System.Drawing.Size(99, 16)
        Me.lblHistoryNetAMount.TabIndex = 297
        Me.lblHistoryNetAMount.Text = "Net Amount"
        Me.lblHistoryNetAMount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHistoryNetAmt
        '
        Me.txtHistoryNetAmt.AllowNegative = True
        Me.txtHistoryNetAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtHistoryNetAmt.DigitsInGroup = 0
        Me.txtHistoryNetAmt.Flags = 0
        Me.txtHistoryNetAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHistoryNetAmt.Location = New System.Drawing.Point(422, 358)
        Me.txtHistoryNetAmt.MaxDecimalPlaces = 6
        Me.txtHistoryNetAmt.MaxWholeDigits = 21
        Me.txtHistoryNetAmt.Name = "txtHistoryNetAmt"
        Me.txtHistoryNetAmt.Prefix = ""
        Me.txtHistoryNetAmt.RangeMax = 1.7976931348623157E+308
        Me.txtHistoryNetAmt.RangeMin = -1.7976931348623157E+308
        Me.txtHistoryNetAmt.ReadOnly = True
        Me.txtHistoryNetAmt.Size = New System.Drawing.Size(114, 21)
        Me.txtHistoryNetAmt.TabIndex = 27
        Me.txtHistoryNetAmt.Text = "0"
        Me.txtHistoryNetAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(542, 331)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(8, 48)
        Me.EZeeStraightLine1.TabIndex = 296
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lblHistoryRepaymentAmt
        '
        Me.lblHistoryRepaymentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHistoryRepaymentAmt.Location = New System.Drawing.Point(556, 333)
        Me.lblHistoryRepaymentAmt.Name = "lblHistoryRepaymentAmt"
        Me.lblHistoryRepaymentAmt.Size = New System.Drawing.Size(99, 16)
        Me.lblHistoryRepaymentAmt.TabIndex = 294
        Me.lblHistoryRepaymentAmt.Text = "Repayment Amt."
        Me.lblHistoryRepaymentAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHistoryRepaymentAmt
        '
        Me.txtHistoryRepaymentAmt.AllowNegative = True
        Me.txtHistoryRepaymentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtHistoryRepaymentAmt.DigitsInGroup = 0
        Me.txtHistoryRepaymentAmt.Flags = 0
        Me.txtHistoryRepaymentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHistoryRepaymentAmt.Location = New System.Drawing.Point(661, 331)
        Me.txtHistoryRepaymentAmt.MaxDecimalPlaces = 6
        Me.txtHistoryRepaymentAmt.MaxWholeDigits = 21
        Me.txtHistoryRepaymentAmt.Name = "txtHistoryRepaymentAmt"
        Me.txtHistoryRepaymentAmt.Prefix = ""
        Me.txtHistoryRepaymentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtHistoryRepaymentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtHistoryRepaymentAmt.ReadOnly = True
        Me.txtHistoryRepaymentAmt.Size = New System.Drawing.Size(114, 21)
        Me.txtHistoryRepaymentAmt.TabIndex = 26
        Me.txtHistoryRepaymentAmt.Text = "0"
        Me.txtHistoryRepaymentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblHistoryAmount
        '
        Me.lblHistoryAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHistoryAmount.Location = New System.Drawing.Point(317, 333)
        Me.lblHistoryAmount.Name = "lblHistoryAmount"
        Me.lblHistoryAmount.Size = New System.Drawing.Size(99, 16)
        Me.lblHistoryAmount.TabIndex = 292
        Me.lblHistoryAmount.Text = "Amount"
        Me.lblHistoryAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHistoryAmount
        '
        Me.txtHistoryAmount.AllowNegative = True
        Me.txtHistoryAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtHistoryAmount.DigitsInGroup = 0
        Me.txtHistoryAmount.Flags = 0
        Me.txtHistoryAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHistoryAmount.Location = New System.Drawing.Point(422, 331)
        Me.txtHistoryAmount.MaxDecimalPlaces = 6
        Me.txtHistoryAmount.MaxWholeDigits = 21
        Me.txtHistoryAmount.Name = "txtHistoryAmount"
        Me.txtHistoryAmount.Prefix = ""
        Me.txtHistoryAmount.RangeMax = 1.7976931348623157E+308
        Me.txtHistoryAmount.RangeMin = -1.7976931348623157E+308
        Me.txtHistoryAmount.ReadOnly = True
        Me.txtHistoryAmount.Size = New System.Drawing.Size(114, 21)
        Me.txtHistoryAmount.TabIndex = 25
        Me.txtHistoryAmount.Text = "0"
        Me.txtHistoryAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmLoanAdvance_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(818, 494)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanAdvance_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan And Advance"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.tabcLoanAdvance.ResumeLayout(False)
        Me.tabpLoanAdvance.ResumeLayout(False)
        Me.gbLoanInfo.ResumeLayout(False)
        Me.pnlLoanInfo.ResumeLayout(False)
        Me.pnlLoanInfo.PerformLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEMIAmount.ResumeLayout(False)
        Me.gbEMIAmount.PerformLayout()
        Me.gbEMIMonth.ResumeLayout(False)
        Me.gbEMIMonth.PerformLayout()
        Me.gbAdvanceAmountInfo.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.pnlAdvanceTaken.PerformLayout()
        Me.gbLoanAdvanceInfo.ResumeLayout(False)
        Me.gbLoanAdvanceInfo.PerformLayout()
        Me.tabpHostory.ResumeLayout(False)
        Me.gbLoanAdvanceHistory.ResumeLayout(False)
        Me.gbLoanAdvanceHistory.PerformLayout()
        Me.pnlLoanAdvanceHistory.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcLoanAdvance As System.Windows.Forms.TabControl
    Friend WithEvents tabpLoanAdvance As System.Windows.Forms.TabPage
    Friend WithEvents gbLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLoanInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents gbEMIMonth As System.Windows.Forms.GroupBox
    Friend WithEvents txtEMIMonthsAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIMonthsAmt As System.Windows.Forms.Label
    Friend WithEvents txtEMIMonths As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNoOfInstallment As System.Windows.Forms.Label
    Friend WithEvents gbEMIAmount As System.Windows.Forms.GroupBox
    Friend WithEvents txtNoOfEMI As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNoOfInstallments As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents radReduceBalInterest As System.Windows.Forms.RadioButton
    Friend WithEvents radSimpleInterest As System.Windows.Forms.RadioButton
    Friend WithEvents lnInterestCalcType As eZee.Common.eZeeLine
    Friend WithEvents lblLoanSchedule As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheduleBy As System.Windows.Forms.ComboBox
    Friend WithEvents lnInstallmentInfo As eZee.Common.eZeeLine
    Friend WithEvents txtNetAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtInterestAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInterestAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanInterest As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNetAmount As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents gbAdvanceAmountInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents txtAdvanceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAdvanceAmt As System.Windows.Forms.Label
    Friend WithEvents gbLoanAdvanceInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPurpose As System.Windows.Forms.Label
    Friend WithEvents txtPurpose As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovedBy As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtVoucherNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents tabpHostory As System.Windows.Forms.TabPage
    Friend WithEvents gbLoanAdvanceHistory As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLoanAdvanceHistory As System.Windows.Forms.Panel
    Friend WithEvents lvLoanHistory As eZee.Common.eZeeListView
    Friend WithEvents colhLoanScheme As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVoucherNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNetAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRepayment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBalanceAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblHistoryBalanceAmt As System.Windows.Forms.Label
    Friend WithEvents txtHistoryBalanceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblHistoryNetAMount As System.Windows.Forms.Label
    Friend WithEvents txtHistoryNetAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblHistoryRepaymentAmt As System.Windows.Forms.Label
    Friend WithEvents txtHistoryRepaymentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblHistoryAmount As System.Windows.Forms.Label
    Friend WithEvents txtHistoryAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnAddLoanScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeStraightLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine4 As eZee.Common.eZeeStraightLine
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents txtApprovedBy As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents radCompoundInterest As System.Windows.Forms.RadioButton
End Class

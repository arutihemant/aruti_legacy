﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportLoan_AdvanceWizard

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportLoan_AdvanceWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing

    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub DoPanelOperation(ByVal pnlHidePanel As Panel, ByVal pnlVisiblePanel As Panel)
        Try
            pnlHidePanel.Visible = False : pnlVisiblePanel.Visible = True
            pnlVisiblePanel.Location = New Point(2, 26)
            pnlVisiblePanel.Size = New Size(605, 356)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoPanelOperation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("vocno", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ladate", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("assignperiod", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("dedperiod", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanscheme", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("la_amount", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("interest", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanduration", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("loanschdule", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("schduleby", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("exentityname", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("balanceamount", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = Nothing

            If radLoan.Checked = True Then
                dtTemp = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            ElseIf radAdvance.Checked = True Then
                dtTemp = mds_ImportData.Tables(0).Select("" & cboAEmployeeCode.Text & " IS NULL")
            End If


            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)

                If blnIsNotThrown = False Then Exit Sub

                drNewRow = mdt_ImportData_Others.NewRow
                If radLoan.Checked = True Then
                    drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                    If cboFirstname.Text.Trim.Length > 0 Then
                        drNewRow.Item("firstname") = dtRow.Item(cboFirstname.Text).ToString.Trim
                    Else
                        drNewRow.Item("firstname") = ""
                    End If

                    If cboLastname.Text.Trim.Length > 0 Then
                        drNewRow.Item("surname") = dtRow.Item(cboLastname.Text).ToString.Trim
                    Else
                        drNewRow.Item("surname") = ""
                    End If

                    If cboLoanVocNo.Text.Trim.Length > 0 Then
                        drNewRow.Item("vocno") = dtRow.Item(cboLoanVocNo.Text).ToString.Trim
                    Else
                        drNewRow.Item("vocno") = ""
                    End If

                    If cboLoanAssignedDate.Text.Trim.Length > 0 Then
                        drNewRow.Item("ladate") = dtRow.Item(cboLoanAssignedDate.Text).ToString.Trim
                    Else
                        drNewRow.Item("ladate") = ""
                    End If

                    If cboLoanAssignedPeriod.Text.Trim.Length > 0 Then
                        drNewRow.Item("assignperiod") = dtRow.Item(cboLoanAssignedPeriod.Text).ToString.Trim
                    Else
                        drNewRow.Item("assignperiod") = ""
                    End If

                    If cboLoanDeductionPeriod.Text.Trim.Length > 0 Then
                        drNewRow.Item("dedperiod") = dtRow.Item(cboLoanDeductionPeriod.Text).ToString.Trim
                    Else
                        drNewRow.Item("dedperiod") = ""
                    End If

                    If cboLoanScheme.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanscheme") = dtRow.Item(cboLoanScheme.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanscheme") = ""
                    End If

                    If cboLoanAmount.Text.Trim.Length > 0 Then
                        drNewRow.Item("la_amount") = dtRow.Item(cboLoanAmount.Text).ToString.Trim
                    Else
                        drNewRow.Item("la_amount") = 0
                    End If

                    If cboInterestRate.Text.Trim.Length > 0 Then
                        drNewRow.Item("interest") = dtRow.Item(cboInterestRate.Text).ToString.Trim
                    Else
                        drNewRow.Item("interest") = 0
                    End If

                    If cboLoanDuration.Text.Trim.Length > 0 Then
                        drNewRow.Item("loanduration") = dtRow.Item(cboLoanDuration.Text).ToString.Trim
                    Else
                        drNewRow.Item("loanduration") = 1
                    End If

                    'If cboLoanSchedule.Text.Trim.Length > 0 Then
                    '    drNewRow.Item("loanschdule") = dtRow.Item(cboLoanSchedule.Text).ToString.Trim
                    'Else
                    '    drNewRow.Item("loanschdule") = ""
                    'End If

                    If cboLoanSchedule.SelectedValue > 0 Then
                        drNewRow.Item("loanschdule") = cboLoanSchedule.SelectedValue.ToString
                    Else
                        drNewRow.Item("loanschdule") = ""
                    End If

                    If cboSchduleBy.Text.Trim.Length > 0 Then
                        drNewRow.Item("schduleby") = dtRow.Item(cboSchduleBy.Text).ToString.Trim
                    Else
                        drNewRow.Item("schduleby") = 0
                    End If

                    If cboExternalEntity.Text.Trim.Length > 0 Then
                        drNewRow.Item("exentityname") = dtRow.Item(cboExternalEntity.Text).ToString.Trim
                    Else
                        drNewRow.Item("exentityname") = ""
                    End If

                    If cboLoanBalance.Text.Trim.Length > 0 Then
                        drNewRow.Item("balanceamount") = dtRow.Item(cboLoanBalance.Text).ToString.Trim
                    Else
                        drNewRow.Item("balanceamount") = 0
                    End If

                ElseIf radAdvance.Checked = True Then

                    drNewRow.Item("ecode") = dtRow.Item(cboAEmployeeCode.Text).ToString.Trim

                    If cboAFirstname.Text.Trim.Length > 0 Then
                        drNewRow.Item("firstname") = dtRow.Item(cboAFirstname.Text).ToString.Trim
                    Else
                        drNewRow.Item("firstname") = ""
                    End If

                    If cboASurname.Text.Trim.Length > 0 Then
                        drNewRow.Item("surname") = dtRow.Item(cboASurname.Text).ToString.Trim
                    Else
                        drNewRow.Item("surname") = ""
                    End If

                    If cboAdvanceVocNo.Text.Trim.Length > 0 Then
                        drNewRow.Item("vocno") = dtRow.Item(cboAdvanceVocNo.Text).ToString.Trim
                    Else
                        drNewRow.Item("vocno") = ""
                    End If

                    If cboAAssignDate.Text.Trim.Length > 0 Then
                        drNewRow.Item("ladate") = dtRow.Item(cboAAssignDate.Text).ToString.Trim
                    Else
                        drNewRow.Item("ladate") = ""
                    End If

                    If cboAAssignPeriod.Text.Trim.Length > 0 Then
                        drNewRow.Item("assignperiod") = dtRow.Item(cboAAssignPeriod.Text).ToString.Trim
                    Else
                        drNewRow.Item("assignperiod") = ""
                    End If

                    If cboADeductPeriod.Text.Trim.Length > 0 Then
                        drNewRow.Item("dedperiod") = dtRow.Item(cboADeductPeriod.Text).ToString.Trim
                    Else
                        drNewRow.Item("dedperiod") = ""
                    End If

                    If cboAdvanceAmt.Text.Trim.Length > 0 Then
                        drNewRow.Item("la_amount") = dtRow.Item(cboAdvanceAmt.Text).ToString.Trim
                    Else
                        drNewRow.Item("la_amount") = 0
                    End If

                    drNewRow.Item("loanduration") = 1

                    If cboAExEntity.Text.Trim.Length > 0 Then
                        drNewRow.Item("exentityname") = dtRow.Item(cboAExEntity.Text).ToString.Trim
                    Else
                        drNewRow.Item("exentityname") = ""
                    End If

                    If cboAdvaceBalance.Text.Trim.Length > 0 Then
                        drNewRow.Item("balanceamount") = dtRow.Item(cboAdvaceBalance.Text).ToString.Trim
                    Else
                        drNewRow.Item("balanceamount") = 0
                    End If
                End If

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                dgData.AutoGenerateColumns = False
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizImportLoan.BackEnabled = False
            eZeeWizImportLoan.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If radLoan.Checked = True Then

                    If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanAssignedDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Date cannot be blank. Please set Loan Assignment Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanAssignedPeriod.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanDuration.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    'If .Item(cboLoanSchedule.Text).ToString.Trim.Length <= 0 Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    '    Return False
                    'End If

                    If cboLoanSchedule.SelectedValue <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboSchduleBy.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Schduled Value cannot be blank. Please set Schduled Value in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboExternalEntity.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    If .Item(cboLoanBalance.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                        Return False
                    End If

                ElseIf radAdvance.Checked = True Then

                    If .Item(cboAEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboEmployeeCode.Focus()
                        Return False
                    End If

                    If .Item(cboAdvanceVocNo.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Voc. No. cannot be blank. Please set Advance Voc. No. in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboLoanVocNo.Focus()
                        Return False
                    End If

                    If .Item(cboAAssignDate.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Advance Assignment Date cannot be blank. Please set Advance Assignment Date in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboAAssignDate.Focus()
                        Return False
                    End If

                    If .Item(cboAAssignPeriod.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Advance Assignment Period cannot be blank. Please set Advance Assignment Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboAAssignPeriod.Focus()
                        Return False
                    End If

                    If .Item(cboADeductPeriod.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Advance Deduction Period cannot be blank. Please set Advance Deduction Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboADeductPeriod.Focus()
                        Return False
                    End If

                    If .Item(cboAdvanceAmt.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboLoanAmount.Focus()
                        Return False
                    End If

                    If .Item(cboAExEntity.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "External Entity cannot be blank. Please set External Entity in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboExternalEntity.Focus()
                        Return False
                    End If

                    If .Item(cboAdvaceBalance.Text).ToString.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Advance Balance cannot be blank. Please set Loan Balance in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                        cboLoanBalance.Focus()
                        Return False
                    End If

                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetDataCombo()
        Try

            If radLoan.Checked = True Then
                For Each ctrl As Control In pnlLoanMapping.Controls
                    If TypeOf ctrl Is ComboBox AndAlso ctrl.Name <> "cboLoanSchedule" Then
                        Call ClearCombo(CType(ctrl, ComboBox))
                    End If
                Next

                For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                    cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                    cboFirstname.Items.Add(dtColumns.ColumnName)
                    cboLastname.Items.Add(dtColumns.ColumnName)
                    cboLoanVocNo.Items.Add(dtColumns.ColumnName)
                    cboLoanAssignedDate.Items.Add(dtColumns.ColumnName)
                    cboLoanAssignedPeriod.Items.Add(dtColumns.ColumnName)
                    cboLoanDeductionPeriod.Items.Add(dtColumns.ColumnName)
                    cboLoanScheme.Items.Add(dtColumns.ColumnName)
                    cboLoanAmount.Items.Add(dtColumns.ColumnName)
                    cboInterestRate.Items.Add(dtColumns.ColumnName)
                    cboLoanDuration.Items.Add(dtColumns.ColumnName)
                    'cboLoanSchedule.Items.Add(dtColumns.ColumnName)
                    cboSchduleBy.Items.Add(dtColumns.ColumnName)
                    cboExternalEntity.Items.Add(dtColumns.ColumnName)
                    cboLoanBalance.Items.Add(dtColumns.ColumnName)
                Next

            ElseIf radAdvance.Checked = True Then
                For Each ctrl As Control In pnlAdvance.Controls
                    If TypeOf ctrl Is ComboBox Then
                        Call ClearCombo(CType(ctrl, ComboBox))
                    End If
                Next

                For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                    cboAEmployeeCode.Items.Add(dtColumns.ColumnName)
                    cboAFirstname.Items.Add(dtColumns.ColumnName)
                    cboASurname.Items.Add(dtColumns.ColumnName)
                    cboAdvanceVocNo.Items.Add(dtColumns.ColumnName)
                    cboAAssignDate.Items.Add(dtColumns.ColumnName)
                    cboAAssignPeriod.Items.Add(dtColumns.ColumnName)
                    cboADeductPeriod.Items.Add(dtColumns.ColumnName)
                    cboAdvanceAmt.Items.Add(dtColumns.ColumnName)
                    cboAExEntity.Items.Add(dtColumns.ColumnName)
                    cboAdvaceBalance.Items.Add(dtColumns.ColumnName)
                Next

            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objApprover As New clsLoan_Approver
        Dim objMasterData As New clsMasterData
        Try
            dsCombo = objApprover.GetList("List", True, True, True)
            With cboApprovedBy
                .ValueMember = "approverunkid"
                .DisplayMember = "approvername"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objMasterData.GetLoanScheduling("LoanScheduling")
            With cboLoanSchedule
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("LoanScheduling")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objApprover = Nothing : objMasterData = Nothing
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If radLoan.Checked = True Then

                If (cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboEmployeeCode.Focus()
                    Return False
                End If

                If (cboLoanVocNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanVocNo.Focus()
                    Return False
                End If

                If (cboLoanAssignedDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Assignment Date cannot be blank. Please set Loan Assignment Date in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanAssignedDate.Focus()
                    Return False
                End If

                If (cboLoanAssignedPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanAssignedPeriod.Focus()
                    Return False
                End If

                If (cboLoanDeductionPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanDeductionPeriod.Focus()
                    Return False
                End If

                If (cboLoanScheme.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If

                If (cboLoanAmount.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanAmount.Focus()
                    Return False
                End If

                If (cboLoanDuration.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanDuration.Focus()
                    Return False
                End If

                'If (cboLoanSchedule.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                If cboLoanSchedule.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanSchedule.Focus()
                    Return False
                End If

                If (cboSchduleBy.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Schduled Value cannot be blank. Please set Schduled Value in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboSchduleBy.Focus()
                    Return False
                End If

                If (cboExternalEntity.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboExternalEntity.Focus()
                    Return False
                End If

                If (cboLoanBalance.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s)."), enMsgBoxStyle.Information)
                    cboLoanBalance.Focus()
                    Return False
                End If

            ElseIf radAdvance.Checked = True Then

                If (cboAEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboEmployeeCode.Focus()
                    Return False
                End If

                If (cboAdvanceVocNo.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Advance Voc. No. cannot be blank. Please set Advance Voc. No. in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboLoanVocNo.Focus()
                    Return False
                End If

                If (cboAAssignDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Advance Assignment Date cannot be blank. Please set Advance Assignment Date in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboAAssignDate.Focus()
                    Return False
                End If

                If (cboAAssignPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Advance Assignment Period cannot be blank. Please set Advance Assignment Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboAAssignPeriod.Focus()
                    Return False
                End If

                If (cboADeductPeriod.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Advance Deduction Period cannot be blank. Please set Advance Deduction Period in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboADeductPeriod.Focus()
                    Return False
                End If

                If (cboAdvanceAmt.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboLoanAmount.Focus()
                    Return False
                End If

                If (cboAExEntity.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "External Entity cannot be blank. Please set External Entity in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboExternalEntity.Focus()
                    Return False
                End If

                If (cboAdvaceBalance.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Advance Balance cannot be blank. Please set Loan Balance in order to import Employee Advance(s)."), enMsgBoxStyle.Information)
                    cboLoanBalance.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objLoanScheme As New clsLoan_Scheme
            Dim objProcessPending As clsProcess_pending_loan = Nothing
            Dim objPeriod As New clscommom_period_Tran
            Dim objLoan_Advance As clsLoan_Advance

            Dim objEmp As New clsEmployee_Master

            Dim intLoanSchemeUnkid As Integer = -1
            Dim intProcessPendingUnkid As Integer = -1
            Dim intPeriodUnkid As Integer = -1
            Dim intDeductionPeriodUnkid As Integer = -1
            Dim intEmployeeId As Integer = -1

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                'S.SANDEEP [ 21 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CDec(dtRow.Item("la_amount")) < CDec(dtRow.Item("balanceamount")) Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Invalid Balance Amount.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                'S.SANDEEP [ 21 MAR 2013 ] -- END

                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmployeeId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmployeeId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 24, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                '------------------------------ CHECKING IF PERIOD PRESENT.
                If dtRow.Item("assignperiod").ToString.Trim.Length > 0 Then
                    intPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("assignperiod").ToString.Trim, enModuleReference.Payroll)
                    If intPeriodUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = dtRow.Item("assignperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 28, ", Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        If IsDate(dtRow.Item("ladate")) Then
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objPeriod._Periodunkid = intPeriodUnkid
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodUnkid
                            'Sohail (21 Aug 2015) -- End
                            If CDate(dtRow.Item("ladate")) >= objPeriod._Start_Date.Date And CDate(dtRow.Item("ladate")) <= objPeriod._End_Date.Date Then
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        Else
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If
                    End If
                End If

                If dtRow.Item("dedperiod").ToString.Trim.Length > 0 Then
                    intDeductionPeriodUnkid = objPeriod.GetPeriodByName(dtRow.Item("dedperiod").ToString.Trim, enModuleReference.Payroll)
                    If intDeductionPeriodUnkid <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = dtRow.Item("dedperiod").ToString.Trim & " " & Language.getMessage(mstrModuleName, 28, ", Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                If radLoan.Checked = True Then
                    '------------------------------ CHECKING IF LOAN SCHEME PRESENT.
                    If dtRow.Item("loanscheme").ToString.Trim.Length > 0 Then
                        intLoanSchemeUnkid = objLoanScheme.GetLoanSchemeUnkid(dtRow.Item("loanscheme").ToString.Trim)
                        If intLoanSchemeUnkid <= 0 Then
                            If objLoanScheme IsNot Nothing Then objLoanScheme = Nothing
                            objLoanScheme = New clsLoan_Scheme

                            objLoanScheme._Code = dtRow.Item("loanscheme").ToString.Trim
                            objLoanScheme._Isactive = True
                            objLoanScheme._Name = dtRow.Item("loanscheme").ToString.Trim
                            objLoanScheme._Minnetsalary = 0

                            If objLoanScheme.Insert Then
                                intLoanSchemeUnkid = objLoanScheme._Loanschemeunkid
                            Else
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = objLoanScheme._Code & "/" & objLoanScheme._Name & " : " & objLoanScheme._Message
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                    End If
                End If

                '------------------------------ INSERTING PROCESS PENDING LOAN.

                If objProcessPending IsNot Nothing Then objProcessPending = Nothing

                objProcessPending = New clsProcess_pending_loan

                If radLoan.Checked = True Then
                    objProcessPending._Isloan = True
                    objProcessPending._Loanschemeunkid = intLoanSchemeUnkid
                ElseIf radAdvance.Checked = True Then
                    objProcessPending._Isloan = False
                    objProcessPending._Loanschemeunkid = -1
                End If

                objProcessPending._Application_No = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Now.ToString("hhmmss") & Now.Millisecond.ToString

                If IsDate(dtRow.Item("ladate")) Then
                    objProcessPending._Application_Date = CDate(dtRow.Item("ladate")).AddDays(-1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                objProcessPending._Loan_Amount = CDec(dtRow.Item("la_amount"))
                objProcessPending._Approved_Amount = CDec(dtRow.Item("la_amount"))
                objProcessPending._Employeeunkid = intEmployeeId
                objProcessPending._Approverunkid = cboApprovedBy.SelectedValue
                objProcessPending._Loan_Statusunkid = 4
                objProcessPending._Isvoid = False
                objProcessPending._Voiddatetime = Nothing
                objProcessPending._Voiduserunkid = -1
                objProcessPending._Isexternal_Entity = True
                objProcessPending._External_Entity_Name = dtRow.Item("exentityname").ToString
                objProcessPending._Userunkid = User._Object._Userunkid

                If objProcessPending.Insert Then
                    intProcessPendingUnkid = objProcessPending._Processpendingloanunkid
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objProcessPending._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                objLoan_Advance = New clsLoan_Advance

                If radLoan.Checked = True Then
                    objLoan_Advance._Isloan = True
                    objLoan_Advance._Advance_Amount = 0
                    objLoan_Advance._Calctype_Id = 1
                    objLoan_Advance._Interest_Rate = CDbl(dtRow.Item("interest"))
                    objLoan_Advance._Loan_Amount = CDec(dtRow.Item("la_amount"))
                    objLoan_Advance._Balance_Amount = CDec(dtRow.Item("balanceamount"))
                    objLoan_Advance._Loan_Duration = dtRow.Item("loanduration")

                    Dim mdecNetAmount As Decimal = 0 : Dim mdecInterest_Amount As Decimal = 0

                    objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, CDec(dtRow.Item("la_amount")), dtRow.Item("loanduration"), CDbl(dtRow.Item("interest")), mdecNetAmount, mdecInterest_Amount)

                    objLoan_Advance._Interest_Amount = mdecInterest_Amount
                    objLoan_Advance._Net_Amount = mdecNetAmount


                    If CInt(cboLoanSchedule.SelectedValue) > 0 Then
                        Select Case CInt(cboLoanSchedule.SelectedValue)
                            Case 1  'AMOUNTS
                                Dim mintTotalEMI As Integer = 0
                                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanSchedule.SelectedValue), mdecNetAmount, CDec(dtRow.Item("schduleby")), mintTotalEMI)
                                objLoan_Advance._Emi_Amount = CDec(dtRow.Item("schduleby"))
                                objLoan_Advance._Emi_Tenure = mintTotalEMI

                                If mintTotalEMI > CInt(dtRow.Item("loanduration")) Then
                                    dtRow.Item("image") = imgError
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "The Number of Installments cannot be greater than the loan duration.")
                                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                                    dtRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If

                            Case 2  'MONTHS
                                Dim mdecInstallmentAmount As Decimal = 0
                                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanSchedule.SelectedValue), mdecNetAmount, mdecInstallmentAmount, CInt(dtRow.Item("schduleby")))
                                objLoan_Advance._Emi_Amount = mdecInstallmentAmount
                                objLoan_Advance._Emi_Tenure = CInt(dtRow.Item("schduleby"))

                                If CInt(dtRow.Item("schduleby")) > dtRow.Item("loanduration") Then
                                    dtRow.Item("image") = imgError
                                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 27, "The Number of Installments cannot be greater than the loan duration.")
                                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                                    dtRow.Item("objStatus") = 2
                                    objError.Text = CStr(Val(objError.Text) + 1)
                                    Continue For
                                End If

                        End Select
                    End If

                    objLoan_Advance._Loanscheduleunkid = cboLoanSchedule.SelectedValue
                    objLoan_Advance._Loanschemeunkid = intLoanSchemeUnkid
                    objLoan_Advance._Net_Amount = CDec(dtRow.Item("la_amount")) + mdecInterest_Amount
                    'S.SANDEEP [ 18 JAN 2014 ] -- START
                    If CDec(dtRow.Item("balanceamount")) <= 0 Then
                        objLoan_Advance._LoanBF_Amount = CDec(dtRow.Item("la_amount")) + mdecInterest_Amount
                    Else
                        objLoan_Advance._LoanBF_Amount = CDec(dtRow.Item("balanceamount"))
                    End If
                    'S.SANDEEP [ 18 JAN 2014 ] -- END

                ElseIf radAdvance.Checked = True Then

                    objLoan_Advance._Isloan = False
                    objLoan_Advance._Advance_Amount = CDec(dtRow.Item("la_amount"))
                    objLoan_Advance._Balance_Amount = CDec(dtRow.Item("la_amount"))
                    objLoan_Advance._Calctype_Id = 0
                    objLoan_Advance._Loanscheduleunkid = 0
                    objLoan_Advance._Loanschemeunkid = 0
                    objLoan_Advance._Net_Amount = 0
                    objLoan_Advance._Loan_Duration = 1

                End If

                objLoan_Advance._Approverunkid = cboApprovedBy.SelectedValue
                objLoan_Advance._Employeeunkid = intEmployeeId
                objLoan_Advance._Isbrought_Forward = False

                If dtRow.Item("vocno").ToString.Trim.Length > 0 Then
                    objLoan_Advance._Loanvoucher_No = dtRow.Item("vocno")
                Else
                    objLoan_Advance._Loanvoucher_No = ""
                End If

                objLoan_Advance._LoanStatus = 1
                objLoan_Advance._Userunkid = User._Object._Userunkid
                objLoan_Advance._Processpendingloanunkid = intProcessPendingUnkid
                objLoan_Advance._Periodunkid = intPeriodUnkid
                objLoan_Advance._Deductionperiodunkid = intDeductionPeriodUnkid


                If IsDate(dtRow.Item("ladate")) Then
                    objLoan_Advance._Effective_Date = CDate(dtRow.Item("ladate"))
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 26, "Invalid Date.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                
                If objLoan_Advance.Insert() Then
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 29, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objLoan_Advance._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 25, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmImportLoan_AdvanceWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
            radLoan.Checked = True
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportLoan_AdvanceWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "All Files(*.*)|*.*|CSV File(*.csv)|*.csv|Excel File(*.xls)|*.xls|XML File(*.xml)|*.xml"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLoanApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLoanApprover.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboApprovedBy.ValueMember
                .DisplayMember = cboApprovedBy.DisplayMember
                .DataSource = cboApprovedBy.DataSource
                .CodeMember = "Code"
            End With

            If frm.DisplayDialog Then
                cboApprovedBy.SelectedValue = frm.SelectedValue
                cboApprovedBy.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApprover_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Private Sub radAdvance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
        Call DoPanelOperation(pnlLoanMapping, pnlAdvance)
    End Sub

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Call DoPanelOperation(pnlAdvance, pnlLoanMapping)
    End Sub

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                If radLoan.Checked = True Then
                    For Each cr As Control In pnlLoanMapping.Controls
                        If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                            If cr.Name = "cboLoanSchedule" Then Continue For

                            If cr.Name <> cmb.Name Then

                                If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    cmb.SelectedIndex = -1
                                    cmb.Select()
                                    Exit Sub
                                End If

                            End If

                        End If
                    Next
                ElseIf radAdvance.Checked = True Then
                    For Each cr As Control In pnlAdvance.Controls
                        If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                            If cr.Name <> cmb.Name Then

                                If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    cmb.SelectedIndex = -1
                                    cmb.Select()
                                    Exit Sub
                                End If

                            End If

                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xls)|*.xls"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Dependant Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportLoan_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportLoan.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportLoan_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportLoan.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        Dim iExcelData As New ExcelData
                        Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        mds_ImportData = iExcelData.Import(txtFilePath.Text)
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                Case eZeeWizImportLoan.Pages.IndexOf(WizPageApprover)
                    If e.NewIndex > e.OldIndex Then
                        If cboApprovedBy.SelectedValue <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver is mandatory information. Please select approver to import the loan/advace data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            cboApprovedBy.Focus()
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If

                    Call SetDataCombo()

                Case eZeeWizImportLoan.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        If IsValid() = False Then
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case eZeeWizImportLoan.Pages.IndexOf(WizPageImporting)
                    Me.Close()

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportLoan_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbImportInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImportInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeWizImportLoan.CancelText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_CancelText" , Me.eZeeWizImportLoan.CancelText)
			Me.eZeeWizImportLoan.NextText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_NextText" , Me.eZeeWizImportLoan.NextText)
			Me.eZeeWizImportLoan.BackText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_BackText" , Me.eZeeWizImportLoan.BackText)
			Me.eZeeWizImportLoan.FinishText = Language._Object.getCaption(Me.eZeeWizImportLoan.Name & "_FinishText" , Me.eZeeWizImportLoan.FinishText)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.gbImportInformation.Text = Language._Object.getCaption(Me.gbImportInformation.Name, Me.gbImportInformation.Text)
			Me.elImportationType.Text = Language._Object.getCaption(Me.elImportationType.Name, Me.elImportationType.Text)
			Me.elApproverInfo.Text = Language._Object.getCaption(Me.elApproverInfo.Name, Me.elApproverInfo.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.lblImportation.Text = Language._Object.getCaption(Me.lblImportation.Name, Me.lblImportation.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
			Me.lblAssignedPeriod.Text = Language._Object.getCaption(Me.lblAssignedPeriod.Name, Me.lblAssignedPeriod.Text)
			Me.lblLoanDate.Text = Language._Object.getCaption(Me.lblLoanDate.Name, Me.lblLoanDate.Text)
			Me.lblVocNo.Text = Language._Object.getCaption(Me.lblVocNo.Name, Me.lblVocNo.Text)
			Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
			Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
			Me.lblLastName.Text = Language._Object.getCaption(Me.lblLastName.Name, Me.lblLastName.Text)
			Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.Name, Me.lblLoanAmount.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblLoanDuration.Text = Language._Object.getCaption(Me.lblLoanDuration.Name, Me.lblLoanDuration.Text)
			Me.lblInterestRate.Text = Language._Object.getCaption(Me.lblInterestRate.Name, Me.lblInterestRate.Text)
			Me.lblLoanSchedule.Text = Language._Object.getCaption(Me.lblLoanSchedule.Name, Me.lblLoanSchedule.Text)
			Me.lblScheduleBy.Text = Language._Object.getCaption(Me.lblScheduleBy.Name, Me.lblScheduleBy.Text)
			Me.lblExternalEntity.Text = Language._Object.getCaption(Me.lblExternalEntity.Name, Me.lblExternalEntity.Text)
			Me.lblLoanBalance.Text = Language._Object.getCaption(Me.lblLoanBalance.Name, Me.lblLoanBalance.Text)
			Me.lblABalance.Text = Language._Object.getCaption(Me.lblABalance.Name, Me.lblABalance.Text)
			Me.lblAExEntity.Text = Language._Object.getCaption(Me.lblAExEntity.Name, Me.lblAExEntity.Text)
			Me.lblAAmount.Text = Language._Object.getCaption(Me.lblAAmount.Name, Me.lblAAmount.Text)
			Me.lblADeductionPeriod.Text = Language._Object.getCaption(Me.lblADeductionPeriod.Name, Me.lblADeductionPeriod.Text)
			Me.lblAAssignedPeriod.Text = Language._Object.getCaption(Me.lblAAssignedPeriod.Name, Me.lblAAssignedPeriod.Text)
			Me.lblAAssignDate.Text = Language._Object.getCaption(Me.lblAAssignDate.Name, Me.lblAAssignDate.Text)
			Me.lblAVocNo.Text = Language._Object.getCaption(Me.lblAVocNo.Name, Me.lblAVocNo.Text)
			Me.lblAFirstname.Text = Language._Object.getCaption(Me.lblAFirstname.Name, Me.lblAFirstname.Text)
			Me.lblAEmpCode.Text = Language._Object.getCaption(Me.lblAEmpCode.Name, Me.lblAEmpCode.Text)
			Me.lblASurname.Text = Language._Object.getCaption(Me.lblASurname.Name, Me.lblASurname.Text)
			Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
			Me.colhlogindate.HeaderText = Language._Object.getCaption(Me.colhlogindate.Name, Me.colhlogindate.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please the select proper file to Import Data from.")
			Language.setMessage(mstrModuleName, 2, "Approver is mandatory information. Please select approver to import the loan/advace data.")
			Language.setMessage(mstrModuleName, 3, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 4, "Loan Voc. No. cannot be blank. Please set Loan Voc. No. in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 5, "Loan Assignment Date cannot be blank. Please set Loan Assignment Date in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 6, "Loan Assignment Period cannot be blank. Please set Loan Assignment Period in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 7, "Loan Deduction Period cannot be blank. Please set Loan Deduction Period in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 8, "Loan Scheme cannot be blank. Please set Loan Scheme in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 9, "Loan Amount cannot be blank. Please set Loan Amount in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 10, "Loan Duration cannot be blank. Please set Loan Duration in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 11, "Loan Schedule cannot be blank. Please set Loan Schedule in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 12, "Schduled Value cannot be blank. Please set Schduled Value in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 13, "External Entity cannot be blank. Please set External Entity in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 14, "Loan Balance cannot be blank. Please set Loan Balance in order to import Employee Loan(s).")
			Language.setMessage(mstrModuleName, 15, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 16, "Advance Voc. No. cannot be blank. Please set Advance Voc. No. in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 17, "Advance Assignment Date cannot be blank. Please set Advance Assignment Date in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 18, "Advance Assignment Period cannot be blank. Please set Advance Assignment Period in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 19, "Advance Deduction Period cannot be blank. Please set Advance Deduction Period in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 20, "Advance Amount cannot be blank. Please set Advance Amount in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 21, "External Entity cannot be blank. Please set External Entity in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 22, "Advance Balance cannot be blank. Please set Loan Balance in order to import Employee Advance(s).")
			Language.setMessage(mstrModuleName, 23, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 24, "Employee Not Found.")
			Language.setMessage(mstrModuleName, 25, "Fail")
			Language.setMessage(mstrModuleName, 26, "Invalid Date.")
			Language.setMessage(mstrModuleName, 27, "The Number of Installments cannot be greater than the loan duration.")
			Language.setMessage(mstrModuleName, 28, ", Not Found.")
			Language.setMessage(mstrModuleName, 29, "Success")
			Language.setMessage(mstrModuleName, 30, "Invalid Balance Amount.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
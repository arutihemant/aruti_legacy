Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib

Public Class frmLetterFields
    Private ReadOnly mstrModuleName As String = "frmLetterFields"
    Private objField As clsLetterFields
    Private mstrStringTag As String = String.Empty
    Private mintModuleId As Integer = -1
    Private mstrColumnName As String = String.Empty

    '#Region " Properties "
    '    Public Property _FieldTags() As String
    '        Get
    '            Return mstrStringTag
    '        End Get
    '        Set(ByVal value As String)
    '            mstrStringTag = value
    '        End Set
    '    End Property

    '    Public Property _Module_Id() As Integer
    '        Get
    '            Return mintModuleId
    '        End Get
    '        Set(ByVal value As Integer)
    '            mintModuleId = value
    '        End Set
    '    End Property

    '    Public Property _FieldNames() As String
    '        Get
    '            Return mstrColumnName
    '        End Get
    '        Set(ByVal value As String)
    '            mstrColumnName = value
    '        End Set
    '    End Property
    '#End Region

#Region " Private Function "
    Public Function displayDialog(ByVal intModuleId As Integer, ByRef strStringTag As String, ByRef strColumnName As String) As Boolean
        Try
            mstrStringTag = strStringTag
            mintModuleId = intModuleId
            mstrColumnName = strColumnName
            Me.ShowDialog()

            strStringTag = mstrStringTag
            strColumnName = mstrColumnName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

    Private Sub FillList()
        Try
            Dim dsList As New DataSet
            Dim i As Integer = 0
            Dim lvItem As ListViewItem = Nothing
            Select Case mintModuleId
                Case enImg_Email_RefId.Employee_Module  'Employee
                    dsList = objField.GetList(enImg_Email_RefId.Employee_Module, "Employee")
                Case enImg_Email_RefId.Applicant_Module 'Applicant
                    dsList = objField.GetList(enImg_Email_RefId.Applicant_Module, "Applicant")
                Case enImg_Email_RefId.Leave_Module 'Leave 
                    dsList = objField.GetList(enImg_Email_RefId.Leave_Module, "Leave")
                Case enImg_Email_RefId.Payroll_Module   'Payroll
                    dsList = objField.GetList(enImg_Email_RefId.Payroll_Module, "Payroll")
                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enImg_Email_RefId.Discipline_Module
                    dsList = objField.GetList(enImg_Email_RefId.Discipline_Module, "Discipline")
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END

                    'S.SANDEEP [18-AUG-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#290}
                Case enImg_Email_RefId.PerformanceAssessment_Employee
                    dsList = objField.GetList(enImg_Email_RefId.PerformanceAssessment_Employee, "EmpPA")
                Case enImg_Email_RefId.PerformanceAssessment_Assessor
                    dsList = objField.GetList(enImg_Email_RefId.PerformanceAssessment_Assessor, "AsrPA")
                Case enImg_Email_RefId.PerformanceAssessment_Reviewer
                    dsList = objField.GetList(enImg_Email_RefId.PerformanceAssessment_Reviewer, "RevPA")
                    'S.SANDEEP [18-AUG-2018] -- END

                    'Pinkal (16-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                Case enImg_Email_RefId.Leave_Planner_Module
                    dsList = objField.GetList(enImg_Email_RefId.Leave_Planner_Module, "LeavePlanner")
                    'Pinkal (16-Oct-2018) -- End


                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                Case enImg_Email_RefId.Grievance
                    dsList = objField.GetList(enImg_Email_RefId.Grievance, "Grievance")
                    'Gajanan [13-July-2019] -- End

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                Case enImg_Email_RefId.Batch_Scheduling
                    dsList = objField.GetList(enImg_Email_RefId.Batch_Scheduling, "BatchScheduling")
                    'Hemant (07 Oct 2019) -- End

                    'Hemant (30 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                Case enImg_Email_RefId.Applicant_Job_Vacancy
                    dsList = objField.GetList(enImg_Email_RefId.Applicant_Job_Vacancy, "ApplicantJobVacancy")
                    'Hemant (30 Oct 2019) -- End

                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                Case enImg_Email_RefId.Loan_Balance
                    dsList = objField.GetList(enImg_Email_RefId.Loan_Balance, "LoanBalance")
                    'Sohail (17 Feb 2021) -- End

            End Select

            'Sandeep [ 02 Oct 2010 ] -- Start
            'For Each dtCol As DataColumn In dsList.Tables(0).Columns
            '    lvItem = New ListViewItem
            '    i += 1
            '    lvItem.Text = dtCol.ColumnName
            '    lvItem.Tag = i
            '    lvFields.Items.Add(lvItem)

            '    lvItem = Nothing
            'Next

            RemoveHandler lvFields.ItemChecked, AddressOf lvFields_ItemChecked
            RemoveHandler chkCheckAll.CheckedChanged, AddressOf chkCheckAll_CheckedChanged

            If dsList.Tables.Count > 0 Then
                For Each dtCol As DataColumn In dsList.Tables(0).Columns
                    lvItem = New ListViewItem
                    i += 1
                    lvItem.Text = dtCol.ColumnName
                    lvItem.Tag = i
                    lvFields.Items.Add(lvItem)

                    lvItem = Nothing
                Next
            End If
            'Sandeep [ 02 Oct 2010 ] -- End 

            If lvFields.Items.Count > 15 Then
                colhLetterFields.Width = 240 - 20
            Else
                colhLetterFields.Width = 240
            End If

            AddHandler chkCheckAll.CheckedChanged, AddressOf chkCheckAll_CheckedChanged
            AddHandler lvFields.ItemChecked, AddressOf lvFields_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmLetterFields_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Call btnOK_Click(Nothing, Nothing)
        End If
        If Asc(e.KeyChar) = 27 Then
            Call btnCancel_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub frmLetterFields_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage
            objField = New clsLetterFields

            Call FillList()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "frmLetterFields_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If lvFields.CheckedItems.Count > 0 Then
            mstrStringTag = String.Empty
            mstrColumnName = String.Empty
            For i As Integer = 0 To lvFields.CheckedItems.Count - 1
                If mstrColumnName.Length <= 0 Then
                    mstrColumnName = lvFields.CheckedItems(i).Text
                Else
                    mstrColumnName &= "|" & lvFields.CheckedItems(i).Text
                End If
                If mstrStringTag.Length <= 0 Then
                    mstrStringTag = CStr(lvFields.CheckedItems(i).Tag)
                Else
                    mstrStringTag &= "|" & CStr(lvFields.CheckedItems(i).Tag)
                End If
            Next
        End If
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLetterFields.SetMessages()
            objfrm._Other_ModuleNames = "clsLetterFields"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan [04 June 2014] -- End
#End Region

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim lviFound As ListViewItem = lvFields.FindItemWithText(txtSearch.Text.Trim, False, 0, True)
        If lviFound IsNot Nothing Then
            lvFields.TopItem = lviFound
            lviFound.Selected = True
        End If
    End Sub

    Private Sub chkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheckAll.CheckedChanged
        Try
            RemoveHandler lvFields.ItemChecked, AddressOf lvFields_ItemChecked
            For Each lvItem As ListViewItem In lvFields.Items
                lvItem.Checked = CBool(chkCheckAll.CheckState)
            Next
            AddHandler lvFields.ItemChecked, AddressOf lvFields_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkCheckAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvFields_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvFields.ItemChecked
        Try
            RemoveHandler chkCheckAll.CheckedChanged, AddressOf chkCheckAll_CheckedChanged
            If lvFields.CheckedItems.Count <= 0 Then
                chkCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvFields.CheckedItems.Count < lvFields.Items.Count Then
                chkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvFields.CheckedItems.Count = lvFields.Items.Count Then
                chkCheckAll.CheckState = CheckState.Checked
            End If
            AddHandler chkCheckAll.CheckedChanged, AddressOf chkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
			Me.colhLetterFields.Text = Language._Object.getCaption(CStr(Me.colhLetterFields.Tag), Me.colhLetterFields.Text)
			Me.chkCheckAll.Text = Language._Object.getCaption(Me.chkCheckAll.Name, Me.chkCheckAll.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSendMail
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSendMail))
        Me.pnlMailCofirmation = New System.Windows.Forms.Panel
        Me.tabcSendMail = New System.Windows.Forms.TabControl
        Me.tabpEmailInfo = New System.Windows.Forms.TabPage
        Me.pnlControlGroup = New System.Windows.Forms.Panel
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.rtbDocument = New System.Windows.Forms.RichTextBox
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCut = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCopy = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPaste = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlColor = New System.Windows.Forms.Panel
        Me.objbtn48 = New System.Windows.Forms.Button
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.btnMoreColor = New System.Windows.Forms.Button
        Me.objbtn47 = New System.Windows.Forms.Button
        Me.objbtn34 = New System.Windows.Forms.Button
        Me.objbtn18 = New System.Windows.Forms.Button
        Me.objbtn46 = New System.Windows.Forms.Button
        Me.objbtn32 = New System.Windows.Forms.Button
        Me.objbtn33 = New System.Windows.Forms.Button
        Me.objbtn45 = New System.Windows.Forms.Button
        Me.objbtn19 = New System.Windows.Forms.Button
        Me.objbtn2 = New System.Windows.Forms.Button
        Me.objbtn44 = New System.Windows.Forms.Button
        Me.objbtn31 = New System.Windows.Forms.Button
        Me.objbtn42 = New System.Windows.Forms.Button
        Me.objbtn20 = New System.Windows.Forms.Button
        Me.objbtn43 = New System.Windows.Forms.Button
        Me.objbtn3 = New System.Windows.Forms.Button
        Me.objbtn15 = New System.Windows.Forms.Button
        Me.objbtn26 = New System.Windows.Forms.Button
        Me.objbtn16 = New System.Windows.Forms.Button
        Me.objbtn35 = New System.Windows.Forms.Button
        Me.objbtn41 = New System.Windows.Forms.Button
        Me.objbtn21 = New System.Windows.Forms.Button
        Me.objbtn8 = New System.Windows.Forms.Button
        Me.objbtn4 = New System.Windows.Forms.Button
        Me.objbtn23 = New System.Windows.Forms.Button
        Me.objbtn27 = New System.Windows.Forms.Button
        Me.objbtn1 = New System.Windows.Forms.Button
        Me.objbtn36 = New System.Windows.Forms.Button
        Me.objbtn7 = New System.Windows.Forms.Button
        Me.objbtn22 = New System.Windows.Forms.Button
        Me.objbtn24 = New System.Windows.Forms.Button
        Me.objbtn5 = New System.Windows.Forms.Button
        Me.objbtn12 = New System.Windows.Forms.Button
        Me.objbtn28 = New System.Windows.Forms.Button
        Me.objbtn17 = New System.Windows.Forms.Button
        Me.objbtn37 = New System.Windows.Forms.Button
        Me.objbtn40 = New System.Windows.Forms.Button
        Me.objbtn9 = New System.Windows.Forms.Button
        Me.objbtn25 = New System.Windows.Forms.Button
        Me.objbtn6 = New System.Windows.Forms.Button
        Me.objbtn13 = New System.Windows.Forms.Button
        Me.objbtn29 = New System.Windows.Forms.Button
        Me.objbtn11 = New System.Windows.Forms.Button
        Me.objbtn38 = New System.Windows.Forms.Button
        Me.objbtn39 = New System.Windows.Forms.Button
        Me.objbtn10 = New System.Windows.Forms.Button
        Me.objbtn30 = New System.Windows.Forms.Button
        Me.objbtn14 = New System.Windows.Forms.Button
        Me.tlsOperations = New System.Windows.Forms.ToolStrip
        Me.objtlbbtnOpenTemplate = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCut = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCopy = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnPaste = New System.Windows.Forms.ToolStripButton
        Me.objtoolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.objcboFont = New System.Windows.Forms.ToolStripComboBox
        Me.objcboFontSize = New System.Windows.Forms.ToolStripComboBox
        Me.objToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnColor = New System.Windows.Forms.ToolStripDropDownButton
        Me.objToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBold = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnItalic = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnUnderline = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnCenterAlign = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightAlign = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnUndo = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRedo = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnBulletSimple = New System.Windows.Forms.ToolStripButton
        Me.objToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.objtlbbtnLeftIndent = New System.Windows.Forms.ToolStripButton
        Me.objtlbbtnRightIndent = New System.Windows.Forms.ToolStripButton
        Me.btnTo = New eZee.Common.eZeeSplitButton
        Me.cmnuMailTo = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuApplicant = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmployee = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSendPayslip = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSendSickSheet = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAppraisalLetters = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDiscipline = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSendLeaveForm = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEmailReports = New System.Windows.Forms.ToolStripMenuItem
        Me.txtToAddress = New System.Windows.Forms.TextBox
        Me.txtSubject = New eZee.TextBox.AlphanumericTextBox
        Me.lblSubject = New System.Windows.Forms.Label
        Me.tabpAttachment = New System.Windows.Forms.TabPage
        Me.pnlButtonGroup = New System.Windows.Forms.Panel
        Me.btnEmployeeDocument = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRemove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAttach = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvAttachedFile = New System.Windows.Forms.ListView
        Me.objcolhFiles = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsEmpDoc = New System.Windows.Forms.ColumnHeader
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.objbtnPrevious = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSend = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnViewMerge = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFinish = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.ofdMailAttachment = New System.Windows.Forms.OpenFileDialog
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog
        Me.bgwSendMail = New System.ComponentModel.BackgroundWorker
        Me.mnuLoanBalance = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMailCofirmation.SuspendLayout()
        Me.tabcSendMail.SuspendLayout()
        Me.tabpEmailInfo.SuspendLayout()
        Me.pnlControlGroup.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.pnlColor.SuspendLayout()
        Me.tlsOperations.SuspendLayout()
        Me.cmnuMailTo.SuspendLayout()
        Me.tabpAttachment.SuspendLayout()
        Me.pnlButtonGroup.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMailCofirmation
        '
        Me.pnlMailCofirmation.BackColor = System.Drawing.Color.Transparent
        Me.pnlMailCofirmation.Controls.Add(Me.tabcSendMail)
        Me.pnlMailCofirmation.Controls.Add(Me.objefFormFooter)
        Me.pnlMailCofirmation.Controls.Add(Me.eZeeHeader)
        Me.pnlMailCofirmation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMailCofirmation.Location = New System.Drawing.Point(0, 0)
        Me.pnlMailCofirmation.Name = "pnlMailCofirmation"
        Me.pnlMailCofirmation.Size = New System.Drawing.Size(748, 479)
        Me.pnlMailCofirmation.TabIndex = 0
        '
        'tabcSendMail
        '
        Me.tabcSendMail.Controls.Add(Me.tabpEmailInfo)
        Me.tabcSendMail.Controls.Add(Me.tabpAttachment)
        Me.tabcSendMail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcSendMail.Location = New System.Drawing.Point(0, 58)
        Me.tabcSendMail.Name = "tabcSendMail"
        Me.tabcSendMail.SelectedIndex = 0
        Me.tabcSendMail.Size = New System.Drawing.Size(748, 366)
        Me.tabcSendMail.TabIndex = 4
        '
        'tabpEmailInfo
        '
        Me.tabpEmailInfo.Controls.Add(Me.pnlControlGroup)
        Me.tabpEmailInfo.Controls.Add(Me.pnlColor)
        Me.tabpEmailInfo.Controls.Add(Me.tlsOperations)
        Me.tabpEmailInfo.Controls.Add(Me.btnTo)
        Me.tabpEmailInfo.Controls.Add(Me.txtToAddress)
        Me.tabpEmailInfo.Controls.Add(Me.txtSubject)
        Me.tabpEmailInfo.Controls.Add(Me.lblSubject)
        Me.tabpEmailInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpEmailInfo.Name = "tabpEmailInfo"
        Me.tabpEmailInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpEmailInfo.Size = New System.Drawing.Size(740, 340)
        Me.tabpEmailInfo.TabIndex = 0
        Me.tabpEmailInfo.Text = "Email Information"
        Me.tabpEmailInfo.UseVisualStyleBackColor = True
        '
        'pnlControlGroup
        '
        Me.pnlControlGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlControlGroup.Controls.Add(Me.objlblCaption)
        Me.pnlControlGroup.Controls.Add(Me.rtbDocument)
        Me.pnlControlGroup.Location = New System.Drawing.Point(91, 116)
        Me.pnlControlGroup.Name = "pnlControlGroup"
        Me.pnlControlGroup.Size = New System.Drawing.Size(637, 218)
        Me.pnlControlGroup.TabIndex = 149
        '
        'objlblCaption
        '
        Me.objlblCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Gray
        Me.objlblCaption.Location = New System.Drawing.Point(0, 0)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(635, 216)
        Me.objlblCaption.TabIndex = 2
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'rtbDocument
        '
        Me.rtbDocument.AcceptsTab = True
        Me.rtbDocument.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbDocument.ContextMenuStrip = Me.mnuOperations
        Me.rtbDocument.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbDocument.HideSelection = False
        Me.rtbDocument.Location = New System.Drawing.Point(0, 0)
        Me.rtbDocument.Name = "rtbDocument"
        Me.rtbDocument.Size = New System.Drawing.Size(635, 216)
        Me.rtbDocument.TabIndex = 1
        Me.rtbDocument.Text = ""
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(103, 70)
        '
        'mnuCut
        '
        Me.mnuCut.Name = "mnuCut"
        Me.mnuCut.Size = New System.Drawing.Size(102, 22)
        Me.mnuCut.Tag = "mnuCut"
        Me.mnuCut.Text = "Cut"
        Me.mnuCut.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        '
        'mnuCopy
        '
        Me.mnuCopy.Name = "mnuCopy"
        Me.mnuCopy.Size = New System.Drawing.Size(102, 22)
        Me.mnuCopy.Tag = "mnuCopy"
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Name = "mnuPaste"
        Me.mnuPaste.Size = New System.Drawing.Size(102, 22)
        Me.mnuPaste.Tag = "mnuPaste"
        Me.mnuPaste.Text = "Paste"
        '
        'pnlColor
        '
        Me.pnlColor.BackColor = System.Drawing.Color.White
        Me.pnlColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlColor.Controls.Add(Me.objbtn48)
        Me.pnlColor.Controls.Add(Me.EZeeStraightLine1)
        Me.pnlColor.Controls.Add(Me.btnMoreColor)
        Me.pnlColor.Controls.Add(Me.objbtn47)
        Me.pnlColor.Controls.Add(Me.objbtn34)
        Me.pnlColor.Controls.Add(Me.objbtn18)
        Me.pnlColor.Controls.Add(Me.objbtn46)
        Me.pnlColor.Controls.Add(Me.objbtn32)
        Me.pnlColor.Controls.Add(Me.objbtn33)
        Me.pnlColor.Controls.Add(Me.objbtn45)
        Me.pnlColor.Controls.Add(Me.objbtn19)
        Me.pnlColor.Controls.Add(Me.objbtn2)
        Me.pnlColor.Controls.Add(Me.objbtn44)
        Me.pnlColor.Controls.Add(Me.objbtn31)
        Me.pnlColor.Controls.Add(Me.objbtn42)
        Me.pnlColor.Controls.Add(Me.objbtn20)
        Me.pnlColor.Controls.Add(Me.objbtn43)
        Me.pnlColor.Controls.Add(Me.objbtn3)
        Me.pnlColor.Controls.Add(Me.objbtn15)
        Me.pnlColor.Controls.Add(Me.objbtn26)
        Me.pnlColor.Controls.Add(Me.objbtn16)
        Me.pnlColor.Controls.Add(Me.objbtn35)
        Me.pnlColor.Controls.Add(Me.objbtn41)
        Me.pnlColor.Controls.Add(Me.objbtn21)
        Me.pnlColor.Controls.Add(Me.objbtn8)
        Me.pnlColor.Controls.Add(Me.objbtn4)
        Me.pnlColor.Controls.Add(Me.objbtn23)
        Me.pnlColor.Controls.Add(Me.objbtn27)
        Me.pnlColor.Controls.Add(Me.objbtn1)
        Me.pnlColor.Controls.Add(Me.objbtn36)
        Me.pnlColor.Controls.Add(Me.objbtn7)
        Me.pnlColor.Controls.Add(Me.objbtn22)
        Me.pnlColor.Controls.Add(Me.objbtn24)
        Me.pnlColor.Controls.Add(Me.objbtn5)
        Me.pnlColor.Controls.Add(Me.objbtn12)
        Me.pnlColor.Controls.Add(Me.objbtn28)
        Me.pnlColor.Controls.Add(Me.objbtn17)
        Me.pnlColor.Controls.Add(Me.objbtn37)
        Me.pnlColor.Controls.Add(Me.objbtn40)
        Me.pnlColor.Controls.Add(Me.objbtn9)
        Me.pnlColor.Controls.Add(Me.objbtn25)
        Me.pnlColor.Controls.Add(Me.objbtn6)
        Me.pnlColor.Controls.Add(Me.objbtn13)
        Me.pnlColor.Controls.Add(Me.objbtn29)
        Me.pnlColor.Controls.Add(Me.objbtn11)
        Me.pnlColor.Controls.Add(Me.objbtn38)
        Me.pnlColor.Controls.Add(Me.objbtn39)
        Me.pnlColor.Controls.Add(Me.objbtn10)
        Me.pnlColor.Controls.Add(Me.objbtn30)
        Me.pnlColor.Controls.Add(Me.objbtn14)
        Me.pnlColor.Location = New System.Drawing.Point(362, 116)
        Me.pnlColor.Name = "pnlColor"
        Me.pnlColor.Size = New System.Drawing.Size(214, 197)
        Me.pnlColor.TabIndex = 152
        '
        'objbtn48
        '
        Me.objbtn48.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn48.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn48.Location = New System.Drawing.Point(187, 135)
        Me.objbtn48.Name = "objbtn48"
        Me.objbtn48.Size = New System.Drawing.Size(20, 20)
        Me.objbtn48.TabIndex = 76
        Me.objbtn48.UseVisualStyleBackColor = False
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(5, 161)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(202, 3)
        Me.EZeeStraightLine1.TabIndex = 3
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'btnMoreColor
        '
        Me.btnMoreColor.BackColor = System.Drawing.Color.White
        Me.btnMoreColor.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnMoreColor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnMoreColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnMoreColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMoreColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMoreColor.Location = New System.Drawing.Point(5, 164)
        Me.btnMoreColor.Name = "btnMoreColor"
        Me.btnMoreColor.Size = New System.Drawing.Size(202, 27)
        Me.btnMoreColor.TabIndex = 68
        Me.btnMoreColor.Text = "More Color"
        Me.btnMoreColor.UseVisualStyleBackColor = False
        '
        'objbtn47
        '
        Me.objbtn47.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn47.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn47.Location = New System.Drawing.Point(161, 135)
        Me.objbtn47.Name = "objbtn47"
        Me.objbtn47.Size = New System.Drawing.Size(20, 20)
        Me.objbtn47.TabIndex = 75
        Me.objbtn47.UseVisualStyleBackColor = False
        '
        'objbtn34
        '
        Me.objbtn34.BackColor = System.Drawing.Color.Maroon
        Me.objbtn34.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn34.Location = New System.Drawing.Point(31, 109)
        Me.objbtn34.Name = "objbtn34"
        Me.objbtn34.Size = New System.Drawing.Size(20, 20)
        Me.objbtn34.TabIndex = 61
        Me.objbtn34.UseVisualStyleBackColor = False
        '
        'objbtn18
        '
        Me.objbtn18.BackColor = System.Drawing.Color.Red
        Me.objbtn18.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn18.Location = New System.Drawing.Point(31, 57)
        Me.objbtn18.Name = "objbtn18"
        Me.objbtn18.Size = New System.Drawing.Size(20, 20)
        Me.objbtn18.TabIndex = 37
        Me.objbtn18.UseVisualStyleBackColor = False
        '
        'objbtn46
        '
        Me.objbtn46.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn46.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn46.Location = New System.Drawing.Point(135, 135)
        Me.objbtn46.Name = "objbtn46"
        Me.objbtn46.Size = New System.Drawing.Size(20, 20)
        Me.objbtn46.TabIndex = 74
        Me.objbtn46.UseVisualStyleBackColor = False
        '
        'objbtn32
        '
        Me.objbtn32.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn32.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn32.Location = New System.Drawing.Point(187, 83)
        Me.objbtn32.Name = "objbtn32"
        Me.objbtn32.Size = New System.Drawing.Size(20, 20)
        Me.objbtn32.TabIndex = 59
        Me.objbtn32.UseVisualStyleBackColor = False
        '
        'objbtn33
        '
        Me.objbtn33.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn33.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn33.Location = New System.Drawing.Point(5, 109)
        Me.objbtn33.Name = "objbtn33"
        Me.objbtn33.Size = New System.Drawing.Size(20, 20)
        Me.objbtn33.TabIndex = 60
        Me.objbtn33.UseVisualStyleBackColor = False
        '
        'objbtn45
        '
        Me.objbtn45.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn45.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn45.Location = New System.Drawing.Point(109, 135)
        Me.objbtn45.Name = "objbtn45"
        Me.objbtn45.Size = New System.Drawing.Size(20, 20)
        Me.objbtn45.TabIndex = 73
        Me.objbtn45.UseVisualStyleBackColor = False
        '
        'objbtn19
        '
        Me.objbtn19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn19.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn19.Location = New System.Drawing.Point(57, 57)
        Me.objbtn19.Name = "objbtn19"
        Me.objbtn19.Size = New System.Drawing.Size(20, 20)
        Me.objbtn19.TabIndex = 38
        Me.objbtn19.UseVisualStyleBackColor = False
        '
        'objbtn2
        '
        Me.objbtn2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn2.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn2.Location = New System.Drawing.Point(31, 5)
        Me.objbtn2.Name = "objbtn2"
        Me.objbtn2.Size = New System.Drawing.Size(20, 20)
        Me.objbtn2.TabIndex = 36
        Me.objbtn2.UseVisualStyleBackColor = False
        '
        'objbtn44
        '
        Me.objbtn44.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn44.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn44.Location = New System.Drawing.Point(83, 135)
        Me.objbtn44.Name = "objbtn44"
        Me.objbtn44.Size = New System.Drawing.Size(20, 20)
        Me.objbtn44.TabIndex = 72
        Me.objbtn44.UseVisualStyleBackColor = False
        '
        'objbtn31
        '
        Me.objbtn31.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn31.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn31.Location = New System.Drawing.Point(161, 83)
        Me.objbtn31.Name = "objbtn31"
        Me.objbtn31.Size = New System.Drawing.Size(20, 20)
        Me.objbtn31.TabIndex = 58
        Me.objbtn31.UseVisualStyleBackColor = False
        '
        'objbtn42
        '
        Me.objbtn42.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn42.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn42.Location = New System.Drawing.Point(31, 135)
        Me.objbtn42.Name = "objbtn42"
        Me.objbtn42.Size = New System.Drawing.Size(20, 20)
        Me.objbtn42.TabIndex = 70
        Me.objbtn42.UseVisualStyleBackColor = False
        '
        'objbtn20
        '
        Me.objbtn20.BackColor = System.Drawing.Color.Yellow
        Me.objbtn20.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn20.Location = New System.Drawing.Point(83, 57)
        Me.objbtn20.Name = "objbtn20"
        Me.objbtn20.Size = New System.Drawing.Size(20, 20)
        Me.objbtn20.TabIndex = 39
        Me.objbtn20.UseVisualStyleBackColor = False
        '
        'objbtn43
        '
        Me.objbtn43.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.objbtn43.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn43.Location = New System.Drawing.Point(57, 135)
        Me.objbtn43.Name = "objbtn43"
        Me.objbtn43.Size = New System.Drawing.Size(20, 20)
        Me.objbtn43.TabIndex = 71
        Me.objbtn43.UseVisualStyleBackColor = False
        '
        'objbtn3
        '
        Me.objbtn3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn3.Location = New System.Drawing.Point(57, 5)
        Me.objbtn3.Name = "objbtn3"
        Me.objbtn3.Size = New System.Drawing.Size(20, 20)
        Me.objbtn3.TabIndex = 35
        Me.objbtn3.UseVisualStyleBackColor = False
        '
        'objbtn15
        '
        Me.objbtn15.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn15.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn15.Location = New System.Drawing.Point(161, 31)
        Me.objbtn15.Name = "objbtn15"
        Me.objbtn15.Size = New System.Drawing.Size(20, 20)
        Me.objbtn15.TabIndex = 48
        Me.objbtn15.UseVisualStyleBackColor = False
        '
        'objbtn26
        '
        Me.objbtn26.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn26.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn26.Location = New System.Drawing.Point(31, 83)
        Me.objbtn26.Name = "objbtn26"
        Me.objbtn26.Size = New System.Drawing.Size(20, 20)
        Me.objbtn26.TabIndex = 57
        Me.objbtn26.UseVisualStyleBackColor = False
        '
        'objbtn16
        '
        Me.objbtn16.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn16.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn16.Location = New System.Drawing.Point(187, 31)
        Me.objbtn16.Name = "objbtn16"
        Me.objbtn16.Size = New System.Drawing.Size(20, 20)
        Me.objbtn16.TabIndex = 49
        Me.objbtn16.UseVisualStyleBackColor = False
        '
        'objbtn35
        '
        Me.objbtn35.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn35.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn35.Location = New System.Drawing.Point(57, 109)
        Me.objbtn35.Name = "objbtn35"
        Me.objbtn35.Size = New System.Drawing.Size(20, 20)
        Me.objbtn35.TabIndex = 62
        Me.objbtn35.UseVisualStyleBackColor = False
        '
        'objbtn41
        '
        Me.objbtn41.BackColor = System.Drawing.Color.Black
        Me.objbtn41.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn41.Location = New System.Drawing.Point(5, 135)
        Me.objbtn41.Name = "objbtn41"
        Me.objbtn41.Size = New System.Drawing.Size(20, 20)
        Me.objbtn41.TabIndex = 69
        Me.objbtn41.UseVisualStyleBackColor = False
        '
        'objbtn21
        '
        Me.objbtn21.BackColor = System.Drawing.Color.Lime
        Me.objbtn21.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn21.Location = New System.Drawing.Point(109, 57)
        Me.objbtn21.Name = "objbtn21"
        Me.objbtn21.Size = New System.Drawing.Size(20, 20)
        Me.objbtn21.TabIndex = 40
        Me.objbtn21.UseVisualStyleBackColor = False
        '
        'objbtn8
        '
        Me.objbtn8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn8.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn8.Location = New System.Drawing.Point(187, 5)
        Me.objbtn8.Name = "objbtn8"
        Me.objbtn8.Size = New System.Drawing.Size(20, 20)
        Me.objbtn8.TabIndex = 47
        Me.objbtn8.UseVisualStyleBackColor = False
        '
        'objbtn4
        '
        Me.objbtn4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn4.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn4.Location = New System.Drawing.Point(83, 5)
        Me.objbtn4.Name = "objbtn4"
        Me.objbtn4.Size = New System.Drawing.Size(20, 20)
        Me.objbtn4.TabIndex = 34
        Me.objbtn4.UseVisualStyleBackColor = False
        '
        'objbtn23
        '
        Me.objbtn23.BackColor = System.Drawing.Color.Blue
        Me.objbtn23.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn23.Location = New System.Drawing.Point(161, 57)
        Me.objbtn23.Name = "objbtn23"
        Me.objbtn23.Size = New System.Drawing.Size(20, 20)
        Me.objbtn23.TabIndex = 50
        Me.objbtn23.UseVisualStyleBackColor = False
        '
        'objbtn27
        '
        Me.objbtn27.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn27.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn27.Location = New System.Drawing.Point(57, 83)
        Me.objbtn27.Name = "objbtn27"
        Me.objbtn27.Size = New System.Drawing.Size(20, 20)
        Me.objbtn27.TabIndex = 56
        Me.objbtn27.UseVisualStyleBackColor = False
        '
        'objbtn1
        '
        Me.objbtn1.BackColor = System.Drawing.Color.White
        Me.objbtn1.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn1.Location = New System.Drawing.Point(5, 5)
        Me.objbtn1.Name = "objbtn1"
        Me.objbtn1.Size = New System.Drawing.Size(20, 20)
        Me.objbtn1.TabIndex = 28
        Me.objbtn1.UseVisualStyleBackColor = False
        '
        'objbtn36
        '
        Me.objbtn36.BackColor = System.Drawing.Color.Olive
        Me.objbtn36.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn36.Location = New System.Drawing.Point(83, 109)
        Me.objbtn36.Name = "objbtn36"
        Me.objbtn36.Size = New System.Drawing.Size(20, 20)
        Me.objbtn36.TabIndex = 63
        Me.objbtn36.UseVisualStyleBackColor = False
        '
        'objbtn7
        '
        Me.objbtn7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn7.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn7.Location = New System.Drawing.Point(161, 5)
        Me.objbtn7.Name = "objbtn7"
        Me.objbtn7.Size = New System.Drawing.Size(20, 20)
        Me.objbtn7.TabIndex = 46
        Me.objbtn7.UseVisualStyleBackColor = False
        '
        'objbtn22
        '
        Me.objbtn22.BackColor = System.Drawing.Color.Cyan
        Me.objbtn22.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn22.Location = New System.Drawing.Point(135, 57)
        Me.objbtn22.Name = "objbtn22"
        Me.objbtn22.Size = New System.Drawing.Size(20, 20)
        Me.objbtn22.TabIndex = 41
        Me.objbtn22.UseVisualStyleBackColor = False
        '
        'objbtn24
        '
        Me.objbtn24.BackColor = System.Drawing.Color.Fuchsia
        Me.objbtn24.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn24.Location = New System.Drawing.Point(187, 57)
        Me.objbtn24.Name = "objbtn24"
        Me.objbtn24.Size = New System.Drawing.Size(20, 20)
        Me.objbtn24.TabIndex = 51
        Me.objbtn24.UseVisualStyleBackColor = False
        '
        'objbtn5
        '
        Me.objbtn5.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn5.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn5.Location = New System.Drawing.Point(109, 5)
        Me.objbtn5.Name = "objbtn5"
        Me.objbtn5.Size = New System.Drawing.Size(20, 20)
        Me.objbtn5.TabIndex = 33
        Me.objbtn5.UseVisualStyleBackColor = False
        '
        'objbtn12
        '
        Me.objbtn12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn12.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn12.Location = New System.Drawing.Point(83, 31)
        Me.objbtn12.Name = "objbtn12"
        Me.objbtn12.Size = New System.Drawing.Size(20, 20)
        Me.objbtn12.TabIndex = 29
        Me.objbtn12.UseVisualStyleBackColor = False
        '
        'objbtn28
        '
        Me.objbtn28.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn28.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn28.Location = New System.Drawing.Point(83, 83)
        Me.objbtn28.Name = "objbtn28"
        Me.objbtn28.Size = New System.Drawing.Size(20, 20)
        Me.objbtn28.TabIndex = 55
        Me.objbtn28.UseVisualStyleBackColor = False
        '
        'objbtn17
        '
        Me.objbtn17.BackColor = System.Drawing.Color.Silver
        Me.objbtn17.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn17.Location = New System.Drawing.Point(5, 57)
        Me.objbtn17.Name = "objbtn17"
        Me.objbtn17.Size = New System.Drawing.Size(20, 20)
        Me.objbtn17.TabIndex = 45
        Me.objbtn17.UseVisualStyleBackColor = False
        '
        'objbtn37
        '
        Me.objbtn37.BackColor = System.Drawing.Color.Green
        Me.objbtn37.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn37.Location = New System.Drawing.Point(109, 109)
        Me.objbtn37.Name = "objbtn37"
        Me.objbtn37.Size = New System.Drawing.Size(20, 20)
        Me.objbtn37.TabIndex = 64
        Me.objbtn37.UseVisualStyleBackColor = False
        '
        'objbtn40
        '
        Me.objbtn40.BackColor = System.Drawing.Color.Purple
        Me.objbtn40.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn40.Location = New System.Drawing.Point(187, 109)
        Me.objbtn40.Name = "objbtn40"
        Me.objbtn40.Size = New System.Drawing.Size(20, 20)
        Me.objbtn40.TabIndex = 67
        Me.objbtn40.UseVisualStyleBackColor = False
        '
        'objbtn9
        '
        Me.objbtn9.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.objbtn9.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn9.Location = New System.Drawing.Point(5, 31)
        Me.objbtn9.Name = "objbtn9"
        Me.objbtn9.Size = New System.Drawing.Size(20, 20)
        Me.objbtn9.TabIndex = 42
        Me.objbtn9.UseVisualStyleBackColor = False
        '
        'objbtn25
        '
        Me.objbtn25.BackColor = System.Drawing.Color.Gray
        Me.objbtn25.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn25.Location = New System.Drawing.Point(5, 83)
        Me.objbtn25.Name = "objbtn25"
        Me.objbtn25.Size = New System.Drawing.Size(20, 20)
        Me.objbtn25.TabIndex = 52
        Me.objbtn25.UseVisualStyleBackColor = False
        '
        'objbtn6
        '
        Me.objbtn6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn6.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn6.Location = New System.Drawing.Point(135, 5)
        Me.objbtn6.Name = "objbtn6"
        Me.objbtn6.Size = New System.Drawing.Size(20, 20)
        Me.objbtn6.TabIndex = 32
        Me.objbtn6.UseVisualStyleBackColor = False
        '
        'objbtn13
        '
        Me.objbtn13.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn13.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn13.Location = New System.Drawing.Point(109, 31)
        Me.objbtn13.Name = "objbtn13"
        Me.objbtn13.Size = New System.Drawing.Size(20, 20)
        Me.objbtn13.TabIndex = 30
        Me.objbtn13.UseVisualStyleBackColor = False
        '
        'objbtn29
        '
        Me.objbtn29.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtn29.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn29.Location = New System.Drawing.Point(109, 83)
        Me.objbtn29.Name = "objbtn29"
        Me.objbtn29.Size = New System.Drawing.Size(20, 20)
        Me.objbtn29.TabIndex = 54
        Me.objbtn29.UseVisualStyleBackColor = False
        '
        'objbtn11
        '
        Me.objbtn11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn11.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn11.Location = New System.Drawing.Point(57, 31)
        Me.objbtn11.Name = "objbtn11"
        Me.objbtn11.Size = New System.Drawing.Size(20, 20)
        Me.objbtn11.TabIndex = 44
        Me.objbtn11.UseVisualStyleBackColor = False
        '
        'objbtn38
        '
        Me.objbtn38.BackColor = System.Drawing.Color.Teal
        Me.objbtn38.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn38.Location = New System.Drawing.Point(135, 109)
        Me.objbtn38.Name = "objbtn38"
        Me.objbtn38.Size = New System.Drawing.Size(20, 20)
        Me.objbtn38.TabIndex = 65
        Me.objbtn38.UseVisualStyleBackColor = False
        '
        'objbtn39
        '
        Me.objbtn39.BackColor = System.Drawing.Color.Navy
        Me.objbtn39.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn39.Location = New System.Drawing.Point(161, 109)
        Me.objbtn39.Name = "objbtn39"
        Me.objbtn39.Size = New System.Drawing.Size(20, 20)
        Me.objbtn39.TabIndex = 66
        Me.objbtn39.UseVisualStyleBackColor = False
        '
        'objbtn10
        '
        Me.objbtn10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.objbtn10.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn10.Location = New System.Drawing.Point(31, 31)
        Me.objbtn10.Name = "objbtn10"
        Me.objbtn10.Size = New System.Drawing.Size(20, 20)
        Me.objbtn10.TabIndex = 43
        Me.objbtn10.UseVisualStyleBackColor = False
        '
        'objbtn30
        '
        Me.objbtn30.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtn30.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn30.Location = New System.Drawing.Point(135, 83)
        Me.objbtn30.Name = "objbtn30"
        Me.objbtn30.Size = New System.Drawing.Size(20, 20)
        Me.objbtn30.TabIndex = 53
        Me.objbtn30.UseVisualStyleBackColor = False
        '
        'objbtn14
        '
        Me.objbtn14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.objbtn14.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.objbtn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtn14.Location = New System.Drawing.Point(135, 31)
        Me.objbtn14.Name = "objbtn14"
        Me.objbtn14.Size = New System.Drawing.Size(20, 20)
        Me.objbtn14.TabIndex = 31
        Me.objbtn14.UseVisualStyleBackColor = False
        '
        'tlsOperations
        '
        Me.tlsOperations.BackColor = System.Drawing.Color.Transparent
        Me.tlsOperations.Dock = System.Windows.Forms.DockStyle.None
        Me.tlsOperations.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.tlsOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.objtlbbtnOpenTemplate, Me.objtlbbtnCut, Me.objtlbbtnCopy, Me.objtlbbtnPaste, Me.objtoolStripSeparator1, Me.objcboFont, Me.objcboFontSize, Me.objToolStripSeparator2, Me.objtlbbtnColor, Me.objToolStripSeparator7, Me.objtlbbtnBold, Me.objtlbbtnItalic, Me.objtlbbtnUnderline, Me.objToolStripSeparator4, Me.objtlbbtnLeftAlign, Me.objtlbbtnCenterAlign, Me.objtlbbtnRightAlign, Me.objToolStripSeparator5, Me.objtlbbtnUndo, Me.objtlbbtnRedo, Me.objToolStripSeparator3, Me.objtlbbtnBulletSimple, Me.objToolStripSeparator8, Me.objtlbbtnLeftIndent, Me.objtlbbtnRightIndent})
        Me.tlsOperations.Location = New System.Drawing.Point(91, 88)
        Me.tlsOperations.Name = "tlsOperations"
        Me.tlsOperations.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tlsOperations.Size = New System.Drawing.Size(588, 25)
        Me.tlsOperations.TabIndex = 151
        '
        'objtlbbtnOpenTemplate
        '
        Me.objtlbbtnOpenTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnOpenTemplate.Image = CType(resources.GetObject("objtlbbtnOpenTemplate.Image"), System.Drawing.Image)
        Me.objtlbbtnOpenTemplate.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnOpenTemplate.Name = "objtlbbtnOpenTemplate"
        Me.objtlbbtnOpenTemplate.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnOpenTemplate.Text = "ToolStripButton1"
        '
        'objtlbbtnCut
        '
        Me.objtlbbtnCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCut.Image = CType(resources.GetObject("objtlbbtnCut.Image"), System.Drawing.Image)
        Me.objtlbbtnCut.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCut.Name = "objtlbbtnCut"
        Me.objtlbbtnCut.Size = New System.Drawing.Size(23, 22)
        '
        'objtlbbtnCopy
        '
        Me.objtlbbtnCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCopy.Image = CType(resources.GetObject("objtlbbtnCopy.Image"), System.Drawing.Image)
        Me.objtlbbtnCopy.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCopy.Name = "objtlbbtnCopy"
        Me.objtlbbtnCopy.Size = New System.Drawing.Size(23, 22)
        '
        'objtlbbtnPaste
        '
        Me.objtlbbtnPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnPaste.Image = CType(resources.GetObject("objtlbbtnPaste.Image"), System.Drawing.Image)
        Me.objtlbbtnPaste.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnPaste.Name = "objtlbbtnPaste"
        Me.objtlbbtnPaste.Size = New System.Drawing.Size(23, 22)
        '
        'objtoolStripSeparator1
        '
        Me.objtoolStripSeparator1.Name = "objtoolStripSeparator1"
        Me.objtoolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'objcboFont
        '
        Me.objcboFont.DropDownHeight = 150
        Me.objcboFont.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.objcboFont.DropDownWidth = 250
        Me.objcboFont.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFont.IntegralHeight = False
        Me.objcboFont.Name = "objcboFont"
        Me.objcboFont.Size = New System.Drawing.Size(125, 25)
        Me.objcboFont.ToolTipText = "Select Font"
        '
        'objcboFontSize
        '
        Me.objcboFontSize.AutoSize = False
        Me.objcboFontSize.DropDownWidth = 75
        Me.objcboFontSize.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.objcboFontSize.Name = "objcboFontSize"
        Me.objcboFontSize.Size = New System.Drawing.Size(40, 23)
        Me.objcboFontSize.ToolTipText = "Font Size"
        '
        'objToolStripSeparator2
        '
        Me.objToolStripSeparator2.Name = "objToolStripSeparator2"
        Me.objToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnColor
        '
        Me.objtlbbtnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnColor.Image = CType(resources.GetObject("objtlbbtnColor.Image"), System.Drawing.Image)
        Me.objtlbbtnColor.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnColor.Name = "objtlbbtnColor"
        Me.objtlbbtnColor.Size = New System.Drawing.Size(29, 22)
        Me.objtlbbtnColor.ToolTipText = "Color"
        '
        'objToolStripSeparator7
        '
        Me.objToolStripSeparator7.Name = "objToolStripSeparator7"
        Me.objToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnBold
        '
        Me.objtlbbtnBold.CheckOnClick = True
        Me.objtlbbtnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBold.Image = CType(resources.GetObject("objtlbbtnBold.Image"), System.Drawing.Image)
        Me.objtlbbtnBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBold.Name = "objtlbbtnBold"
        Me.objtlbbtnBold.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnBold.ToolTipText = "Bold"
        '
        'objtlbbtnItalic
        '
        Me.objtlbbtnItalic.CheckOnClick = True
        Me.objtlbbtnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnItalic.Image = CType(resources.GetObject("objtlbbtnItalic.Image"), System.Drawing.Image)
        Me.objtlbbtnItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnItalic.Name = "objtlbbtnItalic"
        Me.objtlbbtnItalic.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnItalic.ToolTipText = "Italic"
        '
        'objtlbbtnUnderline
        '
        Me.objtlbbtnUnderline.CheckOnClick = True
        Me.objtlbbtnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUnderline.Image = CType(resources.GetObject("objtlbbtnUnderline.Image"), System.Drawing.Image)
        Me.objtlbbtnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUnderline.Name = "objtlbbtnUnderline"
        Me.objtlbbtnUnderline.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnUnderline.ToolTipText = "Underline"
        '
        'objToolStripSeparator4
        '
        Me.objToolStripSeparator4.Name = "objToolStripSeparator4"
        Me.objToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnLeftAlign
        '
        Me.objtlbbtnLeftAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftAlign.Image = CType(resources.GetObject("objtlbbtnLeftAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftAlign.Name = "objtlbbtnLeftAlign"
        Me.objtlbbtnLeftAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnLeftAlign.ToolTipText = "Align Left"
        '
        'objtlbbtnCenterAlign
        '
        Me.objtlbbtnCenterAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnCenterAlign.Image = CType(resources.GetObject("objtlbbtnCenterAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnCenterAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnCenterAlign.Name = "objtlbbtnCenterAlign"
        Me.objtlbbtnCenterAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnCenterAlign.ToolTipText = "Center"
        '
        'objtlbbtnRightAlign
        '
        Me.objtlbbtnRightAlign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightAlign.Image = CType(resources.GetObject("objtlbbtnRightAlign.Image"), System.Drawing.Image)
        Me.objtlbbtnRightAlign.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightAlign.Name = "objtlbbtnRightAlign"
        Me.objtlbbtnRightAlign.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRightAlign.ToolTipText = "Align Right"
        '
        'objToolStripSeparator5
        '
        Me.objToolStripSeparator5.Name = "objToolStripSeparator5"
        Me.objToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnUndo
        '
        Me.objtlbbtnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnUndo.Image = CType(resources.GetObject("objtlbbtnUndo.Image"), System.Drawing.Image)
        Me.objtlbbtnUndo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnUndo.Name = "objtlbbtnUndo"
        Me.objtlbbtnUndo.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnUndo.ToolTipText = "Undo"
        '
        'objtlbbtnRedo
        '
        Me.objtlbbtnRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRedo.Image = CType(resources.GetObject("objtlbbtnRedo.Image"), System.Drawing.Image)
        Me.objtlbbtnRedo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRedo.Name = "objtlbbtnRedo"
        Me.objtlbbtnRedo.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRedo.ToolTipText = "Redo"
        '
        'objToolStripSeparator3
        '
        Me.objToolStripSeparator3.Name = "objToolStripSeparator3"
        Me.objToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnBulletSimple
        '
        Me.objtlbbtnBulletSimple.CheckOnClick = True
        Me.objtlbbtnBulletSimple.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnBulletSimple.Image = CType(resources.GetObject("objtlbbtnBulletSimple.Image"), System.Drawing.Image)
        Me.objtlbbtnBulletSimple.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnBulletSimple.Name = "objtlbbtnBulletSimple"
        Me.objtlbbtnBulletSimple.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnBulletSimple.ToolTipText = "Insert Bullet"
        '
        'objToolStripSeparator8
        '
        Me.objToolStripSeparator8.Name = "objToolStripSeparator8"
        Me.objToolStripSeparator8.Size = New System.Drawing.Size(6, 25)
        '
        'objtlbbtnLeftIndent
        '
        Me.objtlbbtnLeftIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnLeftIndent.Image = CType(resources.GetObject("objtlbbtnLeftIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnLeftIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnLeftIndent.Name = "objtlbbtnLeftIndent"
        Me.objtlbbtnLeftIndent.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnLeftIndent.ToolTipText = "Left Indent"
        '
        'objtlbbtnRightIndent
        '
        Me.objtlbbtnRightIndent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.objtlbbtnRightIndent.Image = CType(resources.GetObject("objtlbbtnRightIndent.Image"), System.Drawing.Image)
        Me.objtlbbtnRightIndent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.objtlbbtnRightIndent.Name = "objtlbbtnRightIndent"
        Me.objtlbbtnRightIndent.Size = New System.Drawing.Size(23, 22)
        Me.objtlbbtnRightIndent.ToolTipText = "Right Indent"
        '
        'btnTo
        '
        Me.btnTo.BorderColor = System.Drawing.Color.Black
        Me.btnTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTo.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnTo.Location = New System.Drawing.Point(7, 6)
        Me.btnTo.Name = "btnTo"
        Me.btnTo.ShowDefaultBorderColor = True
        Me.btnTo.Size = New System.Drawing.Size(76, 30)
        Me.btnTo.SplitButtonMenu = Me.cmnuMailTo
        Me.btnTo.TabIndex = 10
        Me.btnTo.Text = "To"
        '
        'cmnuMailTo
        '
        Me.cmnuMailTo.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuApplicant, Me.mnuEmployee, Me.mnuSendPayslip, Me.mnuSendSickSheet, Me.mnuAppraisalLetters, Me.mnuDiscipline, Me.mnuSendLeaveForm, Me.mnuEmailReports, Me.mnuLoanBalance})
        Me.cmnuMailTo.Name = "cmnuMailTo"
        Me.cmnuMailTo.Size = New System.Drawing.Size(191, 224)
        '
        'mnuApplicant
        '
        Me.mnuApplicant.Name = "mnuApplicant"
        Me.mnuApplicant.Size = New System.Drawing.Size(190, 22)
        Me.mnuApplicant.Tag = "mnuApplicant"
        Me.mnuApplicant.Text = "Applicant"
        '
        'mnuEmployee
        '
        Me.mnuEmployee.Name = "mnuEmployee"
        Me.mnuEmployee.Size = New System.Drawing.Size(190, 22)
        Me.mnuEmployee.Tag = "mnuEmployee"
        Me.mnuEmployee.Text = "Employee"
        '
        'mnuSendPayslip
        '
        Me.mnuSendPayslip.Name = "mnuSendPayslip"
        Me.mnuSendPayslip.Size = New System.Drawing.Size(190, 22)
        Me.mnuSendPayslip.Text = "Employee Payslip"
        '
        'mnuSendSickSheet
        '
        Me.mnuSendSickSheet.Name = "mnuSendSickSheet"
        Me.mnuSendSickSheet.Size = New System.Drawing.Size(190, 22)
        Me.mnuSendSickSheet.Text = "Employee SickSheet"
        '
        'mnuAppraisalLetters
        '
        Me.mnuAppraisalLetters.Name = "mnuAppraisalLetters"
        Me.mnuAppraisalLetters.Size = New System.Drawing.Size(190, 22)
        Me.mnuAppraisalLetters.Text = "Appraisal Letters"
        '
        'mnuDiscipline
        '
        Me.mnuDiscipline.Name = "mnuDiscipline"
        Me.mnuDiscipline.Size = New System.Drawing.Size(190, 22)
        Me.mnuDiscipline.Tag = "mnuDiscipline"
        Me.mnuDiscipline.Text = "&Discipline"
        '
        'mnuSendLeaveForm
        '
        Me.mnuSendLeaveForm.Name = "mnuSendLeaveForm"
        Me.mnuSendLeaveForm.Size = New System.Drawing.Size(190, 22)
        Me.mnuSendLeaveForm.Text = "Employee Leave Form"
        '
        'mnuEmailReports
        '
        Me.mnuEmailReports.Name = "mnuEmailReports"
        Me.mnuEmailReports.Size = New System.Drawing.Size(190, 22)
        Me.mnuEmailReports.Tag = "mnuEmailReports"
        Me.mnuEmailReports.Text = "Email Daily Timesheet"
        '
        'txtToAddress
        '
        Me.txtToAddress.BackColor = System.Drawing.Color.White
        Me.txtToAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToAddress.Location = New System.Drawing.Point(91, 6)
        Me.txtToAddress.Multiline = True
        Me.txtToAddress.Name = "txtToAddress"
        Me.txtToAddress.ReadOnly = True
        Me.txtToAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtToAddress.Size = New System.Drawing.Size(637, 52)
        Me.txtToAddress.TabIndex = 1
        '
        'txtSubject
        '
        Me.txtSubject.Flags = 0
        Me.txtSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubject.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSubject.Location = New System.Drawing.Point(91, 64)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(637, 21)
        Me.txtSubject.TabIndex = 3
        '
        'lblSubject
        '
        Me.lblSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.lblSubject.Location = New System.Drawing.Point(7, 67)
        Me.lblSubject.Name = "lblSubject"
        Me.lblSubject.Size = New System.Drawing.Size(78, 15)
        Me.lblSubject.TabIndex = 2
        Me.lblSubject.Text = "Subject"
        Me.lblSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpAttachment
        '
        Me.tabpAttachment.Controls.Add(Me.pnlButtonGroup)
        Me.tabpAttachment.Controls.Add(Me.lvAttachedFile)
        Me.tabpAttachment.Location = New System.Drawing.Point(4, 22)
        Me.tabpAttachment.Name = "tabpAttachment"
        Me.tabpAttachment.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAttachment.Size = New System.Drawing.Size(740, 340)
        Me.tabpAttachment.TabIndex = 1
        Me.tabpAttachment.Text = "Attachment Information"
        Me.tabpAttachment.UseVisualStyleBackColor = True
        '
        'pnlButtonGroup
        '
        Me.pnlButtonGroup.Controls.Add(Me.btnEmployeeDocument)
        Me.pnlButtonGroup.Controls.Add(Me.btnRemove)
        Me.pnlButtonGroup.Controls.Add(Me.btnAttach)
        Me.pnlButtonGroup.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlButtonGroup.Location = New System.Drawing.Point(3, 300)
        Me.pnlButtonGroup.Name = "pnlButtonGroup"
        Me.pnlButtonGroup.Size = New System.Drawing.Size(734, 37)
        Me.pnlButtonGroup.TabIndex = 3
        '
        'btnEmployeeDocument
        '
        Me.btnEmployeeDocument.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmployeeDocument.BackColor = System.Drawing.Color.White
        Me.btnEmployeeDocument.BackgroundImage = CType(resources.GetObject("btnEmployeeDocument.BackgroundImage"), System.Drawing.Image)
        Me.btnEmployeeDocument.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmployeeDocument.BorderColor = System.Drawing.Color.Empty
        Me.btnEmployeeDocument.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmployeeDocument.FlatAppearance.BorderSize = 0
        Me.btnEmployeeDocument.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployeeDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployeeDocument.ForeColor = System.Drawing.Color.Black
        Me.btnEmployeeDocument.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmployeeDocument.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeDocument.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeDocument.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeDocument.Location = New System.Drawing.Point(3, 3)
        Me.btnEmployeeDocument.Name = "btnEmployeeDocument"
        Me.btnEmployeeDocument.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeDocument.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeDocument.Size = New System.Drawing.Size(134, 30)
        Me.btnEmployeeDocument.TabIndex = 6
        Me.btnEmployeeDocument.Text = "&Employee Document"
        Me.btnEmployeeDocument.UseVisualStyleBackColor = False
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.BackColor = System.Drawing.Color.White
        Me.btnRemove.BackgroundImage = CType(resources.GetObject("btnRemove.BackgroundImage"), System.Drawing.Image)
        Me.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemove.BorderColor = System.Drawing.Color.Empty
        Me.btnRemove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemove.FlatAppearance.BorderSize = 0
        Me.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.ForeColor = System.Drawing.Color.Black
        Me.btnRemove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemove.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Location = New System.Drawing.Point(639, 3)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Size = New System.Drawing.Size(90, 30)
        Me.btnRemove.TabIndex = 5
        Me.btnRemove.Text = "&Remove"
        Me.btnRemove.UseVisualStyleBackColor = False
        '
        'btnAttach
        '
        Me.btnAttach.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAttach.BackColor = System.Drawing.Color.White
        Me.btnAttach.BackgroundImage = CType(resources.GetObject("btnAttach.BackgroundImage"), System.Drawing.Image)
        Me.btnAttach.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAttach.BorderColor = System.Drawing.Color.Empty
        Me.btnAttach.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAttach.FlatAppearance.BorderSize = 0
        Me.btnAttach.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAttach.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAttach.ForeColor = System.Drawing.Color.Black
        Me.btnAttach.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAttach.GradientForeColor = System.Drawing.Color.Black
        Me.btnAttach.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttach.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAttach.Location = New System.Drawing.Point(543, 3)
        Me.btnAttach.Name = "btnAttach"
        Me.btnAttach.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAttach.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAttach.Size = New System.Drawing.Size(90, 30)
        Me.btnAttach.TabIndex = 4
        Me.btnAttach.Text = "&Browse"
        Me.btnAttach.UseVisualStyleBackColor = False
        '
        'lvAttachedFile
        '
        Me.lvAttachedFile.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhFiles, Me.objcolhIsEmpDoc})
        Me.lvAttachedFile.Dock = System.Windows.Forms.DockStyle.Top
        Me.lvAttachedFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAttachedFile.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvAttachedFile.Location = New System.Drawing.Point(3, 3)
        Me.lvAttachedFile.MultiSelect = False
        Me.lvAttachedFile.Name = "lvAttachedFile"
        Me.lvAttachedFile.Size = New System.Drawing.Size(734, 294)
        Me.lvAttachedFile.TabIndex = 2
        Me.lvAttachedFile.UseCompatibleStateImageBehavior = False
        Me.lvAttachedFile.View = System.Windows.Forms.View.Details
        '
        'objcolhFiles
        '
        Me.objcolhFiles.Tag = "objcolhFiles"
        Me.objcolhFiles.Text = ""
        Me.objcolhFiles.Width = 610
        '
        'objcolhIsEmpDoc
        '
        Me.objcolhIsEmpDoc.Tag = "objcolhIsEmpDoc"
        Me.objcolhIsEmpDoc.Text = ""
        Me.objcolhIsEmpDoc.Width = 0
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.pbProgress)
        Me.objefFormFooter.Controls.Add(Me.objbtnPrevious)
        Me.objefFormFooter.Controls.Add(Me.objbtnNext)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnSend)
        Me.objefFormFooter.Controls.Add(Me.btnViewMerge)
        Me.objefFormFooter.Controls.Add(Me.btnFinish)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 424)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(748, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'pbProgress
        '
        Me.pbProgress.Location = New System.Drawing.Point(95, 18)
        Me.pbProgress.Margin = New System.Windows.Forms.Padding(0)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(353, 19)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 10
        Me.pbProgress.Visible = False
        '
        'objbtnPrevious
        '
        Me.objbtnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnPrevious.BackColor = System.Drawing.Color.White
        Me.objbtnPrevious.BackgroundImage = CType(resources.GetObject("objbtnPrevious.BackgroundImage"), System.Drawing.Image)
        Me.objbtnPrevious.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnPrevious.BorderColor = System.Drawing.Color.Empty
        Me.objbtnPrevious.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnPrevious.FlatAppearance.BorderSize = 0
        Me.objbtnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnPrevious.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnPrevious.ForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnPrevious.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnPrevious.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnPrevious.Location = New System.Drawing.Point(14, 12)
        Me.objbtnPrevious.Name = "objbtnPrevious"
        Me.objbtnPrevious.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnPrevious.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnPrevious.Size = New System.Drawing.Size(30, 30)
        Me.objbtnPrevious.TabIndex = 0
        Me.objbtnPrevious.UseVisualStyleBackColor = False
        '
        'objbtnNext
        '
        Me.objbtnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnNext.BackColor = System.Drawing.Color.White
        Me.objbtnNext.BackgroundImage = CType(resources.GetObject("objbtnNext.BackgroundImage"), System.Drawing.Image)
        Me.objbtnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnNext.BorderColor = System.Drawing.Color.Empty
        Me.objbtnNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnNext.FlatAppearance.BorderSize = 0
        Me.objbtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnNext.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnNext.ForeColor = System.Drawing.Color.Black
        Me.objbtnNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnNext.Location = New System.Drawing.Point(50, 12)
        Me.objbtnNext.Name = "objbtnNext"
        Me.objbtnNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnNext.Size = New System.Drawing.Size(30, 30)
        Me.objbtnNext.TabIndex = 1
        Me.objbtnNext.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(646, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSend
        '
        Me.btnSend.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSend.BackColor = System.Drawing.Color.White
        Me.btnSend.BackgroundImage = CType(resources.GetObject("btnSend.BackgroundImage"), System.Drawing.Image)
        Me.btnSend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSend.BorderColor = System.Drawing.Color.Empty
        Me.btnSend.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSend.FlatAppearance.BorderSize = 0
        Me.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSend.ForeColor = System.Drawing.Color.Black
        Me.btnSend.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSend.GradientForeColor = System.Drawing.Color.Black
        Me.btnSend.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSend.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSend.Location = New System.Drawing.Point(550, 12)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSend.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSend.Size = New System.Drawing.Size(90, 30)
        Me.btnSend.TabIndex = 3
        Me.btnSend.Text = "&Send"
        Me.btnSend.UseVisualStyleBackColor = False
        '
        'btnViewMerge
        '
        Me.btnViewMerge.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewMerge.BackColor = System.Drawing.Color.White
        Me.btnViewMerge.BackgroundImage = CType(resources.GetObject("btnViewMerge.BackgroundImage"), System.Drawing.Image)
        Me.btnViewMerge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewMerge.BorderColor = System.Drawing.Color.Empty
        Me.btnViewMerge.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewMerge.FlatAppearance.BorderSize = 0
        Me.btnViewMerge.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewMerge.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewMerge.ForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewMerge.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewMerge.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.Location = New System.Drawing.Point(454, 12)
        Me.btnViewMerge.Name = "btnViewMerge"
        Me.btnViewMerge.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewMerge.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewMerge.Size = New System.Drawing.Size(90, 30)
        Me.btnViewMerge.TabIndex = 2
        Me.btnViewMerge.Text = "&View Merge"
        Me.btnViewMerge.UseVisualStyleBackColor = False
        '
        'btnFinish
        '
        Me.btnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFinish.BackColor = System.Drawing.Color.White
        Me.btnFinish.BackgroundImage = CType(resources.GetObject("btnFinish.BackgroundImage"), System.Drawing.Image)
        Me.btnFinish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinish.BorderColor = System.Drawing.Color.Empty
        Me.btnFinish.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinish.FlatAppearance.BorderSize = 0
        Me.btnFinish.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinish.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinish.ForeColor = System.Drawing.Color.Black
        Me.btnFinish.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinish.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinish.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.Location = New System.Drawing.Point(454, 12)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinish.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinish.Size = New System.Drawing.Size(90, 30)
        Me.btnFinish.TabIndex = 9
        Me.btnFinish.Text = "&Finish"
        Me.btnFinish.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.Color.White
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(748, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Send Mail"
        '
        'ofdMailAttachment
        '
        Me.ofdMailAttachment.FileName = "OpenFileDialog1"
        '
        'bgwSendMail
        '
        Me.bgwSendMail.WorkerReportsProgress = True
        Me.bgwSendMail.WorkerSupportsCancellation = True
        '
        'mnuLoanBalance
        '
        Me.mnuLoanBalance.Name = "mnuLoanBalance"
        Me.mnuLoanBalance.Size = New System.Drawing.Size(190, 22)
        Me.mnuLoanBalance.Text = "Loan Balance"
        '
        'frmSendMail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 479)
        Me.Controls.Add(Me.pnlMailCofirmation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmSendMail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Send Mail"
        Me.pnlMailCofirmation.ResumeLayout(False)
        Me.tabcSendMail.ResumeLayout(False)
        Me.tabpEmailInfo.ResumeLayout(False)
        Me.tabpEmailInfo.PerformLayout()
        Me.pnlControlGroup.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.pnlColor.ResumeLayout(False)
        Me.tlsOperations.ResumeLayout(False)
        Me.tlsOperations.PerformLayout()
        Me.cmnuMailTo.ResumeLayout(False)
        Me.tabpAttachment.ResumeLayout(False)
        Me.pnlButtonGroup.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMailCofirmation As System.Windows.Forms.Panel
    Friend WithEvents ofdMailAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCut As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnFinish As eZee.Common.eZeeLightButton
    Friend WithEvents btnViewMerge As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSend As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnPrevious As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnNext As eZee.Common.eZeeLightButton
    Friend WithEvents pnlControlGroup As System.Windows.Forms.Panel
    Friend WithEvents rtbDocument As System.Windows.Forms.RichTextBox
    Friend WithEvents txtToAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtSubject As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSubject As System.Windows.Forms.Label
    Friend WithEvents lvAttachedFile As System.Windows.Forms.ListView
    Friend WithEvents pnlButtonGroup As System.Windows.Forms.Panel
    Friend WithEvents btnRemove As eZee.Common.eZeeLightButton
    Friend WithEvents tlsOperations As System.Windows.Forms.ToolStrip
    Friend WithEvents objtlbbtnCut As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCopy As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnPaste As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtoolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objcboFont As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objcboFontSize As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents objToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnColor As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents objToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnUnderline As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnCenterAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightAlign As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnUndo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRedo As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnBulletSimple As System.Windows.Forms.ToolStripButton
    Friend WithEvents objToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objtlbbtnLeftIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtlbbtnRightIndent As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnlColor As System.Windows.Forms.Panel
    Friend WithEvents objbtn48 As System.Windows.Forms.Button
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents btnMoreColor As System.Windows.Forms.Button
    Friend WithEvents objbtn47 As System.Windows.Forms.Button
    Friend WithEvents objbtn34 As System.Windows.Forms.Button
    Friend WithEvents objbtn18 As System.Windows.Forms.Button
    Friend WithEvents objbtn46 As System.Windows.Forms.Button
    Friend WithEvents objbtn32 As System.Windows.Forms.Button
    Friend WithEvents objbtn33 As System.Windows.Forms.Button
    Friend WithEvents objbtn45 As System.Windows.Forms.Button
    Friend WithEvents objbtn19 As System.Windows.Forms.Button
    Friend WithEvents objbtn2 As System.Windows.Forms.Button
    Friend WithEvents objbtn44 As System.Windows.Forms.Button
    Friend WithEvents objbtn31 As System.Windows.Forms.Button
    Friend WithEvents objbtn42 As System.Windows.Forms.Button
    Friend WithEvents objbtn20 As System.Windows.Forms.Button
    Friend WithEvents objbtn43 As System.Windows.Forms.Button
    Friend WithEvents objbtn3 As System.Windows.Forms.Button
    Friend WithEvents objbtn15 As System.Windows.Forms.Button
    Friend WithEvents objbtn26 As System.Windows.Forms.Button
    Friend WithEvents objbtn16 As System.Windows.Forms.Button
    Friend WithEvents objbtn35 As System.Windows.Forms.Button
    Friend WithEvents objbtn41 As System.Windows.Forms.Button
    Friend WithEvents objbtn21 As System.Windows.Forms.Button
    Friend WithEvents objbtn8 As System.Windows.Forms.Button
    Friend WithEvents objbtn4 As System.Windows.Forms.Button
    Friend WithEvents objbtn23 As System.Windows.Forms.Button
    Friend WithEvents objbtn27 As System.Windows.Forms.Button
    Friend WithEvents objbtn1 As System.Windows.Forms.Button
    Friend WithEvents objbtn36 As System.Windows.Forms.Button
    Friend WithEvents objbtn7 As System.Windows.Forms.Button
    Friend WithEvents objbtn22 As System.Windows.Forms.Button
    Friend WithEvents objbtn24 As System.Windows.Forms.Button
    Friend WithEvents objbtn5 As System.Windows.Forms.Button
    Friend WithEvents objbtn12 As System.Windows.Forms.Button
    Friend WithEvents objbtn28 As System.Windows.Forms.Button
    Friend WithEvents objbtn17 As System.Windows.Forms.Button
    Friend WithEvents objbtn37 As System.Windows.Forms.Button
    Friend WithEvents objbtn40 As System.Windows.Forms.Button
    Friend WithEvents objbtn9 As System.Windows.Forms.Button
    Friend WithEvents objbtn25 As System.Windows.Forms.Button
    Friend WithEvents objbtn6 As System.Windows.Forms.Button
    Friend WithEvents objbtn13 As System.Windows.Forms.Button
    Friend WithEvents objbtn29 As System.Windows.Forms.Button
    Friend WithEvents objbtn11 As System.Windows.Forms.Button
    Friend WithEvents objbtn38 As System.Windows.Forms.Button
    Friend WithEvents objbtn39 As System.Windows.Forms.Button
    Friend WithEvents objbtn10 As System.Windows.Forms.Button
    Friend WithEvents objbtn30 As System.Windows.Forms.Button
    Friend WithEvents objbtn14 As System.Windows.Forms.Button
    Friend WithEvents objtlbbtnOpenTemplate As System.Windows.Forms.ToolStripButton
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents cmnuMailTo As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuApplicant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnTo As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuSendPayslip As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bgwSendMail As System.ComponentModel.BackgroundWorker
    Friend WithEvents mnuSendSickSheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAppraisalLetters As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAttach As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeDocument As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhFiles As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsEmpDoc As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabcSendMail As System.Windows.Forms.TabControl
    Friend WithEvents tabpEmailInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpAttachment As System.Windows.Forms.TabPage
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents mnuDiscipline As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents mnuSendLeaveForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEmailReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLoanBalance As System.Windows.Forms.ToolStripMenuItem
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpAssetDeclarationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpAssetDeclarationList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lvAssetDeclare = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheckBox = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhNoOfEmployment = New System.Windows.Forms.ColumnHeader
        Me.colhCenterForWork = New System.Windows.Forms.ColumnHeader
        Me.colhPosition = New System.Windows.Forms.ColumnHeader
        Me.colhWorkstation = New System.Windows.Forms.ColumnHeader
        Me.objcolhYearName = New System.Windows.Forms.ColumnHeader
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeClosedYearTrans = New System.Windows.Forms.CheckBox
        Me.lblCarRegistration = New System.Windows.Forms.Label
        Me.txtCarRegNo = New System.Windows.Forms.TextBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.EZeeSplitButton1 = New eZee.Common.eZeeSplitButton
        Me.cmnuADOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuScanDocuments = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRemoveDeclaration = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintDeclaration = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlockFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objcolhYearId = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEmployeeInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuADOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lvAssetDeclare)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(683, 452)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lvAssetDeclare
        '
        Me.lvAssetDeclare.BackColorOnChecked = False
        Me.lvAssetDeclare.ColumnHeaders = Nothing
        Me.lvAssetDeclare.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheckBox, Me.colhEmployee, Me.colhNoOfEmployment, Me.colhCenterForWork, Me.colhPosition, Me.colhWorkstation, Me.objcolhYearName, Me.objcolhYearId})
        Me.lvAssetDeclare.CompulsoryColumns = ""
        Me.lvAssetDeclare.FullRowSelect = True
        Me.lvAssetDeclare.GridLines = True
        Me.lvAssetDeclare.GroupingColumn = Nothing
        Me.lvAssetDeclare.HideSelection = False
        Me.lvAssetDeclare.Location = New System.Drawing.Point(9, 157)
        Me.lvAssetDeclare.MinColumnWidth = 50
        Me.lvAssetDeclare.MultiSelect = False
        Me.lvAssetDeclare.Name = "lvAssetDeclare"
        Me.lvAssetDeclare.OptionalColumns = ""
        Me.lvAssetDeclare.ShowMoreItem = False
        Me.lvAssetDeclare.ShowSaveItem = False
        Me.lvAssetDeclare.ShowSelectAll = True
        Me.lvAssetDeclare.ShowSizeAllColumnsToFit = True
        Me.lvAssetDeclare.Size = New System.Drawing.Size(657, 219)
        Me.lvAssetDeclare.Sortable = True
        Me.lvAssetDeclare.TabIndex = 115
        Me.lvAssetDeclare.UseCompatibleStateImageBehavior = False
        Me.lvAssetDeclare.View = System.Windows.Forms.View.Details
        '
        'colhCheckBox
        '
        Me.colhCheckBox.Tag = "colhCheckBox"
        Me.colhCheckBox.Text = ""
        Me.colhCheckBox.Width = 0
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 200
        '
        'colhNoOfEmployment
        '
        Me.colhNoOfEmployment.Tag = "colhNoOfEmployment"
        Me.colhNoOfEmployment.Text = "Employment Code"
        Me.colhNoOfEmployment.Width = 130
        '
        'colhCenterForWork
        '
        Me.colhCenterForWork.Tag = "colhCenterForWork"
        Me.colhCenterForWork.Text = "Department"
        Me.colhCenterForWork.Width = 170
        '
        'colhPosition
        '
        Me.colhPosition.Tag = "colhPosition"
        Me.colhPosition.Text = "Office / Position"
        Me.colhPosition.Width = 150
        '
        'colhWorkstation
        '
        Me.colhWorkstation.Tag = "colhWorkstation"
        Me.colhWorkstation.Text = "Workstation"
        Me.colhWorkstation.Width = 0
        '
        'objcolhYearName
        '
        Me.objcolhYearName.Tag = "objcolhYearName"
        Me.objcolhYearName.Text = ""
        Me.objcolhYearName.Width = 0
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.chkIncludeClosedYearTrans)
        Me.gbEmployeeInfo.Controls.Add(Me.lblCarRegistration)
        Me.gbEmployeeInfo.Controls.Add(Me.txtCarRegNo)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(654, 85)
        Me.gbEmployeeInfo.TabIndex = 114
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Filter Criteria"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeClosedYearTrans
        '
        Me.chkIncludeClosedYearTrans.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeClosedYearTrans.Location = New System.Drawing.Point(79, 60)
        Me.chkIncludeClosedYearTrans.Name = "chkIncludeClosedYearTrans"
        Me.chkIncludeClosedYearTrans.Size = New System.Drawing.Size(203, 17)
        Me.chkIncludeClosedYearTrans.TabIndex = 234
        Me.chkIncludeClosedYearTrans.Text = "Include Closed Year Transactions"
        Me.chkIncludeClosedYearTrans.UseVisualStyleBackColor = True
        '
        'lblCarRegistration
        '
        Me.lblCarRegistration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCarRegistration.Location = New System.Drawing.Point(309, 37)
        Me.lblCarRegistration.Name = "lblCarRegistration"
        Me.lblCarRegistration.Size = New System.Drawing.Size(99, 13)
        Me.lblCarRegistration.TabIndex = 232
        Me.lblCarRegistration.Text = "Car Registration"
        '
        'txtCarRegNo
        '
        Me.txtCarRegNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCarRegNo.Location = New System.Drawing.Point(414, 33)
        Me.txtCarRegNo.Name = "txtCarRegNo"
        Me.txtCarRegNo.Size = New System.Drawing.Size(232, 21)
        Me.txtCarRegNo.TabIndex = 231
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(627, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 23)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(603, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 23)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(261, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 215
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(79, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(176, 21)
        Me.cboEmployee.TabIndex = 229
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(65, 15)
        Me.lblEmployee.TabIndex = 227
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.EZeeSplitButton1)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 397)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(683, 55)
        Me.objFooter.TabIndex = 4
        '
        'EZeeSplitButton1
        '
        Me.EZeeSplitButton1.BorderColor = System.Drawing.Color.Black
        Me.EZeeSplitButton1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeSplitButton1.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.EZeeSplitButton1.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.EZeeSplitButton1.Location = New System.Drawing.Point(9, 13)
        Me.EZeeSplitButton1.Name = "EZeeSplitButton1"
        Me.EZeeSplitButton1.ShowDefaultBorderColor = True
        Me.EZeeSplitButton1.Size = New System.Drawing.Size(110, 30)
        Me.EZeeSplitButton1.SplitButtonMenu = Me.cmnuADOperations
        Me.EZeeSplitButton1.TabIndex = 8
        Me.EZeeSplitButton1.Text = "Operations"
        '
        'cmnuADOperations
        '
        Me.cmnuADOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScanDocuments, Me.mnuRemoveDeclaration, Me.mnuPrintDeclaration, Me.ToolStripMenuItem1, Me.mnuFinalSave, Me.mnuUnlockFinalSave})
        Me.cmnuADOperations.Name = "cmnuADOperations"
        Me.cmnuADOperations.Size = New System.Drawing.Size(233, 120)
        '
        'mnuScanDocuments
        '
        Me.mnuScanDocuments.Name = "mnuScanDocuments"
        Me.mnuScanDocuments.Size = New System.Drawing.Size(232, 22)
        Me.mnuScanDocuments.Text = "Browse"
        '
        'mnuRemoveDeclaration
        '
        Me.mnuRemoveDeclaration.Name = "mnuRemoveDeclaration"
        Me.mnuRemoveDeclaration.Size = New System.Drawing.Size(232, 22)
        Me.mnuRemoveDeclaration.Text = "Remove Attached Documents"
        '
        'mnuPrintDeclaration
        '
        Me.mnuPrintDeclaration.Name = "mnuPrintDeclaration"
        Me.mnuPrintDeclaration.Size = New System.Drawing.Size(232, 22)
        Me.mnuPrintDeclaration.Text = "Preview Declaration"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(229, 6)
        '
        'mnuFinalSave
        '
        Me.mnuFinalSave.Name = "mnuFinalSave"
        Me.mnuFinalSave.Size = New System.Drawing.Size(232, 22)
        Me.mnuFinalSave.Text = "Final Save"
        '
        'mnuUnlockFinalSave
        '
        Me.mnuUnlockFinalSave.Name = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Size = New System.Drawing.Size(232, 22)
        Me.mnuUnlockFinalSave.Text = "Unlock Final Save"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(471, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(368, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(265, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(574, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(683, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Employee Asset Declaration List"
        '
        'objcolhYearId
        '
        Me.objcolhYearId.Tag = "objcolhYearId"
        Me.objcolhYearId.Width = 0
        '
        'frmEmpAssetDeclarationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(683, 452)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpAssetDeclarationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Asset Declaration List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEmployeeInfo.ResumeLayout(False)
        Me.gbEmployeeInfo.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuADOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lvAssetDeclare As eZee.Common.eZeeListView
    Friend WithEvents colhCheckBox As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNoOfEmployment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPosition As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeSplitButton1 As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuADOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScanDocuments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRemoveDeclaration As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintDeclaration As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlockFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhWorkstation As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtCarRegNo As System.Windows.Forms.TextBox
    Friend WithEvents lblCarRegistration As System.Windows.Forms.Label
    Friend WithEvents colhCenterForWork As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkIncludeClosedYearTrans As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhYearName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhYearId As System.Windows.Forms.ColumnHeader
End Class

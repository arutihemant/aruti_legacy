﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDependentsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDependentsList))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgDepedent = New System.Windows.Forms.DataGridView
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAge = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhMonth = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.dgDepedent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgDepedent)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(445, 306)
        Me.Panel1.TabIndex = 273
        '
        'dgDepedent
        '
        Me.dgDepedent.AllowUserToAddRows = False
        Me.dgDepedent.AllowUserToDeleteRows = False
        Me.dgDepedent.AllowUserToResizeColumns = False
        Me.dgDepedent.AllowUserToResizeRows = False
        Me.dgDepedent.BackgroundColor = System.Drawing.Color.White
        Me.dgDepedent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhName, Me.dgcolhGender, Me.dgcolhAge, Me.dgcolhMonth, Me.dgcolhRelation})
        Me.dgDepedent.Location = New System.Drawing.Point(2, 2)
        Me.dgDepedent.Name = "dgDepedent"
        Me.dgDepedent.ReadOnly = True
        Me.dgDepedent.RowHeadersVisible = False
        Me.dgDepedent.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgDepedent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgDepedent.Size = New System.Drawing.Size(441, 251)
        Me.dgDepedent.TabIndex = 272
        Me.dgDepedent.TabStop = False
        '
        'dgcolhName
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.dgcolhName.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhName.HeaderText = "Name"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        Me.dgcolhName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhName.Width = 137
        '
        'dgcolhGender
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhGender.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhGender.HeaderText = "Gender"
        Me.dgcolhGender.Name = "dgcolhGender"
        Me.dgcolhGender.ReadOnly = True
        Me.dgcolhGender.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhGender.Width = 75
        '
        'dgcolhAge
        '
        Me.dgcolhAge.AllowNegative = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhAge.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhAge.HeaderText = "Age"
        Me.dgcolhAge.Name = "dgcolhAge"
        Me.dgcolhAge.ReadOnly = True
        Me.dgcolhAge.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhAge.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAge.Width = 75
        '
        'dgcolhMonth
        '
        Me.dgcolhMonth.AllowNegative = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F0"
        Me.dgcolhMonth.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhMonth.HeaderText = "Month"
        Me.dgcolhMonth.Name = "dgcolhMonth"
        Me.dgcolhMonth.ReadOnly = True
        Me.dgcolhMonth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhMonth.Width = 60
        '
        'dgcolhRelation
        '
        Me.dgcolhRelation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.NullValue = Nothing
        Me.dgcolhRelation.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhRelation.HeaderText = "Relation"
        Me.dgcolhRelation.Name = "dgcolhRelation"
        Me.dgcolhRelation.ReadOnly = True
        Me.dgcolhRelation.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhRelation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 256)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(445, 50)
        Me.objFooter.TabIndex = 275
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(348, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 15
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmDependentsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(445, 306)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDependentsList"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dependents List"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgDepedent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgDepedent As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAge As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhMonth As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
End Class

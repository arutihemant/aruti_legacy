﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapExpenses_Approver
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapExpenses_Approver))
        Me.gbLeavedetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblApprover = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objlblApproverLevel = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvExpense = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUoM = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpenseId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objApproverFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbLeavedetail.SuspendLayout()
        CType(Me.dgvExpense, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objApproverFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbLeavedetail
        '
        Me.gbLeavedetail.BorderColor = System.Drawing.Color.Black
        Me.gbLeavedetail.Checked = False
        Me.gbLeavedetail.CollapseAllExceptThis = False
        Me.gbLeavedetail.CollapsedHoverImage = Nothing
        Me.gbLeavedetail.CollapsedNormalImage = Nothing
        Me.gbLeavedetail.CollapsedPressedImage = Nothing
        Me.gbLeavedetail.CollapseOnLoad = False
        Me.gbLeavedetail.Controls.Add(Me.objlblApprover)
        Me.gbLeavedetail.Controls.Add(Me.lblApprover)
        Me.gbLeavedetail.Controls.Add(Me.objlblApproverLevel)
        Me.gbLeavedetail.Controls.Add(Me.lblApproverLevel)
        Me.gbLeavedetail.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbLeavedetail.ExpandedHoverImage = Nothing
        Me.gbLeavedetail.ExpandedNormalImage = Nothing
        Me.gbLeavedetail.ExpandedPressedImage = Nothing
        Me.gbLeavedetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavedetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavedetail.HeaderHeight = 25
        Me.gbLeavedetail.HeaderMessage = ""
        Me.gbLeavedetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavedetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavedetail.HeightOnCollapse = 0
        Me.gbLeavedetail.LeftTextSpace = 0
        Me.gbLeavedetail.Location = New System.Drawing.Point(0, 0)
        Me.gbLeavedetail.Name = "gbLeavedetail"
        Me.gbLeavedetail.OpenHeight = 300
        Me.gbLeavedetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavedetail.ShowBorder = True
        Me.gbLeavedetail.ShowCheckBox = False
        Me.gbLeavedetail.ShowCollapseButton = False
        Me.gbLeavedetail.ShowDefaultBorderColor = True
        Me.gbLeavedetail.ShowDownButton = False
        Me.gbLeavedetail.ShowHeader = True
        Me.gbLeavedetail.Size = New System.Drawing.Size(445, 84)
        Me.gbLeavedetail.TabIndex = 9
        Me.gbLeavedetail.Temp = 0
        Me.gbLeavedetail.Text = "Approver Detail"
        Me.gbLeavedetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApprover
        '
        Me.objlblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApprover.Location = New System.Drawing.Point(127, 33)
        Me.objlblApprover.Name = "objlblApprover"
        Me.objlblApprover.Size = New System.Drawing.Size(297, 14)
        Me.objlblApprover.TabIndex = 282
        Me.objlblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(16, 33)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(104, 14)
        Me.lblApprover.TabIndex = 281
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApproverLevel
        '
        Me.objlblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApproverLevel.Location = New System.Drawing.Point(127, 59)
        Me.objlblApproverLevel.Name = "objlblApproverLevel"
        Me.objlblApproverLevel.Size = New System.Drawing.Size(297, 14)
        Me.objlblApproverLevel.TabIndex = 280
        Me.objlblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(16, 59)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(104, 14)
        Me.lblApproverLevel.TabIndex = 279
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.Location = New System.Drawing.Point(6, 89)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(13, 14)
        Me.objchkSelectAll.TabIndex = 284
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        Me.objchkSelectAll.Visible = False
        '
        'dgvExpense
        '
        Me.dgvExpense.AllowUserToAddRows = False
        Me.dgvExpense.AllowUserToDeleteRows = False
        Me.dgvExpense.AllowUserToResizeRows = False
        Me.dgvExpense.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvExpense.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvExpense.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhCode, Me.dgcolhName, Me.dgcolhUoM, Me.objdgcolhExpenseId})
        Me.dgvExpense.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvExpense.Location = New System.Drawing.Point(0, 84)
        Me.dgvExpense.Name = "dgvExpense"
        Me.dgvExpense.RowHeadersVisible = False
        Me.dgvExpense.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvExpense.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvExpense.Size = New System.Drawing.Size(445, 249)
        Me.dgvExpense.TabIndex = 0
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.Frozen = True
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhCode
        '
        Me.dgcolhCode.HeaderText = "Code"
        Me.dgcolhCode.Name = "dgcolhCode"
        Me.dgcolhCode.ReadOnly = True
        Me.dgcolhCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'dgcolhName
        '
        Me.dgcolhName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhName.HeaderText = "Expense"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        Me.dgcolhName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'dgcolhUoM
        '
        Me.dgcolhUoM.HeaderText = "UoM"
        Me.dgcolhUoM.Name = "dgcolhUoM"
        Me.dgcolhUoM.ReadOnly = True
        '
        'objdgcolhExpenseId
        '
        Me.objdgcolhExpenseId.HeaderText = "objdgcolhExpenseId"
        Me.objdgcolhExpenseId.Name = "objdgcolhExpenseId"
        Me.objdgcolhExpenseId.ReadOnly = True
        Me.objdgcolhExpenseId.Visible = False
        '
        'objApproverFooter
        '
        Me.objApproverFooter.BorderColor = System.Drawing.Color.Silver
        Me.objApproverFooter.Controls.Add(Me.btnOk)
        Me.objApproverFooter.Controls.Add(Me.btnCancel)
        Me.objApproverFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objApproverFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objApproverFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objApproverFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objApproverFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objApproverFooter.Location = New System.Drawing.Point(0, 333)
        Me.objApproverFooter.Name = "objApproverFooter"
        Me.objApproverFooter.Size = New System.Drawing.Size(445, 55)
        Me.objApproverFooter.TabIndex = 285
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(335, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(335, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        Me.btnCancel.Visible = False
        '
        'frmMapExpenses_Approver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(445, 388)
        Me.Controls.Add(Me.objApproverFooter)
        Me.Controls.Add(Me.objchkSelectAll)
        Me.Controls.Add(Me.dgvExpense)
        Me.Controls.Add(Me.gbLeavedetail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMapExpenses_Approver"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Expense Mapping"
        Me.gbLeavedetail.ResumeLayout(False)
        CType(Me.dgvExpense, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objApproverFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbLeavedetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblApprover As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objlblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvExpense As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUoM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpenseId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objApproverFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
End Class

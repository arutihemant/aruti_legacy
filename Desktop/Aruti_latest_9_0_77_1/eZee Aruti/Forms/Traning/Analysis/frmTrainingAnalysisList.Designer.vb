﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingAnalysisList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrainingAnalysisList))
        Me.pnlTrainingAnalysisList = New System.Windows.Forms.Panel
        Me.lvAnalysisList = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhCourse = New System.Windows.Forms.ColumnHeader
        Me.colhAnalysisDate = New System.Windows.Forms.ColumnHeader
        Me.colhTrainer = New System.Windows.Forms.ColumnHeader
        Me.colhResult = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhEnrollId = New System.Windows.Forms.ColumnHeader
        Me.objcolhAnalysisTranId = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchResult = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchTrainer = New eZee.Common.eZeeGradientButton
        Me.cboResult = New System.Windows.Forms.ComboBox
        Me.lblResult = New System.Windows.Forms.Label
        Me.cboTrainer = New System.Windows.Forms.ComboBox
        Me.lblTrainer = New System.Windows.Forms.Label
        Me.chkIncomplete = New System.Windows.Forms.CheckBox
        Me.objStLine2 = New eZee.Common.eZeeStraightLine
        Me.chkComplete = New System.Windows.Forms.CheckBox
        Me.objStLine3 = New eZee.Common.eZeeStraightLine
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker
        Me.lblCourseDateTo = New System.Windows.Forms.Label
        Me.lblAnalysisDateFrom = New System.Windows.Forms.Label
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.lblCourse = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnFinalEvaluation = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTrainingAnalysisList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTrainingAnalysisList
        '
        Me.pnlTrainingAnalysisList.Controls.Add(Me.lvAnalysisList)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlTrainingAnalysisList.Controls.Add(Me.eZeeHeader)
        Me.pnlTrainingAnalysisList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTrainingAnalysisList.Location = New System.Drawing.Point(0, 0)
        Me.pnlTrainingAnalysisList.Name = "pnlTrainingAnalysisList"
        Me.pnlTrainingAnalysisList.Size = New System.Drawing.Size(840, 450)
        Me.pnlTrainingAnalysisList.TabIndex = 0
        '
        'lvAnalysisList
        '
        Me.lvAnalysisList.BackColorOnChecked = True
        Me.lvAnalysisList.ColumnHeaders = Nothing
        Me.lvAnalysisList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhCourse, Me.colhAnalysisDate, Me.colhTrainer, Me.colhResult, Me.colhStatus, Me.colhRemark, Me.objcolhEnrollId, Me.objcolhAnalysisTranId})
        Me.lvAnalysisList.CompulsoryColumns = ""
        Me.lvAnalysisList.FullRowSelect = True
        Me.lvAnalysisList.GridLines = True
        Me.lvAnalysisList.GroupingColumn = Nothing
        Me.lvAnalysisList.HideSelection = False
        Me.lvAnalysisList.Location = New System.Drawing.Point(12, 159)
        Me.lvAnalysisList.MinColumnWidth = 50
        Me.lvAnalysisList.MultiSelect = False
        Me.lvAnalysisList.Name = "lvAnalysisList"
        Me.lvAnalysisList.OptionalColumns = ""
        Me.lvAnalysisList.ShowMoreItem = False
        Me.lvAnalysisList.ShowSaveItem = False
        Me.lvAnalysisList.ShowSelectAll = True
        Me.lvAnalysisList.ShowSizeAllColumnsToFit = True
        Me.lvAnalysisList.Size = New System.Drawing.Size(816, 230)
        Me.lvAnalysisList.Sortable = True
        Me.lvAnalysisList.TabIndex = 3
        Me.lvAnalysisList.UseCompatibleStateImageBehavior = False
        Me.lvAnalysisList.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 120
        '
        'colhCourse
        '
        Me.colhCourse.Tag = "colhCourse"
        Me.colhCourse.Text = "Course"
        Me.colhCourse.Width = 120
        '
        'colhAnalysisDate
        '
        Me.colhAnalysisDate.Tag = "colhAnalysisDate"
        Me.colhAnalysisDate.Text = "Analysis Date"
        Me.colhAnalysisDate.Width = 95
        '
        'colhTrainer
        '
        Me.colhTrainer.Tag = "colhTrainer"
        Me.colhTrainer.Text = "Trainer"
        Me.colhTrainer.Width = 115
        '
        'colhResult
        '
        Me.colhResult.Tag = "colhResult"
        Me.colhResult.Text = "Result"
        Me.colhResult.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 180
        '
        'objcolhEnrollId
        '
        Me.objcolhEnrollId.Tag = "objcolhEnrollId"
        Me.objcolhEnrollId.Text = "objcolhEnrollId"
        Me.objcolhEnrollId.Width = 0
        '
        'objcolhAnalysisTranId
        '
        Me.objcolhAnalysisTranId.Tag = "objcolhAnalysisTranId"
        Me.objcolhAnalysisTranId.Text = "objcolhAnalysisTranId"
        Me.objcolhAnalysisTranId.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchResult)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTrainer)
        Me.gbFilterCriteria.Controls.Add(Me.cboResult)
        Me.gbFilterCriteria.Controls.Add(Me.lblResult)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainer)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainer)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncomplete)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine2)
        Me.gbFilterCriteria.Controls.Add(Me.chkComplete)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine3)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblCourseDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAnalysisDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboCourse)
        Me.gbFilterCriteria.Controls.Add(Me.lblCourse)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 92
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(816, 88)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchResult
        '
        Me.objbtnSearchResult.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchResult.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchResult.BorderSelected = False
        Me.objbtnSearchResult.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchResult.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchResult.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchResult.Location = New System.Drawing.Point(459, 57)
        Me.objbtnSearchResult.Name = "objbtnSearchResult"
        Me.objbtnSearchResult.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchResult.TabIndex = 39
        '
        'objbtnSearchTrainer
        '
        Me.objbtnSearchTrainer.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainer.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainer.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrainer.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrainer.BorderSelected = False
        Me.objbtnSearchTrainer.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrainer.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrainer.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrainer.Location = New System.Drawing.Point(459, 30)
        Me.objbtnSearchTrainer.Name = "objbtnSearchTrainer"
        Me.objbtnSearchTrainer.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrainer.TabIndex = 38
        '
        'cboResult
        '
        Me.cboResult.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResult.FormattingEnabled = True
        Me.cboResult.Location = New System.Drawing.Point(321, 57)
        Me.cboResult.Name = "cboResult"
        Me.cboResult.Size = New System.Drawing.Size(132, 21)
        Me.cboResult.TabIndex = 37
        '
        'lblResult
        '
        Me.lblResult.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(255, 60)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(60, 15)
        Me.lblResult.TabIndex = 36
        Me.lblResult.Text = "Result"
        '
        'cboTrainer
        '
        Me.cboTrainer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainer.FormattingEnabled = True
        Me.cboTrainer.Location = New System.Drawing.Point(321, 30)
        Me.cboTrainer.Name = "cboTrainer"
        Me.cboTrainer.Size = New System.Drawing.Size(132, 21)
        Me.cboTrainer.TabIndex = 35
        '
        'lblTrainer
        '
        Me.lblTrainer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainer.Location = New System.Drawing.Point(255, 33)
        Me.lblTrainer.Name = "lblTrainer"
        Me.lblTrainer.Size = New System.Drawing.Size(60, 15)
        Me.lblTrainer.TabIndex = 34
        Me.lblTrainer.Text = "Trainer"
        '
        'chkIncomplete
        '
        Me.chkIncomplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncomplete.Location = New System.Drawing.Point(705, 62)
        Me.chkIncomplete.Name = "chkIncomplete"
        Me.chkIncomplete.Size = New System.Drawing.Size(95, 16)
        Me.chkIncomplete.TabIndex = 12
        Me.chkIncomplete.Text = "Incomplete"
        Me.chkIncomplete.UseVisualStyleBackColor = True
        '
        'objStLine2
        '
        Me.objStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objStLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.objStLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine2.Location = New System.Drawing.Point(688, 24)
        Me.objStLine2.Name = "objStLine2"
        Me.objStLine2.Size = New System.Drawing.Size(11, 63)
        Me.objStLine2.TabIndex = 10
        '
        'chkComplete
        '
        Me.chkComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkComplete.Location = New System.Drawing.Point(705, 35)
        Me.chkComplete.Name = "chkComplete"
        Me.chkComplete.Size = New System.Drawing.Size(95, 16)
        Me.chkComplete.TabIndex = 11
        Me.chkComplete.Text = "Complete"
        Me.chkComplete.UseVisualStyleBackColor = True
        '
        'objStLine3
        '
        Me.objStLine3.BackColor = System.Drawing.Color.Transparent
        Me.objStLine3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.objStLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine3.Location = New System.Drawing.Point(239, 24)
        Me.objStLine3.Name = "objStLine3"
        Me.objStLine3.Size = New System.Drawing.Size(10, 63)
        Me.objStLine3.TabIndex = 5
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(485, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(10, 63)
        Me.objStLine1.TabIndex = 5
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(790, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 32
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(767, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 31
        Me.objbtnSearch.TabStop = False
        '
        'dtpDateTo
        '
        Me.dtpDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateTo.Checked = False
        Me.dtpDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateTo.Location = New System.Drawing.Point(586, 60)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.ShowCheckBox = True
        Me.dtpDateTo.Size = New System.Drawing.Size(96, 21)
        Me.dtpDateTo.TabIndex = 9
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Checked = False
        Me.dtpDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDateFrom.Location = New System.Drawing.Point(586, 33)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.ShowCheckBox = True
        Me.dtpDateFrom.Size = New System.Drawing.Size(96, 21)
        Me.dtpDateFrom.TabIndex = 7
        '
        'lblCourseDateTo
        '
        Me.lblCourseDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourseDateTo.Location = New System.Drawing.Point(501, 63)
        Me.lblCourseDateTo.Name = "lblCourseDateTo"
        Me.lblCourseDateTo.Size = New System.Drawing.Size(79, 15)
        Me.lblCourseDateTo.TabIndex = 8
        Me.lblCourseDateTo.Text = "To"
        '
        'lblAnalysisDateFrom
        '
        Me.lblAnalysisDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnalysisDateFrom.Location = New System.Drawing.Point(501, 36)
        Me.lblAnalysisDateFrom.Name = "lblAnalysisDateFrom"
        Me.lblAnalysisDateFrom.Size = New System.Drawing.Size(79, 15)
        Me.lblAnalysisDateFrom.TabIndex = 6
        Me.lblAnalysisDateFrom.Text = "Analysis Date"
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(74, 60)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(132, 21)
        Me.cboCourse.TabIndex = 4
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(8, 63)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(60, 15)
        Me.lblCourse.TabIndex = 3
        Me.lblCourse.Text = "Course"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(212, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(74, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(132, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(60, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(840, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Training Analysis List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnFinalEvaluation)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 395)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(840, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnFinalEvaluation
        '
        Me.btnFinalEvaluation.BackColor = System.Drawing.Color.White
        Me.btnFinalEvaluation.BackgroundImage = CType(resources.GetObject("btnFinalEvaluation.BackgroundImage"), System.Drawing.Image)
        Me.btnFinalEvaluation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFinalEvaluation.BorderColor = System.Drawing.Color.Empty
        Me.btnFinalEvaluation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFinalEvaluation.FlatAppearance.BorderSize = 0
        Me.btnFinalEvaluation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFinalEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFinalEvaluation.ForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFinalEvaluation.GradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalEvaluation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.Location = New System.Drawing.Point(12, 13)
        Me.btnFinalEvaluation.Name = "btnFinalEvaluation"
        Me.btnFinalEvaluation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFinalEvaluation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFinalEvaluation.Size = New System.Drawing.Size(106, 30)
        Me.btnFinalEvaluation.TabIndex = 158
        Me.btnFinalEvaluation.Text = "&Final Analysis"
        Me.btnFinalEvaluation.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(731, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 157
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(628, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 156
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(525, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 155
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'frmTrainingAnalysisList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(840, 450)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlTrainingAnalysisList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTrainingAnalysisList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Training Analysis"
        Me.pnlTrainingAnalysisList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTrainingAnalysisList As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCourseDateTo As System.Windows.Forms.Label
    Friend WithEvents lblAnalysisDateFrom As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnFinalEvaluation As eZee.Common.eZeeLightButton
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents chkIncomplete As System.Windows.Forms.CheckBox
    Friend WithEvents chkComplete As System.Windows.Forms.CheckBox
    Friend WithEvents lvAnalysisList As eZee.Common.eZeeListView
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCourse As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAnalysisDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTrainer As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhResult As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchResult As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTrainer As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResult As System.Windows.Forms.ComboBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents cboTrainer As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainer As System.Windows.Forms.Label
    Friend WithEvents objStLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents objcolhEnrollId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAnalysisTranId As System.Windows.Forms.ColumnHeader
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFeedbackSubItemList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFeedbackSubItemList))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lvSubItemList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhFeedbackItem = New System.Windows.Forms.ColumnHeader
        Me.colhSubItemCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnSearchFeedbackGrp = New eZee.Common.eZeeGradientButton
        Me.cboFeedbackGroup = New System.Windows.Forms.ComboBox
        Me.lblFeedbackGroup = New System.Windows.Forms.Label
        Me.objbtnSearchItems = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboFeedbackItem = New System.Windows.Forms.ComboBox
        Me.lblFeedbackItem = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.Panel1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lvSubItemList)
        Me.Panel1.Controls.Add(Me.gbFilterCriteria)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(685, 399)
        Me.Panel1.TabIndex = 0
        '
        'lvSubItemList
        '
        Me.lvSubItemList.BackColorOnChecked = False
        Me.lvSubItemList.ColumnHeaders = Nothing
        Me.lvSubItemList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhFeedbackItem, Me.colhSubItemCode, Me.colhName, Me.colhDescription})
        Me.lvSubItemList.CompulsoryColumns = ""
        Me.lvSubItemList.FullRowSelect = True
        Me.lvSubItemList.GridLines = True
        Me.lvSubItemList.GroupingColumn = Nothing
        Me.lvSubItemList.HideSelection = False
        Me.lvSubItemList.Location = New System.Drawing.Point(10, 134)
        Me.lvSubItemList.MinColumnWidth = 50
        Me.lvSubItemList.MultiSelect = False
        Me.lvSubItemList.Name = "lvSubItemList"
        Me.lvSubItemList.OptionalColumns = ""
        Me.lvSubItemList.ShowMoreItem = False
        Me.lvSubItemList.ShowSaveItem = False
        Me.lvSubItemList.ShowSelectAll = True
        Me.lvSubItemList.ShowSizeAllColumnsToFit = True
        Me.lvSubItemList.Size = New System.Drawing.Size(663, 205)
        Me.lvSubItemList.Sortable = True
        Me.lvSubItemList.TabIndex = 6
        Me.lvSubItemList.UseCompatibleStateImageBehavior = False
        Me.lvSubItemList.View = System.Windows.Forms.View.Details
        '
        'objcolhFeedbackItem
        '
        Me.objcolhFeedbackItem.Tag = "objcolhFeedbackItem"
        Me.objcolhFeedbackItem.Text = ""
        Me.objcolhFeedbackItem.Width = 0
        '
        'colhSubItemCode
        '
        Me.colhSubItemCode.Tag = "colhSubItemCode"
        Me.colhSubItemCode.Text = "Item Code"
        Me.colhSubItemCode.Width = 105
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 354
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 200
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.btnSearchFeedbackGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboFeedbackGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblFeedbackGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchItems)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboFeedbackItem)
        Me.gbFilterCriteria.Controls.Add(Me.lblFeedbackItem)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(10, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(663, 64)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSearchFeedbackGrp
        '
        Me.btnSearchFeedbackGrp.BackColor = System.Drawing.Color.Transparent
        Me.btnSearchFeedbackGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearchFeedbackGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearchFeedbackGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearchFeedbackGrp.BorderSelected = False
        Me.btnSearchFeedbackGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearchFeedbackGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.btnSearchFeedbackGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearchFeedbackGrp.Location = New System.Drawing.Point(305, 33)
        Me.btnSearchFeedbackGrp.Name = "btnSearchFeedbackGrp"
        Me.btnSearchFeedbackGrp.Size = New System.Drawing.Size(21, 21)
        Me.btnSearchFeedbackGrp.TabIndex = 220
        '
        'cboFeedbackGroup
        '
        Me.cboFeedbackGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackGroup.DropDownWidth = 350
        Me.cboFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackGroup.FormattingEnabled = True
        Me.cboFeedbackGroup.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboFeedbackGroup.Location = New System.Drawing.Point(120, 33)
        Me.cboFeedbackGroup.Name = "cboFeedbackGroup"
        Me.cboFeedbackGroup.Size = New System.Drawing.Size(179, 21)
        Me.cboFeedbackGroup.TabIndex = 218
        '
        'lblFeedbackGroup
        '
        Me.lblFeedbackGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFeedbackGroup.Location = New System.Drawing.Point(8, 36)
        Me.lblFeedbackGroup.Name = "lblFeedbackGroup"
        Me.lblFeedbackGroup.Size = New System.Drawing.Size(106, 15)
        Me.lblFeedbackGroup.TabIndex = 219
        Me.lblFeedbackGroup.Text = "Feedback Group"
        Me.lblFeedbackGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchItems
        '
        Me.objbtnSearchItems.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchItems.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchItems.BorderSelected = False
        Me.objbtnSearchItems.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchItems.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchItems.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchItems.Location = New System.Drawing.Point(632, 32)
        Me.objbtnSearchItems.Name = "objbtnSearchItems"
        Me.objbtnSearchItems.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchItems.TabIndex = 216
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(636, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(613, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'cboFeedbackItem
        '
        Me.cboFeedbackItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFeedbackItem.DropDownWidth = 350
        Me.cboFeedbackItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFeedbackItem.FormattingEnabled = True
        Me.cboFeedbackItem.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboFeedbackItem.Location = New System.Drawing.Point(447, 32)
        Me.cboFeedbackItem.Name = "cboFeedbackItem"
        Me.cboFeedbackItem.Size = New System.Drawing.Size(179, 21)
        Me.cboFeedbackItem.TabIndex = 1
        '
        'lblFeedbackItem
        '
        Me.lblFeedbackItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFeedbackItem.Location = New System.Drawing.Point(344, 35)
        Me.lblFeedbackItem.Name = "lblFeedbackItem"
        Me.lblFeedbackItem.Size = New System.Drawing.Size(97, 15)
        Me.lblFeedbackItem.TabIndex = 8
        Me.lblFeedbackItem.Text = "Feedback Item"
        Me.lblFeedbackItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 344)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(685, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(473, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(370, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 5
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(267, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 4
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(576, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(685, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Feedback Sub Items List"
        '
        'frmFeedbackSubItemList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 399)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFeedbackSubItemList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Feedback SubItem List"
        Me.Panel1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSearchFeedbackGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFeedbackGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblFeedbackGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchItems As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboFeedbackItem As System.Windows.Forms.ComboBox
    Friend WithEvents lblFeedbackItem As System.Windows.Forms.Label
    Friend WithEvents lvSubItemList As eZee.Common.eZeeListView
    Friend WithEvents objcolhFeedbackItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSubItemCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
End Class

﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeFeedback

#Region " Private Variables "

    Private mstrModuleName As String = "frmEmployeeFeedback"
    Private mblnCancel As Boolean = True
    Private objFMaster As clshrtnafeedback_master
    Private objFTran As clshrtnafeedback_tran
    Private mdtFeedback As DataTable
    Private menAction As enAction
    Private mintFeedbackMasterUnkid As Integer = -1
    Private dsItems As New DataSet

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintFeedbackMasterUnkid = intUnkid
            menAction = eAction
            Me.ShowDialog()
            intUnkid = mintFeedbackMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objTSchedule As New clsTraining_Scheduling
        Dim objFdbkGrp As New clshrtnafdbk_group_master
        Dim dsCombo As New DataSet
        Try
            dsCombo = objTSchedule.getComboList("List", True, FinancialYear._Object._YearUnkid, True)
            With cboCourseTitle
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objFdbkGrp.getComboList("List", True)
            With cboFeedbackGroup
                .ValueMember = "fdbkgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objFdbkGrp = Nothing : objTSchedule = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCourseTitle.SelectedValue = objFMaster._Trainingschduleunkid
            cboEmployee.SelectedValue = objFMaster._Employeeunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            Dim dTemp() As DataRow = CType(cboCourseTitle.DataSource, DataTable).Select("Id ='" & CInt(cboCourseTitle.SelectedValue) & "'")
            If dTemp.Length > 0 Then
                objFMaster._Courseunkid = CInt(dTemp(0)("courseunkid"))
            End If
            objFMaster._Feedbackdate = ConfigParameter._Object._CurrentDateAndTime
            objFMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objFMaster._Isvoid = False
            objFMaster._Trainingschduleunkid = CInt(cboCourseTitle.SelectedValue)
            objFMaster._Userunkid = User._Object._Userunkid
            objFMaster._Voiddatetime = Nothing
            objFMaster._Voidreason = ""
            objFMaster._Voiduserunkid = -1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillFeedbackList()
        Try
            lvFeedback.Items.Clear()
            Dim lvItem As ListViewItem
            Dim dView As DataView = mdtFeedback.DefaultView
            dView.Sort = "feedback_group"
            mdtFeedback = dView.ToTable
            For Each dtRow As DataRow In mdtFeedback.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("feedback_item").ToString
                    lvItem.SubItems.Add(dtRow.Item("feedback_subitem").ToString)
                    lvItem.SubItems.Add(dtRow.Item("feedback_result").ToString)
                    lvItem.SubItems.Add(dtRow.Item("other_result").ToString)
                    lvItem.SubItems.Add(dtRow.Item("feedback_group").ToString)
                    lvItem.SubItems.Add(dtRow.Item("fdbkgroupunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("fdbkitemunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("fdbksubitemunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("resultunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("feedbacktranunkid")

                    lvFeedback.Items.Add(lvItem)
                End If
            Next

            If lvFeedback.Items.Count > 2 Then
                colhTextResult.Width = 208 - 20
            Else
                colhTextResult.Width = 208
            End If

            lvFeedback.GroupingColumn = objcolhFdbkGrp
            lvFeedback.DisplayGroups(True)

            lvFeedback.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetCombos()
        Try
            cboResult.SelectedValue = 0
            cboFdbkSubItems.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetCombos", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboCourseTitle.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Course Title is mandatory information. Please select Course Title to continue."), enMsgBoxStyle.Information)
                cboCourseTitle.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboFeedbackGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Feedback Group is mandatory information. Please select Feedback Group to continue."), enMsgBoxStyle.Information)
                cboFeedbackGroup.Focus()
                Return False
            End If

            If CInt(cboFeedbackItem.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Feedback Item is mandatory information. Please select Feedback Item to continue."), enMsgBoxStyle.Information)
                cboFeedbackItem.Focus()
                Return False
            End If

            Dim dtTemp() As DataRow = dsItems.Tables(0).Select("Id = '" & CInt(cboFeedbackItem.SelectedValue) & "'")
            If dtTemp.Length > 0 Then
                If CInt(dtTemp(0).Item("Cnt")) > 0 AndAlso CInt(cboFdbkSubItems.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Feedback Sub Item compulsory information.Please Select Feedback Sub Item."), enMsgBoxStyle.Information)
                    cboFdbkSubItems.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Is_AllFeedback(Optional ByVal blnFlag As Boolean = False) As Boolean
        Try
            Dim dView As DataView = mdtFeedback.DefaultView
            Dim intCount As Integer = objFMaster.GetItems_BasedGroup()

            If intCount <> dView.ToTable(True, "fdbkitemunkid").Rows.Count Then
                If blnFlag = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback for all items."), enMsgBoxStyle.Information)
                End If
                cboFeedbackGroup.Focus()
                Return False
            End If

            dsItems = objFMaster.GetSubItemCount()

            If dsItems.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsItems.Tables(0).Rows
                    Dim dtTemp() As DataRow = mdtFeedback.Select("fdbkitemunkid = '" & CInt(dRow.Item("Id")) & "'")
                    If dtTemp.Length > 0 AndAlso CInt(dtTemp(0)("fdbksubitemunkid")) <> 0 Then
                        If dtTemp.Length <> CInt(dRow.Item("Cnt")) Then
                            If blnFlag = True Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback for all subitems in some item(s)."), enMsgBoxStyle.Information)
                                cboFdbkSubItems.Focus()
                            End If
                            Return False
                        End If
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_AllFeedback", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeFeedback_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFMaster = New clshrtnafeedback_master
        objFTran = New clshrtnafeedback_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call FillCombo()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            btnSaveComplete.Enabled = User._Object.Privilege._AllowToSave_CompleteLevelIEvaluation
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            If menAction = enAction.EDIT_ONE Then
                objFMaster._Feedbackmasterunkid = mintFeedbackMasterUnkid
                cboCourseTitle.Enabled = False : objbtnSearchCourseTitle.Enabled = False
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If

            Call GetValue()

            objFTran._Feedbackmasterunkid = mintFeedbackMasterUnkid
            mdtFeedback = objFTran._DataTable

            Call FillFeedbackList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmEmployeeFeedback_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeFeedback_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeFeedback_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeFeedback_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeFeedback_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objFMaster = Nothing : objFTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnafeedback_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnafeedback_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAddFeedback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFeedback.Click
        Try
            If IsValid() = False Then Exit Sub

            If objFTran.IsExists(CInt(cboCourseTitle.SelectedValue), _
                                 CInt(cboEmployee.SelectedValue), _
                                 CInt(cboFeedbackGroup.SelectedValue), _
                                 CInt(cboFeedbackItem.SelectedValue), _
                                 CInt(cboFdbkSubItems.SelectedValue)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Feedback is already present for selected selection."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboFdbkSubItems.SelectedValue) > 0 Then
                Dim dtRow As DataRow() = mdtFeedback.Select("fdbksubitemunkid = '" & CInt(cboFdbkSubItems.SelectedValue) & "' AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                Dim dtRow As DataRow() = mdtFeedback.Select("fdbkitemunkid = '" & CInt(cboFeedbackItem.SelectedValue) & "' AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim dRow As DataRow = mdtFeedback.NewRow

            dRow.Item("feedbacktranunkid") = -1
            dRow.Item("feedbackmasterunkid") = mintFeedbackMasterUnkid
            dRow.Item("fdbkgroupunkid") = cboFeedbackGroup.SelectedValue
            dRow.Item("fdbkitemunkid") = cboFeedbackItem.SelectedValue
            dRow.Item("fdbksubitemunkid") = cboFdbkSubItems.SelectedValue
            dRow.Item("resultunkid") = cboResult.SelectedValue
            dRow.Item("other_result") = txtTextResult.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("feedback_group") = cboFeedbackGroup.Text
            dRow.Item("feedback_item") = cboFeedbackItem.Text
            If CInt(cboFdbkSubItems.SelectedValue) > 0 Then
                dRow.Item("feedback_subitem") = cboFdbkSubItems.Text
            Else
                dRow.Item("feedback_subitem") = ""
            End If
            If CInt(cboResult.SelectedValue) > 0 Then
                dRow.Item("feedback_result") = cboResult.Text
            Else
                dRow.Item("feedback_result") = ""
            End If
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtFeedback.Rows.Add(dRow)

            Call FillFeedbackList()
            Call ResetCombos()

            cboCourseTitle.Enabled = False : cboEmployee.Enabled = False
            objbtnSearchCourseTitle.Enabled = False : objbtnSearchEmployee.Enabled = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddFeedback_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditFeedback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditFeedback.Click
        Try
            If lvFeedback.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvFeedback.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtFeedback.Select("GUID = '" & lvFeedback.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "'")
                Else
                    drTemp = mdtFeedback.Select("feedbacktranunkid = '" & CInt(lvFeedback.SelectedItems(0).Tag) & "'")
                End If

                Dim dtTemp() As DataRow = Nothing
                If CInt(cboFdbkSubItems.SelectedValue) > 0 Then
                    dtTemp = mdtFeedback.Select("fdbksubitemunkid = '" & CInt(cboFdbkSubItems.SelectedValue) & "'AND GUID <> '" & lvFeedback.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "' AND AUD <> 'D' ")
                Else
                    dtTemp = mdtFeedback.Select("fdbkitemunkid = '" & CInt(cboFeedbackItem.SelectedValue) & "'AND GUID <> '" & lvFeedback.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "' AND AUD <> 'D' ")
                End If

                If dtTemp.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If IsValid() = False Then Exit Sub

                With drTemp(0)
                    .Item("feedbacktranunkid") = lvFeedback.SelectedItems(0).Tag
                    .Item("feedbackmasterunkid") = mintFeedbackMasterUnkid
                    .Item("fdbkgroupunkid") = cboFeedbackGroup.SelectedValue
                    .Item("fdbkitemunkid") = cboFeedbackItem.SelectedValue
                    .Item("fdbksubitemunkid") = cboFdbkSubItems.SelectedValue
                    .Item("resultunkid") = cboResult.SelectedValue
                    .Item("other_result") = txtTextResult.Text
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("feedback_group") = cboFeedbackGroup.Text
                    .Item("feedback_item") = cboFeedbackItem.Text
                    If CInt(cboFdbkSubItems.SelectedValue) > 0 Then
                        .Item("feedback_subitem") = cboFdbkSubItems.Text
                    Else
                        .Item("feedback_subitem") = ""
                    End If
                    If CInt(cboResult.SelectedValue) > 0 Then
                        .Item("feedback_result") = cboResult.Text
                    Else
                        .Item("feedback_result") = ""
                    End If
                    If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                        .Item("AUD") = "U"
                    End If
                    .Item("GUID") = Guid.NewGuid.ToString
                    .AcceptChanges()
                End With
                Call FillFeedbackList()
            End If
            Call ResetCombos()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditFeedback_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteFeedback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteFeedback.Click
        Try
            If lvFeedback.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvFeedback.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtFeedback.Select("GUID = '" & lvFeedback.SelectedItems(0).SubItems(objcolhGuid.Index).Text & "'")
                Else
                    drTemp = mdtFeedback.Select("feedbacktranunkid = '" & CInt(lvFeedback.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    If CInt(lvFeedback.SelectedItems(0).Tag) > 0 Then
                        Dim frm As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    Else
                        drTemp(0).Item("AUD") = "D"
                    End If
                    Call FillFeedbackList()
                End If
                Call ResetCombos()
            End If
            If lvFeedback.Items.Count <= 0 Then
                cboCourseTitle.Enabled = True : cboEmployee.Enabled = True
                objbtnSearchCourseTitle.Enabled = True : objbtnSearchEmployee.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteFeedback_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveComplete.Click
        Dim blnFlag As Boolean = False
        Try
            If lvFeedback.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please add atleast one evaluation in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSAVECOMPLETE"
                    If Is_AllFeedback(False) = False Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "There are some Feedback Items which answers are still not given.If you Press Yes,you will Complete this Feedback.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            objFMaster._Iscomplete = False
                        Else
                            objFMaster._Iscomplete = True
                        End If
                    Else
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to Finally save this feedback.After this you won't be allowed to EDIT this feedback.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            objFMaster._Iscomplete = False
                        Else
                            objFMaster._Iscomplete = True
                        End If
                    End If

            End Select

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objFMaster._FormName = mstrModuleName
            objFMaster._LoginEmployeeunkid = 0
            objFMaster._ClientIP = getIP()
            objFMaster._HostName = getHostName()
            objFMaster._FromWeb = False
            objFMaster._AuditUserId = User._Object._Userunkid
objFMaster._CompanyUnkid = Company._Object._Companyunkid
            objFMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objFMaster.Update(mdtFeedback)
            Else
                blnFlag = objFMaster.Insert(mdtFeedback)
            End If

            'S.SANDEEP [06-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Training Module Notification
            If CType(sender, eZee.Common.eZeeLightButton).Name = btnSaveComplete.Name Then
                If ConfigParameter._Object._Ntf_TrainingLevelI_EvalUserIds.Trim.Length > 0 Then
                    If blnFlag Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enLogin_Mode.DESKTOP, User._Object._Userunkid, ConfigParameter._Object._Ntf_TrainingLevelI_EvalUserIds, 0, mstrModuleName)
                        objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enLogin_Mode.DESKTOP, User._Object._Userunkid, ConfigParameter._Object._Ntf_TrainingLevelI_EvalUserIds, 0, mstrModuleName, Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    End If
                End If
            End If
            'S.SANDEEP [06-MAR-2017] -- END

            If blnFlag = False And objFMaster._Message <> "" Then
                eZeeMsgBox.Show(objFMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboCourseTitle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCourseTitle.SelectedIndexChanged
        Try
            If CInt(cboCourseTitle.SelectedValue) > 0 Then
                Dim objTEnroll As New clsTraining_Enrollment_Tran
                Dim dsList As New DataSet
                dsList = objTEnroll.Get_EnrolledEmp(CInt(cboCourseTitle.SelectedValue))
                With cboEmployee
                    .ValueMember = "employeeunkid"
                    .DisplayMember = "employeename"
                    .DataSource = dsList.Tables(0)
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCourseTitle_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFeedbackGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackGroup.SelectedIndexChanged
        Try
            If CInt(cboFeedbackGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objfdbkItems As New clshrtnafdbk_item_master
                dsList = objfdbkItems.getComboList("List", True, CInt(cboFeedbackGroup.SelectedValue))
                With cboFeedbackItem
                    .ValueMember = "fdbkitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                End With
                dsItems = objFMaster.GetSubItemCount(CInt(cboFeedbackGroup.SelectedValue))
            Else
                cboFeedbackItem.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFeedbackGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFeedbackItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackItem.SelectedIndexChanged
        Try
            If CInt(cboFeedbackItem.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objfdbkItem As New clshrtnafdbk_item_master
                Dim objfdbkSItem As New clshrtnafdbk_subitem_master
                Dim objResult As New clsresult_master
                objfdbkItem._Fdbkitemunkid = CInt(cboFeedbackItem.SelectedValue)

                dsList = objfdbkSItem.getComboList("List", True, CInt(cboFeedbackItem.SelectedValue))
                With cboFdbkSubItems
                    .ValueMember = "fdbksubitemunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With

                dsList = objResult.getComboList("List", True, objfdbkItem._Resultgroupunkid)
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With

                If objfdbkItem._Resultgroupunkid > 0 Then
                    txtTextResult.Text = ""
                    txtTextResult.Enabled = False
                    cboResult.SelectedValue = 0
                    cboResult.Enabled = True
                    objbtnSearchResult.Enabled = True
                Else
                    txtTextResult.Text = ""
                    txtTextResult.Enabled = True
                    cboResult.SelectedValue = 0
                    cboResult.Enabled = False
                    objbtnSearchResult.Enabled = False
                End If

                objfdbkItem = Nothing : objfdbkSItem = Nothing : objResult = Nothing
            Else
                txtTextResult.Text = ""
                txtTextResult.Enabled = True
                cboResult.DataSource = Nothing : cboFdbkSubItems.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFeedbackItem_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCourseTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourseTitle.Click, objbtnSearchEmployee.Click, _
                                                                                                                  objbtnSearchFeedbackGroup.Click, objbtnSearchItems.Click, _
                                                                                                                  objbtnSearchFdbkSubItems.Click, objbtnSearchResult.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .DataSource = Nothing
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHCOURSETITLE"
                        .ValueMember = cboCourseTitle.ValueMember
                        .DisplayMember = cboCourseTitle.DisplayMember
                        .DataSource = CType(cboCourseTitle.DataSource, DataTable)
                    Case "OBJBTNSEARCHEMPLOYEE"
                        If cboEmployee.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboEmployee.ValueMember
                        .DisplayMember = cboEmployee.DisplayMember
                        .DataSource = CType(cboEmployee.DataSource, DataTable)
                    Case "OBJBTNSEARCHFEEDBACKGROUP"
                        .ValueMember = cboFeedbackGroup.ValueMember
                        .DisplayMember = cboFeedbackGroup.DisplayMember
                        .DataSource = CType(cboFeedbackGroup.DataSource, DataTable)
                    Case "OBJBTNSEARCHITEMS"
                        If cboFeedbackItem.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboFeedbackItem.ValueMember
                        .DisplayMember = cboFeedbackItem.DisplayMember
                        .DataSource = CType(cboFeedbackItem.DataSource, DataTable)
                    Case "OBJBTNSEARCHFDBKSUBITEMS"
                        If cboFdbkSubItems.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboFdbkSubItems.ValueMember
                        .DisplayMember = cboFdbkSubItems.DisplayMember
                        .DataSource = CType(cboFdbkSubItems.DataSource, DataTable)
                    Case "OBJBTNSEARCHRESULT"
                        If cboResult.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboResult.ValueMember
                        .DisplayMember = cboResult.DisplayMember
                        .DataSource = CType(cboResult.DataSource, DataTable)
                End Select
                If .DisplayDialog Then
                    Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                        Case "OBJBTNSEARCHCOURSETITLE"
                            cboCourseTitle.SelectedValue = .SelectedValue
                        Case "OBJBTNSEARCHEMPLOYEE"
                            cboEmployee.SelectedValue = .SelectedValue
                        Case "OBJBTNSEARCHFEEDBACKGROUP"
                            cboFeedbackGroup.SelectedValue = .SelectedValue
                        Case "OBJBTNSEARCHITEMS"
                            cboFeedbackItem.SelectedValue = .SelectedValue
                        Case "OBJBTNSEARCHFDBKSUBITEMS"
                            cboFdbkSubItems.SelectedValue = .SelectedValue
                        Case "OBJBTNSEARCHRESULT"
                            cboResult.SelectedValue = .SelectedValue
                    End Select
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourseTitle_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvFeedback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvFeedback.Click
        Try
            If lvFeedback.SelectedItems.Count > 0 Then
                cboFeedbackGroup.SelectedValue = 0
                cboFeedbackGroup.SelectedValue = CInt(lvFeedback.SelectedItems(0).SubItems(objcolhFdbkGrpId.Index).Text)
                cboFeedbackItem.SelectedValue = CInt(lvFeedback.SelectedItems(0).SubItems(objcolhFdbkItemId.Index).Text)
                cboFdbkSubItems.SelectedValue = CInt(lvFeedback.SelectedItems(0).SubItems(objcolhFdbkSubItemId.Index).Text)
                cboResult.SelectedValue = CInt(lvFeedback.SelectedItems(0).SubItems(objcolhResultId.Index).Text)
                txtTextResult.Text = lvFeedback.SelectedItems(0).SubItems(colhTextResult.Index).Text
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFeedback_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFeedbackInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFeedbackInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteFeedback.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteFeedback.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditFeedback.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditFeedback.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddFeedback.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddFeedback.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveComplete.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveComplete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
            Me.lblFeedbackGroup.Text = Language._Object.getCaption(Me.lblFeedbackGroup.Name, Me.lblFeedbackGroup.Text)
            Me.lblFeedbackItems.Text = Language._Object.getCaption(Me.lblFeedbackItems.Name, Me.lblFeedbackItems.Text)
            Me.lblFeedbackSubItems.Text = Language._Object.getCaption(Me.lblFeedbackSubItems.Name, Me.lblFeedbackSubItems.Text)
            Me.lblCourseTitle.Text = Language._Object.getCaption(Me.lblCourseTitle.Name, Me.lblCourseTitle.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.gbFeedbackInformation.Text = Language._Object.getCaption(Me.gbFeedbackInformation.Name, Me.gbFeedbackInformation.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnDeleteFeedback.Text = Language._Object.getCaption(Me.btnDeleteFeedback.Name, Me.btnDeleteFeedback.Text)
            Me.btnEditFeedback.Text = Language._Object.getCaption(Me.btnEditFeedback.Name, Me.btnEditFeedback.Text)
            Me.btnAddFeedback.Text = Language._Object.getCaption(Me.btnAddFeedback.Name, Me.btnAddFeedback.Text)
            Me.colhFeedbackItem.Text = Language._Object.getCaption(CStr(Me.colhFeedbackItem.Tag), Me.colhFeedbackItem.Text)
            Me.colhFeedbackSubItems.Text = Language._Object.getCaption(CStr(Me.colhFeedbackSubItems.Tag), Me.colhFeedbackSubItems.Text)
            Me.colhResult.Text = Language._Object.getCaption(CStr(Me.colhResult.Tag), Me.colhResult.Text)
            Me.colhTextResult.Text = Language._Object.getCaption(CStr(Me.colhTextResult.Tag), Me.colhTextResult.Text)
            Me.btnSaveComplete.Text = Language._Object.getCaption(Me.btnSaveComplete.Name, Me.btnSaveComplete.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Feedback is already present for selected selection.")
            Language.setMessage(mstrModuleName, 2, "Course Title is mandatory information. Please select Course Title to continue.")
            Language.setMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 4, "Feedback Group is mandatory information. Please select Feedback Group to continue.")
            Language.setMessage(mstrModuleName, 5, "Feedback Item is mandatory information. Please select Feedback Item to continue.")
            Language.setMessage(mstrModuleName, 6, "Feedback Sub Item compulsory information.Please Select Feedback Sub Item.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot add same item again in the list.")
            Language.setMessage(mstrModuleName, 9, "Please add atleast one evaluation in order to save.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback for all items.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback for all subitems in some item(s).")
            Language.setMessage(mstrModuleName, 12, "There are some Feedback Items which answers are still not given.If you Press Yes,you will Complete this Feedback.Do you want to continue?")
            Language.setMessage(mstrModuleName, 13, "Are you sure you want to Finally save this feedback.After this you won't be allowed to EDIT this feedback.Do you want to continue?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewEnrollCancellation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewEnrollCancellation))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEnroll = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmployeeInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlEmpList = New System.Windows.Forms.Panel
        Me.lvEnrollEmployee = New eZee.Common.eZeeListView(Me.components)
        Me.colhECode = New System.Windows.Forms.ColumnHeader
        Me.colhEName = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsTnA = New System.Windows.Forms.ColumnHeader
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtJob = New eZee.TextBox.AlphanumericTextBox
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.gbCourseDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchCourse = New eZee.Common.eZeeGradientButton
        Me.lblEnrollmentRemark = New System.Windows.Forms.Label
        Me.txtEnrollRemark = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.chkTnAEnrollment = New System.Windows.Forms.CheckBox
        Me.lblTrainingYear = New System.Windows.Forms.Label
        Me.cboTrainingYears = New System.Windows.Forms.ComboBox
        Me.txtEligibility = New eZee.TextBox.AlphanumericTextBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.dtpEnrolldate = New System.Windows.Forms.DateTimePicker
        Me.lblEnrollmentDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.lblCourse = New System.Windows.Forms.Label
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboCourse = New System.Windows.Forms.ComboBox
        Me.lblEligibility = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblNoOfPositons = New System.Windows.Forms.Label
        Me.txtFilledPosition = New eZee.TextBox.AlphanumericTextBox
        Me.txtTotalPosition = New eZee.TextBox.AlphanumericTextBox
        Me.lblFilledPositions = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeInformation.SuspendLayout()
        Me.pnlEmpList.SuspendLayout()
        Me.gbCourseDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbEmployeeInformation)
        Me.pnlMain.Controls.Add(Me.gbCourseDetails)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(809, 447)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEnroll)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 392)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(809, 55)
        Me.objFooter.TabIndex = 6
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(701, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 124
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEnroll
        '
        Me.btnEnroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEnroll.BackColor = System.Drawing.Color.White
        Me.btnEnroll.BackgroundImage = CType(resources.GetObject("btnEnroll.BackgroundImage"), System.Drawing.Image)
        Me.btnEnroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEnroll.BorderColor = System.Drawing.Color.Empty
        Me.btnEnroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEnroll.FlatAppearance.BorderSize = 0
        Me.btnEnroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnroll.ForeColor = System.Drawing.Color.Black
        Me.btnEnroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEnroll.GradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnroll.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.Location = New System.Drawing.Point(598, 13)
        Me.btnEnroll.Name = "btnEnroll"
        Me.btnEnroll.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnroll.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEnroll.Size = New System.Drawing.Size(97, 30)
        Me.btnEnroll.TabIndex = 123
        Me.btnEnroll.Text = "&Save"
        Me.btnEnroll.UseVisualStyleBackColor = True
        '
        'gbEmployeeInformation
        '
        Me.gbEmployeeInformation.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInformation.Checked = False
        Me.gbEmployeeInformation.CollapseAllExceptThis = False
        Me.gbEmployeeInformation.CollapsedHoverImage = Nothing
        Me.gbEmployeeInformation.CollapsedNormalImage = Nothing
        Me.gbEmployeeInformation.CollapsedPressedImage = Nothing
        Me.gbEmployeeInformation.CollapseOnLoad = False
        Me.gbEmployeeInformation.Controls.Add(Me.pnlEmpList)
        Me.gbEmployeeInformation.Controls.Add(Me.btnAdd)
        Me.gbEmployeeInformation.Controls.Add(Me.btnDelete)
        Me.gbEmployeeInformation.Controls.Add(Me.txtJob)
        Me.gbEmployeeInformation.Controls.Add(Me.txtDepartment)
        Me.gbEmployeeInformation.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeInformation.Controls.Add(Me.lblJob)
        Me.gbEmployeeInformation.Controls.Add(Me.lblDepartment)
        Me.gbEmployeeInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeInformation.Controls.Add(Me.lblEmployeeName)
        Me.gbEmployeeInformation.ExpandedHoverImage = Nothing
        Me.gbEmployeeInformation.ExpandedNormalImage = Nothing
        Me.gbEmployeeInformation.ExpandedPressedImage = Nothing
        Me.gbEmployeeInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInformation.HeaderHeight = 25
        Me.gbEmployeeInformation.HeaderMessage = ""
        Me.gbEmployeeInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInformation.HeightOnCollapse = 0
        Me.gbEmployeeInformation.LeftTextSpace = 0
        Me.gbEmployeeInformation.Location = New System.Drawing.Point(376, 3)
        Me.gbEmployeeInformation.Name = "gbEmployeeInformation"
        Me.gbEmployeeInformation.OpenHeight = 176
        Me.gbEmployeeInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInformation.ShowBorder = True
        Me.gbEmployeeInformation.ShowCheckBox = False
        Me.gbEmployeeInformation.ShowCollapseButton = False
        Me.gbEmployeeInformation.ShowDefaultBorderColor = True
        Me.gbEmployeeInformation.ShowDownButton = False
        Me.gbEmployeeInformation.ShowHeader = True
        Me.gbEmployeeInformation.Size = New System.Drawing.Size(430, 387)
        Me.gbEmployeeInformation.TabIndex = 5
        Me.gbEmployeeInformation.Temp = 0
        Me.gbEmployeeInformation.Text = "Employee Info"
        Me.gbEmployeeInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmpList
        '
        Me.pnlEmpList.Controls.Add(Me.lvEnrollEmployee)
        Me.pnlEmpList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmpList.Location = New System.Drawing.Point(11, 151)
        Me.pnlEmpList.Name = "pnlEmpList"
        Me.pnlEmpList.Size = New System.Drawing.Size(409, 228)
        Me.pnlEmpList.TabIndex = 39
        '
        'lvEnrollEmployee
        '
        Me.lvEnrollEmployee.BackColorOnChecked = False
        Me.lvEnrollEmployee.CheckBoxes = True
        Me.lvEnrollEmployee.ColumnHeaders = Nothing
        Me.lvEnrollEmployee.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhECode, Me.colhEName, Me.objcolhIsTnA})
        Me.lvEnrollEmployee.CompulsoryColumns = ""
        Me.lvEnrollEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEnrollEmployee.FullRowSelect = True
        Me.lvEnrollEmployee.GridLines = True
        Me.lvEnrollEmployee.GroupingColumn = Nothing
        Me.lvEnrollEmployee.HideSelection = False
        Me.lvEnrollEmployee.Location = New System.Drawing.Point(0, 0)
        Me.lvEnrollEmployee.MinColumnWidth = 50
        Me.lvEnrollEmployee.MultiSelect = False
        Me.lvEnrollEmployee.Name = "lvEnrollEmployee"
        Me.lvEnrollEmployee.OptionalColumns = ""
        Me.lvEnrollEmployee.ShowMoreItem = False
        Me.lvEnrollEmployee.ShowSaveItem = False
        Me.lvEnrollEmployee.ShowSelectAll = True
        Me.lvEnrollEmployee.ShowSizeAllColumnsToFit = True
        Me.lvEnrollEmployee.Size = New System.Drawing.Size(409, 228)
        Me.lvEnrollEmployee.Sortable = True
        Me.lvEnrollEmployee.TabIndex = 38
        Me.lvEnrollEmployee.UseCompatibleStateImageBehavior = False
        Me.lvEnrollEmployee.View = System.Windows.Forms.View.Details
        '
        'colhECode
        '
        Me.colhECode.Tag = "colhECode"
        Me.colhECode.Text = "Code"
        Me.colhECode.Width = 135
        '
        'colhEName
        '
        Me.colhEName.Tag = "colhEName"
        Me.colhEName.Text = "Employee"
        Me.colhEName.Width = 270
        '
        'objcolhIsTnA
        '
        Me.objcolhIsTnA.Tag = "objcolhIsTnA"
        Me.objcolhIsTnA.Text = ""
        Me.objcolhIsTnA.Width = 0
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(220, 115)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 35
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(323, 115)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 37
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'txtJob
        '
        Me.txtJob.Flags = 0
        Me.txtJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJob.Location = New System.Drawing.Point(103, 88)
        Me.txtJob.Name = "txtJob"
        Me.txtJob.ReadOnly = True
        Me.txtJob.Size = New System.Drawing.Size(317, 21)
        Me.txtJob.TabIndex = 33
        '
        'txtDepartment
        '
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(103, 61)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(317, 21)
        Me.txtDepartment.TabIndex = 32
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(103, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(290, 21)
        Me.cboEmployee.TabIndex = 15
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 91)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(89, 15)
        Me.lblJob.TabIndex = 11
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 64)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(89, 15)
        Me.lblDepartment.TabIndex = 10
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(399, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 14
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(89, 15)
        Me.lblEmployeeName.TabIndex = 9
        Me.lblEmployeeName.Text = "Employee Name"
        Me.lblEmployeeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbCourseDetails
        '
        Me.gbCourseDetails.BorderColor = System.Drawing.Color.Black
        Me.gbCourseDetails.Checked = False
        Me.gbCourseDetails.CollapseAllExceptThis = False
        Me.gbCourseDetails.CollapsedHoverImage = Nothing
        Me.gbCourseDetails.CollapsedNormalImage = Nothing
        Me.gbCourseDetails.CollapsedPressedImage = Nothing
        Me.gbCourseDetails.CollapseOnLoad = False
        Me.gbCourseDetails.Controls.Add(Me.objbtnSearchCourse)
        Me.gbCourseDetails.Controls.Add(Me.lblEnrollmentRemark)
        Me.gbCourseDetails.Controls.Add(Me.txtEnrollRemark)
        Me.gbCourseDetails.Controls.Add(Me.EZeeLine2)
        Me.gbCourseDetails.Controls.Add(Me.chkTnAEnrollment)
        Me.gbCourseDetails.Controls.Add(Me.lblTrainingYear)
        Me.gbCourseDetails.Controls.Add(Me.cboTrainingYears)
        Me.gbCourseDetails.Controls.Add(Me.txtEligibility)
        Me.gbCourseDetails.Controls.Add(Me.lblEndDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpEnrolldate)
        Me.gbCourseDetails.Controls.Add(Me.lblEnrollmentDate)
        Me.gbCourseDetails.Controls.Add(Me.lblStartDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpEndDate)
        Me.gbCourseDetails.Controls.Add(Me.dtpStartDate)
        Me.gbCourseDetails.Controls.Add(Me.EZeeLine1)
        Me.gbCourseDetails.Controls.Add(Me.objelLine1)
        Me.gbCourseDetails.Controls.Add(Me.lblCourse)
        Me.gbCourseDetails.Controls.Add(Me.lblStatus)
        Me.gbCourseDetails.Controls.Add(Me.cboCourse)
        Me.gbCourseDetails.Controls.Add(Me.lblEligibility)
        Me.gbCourseDetails.Controls.Add(Me.cboStatus)
        Me.gbCourseDetails.Controls.Add(Me.lblNoOfPositons)
        Me.gbCourseDetails.Controls.Add(Me.txtFilledPosition)
        Me.gbCourseDetails.Controls.Add(Me.txtTotalPosition)
        Me.gbCourseDetails.Controls.Add(Me.lblFilledPositions)
        Me.gbCourseDetails.ExpandedHoverImage = Nothing
        Me.gbCourseDetails.ExpandedNormalImage = Nothing
        Me.gbCourseDetails.ExpandedPressedImage = Nothing
        Me.gbCourseDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCourseDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCourseDetails.HeaderHeight = 25
        Me.gbCourseDetails.HeaderMessage = ""
        Me.gbCourseDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbCourseDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCourseDetails.HeightOnCollapse = 0
        Me.gbCourseDetails.LeftTextSpace = 0
        Me.gbCourseDetails.Location = New System.Drawing.Point(3, 3)
        Me.gbCourseDetails.Name = "gbCourseDetails"
        Me.gbCourseDetails.OpenHeight = 176
        Me.gbCourseDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCourseDetails.ShowBorder = True
        Me.gbCourseDetails.ShowCheckBox = False
        Me.gbCourseDetails.ShowCollapseButton = False
        Me.gbCourseDetails.ShowDefaultBorderColor = True
        Me.gbCourseDetails.ShowDownButton = False
        Me.gbCourseDetails.ShowHeader = True
        Me.gbCourseDetails.Size = New System.Drawing.Size(370, 387)
        Me.gbCourseDetails.TabIndex = 4
        Me.gbCourseDetails.Temp = 0
        Me.gbCourseDetails.Text = "Course Details"
        Me.gbCourseDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCourse
        '
        Me.objbtnSearchCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourse.BorderSelected = False
        Me.objbtnSearchCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourse.Location = New System.Drawing.Point(341, 86)
        Me.objbtnSearchCourse.Name = "objbtnSearchCourse"
        Me.objbtnSearchCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourse.TabIndex = 146
        '
        'lblEnrollmentRemark
        '
        Me.lblEnrollmentRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentRemark.Location = New System.Drawing.Point(11, 293)
        Me.lblEnrollmentRemark.Name = "lblEnrollmentRemark"
        Me.lblEnrollmentRemark.Size = New System.Drawing.Size(324, 15)
        Me.lblEnrollmentRemark.TabIndex = 144
        Me.lblEnrollmentRemark.Text = "Enrollment Remark"
        Me.lblEnrollmentRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEnrollRemark
        '
        Me.txtEnrollRemark.BackColor = System.Drawing.Color.White
        Me.txtEnrollRemark.Flags = 0
        Me.txtEnrollRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEnrollRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEnrollRemark.Location = New System.Drawing.Point(11, 311)
        Me.txtEnrollRemark.Multiline = True
        Me.txtEnrollRemark.Name = "txtEnrollRemark"
        Me.txtEnrollRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEnrollRemark.Size = New System.Drawing.Size(324, 68)
        Me.txtEnrollRemark.TabIndex = 43
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(8, 277)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(327, 16)
        Me.EZeeLine2.TabIndex = 143
        Me.EZeeLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkTnAEnrollment
        '
        Me.chkTnAEnrollment.Checked = True
        Me.chkTnAEnrollment.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTnAEnrollment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTnAEnrollment.Location = New System.Drawing.Point(11, 36)
        Me.chkTnAEnrollment.Name = "chkTnAEnrollment"
        Me.chkTnAEnrollment.Size = New System.Drawing.Size(324, 17)
        Me.chkTnAEnrollment.TabIndex = 142
        Me.chkTnAEnrollment.Text = "Enroll from Training Needs Analysis"
        Me.chkTnAEnrollment.UseVisualStyleBackColor = True
        '
        'lblTrainingYear
        '
        Me.lblTrainingYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingYear.Location = New System.Drawing.Point(8, 62)
        Me.lblTrainingYear.Name = "lblTrainingYear"
        Me.lblTrainingYear.Size = New System.Drawing.Size(104, 15)
        Me.lblTrainingYear.TabIndex = 140
        Me.lblTrainingYear.Text = "Training Years"
        Me.lblTrainingYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingYears
        '
        Me.cboTrainingYears.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingYears.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingYears.FormattingEnabled = True
        Me.cboTrainingYears.Location = New System.Drawing.Point(118, 59)
        Me.cboTrainingYears.Name = "cboTrainingYears"
        Me.cboTrainingYears.Size = New System.Drawing.Size(217, 21)
        Me.cboTrainingYears.TabIndex = 139
        '
        'txtEligibility
        '
        Me.txtEligibility.Flags = 0
        Me.txtEligibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibility.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEligibility.Location = New System.Drawing.Point(118, 113)
        Me.txtEligibility.Name = "txtEligibility"
        Me.txtEligibility.ReadOnly = True
        Me.txtEligibility.Size = New System.Drawing.Size(217, 21)
        Me.txtEligibility.TabIndex = 28
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(177, 186)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(63, 15)
        Me.lblEndDate.TabIndex = 73
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEnrolldate
        '
        Me.dtpEnrolldate.Checked = False
        Me.dtpEnrolldate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnrolldate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnrolldate.Location = New System.Drawing.Point(118, 226)
        Me.dtpEnrolldate.Name = "dtpEnrolldate"
        Me.dtpEnrolldate.Size = New System.Drawing.Size(90, 21)
        Me.dtpEnrolldate.TabIndex = 36
        '
        'lblEnrollmentDate
        '
        Me.lblEnrollmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentDate.Location = New System.Drawing.Point(11, 229)
        Me.lblEnrollmentDate.Name = "lblEnrollmentDate"
        Me.lblEnrollmentDate.Size = New System.Drawing.Size(101, 15)
        Me.lblEnrollmentDate.TabIndex = 35
        Me.lblEnrollmentDate.Text = "Enrollment Date"
        Me.lblEnrollmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(177, 159)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(63, 15)
        Me.lblStartDate.TabIndex = 72
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Enabled = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(246, 183)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpEndDate.TabIndex = 71
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Enabled = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(246, 156)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(89, 21)
        Me.dtpStartDate.TabIndex = 70
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(8, 207)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(327, 16)
        Me.EZeeLine1.TabIndex = 67
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(8, 137)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(327, 16)
        Me.objelLine1.TabIndex = 59
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCourse
        '
        Me.lblCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCourse.Location = New System.Drawing.Point(8, 89)
        Me.lblCourse.Name = "lblCourse"
        Me.lblCourse.Size = New System.Drawing.Size(104, 15)
        Me.lblCourse.TabIndex = 1
        Me.lblCourse.Text = "Scheduled Course"
        Me.lblCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(11, 256)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(101, 15)
        Me.lblStatus.TabIndex = 38
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCourse
        '
        Me.cboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourse.DropDownWidth = 400
        Me.cboCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourse.FormattingEnabled = True
        Me.cboCourse.Location = New System.Drawing.Point(118, 86)
        Me.cboCourse.Name = "cboCourse"
        Me.cboCourse.Size = New System.Drawing.Size(217, 21)
        Me.cboCourse.TabIndex = 7
        '
        'lblEligibility
        '
        Me.lblEligibility.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEligibility.Location = New System.Drawing.Point(8, 116)
        Me.lblEligibility.Name = "lblEligibility"
        Me.lblEligibility.Size = New System.Drawing.Size(104, 15)
        Me.lblEligibility.TabIndex = 3
        Me.lblEligibility.Text = "Eligibility"
        Me.lblEligibility.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(118, 253)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(217, 21)
        Me.cboStatus.TabIndex = 37
        '
        'lblNoOfPositons
        '
        Me.lblNoOfPositons.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfPositons.Location = New System.Drawing.Point(8, 159)
        Me.lblNoOfPositons.Name = "lblNoOfPositons"
        Me.lblNoOfPositons.Size = New System.Drawing.Size(104, 15)
        Me.lblNoOfPositons.TabIndex = 4
        Me.lblNoOfPositons.Text = "No. of Participants"
        Me.lblNoOfPositons.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFilledPosition
        '
        Me.txtFilledPosition.Flags = 0
        Me.txtFilledPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilledPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFilledPosition.Location = New System.Drawing.Point(118, 183)
        Me.txtFilledPosition.Name = "txtFilledPosition"
        Me.txtFilledPosition.ReadOnly = True
        Me.txtFilledPosition.Size = New System.Drawing.Size(53, 21)
        Me.txtFilledPosition.TabIndex = 30
        '
        'txtTotalPosition
        '
        Me.txtTotalPosition.Flags = 0
        Me.txtTotalPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPosition.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTotalPosition.Location = New System.Drawing.Point(118, 156)
        Me.txtTotalPosition.Name = "txtTotalPosition"
        Me.txtTotalPosition.ReadOnly = True
        Me.txtTotalPosition.Size = New System.Drawing.Size(53, 21)
        Me.txtTotalPosition.TabIndex = 29
        Me.txtTotalPosition.Text = "0"
        '
        'lblFilledPositions
        '
        Me.lblFilledPositions.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilledPositions.Location = New System.Drawing.Point(8, 186)
        Me.lblFilledPositions.Name = "lblFilledPositions"
        Me.lblFilledPositions.Size = New System.Drawing.Size(104, 15)
        Me.lblFilledPositions.TabIndex = 5
        Me.lblFilledPositions.Text = "Enrolled Participants"
        Me.lblFilledPositions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmNewEnrollCancellation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(809, 447)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewEnrollCancellation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Enrollment And Cancellation"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeInformation.ResumeLayout(False)
        Me.gbEmployeeInformation.PerformLayout()
        Me.pnlEmpList.ResumeLayout(False)
        Me.gbCourseDetails.ResumeLayout(False)
        Me.gbCourseDetails.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbCourseDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTrainingYear As System.Windows.Forms.Label
    Friend WithEvents cboTrainingYears As System.Windows.Forms.ComboBox
    Friend WithEvents txtEligibility As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtpEnrolldate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEnrollmentDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblCourse As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboCourse As System.Windows.Forms.ComboBox
    Friend WithEvents lblEligibility As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblNoOfPositons As System.Windows.Forms.Label
    Friend WithEvents txtFilledPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTotalPosition As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFilledPositions As System.Windows.Forms.Label
    Friend WithEvents chkTnAEnrollment As System.Windows.Forms.CheckBox
    Friend WithEvents gbEmployeeInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents txtEnrollRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents lblEnrollmentRemark As System.Windows.Forms.Label
    Friend WithEvents lvEnrollEmployee As eZee.Common.eZeeListView
    Friend WithEvents colhECode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsTnA As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents pnlEmpList As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEnroll As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchCourse As eZee.Common.eZeeGradientButton
End Class

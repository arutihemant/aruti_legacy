﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportdata
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportdata))
        Me.PnlImportdata = New System.Windows.Forms.Panel
        Me.wzImportData = New eZee.Common.eZeeWizard
        Me.wpMapfield = New eZee.Common.eZeeWizardPage(Me.components)
        Me.pnlControl = New System.Windows.Forms.Panel
        Me.chkUseMappedDeviceUser = New System.Windows.Forms.CheckBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.objLblEmpCode = New System.Windows.Forms.Label
        Me.cboCheckout = New System.Windows.Forms.ComboBox
        Me.cboLoginDate = New System.Windows.Forms.ComboBox
        Me.lblCheckintime = New System.Windows.Forms.Label
        Me.lblLogindate = New System.Windows.Forms.Label
        Me.lblCheckouttime = New System.Windows.Forms.Label
        Me.cboCheckin = New System.Windows.Forms.ComboBox
        Me.lblMapfield = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.wpImportData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnReplace = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.ShowTimeSheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ShowHolidayDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ShowLeaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.dgEmployeedata = New System.Windows.Forms.DataGridView
        Me.colhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhImortedFor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.wpFileSelection = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblComProduct = New System.Windows.Forms.Label
        Me.cboCommunicationDevice = New System.Windows.Forms.ComboBox
        Me.rdOtherfile = New System.Windows.Forms.RadioButton
        Me.rdPayrollexportFile = New System.Windows.Forms.RadioButton
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LblInstruction = New System.Windows.Forms.Label
        Me.PnlImportdata.SuspendLayout()
        Me.wzImportData.SuspendLayout()
        Me.wpMapfield.SuspendLayout()
        Me.pnlControl.SuspendLayout()
        Me.wpImportData.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.wpFileSelection.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlImportdata
        '
        Me.PnlImportdata.Controls.Add(Me.wzImportData)
        Me.PnlImportdata.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PnlImportdata.Location = New System.Drawing.Point(0, 0)
        Me.PnlImportdata.Name = "PnlImportdata"
        Me.PnlImportdata.Size = New System.Drawing.Size(618, 424)
        Me.PnlImportdata.TabIndex = 0
        '
        'wzImportData
        '
        Me.wzImportData.Controls.Add(Me.wpMapfield)
        Me.wzImportData.Controls.Add(Me.wpFileSelection)
        Me.wzImportData.Controls.Add(Me.wpImportData)
        Me.wzImportData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wzImportData.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.wzImportData.HeaderTitleFont = New System.Drawing.Font("Tahoma", 10.25!, System.Drawing.FontStyle.Bold)
        Me.wzImportData.Location = New System.Drawing.Point(0, 0)
        Me.wzImportData.Name = "wzImportData"
        Me.wzImportData.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpFileSelection, Me.wpMapfield, Me.wpImportData})
        Me.wzImportData.SaveEnabled = True
        Me.wzImportData.SaveText = "Save && Finish"
        Me.wzImportData.SaveVisible = False
        Me.wzImportData.SetSaveIndexBeforeFinishIndex = False
        Me.wzImportData.Size = New System.Drawing.Size(618, 424)
        Me.wzImportData.TabIndex = 0
        Me.wzImportData.WelcomeFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wzImportData.WelcomeImage = Nothing
        Me.wzImportData.WelcomeTitleFont = New System.Drawing.Font("Tahoma", 18.25!, System.Drawing.FontStyle.Bold)
        '
        'wpMapfield
        '
        Me.wpMapfield.Controls.Add(Me.LblInstruction)
        Me.wpMapfield.Controls.Add(Me.pnlControl)
        Me.wpMapfield.Controls.Add(Me.lblMapfield)
        Me.wpMapfield.Controls.Add(Me.cboEmployee)
        Me.wpMapfield.Controls.Add(Me.lblEmployee)
        Me.wpMapfield.Location = New System.Drawing.Point(0, 0)
        Me.wpMapfield.Name = "wpMapfield"
        Me.wpMapfield.Size = New System.Drawing.Size(618, 376)
        Me.wpMapfield.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpMapfield.TabIndex = 9
        '
        'pnlControl
        '
        Me.pnlControl.Controls.Add(Me.chkUseMappedDeviceUser)
        Me.pnlControl.Controls.Add(Me.cboEmployeeCode)
        Me.pnlControl.Controls.Add(Me.objLblEmpCode)
        Me.pnlControl.Controls.Add(Me.cboCheckout)
        Me.pnlControl.Controls.Add(Me.cboLoginDate)
        Me.pnlControl.Controls.Add(Me.lblCheckintime)
        Me.pnlControl.Controls.Add(Me.lblLogindate)
        Me.pnlControl.Controls.Add(Me.lblCheckouttime)
        Me.pnlControl.Controls.Add(Me.cboCheckin)
        Me.pnlControl.Location = New System.Drawing.Point(183, 57)
        Me.pnlControl.Name = "pnlControl"
        Me.pnlControl.Size = New System.Drawing.Size(390, 142)
        Me.pnlControl.TabIndex = 23
        '
        'chkUseMappedDeviceUser
        '
        Me.chkUseMappedDeviceUser.Location = New System.Drawing.Point(119, 120)
        Me.chkUseMappedDeviceUser.Name = "chkUseMappedDeviceUser"
        Me.chkUseMappedDeviceUser.Size = New System.Drawing.Size(188, 17)
        Me.chkUseMappedDeviceUser.TabIndex = 39
        Me.chkUseMappedDeviceUser.Text = "Use Mapped Device User"
        Me.chkUseMappedDeviceUser.UseVisualStyleBackColor = True
        Me.chkUseMappedDeviceUser.Visible = False
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(119, 9)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(121, 21)
        Me.cboEmployeeCode.TabIndex = 23
        '
        'objLblEmpCode
        '
        Me.objLblEmpCode.Location = New System.Drawing.Point(14, 10)
        Me.objLblEmpCode.Name = "objLblEmpCode"
        Me.objLblEmpCode.Size = New System.Drawing.Size(90, 19)
        Me.objLblEmpCode.TabIndex = 24
        Me.objLblEmpCode.Text = "Employee Code"
        Me.objLblEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCheckout
        '
        Me.cboCheckout.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckout.FormattingEnabled = True
        Me.cboCheckout.Location = New System.Drawing.Point(119, 92)
        Me.cboCheckout.Name = "cboCheckout"
        Me.cboCheckout.Size = New System.Drawing.Size(121, 21)
        Me.cboCheckout.TabIndex = 4
        '
        'cboLoginDate
        '
        Me.cboLoginDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoginDate.FormattingEnabled = True
        Me.cboLoginDate.Location = New System.Drawing.Point(119, 36)
        Me.cboLoginDate.Name = "cboLoginDate"
        Me.cboLoginDate.Size = New System.Drawing.Size(121, 21)
        Me.cboLoginDate.TabIndex = 2
        '
        'lblCheckintime
        '
        Me.lblCheckintime.Location = New System.Drawing.Point(14, 65)
        Me.lblCheckintime.Name = "lblCheckintime"
        Me.lblCheckintime.Size = New System.Drawing.Size(90, 19)
        Me.lblCheckintime.TabIndex = 13
        Me.lblCheckintime.Text = "Check In Time"
        Me.lblCheckintime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLogindate
        '
        Me.lblLogindate.Location = New System.Drawing.Point(14, 37)
        Me.lblLogindate.Name = "lblLogindate"
        Me.lblLogindate.Size = New System.Drawing.Size(90, 19)
        Me.lblLogindate.TabIndex = 20
        Me.lblLogindate.Text = "Login Date"
        Me.lblLogindate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCheckouttime
        '
        Me.lblCheckouttime.Location = New System.Drawing.Point(14, 93)
        Me.lblCheckouttime.Name = "lblCheckouttime"
        Me.lblCheckouttime.Size = New System.Drawing.Size(90, 19)
        Me.lblCheckouttime.TabIndex = 14
        Me.lblCheckouttime.Text = "Check Out Time"
        Me.lblCheckouttime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCheckin
        '
        Me.cboCheckin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckin.FormattingEnabled = True
        Me.cboCheckin.Location = New System.Drawing.Point(119, 64)
        Me.cboCheckin.Name = "cboCheckin"
        Me.cboCheckin.Size = New System.Drawing.Size(121, 21)
        Me.cboCheckin.TabIndex = 3
        '
        'lblMapfield
        '
        Me.lblMapfield.BackColor = System.Drawing.Color.Transparent
        Me.lblMapfield.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMapfield.Location = New System.Drawing.Point(179, 19)
        Me.lblMapfield.Name = "lblMapfield"
        Me.lblMapfield.Size = New System.Drawing.Size(124, 23)
        Me.lblMapfield.TabIndex = 10
        Me.lblMapfield.Text = "Map Fields"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(302, 350)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(121, 21)
        Me.cboEmployee.TabIndex = 0
        Me.cboEmployee.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Location = New System.Drawing.Point(197, 351)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(90, 19)
        Me.lblEmployee.TabIndex = 11
        Me.lblEmployee.Text = "Employee Name"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmployee.Visible = False
        '
        'wpImportData
        '
        Me.wpImportData.Controls.Add(Me.btnReplace)
        Me.wpImportData.Controls.Add(Me.btnFilter)
        Me.wpImportData.Controls.Add(Me.dgEmployeedata)
        Me.wpImportData.Controls.Add(Me.Panel1)
        Me.wpImportData.Location = New System.Drawing.Point(0, 0)
        Me.wpImportData.Name = "wpImportData"
        Me.wpImportData.Size = New System.Drawing.Size(618, 376)
        Me.wpImportData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wpImportData.TabIndex = 8
        '
        'btnReplace
        '
        Me.btnReplace.BackColor = System.Drawing.Color.White
        Me.btnReplace.BackgroundImage = CType(resources.GetObject("btnReplace.BackgroundImage"), System.Drawing.Image)
        Me.btnReplace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReplace.BorderColor = System.Drawing.Color.Empty
        Me.btnReplace.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReplace.FlatAppearance.BorderSize = 0
        Me.btnReplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplace.ForeColor = System.Drawing.Color.Black
        Me.btnReplace.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReplace.GradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Location = New System.Drawing.Point(525, 335)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Size = New System.Drawing.Size(81, 30)
        Me.btnReplace.TabIndex = 20
        Me.btnReplace.Text = "&Replace"
        Me.btnReplace.UseVisualStyleBackColor = True
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 335)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(91, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 19
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.ShowTimeSheetToolStripMenuItem, Me.ShowHolidayDataToolStripMenuItem, Me.ShowLeaveToolStripMenuItem})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 158)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'ShowTimeSheetToolStripMenuItem
        '
        Me.ShowTimeSheetToolStripMenuItem.Name = "ShowTimeSheetToolStripMenuItem"
        Me.ShowTimeSheetToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.ShowTimeSheetToolStripMenuItem.Tag = "Timesheet"
        Me.ShowTimeSheetToolStripMenuItem.Text = "Show Time Sheet Data"
        '
        'ShowHolidayDataToolStripMenuItem
        '
        Me.ShowHolidayDataToolStripMenuItem.Name = "ShowHolidayDataToolStripMenuItem"
        Me.ShowHolidayDataToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.ShowHolidayDataToolStripMenuItem.Tag = "Holiday"
        Me.ShowHolidayDataToolStripMenuItem.Text = "Show Holiday Data"
        '
        'ShowLeaveToolStripMenuItem
        '
        Me.ShowLeaveToolStripMenuItem.Name = "ShowLeaveToolStripMenuItem"
        Me.ShowLeaveToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.ShowLeaveToolStripMenuItem.Tag = "Leave"
        Me.ShowLeaveToolStripMenuItem.Text = "Show Leave Data"
        '
        'dgEmployeedata
        '
        Me.dgEmployeedata.AllowUserToAddRows = False
        Me.dgEmployeedata.AllowUserToDeleteRows = False
        Me.dgEmployeedata.AllowUserToResizeColumns = False
        Me.dgEmployeedata.AllowUserToResizeRows = False
        Me.dgEmployeedata.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployeedata.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployeedata.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhImage, Me.colhEmployeeCode, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhImortedFor, Me.objcolhstatus, Me.objcolhDate})
        Me.dgEmployeedata.Location = New System.Drawing.Point(12, 64)
        Me.dgEmployeedata.MultiSelect = False
        Me.dgEmployeedata.Name = "dgEmployeedata"
        Me.dgEmployeedata.ReadOnly = True
        Me.dgEmployeedata.RowHeadersVisible = False
        Me.dgEmployeedata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgEmployeedata.Size = New System.Drawing.Size(594, 265)
        Me.dgEmployeedata.TabIndex = 18
        '
        'colhImage
        '
        Me.colhImage.HeaderText = ""
        Me.colhImage.Name = "colhImage"
        Me.colhImage.ReadOnly = True
        Me.colhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colhImage.Width = 30
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.HeaderText = "Employee Code"
        Me.colhEmployeeCode.Name = "colhEmployeeCode"
        Me.colhEmployeeCode.ReadOnly = True
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhImortedFor
        '
        Me.objcolhImortedFor.HeaderText = "objcolhImortedFor"
        Me.objcolhImortedFor.Name = "objcolhImortedFor"
        Me.objcolhImortedFor.ReadOnly = True
        Me.objcolhImortedFor.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ezWait)
        Me.Panel1.Controls.Add(Me.objError)
        Me.Panel1.Controls.Add(Me.objWarning)
        Me.Panel1.Controls.Add(Me.objSuccess)
        Me.Panel1.Controls.Add(Me.lblWarning)
        Me.Panel1.Controls.Add(Me.lblError)
        Me.Panel1.Controls.Add(Me.objTotal)
        Me.Panel1.Controls.Add(Me.lblSuccess)
        Me.Panel1.Controls.Add(Me.lblTotal)
        Me.Panel1.Location = New System.Drawing.Point(12, 10)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(594, 51)
        Me.Panel1.TabIndex = 1
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(472, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(358, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(472, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(404, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(517, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(358, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(517, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(404, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wpFileSelection
        '
        Me.wpFileSelection.Controls.Add(Me.lblComProduct)
        Me.wpFileSelection.Controls.Add(Me.cboCommunicationDevice)
        Me.wpFileSelection.Controls.Add(Me.rdOtherfile)
        Me.wpFileSelection.Controls.Add(Me.rdPayrollexportFile)
        Me.wpFileSelection.Controls.Add(Me.btnOpenFile)
        Me.wpFileSelection.Controls.Add(Me.txtFilePath)
        Me.wpFileSelection.Controls.Add(Me.lblSelectfile)
        Me.wpFileSelection.Controls.Add(Me.lblMessage)
        Me.wpFileSelection.Controls.Add(Me.lblTitle)
        Me.wpFileSelection.Location = New System.Drawing.Point(0, 0)
        Me.wpFileSelection.Name = "wpFileSelection"
        Me.wpFileSelection.Size = New System.Drawing.Size(618, 376)
        Me.wpFileSelection.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpFileSelection.TabIndex = 7
        '
        'lblComProduct
        '
        Me.lblComProduct.Location = New System.Drawing.Point(180, 113)
        Me.lblComProduct.Name = "lblComProduct"
        Me.lblComProduct.Size = New System.Drawing.Size(138, 19)
        Me.lblComProduct.TabIndex = 25
        Me.lblComProduct.Text = "Communication Device"
        Me.lblComProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCommunicationDevice
        '
        Me.cboCommunicationDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommunicationDevice.FormattingEnabled = True
        Me.cboCommunicationDevice.Location = New System.Drawing.Point(324, 113)
        Me.cboCommunicationDevice.Name = "cboCommunicationDevice"
        Me.cboCommunicationDevice.Size = New System.Drawing.Size(167, 21)
        Me.cboCommunicationDevice.TabIndex = 24
        '
        'rdOtherfile
        '
        Me.rdOtherfile.BackColor = System.Drawing.Color.Transparent
        Me.rdOtherfile.Location = New System.Drawing.Point(183, 178)
        Me.rdOtherfile.Name = "rdOtherfile"
        Me.rdOtherfile.Size = New System.Drawing.Size(123, 19)
        Me.rdOtherfile.TabIndex = 15
        Me.rdOtherfile.Text = "Other File"
        Me.rdOtherfile.UseVisualStyleBackColor = False
        '
        'rdPayrollexportFile
        '
        Me.rdPayrollexportFile.BackColor = System.Drawing.Color.Transparent
        Me.rdPayrollexportFile.Checked = True
        Me.rdPayrollexportFile.Location = New System.Drawing.Point(183, 153)
        Me.rdPayrollexportFile.Name = "rdPayrollexportFile"
        Me.rdPayrollexportFile.Size = New System.Drawing.Size(225, 19)
        Me.rdPayrollexportFile.TabIndex = 14
        Me.rdPayrollexportFile.TabStop = True
        Me.rdPayrollexportFile.Text = "Payroll Export File (Only Xml File)"
        Me.rdPayrollexportFile.UseVisualStyleBackColor = False
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(535, 251)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 13
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(183, 251)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(346, 21)
        Me.txtFilePath.TabIndex = 12
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(183, 224)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(96, 20)
        Me.lblSelectfile.TabIndex = 11
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(179, 59)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(370, 34)
        Me.lblMessage.TabIndex = 10
        Me.lblMessage.Text = "This wizard will import 'Employee Check In' and 'Employee Check Out' records made" & _
            " from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(179, 19)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(370, 23)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "Data Import Wizard"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 30
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Import For"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Message"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 175
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhImortedFor"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objcolhstatus"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objcolhDate"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'LblInstruction
        '
        Me.LblInstruction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblInstruction.ForeColor = System.Drawing.Color.Red
        Me.LblInstruction.Location = New System.Drawing.Point(183, 205)
        Me.LblInstruction.Name = "LblInstruction"
        Me.LblInstruction.Size = New System.Drawing.Size(400, 80)
        Me.LblInstruction.TabIndex = 24
        Me.LblInstruction.Text = "Please Set Date Format for following Fields" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Login Date : dd-MMM-yyyy" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Check In" & _
            " Time : dd-MMM-yyyy hh:mm AM/PM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Check  Time : dd-MMM-yyyy hh:mm AM/PM"
        '
        'frmImportdata
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(618, 424)
        Me.Controls.Add(Me.PnlImportdata)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportdata"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Data"
        Me.PnlImportdata.ResumeLayout(False)
        Me.wzImportData.ResumeLayout(False)
        Me.wpMapfield.ResumeLayout(False)
        Me.pnlControl.ResumeLayout(False)
        Me.wpImportData.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgEmployeedata, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.wpFileSelection.ResumeLayout(False)
        Me.wpFileSelection.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PnlImportdata As System.Windows.Forms.Panel
    Friend WithEvents wzImportData As eZee.Common.eZeeWizard
    Friend WithEvents wpFileSelection As eZee.Common.eZeeWizardPage
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents wpImportData As eZee.Common.eZeeWizardPage
    Friend WithEvents dgEmployeedata As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowTimeSheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowHolidayDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowLeaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents rdPayrollexportFile As System.Windows.Forms.RadioButton
    Friend WithEvents rdOtherfile As System.Windows.Forms.RadioButton
    Friend WithEvents wpMapfield As eZee.Common.eZeeWizardPage
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblMapfield As System.Windows.Forms.Label
    Friend WithEvents cboCheckout As System.Windows.Forms.ComboBox
    Friend WithEvents cboCheckin As System.Windows.Forms.ComboBox
    Friend WithEvents lblCheckouttime As System.Windows.Forms.Label
    Friend WithEvents lblCheckintime As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents btnReplace As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboLoginDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblLogindate As System.Windows.Forms.Label
    Friend WithEvents pnlControl As System.Windows.Forms.Panel
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents objLblEmpCode As System.Windows.Forms.Label
    Friend WithEvents colhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhImortedFor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblComProduct As System.Windows.Forms.Label
    Friend WithEvents cboCommunicationDevice As System.Windows.Forms.ComboBox
    Friend WithEvents chkUseMappedDeviceUser As System.Windows.Forms.CheckBox
    Friend WithEvents LblInstruction As System.Windows.Forms.Label
End Class

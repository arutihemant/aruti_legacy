﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 9

Public Class frmExportdata

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExportdata"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objExportdata As clsExportData
    Private mstrEmployeeId As String = String.Empty
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillData()
        Dim dtFill As DataTable = Nothing
        Try

            dtFill = New DataTable
            dtFill.Columns.Add("Id", Type.GetType("System.Int16"))
            dtFill.Columns.Add("Name", Type.GetType("System.String"))

            Dim drRow As DataRow = dtFill.NewRow
            drRow("Id") = 1
            drRow("Name") = Language.getMessage(mstrModuleName, 1, "TimeSheet")
            dtFill.Rows.Add(drRow)


            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            'drRow = dtFill.NewRow
            'drRow("Id") = 2
            'drRow("Name") = Language.getMessage(mstrModuleName, 2, "Holiday")
            'dtFill.Rows.Add(drRow)

            'drRow = dtFill.NewRow
            'drRow("Id") = 3
            'drRow("Name") = Language.getMessage(mstrModuleName, 3, "Leave")
            'dtFill.Rows.Add(drRow)

            'Pinkal (29-Aug-2012) -- End

            Dim lvitem As ListViewItem
            lvData.Items.Clear()
            For Each dr As DataRow In dtFill.Rows
                lvitem = New ListViewItem
                lvitem.Tag = dr("Id").ToString
                lvitem.Text = dr("Name").ToString
                lvData.Items.Add(lvitem)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillData", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim objEmployee As New clsEmployee_Master
        Dim objStation As New clsStation
        Dim objDept As New clsDepartment
        Try
            tvEmployee.Nodes.Clear()



            Dim dsDepartment As DataSet = objDept.GetList("Department", True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsEmployee As DataSet = objEmployee.GetList("Employee")
            Dim dsEmployee As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, _
                                                            FinancialYear._Object._YearUnkid, _
                                                            Company._Object._Companyunkid, _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                                            True, _
                                                            "Employee", _
                                                            ConfigParameter._Object._ShowFirstAppointmentDate)
            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            'Dim dsStation As DataSet = objStation.GetList("StationList", True)

            'For Each drStation As DataRow In dsStation.Tables("StationList").Rows
            '    Dim tvNodeStation As TreeNode
            '    tvNodeStation = tvEmployee.Nodes.Add(drStation.Item("name").ToString)

            '    Dim dtDept As DataTable = New DataView(dsDepartment.Tables("Department"), "stationunkid=" & CInt(drStation("stationunkid")), "", DataViewRowState.CurrentRows).ToTable
            '    If dtDept.Rows.Count = 0 Then Continue For

            '    For Each drDept As DataRow In dtDept.Rows
            '        tvNodeStation.Nodes.Add(drDept("departmentunkid").ToString, drDept("name").ToString)

            '        Dim dtEmployee As DataTable = New DataView(dsEmployee.Tables("Employee"), "departmentunkid=" & CInt(drDept("departmentunkid")), "", DataViewRowState.CurrentRows).ToTable

            '        For Each drEmployee As DataRow In dtEmployee.Rows
            '            tvNodeStation.Nodes(drDept("departmentunkid").ToString).Nodes.Add(drEmployee("employeeunkid").ToString, drEmployee("name").ToString)
            '        Next

            '    Next
            'Next

            For Each drDept As DataRow In dsDepartment.Tables("Department").Rows
                Dim tvNodeDept As TreeNode
                tvNodeDept = tvEmployee.Nodes.Add(drDept.Item("name").ToString)

                    Dim dtEmployee As DataTable = New DataView(dsEmployee.Tables("Employee"), "departmentunkid=" & CInt(drDept("departmentunkid")), "", DataViewRowState.CurrentRows).ToTable
                If dtEmployee.Rows.Count = 0 Then Continue For

                    For Each drEmployee As DataRow In dtEmployee.Rows
                    tvNodeDept.Nodes.Add(drEmployee("employeeunkid").ToString, drEmployee("name").ToString)
                    Next

                Next

            'Pinkal (29-Aug-2012) -- End

            If tvEmployee.Nodes.Count > 0 Then tvEmployee.ExpandAll()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub CheckForChildren(ByVal nodeSource As TreeNode)
        If nodeSource.Nodes Is Nothing Then Exit Sub

        Try
            For Each nNode As TreeNode In nodeSource.Nodes
                nNode.Checked = nodeSource.Checked
                Call CheckForChildren(nNode)
            Next

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "CheckChildren", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployee()
        Try
            For Each tvDepartment As TreeNode In tvEmployee.Nodes
                    For Each trEmployee As TreeNode In tvDepartment.Nodes
                        If trEmployee.Checked Then
                            mstrEmployeeId &= trEmployee.Name & ","
                        End If
                    Next
                Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmExportdata_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExportdata = New clsExportData
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            cboFileType.SelectedIndex = 0
            FillData()
            FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExportdata_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            Dim objFolderBrowse As New FolderBrowserDialog
            If objFolderBrowse.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFolderBrowse.SelectedPath
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBrowse_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Radio Button's Event"

    Private Sub rabAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rabAll.CheckedChanged
        Try
            If rabAll.Checked Then
                dtpFromdate.Enabled = False
                dtpTodate.Enabled = False
            ElseIf rabDate.Checked Then
                dtpFromdate.Enabled = True
                dtpTodate.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rabAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Wizard's Event"

    Private Sub wzExportData_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzExportData.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wzExportData.Pages.IndexOf(wzpPathSelection)
                    If Not System.IO.Directory.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Path where to Export file."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                Case wzExportData.Pages.IndexOf(wzpSelectData)
                    If e.NewIndex > e.OldIndex Then
                        If lvData.CheckedItems.Count = 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select atleast one Transaction to Export Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case wzExportData.Pages.IndexOf(wzpFilterData)
                    If e.NewIndex > e.OldIndex Then
                        GetEmployee()

                        If cboFileType.SelectedIndex <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select atleast one File Type to Export data"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            cboFileType.Select()
                            e.Cancel = True
                            Exit Sub
                        ElseIf mstrEmployeeId.Length = 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select atleast one Employee to Export data"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                            Exit Sub
                        Else
                            mstrEmployeeId = mstrEmployeeId.Substring(0, mstrEmployeeId.Length - 1)
                        End If

                        Dim blnFlag As Boolean
                        Dim mdtFromdate As Date = Nothing
                        Dim mdtTodate As Date = Nothing

                        If rabDate.Checked Then
                            mdtFromdate = dtpFromdate.Value
                            mdtTodate = dtpTodate.Value
                        End If

                        

                        Dim isTimeSheet As Boolean = False : Dim isHoliday As Boolean = False : Dim isLeave As Boolean = False
                        For i As Integer = 0 To lvData.CheckedItems.Count - 1
                            If CInt(lvData.CheckedItems(i).Tag) = 1 Then
                                isTimeSheet = True
                            ElseIf CInt(lvData.CheckedItems(i).Tag) = 2 Then
                                isHoliday = True
                            ElseIf CInt(lvData.CheckedItems(i).Tag) = 3 Then
                                isLeave = True
                            End If
                        Next

                        Dim FileName As String = String.Empty

                        If CInt(cboFileType.SelectedIndex) = 1 Then
                            FileName = "\" & Date.Now.ToString("yyyyMMdd") & ".xml"
                        ElseIf CInt(cboFileType.SelectedIndex) = 2 Then
                            FileName = "\" & Date.Now.ToString("yyyyMMdd") & ".xls"
                        End If

                        FileName = txtFilePath.Text & FileName

                        blnFlag = objExportdata.ExportData(FileName, mstrEmployeeId, cboFileType.SelectedIndex, mdtFromdate, mdtTodate, isTimeSheet, isHoliday, isLeave)

                        If blnFlag Then
                            objlblFinishMsg.Text = Language.getMessage(mstrModuleName, 6, "Data Successfully Exported to") & " '" & FileName & "'"
                        ElseIf blnFlag = False And objExportdata._Message = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Data did not Export Successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                        ElseIf blnFlag = False And objExportdata._Message <> "" Then
                            eZeeMsgBox.Show(objExportdata._Message, enMsgBoxStyle.Information)
                            e.Cancel = True
                        End If

                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzExportData_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox's Event"

    Private Sub chkSelectall_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectall.CheckedChanged
        Try
            If lvData.Items.Count > 0 Then

                For i As Integer = 0 To lvData.Items.Count - 1
                    lvData.Items(i).Checked = chkSelectall.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectall_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        Try
            If tvEmployee.Nodes.Count > 0 Then

                For i As Integer = 0 To tvEmployee.Nodes.Count - 1
                    tvEmployee.Nodes(i).Checked = chkAll.Checked
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectall_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Treeview's Event"

    Private Sub tvEmployee_AfterCheck(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvEmployee.AfterCheck
        Try
            CheckForChildren(e.Node)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tvEmployee_AfterCheck", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnBrowse.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBrowse.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.wzExportData.CancelText = Language._Object.getCaption(Me.wzExportData.Name & "_CancelText" , Me.wzExportData.CancelText)
			Me.wzExportData.NextText = Language._Object.getCaption(Me.wzExportData.Name & "_NextText" , Me.wzExportData.NextText)
			Me.wzExportData.BackText = Language._Object.getCaption(Me.wzExportData.Name & "_BackText" , Me.wzExportData.BackText)
			Me.wzExportData.FinishText = Language._Object.getCaption(Me.wzExportData.Name & "_FinishText" , Me.wzExportData.FinishText)
			Me.btnBrowse.Text = Language._Object.getCaption(Me.btnBrowse.Name, Me.btnBrowse.Text)
			Me.lblFolderName.Text = Language._Object.getCaption(Me.lblFolderName.Name, Me.lblFolderName.Text)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.lblData.Text = Language._Object.getCaption(Me.lblData.Name, Me.lblData.Text)
			Me.lblSelectData.Text = Language._Object.getCaption(Me.lblSelectData.Name, Me.lblSelectData.Text)
			Me.colhData.Text = Language._Object.getCaption(CStr(Me.colhData.Tag), Me.colhData.Text)
			Me.lblEmployeeSelection.Text = Language._Object.getCaption(Me.lblEmployeeSelection.Name, Me.lblEmployeeSelection.Text)
			Me.chkAll.Text = Language._Object.getCaption(Me.chkAll.Name, Me.chkAll.Text)
			Me.lblFilterdata.Text = Language._Object.getCaption(Me.lblFilterdata.Name, Me.lblFilterdata.Text)
			Me.lblSelectDate.Text = Language._Object.getCaption(Me.lblSelectDate.Name, Me.lblSelectDate.Text)
			Me.rabAll.Text = Language._Object.getCaption(Me.rabAll.Name, Me.rabAll.Text)
			Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
			Me.rabDate.Text = Language._Object.getCaption(Me.rabDate.Name, Me.rabDate.Text)
			Me.chkSelectall.Text = Language._Object.getCaption(Me.chkSelectall.Name, Me.chkSelectall.Text)
			Me.lblFinish.Text = Language._Object.getCaption(Me.lblFinish.Name, Me.lblFinish.Text)
			Me.lblFileType.Text = Language._Object.getCaption(Me.lblFileType.Name, Me.lblFileType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "TimeSheet")
			Language.setMessage(mstrModuleName, 2, "Please select Path where to Export file.")
			Language.setMessage(mstrModuleName, 3, "Please Select atleast one Transaction to Export Data.")
			Language.setMessage(mstrModuleName, 4, "Please Select atleast one File Type to Export data")
			Language.setMessage(mstrModuleName, 5, "Please Select atleast one Employee to Export data")
			Language.setMessage(mstrModuleName, 6, "Data Successfully Exported to")
			Language.setMessage(mstrModuleName, 7, "Data did not Export Successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class









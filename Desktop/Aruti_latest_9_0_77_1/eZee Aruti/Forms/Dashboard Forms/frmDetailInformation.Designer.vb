﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDetailInformation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDetailInformation))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objSlpContainer = New System.Windows.Forms.SplitContainer
        Me.eZeeHeading = New eZee.Common.eZeeHeading
        Me.cboView = New System.Windows.Forms.ComboBox
        Me.chDetail = New System.Windows.Forms.DataVisualization.Charting.Chart
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.pnlMainInfo.SuspendLayout()
        Me.objSlpContainer.Panel1.SuspendLayout()
        Me.objSlpContainer.Panel2.SuspendLayout()
        Me.objSlpContainer.SuspendLayout()
        Me.eZeeHeading.SuspendLayout()
        CType(Me.chDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.BackColor = System.Drawing.Color.Transparent
        Me.pnlMainInfo.Controls.Add(Me.objSlpContainer)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(559, 463)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objSlpContainer
        '
        Me.objSlpContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSlpContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSlpContainer.IsSplitterFixed = True
        Me.objSlpContainer.Location = New System.Drawing.Point(0, 0)
        Me.objSlpContainer.Name = "objSlpContainer"
        Me.objSlpContainer.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSlpContainer.Panel1
        '
        Me.objSlpContainer.Panel1.Controls.Add(Me.eZeeHeading)
        Me.objSlpContainer.Panel1MinSize = 26
        '
        'objSlpContainer.Panel2
        '
        Me.objSlpContainer.Panel2.Controls.Add(Me.chDetail)
        Me.objSlpContainer.Size = New System.Drawing.Size(559, 463)
        Me.objSlpContainer.SplitterDistance = 27
        Me.objSlpContainer.TabIndex = 3
        '
        'eZeeHeading
        '
        Me.eZeeHeading.BorderColor = System.Drawing.Color.Black
        Me.eZeeHeading.Controls.Add(Me.cboView)
        Me.eZeeHeading.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.eZeeHeading.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeading.Name = "eZeeHeading"
        Me.eZeeHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.eZeeHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.eZeeHeading.ShowDefaultBorderColor = True
        Me.eZeeHeading.Size = New System.Drawing.Size(559, 27)
        Me.eZeeHeading.TabIndex = 2
        Me.eZeeHeading.Text = "View Details By :"
        '
        'cboView
        '
        Me.cboView.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboView.FormattingEnabled = True
        Me.cboView.Location = New System.Drawing.Point(335, 3)
        Me.cboView.Name = "cboView"
        Me.cboView.Size = New System.Drawing.Size(221, 21)
        Me.cboView.TabIndex = 1
        '
        'chDetail
        '
        Me.chDetail.BackColor = System.Drawing.SystemColors.Control
        Me.chDetail.BorderlineColor = System.Drawing.SystemColors.Control
        Me.chDetail.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.chDetail.BorderlineWidth = 2
        Me.chDetail.BorderSkin.BackColor = System.Drawing.Color.Transparent
        Me.chDetail.BorderSkin.PageColor = System.Drawing.Color.Transparent
        ChartArea1.Area3DStyle.Enable3D = True
        ChartArea1.Area3DStyle.IsRightAngleAxes = False
        ChartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic
        ChartArea1.Area3DStyle.WallWidth = 2
        ChartArea1.BackColor = System.Drawing.Color.Transparent
        ChartArea1.Name = "ChartArea1"
        Me.chDetail.ChartAreas.Add(ChartArea1)
        Me.chDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Legend1.BackColor = System.Drawing.Color.Transparent
        Legend1.Name = "Legend1"
        Me.chDetail.Legends.Add(Legend1)
        Me.chDetail.Location = New System.Drawing.Point(0, 0)
        Me.chDetail.Margin = New System.Windows.Forms.Padding(0)
        Me.chDetail.Name = "chDetail"
        Me.chDetail.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent
        Series1.ChartArea = "ChartArea1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Series1.ShadowOffset = 2
        Me.chDetail.Series.Add(Series1)
        Me.chDetail.Size = New System.Drawing.Size(559, 432)
        Me.chDetail.TabIndex = 2
        Me.chDetail.Text = "Chart1"
        Title1.Name = "Title1"
        Me.chDetail.Titles.Add(Title1)
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 463)
        Me.objFooter.Margin = New System.Windows.Forms.Padding(0)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(559, 51)
        Me.objFooter.TabIndex = 5
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(13, 11)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(97, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 1
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.objSep1, Me.mnuPrintPreview, Me.mnuPrint})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(153, 98)
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        Me.mnuSave.Size = New System.Drawing.Size(152, 22)
        Me.mnuSave.Text = "&Save"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(149, 6)
        '
        'mnuPrintPreview
        '
        Me.mnuPrintPreview.Name = "mnuPrintPreview"
        Me.mnuPrintPreview.Size = New System.Drawing.Size(152, 22)
        Me.mnuPrintPreview.Text = "Print P&review"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(152, 22)
        Me.mnuPrint.Text = "&Print"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(447, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(100, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.objFooter, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.pnlMainInfo, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(559, 514)
        Me.TableLayoutPanel1.TabIndex = 6
        '
        'frmDetailInformation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(559, 514)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDetailInformation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "View Detail"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objSlpContainer.Panel1.ResumeLayout(False)
        Me.objSlpContainer.Panel2.ResumeLayout(False)
        Me.objSlpContainer.ResumeLayout(False)
        Me.eZeeHeading.ResumeLayout(False)
        CType(Me.chDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeading As eZee.Common.eZeeHeading
    Friend WithEvents objSlpContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents cboView As System.Windows.Forms.ComboBox
    Friend WithEvents chDetail As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPrintPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
End Class

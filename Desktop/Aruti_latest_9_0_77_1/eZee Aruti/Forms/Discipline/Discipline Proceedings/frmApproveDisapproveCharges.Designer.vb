﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveDisapproveCharges
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveDisapproveCharges))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbProceedingInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblProceedingInfo = New System.Windows.Forms.Label
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvProceedingDetails = New System.Windows.Forms.DataGridView
        Me.lblRemark = New System.Windows.Forms.Label
        Me.lblApprovalDate = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.dtpApprovalDate = New System.Windows.Forms.DateTimePicker
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisapprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhParticulars = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhValues = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbProceedingInfo.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbProceedingInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(767, 447)
        Me.pnlMain.TabIndex = 0
        '
        'gbProceedingInfo
        '
        Me.gbProceedingInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProceedingInfo.Checked = False
        Me.gbProceedingInfo.CollapseAllExceptThis = False
        Me.gbProceedingInfo.CollapsedHoverImage = Nothing
        Me.gbProceedingInfo.CollapsedNormalImage = Nothing
        Me.gbProceedingInfo.CollapsedPressedImage = Nothing
        Me.gbProceedingInfo.CollapseOnLoad = False
        Me.gbProceedingInfo.Controls.Add(Me.lblProceedingInfo)
        Me.gbProceedingInfo.Controls.Add(Me.pnlData)
        Me.gbProceedingInfo.Controls.Add(Me.lblRemark)
        Me.gbProceedingInfo.Controls.Add(Me.lblApprovalDate)
        Me.gbProceedingInfo.Controls.Add(Me.txtRemark)
        Me.gbProceedingInfo.Controls.Add(Me.dtpApprovalDate)
        Me.gbProceedingInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProceedingInfo.ExpandedHoverImage = Nothing
        Me.gbProceedingInfo.ExpandedNormalImage = Nothing
        Me.gbProceedingInfo.ExpandedPressedImage = Nothing
        Me.gbProceedingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProceedingInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProceedingInfo.HeaderHeight = 25
        Me.gbProceedingInfo.HeaderMessage = ""
        Me.gbProceedingInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbProceedingInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProceedingInfo.HeightOnCollapse = 0
        Me.gbProceedingInfo.LeftTextSpace = 0
        Me.gbProceedingInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbProceedingInfo.Name = "gbProceedingInfo"
        Me.gbProceedingInfo.OpenHeight = 300
        Me.gbProceedingInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProceedingInfo.ShowBorder = True
        Me.gbProceedingInfo.ShowCheckBox = False
        Me.gbProceedingInfo.ShowCollapseButton = False
        Me.gbProceedingInfo.ShowDefaultBorderColor = True
        Me.gbProceedingInfo.ShowDownButton = False
        Me.gbProceedingInfo.ShowHeader = True
        Me.gbProceedingInfo.Size = New System.Drawing.Size(767, 392)
        Me.gbProceedingInfo.TabIndex = 197
        Me.gbProceedingInfo.Temp = 0
        Me.gbProceedingInfo.Text = "Proceedings Details"
        Me.gbProceedingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProceedingInfo
        '
        Me.lblProceedingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProceedingInfo.Location = New System.Drawing.Point(8, 36)
        Me.lblProceedingInfo.Name = "lblProceedingInfo"
        Me.lblProceedingInfo.Size = New System.Drawing.Size(108, 17)
        Me.lblProceedingInfo.TabIndex = 197
        Me.lblProceedingInfo.Text = "Proceeding Details"
        Me.lblProceedingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvProceedingDetails)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(123, 34)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(632, 260)
        Me.pnlData.TabIndex = 3
        '
        'dgvProceedingDetails
        '
        Me.dgvProceedingDetails.AllowUserToAddRows = False
        Me.dgvProceedingDetails.AllowUserToDeleteRows = False
        Me.dgvProceedingDetails.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvProceedingDetails.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvProceedingDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvProceedingDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvProceedingDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvProceedingDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhParticulars, Me.dgcolhValues})
        Me.dgvProceedingDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProceedingDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvProceedingDetails.MultiSelect = False
        Me.dgvProceedingDetails.Name = "dgvProceedingDetails"
        Me.dgvProceedingDetails.ReadOnly = True
        Me.dgvProceedingDetails.RowHeadersVisible = False
        Me.dgvProceedingDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvProceedingDetails.Size = New System.Drawing.Size(632, 260)
        Me.dgvProceedingDetails.TabIndex = 150
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(8, 329)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(108, 17)
        Me.lblRemark.TabIndex = 196
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprovalDate
        '
        Me.lblApprovalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalDate.Location = New System.Drawing.Point(8, 302)
        Me.lblApprovalDate.Name = "lblApprovalDate"
        Me.lblApprovalDate.Size = New System.Drawing.Size(108, 17)
        Me.lblApprovalDate.TabIndex = 195
        Me.lblApprovalDate.Text = "Approval Date"
        Me.lblApprovalDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(123, 327)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(632, 59)
        Me.txtRemark.TabIndex = 152
        '
        'dtpApprovalDate
        '
        Me.dtpApprovalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApprovalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApprovalDate.Location = New System.Drawing.Point(123, 300)
        Me.dtpApprovalDate.Name = "dtpApprovalDate"
        Me.dtpApprovalDate.Size = New System.Drawing.Size(108, 21)
        Me.dtpApprovalDate.TabIndex = 151
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDisapprove)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 392)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(767, 55)
        Me.objFooter.TabIndex = 149
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(555, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(97, 30)
        Me.btnApprove.TabIndex = 17
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnDisapprove
        '
        Me.btnDisapprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisapprove.BackColor = System.Drawing.Color.White
        Me.btnDisapprove.BackgroundImage = CType(resources.GetObject("btnDisapprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisapprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisapprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisapprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisapprove.FlatAppearance.BorderSize = 0
        Me.btnDisapprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisapprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisapprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisapprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Location = New System.Drawing.Point(555, 13)
        Me.btnDisapprove.Name = "btnDisapprove"
        Me.btnDisapprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Size = New System.Drawing.Size(97, 30)
        Me.btnDisapprove.TabIndex = 15
        Me.btnDisapprove.Text = "&Disapprove"
        Me.btnDisapprove.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(658, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn1.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn2.HeaderText = "Details"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhParticulars
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgcolhParticulars.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhParticulars.HeaderText = "Particulars"
        Me.dgcolhParticulars.Name = "dgcolhParticulars"
        Me.dgcolhParticulars.ReadOnly = True
        Me.dgcolhParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhParticulars.Width = 200
        '
        'dgcolhValues
        '
        Me.dgcolhValues.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhValues.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhValues.HeaderText = "Details"
        Me.dgcolhValues.Name = "dgcolhValues"
        Me.dgcolhValues.ReadOnly = True
        Me.dgcolhValues.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmApproveDisapproveCharges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 447)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveDisapproveCharges"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve/Disapprove Proceeding Count(s)"
        Me.pnlMain.ResumeLayout(False)
        Me.gbProceedingInfo.ResumeLayout(False)
        Me.gbProceedingInfo.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvProceedingDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisapprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvProceedingDetails As System.Windows.Forms.DataGridView
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents dtpApprovalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents lblApprovalDate As System.Windows.Forms.Label
    Friend WithEvents gbProceedingInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblProceedingInfo As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhParticulars As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhValues As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

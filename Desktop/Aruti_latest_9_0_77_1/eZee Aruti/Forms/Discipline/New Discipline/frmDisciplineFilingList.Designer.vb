﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineFilingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineFilingList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvDisciplineInfo = New System.Windows.Forms.ListView
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhDisciplineType = New System.Windows.Forms.ColumnHeader
        Me.colhSeverity = New System.Windows.Forms.ColumnHeader
        Me.colhPersonInvolved = New System.Windows.Forms.ColumnHeader
        Me.colhIncident = New System.Windows.Forms.ColumnHeader
        Me.objcolhInvolvedEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhAgainstEmpId = New System.Windows.Forms.ColumnHeader
        Me.colhCategory = New System.Windows.Forms.ColumnHeader
        Me.objcolhCategoryId = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmailId = New System.Windows.Forms.ColumnHeader
        Me.objemailFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtPersonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.dtpDisciplineDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblPersonInvolved = New System.Windows.Forms.Label
        Me.cboDisciplineType = New System.Windows.Forms.ComboBox
        Me.lblDisciplineType = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuScanDocuments = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep3 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuInAddResolutionSteps = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExAddResolutionSteps = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep4 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuCloseCase = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep5 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuInReopen = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExReopen = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuExemptHead = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPostTransHead = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuViewDisciplineHeads = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExemptedHeadsList = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPostedHeadsList = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep6 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuDisciplineCharge = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.objemailFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objchkAll)
        Me.pnlMain.Controls.Add(Me.lvDisciplineInfo)
        Me.pnlMain.Controls.Add(Me.objemailFooter)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(739, 427)
        Me.pnlMain.TabIndex = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(20, 137)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 8
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvDisciplineInfo
        '
        Me.lvDisciplineInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhDate, Me.colhDisciplineType, Me.colhSeverity, Me.colhPersonInvolved, Me.colhIncident, Me.objcolhInvolvedEmpId, Me.objcolhAgainstEmpId, Me.colhCategory, Me.objcolhCategoryId, Me.objcolhEmailId})
        Me.lvDisciplineInfo.FullRowSelect = True
        Me.lvDisciplineInfo.GridLines = True
        Me.lvDisciplineInfo.HideSelection = False
        Me.lvDisciplineInfo.Location = New System.Drawing.Point(12, 133)
        Me.lvDisciplineInfo.MultiSelect = False
        Me.lvDisciplineInfo.Name = "lvDisciplineInfo"
        Me.lvDisciplineInfo.Size = New System.Drawing.Size(715, 233)
        Me.lvDisciplineInfo.TabIndex = 7
        Me.lvDisciplineInfo.UseCompatibleStateImageBehavior = False
        Me.lvDisciplineInfo.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 85
        '
        'colhDisciplineType
        '
        Me.colhDisciplineType.Tag = "colhDisciplineType"
        Me.colhDisciplineType.Text = "Disciplinary Offences"
        Me.colhDisciplineType.Width = 115
        '
        'colhSeverity
        '
        Me.colhSeverity.Tag = "colhSeverity"
        Me.colhSeverity.Text = "Severity"
        Me.colhSeverity.Width = 55
        '
        'colhPersonInvolved
        '
        Me.colhPersonInvolved.Tag = "colhPersonInvolved"
        Me.colhPersonInvolved.Text = "Person Involved"
        Me.colhPersonInvolved.Width = 155
        '
        'colhIncident
        '
        Me.colhIncident.Tag = "colhIncident"
        Me.colhIncident.Text = "Incident"
        Me.colhIncident.Width = 185
        '
        'objcolhInvolvedEmpId
        '
        Me.objcolhInvolvedEmpId.Tag = "objcolhInvolvedEmpId"
        Me.objcolhInvolvedEmpId.Text = ""
        Me.objcolhInvolvedEmpId.Width = 0
        '
        'objcolhAgainstEmpId
        '
        Me.objcolhAgainstEmpId.Tag = "objcolhAgainstEmpId"
        Me.objcolhAgainstEmpId.Text = ""
        Me.objcolhAgainstEmpId.Width = 0
        '
        'colhCategory
        '
        Me.colhCategory.Tag = "colhCategory"
        Me.colhCategory.Text = "Category"
        Me.colhCategory.Width = 90
        '
        'objcolhCategoryId
        '
        Me.objcolhCategoryId.Tag = "objcolhCategoryId"
        Me.objcolhCategoryId.Text = ""
        Me.objcolhCategoryId.Width = 0
        '
        'objcolhEmailId
        '
        Me.objcolhEmailId.Tag = "objcolhEmailId"
        Me.objcolhEmailId.Text = "objcolhEmailId"
        Me.objcolhEmailId.Width = 0
        '
        'objemailFooter
        '
        Me.objemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objemailFooter.Controls.Add(Me.btnOk)
        Me.objemailFooter.Controls.Add(Me.btnEClose)
        Me.objemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objemailFooter.Location = New System.Drawing.Point(0, 317)
        Me.objemailFooter.Name = "objemailFooter"
        Me.objemailFooter.Size = New System.Drawing.Size(739, 55)
        Me.objemailFooter.TabIndex = 88
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(527, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 17
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(630, 13)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEClose.TabIndex = 16
        Me.btnEClose.Text = "&Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtPersonInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDisciplineDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.cboDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.lblDisciplineType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(715, 63)
        Me.gbFilterCriteria.TabIndex = 6
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPersonInvolved
        '
        Me.txtPersonInvolved.Flags = 0
        Me.txtPersonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPersonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPersonInvolved.Location = New System.Drawing.Point(538, 33)
        Me.txtPersonInvolved.Name = "txtPersonInvolved"
        Me.txtPersonInvolved.Size = New System.Drawing.Size(168, 21)
        Me.txtPersonInvolved.TabIndex = 106
        '
        'dtpDisciplineDate
        '
        Me.dtpDisciplineDate.Checked = False
        Me.dtpDisciplineDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDisciplineDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDisciplineDate.Location = New System.Drawing.Point(336, 33)
        Me.dtpDisciplineDate.Name = "dtpDisciplineDate"
        Me.dtpDisciplineDate.ShowCheckBox = True
        Me.dtpDisciplineDate.Size = New System.Drawing.Size(94, 21)
        Me.dtpDisciplineDate.TabIndex = 96
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(290, 36)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(42, 15)
        Me.lblDate.TabIndex = 93
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonInvolved
        '
        Me.lblPersonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonInvolved.Location = New System.Drawing.Point(435, 36)
        Me.lblPersonInvolved.Name = "lblPersonInvolved"
        Me.lblPersonInvolved.Size = New System.Drawing.Size(98, 15)
        Me.lblPersonInvolved.TabIndex = 105
        Me.lblPersonInvolved.Text = "Person Involved"
        Me.lblPersonInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDisciplineType
        '
        Me.cboDisciplineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisciplineType.FormattingEnabled = True
        Me.cboDisciplineType.Location = New System.Drawing.Point(123, 33)
        Me.cboDisciplineType.Name = "cboDisciplineType"
        Me.cboDisciplineType.Size = New System.Drawing.Size(160, 21)
        Me.cboDisciplineType.TabIndex = 97
        '
        'lblDisciplineType
        '
        Me.lblDisciplineType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisciplineType.Location = New System.Drawing.Point(5, 36)
        Me.lblDisciplineType.Name = "lblDisciplineType"
        Me.lblDisciplineType.Size = New System.Drawing.Size(114, 15)
        Me.lblDisciplineType.TabIndex = 94
        Me.lblDisciplineType.Text = "Disciplinary Offence"
        Me.lblDisciplineType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(688, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(665, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(739, 58)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Disciplinary Charges List"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 372)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(739, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperation
        Me.btnOperations.TabIndex = 125
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScanDocuments, Me.objSep3, Me.mnuInAddResolutionSteps, Me.mnuExAddResolutionSteps, Me.objSep4, Me.mnuCloseCase, Me.objSep5, Me.mnuInReopen, Me.mnuExReopen, Me.objSep1, Me.mnuExemptHead, Me.mnuPostTransHead, Me.objSep2, Me.mnuViewDisciplineHeads, Me.objSep6, Me.mnuDisciplineCharge})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(231, 260)
        '
        'mnuScanDocuments
        '
        Me.mnuScanDocuments.Name = "mnuScanDocuments"
        Me.mnuScanDocuments.Size = New System.Drawing.Size(230, 22)
        Me.mnuScanDocuments.Text = "&Scan Documents"
        '
        'objSep3
        '
        Me.objSep3.Name = "objSep3"
        Me.objSep3.Size = New System.Drawing.Size(227, 6)
        '
        'mnuInAddResolutionSteps
        '
        Me.mnuInAddResolutionSteps.Name = "mnuInAddResolutionSteps"
        Me.mnuInAddResolutionSteps.Size = New System.Drawing.Size(230, 22)
        Me.mnuInAddResolutionSteps.Text = "Add Internal &Resolution Steps"
        '
        'mnuExAddResolutionSteps
        '
        Me.mnuExAddResolutionSteps.Name = "mnuExAddResolutionSteps"
        Me.mnuExAddResolutionSteps.Size = New System.Drawing.Size(230, 22)
        Me.mnuExAddResolutionSteps.Text = "Add External &Resolution Steps"
        '
        'objSep4
        '
        Me.objSep4.Name = "objSep4"
        Me.objSep4.Size = New System.Drawing.Size(227, 6)
        '
        'mnuCloseCase
        '
        Me.mnuCloseCase.Name = "mnuCloseCase"
        Me.mnuCloseCase.Size = New System.Drawing.Size(230, 22)
        Me.mnuCloseCase.Tag = "mnuCloseCase"
        Me.mnuCloseCase.Text = "Add &Final Step"
        '
        'objSep5
        '
        Me.objSep5.Name = "objSep5"
        Me.objSep5.Size = New System.Drawing.Size(227, 6)
        '
        'mnuInReopen
        '
        Me.mnuInReopen.Name = "mnuInReopen"
        Me.mnuInReopen.Size = New System.Drawing.Size(230, 22)
        Me.mnuInReopen.Tag = "mnuInReopen"
        Me.mnuInReopen.Text = "&Re-Open Case Internally"
        '
        'mnuExReopen
        '
        Me.mnuExReopen.Name = "mnuExReopen"
        Me.mnuExReopen.Size = New System.Drawing.Size(230, 22)
        Me.mnuExReopen.Tag = "mnuExReopen"
        Me.mnuExReopen.Text = "&Re-Open Case Externally"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(227, 6)
        '
        'mnuExemptHead
        '
        Me.mnuExemptHead.Name = "mnuExemptHead"
        Me.mnuExemptHead.Size = New System.Drawing.Size(230, 22)
        Me.mnuExemptHead.Text = "Exempt Trans. Heads"
        '
        'mnuPostTransHead
        '
        Me.mnuPostTransHead.Name = "mnuPostTransHead"
        Me.mnuPostTransHead.Size = New System.Drawing.Size(230, 22)
        Me.mnuPostTransHead.Text = "Post Trans. Head"
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(227, 6)
        '
        'mnuViewDisciplineHeads
        '
        Me.mnuViewDisciplineHeads.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExemptedHeadsList, Me.mnuPostedHeadsList})
        Me.mnuViewDisciplineHeads.Name = "mnuViewDisciplineHeads"
        Me.mnuViewDisciplineHeads.Size = New System.Drawing.Size(230, 22)
        Me.mnuViewDisciplineHeads.Text = "&View Discipline Heads"
        '
        'mnuExemptedHeadsList
        '
        Me.mnuExemptedHeadsList.Name = "mnuExemptedHeadsList"
        Me.mnuExemptedHeadsList.Size = New System.Drawing.Size(166, 22)
        Me.mnuExemptedHeadsList.Text = "E&xempted Heads"
        '
        'mnuPostedHeadsList
        '
        Me.mnuPostedHeadsList.Name = "mnuPostedHeadsList"
        Me.mnuPostedHeadsList.Size = New System.Drawing.Size(166, 22)
        Me.mnuPostedHeadsList.Text = "P&osted Heads"
        '
        'objSep6
        '
        Me.objSep6.Name = "objSep6"
        Me.objSep6.Size = New System.Drawing.Size(227, 6)
        '
        'mnuDisciplineCharge
        '
        Me.mnuDisciplineCharge.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuPreview})
        Me.mnuDisciplineCharge.Name = "mnuDisciplineCharge"
        Me.mnuDisciplineCharge.Size = New System.Drawing.Size(230, 22)
        Me.mnuDisciplineCharge.Tag = "mnuDisciplineCharge"
        Me.mnuDisciplineCharge.Text = "Discipline Charge"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(123, 22)
        Me.mnuPrint.Tag = "mnuPrint"
        Me.mnuPrint.Text = "&Print"
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(123, 22)
        Me.mnuPreview.Tag = "mnuPreview"
        Me.mnuPreview.Text = "P&review"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(527, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(424, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(321, 13)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 122
        Me.btnAdd.Text = "&New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(630, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmDisciplineFilingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 427)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineFilingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Disciplinary Charges List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objemailFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPersonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPersonInvolved As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpDisciplineDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDisciplineType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisciplineType As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lvDisciplineInfo As System.Windows.Forms.ListView
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDisciplineType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSeverity As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPersonInvolved As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIncident As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScanDocuments As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInAddResolutionSteps As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhInvolvedEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAgainstEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExemptHead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostTransHead As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuViewDisciplineHeads As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExemptedHeadsList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostedHeadsList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuCloseCase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInReopen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colhCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSep3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExAddResolutionSteps As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objSep5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExReopen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhCategoryId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhEmailId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSep6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDisciplineCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
End Class

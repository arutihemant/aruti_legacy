﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralSettings
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGeneralSettings))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lblChanges = New System.Windows.Forms.Label
        Me.tabcGeneralInfomation = New System.Windows.Forms.TabControl
        Me.tabpGeneralSettings = New System.Windows.Forms.TabPage
        Me.gbReaderType = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblDelaySec = New System.Windows.Forms.Label
        Me.nudDelayTime = New System.Windows.Forms.NumericUpDown
        Me.lblDelayTime = New System.Windows.Forms.Label
        Me.cboReaderType = New System.Windows.Forms.ComboBox
        Me.lblReaderType = New System.Windows.Forms.Label
        Me.gbPortSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPort = New System.Windows.Forms.ComboBox
        Me.cboStopBits = New System.Windows.Forms.ComboBox
        Me.cboParity = New System.Windows.Forms.ComboBox
        Me.cboDataBits = New System.Windows.Forms.ComboBox
        Me.cboBitPerSecond = New System.Windows.Forms.ComboBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblStopBits = New System.Windows.Forms.Label
        Me.lblParity = New System.Windows.Forms.Label
        Me.lblDataBits = New System.Windows.Forms.Label
        Me.lblBitPerSettings = New System.Windows.Forms.Label
        Me.tabpClientServer = New System.Windows.Forms.TabPage
        Me.picClientServer = New System.Windows.Forms.PictureBox
        Me.gbClientServerSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSQLPort = New eZee.TextBox.NumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblAutoUpdateServer = New System.Windows.Forms.Label
        Me.txtAutoUpdateServer = New eZee.TextBox.AlphanumericTextBox
        Me.lblMinutes = New System.Windows.Forms.Label
        Me.nudRefreshInterval = New System.Windows.Forms.NumericUpDown
        Me.lblInterval = New System.Windows.Forms.Label
        Me.txtServer = New eZee.TextBox.AlphanumericTextBox
        Me.lblServer = New System.Windows.Forms.Label
        Me.cboWorkingMode = New System.Windows.Forms.ComboBox
        Me.lblWorkingMode = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnInstructions = New eZee.Common.eZeeGradientButton
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcGeneralInfomation.SuspendLayout()
        Me.tabpGeneralSettings.SuspendLayout()
        Me.gbReaderType.SuspendLayout()
        CType(Me.nudDelayTime, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPortSettings.SuspendLayout()
        Me.tabpClientServer.SuspendLayout()
        CType(Me.picClientServer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbClientServerSettings.SuspendLayout()
        CType(Me.nudRefreshInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lblChanges)
        Me.pnlMainInfo.Controls.Add(Me.tabcGeneralInfomation)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(505, 353)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lblChanges
        '
        Me.lblChanges.ForeColor = System.Drawing.Color.DarkRed
        Me.lblChanges.Location = New System.Drawing.Point(12, 273)
        Me.lblChanges.Name = "lblChanges"
        Me.lblChanges.Size = New System.Drawing.Size(482, 16)
        Me.lblChanges.TabIndex = 14
        Me.lblChanges.Text = "Note : You have to restart the system for implementing new changes."
        Me.lblChanges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcGeneralInfomation
        '
        Me.tabcGeneralInfomation.Controls.Add(Me.tabpGeneralSettings)
        Me.tabcGeneralInfomation.Controls.Add(Me.tabpClientServer)
        Me.tabcGeneralInfomation.Location = New System.Drawing.Point(12, 66)
        Me.tabcGeneralInfomation.Name = "tabcGeneralInfomation"
        Me.tabcGeneralInfomation.SelectedIndex = 0
        Me.tabcGeneralInfomation.Size = New System.Drawing.Size(482, 200)
        Me.tabcGeneralInfomation.TabIndex = 13
        '
        'tabpGeneralSettings
        '
        Me.tabpGeneralSettings.Controls.Add(Me.gbReaderType)
        Me.tabpGeneralSettings.Controls.Add(Me.gbPortSettings)
        Me.tabpGeneralSettings.Location = New System.Drawing.Point(4, 22)
        Me.tabpGeneralSettings.Name = "tabpGeneralSettings"
        Me.tabpGeneralSettings.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpGeneralSettings.Size = New System.Drawing.Size(474, 174)
        Me.tabpGeneralSettings.TabIndex = 0
        Me.tabpGeneralSettings.Text = "General Settings"
        Me.tabpGeneralSettings.UseVisualStyleBackColor = True
        '
        'gbReaderType
        '
        Me.gbReaderType.BorderColor = System.Drawing.Color.Black
        Me.gbReaderType.Checked = False
        Me.gbReaderType.CollapseAllExceptThis = False
        Me.gbReaderType.CollapsedHoverImage = Nothing
        Me.gbReaderType.CollapsedNormalImage = Nothing
        Me.gbReaderType.CollapsedPressedImage = Nothing
        Me.gbReaderType.CollapseOnLoad = False
        Me.gbReaderType.Controls.Add(Me.lblDelaySec)
        Me.gbReaderType.Controls.Add(Me.nudDelayTime)
        Me.gbReaderType.Controls.Add(Me.lblDelayTime)
        Me.gbReaderType.Controls.Add(Me.cboReaderType)
        Me.gbReaderType.Controls.Add(Me.lblReaderType)
        Me.gbReaderType.ExpandedHoverImage = Nothing
        Me.gbReaderType.ExpandedNormalImage = Nothing
        Me.gbReaderType.ExpandedPressedImage = Nothing
        Me.gbReaderType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReaderType.GradientColor = System.Drawing.Color.Transparent
        Me.gbReaderType.HeaderHeight = 25
        Me.gbReaderType.HeaderMessage = ""
        Me.gbReaderType.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbReaderType.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReaderType.HeightOnCollapse = 0
        Me.gbReaderType.LeftTextSpace = 0
        Me.gbReaderType.Location = New System.Drawing.Point(224, 3)
        Me.gbReaderType.Name = "gbReaderType"
        Me.gbReaderType.OpenHeight = 300
        Me.gbReaderType.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReaderType.ShowBorder = True
        Me.gbReaderType.ShowCheckBox = False
        Me.gbReaderType.ShowCollapseButton = False
        Me.gbReaderType.ShowDefaultBorderColor = True
        Me.gbReaderType.ShowDownButton = False
        Me.gbReaderType.ShowHeader = True
        Me.gbReaderType.Size = New System.Drawing.Size(246, 91)
        Me.gbReaderType.TabIndex = 1
        Me.gbReaderType.Temp = 0
        Me.gbReaderType.Text = "Reader Type"
        Me.gbReaderType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDelaySec
        '
        Me.lblDelaySec.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDelaySec.Location = New System.Drawing.Point(148, 63)
        Me.lblDelaySec.Name = "lblDelaySec"
        Me.lblDelaySec.Size = New System.Drawing.Size(87, 16)
        Me.lblDelaySec.TabIndex = 9
        Me.lblDelaySec.Text = "Sec."
        Me.lblDelaySec.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDelayTime
        '
        Me.nudDelayTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDelayTime.Location = New System.Drawing.Point(95, 61)
        Me.nudDelayTime.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.nudDelayTime.Name = "nudDelayTime"
        Me.nudDelayTime.ReadOnly = True
        Me.nudDelayTime.Size = New System.Drawing.Size(47, 21)
        Me.nudDelayTime.TabIndex = 17
        Me.nudDelayTime.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'lblDelayTime
        '
        Me.lblDelayTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDelayTime.Location = New System.Drawing.Point(8, 63)
        Me.lblDelayTime.Name = "lblDelayTime"
        Me.lblDelayTime.Size = New System.Drawing.Size(81, 16)
        Me.lblDelayTime.TabIndex = 16
        Me.lblDelayTime.Text = "Delay Time"
        Me.lblDelayTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReaderType
        '
        Me.cboReaderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReaderType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReaderType.FormattingEnabled = True
        Me.cboReaderType.Location = New System.Drawing.Point(95, 34)
        Me.cboReaderType.Name = "cboReaderType"
        Me.cboReaderType.Size = New System.Drawing.Size(140, 21)
        Me.cboReaderType.TabIndex = 15
        '
        'lblReaderType
        '
        Me.lblReaderType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReaderType.Location = New System.Drawing.Point(8, 36)
        Me.lblReaderType.Name = "lblReaderType"
        Me.lblReaderType.Size = New System.Drawing.Size(81, 16)
        Me.lblReaderType.TabIndex = 12
        Me.lblReaderType.Text = "Reader Type"
        Me.lblReaderType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbPortSettings
        '
        Me.gbPortSettings.BorderColor = System.Drawing.Color.Black
        Me.gbPortSettings.Checked = False
        Me.gbPortSettings.CollapseAllExceptThis = False
        Me.gbPortSettings.CollapsedHoverImage = Nothing
        Me.gbPortSettings.CollapsedNormalImage = Nothing
        Me.gbPortSettings.CollapsedPressedImage = Nothing
        Me.gbPortSettings.CollapseOnLoad = False
        Me.gbPortSettings.Controls.Add(Me.cboPort)
        Me.gbPortSettings.Controls.Add(Me.cboStopBits)
        Me.gbPortSettings.Controls.Add(Me.cboParity)
        Me.gbPortSettings.Controls.Add(Me.cboDataBits)
        Me.gbPortSettings.Controls.Add(Me.cboBitPerSecond)
        Me.gbPortSettings.Controls.Add(Me.lblPort)
        Me.gbPortSettings.Controls.Add(Me.lblStopBits)
        Me.gbPortSettings.Controls.Add(Me.lblParity)
        Me.gbPortSettings.Controls.Add(Me.lblDataBits)
        Me.gbPortSettings.Controls.Add(Me.lblBitPerSettings)
        Me.gbPortSettings.ExpandedHoverImage = Nothing
        Me.gbPortSettings.ExpandedNormalImage = Nothing
        Me.gbPortSettings.ExpandedPressedImage = Nothing
        Me.gbPortSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPortSettings.GradientColor = System.Drawing.Color.Transparent
        Me.gbPortSettings.HeaderHeight = 25
        Me.gbPortSettings.HeaderMessage = ""
        Me.gbPortSettings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPortSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPortSettings.HeightOnCollapse = 0
        Me.gbPortSettings.LeftTextSpace = 0
        Me.gbPortSettings.Location = New System.Drawing.Point(3, 3)
        Me.gbPortSettings.Name = "gbPortSettings"
        Me.gbPortSettings.OpenHeight = 300
        Me.gbPortSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPortSettings.ShowBorder = True
        Me.gbPortSettings.ShowCheckBox = False
        Me.gbPortSettings.ShowCollapseButton = False
        Me.gbPortSettings.ShowDefaultBorderColor = True
        Me.gbPortSettings.ShowDownButton = False
        Me.gbPortSettings.ShowHeader = True
        Me.gbPortSettings.Size = New System.Drawing.Size(214, 168)
        Me.gbPortSettings.TabIndex = 0
        Me.gbPortSettings.Temp = 0
        Me.gbPortSettings.Text = "Port Settings"
        Me.gbPortSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPort
        '
        Me.cboPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPort.FormattingEnabled = True
        Me.cboPort.Location = New System.Drawing.Point(99, 141)
        Me.cboPort.Name = "cboPort"
        Me.cboPort.Size = New System.Drawing.Size(107, 21)
        Me.cboPort.TabIndex = 18
        '
        'cboStopBits
        '
        Me.cboStopBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStopBits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStopBits.FormattingEnabled = True
        Me.cboStopBits.Location = New System.Drawing.Point(99, 114)
        Me.cboStopBits.Name = "cboStopBits"
        Me.cboStopBits.Size = New System.Drawing.Size(107, 21)
        Me.cboStopBits.TabIndex = 17
        '
        'cboParity
        '
        Me.cboParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboParity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboParity.FormattingEnabled = True
        Me.cboParity.Location = New System.Drawing.Point(99, 87)
        Me.cboParity.Name = "cboParity"
        Me.cboParity.Size = New System.Drawing.Size(107, 21)
        Me.cboParity.TabIndex = 16
        '
        'cboDataBits
        '
        Me.cboDataBits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDataBits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDataBits.FormattingEnabled = True
        Me.cboDataBits.Location = New System.Drawing.Point(99, 60)
        Me.cboDataBits.Name = "cboDataBits"
        Me.cboDataBits.Size = New System.Drawing.Size(107, 21)
        Me.cboDataBits.TabIndex = 15
        '
        'cboBitPerSecond
        '
        Me.cboBitPerSecond.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBitPerSecond.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBitPerSecond.FormattingEnabled = True
        Me.cboBitPerSecond.Location = New System.Drawing.Point(99, 33)
        Me.cboBitPerSecond.Name = "cboBitPerSecond"
        Me.cboBitPerSecond.Size = New System.Drawing.Size(107, 21)
        Me.cboBitPerSecond.TabIndex = 14
        '
        'lblPort
        '
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPort.Location = New System.Drawing.Point(12, 143)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(81, 16)
        Me.lblPort.TabIndex = 13
        Me.lblPort.Text = "Port"
        Me.lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStopBits
        '
        Me.lblStopBits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopBits.Location = New System.Drawing.Point(12, 116)
        Me.lblStopBits.Name = "lblStopBits"
        Me.lblStopBits.Size = New System.Drawing.Size(81, 16)
        Me.lblStopBits.TabIndex = 12
        Me.lblStopBits.Text = "Stop Bits"
        Me.lblStopBits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblParity
        '
        Me.lblParity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblParity.Location = New System.Drawing.Point(12, 89)
        Me.lblParity.Name = "lblParity"
        Me.lblParity.Size = New System.Drawing.Size(81, 16)
        Me.lblParity.TabIndex = 11
        Me.lblParity.Text = "Parity"
        Me.lblParity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDataBits
        '
        Me.lblDataBits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataBits.Location = New System.Drawing.Point(12, 62)
        Me.lblDataBits.Name = "lblDataBits"
        Me.lblDataBits.Size = New System.Drawing.Size(81, 16)
        Me.lblDataBits.TabIndex = 10
        Me.lblDataBits.Text = "Data Bits"
        Me.lblDataBits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBitPerSettings
        '
        Me.lblBitPerSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBitPerSettings.Location = New System.Drawing.Point(12, 35)
        Me.lblBitPerSettings.Name = "lblBitPerSettings"
        Me.lblBitPerSettings.Size = New System.Drawing.Size(81, 16)
        Me.lblBitPerSettings.TabIndex = 9
        Me.lblBitPerSettings.Text = "Bit Per Second"
        Me.lblBitPerSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpClientServer
        '
        Me.tabpClientServer.Controls.Add(Me.picClientServer)
        Me.tabpClientServer.Controls.Add(Me.gbClientServerSettings)
        Me.tabpClientServer.Location = New System.Drawing.Point(4, 22)
        Me.tabpClientServer.Name = "tabpClientServer"
        Me.tabpClientServer.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpClientServer.Size = New System.Drawing.Size(474, 174)
        Me.tabpClientServer.TabIndex = 1
        Me.tabpClientServer.Text = "Client/Server"
        Me.tabpClientServer.UseVisualStyleBackColor = True
        '
        'picClientServer
        '
        Me.picClientServer.BackgroundImage = CType(resources.GetObject("picClientServer.BackgroundImage"), System.Drawing.Image)
        Me.picClientServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picClientServer.Location = New System.Drawing.Point(3, 3)
        Me.picClientServer.Name = "picClientServer"
        Me.picClientServer.Size = New System.Drawing.Size(172, 168)
        Me.picClientServer.TabIndex = 2
        Me.picClientServer.TabStop = False
        '
        'gbClientServerSettings
        '
        Me.gbClientServerSettings.BorderColor = System.Drawing.Color.Black
        Me.gbClientServerSettings.Checked = False
        Me.gbClientServerSettings.CollapseAllExceptThis = False
        Me.gbClientServerSettings.CollapsedHoverImage = Nothing
        Me.gbClientServerSettings.CollapsedNormalImage = Nothing
        Me.gbClientServerSettings.CollapsedPressedImage = Nothing
        Me.gbClientServerSettings.CollapseOnLoad = False
        Me.gbClientServerSettings.Controls.Add(Me.objbtnInstructions)
        Me.gbClientServerSettings.Controls.Add(Me.txtSQLPort)
        Me.gbClientServerSettings.Controls.Add(Me.Label1)
        Me.gbClientServerSettings.Controls.Add(Me.lblAutoUpdateServer)
        Me.gbClientServerSettings.Controls.Add(Me.txtAutoUpdateServer)
        Me.gbClientServerSettings.Controls.Add(Me.lblMinutes)
        Me.gbClientServerSettings.Controls.Add(Me.nudRefreshInterval)
        Me.gbClientServerSettings.Controls.Add(Me.lblInterval)
        Me.gbClientServerSettings.Controls.Add(Me.txtServer)
        Me.gbClientServerSettings.Controls.Add(Me.lblServer)
        Me.gbClientServerSettings.Controls.Add(Me.cboWorkingMode)
        Me.gbClientServerSettings.Controls.Add(Me.lblWorkingMode)
        Me.gbClientServerSettings.ExpandedHoverImage = Nothing
        Me.gbClientServerSettings.ExpandedNormalImage = Nothing
        Me.gbClientServerSettings.ExpandedPressedImage = Nothing
        Me.gbClientServerSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClientServerSettings.GradientColor = System.Drawing.Color.Transparent
        Me.gbClientServerSettings.HeaderHeight = 25
        Me.gbClientServerSettings.HeaderMessage = ""
        Me.gbClientServerSettings.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbClientServerSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbClientServerSettings.HeightOnCollapse = 0
        Me.gbClientServerSettings.LeftTextSpace = 0
        Me.gbClientServerSettings.Location = New System.Drawing.Point(181, 3)
        Me.gbClientServerSettings.Name = "gbClientServerSettings"
        Me.gbClientServerSettings.OpenHeight = 300
        Me.gbClientServerSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClientServerSettings.ShowBorder = True
        Me.gbClientServerSettings.ShowCheckBox = False
        Me.gbClientServerSettings.ShowCollapseButton = False
        Me.gbClientServerSettings.ShowDefaultBorderColor = True
        Me.gbClientServerSettings.ShowDownButton = False
        Me.gbClientServerSettings.ShowHeader = True
        Me.gbClientServerSettings.Size = New System.Drawing.Size(290, 168)
        Me.gbClientServerSettings.TabIndex = 1
        Me.gbClientServerSettings.Temp = 0
        Me.gbClientServerSettings.Text = "Client/Server Settings"
        Me.gbClientServerSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSQLPort
        '
        Me.txtSQLPort.AllowNegative = False
        Me.txtSQLPort.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSQLPort.DigitsInGroup = 0
        Me.txtSQLPort.Flags = 65536
        Me.txtSQLPort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSQLPort.Location = New System.Drawing.Point(124, 114)
        Me.txtSQLPort.MaxDecimalPlaces = 0
        Me.txtSQLPort.MaxWholeDigits = 21
        Me.txtSQLPort.Name = "txtSQLPort"
        Me.txtSQLPort.Prefix = ""
        Me.txtSQLPort.RangeMax = 1.7976931348623157E+308
        Me.txtSQLPort.RangeMin = -1.7976931348623157E+308
        Me.txtSQLPort.Size = New System.Drawing.Size(60, 21)
        Me.txtSQLPort.TabIndex = 26
        Me.txtSQLPort.Text = "0"
        Me.txtSQLPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 16)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "SQL Port"
        '
        'lblAutoUpdateServer
        '
        Me.lblAutoUpdateServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAutoUpdateServer.Location = New System.Drawing.Point(8, 143)
        Me.lblAutoUpdateServer.Name = "lblAutoUpdateServer"
        Me.lblAutoUpdateServer.Size = New System.Drawing.Size(110, 16)
        Me.lblAutoUpdateServer.TabIndex = 11
        Me.lblAutoUpdateServer.Text = "Auto Update Server Server"
        Me.lblAutoUpdateServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAutoUpdateServer
        '
        Me.txtAutoUpdateServer.Flags = 0
        Me.txtAutoUpdateServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAutoUpdateServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAutoUpdateServer.Location = New System.Drawing.Point(124, 141)
        Me.txtAutoUpdateServer.Name = "txtAutoUpdateServer"
        Me.txtAutoUpdateServer.Size = New System.Drawing.Size(140, 21)
        Me.txtAutoUpdateServer.TabIndex = 10
        '
        'lblMinutes
        '
        Me.lblMinutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinutes.Location = New System.Drawing.Point(191, 89)
        Me.lblMinutes.Name = "lblMinutes"
        Me.lblMinutes.Size = New System.Drawing.Size(62, 16)
        Me.lblMinutes.TabIndex = 8
        Me.lblMinutes.Text = "Minutes"
        Me.lblMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudRefreshInterval
        '
        Me.nudRefreshInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudRefreshInterval.Location = New System.Drawing.Point(124, 87)
        Me.nudRefreshInterval.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.nudRefreshInterval.Name = "nudRefreshInterval"
        Me.nudRefreshInterval.ReadOnly = True
        Me.nudRefreshInterval.Size = New System.Drawing.Size(61, 21)
        Me.nudRefreshInterval.TabIndex = 7
        Me.nudRefreshInterval.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'lblInterval
        '
        Me.lblInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterval.Location = New System.Drawing.Point(8, 89)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(110, 16)
        Me.lblInterval.TabIndex = 6
        Me.lblInterval.Text = "Refresh Interval"
        Me.lblInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtServer
        '
        Me.txtServer.Flags = 0
        Me.txtServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServer.Location = New System.Drawing.Point(124, 59)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(140, 21)
        Me.txtServer.TabIndex = 5
        Me.txtServer.Text = "(Local)"
        '
        'lblServer
        '
        Me.lblServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServer.Location = New System.Drawing.Point(8, 61)
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(110, 16)
        Me.lblServer.TabIndex = 4
        Me.lblServer.Text = "Database Server"
        Me.lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboWorkingMode
        '
        Me.cboWorkingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWorkingMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWorkingMode.FormattingEnabled = True
        Me.cboWorkingMode.Location = New System.Drawing.Point(124, 32)
        Me.cboWorkingMode.Name = "cboWorkingMode"
        Me.cboWorkingMode.Size = New System.Drawing.Size(140, 21)
        Me.cboWorkingMode.TabIndex = 3
        '
        'lblWorkingMode
        '
        Me.lblWorkingMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkingMode.Location = New System.Drawing.Point(8, 33)
        Me.lblWorkingMode.Name = "lblWorkingMode"
        Me.lblWorkingMode.Size = New System.Drawing.Size(110, 16)
        Me.lblWorkingMode.TabIndex = 2
        Me.lblWorkingMode.Text = "Working Mode"
        Me.lblWorkingMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(505, 60)
        Me.eZeeHeader.TabIndex = 12
        Me.eZeeHeader.Title = "General Settings"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 298)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(505, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(293, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(396, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnInstructions
        '
        Me.objbtnInstructions.BackColor = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnInstructions.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnInstructions.BorderSelected = False
        Me.objbtnInstructions.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnInstructions.Image = CType(resources.GetObject("objbtnInstructions.Image"), System.Drawing.Image)
        Me.objbtnInstructions.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnInstructions.Location = New System.Drawing.Point(190, 114)
        Me.objbtnInstructions.Name = "objbtnInstructions"
        Me.objbtnInstructions.Size = New System.Drawing.Size(21, 21)
        Me.objbtnInstructions.TabIndex = 28
        '
        'frmGeneralSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 353)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGeneralSettings"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "General Settings"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcGeneralInfomation.ResumeLayout(False)
        Me.tabpGeneralSettings.ResumeLayout(False)
        Me.gbReaderType.ResumeLayout(False)
        CType(Me.nudDelayTime, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPortSettings.ResumeLayout(False)
        Me.tabpClientServer.ResumeLayout(False)
        CType(Me.picClientServer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbClientServerSettings.ResumeLayout(False)
        Me.gbClientServerSettings.PerformLayout()
        CType(Me.nudRefreshInterval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents tabcGeneralInfomation As System.Windows.Forms.TabControl
    Friend WithEvents tabpGeneralSettings As System.Windows.Forms.TabPage
    Friend WithEvents tabpClientServer As System.Windows.Forms.TabPage
    Friend WithEvents gbClientServerSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblWorkingMode As System.Windows.Forms.Label
    Friend WithEvents cboWorkingMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblServer As System.Windows.Forms.Label
    Friend WithEvents txtServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents nudRefreshInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblInterval As System.Windows.Forms.Label
    Friend WithEvents lblMinutes As System.Windows.Forms.Label
    Friend WithEvents gbPortSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents lblStopBits As System.Windows.Forms.Label
    Friend WithEvents lblParity As System.Windows.Forms.Label
    Friend WithEvents lblDataBits As System.Windows.Forms.Label
    Friend WithEvents lblBitPerSettings As System.Windows.Forms.Label
    Friend WithEvents cboPort As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopBits As System.Windows.Forms.ComboBox
    Friend WithEvents cboParity As System.Windows.Forms.ComboBox
    Friend WithEvents cboDataBits As System.Windows.Forms.ComboBox
    Friend WithEvents cboBitPerSecond As System.Windows.Forms.ComboBox
    Friend WithEvents gbReaderType As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblReaderType As System.Windows.Forms.Label
    Friend WithEvents cboReaderType As System.Windows.Forms.ComboBox
    Friend WithEvents nudDelayTime As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDelayTime As System.Windows.Forms.Label
    Friend WithEvents lblDelaySec As System.Windows.Forms.Label
    Friend WithEvents picClientServer As System.Windows.Forms.PictureBox
    Friend WithEvents lblChanges As System.Windows.Forms.Label
    Friend WithEvents lblAutoUpdateServer As System.Windows.Forms.Label
    Friend WithEvents txtAutoUpdateServer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSQLPort As eZee.TextBox.NumericTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objbtnInstructions As eZee.Common.eZeeGradientButton
End Class

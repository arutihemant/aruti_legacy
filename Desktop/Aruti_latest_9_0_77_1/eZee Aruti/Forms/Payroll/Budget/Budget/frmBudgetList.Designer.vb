﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetList))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuBudgetApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSetAsDefault = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClearDefault = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbEmployeeExempetion = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnSearchBudget = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboBudget = New System.Windows.Forms.ComboBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.dgvBudgetList = New System.Windows.Forms.DataGridView
        Me.objcolhBudgetUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBudgetCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBudgetName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPayyearUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPayyear = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhViewById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhViewBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAllocationById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAllocationBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPresentationmodeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPresentationmode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhWhotoIncludeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhWhotoInclude = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSalaryLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSalaryLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsApproved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPlannedEmployeeMapping = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.gbEmployeeExempetion.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBudgetList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 497)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(863, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 124
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBudgetApproval, Me.ToolStripSeparator1, Me.mnuSetAsDefault, Me.mnuClearDefault, Me.ToolStripMenuItem1, Me.mnuPlannedEmployeeMapping})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(224, 126)
        '
        'mnuBudgetApproval
        '
        Me.mnuBudgetApproval.Name = "mnuBudgetApproval"
        Me.mnuBudgetApproval.Size = New System.Drawing.Size(223, 22)
        Me.mnuBudgetApproval.Text = "&Budget Approval"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(220, 6)
        '
        'mnuSetAsDefault
        '
        Me.mnuSetAsDefault.Name = "mnuSetAsDefault"
        Me.mnuSetAsDefault.Size = New System.Drawing.Size(223, 22)
        Me.mnuSetAsDefault.Text = "Set as &Default"
        '
        'mnuClearDefault
        '
        Me.mnuClearDefault.Name = "mnuClearDefault"
        Me.mnuClearDefault.Size = New System.Drawing.Size(223, 22)
        Me.mnuClearDefault.Text = "&Clear Default"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(666, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(570, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(474, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(762, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(863, 60)
        Me.eZeeHeader.TabIndex = 4
        Me.eZeeHeader.Title = "Budget List"
        '
        'gbEmployeeExempetion
        '
        Me.gbEmployeeExempetion.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeExempetion.Checked = False
        Me.gbEmployeeExempetion.CollapseAllExceptThis = False
        Me.gbEmployeeExempetion.CollapsedHoverImage = Nothing
        Me.gbEmployeeExempetion.CollapsedNormalImage = Nothing
        Me.gbEmployeeExempetion.CollapsedPressedImage = Nothing
        Me.gbEmployeeExempetion.CollapseOnLoad = False
        Me.gbEmployeeExempetion.Controls.Add(Me.cboStatus)
        Me.gbEmployeeExempetion.Controls.Add(Me.lblStatus)
        Me.gbEmployeeExempetion.Controls.Add(Me.objbtnSearchBudget)
        Me.gbEmployeeExempetion.Controls.Add(Me.objbtnReset)
        Me.gbEmployeeExempetion.Controls.Add(Me.objbtnSearch)
        Me.gbEmployeeExempetion.Controls.Add(Me.cboBudget)
        Me.gbEmployeeExempetion.Controls.Add(Me.lblBudget)
        Me.gbEmployeeExempetion.ExpandedHoverImage = Nothing
        Me.gbEmployeeExempetion.ExpandedNormalImage = Nothing
        Me.gbEmployeeExempetion.ExpandedPressedImage = Nothing
        Me.gbEmployeeExempetion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeExempetion.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeExempetion.HeaderHeight = 25
        Me.gbEmployeeExempetion.HeaderMessage = ""
        Me.gbEmployeeExempetion.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeExempetion.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeExempetion.HeightOnCollapse = 0
        Me.gbEmployeeExempetion.LeftTextSpace = 0
        Me.gbEmployeeExempetion.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeExempetion.Name = "gbEmployeeExempetion"
        Me.gbEmployeeExempetion.OpenHeight = 91
        Me.gbEmployeeExempetion.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeExempetion.ShowBorder = True
        Me.gbEmployeeExempetion.ShowCheckBox = False
        Me.gbEmployeeExempetion.ShowCollapseButton = False
        Me.gbEmployeeExempetion.ShowDefaultBorderColor = True
        Me.gbEmployeeExempetion.ShowDownButton = False
        Me.gbEmployeeExempetion.ShowHeader = True
        Me.gbEmployeeExempetion.Size = New System.Drawing.Size(843, 64)
        Me.gbEmployeeExempetion.TabIndex = 5
        Me.gbEmployeeExempetion.Temp = 0
        Me.gbEmployeeExempetion.Text = "Filter Criteria"
        Me.gbEmployeeExempetion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(620, 34)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(192, 21)
        Me.cboStatus.TabIndex = 150
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(512, 37)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(102, 15)
        Me.lblStatus.TabIndex = 151
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchBudget
        '
        Me.objbtnSearchBudget.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBudget.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBudget.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBudget.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBudget.BorderSelected = False
        Me.objbtnSearchBudget.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBudget.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBudget.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBudget.Location = New System.Drawing.Point(314, 33)
        Me.objbtnSearchBudget.Name = "objbtnSearchBudget"
        Me.objbtnSearchBudget.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBudget.TabIndex = 148
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(811, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(788, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboBudget
        '
        Me.cboBudget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudget.FormattingEnabled = True
        Me.cboBudget.Location = New System.Drawing.Point(116, 33)
        Me.cboBudget.Name = "cboBudget"
        Me.cboBudget.Size = New System.Drawing.Size(192, 21)
        Me.cboBudget.TabIndex = 0
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(8, 36)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(102, 15)
        Me.lblBudget.TabIndex = 85
        Me.lblBudget.Text = "Budget"
        Me.lblBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvBudgetList
        '
        Me.dgvBudgetList.AllowUserToAddRows = False
        Me.dgvBudgetList.AllowUserToDeleteRows = False
        Me.dgvBudgetList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBudgetList.BackgroundColor = System.Drawing.Color.White
        Me.dgvBudgetList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudgetList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhBudgetUnkid, Me.colhBudgetCode, Me.colhBudgetName, Me.objcolhPayyearUnkid, Me.colhPayyear, Me.objcolhViewById, Me.colhViewBy, Me.objcolhAllocationById, Me.colhAllocationBy, Me.objcolhPresentationmodeId, Me.colhPresentationmode, Me.objcolhWhotoIncludeId, Me.colhWhotoInclude, Me.objcolhSalaryLevelId, Me.colhSalaryLevel, Me.colhIsDefault, Me.objcolhIsDefault, Me.colhIsApproved, Me.objcolhIsApproved})
        Me.dgvBudgetList.Location = New System.Drawing.Point(12, 136)
        Me.dgvBudgetList.Name = "dgvBudgetList"
        Me.dgvBudgetList.RowHeadersVisible = False
        Me.dgvBudgetList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBudgetList.Size = New System.Drawing.Size(843, 355)
        Me.dgvBudgetList.TabIndex = 6
        '
        'objcolhBudgetUnkid
        '
        Me.objcolhBudgetUnkid.HeaderText = "BudgetUnkid"
        Me.objcolhBudgetUnkid.Name = "objcolhBudgetUnkid"
        Me.objcolhBudgetUnkid.ReadOnly = True
        Me.objcolhBudgetUnkid.Visible = False
        '
        'colhBudgetCode
        '
        Me.colhBudgetCode.HeaderText = "Budget Code"
        Me.colhBudgetCode.Name = "colhBudgetCode"
        Me.colhBudgetCode.ReadOnly = True
        Me.colhBudgetCode.Width = 70
        '
        'colhBudgetName
        '
        Me.colhBudgetName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhBudgetName.HeaderText = "Budget Name"
        Me.colhBudgetName.Name = "colhBudgetName"
        Me.colhBudgetName.ReadOnly = True
        '
        'objcolhPayyearUnkid
        '
        Me.objcolhPayyearUnkid.HeaderText = "PayyearUnkid"
        Me.objcolhPayyearUnkid.Name = "objcolhPayyearUnkid"
        Me.objcolhPayyearUnkid.ReadOnly = True
        Me.objcolhPayyearUnkid.Visible = False
        '
        'colhPayyear
        '
        Me.colhPayyear.HeaderText = "Pay Year"
        Me.colhPayyear.Name = "colhPayyear"
        Me.colhPayyear.ReadOnly = True
        '
        'objcolhViewById
        '
        Me.objcolhViewById.HeaderText = "ViewById"
        Me.objcolhViewById.Name = "objcolhViewById"
        Me.objcolhViewById.ReadOnly = True
        Me.objcolhViewById.Visible = False
        '
        'colhViewBy
        '
        Me.colhViewBy.HeaderText = "View By"
        Me.colhViewBy.Name = "colhViewBy"
        Me.colhViewBy.ReadOnly = True
        '
        'objcolhAllocationById
        '
        Me.objcolhAllocationById.HeaderText = "AllocationById"
        Me.objcolhAllocationById.Name = "objcolhAllocationById"
        Me.objcolhAllocationById.ReadOnly = True
        Me.objcolhAllocationById.Visible = False
        '
        'colhAllocationBy
        '
        Me.colhAllocationBy.HeaderText = "Allocation By"
        Me.colhAllocationBy.Name = "colhAllocationBy"
        Me.colhAllocationBy.ReadOnly = True
        '
        'objcolhPresentationmodeId
        '
        Me.objcolhPresentationmodeId.HeaderText = "PresentationmodeId"
        Me.objcolhPresentationmodeId.Name = "objcolhPresentationmodeId"
        Me.objcolhPresentationmodeId.ReadOnly = True
        Me.objcolhPresentationmodeId.Visible = False
        '
        'colhPresentationmode
        '
        Me.colhPresentationmode.HeaderText = "Presentation Mode"
        Me.colhPresentationmode.Name = "colhPresentationmode"
        Me.colhPresentationmode.ReadOnly = True
        '
        'objcolhWhotoIncludeId
        '
        Me.objcolhWhotoIncludeId.HeaderText = "WhotoIncludeId"
        Me.objcolhWhotoIncludeId.Name = "objcolhWhotoIncludeId"
        Me.objcolhWhotoIncludeId.ReadOnly = True
        Me.objcolhWhotoIncludeId.Visible = False
        '
        'colhWhotoInclude
        '
        Me.colhWhotoInclude.HeaderText = "Who To Include"
        Me.colhWhotoInclude.Name = "colhWhotoInclude"
        Me.colhWhotoInclude.ReadOnly = True
        Me.colhWhotoInclude.Width = 135
        '
        'objcolhSalaryLevelId
        '
        Me.objcolhSalaryLevelId.HeaderText = "SalaryLevelId"
        Me.objcolhSalaryLevelId.Name = "objcolhSalaryLevelId"
        Me.objcolhSalaryLevelId.ReadOnly = True
        Me.objcolhSalaryLevelId.Visible = False
        '
        'colhSalaryLevel
        '
        Me.colhSalaryLevel.HeaderText = "Salary Level"
        Me.colhSalaryLevel.Name = "colhSalaryLevel"
        Me.colhSalaryLevel.ReadOnly = True
        Me.colhSalaryLevel.Visible = False
        '
        'colhIsDefault
        '
        Me.colhIsDefault.HeaderText = "Is Default"
        Me.colhIsDefault.Name = "colhIsDefault"
        Me.colhIsDefault.ReadOnly = True
        Me.colhIsDefault.Width = 60
        '
        'objcolhIsDefault
        '
        Me.objcolhIsDefault.HeaderText = "IsDefault"
        Me.objcolhIsDefault.Name = "objcolhIsDefault"
        Me.objcolhIsDefault.ReadOnly = True
        Me.objcolhIsDefault.Visible = False
        '
        'colhIsApproved
        '
        Me.colhIsApproved.HeaderText = "Approved"
        Me.colhIsApproved.Name = "colhIsApproved"
        Me.colhIsApproved.ReadOnly = True
        Me.colhIsApproved.Width = 60
        '
        'objcolhIsApproved
        '
        Me.objcolhIsApproved.HeaderText = "Approved"
        Me.objcolhIsApproved.Name = "objcolhIsApproved"
        Me.objcolhIsApproved.ReadOnly = True
        Me.objcolhIsApproved.Visible = False
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(220, 6)
        '
        'mnuPlannedEmployeeMapping
        '
        Me.mnuPlannedEmployeeMapping.Name = "mnuPlannedEmployeeMapping"
        Me.mnuPlannedEmployeeMapping.Size = New System.Drawing.Size(223, 22)
        Me.mnuPlannedEmployeeMapping.Text = "Planned Employee Mapping"
        '
        'frmBudgetList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(863, 552)
        Me.Controls.Add(Me.dgvBudgetList)
        Me.Controls.Add(Me.gbEmployeeExempetion)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget List"
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.gbEmployeeExempetion.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBudgetList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbEmployeeExempetion As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchBudget As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboBudget As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents dgvBudgetList As System.Windows.Forms.DataGridView
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSetAsDefault As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClearDefault As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBudgetApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objcolhBudgetUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBudgetCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBudgetName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPayyearUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPayyear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhViewById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhViewBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAllocationById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAllocationBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPresentationmodeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPresentationmode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhWhotoIncludeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhWhotoInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSalaryLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSalaryLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPlannedEmployeeMapping As System.Windows.Forms.ToolStripMenuItem
End Class

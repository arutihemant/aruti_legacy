﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBudgetAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBudgetAddEdit))
        Me.gbBudget = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.dtpBudgetDate = New System.Windows.Forms.DateTimePicker
        Me.lblBudgetDate = New System.Windows.Forms.Label
        Me.txtBudgetName = New eZee.TextBox.AlphanumericTextBox
        Me.chkOverWrite = New System.Windows.Forms.CheckBox
        Me.lblBudgetName = New System.Windows.Forms.Label
        Me.txtSearchHead = New eZee.TextBox.AlphanumericTextBox
        Me.txtBudgetCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblBudgetCode = New System.Windows.Forms.Label
        Me.cboBudgetYear = New System.Windows.Forms.ComboBox
        Me.lblBudgetYear = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.btnGenerate = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboSalaryLevel = New System.Windows.Forms.ComboBox
        Me.lblSalaryLevel = New System.Windows.Forms.Label
        Me.dgvTranHead = New System.Windows.Forms.DataGridView
        Me.objcolhIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhTranheadUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranHeadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranHeadName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhHeadType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFormulaMappingHead = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.objcolhFormulaMappingHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsSalary = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhFormulaid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFormula = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboWhoToInclude = New System.Windows.Forms.ComboBox
        Me.lblWhoToInclude = New System.Windows.Forms.Label
        Me.cboPresentation = New System.Windows.Forms.ComboBox
        Me.lblPresentation = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.lblYear = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objchkSelectAll_Period = New System.Windows.Forms.CheckBox
        Me.lvPeriod = New System.Windows.Forms.ListView
        Me.objcolh_Period_Check = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_PayPeriod = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_StartDate = New System.Windows.Forms.ColumnHeader
        Me.colh_Period_EndDate = New System.Windows.Forms.ColumnHeader
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.objchkSelectAll_Budget = New System.Windows.Forms.CheckBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuResetBudget = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuExportPercentageAllocation = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportPercentageAllocation = New System.Windows.Forms.ToolStripMenuItem
        Me.objlblGrandTotal = New System.Windows.Forms.Label
        Me.objlblBudgetTotal = New System.Windows.Forms.Label
        Me.lblGrandTotal = New System.Windows.Forms.Label
        Me.lblBudgetTotal = New System.Windows.Forms.Label
        Me.objlblOtherCostTotal = New System.Windows.Forms.Label
        Me.objlblTotalEmplyees = New System.Windows.Forms.Label
        Me.lblOtherCostTotal = New System.Windows.Forms.Label
        Me.lblTotalEmplyees = New System.Windows.Forms.Label
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cbOnScreenComputations = New System.Windows.Forms.GroupBox
        Me.cboProjectCode = New System.Windows.Forms.ComboBox
        Me.lblProjectCode = New System.Windows.Forms.Label
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.radApplyToChecked = New System.Windows.Forms.RadioButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkAddToExisting = New System.Windows.Forms.CheckBox
        Me.radPercentage = New System.Windows.Forms.RadioButton
        Me.txtValue = New eZee.TextBox.NumericTextBox
        Me.radFixedAmount = New System.Windows.Forms.RadioButton
        Me.cboAffectedColumns = New System.Windows.Forms.ComboBox
        Me.lblAffectedColumns = New System.Windows.Forms.Label
        Me.btnApply = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.txtTotalPercentage = New eZee.TextBox.NumericTextBox
        Me.lblTotalPercentage = New System.Windows.Forms.Label
        Me.gbBudgetData = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dgvBudget = New Aruti.Data.GroupByGrid
        Me.gbBudget.SuspendLayout()
        CType(Me.dgvTranHead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.mnuOperations.SuspendLayout()
        Me.cbOnScreenComputations.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbBudgetData.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBudget, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbBudget
        '
        Me.gbBudget.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbBudget.BorderColor = System.Drawing.Color.Black
        Me.gbBudget.Checked = False
        Me.gbBudget.CollapseAllExceptThis = False
        Me.gbBudget.CollapsedHoverImage = CType(resources.GetObject("gbBudget.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbBudget.CollapsedNormalImage = CType(resources.GetObject("gbBudget.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbBudget.CollapsedPressedImage = CType(resources.GetObject("gbBudget.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbBudget.CollapseOnLoad = False
        Me.gbBudget.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbBudget.Controls.Add(Me.dtpBudgetDate)
        Me.gbBudget.Controls.Add(Me.lblBudgetDate)
        Me.gbBudget.Controls.Add(Me.txtBudgetName)
        Me.gbBudget.Controls.Add(Me.chkOverWrite)
        Me.gbBudget.Controls.Add(Me.lblBudgetName)
        Me.gbBudget.Controls.Add(Me.txtSearchHead)
        Me.gbBudget.Controls.Add(Me.txtBudgetCode)
        Me.gbBudget.Controls.Add(Me.cboTrnHeadType)
        Me.gbBudget.Controls.Add(Me.lblBudgetCode)
        Me.gbBudget.Controls.Add(Me.cboBudgetYear)
        Me.gbBudget.Controls.Add(Me.lblBudgetYear)
        Me.gbBudget.Controls.Add(Me.lnkSetAnalysis)
        Me.gbBudget.Controls.Add(Me.btnGenerate)
        Me.gbBudget.Controls.Add(Me.cboSalaryLevel)
        Me.gbBudget.Controls.Add(Me.lblSalaryLevel)
        Me.gbBudget.Controls.Add(Me.dgvTranHead)
        Me.gbBudget.Controls.Add(Me.cboWhoToInclude)
        Me.gbBudget.Controls.Add(Me.lblWhoToInclude)
        Me.gbBudget.Controls.Add(Me.cboPresentation)
        Me.gbBudget.Controls.Add(Me.lblPresentation)
        Me.gbBudget.Controls.Add(Me.cboViewBy)
        Me.gbBudget.Controls.Add(Me.lblViewBy)
        Me.gbBudget.Controls.Add(Me.lblYear)
        Me.gbBudget.Controls.Add(Me.Panel2)
        Me.gbBudget.Controls.Add(Me.cboYear)
        Me.gbBudget.ExpandedHoverImage = CType(resources.GetObject("gbBudget.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbBudget.ExpandedNormalImage = CType(resources.GetObject("gbBudget.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbBudget.ExpandedPressedImage = CType(resources.GetObject("gbBudget.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudget.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudget.HeaderHeight = 25
        Me.gbBudget.HeaderMessage = ""
        Me.gbBudget.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudget.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudget.HeightOnCollapse = 0
        Me.gbBudget.LeftTextSpace = 0
        Me.gbBudget.Location = New System.Drawing.Point(0, 0)
        Me.gbBudget.Name = "gbBudget"
        Me.gbBudget.OpenHeight = 223
        Me.gbBudget.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudget.ShowBorder = True
        Me.gbBudget.ShowCheckBox = False
        Me.gbBudget.ShowCollapseButton = True
        Me.gbBudget.ShowDefaultBorderColor = True
        Me.gbBudget.ShowDownButton = False
        Me.gbBudget.ShowHeader = True
        Me.gbBudget.Size = New System.Drawing.Size(1005, 223)
        Me.gbBudget.TabIndex = 0
        Me.gbBudget.Temp = 0
        Me.gbBudget.Text = "Budget Information"
        Me.gbBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(247, 59)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(22, 22)
        Me.objbtnOtherLanguage.TabIndex = 188
        '
        'dtpBudgetDate
        '
        Me.dtpBudgetDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBudgetDate.Checked = False
        Me.dtpBudgetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpBudgetDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpBudgetDate.Location = New System.Drawing.Point(102, 194)
        Me.dtpBudgetDate.Name = "dtpBudgetDate"
        Me.dtpBudgetDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpBudgetDate.TabIndex = 188
        '
        'lblBudgetDate
        '
        Me.lblBudgetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetDate.Location = New System.Drawing.Point(13, 196)
        Me.lblBudgetDate.Name = "lblBudgetDate"
        Me.lblBudgetDate.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetDate.TabIndex = 191
        Me.lblBudgetDate.Text = "Budget Date"
        Me.lblBudgetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBudgetName
        '
        Me.txtBudgetName.Flags = 0
        Me.txtBudgetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetName.Location = New System.Drawing.Point(102, 59)
        Me.txtBudgetName.Name = "txtBudgetName"
        Me.txtBudgetName.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetName.TabIndex = 1
        '
        'chkOverWrite
        '
        Me.chkOverWrite.Location = New System.Drawing.Point(277, 183)
        Me.chkOverWrite.Name = "chkOverWrite"
        Me.chkOverWrite.Size = New System.Drawing.Size(114, 37)
        Me.chkOverWrite.TabIndex = 188
        Me.chkOverWrite.Text = "Overwrite New Budget Figure"
        Me.chkOverWrite.UseVisualStyleBackColor = True
        '
        'lblBudgetName
        '
        Me.lblBudgetName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetName.Location = New System.Drawing.Point(13, 60)
        Me.lblBudgetName.Name = "lblBudgetName"
        Me.lblBudgetName.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetName.TabIndex = 187
        Me.lblBudgetName.Text = "Budget Name"
        Me.lblBudgetName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearchHead
        '
        Me.txtSearchHead.Flags = 0
        Me.txtSearchHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchHead.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchHead.Location = New System.Drawing.Point(684, 32)
        Me.txtSearchHead.MaxLength = 50
        Me.txtSearchHead.Name = "txtSearchHead"
        Me.txtSearchHead.Size = New System.Drawing.Size(317, 21)
        Me.txtSearchHead.TabIndex = 8
        '
        'txtBudgetCode
        '
        Me.txtBudgetCode.Flags = 0
        Me.txtBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBudgetCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBudgetCode.Location = New System.Drawing.Point(102, 32)
        Me.txtBudgetCode.Name = "txtBudgetCode"
        Me.txtBudgetCode.Size = New System.Drawing.Size(139, 21)
        Me.txtBudgetCode.TabIndex = 0
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(501, 32)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(177, 21)
        Me.cboTrnHeadType.TabIndex = 189
        '
        'lblBudgetCode
        '
        Me.lblBudgetCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetCode.Location = New System.Drawing.Point(13, 33)
        Me.lblBudgetCode.Name = "lblBudgetCode"
        Me.lblBudgetCode.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetCode.TabIndex = 184
        Me.lblBudgetCode.Text = "Budget Code"
        Me.lblBudgetCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBudgetYear
        '
        Me.cboBudgetYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudgetYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudgetYear.FormattingEnabled = True
        Me.cboBudgetYear.Location = New System.Drawing.Point(102, 86)
        Me.cboBudgetYear.Name = "cboBudgetYear"
        Me.cboBudgetYear.Size = New System.Drawing.Size(139, 21)
        Me.cboBudgetYear.TabIndex = 2
        '
        'lblBudgetYear
        '
        Me.lblBudgetYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetYear.Location = New System.Drawing.Point(13, 87)
        Me.lblBudgetYear.Name = "lblBudgetYear"
        Me.lblBudgetYear.Size = New System.Drawing.Size(83, 16)
        Me.lblBudgetYear.TabIndex = 180
        Me.lblBudgetYear.Text = "Budget Year"
        Me.lblBudgetYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(845, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(128, 17)
        Me.lnkSetAnalysis.TabIndex = 177
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Allocation / Employee"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnGenerate
        '
        Me.btnGenerate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGenerate.BackColor = System.Drawing.Color.White
        Me.btnGenerate.BackgroundImage = CType(resources.GetObject("btnGenerate.BackgroundImage"), System.Drawing.Image)
        Me.btnGenerate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGenerate.BorderColor = System.Drawing.Color.Empty
        Me.btnGenerate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnGenerate.FlatAppearance.BorderSize = 0
        Me.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.ForeColor = System.Drawing.Color.Black
        Me.btnGenerate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGenerate.GradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.Location = New System.Drawing.Point(397, 187)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGenerate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGenerate.Size = New System.Drawing.Size(94, 30)
        Me.btnGenerate.TabIndex = 175
        Me.btnGenerate.Text = "&Generate"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'cboSalaryLevel
        '
        Me.cboSalaryLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryLevel.FormattingEnabled = True
        Me.cboSalaryLevel.Location = New System.Drawing.Point(199, 194)
        Me.cboSalaryLevel.Name = "cboSalaryLevel"
        Me.cboSalaryLevel.Size = New System.Drawing.Size(42, 21)
        Me.cboSalaryLevel.TabIndex = 6
        Me.cboSalaryLevel.Visible = False
        '
        'lblSalaryLevel
        '
        Me.lblSalaryLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryLevel.Location = New System.Drawing.Point(202, 183)
        Me.lblSalaryLevel.Name = "lblSalaryLevel"
        Me.lblSalaryLevel.Size = New System.Drawing.Size(42, 16)
        Me.lblSalaryLevel.TabIndex = 166
        Me.lblSalaryLevel.Text = "Salary Level"
        Me.lblSalaryLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblSalaryLevel.Visible = False
        '
        'dgvTranHead
        '
        Me.dgvTranHead.AllowUserToAddRows = False
        Me.dgvTranHead.AllowUserToDeleteRows = False
        Me.dgvTranHead.BackgroundColor = System.Drawing.Color.White
        Me.dgvTranHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTranHead.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhIsCheck, Me.objcolhTranheadUnkId, Me.colhTranHeadCode, Me.colhTranHeadName, Me.colhHeadType, Me.colhFormulaMappingHead, Me.objcolhFormulaMappingHead, Me.colhIsSalary, Me.objcolhFormulaid, Me.objcolhFormula})
        Me.dgvTranHead.Location = New System.Drawing.Point(501, 59)
        Me.dgvTranHead.Name = "dgvTranHead"
        Me.dgvTranHead.RowHeadersVisible = False
        Me.dgvTranHead.Size = New System.Drawing.Size(501, 159)
        Me.dgvTranHead.TabIndex = 9
        '
        'objcolhIsCheck
        '
        Me.objcolhIsCheck.HeaderText = ""
        Me.objcolhIsCheck.Name = "objcolhIsCheck"
        Me.objcolhIsCheck.Width = 25
        '
        'objcolhTranheadUnkId
        '
        Me.objcolhTranheadUnkId.HeaderText = "Tran. Head Unk Id"
        Me.objcolhTranheadUnkId.Name = "objcolhTranheadUnkId"
        Me.objcolhTranheadUnkId.ReadOnly = True
        Me.objcolhTranheadUnkId.Visible = False
        '
        'colhTranHeadCode
        '
        Me.colhTranHeadCode.HeaderText = "Head Code"
        Me.colhTranHeadCode.Name = "colhTranHeadCode"
        Me.colhTranHeadCode.ReadOnly = True
        Me.colhTranHeadCode.Width = 85
        '
        'colhTranHeadName
        '
        Me.colhTranHeadName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhTranHeadName.HeaderText = "Tran. Head Name"
        Me.colhTranHeadName.Name = "colhTranHeadName"
        Me.colhTranHeadName.ReadOnly = True
        '
        'colhHeadType
        '
        Me.colhHeadType.HeaderText = "Head Type"
        Me.colhHeadType.Name = "colhHeadType"
        Me.colhHeadType.ReadOnly = True
        '
        'colhFormulaMappingHead
        '
        Me.colhFormulaMappingHead.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.[Nothing]
        Me.colhFormulaMappingHead.HeaderText = "Budget Head"
        Me.colhFormulaMappingHead.Name = "colhFormulaMappingHead"
        Me.colhFormulaMappingHead.Width = 115
        '
        'objcolhFormulaMappingHead
        '
        Me.objcolhFormulaMappingHead.HeaderText = "objcolhFormulaMappingHead"
        Me.objcolhFormulaMappingHead.Name = "objcolhFormulaMappingHead"
        Me.objcolhFormulaMappingHead.ReadOnly = True
        Me.objcolhFormulaMappingHead.Visible = False
        '
        'colhIsSalary
        '
        Me.colhIsSalary.HeaderText = "Is Salary"
        Me.colhIsSalary.Name = "colhIsSalary"
        Me.colhIsSalary.ReadOnly = True
        Me.colhIsSalary.Width = 55
        '
        'objcolhFormulaid
        '
        Me.objcolhFormulaid.HeaderText = "Formulaid"
        Me.objcolhFormulaid.Name = "objcolhFormulaid"
        Me.objcolhFormulaid.ReadOnly = True
        Me.objcolhFormulaid.Visible = False
        '
        'objcolhFormula
        '
        Me.objcolhFormula.HeaderText = "Formula"
        Me.objcolhFormula.Name = "objcolhFormula"
        Me.objcolhFormula.ReadOnly = True
        Me.objcolhFormula.Visible = False
        '
        'cboWhoToInclude
        '
        Me.cboWhoToInclude.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWhoToInclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboWhoToInclude.FormattingEnabled = True
        Me.cboWhoToInclude.Location = New System.Drawing.Point(102, 167)
        Me.cboWhoToInclude.Name = "cboWhoToInclude"
        Me.cboWhoToInclude.Size = New System.Drawing.Size(139, 21)
        Me.cboWhoToInclude.TabIndex = 5
        '
        'lblWhoToInclude
        '
        Me.lblWhoToInclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWhoToInclude.Location = New System.Drawing.Point(13, 168)
        Me.lblWhoToInclude.Name = "lblWhoToInclude"
        Me.lblWhoToInclude.Size = New System.Drawing.Size(83, 16)
        Me.lblWhoToInclude.TabIndex = 164
        Me.lblWhoToInclude.Text = "Who to Include"
        Me.lblWhoToInclude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPresentation
        '
        Me.cboPresentation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPresentation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPresentation.FormattingEnabled = True
        Me.cboPresentation.Location = New System.Drawing.Point(102, 140)
        Me.cboPresentation.Name = "cboPresentation"
        Me.cboPresentation.Size = New System.Drawing.Size(139, 21)
        Me.cboPresentation.TabIndex = 4
        '
        'lblPresentation
        '
        Me.lblPresentation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPresentation.Location = New System.Drawing.Point(13, 140)
        Me.lblPresentation.Name = "lblPresentation"
        Me.lblPresentation.Size = New System.Drawing.Size(83, 16)
        Me.lblPresentation.TabIndex = 162
        Me.lblPresentation.Text = "Presentation"
        Me.lblPresentation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(102, 113)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(139, 21)
        Me.cboViewBy.TabIndex = 3
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(13, 114)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(83, 16)
        Me.lblViewBy.TabIndex = 160
        Me.lblViewBy.Text = "Budget For"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(277, 34)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(102, 16)
        Me.lblYear.TabIndex = 12
        Me.lblYear.Text = "Previous Periods"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objchkSelectAll_Period)
        Me.Panel2.Controls.Add(Me.lvPeriod)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(277, 61)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(214, 117)
        Me.Panel2.TabIndex = 158
        '
        'objchkSelectAll_Period
        '
        Me.objchkSelectAll_Period.AutoSize = True
        Me.objchkSelectAll_Period.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll_Period.Name = "objchkSelectAll_Period"
        Me.objchkSelectAll_Period.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll_Period.TabIndex = 163
        Me.objchkSelectAll_Period.UseVisualStyleBackColor = True
        '
        'lvPeriod
        '
        Me.lvPeriod.CheckBoxes = True
        Me.lvPeriod.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolh_Period_Check, Me.colh_Period_PayPeriod, Me.colh_Period_StartDate, Me.colh_Period_EndDate})
        Me.lvPeriod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPeriod.GridLines = True
        Me.lvPeriod.Location = New System.Drawing.Point(0, 0)
        Me.lvPeriod.Name = "lvPeriod"
        Me.lvPeriod.Size = New System.Drawing.Size(214, 117)
        Me.lvPeriod.TabIndex = 0
        Me.lvPeriod.UseCompatibleStateImageBehavior = False
        Me.lvPeriod.View = System.Windows.Forms.View.Details
        '
        'objcolh_Period_Check
        '
        Me.objcolh_Period_Check.Text = ""
        Me.objcolh_Period_Check.Width = 30
        '
        'colh_Period_PayPeriod
        '
        Me.colh_Period_PayPeriod.Tag = "colh_Period_PayPeriod"
        Me.colh_Period_PayPeriod.Text = "Previous Periods"
        Me.colh_Period_PayPeriod.Width = 150
        '
        'colh_Period_StartDate
        '
        Me.colh_Period_StartDate.Tag = "colh_Period_StartDate"
        Me.colh_Period_StartDate.Text = "Start Date"
        Me.colh_Period_StartDate.Width = 0
        '
        'colh_Period_EndDate
        '
        Me.colh_Period_EndDate.Tag = "colh_Period_EndDate"
        Me.colh_Period_EndDate.Text = "End Date"
        Me.colh_Period_EndDate.Width = 0
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(385, 32)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(106, 21)
        Me.cboYear.TabIndex = 7
        '
        'objchkSelectAll_Budget
        '
        Me.objchkSelectAll_Budget.AutoSize = True
        Me.objchkSelectAll_Budget.Location = New System.Drawing.Point(8, 118)
        Me.objchkSelectAll_Budget.Name = "objchkSelectAll_Budget"
        Me.objchkSelectAll_Budget.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll_Budget.TabIndex = 186
        Me.objchkSelectAll_Budget.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.objlblGrandTotal)
        Me.objFooter.Controls.Add(Me.objlblBudgetTotal)
        Me.objFooter.Controls.Add(Me.lblGrandTotal)
        Me.objFooter.Controls.Add(Me.lblBudgetTotal)
        Me.objFooter.Controls.Add(Me.objlblOtherCostTotal)
        Me.objFooter.Controls.Add(Me.objlblTotalEmplyees)
        Me.objFooter.Controls.Add(Me.lblOtherCostTotal)
        Me.objFooter.Controls.Add(Me.lblTotalEmplyees)
        Me.objFooter.Controls.Add(Me.btnReset)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 528)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1017, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(10, 9)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(108, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperations
        Me.btnOperations.TabIndex = 190
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperations
        '
        Me.mnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuResetBudget, Me.ToolStripSeparator1, Me.mnuExportPercentageAllocation, Me.mnuImportPercentageAllocation})
        Me.mnuOperations.Name = "mnuOperations"
        Me.mnuOperations.Size = New System.Drawing.Size(230, 76)
        '
        'mnuResetBudget
        '
        Me.mnuResetBudget.Name = "mnuResetBudget"
        Me.mnuResetBudget.Size = New System.Drawing.Size(229, 22)
        Me.mnuResetBudget.Text = "&Reset Budget"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(226, 6)
        '
        'mnuExportPercentageAllocation
        '
        Me.mnuExportPercentageAllocation.Name = "mnuExportPercentageAllocation"
        Me.mnuExportPercentageAllocation.Size = New System.Drawing.Size(229, 22)
        Me.mnuExportPercentageAllocation.Text = "&Export Percentage Allocation"
        '
        'mnuImportPercentageAllocation
        '
        Me.mnuImportPercentageAllocation.Name = "mnuImportPercentageAllocation"
        Me.mnuImportPercentageAllocation.Size = New System.Drawing.Size(229, 22)
        Me.mnuImportPercentageAllocation.Text = "&Import Percentage Allocation"
        '
        'objlblGrandTotal
        '
        Me.objlblGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblGrandTotal.ForeColor = System.Drawing.Color.Purple
        Me.objlblGrandTotal.Location = New System.Drawing.Point(612, 29)
        Me.objlblGrandTotal.Name = "objlblGrandTotal"
        Me.objlblGrandTotal.Size = New System.Drawing.Size(108, 16)
        Me.objlblGrandTotal.TabIndex = 199
        Me.objlblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblBudgetTotal
        '
        Me.objlblBudgetTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblBudgetTotal.ForeColor = System.Drawing.Color.Purple
        Me.objlblBudgetTotal.Location = New System.Drawing.Point(612, 9)
        Me.objlblBudgetTotal.Name = "objlblBudgetTotal"
        Me.objlblBudgetTotal.Size = New System.Drawing.Size(108, 16)
        Me.objlblBudgetTotal.TabIndex = 198
        Me.objlblBudgetTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblGrandTotal
        '
        Me.lblGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrandTotal.Location = New System.Drawing.Point(432, 29)
        Me.lblGrandTotal.Name = "lblGrandTotal"
        Me.lblGrandTotal.Size = New System.Drawing.Size(174, 16)
        Me.lblGrandTotal.TabIndex = 197
        Me.lblGrandTotal.Text = "New Budget Grand Total :"
        Me.lblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBudgetTotal
        '
        Me.lblBudgetTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudgetTotal.Location = New System.Drawing.Point(432, 9)
        Me.lblBudgetTotal.Name = "lblBudgetTotal"
        Me.lblBudgetTotal.Size = New System.Drawing.Size(174, 16)
        Me.lblBudgetTotal.TabIndex = 196
        Me.lblBudgetTotal.Text = "New Budget Total :"
        Me.lblBudgetTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblOtherCostTotal
        '
        Me.objlblOtherCostTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOtherCostTotal.ForeColor = System.Drawing.Color.Purple
        Me.objlblOtherCostTotal.Location = New System.Drawing.Point(322, 29)
        Me.objlblOtherCostTotal.Name = "objlblOtherCostTotal"
        Me.objlblOtherCostTotal.Size = New System.Drawing.Size(95, 16)
        Me.objlblOtherCostTotal.TabIndex = 195
        Me.objlblOtherCostTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblTotalEmplyees
        '
        Me.objlblTotalEmplyees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalEmplyees.ForeColor = System.Drawing.Color.Purple
        Me.objlblTotalEmplyees.Location = New System.Drawing.Point(322, 9)
        Me.objlblTotalEmplyees.Name = "objlblTotalEmplyees"
        Me.objlblTotalEmplyees.Size = New System.Drawing.Size(95, 16)
        Me.objlblTotalEmplyees.TabIndex = 194
        Me.objlblTotalEmplyees.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOtherCostTotal
        '
        Me.lblOtherCostTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherCostTotal.Location = New System.Drawing.Point(142, 29)
        Me.lblOtherCostTotal.Name = "lblOtherCostTotal"
        Me.lblOtherCostTotal.Size = New System.Drawing.Size(174, 16)
        Me.lblOtherCostTotal.TabIndex = 193
        Me.lblOtherCostTotal.Text = "New Other Costs Total :"
        Me.lblOtherCostTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotalEmplyees
        '
        Me.lblTotalEmplyees.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalEmplyees.Location = New System.Drawing.Point(142, 9)
        Me.lblTotalEmplyees.Name = "lblTotalEmplyees"
        Me.lblTotalEmplyees.Size = New System.Drawing.Size(174, 16)
        Me.lblTotalEmplyees.TabIndex = 192
        Me.lblTotalEmplyees.Text = "Number of Employees Budgeted: :"
        Me.lblTotalEmplyees.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(35, 9)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(94, 30)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        Me.btnReset.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(912, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(812, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cbOnScreenComputations
        '
        Me.cbOnScreenComputations.Controls.Add(Me.cboProjectCode)
        Me.cbOnScreenComputations.Controls.Add(Me.lblProjectCode)
        Me.cbOnScreenComputations.Controls.Add(Me.radApplyToAll)
        Me.cbOnScreenComputations.Controls.Add(Me.radApplyToChecked)
        Me.cbOnScreenComputations.Controls.Add(Me.Panel1)
        Me.cbOnScreenComputations.Controls.Add(Me.cboAffectedColumns)
        Me.cbOnScreenComputations.Controls.Add(Me.lblAffectedColumns)
        Me.cbOnScreenComputations.Controls.Add(Me.btnApply)
        Me.cbOnScreenComputations.ForeColor = System.Drawing.Color.Black
        Me.cbOnScreenComputations.Location = New System.Drawing.Point(3, 33)
        Me.cbOnScreenComputations.Name = "cbOnScreenComputations"
        Me.cbOnScreenComputations.Size = New System.Drawing.Size(993, 73)
        Me.cbOnScreenComputations.TabIndex = 187
        Me.cbOnScreenComputations.TabStop = False
        Me.cbOnScreenComputations.Text = "On Screen Computations"
        '
        'cboProjectCode
        '
        Me.cboProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProjectCode.FormattingEnabled = True
        Me.cboProjectCode.Location = New System.Drawing.Point(104, 46)
        Me.cboProjectCode.Name = "cboProjectCode"
        Me.cboProjectCode.Size = New System.Drawing.Size(118, 21)
        Me.cboProjectCode.TabIndex = 163
        '
        'lblProjectCode
        '
        Me.lblProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjectCode.Location = New System.Drawing.Point(4, 47)
        Me.lblProjectCode.Name = "lblProjectCode"
        Me.lblProjectCode.Size = New System.Drawing.Size(94, 16)
        Me.lblProjectCode.TabIndex = 164
        Me.lblProjectCode.Text = "Project Code"
        Me.lblProjectCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radApplyToAll
        '
        Me.radApplyToAll.Location = New System.Drawing.Point(655, 16)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(86, 24)
        Me.radApplyToAll.TabIndex = 2
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = True
        '
        'radApplyToChecked
        '
        Me.radApplyToChecked.Checked = True
        Me.radApplyToChecked.Location = New System.Drawing.Point(747, 16)
        Me.radApplyToChecked.Name = "radApplyToChecked"
        Me.radApplyToChecked.Size = New System.Drawing.Size(118, 24)
        Me.radApplyToChecked.TabIndex = 3
        Me.radApplyToChecked.TabStop = True
        Me.radApplyToChecked.Text = "Apply To Checked"
        Me.radApplyToChecked.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkAddToExisting)
        Me.Panel1.Controls.Add(Me.radPercentage)
        Me.Panel1.Controls.Add(Me.txtValue)
        Me.Panel1.Controls.Add(Me.radFixedAmount)
        Me.Panel1.Location = New System.Drawing.Point(228, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(422, 31)
        Me.Panel1.TabIndex = 1
        '
        'chkAddToExisting
        '
        Me.chkAddToExisting.Location = New System.Drawing.Point(314, 7)
        Me.chkAddToExisting.Name = "chkAddToExisting"
        Me.chkAddToExisting.Size = New System.Drawing.Size(104, 17)
        Me.chkAddToExisting.TabIndex = 2
        Me.chkAddToExisting.Text = "Add To Existing"
        Me.chkAddToExisting.UseVisualStyleBackColor = True
        '
        'radPercentage
        '
        Me.radPercentage.Location = New System.Drawing.Point(3, 3)
        Me.radPercentage.Name = "radPercentage"
        Me.radPercentage.Size = New System.Drawing.Size(84, 24)
        Me.radPercentage.TabIndex = 0
        Me.radPercentage.Text = "Percentage"
        Me.radPercentage.UseVisualStyleBackColor = True
        '
        'txtValue
        '
        Me.txtValue.AllowNegative = True
        Me.txtValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtValue.DigitsInGroup = 0
        Me.txtValue.Flags = 0
        Me.txtValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValue.Location = New System.Drawing.Point(195, 5)
        Me.txtValue.MaxDecimalPlaces = 6
        Me.txtValue.MaxWholeDigits = 21
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Prefix = ""
        Me.txtValue.RangeMax = 1.7976931348623157E+308
        Me.txtValue.RangeMin = -1.7976931348623157E+308
        Me.txtValue.Size = New System.Drawing.Size(113, 21)
        Me.txtValue.TabIndex = 1
        Me.txtValue.Text = "0"
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radFixedAmount
        '
        Me.radFixedAmount.Checked = True
        Me.radFixedAmount.Location = New System.Drawing.Point(93, 3)
        Me.radFixedAmount.Name = "radFixedAmount"
        Me.radFixedAmount.Size = New System.Drawing.Size(96, 24)
        Me.radFixedAmount.TabIndex = 163
        Me.radFixedAmount.TabStop = True
        Me.radFixedAmount.Text = "Fixed Amount"
        Me.radFixedAmount.UseVisualStyleBackColor = True
        '
        'cboAffectedColumns
        '
        Me.cboAffectedColumns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAffectedColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAffectedColumns.FormattingEnabled = True
        Me.cboAffectedColumns.Location = New System.Drawing.Point(104, 19)
        Me.cboAffectedColumns.Name = "cboAffectedColumns"
        Me.cboAffectedColumns.Size = New System.Drawing.Size(118, 21)
        Me.cboAffectedColumns.TabIndex = 0
        '
        'lblAffectedColumns
        '
        Me.lblAffectedColumns.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAffectedColumns.Location = New System.Drawing.Point(4, 19)
        Me.lblAffectedColumns.Name = "lblAffectedColumns"
        Me.lblAffectedColumns.Size = New System.Drawing.Size(94, 16)
        Me.lblAffectedColumns.TabIndex = 162
        Me.lblAffectedColumns.Text = "Affected Columns"
        Me.lblAffectedColumns.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.BackColor = System.Drawing.Color.White
        Me.btnApply.BackgroundImage = CType(resources.GetObject("btnApply.BackgroundImage"), System.Drawing.Image)
        Me.btnApply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApply.BorderColor = System.Drawing.Color.Empty
        Me.btnApply.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApply.FlatAppearance.BorderSize = 0
        Me.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.ForeColor = System.Drawing.Color.Black
        Me.btnApply.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApply.GradientForeColor = System.Drawing.Color.Black
        Me.btnApply.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Location = New System.Drawing.Point(893, 13)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApply.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApply.Size = New System.Drawing.Size(94, 30)
        Me.btnApply.TabIndex = 4
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'cboCondition
        '
        Me.cboCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Location = New System.Drawing.Point(714, 5)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(42, 21)
        Me.cboCondition.TabIndex = 167
        '
        'txtTotalPercentage
        '
        Me.txtTotalPercentage.AllowNegative = True
        Me.txtTotalPercentage.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPercentage.DigitsInGroup = 0
        Me.txtTotalPercentage.Flags = 0
        Me.txtTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPercentage.Location = New System.Drawing.Point(762, 5)
        Me.txtTotalPercentage.MaxDecimalPlaces = 2
        Me.txtTotalPercentage.MaxWholeDigits = 21
        Me.txtTotalPercentage.Name = "txtTotalPercentage"
        Me.txtTotalPercentage.Prefix = ""
        Me.txtTotalPercentage.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPercentage.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPercentage.Size = New System.Drawing.Size(60, 21)
        Me.txtTotalPercentage.TabIndex = 166
        Me.txtTotalPercentage.Text = "0"
        Me.txtTotalPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalPercentage
        '
        Me.lblTotalPercentage.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPercentage.Location = New System.Drawing.Point(652, 7)
        Me.lblTotalPercentage.Name = "lblTotalPercentage"
        Me.lblTotalPercentage.Size = New System.Drawing.Size(56, 16)
        Me.lblTotalPercentage.TabIndex = 165
        Me.lblTotalPercentage.Text = "Total %"
        Me.lblTotalPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBudgetData
        '
        Me.gbBudgetData.BorderColor = System.Drawing.Color.Black
        Me.gbBudgetData.Checked = False
        Me.gbBudgetData.CollapseAllExceptThis = False
        Me.gbBudgetData.CollapsedHoverImage = CType(resources.GetObject("gbBudgetData.CollapsedHoverImage"), System.Drawing.Image)
        Me.gbBudgetData.CollapsedNormalImage = CType(resources.GetObject("gbBudgetData.CollapsedNormalImage"), System.Drawing.Image)
        Me.gbBudgetData.CollapsedPressedImage = CType(resources.GetObject("gbBudgetData.CollapsedPressedImage"), System.Drawing.Image)
        Me.gbBudgetData.CollapseOnLoad = False
        Me.gbBudgetData.Controls.Add(Me.lnkAllocation)
        Me.gbBudgetData.Controls.Add(Me.cboCondition)
        Me.gbBudgetData.Controls.Add(Me.objbtnReset)
        Me.gbBudgetData.Controls.Add(Me.txtTotalPercentage)
        Me.gbBudgetData.Controls.Add(Me.objchkSelectAll_Budget)
        Me.gbBudgetData.Controls.Add(Me.objbtnSearch)
        Me.gbBudgetData.Controls.Add(Me.lblTotalPercentage)
        Me.gbBudgetData.Controls.Add(Me.dgvBudget)
        Me.gbBudgetData.Controls.Add(Me.cbOnScreenComputations)
        Me.gbBudgetData.ExpandedHoverImage = CType(resources.GetObject("gbBudgetData.ExpandedHoverImage"), System.Drawing.Image)
        Me.gbBudgetData.ExpandedNormalImage = CType(resources.GetObject("gbBudgetData.ExpandedNormalImage"), System.Drawing.Image)
        Me.gbBudgetData.ExpandedPressedImage = CType(resources.GetObject("gbBudgetData.ExpandedPressedImage"), System.Drawing.Image)
        Me.gbBudgetData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBudgetData.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBudgetData.HeaderHeight = 30
        Me.gbBudgetData.HeaderMessage = ""
        Me.gbBudgetData.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBudgetData.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBudgetData.HeightOnCollapse = 0
        Me.gbBudgetData.LeftTextSpace = 0
        Me.gbBudgetData.Location = New System.Drawing.Point(0, 229)
        Me.gbBudgetData.Name = "gbBudgetData"
        Me.gbBudgetData.OpenHeight = 293
        Me.gbBudgetData.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBudgetData.ShowBorder = True
        Me.gbBudgetData.ShowCheckBox = False
        Me.gbBudgetData.ShowCollapseButton = True
        Me.gbBudgetData.ShowDefaultBorderColor = True
        Me.gbBudgetData.ShowDownButton = False
        Me.gbBudgetData.ShowHeader = True
        Me.gbBudgetData.Size = New System.Drawing.Size(1002, 293)
        Me.gbBudgetData.TabIndex = 188
        Me.gbBudgetData.Temp = 0
        Me.gbBudgetData.Text = "Filter Criteria"
        Me.gbBudgetData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(828, 7)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(88, 15)
        Me.lnkAllocation.TabIndex = 191
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(947, 2)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(25, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(922, 2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(23, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'dgvBudget
        '
        Me.dgvBudget.AllowUserToAddRows = False
        Me.dgvBudget.AllowUserToDeleteRows = False
        Me.dgvBudget.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBudget.BackgroundColor = System.Drawing.Color.White
        Me.dgvBudget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudget.IgnoreFirstColumn = False
        Me.dgvBudget.Location = New System.Drawing.Point(1, 112)
        Me.dgvBudget.Name = "dgvBudget"
        Me.dgvBudget.RowHeadersVisible = False
        Me.dgvBudget.Size = New System.Drawing.Size(998, 179)
        Me.dgvBudget.TabIndex = 185
        '
        'frmBudgetAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 578)
        Me.Controls.Add(Me.gbBudgetData)
        Me.Controls.Add(Me.gbBudget)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBudgetAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Add / Edit"
        Me.gbBudget.ResumeLayout(False)
        Me.gbBudget.PerformLayout()
        CType(Me.dgvTranHead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperations.ResumeLayout(False)
        Me.cbOnScreenComputations.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbBudgetData.ResumeLayout(False)
        Me.gbBudgetData.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBudget, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbBudget As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll_Period As System.Windows.Forms.CheckBox
    Friend WithEvents lvPeriod As System.Windows.Forms.ListView
    Friend WithEvents objcolh_Period_Check As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_PayPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_StartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colh_Period_EndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboWhoToInclude As System.Windows.Forms.ComboBox
    Friend WithEvents lblWhoToInclude As System.Windows.Forms.Label
    Friend WithEvents cboPresentation As System.Windows.Forms.ComboBox
    Friend WithEvents lblPresentation As System.Windows.Forms.Label
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents dgvTranHead As System.Windows.Forms.DataGridView
    Friend WithEvents cboSalaryLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblSalaryLevel As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As eZee.Common.eZeeLightButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents cboBudgetYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudgetYear As System.Windows.Forms.Label
    Friend WithEvents txtSearchHead As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objchkSelectAll_Budget As System.Windows.Forms.CheckBox
    Friend WithEvents cbOnScreenComputations As System.Windows.Forms.GroupBox
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents radApplyToChecked As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkAddToExisting As System.Windows.Forms.CheckBox
    Friend WithEvents radPercentage As System.Windows.Forms.RadioButton
    Friend WithEvents txtValue As eZee.TextBox.NumericTextBox
    Friend WithEvents radFixedAmount As System.Windows.Forms.RadioButton
    Friend WithEvents cboAffectedColumns As System.Windows.Forms.ComboBox
    Friend WithEvents lblAffectedColumns As System.Windows.Forms.Label
    Friend WithEvents btnApply As eZee.Common.eZeeLightButton
    Friend WithEvents dgvBudget As Aruti.Data.GroupByGrid
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents lblBudgetCode As System.Windows.Forms.Label
    Friend WithEvents txtBudgetCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBudgetName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBudgetName As System.Windows.Forms.Label
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents chkOverWrite As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhTranheadUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranHeadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranHeadName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhHeadType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFormulaMappingHead As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objcolhFormulaMappingHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsSalary As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhFormulaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhFormula As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblBudgetDate As System.Windows.Forms.Label
    Friend WithEvents dtpBudgetDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objlblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents objlblBudgetTotal As System.Windows.Forms.Label
    Friend WithEvents lblGrandTotal As System.Windows.Forms.Label
    Friend WithEvents lblBudgetTotal As System.Windows.Forms.Label
    Friend WithEvents objlblOtherCostTotal As System.Windows.Forms.Label
    Friend WithEvents objlblTotalEmplyees As System.Windows.Forms.Label
    Friend WithEvents lblOtherCostTotal As System.Windows.Forms.Label
    Friend WithEvents lblTotalEmplyees As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents gbBudgetData As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboProjectCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblProjectCode As System.Windows.Forms.Label
    Friend WithEvents lblTotalPercentage As System.Windows.Forms.Label
    Friend WithEvents txtTotalPercentage As eZee.TextBox.NumericTextBox
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents mnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuResetBudget As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExportPercentageAllocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportPercentageAllocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
End Class

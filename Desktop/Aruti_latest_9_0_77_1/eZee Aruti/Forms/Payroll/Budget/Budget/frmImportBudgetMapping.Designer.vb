﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportBudgetMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportBudgetMapping))
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dgvHeadMapping = New System.Windows.Forms.DataGridView
        Me.colhMapeTranHeadCode = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.cboBasicSalary = New System.Windows.Forms.ComboBox
        Me.lblBasicSalary = New System.Windows.Forms.Label
        Me.cboOtherPayrollCost = New System.Windows.Forms.ComboBox
        Me.lblOtherPayrollCost = New System.Windows.Forms.Label
        Me.cboBudget = New System.Windows.Forms.ComboBox
        Me.lblBudget = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvMapping = New System.Windows.Forms.DataGridView
        Me.colhPercentage = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objcolhTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhTranheadCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhMappedTranheadunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhFundprojectcodeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhProjectCodes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPercentageid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhActivityid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefFormFooter.SuspendLayout()
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvHeadMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.lnkAutoMap)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnOk)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 517)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(794, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Location = New System.Drawing.Point(20, 22)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(124, 13)
        Me.lnkAutoMap.TabIndex = 2
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "&Auto Map"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(685, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(582, 13)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = False
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFieldMapping)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(794, 572)
        Me.pnlMainInfo.TabIndex = 2
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.Panel2)
        Me.gbFieldMapping.Controls.Add(Me.cboBasicSalary)
        Me.gbFieldMapping.Controls.Add(Me.lblBasicSalary)
        Me.gbFieldMapping.Controls.Add(Me.cboOtherPayrollCost)
        Me.gbFieldMapping.Controls.Add(Me.lblOtherPayrollCost)
        Me.gbFieldMapping.Controls.Add(Me.cboBudget)
        Me.gbFieldMapping.Controls.Add(Me.lblBudget)
        Me.gbFieldMapping.Controls.Add(Me.Panel1)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(12, 12)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(770, 499)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvHeadMapping)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(390, 85)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(372, 411)
        Me.Panel2.TabIndex = 246
        '
        'dgvHeadMapping
        '
        Me.dgvHeadMapping.AllowUserToAddRows = False
        Me.dgvHeadMapping.AllowUserToDeleteRows = False
        Me.dgvHeadMapping.AllowUserToResizeRows = False
        Me.dgvHeadMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHeadMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhTranheadunkid, Me.colhTranheadCode, Me.colhMapeTranHeadCode, Me.objcolhMappedTranheadunkid})
        Me.dgvHeadMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHeadMapping.Location = New System.Drawing.Point(0, 0)
        Me.dgvHeadMapping.Name = "dgvHeadMapping"
        Me.dgvHeadMapping.RowHeadersVisible = False
        Me.dgvHeadMapping.Size = New System.Drawing.Size(372, 411)
        Me.dgvHeadMapping.TabIndex = 0
        '
        'colhMapeTranHeadCode
        '
        Me.colhMapeTranHeadCode.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colhMapeTranHeadCode.HeaderText = "Map Tran. Head Code"
        Me.colhMapeTranHeadCode.Name = "colhMapeTranHeadCode"
        Me.colhMapeTranHeadCode.Width = 200
        '
        'cboBasicSalary
        '
        Me.cboBasicSalary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBasicSalary.FormattingEnabled = True
        Me.cboBasicSalary.Location = New System.Drawing.Point(510, 31)
        Me.cboBasicSalary.Name = "cboBasicSalary"
        Me.cboBasicSalary.Size = New System.Drawing.Size(157, 21)
        Me.cboBasicSalary.TabIndex = 240
        '
        'lblBasicSalary
        '
        Me.lblBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicSalary.Location = New System.Drawing.Point(399, 32)
        Me.lblBasicSalary.Name = "lblBasicSalary"
        Me.lblBasicSalary.Size = New System.Drawing.Size(105, 16)
        Me.lblBasicSalary.TabIndex = 241
        Me.lblBasicSalary.Text = "Basic Salary"
        Me.lblBasicSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherPayrollCost
        '
        Me.cboOtherPayrollCost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherPayrollCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherPayrollCost.FormattingEnabled = True
        Me.cboOtherPayrollCost.Location = New System.Drawing.Point(510, 58)
        Me.cboOtherPayrollCost.Name = "cboOtherPayrollCost"
        Me.cboOtherPayrollCost.Size = New System.Drawing.Size(157, 21)
        Me.cboOtherPayrollCost.TabIndex = 238
        '
        'lblOtherPayrollCost
        '
        Me.lblOtherPayrollCost.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherPayrollCost.Location = New System.Drawing.Point(399, 59)
        Me.lblOtherPayrollCost.Name = "lblOtherPayrollCost"
        Me.lblOtherPayrollCost.Size = New System.Drawing.Size(105, 16)
        Me.lblOtherPayrollCost.TabIndex = 239
        Me.lblOtherPayrollCost.Text = "Other Payroll Cost"
        Me.lblOtherPayrollCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBudget
        '
        Me.cboBudget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudget.FormattingEnabled = True
        Me.cboBudget.Location = New System.Drawing.Point(135, 31)
        Me.cboBudget.Name = "cboBudget"
        Me.cboBudget.Size = New System.Drawing.Size(157, 21)
        Me.cboBudget.TabIndex = 235
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(9, 32)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(120, 16)
        Me.lblBudget.TabIndex = 236
        Me.lblBudget.Text = "Budget"
        Me.lblBudget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvMapping)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(11, 85)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(373, 411)
        Me.Panel1.TabIndex = 23
        '
        'dgvMapping
        '
        Me.dgvMapping.AllowUserToAddRows = False
        Me.dgvMapping.AllowUserToDeleteRows = False
        Me.dgvMapping.AllowUserToResizeRows = False
        Me.dgvMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMapping.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhFundprojectcodeunkid, Me.colhProjectCodes, Me.colhPercentage, Me.objcolhPercentageid, Me.objcolhActivityid})
        Me.dgvMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvMapping.Location = New System.Drawing.Point(0, 0)
        Me.dgvMapping.Name = "dgvMapping"
        Me.dgvMapping.RowHeadersVisible = False
        Me.dgvMapping.Size = New System.Drawing.Size(373, 411)
        Me.dgvMapping.TabIndex = 0
        '
        'colhPercentage
        '
        Me.colhPercentage.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.colhPercentage.HeaderText = "Project Codes (%)"
        Me.colhPercentage.Name = "colhPercentage"
        Me.colhPercentage.Width = 200
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(135, 58)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(157, 21)
        Me.cboEmployeeCode.TabIndex = 0
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(9, 59)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(120, 16)
        Me.lblEmployeeCode.TabIndex = 1
        Me.lblEmployeeCode.Text = "Emp. / Allocation Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objcolhTranheadunkid
        '
        Me.objcolhTranheadunkid.HeaderText = ""
        Me.objcolhTranheadunkid.Name = "objcolhTranheadunkid"
        Me.objcolhTranheadunkid.ReadOnly = True
        Me.objcolhTranheadunkid.Visible = False
        '
        'colhTranheadCode
        '
        Me.colhTranheadCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhTranheadCode.HeaderText = "Tran. Head Code"
        Me.colhTranheadCode.Name = "colhTranheadCode"
        Me.colhTranheadCode.ReadOnly = True
        '
        'objcolhMappedTranheadunkid
        '
        Me.objcolhMappedTranheadunkid.HeaderText = ""
        Me.objcolhMappedTranheadunkid.Name = "objcolhMappedTranheadunkid"
        Me.objcolhMappedTranheadunkid.ReadOnly = True
        Me.objcolhMappedTranheadunkid.Visible = False
        '
        'objcolhFundprojectcodeunkid
        '
        Me.objcolhFundprojectcodeunkid.HeaderText = ""
        Me.objcolhFundprojectcodeunkid.Name = "objcolhFundprojectcodeunkid"
        Me.objcolhFundprojectcodeunkid.ReadOnly = True
        Me.objcolhFundprojectcodeunkid.Visible = False
        '
        'colhProjectCodes
        '
        Me.colhProjectCodes.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhProjectCodes.HeaderText = "Project Codes"
        Me.colhProjectCodes.Name = "colhProjectCodes"
        Me.colhProjectCodes.ReadOnly = True
        '
        'objcolhPercentageid
        '
        Me.objcolhPercentageid.HeaderText = ""
        Me.objcolhPercentageid.Name = "objcolhPercentageid"
        Me.objcolhPercentageid.ReadOnly = True
        Me.objcolhPercentageid.Visible = False
        '
        'objcolhActivityid
        '
        Me.objcolhActivityid.HeaderText = ""
        Me.objcolhActivityid.Name = "objcolhActivityid"
        Me.objcolhActivityid.ReadOnly = True
        Me.objcolhActivityid.Visible = False
        '
        'frmImportBudgetMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportBudgetMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Import Budget Mapping"
        Me.objefFormFooter.ResumeLayout(False)
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvHeadMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboBudget As System.Windows.Forms.ComboBox
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvMapping As System.Windows.Forms.DataGridView
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objcolhFundprojectcodeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhProjectCodes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPercentage As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objcolhPercentageid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhActivityid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboBasicSalary As System.Windows.Forms.ComboBox
    Friend WithEvents lblBasicSalary As System.Windows.Forms.Label
    Friend WithEvents cboOtherPayrollCost As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherPayrollCost As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvHeadMapping As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhTranheadCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMapeTranHeadCode As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents objcolhMappedTranheadunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApproveDisapproveBudgetList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApproveDisapproveBudgetList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvBudgetApproval = New Aruti.Data.GroupByGrid
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblBudget = New System.Windows.Forms.Label
        Me.cboBudget = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnChangeStatus = New eZee.Common.eZeeLightButton(Me.components)
        Me.colhBudgetCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBudgetName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPayyearUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPayyear = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhViewById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhViewBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAllocationById = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAllocationBy = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPresentationmodeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPresentationmode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhWhotoIncludeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhWhotoInclude = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSalaryLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSalaryLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsDefault = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhApprovalDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhBudgetUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhBudgetapprovaltranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhlevelunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhApproverunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvBudgetApproval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvBudgetApproval)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(854, 572)
        Me.pnlMainInfo.TabIndex = 3
        '
        'dgvBudgetApproval
        '
        Me.dgvBudgetApproval.AllowUserToAddRows = False
        Me.dgvBudgetApproval.AllowUserToDeleteRows = False
        Me.dgvBudgetApproval.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBudgetApproval.BackgroundColor = System.Drawing.Color.White
        Me.dgvBudgetApproval.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBudgetApproval.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhBudgetCode, Me.colhBudgetName, Me.objcolhPayyearUnkid, Me.colhPayyear, Me.objcolhViewById, Me.colhViewBy, Me.objcolhAllocationById, Me.colhAllocationBy, Me.objcolhPresentationmodeId, Me.colhPresentationmode, Me.objcolhWhotoIncludeId, Me.colhWhotoInclude, Me.objcolhSalaryLevelId, Me.colhSalaryLevel, Me.colhIsDefault, Me.objcolhIsDefault, Me.colhLevel, Me.colhApprover, Me.colhPriority, Me.colhApprovalDate, Me.colhStatus, Me.colhRemark, Me.objcolhBudgetUnkid, Me.objcolhBudgetapprovaltranunkid, Me.objcolhlevelunkid, Me.objcolhApproverunkid, Me.objcolhStatus})
        Me.dgvBudgetApproval.IgnoreFirstColumn = False
        Me.dgvBudgetApproval.Location = New System.Drawing.Point(12, 135)
        Me.dgvBudgetApproval.Name = "dgvBudgetApproval"
        Me.dgvBudgetApproval.RowHeadersVisible = False
        Me.dgvBudgetApproval.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBudgetApproval.Size = New System.Drawing.Size(830, 376)
        Me.dgvBudgetApproval.TabIndex = 7
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(854, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Budget Approval List"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblBudget)
        Me.gbFilterCriteria.Controls.Add(Me.cboBudget)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(830, 63)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(611, 33)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(192, 21)
        Me.cboStatus.TabIndex = 152
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(503, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(102, 15)
        Me.lblStatus.TabIndex = 153
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBudget
        '
        Me.lblBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBudget.Location = New System.Drawing.Point(8, 36)
        Me.lblBudget.Name = "lblBudget"
        Me.lblBudget.Size = New System.Drawing.Size(83, 15)
        Me.lblBudget.TabIndex = 74
        Me.lblBudget.Text = "Budget"
        '
        'cboBudget
        '
        Me.cboBudget.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBudget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBudget.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBudget.FormattingEnabled = True
        Me.cboBudget.Location = New System.Drawing.Point(97, 33)
        Me.cboBudget.Name = "cboBudget"
        Me.cboBudget.Size = New System.Drawing.Size(135, 21)
        Me.cboBudget.TabIndex = 0
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(803, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(779, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnChangeStatus)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 517)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(854, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(749, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnChangeStatus
        '
        Me.btnChangeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnChangeStatus.BackColor = System.Drawing.Color.White
        Me.btnChangeStatus.BackgroundImage = CType(resources.GetObject("btnChangeStatus.BackgroundImage"), System.Drawing.Image)
        Me.btnChangeStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnChangeStatus.BorderColor = System.Drawing.Color.Empty
        Me.btnChangeStatus.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnChangeStatus.FlatAppearance.BorderSize = 0
        Me.btnChangeStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnChangeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangeStatus.ForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnChangeStatus.GradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Location = New System.Drawing.Point(634, 13)
        Me.btnChangeStatus.Name = "btnChangeStatus"
        Me.btnChangeStatus.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnChangeStatus.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnChangeStatus.Size = New System.Drawing.Size(109, 30)
        Me.btnChangeStatus.TabIndex = 0
        Me.btnChangeStatus.Text = "&Change Status"
        Me.btnChangeStatus.UseVisualStyleBackColor = True
        '
        'colhBudgetCode
        '
        Me.colhBudgetCode.HeaderText = "Budget Code"
        Me.colhBudgetCode.Name = "colhBudgetCode"
        Me.colhBudgetCode.ReadOnly = True
        Me.colhBudgetCode.Width = 70
        '
        'colhBudgetName
        '
        Me.colhBudgetName.HeaderText = "Budget Name"
        Me.colhBudgetName.Name = "colhBudgetName"
        Me.colhBudgetName.ReadOnly = True
        '
        'objcolhPayyearUnkid
        '
        Me.objcolhPayyearUnkid.HeaderText = "PayyearUnkid"
        Me.objcolhPayyearUnkid.Name = "objcolhPayyearUnkid"
        Me.objcolhPayyearUnkid.ReadOnly = True
        Me.objcolhPayyearUnkid.Visible = False
        '
        'colhPayyear
        '
        Me.colhPayyear.HeaderText = "Pay Year"
        Me.colhPayyear.Name = "colhPayyear"
        Me.colhPayyear.ReadOnly = True
        Me.colhPayyear.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhViewById
        '
        Me.objcolhViewById.HeaderText = "ViewById"
        Me.objcolhViewById.Name = "objcolhViewById"
        Me.objcolhViewById.ReadOnly = True
        Me.objcolhViewById.Visible = False
        '
        'colhViewBy
        '
        Me.colhViewBy.HeaderText = "View By"
        Me.colhViewBy.Name = "colhViewBy"
        Me.colhViewBy.ReadOnly = True
        Me.colhViewBy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhAllocationById
        '
        Me.objcolhAllocationById.HeaderText = "AllocationById"
        Me.objcolhAllocationById.Name = "objcolhAllocationById"
        Me.objcolhAllocationById.ReadOnly = True
        Me.objcolhAllocationById.Visible = False
        '
        'colhAllocationBy
        '
        Me.colhAllocationBy.HeaderText = "Allocation By"
        Me.colhAllocationBy.Name = "colhAllocationBy"
        Me.colhAllocationBy.ReadOnly = True
        Me.colhAllocationBy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhPresentationmodeId
        '
        Me.objcolhPresentationmodeId.HeaderText = "PresentationmodeId"
        Me.objcolhPresentationmodeId.Name = "objcolhPresentationmodeId"
        Me.objcolhPresentationmodeId.ReadOnly = True
        Me.objcolhPresentationmodeId.Visible = False
        '
        'colhPresentationmode
        '
        Me.colhPresentationmode.HeaderText = "Presentation Mode"
        Me.colhPresentationmode.Name = "colhPresentationmode"
        Me.colhPresentationmode.ReadOnly = True
        Me.colhPresentationmode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhWhotoIncludeId
        '
        Me.objcolhWhotoIncludeId.HeaderText = "WhotoIncludeId"
        Me.objcolhWhotoIncludeId.Name = "objcolhWhotoIncludeId"
        Me.objcolhWhotoIncludeId.ReadOnly = True
        Me.objcolhWhotoIncludeId.Visible = False
        '
        'colhWhotoInclude
        '
        Me.colhWhotoInclude.HeaderText = "Who To Include"
        Me.colhWhotoInclude.Name = "colhWhotoInclude"
        Me.colhWhotoInclude.ReadOnly = True
        Me.colhWhotoInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhWhotoInclude.Width = 135
        '
        'objcolhSalaryLevelId
        '
        Me.objcolhSalaryLevelId.HeaderText = "SalaryLevelId"
        Me.objcolhSalaryLevelId.Name = "objcolhSalaryLevelId"
        Me.objcolhSalaryLevelId.ReadOnly = True
        Me.objcolhSalaryLevelId.Visible = False
        '
        'colhSalaryLevel
        '
        Me.colhSalaryLevel.HeaderText = "Salary Level"
        Me.colhSalaryLevel.Name = "colhSalaryLevel"
        Me.colhSalaryLevel.ReadOnly = True
        Me.colhSalaryLevel.Visible = False
        '
        'colhIsDefault
        '
        Me.colhIsDefault.HeaderText = "Is Default"
        Me.colhIsDefault.Name = "colhIsDefault"
        Me.colhIsDefault.ReadOnly = True
        Me.colhIsDefault.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhIsDefault.Width = 60
        '
        'objcolhIsDefault
        '
        Me.objcolhIsDefault.HeaderText = "IsDefault"
        Me.objcolhIsDefault.Name = "objcolhIsDefault"
        Me.objcolhIsDefault.ReadOnly = True
        Me.objcolhIsDefault.Visible = False
        '
        'colhLevel
        '
        Me.colhLevel.HeaderText = "Level"
        Me.colhLevel.Name = "colhLevel"
        Me.colhLevel.ReadOnly = True
        Me.colhLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhApprover
        '
        Me.colhApprover.HeaderText = "Approver"
        Me.colhApprover.Name = "colhApprover"
        Me.colhApprover.ReadOnly = True
        Me.colhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhPriority
        '
        Me.colhPriority.HeaderText = "Priority"
        Me.colhPriority.Name = "colhPriority"
        Me.colhPriority.ReadOnly = True
        Me.colhPriority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhApprovalDate
        '
        Me.colhApprovalDate.HeaderText = "Approval Date"
        Me.colhApprovalDate.Name = "colhApprovalDate"
        Me.colhApprovalDate.ReadOnly = True
        Me.colhApprovalDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhRemark
        '
        Me.colhRemark.HeaderText = "Remark"
        Me.colhRemark.Name = "colhRemark"
        Me.colhRemark.ReadOnly = True
        Me.colhRemark.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhBudgetUnkid
        '
        Me.objcolhBudgetUnkid.HeaderText = "BudgetUnkid"
        Me.objcolhBudgetUnkid.Name = "objcolhBudgetUnkid"
        Me.objcolhBudgetUnkid.ReadOnly = True
        Me.objcolhBudgetUnkid.Visible = False
        '
        'objcolhBudgetapprovaltranunkid
        '
        Me.objcolhBudgetapprovaltranunkid.HeaderText = "budgetapprovaltranunkid"
        Me.objcolhBudgetapprovaltranunkid.Name = "objcolhBudgetapprovaltranunkid"
        Me.objcolhBudgetapprovaltranunkid.ReadOnly = True
        Me.objcolhBudgetapprovaltranunkid.Visible = False
        '
        'objcolhlevelunkid
        '
        Me.objcolhlevelunkid.HeaderText = "levelunkid"
        Me.objcolhlevelunkid.Name = "objcolhlevelunkid"
        Me.objcolhlevelunkid.ReadOnly = True
        Me.objcolhlevelunkid.Visible = False
        '
        'objcolhApproverunkid
        '
        Me.objcolhApproverunkid.HeaderText = "approverunkid"
        Me.objcolhApproverunkid.Name = "objcolhApproverunkid"
        Me.objcolhApproverunkid.ReadOnly = True
        Me.objcolhApproverunkid.Visible = False
        '
        'objcolhStatus
        '
        Me.objcolhStatus.HeaderText = "Status"
        Me.objcolhStatus.Name = "objcolhStatus"
        Me.objcolhStatus.ReadOnly = True
        Me.objcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhStatus.Visible = False
        '
        'frmApproveDisapproveBudgetList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 572)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApproveDisapproveBudgetList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Budget Approval List"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvBudgetApproval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblBudget As System.Windows.Forms.Label
    Friend WithEvents cboBudget As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnChangeStatus As eZee.Common.eZeeLightButton
    Friend WithEvents dgvBudgetApproval As Aruti.Data.GroupByGrid
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents colhBudgetCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBudgetName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPayyearUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPayyear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhViewById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhViewBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAllocationById As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhAllocationBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPresentationmodeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPresentationmode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhWhotoIncludeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhWhotoInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSalaryLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSalaryLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsDefault As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhApprovalDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhBudgetUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhBudgetapprovaltranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhlevelunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhApproverunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

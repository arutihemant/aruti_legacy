﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundActivityAdjustment_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundActivityAdjustment_List))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtCurrentBalTo = New eZee.TextBox.NumericTextBox
        Me.txtCurrentBalFrom = New eZee.TextBox.NumericTextBox
        Me.lblCurrentBalTo = New System.Windows.Forms.Label
        Me.lblCurrentBalFrom = New System.Windows.Forms.Label
        Me.dtpTransactionFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpTransactionTo = New System.Windows.Forms.DateTimePicker
        Me.lblTranToDt = New System.Windows.Forms.Label
        Me.lblTranFromDt = New System.Windows.Forms.Label
        Me.cboActivityName = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblActivityName = New System.Windows.Forms.Label
        Me.objbtnSearchActivityName = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvFundActivityAdjustment = New Aruti.Data.GroupByGrid
        Me.dgcolhProjectName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivityCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivityName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTarnsactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrentBal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncrDecrAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNewBalance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFundActivityAdjustmentunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfundactivityunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPaymenttranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGlobalvocunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvFundActivityAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtCurrentBalTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtCurrentBalFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrentBalTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrentBalFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTransactionFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTransactionTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranToDt)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranFromDt)
        Me.gbFilterCriteria.Controls.Add(Me.cboActivityName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblActivityName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchActivityName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(803, 92)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCurrentBalTo
        '
        Me.txtCurrentBalTo.AllowNegative = True
        Me.txtCurrentBalTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentBalTo.DigitsInGroup = 0
        Me.txtCurrentBalTo.Flags = 0
        Me.txtCurrentBalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentBalTo.Location = New System.Drawing.Point(261, 61)
        Me.txtCurrentBalTo.MaxDecimalPlaces = 6
        Me.txtCurrentBalTo.MaxWholeDigits = 21
        Me.txtCurrentBalTo.Name = "txtCurrentBalTo"
        Me.txtCurrentBalTo.Prefix = ""
        Me.txtCurrentBalTo.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentBalTo.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentBalTo.Size = New System.Drawing.Size(115, 21)
        Me.txtCurrentBalTo.TabIndex = 100
        Me.txtCurrentBalTo.Text = "0"
        Me.txtCurrentBalTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCurrentBalFrom
        '
        Me.txtCurrentBalFrom.AllowNegative = True
        Me.txtCurrentBalFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentBalFrom.DigitsInGroup = 0
        Me.txtCurrentBalFrom.Flags = 0
        Me.txtCurrentBalFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentBalFrom.Location = New System.Drawing.Point(109, 61)
        Me.txtCurrentBalFrom.MaxDecimalPlaces = 6
        Me.txtCurrentBalFrom.MaxWholeDigits = 21
        Me.txtCurrentBalFrom.Name = "txtCurrentBalFrom"
        Me.txtCurrentBalFrom.Prefix = ""
        Me.txtCurrentBalFrom.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentBalFrom.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentBalFrom.Size = New System.Drawing.Size(115, 21)
        Me.txtCurrentBalFrom.TabIndex = 99
        Me.txtCurrentBalFrom.Text = "0"
        Me.txtCurrentBalFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentBalTo
        '
        Me.lblCurrentBalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBalTo.Location = New System.Drawing.Point(230, 64)
        Me.lblCurrentBalTo.Name = "lblCurrentBalTo"
        Me.lblCurrentBalTo.Size = New System.Drawing.Size(25, 15)
        Me.lblCurrentBalTo.TabIndex = 97
        Me.lblCurrentBalTo.Text = "To"
        Me.lblCurrentBalTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentBalFrom
        '
        Me.lblCurrentBalFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBalFrom.Location = New System.Drawing.Point(11, 64)
        Me.lblCurrentBalFrom.Name = "lblCurrentBalFrom"
        Me.lblCurrentBalFrom.Size = New System.Drawing.Size(92, 15)
        Me.lblCurrentBalFrom.TabIndex = 95
        Me.lblCurrentBalFrom.Text = "Current Bal From"
        '
        'dtpTransactionFrom
        '
        Me.dtpTransactionFrom.Checked = False
        Me.dtpTransactionFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTransactionFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTransactionFrom.Location = New System.Drawing.Point(109, 34)
        Me.dtpTransactionFrom.Name = "dtpTransactionFrom"
        Me.dtpTransactionFrom.ShowCheckBox = True
        Me.dtpTransactionFrom.Size = New System.Drawing.Size(115, 21)
        Me.dtpTransactionFrom.TabIndex = 92
        '
        'dtpTransactionTo
        '
        Me.dtpTransactionTo.Checked = False
        Me.dtpTransactionTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTransactionTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTransactionTo.Location = New System.Drawing.Point(261, 34)
        Me.dtpTransactionTo.Name = "dtpTransactionTo"
        Me.dtpTransactionTo.ShowCheckBox = True
        Me.dtpTransactionTo.Size = New System.Drawing.Size(115, 21)
        Me.dtpTransactionTo.TabIndex = 94
        '
        'lblTranToDt
        '
        Me.lblTranToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranToDt.Location = New System.Drawing.Point(230, 37)
        Me.lblTranToDt.Name = "lblTranToDt"
        Me.lblTranToDt.Size = New System.Drawing.Size(25, 15)
        Me.lblTranToDt.TabIndex = 93
        Me.lblTranToDt.Text = "To"
        Me.lblTranToDt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTranFromDt
        '
        Me.lblTranFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranFromDt.Location = New System.Drawing.Point(11, 37)
        Me.lblTranFromDt.Name = "lblTranFromDt"
        Me.lblTranFromDt.Size = New System.Drawing.Size(92, 15)
        Me.lblTranFromDt.TabIndex = 91
        Me.lblTranFromDt.Text = "Transaction From"
        '
        'cboActivityName
        '
        Me.cboActivityName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivityName.DropDownWidth = 150
        Me.cboActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivityName.FormattingEnabled = True
        Me.cboActivityName.Location = New System.Drawing.Point(464, 34)
        Me.cboActivityName.Name = "cboActivityName"
        Me.cboActivityName.Size = New System.Drawing.Size(134, 21)
        Me.cboActivityName.TabIndex = 90
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(777, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'lblActivityName
        '
        Me.lblActivityName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivityName.Location = New System.Drawing.Point(390, 37)
        Me.lblActivityName.Name = "lblActivityName"
        Me.lblActivityName.Size = New System.Drawing.Size(68, 15)
        Me.lblActivityName.TabIndex = 85
        Me.lblActivityName.Text = "Activity Name"
        '
        'objbtnSearchActivityName
        '
        Me.objbtnSearchActivityName.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchActivityName.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivityName.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchActivityName.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchActivityName.BorderSelected = False
        Me.objbtnSearchActivityName.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchActivityName.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchActivityName.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchActivityName.Location = New System.Drawing.Point(604, 34)
        Me.objbtnSearchActivityName.Name = "objbtnSearchActivityName"
        Me.objbtnSearchActivityName.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchActivityName.TabIndex = 88
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(753, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 537)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(827, 55)
        Me.objFooter.TabIndex = 7
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(615, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(512, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(409, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(718, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvFundActivityAdjustment
        '
        Me.dgvFundActivityAdjustment.AllowUserToAddRows = False
        Me.dgvFundActivityAdjustment.AllowUserToDeleteRows = False
        Me.dgvFundActivityAdjustment.AllowUserToResizeRows = False
        Me.dgvFundActivityAdjustment.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvFundActivityAdjustment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvFundActivityAdjustment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvFundActivityAdjustment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFundActivityAdjustment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhProjectName, Me.dgcolhActivityCode, Me.dgcolhActivityName, Me.dgcolhTarnsactionDate, Me.dgcolhCurrentBal, Me.dgcolhIncrDecrAmount, Me.dgcolhNewBalance, Me.objdgcolhFundActivityAdjustmentunkid, Me.objdgcolhfundactivityunkid, Me.objdgcolhPaymenttranunkid, Me.objdgcolhGlobalvocunkid})
        Me.dgvFundActivityAdjustment.IgnoreFirstColumn = False
        Me.dgvFundActivityAdjustment.Location = New System.Drawing.Point(12, 110)
        Me.dgvFundActivityAdjustment.MultiSelect = False
        Me.dgvFundActivityAdjustment.Name = "dgvFundActivityAdjustment"
        Me.dgvFundActivityAdjustment.ReadOnly = True
        Me.dgvFundActivityAdjustment.RowHeadersVisible = False
        Me.dgvFundActivityAdjustment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFundActivityAdjustment.Size = New System.Drawing.Size(803, 421)
        Me.dgvFundActivityAdjustment.TabIndex = 6
        '
        'dgcolhProjectName
        '
        Me.dgcolhProjectName.HeaderText = "Project Name"
        Me.dgcolhProjectName.Name = "dgcolhProjectName"
        Me.dgcolhProjectName.ReadOnly = True
        '
        'dgcolhActivityCode
        '
        Me.dgcolhActivityCode.HeaderText = "Activity Code"
        Me.dgcolhActivityCode.Name = "dgcolhActivityCode"
        Me.dgcolhActivityCode.ReadOnly = True
        Me.dgcolhActivityCode.Width = 90
        '
        'dgcolhActivityName
        '
        Me.dgcolhActivityName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhActivityName.HeaderText = "Activity Name"
        Me.dgcolhActivityName.Name = "dgcolhActivityName"
        Me.dgcolhActivityName.ReadOnly = True
        '
        'dgcolhTarnsactionDate
        '
        Me.dgcolhTarnsactionDate.HeaderText = "Transaction Date"
        Me.dgcolhTarnsactionDate.Name = "dgcolhTarnsactionDate"
        Me.dgcolhTarnsactionDate.ReadOnly = True
        '
        'dgcolhCurrentBal
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhCurrentBal.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhCurrentBal.HeaderText = "Current Balance"
        Me.dgcolhCurrentBal.Name = "dgcolhCurrentBal"
        Me.dgcolhCurrentBal.ReadOnly = True
        Me.dgcolhCurrentBal.Width = 130
        '
        'dgcolhIncrDecrAmount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhIncrDecrAmount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhIncrDecrAmount.HeaderText = "Incr / Decr Amount"
        Me.dgcolhIncrDecrAmount.Name = "dgcolhIncrDecrAmount"
        Me.dgcolhIncrDecrAmount.ReadOnly = True
        Me.dgcolhIncrDecrAmount.Width = 110
        '
        'dgcolhNewBalance
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhNewBalance.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhNewBalance.HeaderText = "New Balance"
        Me.dgcolhNewBalance.Name = "dgcolhNewBalance"
        Me.dgcolhNewBalance.ReadOnly = True
        Me.dgcolhNewBalance.Width = 130
        '
        'objdgcolhFundActivityAdjustmentunkid
        '
        Me.objdgcolhFundActivityAdjustmentunkid.HeaderText = "objdgcolhFundActivityAdjustmentunkid"
        Me.objdgcolhFundActivityAdjustmentunkid.Name = "objdgcolhFundActivityAdjustmentunkid"
        Me.objdgcolhFundActivityAdjustmentunkid.ReadOnly = True
        Me.objdgcolhFundActivityAdjustmentunkid.Visible = False
        Me.objdgcolhFundActivityAdjustmentunkid.Width = 5
        '
        'objdgcolhfundactivityunkid
        '
        Me.objdgcolhfundactivityunkid.HeaderText = "objdgcolhfundsourceunkid"
        Me.objdgcolhfundactivityunkid.Name = "objdgcolhfundactivityunkid"
        Me.objdgcolhfundactivityunkid.ReadOnly = True
        Me.objdgcolhfundactivityunkid.Visible = False
        Me.objdgcolhfundactivityunkid.Width = 5
        '
        'objdgcolhPaymenttranunkid
        '
        Me.objdgcolhPaymenttranunkid.HeaderText = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.Name = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.ReadOnly = True
        Me.objdgcolhPaymenttranunkid.Visible = False
        '
        'objdgcolhGlobalvocunkid
        '
        Me.objdgcolhGlobalvocunkid.HeaderText = "objdgcolhGlobalvocunkid"
        Me.objdgcolhGlobalvocunkid.Name = "objdgcolhGlobalvocunkid"
        Me.objdgcolhGlobalvocunkid.ReadOnly = True
        Me.objdgcolhGlobalvocunkid.Visible = False
        '
        'frmFundActivityAdjustment_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(827, 592)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.dgvFundActivityAdjustment)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundActivityAdjustment_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "List of Fund Activity Adjustments"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvFundActivityAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchActivityName As eZee.Common.eZeeGradientButton
    Friend WithEvents lblActivityName As System.Windows.Forms.Label
    Friend WithEvents dgvFundActivityAdjustment As Aruti.Data.GroupByGrid
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboActivityName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranFromDt As System.Windows.Forms.Label
    Friend WithEvents dtpTransactionTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTranToDt As System.Windows.Forms.Label
    Friend WithEvents dtpTransactionFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrentBalTo As System.Windows.Forms.Label
    Friend WithEvents lblCurrentBalFrom As System.Windows.Forms.Label
    Friend WithEvents txtCurrentBalTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtCurrentBalFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents dgcolhProjectName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivityCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivityName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTarnsactionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrentBal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncrDecrAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNewBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFundActivityAdjustmentunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfundactivityunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPaymenttranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGlobalvocunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundAdjustment_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundAdjustment_List))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtCurrentBalTo = New eZee.TextBox.NumericTextBox
        Me.txtCurrentBalFrom = New eZee.TextBox.NumericTextBox
        Me.lblCurrentBalTo = New System.Windows.Forms.Label
        Me.lblCurrentBalFrom = New System.Windows.Forms.Label
        Me.dtpTransactionFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpTransactionTo = New System.Windows.Forms.DateTimePicker
        Me.lblTranToDt = New System.Windows.Forms.Label
        Me.lblTranFromDt = New System.Windows.Forms.Label
        Me.cboProjectCode = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblFundProjectCode = New System.Windows.Forms.Label
        Me.objbtnSearchProjectCode = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvFundAdjustment = New Aruti.Data.GroupByGrid
        Me.dgcolhFundProjectCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTarnsactionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCurrentBal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncrDecrAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNewBalance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfundadjustmentunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhfundProjectCodeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvFundAdjustment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtCurrentBalTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtCurrentBalFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrentBalTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrentBalFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTransactionFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpTransactionTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranToDt)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranFromDt)
        Me.gbFilterCriteria.Controls.Add(Me.cboProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblFundProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchProjectCode)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(803, 92)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCurrentBalTo
        '
        Me.txtCurrentBalTo.AllowNegative = True
        Me.txtCurrentBalTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentBalTo.DigitsInGroup = 0
        Me.txtCurrentBalTo.Flags = 0
        Me.txtCurrentBalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentBalTo.Location = New System.Drawing.Point(261, 61)
        Me.txtCurrentBalTo.MaxDecimalPlaces = 6
        Me.txtCurrentBalTo.MaxWholeDigits = 21
        Me.txtCurrentBalTo.Name = "txtCurrentBalTo"
        Me.txtCurrentBalTo.Prefix = ""
        Me.txtCurrentBalTo.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentBalTo.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentBalTo.Size = New System.Drawing.Size(115, 21)
        Me.txtCurrentBalTo.TabIndex = 100
        Me.txtCurrentBalTo.Text = "0"
        Me.txtCurrentBalTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCurrentBalFrom
        '
        Me.txtCurrentBalFrom.AllowNegative = True
        Me.txtCurrentBalFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentBalFrom.DigitsInGroup = 0
        Me.txtCurrentBalFrom.Flags = 0
        Me.txtCurrentBalFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentBalFrom.Location = New System.Drawing.Point(109, 61)
        Me.txtCurrentBalFrom.MaxDecimalPlaces = 6
        Me.txtCurrentBalFrom.MaxWholeDigits = 21
        Me.txtCurrentBalFrom.Name = "txtCurrentBalFrom"
        Me.txtCurrentBalFrom.Prefix = ""
        Me.txtCurrentBalFrom.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentBalFrom.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentBalFrom.Size = New System.Drawing.Size(115, 21)
        Me.txtCurrentBalFrom.TabIndex = 99
        Me.txtCurrentBalFrom.Text = "0"
        Me.txtCurrentBalFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCurrentBalTo
        '
        Me.lblCurrentBalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBalTo.Location = New System.Drawing.Point(230, 64)
        Me.lblCurrentBalTo.Name = "lblCurrentBalTo"
        Me.lblCurrentBalTo.Size = New System.Drawing.Size(25, 15)
        Me.lblCurrentBalTo.TabIndex = 97
        Me.lblCurrentBalTo.Text = "To"
        Me.lblCurrentBalTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentBalFrom
        '
        Me.lblCurrentBalFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentBalFrom.Location = New System.Drawing.Point(11, 64)
        Me.lblCurrentBalFrom.Name = "lblCurrentBalFrom"
        Me.lblCurrentBalFrom.Size = New System.Drawing.Size(92, 15)
        Me.lblCurrentBalFrom.TabIndex = 95
        Me.lblCurrentBalFrom.Text = "Current Bal From"
        '
        'dtpTransactionFrom
        '
        Me.dtpTransactionFrom.Checked = False
        Me.dtpTransactionFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTransactionFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTransactionFrom.Location = New System.Drawing.Point(109, 34)
        Me.dtpTransactionFrom.Name = "dtpTransactionFrom"
        Me.dtpTransactionFrom.ShowCheckBox = True
        Me.dtpTransactionFrom.Size = New System.Drawing.Size(115, 21)
        Me.dtpTransactionFrom.TabIndex = 92
        '
        'dtpTransactionTo
        '
        Me.dtpTransactionTo.Checked = False
        Me.dtpTransactionTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTransactionTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTransactionTo.Location = New System.Drawing.Point(261, 34)
        Me.dtpTransactionTo.Name = "dtpTransactionTo"
        Me.dtpTransactionTo.ShowCheckBox = True
        Me.dtpTransactionTo.Size = New System.Drawing.Size(115, 21)
        Me.dtpTransactionTo.TabIndex = 94
        '
        'lblTranToDt
        '
        Me.lblTranToDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranToDt.Location = New System.Drawing.Point(230, 37)
        Me.lblTranToDt.Name = "lblTranToDt"
        Me.lblTranToDt.Size = New System.Drawing.Size(25, 15)
        Me.lblTranToDt.TabIndex = 93
        Me.lblTranToDt.Text = "To"
        Me.lblTranToDt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTranFromDt
        '
        Me.lblTranFromDt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranFromDt.Location = New System.Drawing.Point(11, 37)
        Me.lblTranFromDt.Name = "lblTranFromDt"
        Me.lblTranFromDt.Size = New System.Drawing.Size(92, 15)
        Me.lblTranFromDt.TabIndex = 91
        Me.lblTranFromDt.Text = "Transaction From"
        '
        'cboProjectCode
        '
        Me.cboProjectCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProjectCode.DropDownWidth = 150
        Me.cboProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProjectCode.FormattingEnabled = True
        Me.cboProjectCode.Location = New System.Drawing.Point(489, 35)
        Me.cboProjectCode.Name = "cboProjectCode"
        Me.cboProjectCode.Size = New System.Drawing.Size(134, 21)
        Me.cboProjectCode.TabIndex = 90
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(777, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'lblFundProjectCode
        '
        Me.lblFundProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundProjectCode.Location = New System.Drawing.Point(390, 37)
        Me.lblFundProjectCode.Name = "lblFundProjectCode"
        Me.lblFundProjectCode.Size = New System.Drawing.Size(93, 15)
        Me.lblFundProjectCode.TabIndex = 85
        Me.lblFundProjectCode.Text = "Project Code"
        '
        'objbtnSearchProjectCode
        '
        Me.objbtnSearchProjectCode.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProjectCode.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProjectCode.BorderSelected = False
        Me.objbtnSearchProjectCode.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProjectCode.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProjectCode.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProjectCode.Location = New System.Drawing.Point(629, 35)
        Me.objbtnSearchProjectCode.Name = "objbtnSearchProjectCode"
        Me.objbtnSearchProjectCode.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProjectCode.TabIndex = 88
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(753, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 537)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(827, 55)
        Me.objFooter.TabIndex = 7
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(615, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(512, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(409, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(718, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvFundAdjustment
        '
        Me.dgvFundAdjustment.AllowUserToAddRows = False
        Me.dgvFundAdjustment.AllowUserToDeleteRows = False
        Me.dgvFundAdjustment.AllowUserToResizeRows = False
        Me.dgvFundAdjustment.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvFundAdjustment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvFundAdjustment.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.dgvFundAdjustment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvFundAdjustment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhFundProjectCode, Me.dgcolhTarnsactionDate, Me.dgcolhCurrentBal, Me.dgcolhIncrDecrAmount, Me.dgcolhNewBalance, Me.objdgcolhfundadjustmentunkid, Me.objdgcolhfundProjectCodeunkid})
        Me.dgvFundAdjustment.IgnoreFirstColumn = False
        Me.dgvFundAdjustment.Location = New System.Drawing.Point(12, 110)
        Me.dgvFundAdjustment.MultiSelect = False
        Me.dgvFundAdjustment.Name = "dgvFundAdjustment"
        Me.dgvFundAdjustment.ReadOnly = True
        Me.dgvFundAdjustment.RowHeadersVisible = False
        Me.dgvFundAdjustment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFundAdjustment.Size = New System.Drawing.Size(803, 421)
        Me.dgvFundAdjustment.TabIndex = 6
        '
        'dgcolhFundProjectCode
        '
        Me.dgcolhFundProjectCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFundProjectCode.HeaderText = "Project Code"
        Me.dgcolhFundProjectCode.Name = "dgcolhFundProjectCode"
        Me.dgcolhFundProjectCode.ReadOnly = True
        '
        'dgcolhTarnsactionDate
        '
        Me.dgcolhTarnsactionDate.HeaderText = "Tarnsaction Date"
        Me.dgcolhTarnsactionDate.Name = "dgcolhTarnsactionDate"
        Me.dgcolhTarnsactionDate.ReadOnly = True
        '
        'dgcolhCurrentBal
        '
        Me.dgcolhCurrentBal.HeaderText = "Current Balance"
        Me.dgcolhCurrentBal.Name = "dgcolhCurrentBal"
        Me.dgcolhCurrentBal.ReadOnly = True
        Me.dgcolhCurrentBal.Width = 175
        '
        'dgcolhIncrDecrAmount
        '
        Me.dgcolhIncrDecrAmount.HeaderText = "Incr / Decr Amount"
        Me.dgcolhIncrDecrAmount.Name = "dgcolhIncrDecrAmount"
        Me.dgcolhIncrDecrAmount.ReadOnly = True
        Me.dgcolhIncrDecrAmount.Width = 150
        '
        'dgcolhNewBalance
        '
        Me.dgcolhNewBalance.HeaderText = "New Balance"
        Me.dgcolhNewBalance.Name = "dgcolhNewBalance"
        Me.dgcolhNewBalance.ReadOnly = True
        Me.dgcolhNewBalance.Width = 175
        '
        'objdgcolhfundadjustmentunkid
        '
        Me.objdgcolhfundadjustmentunkid.HeaderText = "objdgcolhfundadjustmentunkid"
        Me.objdgcolhfundadjustmentunkid.Name = "objdgcolhfundadjustmentunkid"
        Me.objdgcolhfundadjustmentunkid.ReadOnly = True
        Me.objdgcolhfundadjustmentunkid.Visible = False
        Me.objdgcolhfundadjustmentunkid.Width = 5
        '
        'objdgcolhfundProjectCodeunkid
        '
        Me.objdgcolhfundProjectCodeunkid.HeaderText = "objdgcolhfundsourceunkid"
        Me.objdgcolhfundProjectCodeunkid.Name = "objdgcolhfundProjectCodeunkid"
        Me.objdgcolhfundProjectCodeunkid.ReadOnly = True
        Me.objdgcolhfundProjectCodeunkid.Visible = False
        Me.objdgcolhfundProjectCodeunkid.Width = 5
        '
        'frmFundAdjustment_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(827, 592)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.dgvFundAdjustment)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundAdjustment_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "List of Project Code Adjustments"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvFundAdjustment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchProjectCode As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFundProjectCode As System.Windows.Forms.Label
    'Friend WithEvents dgvFundAdjustment As System.Windows.Forms.DataGridView
    Friend WithEvents dgvFundAdjustment As Aruti.Data.GroupByGrid
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboProjectCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranFromDt As System.Windows.Forms.Label
    Friend WithEvents dtpTransactionTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTranToDt As System.Windows.Forms.Label
    Friend WithEvents dtpTransactionFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrentBalTo As System.Windows.Forms.Label
    Friend WithEvents lblCurrentBalFrom As System.Windows.Forms.Label
    Friend WithEvents txtCurrentBalTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtCurrentBalFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents dgcolhFundProjectCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTarnsactionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCurrentBal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncrDecrAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNewBalance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfundadjustmentunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhfundProjectCodeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFundSources_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFundSources_AddEdit))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFundSources = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtNotifyAmount = New eZee.TextBox.IntegerTextBox
        Me.lblNotifyAmount = New System.Windows.Forms.Label
        Me.txtProjectCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblProjectCode = New System.Windows.Forms.Label
        Me.dtExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.lblExpiryDate = New System.Windows.Forms.Label
        Me.txtFundName = New eZee.TextBox.AlphanumericTextBox
        Me.lblFundName = New System.Windows.Forms.Label
        Me.txtFundCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtCurrentCeilingBal = New eZee.TextBox.NumericTextBox
        Me.lblCurrentCeilingBal = New System.Windows.Forms.Label
        Me.lblFundCode = New System.Windows.Forms.Label
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.objFooter.SuspendLayout()
        Me.gbFundSources.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 87)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(409, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(304, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(204, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbFundSources
        '
        Me.gbFundSources.BorderColor = System.Drawing.Color.Black
        Me.gbFundSources.Checked = False
        Me.gbFundSources.CollapseAllExceptThis = False
        Me.gbFundSources.CollapsedHoverImage = Nothing
        Me.gbFundSources.CollapsedNormalImage = Nothing
        Me.gbFundSources.CollapsedPressedImage = Nothing
        Me.gbFundSources.CollapseOnLoad = False
        Me.gbFundSources.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbFundSources.Controls.Add(Me.txtNotifyAmount)
        Me.gbFundSources.Controls.Add(Me.lblNotifyAmount)
        Me.gbFundSources.Controls.Add(Me.txtProjectCode)
        Me.gbFundSources.Controls.Add(Me.lblProjectCode)
        Me.gbFundSources.Controls.Add(Me.dtExpiryDate)
        Me.gbFundSources.Controls.Add(Me.lblExpiryDate)
        Me.gbFundSources.Controls.Add(Me.txtFundName)
        Me.gbFundSources.Controls.Add(Me.lblFundName)
        Me.gbFundSources.Controls.Add(Me.txtFundCode)
        Me.gbFundSources.Controls.Add(Me.txtCurrentCeilingBal)
        Me.gbFundSources.Controls.Add(Me.lblCurrentCeilingBal)
        Me.gbFundSources.Controls.Add(Me.lblFundCode)
        Me.gbFundSources.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFundSources.ExpandedHoverImage = Nothing
        Me.gbFundSources.ExpandedNormalImage = Nothing
        Me.gbFundSources.ExpandedPressedImage = Nothing
        Me.gbFundSources.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFundSources.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFundSources.HeaderHeight = 25
        Me.gbFundSources.HeaderMessage = ""
        Me.gbFundSources.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFundSources.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFundSources.HeightOnCollapse = 0
        Me.gbFundSources.LeftTextSpace = 0
        Me.gbFundSources.Location = New System.Drawing.Point(0, 0)
        Me.gbFundSources.Name = "gbFundSources"
        Me.gbFundSources.OpenHeight = 300
        Me.gbFundSources.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFundSources.ShowBorder = True
        Me.gbFundSources.ShowCheckBox = False
        Me.gbFundSources.ShowCollapseButton = False
        Me.gbFundSources.ShowDefaultBorderColor = True
        Me.gbFundSources.ShowDownButton = False
        Me.gbFundSources.ShowHeader = True
        Me.gbFundSources.Size = New System.Drawing.Size(409, 87)
        Me.gbFundSources.TabIndex = 0
        Me.gbFundSources.Temp = 0
        Me.gbFundSources.Text = "Fund Sources Information"
        Me.gbFundSources.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNotifyAmount
        '
        Me.txtNotifyAmount.AllowNegative = False
        Me.txtNotifyAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNotifyAmount.DigitsInGroup = 0
        Me.txtNotifyAmount.Flags = 65536
        Me.txtNotifyAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNotifyAmount.Location = New System.Drawing.Point(152, 167)
        Me.txtNotifyAmount.MaxDecimalPlaces = 4
        Me.txtNotifyAmount.MaxWholeDigits = 21
        Me.txtNotifyAmount.Name = "txtNotifyAmount"
        Me.txtNotifyAmount.Prefix = ""
        Me.txtNotifyAmount.RangeMax = 2147483647
        Me.txtNotifyAmount.RangeMin = -2147483648
        Me.txtNotifyAmount.Size = New System.Drawing.Size(130, 21)
        Me.txtNotifyAmount.TabIndex = 5
        Me.txtNotifyAmount.Text = "0"
        Me.txtNotifyAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNotifyAmount.Visible = False
        '
        'lblNotifyAmount
        '
        Me.lblNotifyAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNotifyAmount.Location = New System.Drawing.Point(15, 167)
        Me.lblNotifyAmount.Name = "lblNotifyAmount"
        Me.lblNotifyAmount.Size = New System.Drawing.Size(131, 29)
        Me.lblNotifyAmount.TabIndex = 13
        Me.lblNotifyAmount.Text = "Notify me when Amounts falls below"
        Me.lblNotifyAmount.Visible = False
        '
        'txtProjectCode
        '
        Me.txtProjectCode.Flags = 0
        Me.txtProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProjectCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtProjectCode.Location = New System.Drawing.Point(152, 86)
        Me.txtProjectCode.Name = "txtProjectCode"
        Me.txtProjectCode.Size = New System.Drawing.Size(224, 21)
        Me.txtProjectCode.TabIndex = 2
        Me.txtProjectCode.Visible = False
        '
        'lblProjectCode
        '
        Me.lblProjectCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProjectCode.Location = New System.Drawing.Point(12, 88)
        Me.lblProjectCode.Name = "lblProjectCode"
        Me.lblProjectCode.Size = New System.Drawing.Size(125, 16)
        Me.lblProjectCode.TabIndex = 87
        Me.lblProjectCode.Text = "Project Code"
        Me.lblProjectCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblProjectCode.Visible = False
        '
        'dtExpiryDate
        '
        Me.dtExpiryDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtExpiryDate.Checked = False
        Me.dtExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtExpiryDate.Location = New System.Drawing.Point(152, 140)
        Me.dtExpiryDate.Name = "dtExpiryDate"
        Me.dtExpiryDate.Size = New System.Drawing.Size(101, 21)
        Me.dtExpiryDate.TabIndex = 4
        Me.dtExpiryDate.Visible = False
        '
        'lblExpiryDate
        '
        Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpiryDate.Location = New System.Drawing.Point(12, 142)
        Me.lblExpiryDate.Name = "lblExpiryDate"
        Me.lblExpiryDate.Size = New System.Drawing.Size(125, 16)
        Me.lblExpiryDate.TabIndex = 84
        Me.lblExpiryDate.Text = "Expiry Date"
        Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblExpiryDate.Visible = False
        '
        'txtFundName
        '
        Me.txtFundName.Flags = 0
        Me.txtFundName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFundName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFundName.Location = New System.Drawing.Point(152, 59)
        Me.txtFundName.Name = "txtFundName"
        Me.txtFundName.Size = New System.Drawing.Size(224, 21)
        Me.txtFundName.TabIndex = 1
        '
        'lblFundName
        '
        Me.lblFundName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundName.Location = New System.Drawing.Point(12, 61)
        Me.lblFundName.Name = "lblFundName"
        Me.lblFundName.Size = New System.Drawing.Size(125, 16)
        Me.lblFundName.TabIndex = 82
        Me.lblFundName.Text = "Fund Name"
        Me.lblFundName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFundCode
        '
        Me.txtFundCode.Flags = 0
        Me.txtFundCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFundCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFundCode.Location = New System.Drawing.Point(152, 32)
        Me.txtFundCode.Name = "txtFundCode"
        Me.txtFundCode.Size = New System.Drawing.Size(106, 21)
        Me.txtFundCode.TabIndex = 0
        '
        'txtCurrentCeilingBal
        '
        Me.txtCurrentCeilingBal.AllowNegative = True
        Me.txtCurrentCeilingBal.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCurrentCeilingBal.DigitsInGroup = 0
        Me.txtCurrentCeilingBal.Flags = 0
        Me.txtCurrentCeilingBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentCeilingBal.Location = New System.Drawing.Point(152, 113)
        Me.txtCurrentCeilingBal.MaxDecimalPlaces = 6
        Me.txtCurrentCeilingBal.MaxWholeDigits = 21
        Me.txtCurrentCeilingBal.Name = "txtCurrentCeilingBal"
        Me.txtCurrentCeilingBal.Prefix = ""
        Me.txtCurrentCeilingBal.RangeMax = 1.7976931348623157E+308
        Me.txtCurrentCeilingBal.RangeMin = -1.7976931348623157E+308
        Me.txtCurrentCeilingBal.ReadOnly = True
        Me.txtCurrentCeilingBal.Size = New System.Drawing.Size(224, 21)
        Me.txtCurrentCeilingBal.TabIndex = 3
        Me.txtCurrentCeilingBal.Text = "0"
        Me.txtCurrentCeilingBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCurrentCeilingBal.Visible = False
        '
        'lblCurrentCeilingBal
        '
        Me.lblCurrentCeilingBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentCeilingBal.Location = New System.Drawing.Point(12, 115)
        Me.lblCurrentCeilingBal.Name = "lblCurrentCeilingBal"
        Me.lblCurrentCeilingBal.Size = New System.Drawing.Size(125, 16)
        Me.lblCurrentCeilingBal.TabIndex = 77
        Me.lblCurrentCeilingBal.Text = "Current Ceiling Balance"
        Me.lblCurrentCeilingBal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCurrentCeilingBal.Visible = False
        '
        'lblFundCode
        '
        Me.lblFundCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFundCode.Location = New System.Drawing.Point(12, 34)
        Me.lblFundCode.Name = "lblFundCode"
        Me.lblFundCode.Size = New System.Drawing.Size(125, 16)
        Me.lblFundCode.TabIndex = 12
        Me.lblFundCode.Text = "Fund Code"
        Me.lblFundCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(382, 59)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(22, 22)
        Me.objbtnOtherLanguage.TabIndex = 89
        '
        'frmFundSources_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 137)
        Me.Controls.Add(Me.gbFundSources)
        Me.Controls.Add(Me.objFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFundSources_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Fund Sources Add/Edit"
        Me.objFooter.ResumeLayout(False)
        Me.gbFundSources.ResumeLayout(False)
        Me.gbFundSources.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbFundSources As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblFundCode As System.Windows.Forms.Label
    Friend WithEvents txtCurrentCeilingBal As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCurrentCeilingBal As System.Windows.Forms.Label
    Friend WithEvents txtFundCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFundName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFundName As System.Windows.Forms.Label
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents dtExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtProjectCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblProjectCode As System.Windows.Forms.Label
    Friend WithEvents txtNotifyAmount As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblNotifyAmount As System.Windows.Forms.Label
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
End Class

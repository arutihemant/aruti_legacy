﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayslipGlobalPayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayslipGlobalPayment))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbAdvanceAmountInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.txtTotalPaymentAmount = New eZee.TextBox.NumericTextBox
        Me.lblTotalPaymentAmount = New System.Windows.Forms.Label
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblEmpCount = New System.Windows.Forms.Label
        Me.pnlEmployeeList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvEmployeeList = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhVoucherNo = New System.Windows.Forms.ColumnHeader
        Me.colhTnAleaveUnkID = New System.Windows.Forms.ColumnHeader
        Me.colhBaseAmount = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.lblPayType = New System.Windows.Forms.Label
        Me.cboPayPoint = New System.Windows.Forms.ComboBox
        Me.lblPayPoint = New System.Windows.Forms.Label
        Me.txtCutOffAmount = New eZee.TextBox.NumericTextBox
        Me.lblCutOffAmount = New System.Windows.Forms.Label
        Me.cboEmpBranch = New System.Windows.Forms.ComboBox
        Me.lblEmpBranch = New System.Windows.Forms.Label
        Me.cboEmpBank = New System.Windows.Forms.ComboBox
        Me.lblEmpBank = New System.Windows.Forms.Label
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbPaymentInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPaymentDatePeriod = New System.Windows.Forms.ComboBox
        Me.lblPaymentDatePeriod = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.cboAccountNo = New System.Windows.Forms.ComboBox
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblACHolder = New System.Windows.Forms.Label
        Me.lblCashPerc = New System.Windows.Forms.Label
        Me.lblCashDescr = New System.Windows.Forms.Label
        Me.txtCashPerc = New eZee.TextBox.NumericTextBox
        Me.txtVoucher = New eZee.TextBox.AlphanumericTextBox
        Me.lblAgainstVoucher = New System.Windows.Forms.Label
        Me.txtChequeNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCheque = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.lblPaymentMode = New System.Windows.Forms.Label
        Me.cboPaymentMode = New System.Windows.Forms.ComboBox
        Me.lblPaymentDate = New System.Windows.Forms.Label
        Me.dtpPaymentDate = New System.Windows.Forms.DateTimePicker
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.cboPayYear = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.objlblColor = New System.Windows.Forms.Label
        Me.btnProcess = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbgwProcess = New System.ComponentModel.BackgroundWorker
        Me.lnkPayrollTotalVarianceReport = New System.Windows.Forms.LinkLabel
        Me.lnkPayrollVarianceReport = New System.Windows.Forms.LinkLabel
        Me.pnlMainInfo.SuspendLayout()
        Me.gbAdvanceAmountInfo.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbEmployeeList.SuspendLayout()
        Me.pnlEmployeeList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPaymentInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lnkPayrollTotalVarianceReport)
        Me.pnlMainInfo.Controls.Add(Me.lnkPayrollVarianceReport)
        Me.pnlMainInfo.Controls.Add(Me.gbAdvanceAmountInfo)
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbPaymentInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(861, 601)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbAdvanceAmountInfo
        '
        Me.gbAdvanceAmountInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.Checked = False
        Me.gbAdvanceAmountInfo.CollapseAllExceptThis = False
        Me.gbAdvanceAmountInfo.CollapsedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.CollapsedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.CollapseOnLoad = False
        Me.gbAdvanceAmountInfo.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbAdvanceAmountInfo.ExpandedHoverImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedNormalImage = Nothing
        Me.gbAdvanceAmountInfo.ExpandedPressedImage = Nothing
        Me.gbAdvanceAmountInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAdvanceAmountInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAdvanceAmountInfo.HeaderHeight = 25
        Me.gbAdvanceAmountInfo.HeaderMessage = ""
        Me.gbAdvanceAmountInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAdvanceAmountInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAdvanceAmountInfo.HeightOnCollapse = 0
        Me.gbAdvanceAmountInfo.LeftTextSpace = 0
        Me.gbAdvanceAmountInfo.Location = New System.Drawing.Point(12, 462)
        Me.gbAdvanceAmountInfo.Name = "gbAdvanceAmountInfo"
        Me.gbAdvanceAmountInfo.OpenHeight = 300
        Me.gbAdvanceAmountInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAdvanceAmountInfo.ShowBorder = True
        Me.gbAdvanceAmountInfo.ShowCheckBox = False
        Me.gbAdvanceAmountInfo.ShowCollapseButton = False
        Me.gbAdvanceAmountInfo.ShowDefaultBorderColor = True
        Me.gbAdvanceAmountInfo.ShowDownButton = False
        Me.gbAdvanceAmountInfo.ShowHeader = True
        Me.gbAdvanceAmountInfo.Size = New System.Drawing.Size(329, 59)
        Me.gbAdvanceAmountInfo.TabIndex = 3
        Me.gbAdvanceAmountInfo.Temp = 0
        Me.gbAdvanceAmountInfo.Text = "Total Payment Amount"
        Me.gbAdvanceAmountInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.txtTotalPaymentAmount)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblTotalPaymentAmount)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(324, 31)
        Me.pnlAdvanceTaken.TabIndex = 0
        '
        'txtTotalPaymentAmount
        '
        Me.txtTotalPaymentAmount.AllowNegative = True
        Me.txtTotalPaymentAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalPaymentAmount.DigitsInGroup = 3
        Me.txtTotalPaymentAmount.Flags = 0
        Me.txtTotalPaymentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalPaymentAmount.Location = New System.Drawing.Point(152, 6)
        Me.txtTotalPaymentAmount.MaxDecimalPlaces = 6
        Me.txtTotalPaymentAmount.MaxWholeDigits = 21
        Me.txtTotalPaymentAmount.Name = "txtTotalPaymentAmount"
        Me.txtTotalPaymentAmount.Prefix = ""
        Me.txtTotalPaymentAmount.RangeMax = 1.7976931348623157E+308
        Me.txtTotalPaymentAmount.RangeMin = -1.7976931348623157E+308
        Me.txtTotalPaymentAmount.ReadOnly = True
        Me.txtTotalPaymentAmount.Size = New System.Drawing.Size(167, 21)
        Me.txtTotalPaymentAmount.TabIndex = 0
        Me.txtTotalPaymentAmount.Text = "0"
        Me.txtTotalPaymentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotalPaymentAmount
        '
        Me.lblTotalPaymentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalPaymentAmount.Location = New System.Drawing.Point(4, 8)
        Me.lblTotalPaymentAmount.Name = "lblTotalPaymentAmount"
        Me.lblTotalPaymentAmount.Size = New System.Drawing.Size(142, 16)
        Me.lblTotalPaymentAmount.TabIndex = 0
        Me.lblTotalPaymentAmount.Text = "Total Payment Amount"
        Me.lblTotalPaymentAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.objlblEmpCount)
        Me.gbEmployeeList.Controls.Add(Me.pnlEmployeeList)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(12, 66)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(329, 391)
        Me.gbEmployeeList.TabIndex = 2
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblEmpCount
        '
        Me.objlblEmpCount.BackColor = System.Drawing.Color.Transparent
        Me.objlblEmpCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpCount.Location = New System.Drawing.Point(182, 4)
        Me.objlblEmpCount.Name = "objlblEmpCount"
        Me.objlblEmpCount.Size = New System.Drawing.Size(71, 16)
        Me.objlblEmpCount.TabIndex = 161
        Me.objlblEmpCount.Text = "( 0 )"
        Me.objlblEmpCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlEmployeeList
        '
        Me.pnlEmployeeList.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployeeList.Controls.Add(Me.lvEmployeeList)
        Me.pnlEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeList.Location = New System.Drawing.Point(1, 26)
        Me.pnlEmployeeList.Name = "pnlEmployeeList"
        Me.pnlEmployeeList.Size = New System.Drawing.Size(327, 364)
        Me.pnlEmployeeList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 17
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvEmployeeList
        '
        Me.lvEmployeeList.CheckBoxes = True
        Me.lvEmployeeList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhCode, Me.colhName, Me.colhAmount, Me.colhVoucherNo, Me.colhTnAleaveUnkID, Me.colhBaseAmount})
        Me.lvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvEmployeeList.FullRowSelect = True
        Me.lvEmployeeList.GridLines = True
        Me.lvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.lvEmployeeList.Name = "lvEmployeeList"
        Me.lvEmployeeList.Size = New System.Drawing.Size(327, 364)
        Me.lvEmployeeList.TabIndex = 0
        Me.lvEmployeeList.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeList.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhCode
        '
        Me.colhCode.Tag = ""
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 0
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 150
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Balance Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 140
        '
        'colhVoucherNo
        '
        Me.colhVoucherNo.Text = "Voucher No"
        Me.colhVoucherNo.Width = 0
        '
        'colhTnAleaveUnkID
        '
        Me.colhTnAleaveUnkID.Text = "TnALeaveUnkID"
        Me.colhTnAleaveUnkID.Width = 0
        '
        'colhBaseAmount
        '
        Me.colhBaseAmount.Tag = "colhBaseAmount"
        Me.colhBaseAmount.Text = "Base Amt"
        Me.colhBaseAmount.Width = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboPayType)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayType)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPoint)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPoint)
        Me.gbFilterCriteria.Controls.Add(Me.txtCutOffAmount)
        Me.gbFilterCriteria.Controls.Add(Me.lblCutOffAmount)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmpBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmpBank)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpBank)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(347, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(502, 118)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Employee Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayType
        '
        Me.cboPayType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(358, 87)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(130, 21)
        Me.cboPayType.TabIndex = 5
        '
        'lblPayType
        '
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(266, 90)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(86, 15)
        Me.lblPayType.TabIndex = 321
        Me.lblPayType.Text = "Pay Type"
        '
        'cboPayPoint
        '
        Me.cboPayPoint.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboPayPoint.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPayPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPoint.FormattingEnabled = True
        Me.cboPayPoint.Location = New System.Drawing.Point(100, 87)
        Me.cboPayPoint.Name = "cboPayPoint"
        Me.cboPayPoint.Size = New System.Drawing.Size(130, 21)
        Me.cboPayPoint.TabIndex = 4
        '
        'lblPayPoint
        '
        Me.lblPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPoint.Location = New System.Drawing.Point(8, 90)
        Me.lblPayPoint.Name = "lblPayPoint"
        Me.lblPayPoint.Size = New System.Drawing.Size(86, 15)
        Me.lblPayPoint.TabIndex = 319
        Me.lblPayPoint.Text = "Pay Point"
        '
        'txtCutOffAmount
        '
        Me.txtCutOffAmount.AllowNegative = True
        Me.txtCutOffAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCutOffAmount.DigitsInGroup = 3
        Me.txtCutOffAmount.Flags = 0
        Me.txtCutOffAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCutOffAmount.Location = New System.Drawing.Point(358, 33)
        Me.txtCutOffAmount.MaxDecimalPlaces = 6
        Me.txtCutOffAmount.MaxWholeDigits = 21
        Me.txtCutOffAmount.Name = "txtCutOffAmount"
        Me.txtCutOffAmount.Prefix = ""
        Me.txtCutOffAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCutOffAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCutOffAmount.Size = New System.Drawing.Size(130, 21)
        Me.txtCutOffAmount.TabIndex = 1
        Me.txtCutOffAmount.Text = "0"
        Me.txtCutOffAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCutOffAmount.Visible = False
        '
        'lblCutOffAmount
        '
        Me.lblCutOffAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCutOffAmount.Location = New System.Drawing.Point(266, 35)
        Me.lblCutOffAmount.Name = "lblCutOffAmount"
        Me.lblCutOffAmount.Size = New System.Drawing.Size(86, 16)
        Me.lblCutOffAmount.TabIndex = 315
        Me.lblCutOffAmount.Text = "Cut Off Amount"
        Me.lblCutOffAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCutOffAmount.Visible = False
        '
        'cboEmpBranch
        '
        Me.cboEmpBranch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmpBranch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmpBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpBranch.DropDownWidth = 200
        Me.cboEmpBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpBranch.FormattingEnabled = True
        Me.cboEmpBranch.Location = New System.Drawing.Point(358, 60)
        Me.cboEmpBranch.Name = "cboEmpBranch"
        Me.cboEmpBranch.Size = New System.Drawing.Size(130, 21)
        Me.cboEmpBranch.TabIndex = 3
        '
        'lblEmpBranch
        '
        Me.lblEmpBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpBranch.Location = New System.Drawing.Point(266, 63)
        Me.lblEmpBranch.Name = "lblEmpBranch"
        Me.lblEmpBranch.Size = New System.Drawing.Size(86, 15)
        Me.lblEmpBranch.TabIndex = 314
        Me.lblEmpBranch.Text = "Emp. Branch"
        '
        'cboEmpBank
        '
        Me.cboEmpBank.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmpBank.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmpBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpBank.DropDownWidth = 200
        Me.cboEmpBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpBank.FormattingEnabled = True
        Me.cboEmpBank.Location = New System.Drawing.Point(100, 60)
        Me.cboEmpBank.Name = "cboEmpBank"
        Me.cboEmpBank.Size = New System.Drawing.Size(130, 21)
        Me.cboEmpBank.TabIndex = 2
        '
        'lblEmpBank
        '
        Me.lblEmpBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpBank.Location = New System.Drawing.Point(8, 63)
        Me.lblEmpBank.Name = "lblEmpBank"
        Me.lblEmpBank.Size = New System.Drawing.Size(86, 15)
        Me.lblEmpBank.TabIndex = 312
        Me.lblEmpBank.Text = "Emp. Bank"
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(313, 6)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(128, 13)
        Me.lnkAdvanceFilter.TabIndex = 309
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = CType(resources.GetObject("objbtnSearchEmployee.Image"), System.Drawing.Image)
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(236, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(100, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(130, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(475, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(452, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(861, 60)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Payslip Global Payment"
        '
        'gbPaymentInfo
        '
        Me.gbPaymentInfo.BorderColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.Checked = False
        Me.gbPaymentInfo.CollapseAllExceptThis = False
        Me.gbPaymentInfo.CollapsedHoverImage = Nothing
        Me.gbPaymentInfo.CollapsedNormalImage = Nothing
        Me.gbPaymentInfo.CollapsedPressedImage = Nothing
        Me.gbPaymentInfo.CollapseOnLoad = False
        Me.gbPaymentInfo.Controls.Add(Me.cboPaymentDatePeriod)
        Me.gbPaymentInfo.Controls.Add(Me.lblPaymentDatePeriod)
        Me.gbPaymentInfo.Controls.Add(Me.txtRemarks)
        Me.gbPaymentInfo.Controls.Add(Me.lblRemarks)
        Me.gbPaymentInfo.Controls.Add(Me.cboAccountNo)
        Me.gbPaymentInfo.Controls.Add(Me.lblAccountNo)
        Me.gbPaymentInfo.Controls.Add(Me.objlblExRate)
        Me.gbPaymentInfo.Controls.Add(Me.cboCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.lblCurrency)
        Me.gbPaymentInfo.Controls.Add(Me.lblACHolder)
        Me.gbPaymentInfo.Controls.Add(Me.lblCashPerc)
        Me.gbPaymentInfo.Controls.Add(Me.lblCashDescr)
        Me.gbPaymentInfo.Controls.Add(Me.txtCashPerc)
        Me.gbPaymentInfo.Controls.Add(Me.txtVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.lblAgainstVoucher)
        Me.gbPaymentInfo.Controls.Add(Me.txtChequeNo)
        Me.gbPaymentInfo.Controls.Add(Me.lblCheque)
        Me.gbPaymentInfo.Controls.Add(Me.cboBranch)
        Me.gbPaymentInfo.Controls.Add(Me.lblBranch)
        Me.gbPaymentInfo.Controls.Add(Me.cboBankGroup)
        Me.gbPaymentInfo.Controls.Add(Me.lblBankGroup)
        Me.gbPaymentInfo.Controls.Add(Me.lblPaymentMode)
        Me.gbPaymentInfo.Controls.Add(Me.cboPaymentMode)
        Me.gbPaymentInfo.Controls.Add(Me.lblPaymentDate)
        Me.gbPaymentInfo.Controls.Add(Me.dtpPaymentDate)
        Me.gbPaymentInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbPaymentInfo.Controls.Add(Me.cboPayYear)
        Me.gbPaymentInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbPaymentInfo.Controls.Add(Me.lblPayYear)
        Me.gbPaymentInfo.ExpandedHoverImage = Nothing
        Me.gbPaymentInfo.ExpandedNormalImage = Nothing
        Me.gbPaymentInfo.ExpandedPressedImage = Nothing
        Me.gbPaymentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPaymentInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPaymentInfo.HeaderHeight = 25
        Me.gbPaymentInfo.HeaderMessage = ""
        Me.gbPaymentInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPaymentInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPaymentInfo.HeightOnCollapse = 0
        Me.gbPaymentInfo.LeftTextSpace = 0
        Me.gbPaymentInfo.Location = New System.Drawing.Point(347, 191)
        Me.gbPaymentInfo.Name = "gbPaymentInfo"
        Me.gbPaymentInfo.OpenHeight = 182
        Me.gbPaymentInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPaymentInfo.ShowBorder = True
        Me.gbPaymentInfo.ShowCheckBox = False
        Me.gbPaymentInfo.ShowCollapseButton = False
        Me.gbPaymentInfo.ShowDefaultBorderColor = True
        Me.gbPaymentInfo.ShowDownButton = False
        Me.gbPaymentInfo.ShowHeader = True
        Me.gbPaymentInfo.Size = New System.Drawing.Size(502, 330)
        Me.gbPaymentInfo.TabIndex = 0
        Me.gbPaymentInfo.Temp = 0
        Me.gbPaymentInfo.Text = "Payment Information"
        Me.gbPaymentInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentDatePeriod
        '
        Me.cboPaymentDatePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentDatePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentDatePeriod.FormattingEnabled = True
        Me.cboPaymentDatePeriod.Location = New System.Drawing.Point(135, 61)
        Me.cboPaymentDatePeriod.Name = "cboPaymentDatePeriod"
        Me.cboPaymentDatePeriod.Size = New System.Drawing.Size(155, 21)
        Me.cboPaymentDatePeriod.TabIndex = 1
        '
        'lblPaymentDatePeriod
        '
        Me.lblPaymentDatePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDatePeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPaymentDatePeriod.Name = "lblPaymentDatePeriod"
        Me.lblPaymentDatePeriod.Size = New System.Drawing.Size(121, 16)
        Me.lblPaymentDatePeriod.TabIndex = 214
        Me.lblPaymentDatePeriod.Text = "Payment Date Period"
        Me.lblPaymentDatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(135, 277)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(320, 44)
        Me.txtRemarks.TabIndex = 10
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(8, 279)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(121, 16)
        Me.lblRemarks.TabIndex = 211
        Me.lblRemarks.Text = "Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccountNo
        '
        Me.cboAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountNo.FormattingEnabled = True
        Me.cboAccountNo.Location = New System.Drawing.Point(135, 223)
        Me.cboAccountNo.Name = "cboAccountNo"
        Me.cboAccountNo.Size = New System.Drawing.Size(155, 21)
        Me.cboAccountNo.TabIndex = 8
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(8, 225)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(121, 16)
        Me.lblAccountNo.TabIndex = 208
        Me.lblAccountNo.Text = "Account No."
        Me.lblAccountNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(291, 90)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(208, 16)
        Me.objlblExRate.TabIndex = 205
        Me.objlblExRate.Text = "1"
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(358, 115)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(97, 21)
        Me.cboCurrency.TabIndex = 4
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(296, 117)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(56, 16)
        Me.lblCurrency.TabIndex = 204
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblACHolder
        '
        Me.lblACHolder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblACHolder.Location = New System.Drawing.Point(317, 144)
        Me.lblACHolder.Name = "lblACHolder"
        Me.lblACHolder.Size = New System.Drawing.Size(182, 16)
        Me.lblACHolder.TabIndex = 189
        Me.lblACHolder.Text = "To Non Bank A/C Holder"
        Me.lblACHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCashPerc
        '
        Me.lblCashPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashPerc.Location = New System.Drawing.Point(296, 144)
        Me.lblCashPerc.Name = "lblCashPerc"
        Me.lblCashPerc.Size = New System.Drawing.Size(15, 16)
        Me.lblCashPerc.TabIndex = 187
        Me.lblCashPerc.Text = "%"
        Me.lblCashPerc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCashDescr
        '
        Me.lblCashDescr.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCashDescr.Location = New System.Drawing.Point(299, 171)
        Me.lblCashDescr.Name = "lblCashDescr"
        Me.lblCashDescr.Size = New System.Drawing.Size(191, 73)
        Me.lblCashDescr.TabIndex = 186
        Me.lblCashDescr.Text = "Each Selected Employee will get the amount of above given percentage of their Bal" & _
            "ance Amount."
        '
        'txtCashPerc
        '
        Me.txtCashPerc.AllowNegative = False
        Me.txtCashPerc.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCashPerc.DigitsInGroup = 0
        Me.txtCashPerc.Flags = 65536
        Me.txtCashPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCashPerc.Location = New System.Drawing.Point(245, 142)
        Me.txtCashPerc.MaxDecimalPlaces = 6
        Me.txtCashPerc.MaxWholeDigits = 3
        Me.txtCashPerc.Name = "txtCashPerc"
        Me.txtCashPerc.Prefix = ""
        Me.txtCashPerc.RangeMax = 1.7976931348623157E+308
        Me.txtCashPerc.RangeMin = -1.7976931348623157E+308
        Me.txtCashPerc.Size = New System.Drawing.Size(45, 21)
        Me.txtCashPerc.TabIndex = 7
        Me.txtCashPerc.Text = "0"
        Me.txtCashPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVoucher
        '
        Me.txtVoucher.Flags = 0
        Me.txtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucher.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucher.Location = New System.Drawing.Point(135, 115)
        Me.txtVoucher.Name = "txtVoucher"
        Me.txtVoucher.Size = New System.Drawing.Size(155, 21)
        Me.txtVoucher.TabIndex = 3
        '
        'lblAgainstVoucher
        '
        Me.lblAgainstVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgainstVoucher.Location = New System.Drawing.Point(8, 117)
        Me.lblAgainstVoucher.Name = "lblAgainstVoucher"
        Me.lblAgainstVoucher.Size = New System.Drawing.Size(121, 16)
        Me.lblAgainstVoucher.TabIndex = 183
        Me.lblAgainstVoucher.Text = "Payment Voucher #"
        Me.lblAgainstVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChequeNo
        '
        Me.txtChequeNo.Flags = 0
        Me.txtChequeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChequeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtChequeNo.Location = New System.Drawing.Point(135, 250)
        Me.txtChequeNo.Name = "txtChequeNo"
        Me.txtChequeNo.Size = New System.Drawing.Size(155, 21)
        Me.txtChequeNo.TabIndex = 9
        '
        'lblCheque
        '
        Me.lblCheque.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCheque.Location = New System.Drawing.Point(8, 252)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(121, 16)
        Me.lblCheque.TabIndex = 172
        Me.lblCheque.Text = "Cheque No."
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(135, 196)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(155, 21)
        Me.cboBranch.TabIndex = 7
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 198)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(121, 16)
        Me.lblBranch.TabIndex = 170
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.Location = New System.Drawing.Point(135, 169)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(155, 21)
        Me.cboBankGroup.TabIndex = 6
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(8, 171)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(121, 16)
        Me.lblBankGroup.TabIndex = 168
        Me.lblBankGroup.Text = "Bank Group"
        Me.lblBankGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaymentMode
        '
        Me.lblPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentMode.Location = New System.Drawing.Point(8, 144)
        Me.lblPaymentMode.Name = "lblPaymentMode"
        Me.lblPaymentMode.Size = New System.Drawing.Size(121, 16)
        Me.lblPaymentMode.TabIndex = 162
        Me.lblPaymentMode.Text = "Payment Mode"
        Me.lblPaymentMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentMode
        '
        Me.cboPaymentMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentMode.FormattingEnabled = True
        Me.cboPaymentMode.Location = New System.Drawing.Point(135, 142)
        Me.cboPaymentMode.Name = "cboPaymentMode"
        Me.cboPaymentMode.Size = New System.Drawing.Size(104, 21)
        Me.cboPaymentMode.TabIndex = 5
        '
        'lblPaymentDate
        '
        Me.lblPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDate.Location = New System.Drawing.Point(8, 90)
        Me.lblPaymentDate.Name = "lblPaymentDate"
        Me.lblPaymentDate.Size = New System.Drawing.Size(121, 16)
        Me.lblPaymentDate.TabIndex = 155
        Me.lblPaymentDate.Text = "Payment Date"
        Me.lblPaymentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPaymentDate
        '
        Me.dtpPaymentDate.Checked = False
        Me.dtpPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPaymentDate.Location = New System.Drawing.Point(135, 88)
        Me.dtpPaymentDate.Name = "dtpPaymentDate"
        Me.dtpPaymentDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpPaymentDate.TabIndex = 2
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(135, 34)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(155, 21)
        Me.cboPayPeriod.TabIndex = 0
        '
        'cboPayYear
        '
        Me.cboPayYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYear.FormattingEnabled = True
        Me.cboPayYear.Location = New System.Drawing.Point(321, 62)
        Me.cboPayYear.Name = "cboPayYear"
        Me.cboPayYear.Size = New System.Drawing.Size(155, 21)
        Me.cboPayYear.TabIndex = 11
        Me.cboPayYear.Visible = False
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(121, 16)
        Me.lblPayPeriod.TabIndex = 141
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(320, 43)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(121, 16)
        Me.lblPayYear.TabIndex = 139
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayYear.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.lblMessage)
        Me.objFooter.Controls.Add(Me.objlblColor)
        Me.objFooter.Controls.Add(Me.btnProcess)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 546)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(861, 55)
        Me.objFooter.TabIndex = 2
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(502, 17)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 217
        '
        'lblMessage
        '
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(12, 6)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(484, 43)
        Me.lblMessage.TabIndex = 89
        '
        'objlblColor
        '
        Me.objlblColor.BackColor = System.Drawing.Color.Red
        Me.objlblColor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblColor.Location = New System.Drawing.Point(594, 32)
        Me.objlblColor.Name = "objlblColor"
        Me.objlblColor.Size = New System.Drawing.Size(49, 15)
        Me.objlblColor.TabIndex = 88
        '
        'btnProcess
        '
        Me.btnProcess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnProcess.BackColor = System.Drawing.Color.White
        Me.btnProcess.BackgroundImage = CType(resources.GetObject("btnProcess.BackgroundImage"), System.Drawing.Image)
        Me.btnProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnProcess.BorderColor = System.Drawing.Color.Empty
        Me.btnProcess.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnProcess.FlatAppearance.BorderSize = 0
        Me.btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcess.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess.ForeColor = System.Drawing.Color.Black
        Me.btnProcess.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnProcess.GradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Location = New System.Drawing.Point(649, 13)
        Me.btnProcess.Name = "btnProcess"
        Me.btnProcess.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnProcess.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnProcess.Size = New System.Drawing.Size(97, 30)
        Me.btnProcess.TabIndex = 0
        Me.btnProcess.Text = "&Process"
        Me.btnProcess.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(752, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbgwProcess
        '
        Me.objbgwProcess.WorkerReportsProgress = True
        Me.objbgwProcess.WorkerSupportsCancellation = True
        '
        'lnkPayrollTotalVarianceReport
        '
        Me.lnkPayrollTotalVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollTotalVarianceReport.Location = New System.Drawing.Point(236, 527)
        Me.lnkPayrollTotalVarianceReport.Name = "lnkPayrollTotalVarianceReport"
        Me.lnkPayrollTotalVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollTotalVarianceReport.TabIndex = 319
        Me.lnkPayrollTotalVarianceReport.TabStop = True
        Me.lnkPayrollTotalVarianceReport.Text = "Show Payroll Total Variance Report"
        '
        'lnkPayrollVarianceReport
        '
        Me.lnkPayrollVarianceReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollVarianceReport.Location = New System.Drawing.Point(12, 527)
        Me.lnkPayrollVarianceReport.Name = "lnkPayrollVarianceReport"
        Me.lnkPayrollVarianceReport.Size = New System.Drawing.Size(200, 13)
        Me.lnkPayrollVarianceReport.TabIndex = 318
        Me.lnkPayrollVarianceReport.TabStop = True
        Me.lnkPayrollVarianceReport.Text = "Show Payroll Variance Report"
        '
        'frmPayslipGlobalPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(861, 601)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayslipGlobalPayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payslip Global Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbAdvanceAmountInfo.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.pnlAdvanceTaken.PerformLayout()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.ResumeLayout(False)
        Me.pnlEmployeeList.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPaymentInfo.ResumeLayout(False)
        Me.gbPaymentInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbPaymentInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnProcess As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlEmployeeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvEmployeeList As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPaymentDate As System.Windows.Forms.Label
    Friend WithEvents dtpPaymentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPaymentMode As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMode As System.Windows.Forms.ComboBox
    Friend WithEvents txtChequeNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbAdvanceAmountInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents txtTotalPaymentAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalPaymentAmount As System.Windows.Forms.Label
    Friend WithEvents txtVoucher As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAgainstVoucher As System.Windows.Forms.Label
    Friend WithEvents lblCashDescr As System.Windows.Forms.Label
    Friend WithEvents txtCashPerc As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCashPerc As System.Windows.Forms.Label
    Friend WithEvents colhVoucherNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTnAleaveUnkID As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblACHolder As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents objlblColor As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents colhBaseAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEmpBank As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpBank As System.Windows.Forms.Label
    Friend WithEvents cboEmpBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpBranch As System.Windows.Forms.Label
    Friend WithEvents txtCutOffAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblCutOffAmount As System.Windows.Forms.Label
    Friend WithEvents objlblEmpCount As System.Windows.Forms.Label
    Friend WithEvents cboPayPoint As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPoint As System.Windows.Forms.Label
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents cboPaymentDatePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaymentDatePeriod As System.Windows.Forms.Label
    Friend WithEvents objbgwProcess As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents lnkPayrollTotalVarianceReport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPayrollVarianceReport As System.Windows.Forms.LinkLabel
End Class

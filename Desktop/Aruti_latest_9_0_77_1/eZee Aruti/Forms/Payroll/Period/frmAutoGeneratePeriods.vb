﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAutoGeneratePeriods

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmAutoGeneratePeriods"
    Private mblnCancel As Boolean = True
    Private objPeriodMaster As clscommom_period_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintPeriodMasterUnkid As Integer = -1
    Private mintModuleRefid As Integer = -1
    Private mblnisAssessment As Boolean = False

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intModuleRefid As Integer, Optional ByVal isAssessment As Boolean = False) As Boolean
        Try
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If isAssessment = True Then
                Me.Name = "frmAssessment_AutoGeneratePeriods"
            Else
                Me.Name = "frmPayroll_AutoGeneratePeriods"
            End If
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            mintPeriodMasterUnkid = intUnkId
            mintModuleRefid = intModuleRefid
            mblnisAssessment = isAssessment
            'If isAssessment Then
            '    Me.Size = CType(New Point(446, 392), Drawing.Size)
            '    gbPeriod.Size = CType(New Point(417, 232), Drawing.Size)
            '    lblStatus.Visible = True
            '    cboStatus.Visible = True
            'End If
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintPeriodMasterUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCodeFormat.BackColor = GUI.ColorComp
            cboNameFormat.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As DataSet
        Try
            dsCombo = objPeriodMaster.getComboListForCodeNameFormat("Format", True)
            With cboCodeFormat
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("Format")
                .SelectedValue = 0
            End With

            dsCombo = objPeriodMaster.getComboListForCodeNameFormat("Format", True)
            With cboNameFormat
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("Format")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboCodeFormat.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Code Format."), enMsgBoxStyle.Information)
                cboCodeFormat.Focus()
                Return False
            ElseIf CInt(cboNameFormat.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Name Format."), enMsgBoxStyle.Information)
                cboNameFormat.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Event "

    Private Sub frmAutoGeneratePeriods_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPeriodMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoGeneratePeriods_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAutoGeneratePeriods_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPeriodMaster = New clscommom_period_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()


            Call FillCombo()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dtpStartdate.MinDate = objPeriodMaster.getNextStartDate(mintModuleRefid)
            dtpStartdate.MinDate = objPeriodMaster.getNextStartDate(mintModuleRefid, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date)
            'Sohail (21 Aug 2015) -- End
            dtpStartdate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpStartdate.Value = dtpStartdate.MinDate

            dtpEnddate.MinDate = dtpStartdate.MinDate
            dtpEnddate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpEnddate.Value = dtpEnddate.MaxDate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoGeneratePeriods_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clscommom_period_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clscommom_period_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End
#End Region

#Region " Combobox's Events "

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Dim intNameColWidth As Integer = 0 'Sohail (07 Jan 2014)
        Try
            lvPeriodList.Items.Clear()
            If IsValid() = False Then Exit Sub

            Dim dtStart As DateTime = dtpStartdate.Value
            Dim strCode As String
            Dim strName As String
            Dim arr As New ArrayList
            Dim lvItem As ListViewItem
            While dtStart < dtpEnddate.Value
                If CDate(dtStart.AddMonths(1)).AddDays(-1) <= dtpEnddate.Value Then
                    lvItem = New ListViewItem

                    Select Case CInt(cboCodeFormat.SelectedValue)
                        Case enPeriodCodeNameFormat.One_Two_Three
                            strCode = Month(dtStart).ToString
                        Case enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree
                            If Month(dtStart).ToString.Length > 1 Then
                                strCode = Month(dtStart).ToString
                            Else
                                strCode = "0" & Month(dtStart).ToString
                            End If
                        Case enPeriodCodeNameFormat.MMM_YY
                            strCode = MonthName(Month(dtStart), True) & " - " & Mid(Year(dtStart).ToString, 3)
                        Case enPeriodCodeNameFormat.MMMM_YYYY
                            strCode = MonthName(Month(dtStart), False) & " - " & Year(dtStart).ToString
                        Case Else
                            strCode = ""
                    End Select

                    Select Case CInt(cboNameFormat.SelectedValue)
                        Case enPeriodCodeNameFormat.One_Two_Three
                            strName = Month(dtStart).ToString
                        Case enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree
                            If Month(dtStart).ToString.Length > 1 Then
                                strName = Month(dtStart).ToString
                            Else
                                strName = "0" & Month(dtStart).ToString
                            End If
                        Case enPeriodCodeNameFormat.MMM_YY
                            strName = MonthName(Month(dtStart), True) & " - " & Mid(Year(dtStart).ToString, 3)
                        Case enPeriodCodeNameFormat.MMMM_YYYY
                            strName = MonthName(Month(dtStart), False) & " - " & Year(dtStart).ToString
                        Case Else
                            strName = ""
                    End Select

                    If strCode <> "" AndAlso strName <> "" Then
                        Dim i As Integer
                        Dim s As String
                        objPeriodMaster._Modulerefid = mintModuleRefid
                        If objPeriodMaster.isExist(strCode) = True OrElse arr.Contains(strCode) = True Then
                            Select Case CInt(cboCodeFormat.SelectedValue)
                                Case enPeriodCodeNameFormat.One_Two_Three, enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree
                                    Integer.TryParse(strCode, i)

                                    While True
                                        strCode = (i + 1).ToString
                                        If CInt(cboCodeFormat.SelectedValue) = enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree Then
                                            If strCode.Length = 1 Then
                                                strCode = "0" & strCode
                                            End If
                                        End If
                                        If objPeriodMaster.isExist(strCode) = False Then
                                            If arr.Contains(strCode) = False Then
                                                arr.Add(strCode)
                                                Exit While
                                            End If
                                        End If
                                        Integer.TryParse(strCode, i)
                                    End While
                                Case enPeriodCodeNameFormat.MMM_YY, enPeriodCodeNameFormat.MMMM_YYYY
                                    i = 1
                                    While True
                                        s = strCode & "(" & i & ")"
                                        If objPeriodMaster.isExist(s) = False Then
                                            If arr.Contains(s) = False Then
                                                strCode = s
                                                arr.Add(strCode)
                                                Exit While
                                            End If
                                        End If
                                        i += 1
                                    End While
                                Case Else
                                    strCode = ""
                            End Select
                        End If
                        If objPeriodMaster.isExist("", strName) = True OrElse arr.Contains(strName) = True Then
                            Select Case CInt(cboNameFormat.SelectedValue)
                                Case enPeriodCodeNameFormat.One_Two_Three, enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree
                                    Integer.TryParse(strCode, i)

                                    While True
                                        strName = (i + 1).ToString
                                        If CInt(cboNameFormat.SelectedValue) = enPeriodCodeNameFormat.ZeroOne_ZeroTwo_ZeroThree Then
                                            If strName.Length = 1 Then
                                                strName = "0" & strName
                                            End If
                                        End If
                                        If objPeriodMaster.isExist("", strName) = False Then
                                            If arr.Contains(strName) = False Then
                                                arr.Add(strName)
                                                Exit While
                                            End If
                                        End If
                                        Integer.TryParse(strName, i)
                                    End While
                                Case enPeriodCodeNameFormat.MMM_YY, enPeriodCodeNameFormat.MMMM_YYYY
                                    i = 1
                                    While True
                                        s = strName & "(" & i & ")"
                                        If objPeriodMaster.isExist("", s) = False Then
                                            If arr.Contains(s) = False Then
                                                strName = s
                                                arr.Add(strName)
                                                Exit While
                                            End If
                                        End If
                                        i += 1
                                    End While
                                Case Else
                                    strCode = ""
                            End Select
                        End If
                        lvItem.Text = strCode
                        lvItem.SubItems.Add(strName)

                        lvItem.SubItems.Add(dtStart.ToShortDateString)
                        lvItem.SubItems(colhStartperiod.Index).Tag = eZeeDate.convertDate(dtStart)

                        lvItem.SubItems.Add(CDate(dtStart.AddMonths(1)).AddDays(-1).ToShortDateString)
                        lvItem.SubItems(colhEndperiod.Index).Tag = eZeeDate.convertDate(CDate(dtStart.AddMonths(1)).AddDays(-1))
                        'Sohail (07 Jan 2014) -- Start
                        'Enhancement - Separate TnA Periods from Payroll Periods
                        lvItem.SubItems.Add(CDate(dtStart.AddMonths(1)).AddDays(-1).ToShortDateString)
                        lvItem.SubItems(colhTnAEndDate.Index).Tag = eZeeDate.convertDate(CDate(dtStart.AddMonths(1)).AddDays(-1))
                        'Sohail (07 Jan 2014) -- End

                        'Nilay (16-Apr-2016) -- Start
                        'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
                        lvItem.SubItems.Add(strCode)
                        lvItem.SubItems(colhSunjvPeriodCode.Index).Tag = strCode
                        'Nilay (16-Apr-2016) -- End

                        lvPeriodList.Items.Add(lvItem)
                    End If
                End If

                dtStart = CDate(dtStart.AddMonths(1))
            End While

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If ConfigParameter._Object._IsSeparateTnAPeriod = False Then
                colhTnAEndDate.Width = 0
                colhPeriodname.Width = 210
            Else
                colhTnAEndDate.Width = 90
                colhPeriodname.Width = 120
            End If
            intNameColWidth = colhPeriodname.Width

            If lvPeriodList.Items.Count > 10 Then
                colhPeriodname.Width = intNameColWidth - 18
            Else
                colhPeriodname.Width = intNameColWidth
            End If
            'Sohail (07 Jan 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnGenerate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lvPeriodList.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please first click on Generate Button to generate period list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure to Save generated period list?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
            Dim intYearId = FinancialYear._Object._YearUnkid
            Dim intUserId As Integer = User._Object._Userunkid
            For Each lvItem As ListViewItem In lvPeriodList.Items
                objPeriodMaster = New clscommom_period_Tran

                With objPeriodMaster
                    ._Yearunkid = intYearId
                    ._Period_Code = lvItem.Text
                    ._Period_Name = lvItem.SubItems(colhPeriodname.Index).Text
                    ._Descrription = ""
                    ._Start_Date = eZeeDate.convertDate(lvItem.SubItems(colhStartperiod.Index).Tag.ToString)
                    ._End_Date = eZeeDate.convertDate(lvItem.SubItems(colhEndperiod.Index).Tag.ToString)
                    ._Userunkid = intUserId
                    ._Statusid = CInt(enStatusType.Open)
                    ._Total_Days = CInt(DateDiff(DateInterval.Day, ._Start_Date, ._End_Date.AddDays(1)))
                    ._Modulerefid = mintModuleRefid
                    ._Constant_Days = CInt(DateDiff(DateInterval.Day, ._Start_Date, ._End_Date.AddDays(1))) 'Sohail (10 Jul 2013)
                    ._TnA_EndDate = eZeeDate.convertDate(lvItem.SubItems(colhEndperiod.Index).Tag.ToString) 'Sohail (07 Jan 2014)
                    ._Prescribed_interest_rate = 0 'Sohail (30 Oct 2015)
                    'Nilay (16-Apr-2016) -- Start
                    'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
                    ._Sunjv_PeriodCode = lvItem.SubItems(colhSunjvPeriodCode.Index).Tag.ToString
                    'Nilay (16-Apr-2016) -- End

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END
                    If .Insert() = False Then
                        If ._Message <> "" Then
                            eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                End With
            Next
            mblnCancel = False

            'Sohail (07 Jan 2014) -- Start
            'Enhancement - Separate TnA Periods from Payroll Periods
            If ConfigParameter._Object._IsSeparateTnAPeriod = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "TnA/Leave End Date has been set as Period End Date. If you want to consider TnA/Leave period to be calculated other than Period End Date then please Edit TnA/Leave End Date."), enMsgBoxStyle.Information)
            End If
            'Sohail (07 Jan 2014) -- End

            If menAction = enAction.ADD_ONE Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Messages "
    '1, "Please select Code Format."
    '2, "Please select Name Format."
    '3, "Please first click on Generate Button to generate period list."
    '4, "Are you sure to Save generated period list?"
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbPeriod.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPeriod.ForeColor = GUI._eZeeContainerHeaderForeColor 


		
			Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnGenerate.GradientBackColor = GUI._ButttonBackColor 
			Me.btnGenerate.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbPeriod.Text = Language._Object.getCaption(Me.gbPeriod.Name, Me.gbPeriod.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblStartdate.Text = Language._Object.getCaption(Me.lblStartdate.Name, Me.lblStartdate.Text)
			Me.lblEnddate.Text = Language._Object.getCaption(Me.lblEnddate.Name, Me.lblEnddate.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.btnGenerate.Text = Language._Object.getCaption(Me.btnGenerate.Name, Me.btnGenerate.Text)
			Me.colhPeriodcode.Text = Language._Object.getCaption(CStr(Me.colhPeriodcode.Tag), Me.colhPeriodcode.Text)
			Me.colhPeriodname.Text = Language._Object.getCaption(CStr(Me.colhPeriodname.Tag), Me.colhPeriodname.Text)
			Me.colhStartperiod.Text = Language._Object.getCaption(CStr(Me.colhStartperiod.Tag), Me.colhStartperiod.Text)
			Me.colhEndperiod.Text = Language._Object.getCaption(CStr(Me.colhEndperiod.Tag), Me.colhEndperiod.Text)
			Me.colhTnAEndDate.Text = Language._Object.getCaption(CStr(Me.colhTnAEndDate.Tag), Me.colhTnAEndDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Code Format.")
			Language.setMessage(mstrModuleName, 2, "Please select Name Format.")
			Language.setMessage(mstrModuleName, 3, "Please first click on Generate Button to generate period list.")
			Language.setMessage(mstrModuleName, 4, "Are you sure to Save generated period list?")
			Language.setMessage(mstrModuleName, 5, "TnA/Leave End Date has been set as Period End Date. If you want to consider TnA/Leave period to be calculated other than Period End Date then please Edit TnA/Leave End Date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO

Public Class frmProjectHoursDetails

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmProjectHoursDetails"
    Private mblnCancel As Boolean = True
    Private mintEmployeeID As Integer = 0
    Private mstrEmployee As String = ""
    Private mstrPeriod As String = ""
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private mstrProject As String = ""
    Private mstrDonor As String = ""
    Private mintActivityId As Integer = 0
    Private mstrActivity As String = ""
    Private mdblAllocatePencentage As Double = 0
    Private mblnTimesheetApproval As Boolean = False
    Private mdtEmpTimesheet As DataTable = Nothing
    'Pinkal (16-May-2018) -- Start
    'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    'Private mdtPeriodStartDate As Date = Nothing
    'Private mdtPeriodEndDate As Date = Nothing
    Public mdtPeriodStartDate As Date = Nothing
    Public mdtPeriodEndDate As Date = Nothing
    'Pinkal (28-Jul-2018) -- End

    'Pinkal (16-May-2018) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef xEmployeeId As Integer, ByVal xEmployee As String, ByVal xPeriod As String _
                                              , ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xProjectName As String _
                                              , ByVal xDonor As String, ByVal xActivityId As Integer, ByVal xActivity As String _
                                              , ByVal xAllocatePencentage As Double, ByVal dtEmpTimesheet As DataTable _
                                              , ByVal blnTimesheetApproval As Boolean, ByVal xPeriodStartDate As Date _
                                              , ByVal xPeriodEndDate As Date) As Boolean

        'Pinkal (16-May-2018) -- 'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.[ByVal xPeriodStartDate As Date , ByVal xPeriodEndDate As Date]

        Try
            mintEmployeeID = xEmployeeId
            mstrEmployee = xEmployee
            mstrPeriod = xPeriod
            mdtFromDate = xFromDate
            mdtToDate = xToDate
            mstrProject = xProjectName
            mstrDonor = xDonor
            mintActivityId = xActivityId
            mstrActivity = xActivity
            mdblAllocatePencentage = xAllocatePencentage
            mdtEmpTimesheet = dtEmpTimesheet
            mblnTimesheetApproval = blnTimesheetApproval
            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            mdtPeriodStartDate = xPeriodStartDate
            mdtPeriodEndDate = xPeriodEndDate
            'Pinkal (16-May-2018) -- End
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region


#Region " Private Methods "

    Public Function GetProjectDetails(ByVal xEmployeeId As Integer, ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xActivityId As Integer, ByVal xAllocatePencentage As Double _
                                                    , ByVal xblnTimesheetApproval As Boolean, ByVal xdtEmpTimesheet As DataTable) As DataTable
        Dim dtProjectDetail As DataTable = Nothing
        Try
            dtProjectDetail = New DataTable()
            dtProjectDetail.Columns.Add("Id", Type.GetType("System.Int32"))
            dtProjectDetail.Columns.Add("Particulars", Type.GetType("System.String"))
            dtProjectDetail.Columns.Add("Hours", Type.GetType("System.String"))

            Dim drRow As DataRow = dtProjectDetail.NewRow()
            drRow("Id") = 1
            drRow("Particulars") = Language.getMessage(mstrModuleName, 1, "Projected(targeted) total number of hours for the month")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 2
            drRow("Particulars") = Language.getMessage(mstrModuleName, 2, "Projected running total number of hours as per entries")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 3
            drRow("Particulars") = Language.getMessage(mstrModuleName, 3, "Projected running total number of hours as of date")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 4
            drRow("Particulars") = Language.getMessage(mstrModuleName, 4, "Total Actual(running) number of hours")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 5
            drRow("Particulars") = Language.getMessage(mstrModuleName, 5, "Balance of the remained hrs")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 6
            drRow("Particulars") = Language.getMessage(mstrModuleName, 6, "Employee % allocation of the projects")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            drRow = dtProjectDetail.NewRow()
            drRow("Id") = 7
            drRow("Particulars") = Language.getMessage(mstrModuleName, 7, "OverTime")
            drRow("Hours") = ""
            dtProjectDetail.Rows.Add(drRow)

            Dim RowIndex As Integer = 1

            ' START - (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)
            Dim mdblProejectedMonthHrs As Double = 0

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(xFromDate, xToDate, xEmployeeId)
            Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(mdtPeriodStartDate, mdtPeriodEndDate, xEmployeeId)
            'Pinkal (16-May-2018) -- End
            
            dtProjectDetail.Rows(RowIndex - 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * xAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            mdblProejectedMonthHrs = CDbl((mdblTotalWorkingHrs * xAllocatePencentage) / 100)
            ' END -  (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)



            'START - (II) Projected running total number of hours as per entries.
            mdblTotalWorkingHrs = 0
            If xdtEmpTimesheet IsNot Nothing AndAlso xdtEmpTimesheet.Rows.Count > 0 Then
                Dim dtDate As Date = Nothing
                If xblnTimesheetApproval Then
                    'Pinkal (04-May-2018) -- Start
                    'Enhancement - Budget timesheet Enhancement for MST/THPS.
                    If IsDBNull(xdtEmpTimesheet.Compute("Max(activitydate)", "IsGrp = 0 AND employeeunkid = " & xEmployeeId & " AND  activityunkid = " & xActivityId)) = False Then
                    dtDate = CDate(xdtEmpTimesheet.Compute("Max(activitydate)", "IsGrp = 0 AND employeeunkid = " & xEmployeeId & " AND  activityunkid = " & xActivityId))
                Else
                        dtProjectDetail.Rows(RowIndex)("Hours") = "00:00"
                    End If
                    'Pinkal (04-May-2018) -- End
                Else
                    'Pinkal (04-May-2018) -- Start
                    'Enhancement - Budget timesheet Enhancement for MST/THPS.
                    If IsDBNull(xdtEmpTimesheet.Compute("Max(activitydate)", "fundactivityunkid = " & xActivityId)) = False Then
                    dtDate = CDate(xdtEmpTimesheet.Compute("Max(activitydate)", "fundactivityunkid = " & xActivityId))
                    Else
                        dtProjectDetail.Rows(RowIndex)("Hours") = "00:00"
                    End If
                    'Pinkal (04-May-2018) -- End
                End If

                'Pinkal (04-May-2018) -- Start
                'Enhancement - Budget timesheet Enhancement for MST/THPS.
                If dtDate <> Nothing Then
                mdblTotalWorkingHrs = GetProjectedTotalHRs(xFromDate, dtDate, xEmployeeId)
                dtProjectDetail.Rows(RowIndex)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * xAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
                End If
                'Pinkal (04-May-2018) -- End

            Else
                dtProjectDetail.Rows(RowIndex)("Hours") = "00:00"
            End If
            'END  - (II)  Projected running total number of hours as per entries.


            ' START - (III) Projected running total number of hours as of date.
            mdblTotalWorkingHrs = 0
            If eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date < xToDate.Date Then
                xToDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
            End If
            mdblTotalWorkingHrs = GetProjectedTotalHRs(xFromDate, xToDate, xEmployeeId)
            dtProjectDetail.Rows(RowIndex + 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * xAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            ' END - (III) Projected running total number of hours as of date. 


            'START - (IV) Total Actual(running) number of hours (only Entries)
            mdblTotalWorkingHrs = 0
            Dim mdblActualHrs As Double = 0
            If xdtEmpTimesheet IsNot Nothing AndAlso xdtEmpTimesheet.Rows.Count > 0 Then
                If xblnTimesheetApproval Then
                    'Pinkal (04-May-2018) -- Start
                    'Enhancement - Budget timesheet Enhancement for MST/THPS.
                    If IsDBNull(xdtEmpTimesheet.Compute("SUM(activity_hrsinMins)", "IsGrp = 0 AND employeeunkid = " & xEmployeeId & " AND activityunkid = " & xActivityId)) = False Then
                    mdblTotalWorkingHrs = CInt(xdtEmpTimesheet.Compute("SUM(activity_hrsinMins)", "IsGrp = 0 AND employeeunkid = " & xEmployeeId & " AND activityunkid = " & xActivityId)) * 60
                    End If
                    'Pinkal (04-May-2018) -- End
                Else
                    'Pinkal (04-May-2018) -- Start
                    'Enhancement - Budget timesheet Enhancement for MST/THPS.
                    If IsDBNull(xdtEmpTimesheet.Compute("SUM(ActivityHoursInMin)", "fundactivityunkid = " & xActivityId)) = False Then
                    mdblTotalWorkingHrs = CInt(xdtEmpTimesheet.Compute("SUM(ActivityHoursInMin)", "fundactivityunkid = " & xActivityId)) * 60
                End If
                    'Pinkal (04-May-2018) -- End
                End If

                mdblActualHrs = mdblTotalWorkingHrs
                dtProjectDetail.Rows(RowIndex + 2)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#00.00").Replace(".", ":")
            Else
                dtProjectDetail.Rows(RowIndex + 2)("Hours") = "00:00"
            End If
            'END - (IV) Total Actual(running) number of hours (only Entries)

            'START - (V) Balance of the remained hrs.
            Dim mblnIsOvertime As Boolean = False
            mdblTotalWorkingHrs = 0
            If mdblActualHrs > mdblProejectedMonthHrs Then
                mdblTotalWorkingHrs = mdblActualHrs - mdblProejectedMonthHrs
                mblnIsOvertime = True
            Else
                mdblTotalWorkingHrs = mdblProejectedMonthHrs - mdblActualHrs
                mblnIsOvertime = False
            End If
            dtProjectDetail.Rows(RowIndex + 3)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#00.00").Replace(".", ":")
            'End - (V) Balance of the remained hrs.

            'START - (VI)  Show employee % allocation of the projects 
            dtProjectDetail.Rows(RowIndex + 4)("Hours") = xAllocatePencentage.ToString("#00.00") & " % "
            'END - (VI) Show employee % allocation of the projects 

            'START (VII) Show Overtime 
            dtProjectDetail.Rows(dtProjectDetail.Rows.Count - 1)("Hours") = mblnIsOvertime
            'End (VII) Show Overtime 

            mdblActualHrs = 0
            mdblProejectedMonthHrs = 0
            mdblTotalWorkingHrs = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetProjectDetails", mstrModuleName)
        End Try
        Return dtProjectDetail
    End Function

    Private Sub SetValue()
        Try

            objLblEmpVal.Text = mstrEmployee
            objLblPeriodVal.Text = mstrPeriod
            objLblProjectVal.Text = mstrProject
            objLblDonorGrantVal.Text = mstrDonor
            objLblActivityVal.Text = mstrActivity

            Dim dtTable As DataTable = GetProjectDetails(mintEmployeeID, mdtFromDate, mdtToDate, mintActivityId, mdblAllocatePencentage, mblnTimesheetApproval, mdtEmpTimesheet)


            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                dtTable.Rows.RemoveAt(dtTable.Rows.Count - 1)
                dtTable.AcceptChanges()
            End If

            dgvProjectHrDetails.AutoGenerateColumns = False
            dgcolhParticulars.DataPropertyName = "Particulars"
            dgcolhHours.DataPropertyName = "Hours"
            dgvProjectHrDetails.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetData", mstrModuleName)
        End Try
    End Sub

    Private Function GetProjectedTotalHRs(ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xEmployeeId As Integer) As Double
        Dim mintTotalWorkingDays As Integer = 0
        Dim mdblTotalWorkingHrs As Double = 0
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objEmpHoliday As New clsemployee_holiday
        Try

            objEmpShift.GetTotalWorkingHours(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                     , ConfigParameter._Object._UserAccessModeSetting, xEmployeeId, xFromDate.Date, xToDate.Date _
                                                                                                     , mintTotalWorkingDays, mdblTotalWorkingHrs, False)


            Dim dsHoliday As DataSet = objEmpHoliday.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                         , xFromDate.Date, xToDate.Date, ConfigParameter._Object._UserAccessModeSetting, True, False, True, xEmployeeId, , _
                                                                                         , " AND convert(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(xFromDate) & "' AND convert(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(xToDate) & "'")

            If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                Dim objShiftTran As New clsshift_tran
                For Each dr As DataRow In dsHoliday.Tables(0).Rows
                    Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, xEmployeeId)
                    objShiftTran.GetShiftTran(mintShiftId)
                    Dim dHLRow() As DataRow = objShiftTran._dtShiftday.Select("isweekend = 0 AND  dayid = " & Weekday(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, FirstDayOfWeek.Sunday) - 1)
                    If dHLRow.Length > 0 Then
                        mintTotalWorkingDays -= 1
                        mdblTotalWorkingHrs -= CDbl(dHLRow(0)("workinghrsinsec"))
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetProjectedTotalHRs", mstrModuleName)
        Finally
            objEmpShift = Nothing
            objEmpHoliday = Nothing
        End Try
        Return mdblTotalWorkingHrs
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmProjectHoursDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            SetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmProjectHoursDetails_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProjectHoursDetails_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsBudgetEmp_timesheet.SetMessages()
            'objfrm._Other_ModuleNames = "clsBudgetEmp_timesheet"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmProjectHoursDetails_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
			Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
			Me.LblProject.Text = Language._Object.getCaption(Me.LblProject.Name, Me.LblProject.Text)
			Me.LblDonorGrant.Text = Language._Object.getCaption(Me.LblDonorGrant.Name, Me.LblDonorGrant.Text)
			Me.LblActivity.Text = Language._Object.getCaption(Me.LblActivity.Name, Me.LblActivity.Text)
			Me.dgcolhParticulars.HeaderText = Language._Object.getCaption(Me.dgcolhParticulars.Name, Me.dgcolhParticulars.HeaderText)
			Me.dgcolhHours.HeaderText = Language._Object.getCaption(Me.dgcolhHours.Name, Me.dgcolhHours.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Projected(targeted) total number of hours for the month")
			Language.setMessage(mstrModuleName, 2, "Projected running total number of hours as per entries")
			Language.setMessage(mstrModuleName, 3, "Projected running total number of hours as of date")
			Language.setMessage(mstrModuleName, 4, "Total Actual(running) number of hours")
			Language.setMessage(mstrModuleName, 5, "Balance of the remained hrs")
			Language.setMessage(mstrModuleName, 6, "Employee % allocation of the projects")
			Language.setMessage(mstrModuleName, 7, "OverTime")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewYear_Wizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.wz_NewYearWizard = New eZee.Common.eZeeWizard
        Me.wpWelcome = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblStep1Message = New System.Windows.Forms.Label
        Me.wpFinish = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblStep7_Finish = New System.Windows.Forms.Label
        Me.wpProgress = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbTransferProgress = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rtbCloseYearLog = New System.Windows.Forms.RichTextBox
        Me.wpYearInfo = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbYearInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.dtpPeriodEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.dtpNewEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblNewEnddate = New System.Windows.Forms.Label
        Me.dtpNewStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblNewStartdate = New System.Windows.Forms.Label
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.dtpCurrEnddate = New System.Windows.Forms.DateTimePicker
        Me.lblCurrEnddate = New System.Windows.Forms.Label
        Me.dtpCurrStartdate = New System.Windows.Forms.DateTimePicker
        Me.lblCurrStartdate = New System.Windows.Forms.Label
        Me.eZeeLine1 = New eZee.Common.eZeeLine
        Me.objbgwCloseYear = New System.ComponentModel.BackgroundWorker
        Me.wz_NewYearWizard.SuspendLayout()
        Me.wpWelcome.SuspendLayout()
        Me.wpFinish.SuspendLayout()
        Me.wpProgress.SuspendLayout()
        Me.gbTransferProgress.SuspendLayout()
        Me.wpYearInfo.SuspendLayout()
        Me.gbYearInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'wz_NewYearWizard
        '
        Me.wz_NewYearWizard.Controls.Add(Me.wpYearInfo)
        Me.wz_NewYearWizard.Controls.Add(Me.wpWelcome)
        Me.wz_NewYearWizard.Controls.Add(Me.wpFinish)
        Me.wz_NewYearWizard.Controls.Add(Me.wpProgress)
        Me.wz_NewYearWizard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wz_NewYearWizard.HeaderImage = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        Me.wz_NewYearWizard.Location = New System.Drawing.Point(0, 0)
        Me.wz_NewYearWizard.Name = "wz_NewYearWizard"
        Me.wz_NewYearWizard.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wpWelcome, Me.wpYearInfo, Me.wpProgress, Me.wpFinish})
        Me.wz_NewYearWizard.SaveEnabled = True
        Me.wz_NewYearWizard.SaveText = "Save && Finish"
        Me.wz_NewYearWizard.SaveVisible = False
        Me.wz_NewYearWizard.SetSaveIndexBeforeFinishIndex = False
        Me.wz_NewYearWizard.Size = New System.Drawing.Size(711, 427)
        Me.wz_NewYearWizard.TabIndex = 0
        Me.wz_NewYearWizard.WelcomeImage = Global.Aruti.Main.My.Resources.Resources.Aruti_Wiz
        '
        'wpWelcome
        '
        Me.wpWelcome.Controls.Add(Me.lblStep1Message)
        Me.wpWelcome.Location = New System.Drawing.Point(0, 0)
        Me.wpWelcome.Name = "wpWelcome"
        Me.wpWelcome.Size = New System.Drawing.Size(711, 379)
        Me.wpWelcome.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpWelcome.TabIndex = 8
        '
        'lblStep1Message
        '
        Me.lblStep1Message.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep1Message.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep1Message.Location = New System.Drawing.Point(165, 2)
        Me.lblStep1Message.Name = "lblStep1Message"
        Me.lblStep1Message.Size = New System.Drawing.Size(544, 375)
        Me.lblStep1Message.TabIndex = 155
        Me.lblStep1Message.Text = "Welcome to Generate New Year Wizard....."
        Me.lblStep1Message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'wpFinish
        '
        Me.wpFinish.Controls.Add(Me.lblStep7_Finish)
        Me.wpFinish.Location = New System.Drawing.Point(0, 0)
        Me.wpFinish.Name = "wpFinish"
        Me.wpFinish.Size = New System.Drawing.Size(711, 379)
        Me.wpFinish.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpFinish.TabIndex = 11
        '
        'lblStep7_Finish
        '
        Me.lblStep7_Finish.Font = New System.Drawing.Font("Tahoma", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStep7_Finish.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.lblStep7_Finish.Location = New System.Drawing.Point(166, 2)
        Me.lblStep7_Finish.Name = "lblStep7_Finish"
        Me.lblStep7_Finish.Size = New System.Drawing.Size(543, 377)
        Me.lblStep7_Finish.TabIndex = 156
        Me.lblStep7_Finish.Text = "The Generate New Year Wizard Completed Successfully."
        Me.lblStep7_Finish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'wpProgress
        '
        Me.wpProgress.Controls.Add(Me.gbTransferProgress)
        Me.wpProgress.Location = New System.Drawing.Point(0, 0)
        Me.wpProgress.Name = "wpProgress"
        Me.wpProgress.Size = New System.Drawing.Size(428, 208)
        Me.wpProgress.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpProgress.TabIndex = 10
        '
        'gbTransferProgress
        '
        Me.gbTransferProgress.BorderColor = System.Drawing.Color.Black
        Me.gbTransferProgress.Checked = False
        Me.gbTransferProgress.CollapseAllExceptThis = False
        Me.gbTransferProgress.CollapsedHoverImage = Nothing
        Me.gbTransferProgress.CollapsedNormalImage = Nothing
        Me.gbTransferProgress.CollapsedPressedImage = Nothing
        Me.gbTransferProgress.CollapseOnLoad = False
        Me.gbTransferProgress.Controls.Add(Me.rtbCloseYearLog)
        Me.gbTransferProgress.ExpandedHoverImage = Nothing
        Me.gbTransferProgress.ExpandedNormalImage = Nothing
        Me.gbTransferProgress.ExpandedPressedImage = Nothing
        Me.gbTransferProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTransferProgress.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbTransferProgress.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTransferProgress.HeaderHeight = 25
        Me.gbTransferProgress.HeaderMessage = ""
        Me.gbTransferProgress.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTransferProgress.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTransferProgress.HeightOnCollapse = 0
        Me.gbTransferProgress.LeftTextSpace = 0
        Me.gbTransferProgress.Location = New System.Drawing.Point(164, 0)
        Me.gbTransferProgress.Name = "gbTransferProgress"
        Me.gbTransferProgress.OpenHeight = 300
        Me.gbTransferProgress.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTransferProgress.ShowBorder = True
        Me.gbTransferProgress.ShowCheckBox = False
        Me.gbTransferProgress.ShowCollapseButton = False
        Me.gbTransferProgress.ShowDefaultBorderColor = True
        Me.gbTransferProgress.ShowDownButton = False
        Me.gbTransferProgress.ShowHeader = True
        Me.gbTransferProgress.Size = New System.Drawing.Size(547, 379)
        Me.gbTransferProgress.TabIndex = 4
        Me.gbTransferProgress.Temp = 0
        Me.gbTransferProgress.Text = "Transfer Progress"
        Me.gbTransferProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rtbCloseYearLog
        '
        Me.rtbCloseYearLog.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbCloseYearLog.Location = New System.Drawing.Point(4, 26)
        Me.rtbCloseYearLog.Name = "rtbCloseYearLog"
        Me.rtbCloseYearLog.ReadOnly = True
        Me.rtbCloseYearLog.Size = New System.Drawing.Size(539, 350)
        Me.rtbCloseYearLog.TabIndex = 181
        Me.rtbCloseYearLog.Text = ""
        Me.rtbCloseYearLog.Visible = False
        '
        'wpYearInfo
        '
        Me.wpYearInfo.Controls.Add(Me.gbYearInformation)
        Me.wpYearInfo.Location = New System.Drawing.Point(0, 0)
        Me.wpYearInfo.Name = "wpYearInfo"
        Me.wpYearInfo.Size = New System.Drawing.Size(711, 379)
        Me.wpYearInfo.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wpYearInfo.TabIndex = 9
        '
        'gbYearInformation
        '
        Me.gbYearInformation.BorderColor = System.Drawing.Color.Black
        Me.gbYearInformation.Checked = False
        Me.gbYearInformation.CollapseAllExceptThis = False
        Me.gbYearInformation.CollapsedHoverImage = Nothing
        Me.gbYearInformation.CollapsedNormalImage = Nothing
        Me.gbYearInformation.CollapsedPressedImage = Nothing
        Me.gbYearInformation.CollapseOnLoad = False
        Me.gbYearInformation.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbYearInformation.Controls.Add(Me.dtpPeriodEnddate)
        Me.gbYearInformation.Controls.Add(Me.lblEnddate)
        Me.gbYearInformation.Controls.Add(Me.lblName)
        Me.gbYearInformation.Controls.Add(Me.txtName)
        Me.gbYearInformation.Controls.Add(Me.lblCode)
        Me.gbYearInformation.Controls.Add(Me.txtCode)
        Me.gbYearInformation.Controls.Add(Me.EZeeLine3)
        Me.gbYearInformation.Controls.Add(Me.dtpNewEnddate)
        Me.gbYearInformation.Controls.Add(Me.lblNewEnddate)
        Me.gbYearInformation.Controls.Add(Me.dtpNewStartdate)
        Me.gbYearInformation.Controls.Add(Me.lblNewStartdate)
        Me.gbYearInformation.Controls.Add(Me.EZeeLine2)
        Me.gbYearInformation.Controls.Add(Me.dtpCurrEnddate)
        Me.gbYearInformation.Controls.Add(Me.lblCurrEnddate)
        Me.gbYearInformation.Controls.Add(Me.dtpCurrStartdate)
        Me.gbYearInformation.Controls.Add(Me.lblCurrStartdate)
        Me.gbYearInformation.Controls.Add(Me.eZeeLine1)
        Me.gbYearInformation.ExpandedHoverImage = Nothing
        Me.gbYearInformation.ExpandedNormalImage = Nothing
        Me.gbYearInformation.ExpandedPressedImage = Nothing
        Me.gbYearInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbYearInformation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.gbYearInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbYearInformation.HeaderHeight = 25
        Me.gbYearInformation.HeaderMessage = ""
        Me.gbYearInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbYearInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbYearInformation.HeightOnCollapse = 0
        Me.gbYearInformation.LeftTextSpace = 0
        Me.gbYearInformation.Location = New System.Drawing.Point(164, 0)
        Me.gbYearInformation.Name = "gbYearInformation"
        Me.gbYearInformation.OpenHeight = 300
        Me.gbYearInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbYearInformation.ShowBorder = True
        Me.gbYearInformation.ShowCheckBox = False
        Me.gbYearInformation.ShowCollapseButton = False
        Me.gbYearInformation.ShowDefaultBorderColor = True
        Me.gbYearInformation.ShowDownButton = False
        Me.gbYearInformation.ShowHeader = True
        Me.gbYearInformation.Size = New System.Drawing.Size(547, 379)
        Me.gbYearInformation.TabIndex = 3
        Me.gbYearInformation.Temp = 0
        Me.gbYearInformation.Text = "Year Information"
        Me.gbYearInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Checked = True
        Me.chkCopyPreviousEDSlab.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(147, 348)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(268, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 179
        Me.chkCopyPreviousEDSlab.Text = "Copy last period ED Slab to New year first period"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'dtpPeriodEnddate
        '
        Me.dtpPeriodEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPeriodEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPeriodEnddate.Location = New System.Drawing.Point(229, 300)
        Me.dtpPeriodEnddate.Name = "dtpPeriodEnddate"
        Me.dtpPeriodEnddate.Size = New System.Drawing.Size(110, 21)
        Me.dtpPeriodEnddate.TabIndex = 105
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(144, 302)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(69, 16)
        Me.lblEnddate.TabIndex = 104
        Me.lblEnddate.Text = "End Date"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(144, 275)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(79, 16)
        Me.lblName.TabIndex = 103
        Me.lblName.Text = "Period Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(229, 273)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(186, 21)
        Me.txtName.TabIndex = 100
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(144, 248)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(79, 16)
        Me.lblCode.TabIndex = 101
        Me.lblCode.Text = "Period Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(229, 246)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(110, 21)
        Me.txtCode.TabIndex = 99
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(137, 223)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(278, 20)
        Me.EZeeLine3.TabIndex = 23
        Me.EZeeLine3.Text = "New Year First Period Information"
        '
        'dtpNewEnddate
        '
        Me.dtpNewEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpNewEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpNewEnddate.Location = New System.Drawing.Point(229, 179)
        Me.dtpNewEnddate.Name = "dtpNewEnddate"
        Me.dtpNewEnddate.Size = New System.Drawing.Size(110, 21)
        Me.dtpNewEnddate.TabIndex = 21
        '
        'lblNewEnddate
        '
        Me.lblNewEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewEnddate.Location = New System.Drawing.Point(144, 183)
        Me.lblNewEnddate.Name = "lblNewEnddate"
        Me.lblNewEnddate.Size = New System.Drawing.Size(69, 15)
        Me.lblNewEnddate.TabIndex = 20
        Me.lblNewEnddate.Text = "End Date"
        Me.lblNewEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpNewStartdate
        '
        Me.dtpNewStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpNewStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpNewStartdate.Location = New System.Drawing.Point(229, 152)
        Me.dtpNewStartdate.Name = "dtpNewStartdate"
        Me.dtpNewStartdate.Size = New System.Drawing.Size(110, 21)
        Me.dtpNewStartdate.TabIndex = 19
        '
        'lblNewStartdate
        '
        Me.lblNewStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewStartdate.Location = New System.Drawing.Point(144, 156)
        Me.lblNewStartdate.Name = "lblNewStartdate"
        Me.lblNewStartdate.Size = New System.Drawing.Size(69, 15)
        Me.lblNewStartdate.TabIndex = 18
        Me.lblNewStartdate.Text = "Start Date"
        Me.lblNewStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(137, 129)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(278, 20)
        Me.EZeeLine2.TabIndex = 17
        Me.EZeeLine2.Text = "New Year Information"
        '
        'dtpCurrEnddate
        '
        Me.dtpCurrEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCurrEnddate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCurrEnddate.Location = New System.Drawing.Point(229, 85)
        Me.dtpCurrEnddate.Name = "dtpCurrEnddate"
        Me.dtpCurrEnddate.Size = New System.Drawing.Size(110, 21)
        Me.dtpCurrEnddate.TabIndex = 16
        '
        'lblCurrEnddate
        '
        Me.lblCurrEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrEnddate.Location = New System.Drawing.Point(144, 89)
        Me.lblCurrEnddate.Name = "lblCurrEnddate"
        Me.lblCurrEnddate.Size = New System.Drawing.Size(69, 15)
        Me.lblCurrEnddate.TabIndex = 15
        Me.lblCurrEnddate.Text = "End Date"
        Me.lblCurrEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpCurrStartdate
        '
        Me.dtpCurrStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpCurrStartdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCurrStartdate.Location = New System.Drawing.Point(229, 58)
        Me.dtpCurrStartdate.Name = "dtpCurrStartdate"
        Me.dtpCurrStartdate.Size = New System.Drawing.Size(110, 21)
        Me.dtpCurrStartdate.TabIndex = 14
        '
        'lblCurrStartdate
        '
        Me.lblCurrStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrStartdate.Location = New System.Drawing.Point(144, 62)
        Me.lblCurrStartdate.Name = "lblCurrStartdate"
        Me.lblCurrStartdate.Size = New System.Drawing.Size(69, 15)
        Me.lblCurrStartdate.TabIndex = 13
        Me.lblCurrStartdate.Text = "Start Date"
        Me.lblCurrStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeLine1
        '
        Me.eZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eZeeLine1.Location = New System.Drawing.Point(137, 35)
        Me.eZeeLine1.Name = "eZeeLine1"
        Me.eZeeLine1.Size = New System.Drawing.Size(278, 20)
        Me.eZeeLine1.TabIndex = 12
        Me.eZeeLine1.Text = "Current Year Information"
        '
        'objbgwCloseYear
        '
        Me.objbgwCloseYear.WorkerReportsProgress = True
        Me.objbgwCloseYear.WorkerSupportsCancellation = True
        '
        'frmNewYear_Wizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 427)
        Me.Controls.Add(Me.wz_NewYearWizard)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewYear_Wizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Generate New Year Wizard"
        Me.wz_NewYearWizard.ResumeLayout(False)
        Me.wpWelcome.ResumeLayout(False)
        Me.wpFinish.ResumeLayout(False)
        Me.wpProgress.ResumeLayout(False)
        Me.gbTransferProgress.ResumeLayout(False)
        Me.wpYearInfo.ResumeLayout(False)
        Me.gbYearInformation.ResumeLayout(False)
        Me.gbYearInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wz_NewYearWizard As eZee.Common.eZeeWizard
    Friend WithEvents wpYearInfo As eZee.Common.eZeeWizardPage
    Friend WithEvents wpWelcome As eZee.Common.eZeeWizardPage
    Friend WithEvents wpFinish As eZee.Common.eZeeWizardPage
    Friend WithEvents wpProgress As eZee.Common.eZeeWizardPage
    Friend WithEvents lblStep1Message As System.Windows.Forms.Label
    Friend WithEvents gbYearInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents dtpPeriodEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents dtpNewEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNewEnddate As System.Windows.Forms.Label
    Friend WithEvents dtpNewStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNewStartdate As System.Windows.Forms.Label
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents dtpCurrEnddate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrEnddate As System.Windows.Forms.Label
    Friend WithEvents dtpCurrStartdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrStartdate As System.Windows.Forms.Label
    Friend WithEvents eZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents gbTransferProgress As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents rtbCloseYearLog As System.Windows.Forms.RichTextBox
    Friend WithEvents lblStep7_Finish As System.Windows.Forms.Label
    Friend WithEvents objbgwCloseYear As System.ComponentModel.BackgroundWorker
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaymentApproverLevelList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaymentApproverLevelList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblActiveInactive = New System.Windows.Forms.Label
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.lblLevel = New System.Windows.Forms.Label
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLevel = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnInactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvApproverLevelList = New eZee.Common.eZeeListView(Me.components)
        Me.colhApproveLevelCode = New System.Windows.Forms.ColumnHeader
        Me.colhApproveLevelName = New System.Windows.Forms.ColumnHeader
        Me.colhApproveLevelPriority = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.lvApproverLevelList)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(687, 445)
        Me.pnlMainInfo.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblLevel)
        Me.gbFilterCriteria.Controls.Add(Me.cboLevel)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLevel)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(663, 67)
        Me.gbFilterCriteria.TabIndex = 11
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblActiveInactive
        '
        Me.lblActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActiveInactive.Location = New System.Drawing.Point(313, 36)
        Me.lblActiveInactive.Name = "lblActiveInactive"
        Me.lblActiveInactive.Size = New System.Drawing.Size(90, 15)
        Me.lblActiveInactive.TabIndex = 302
        Me.lblActiveInactive.Text = "Active/Inactive"
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboActiveInactive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(409, 33)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(105, 21)
        Me.cboActiveInactive.TabIndex = 1
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(8, 36)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(77, 15)
        Me.lblLevel.TabIndex = 74
        Me.lblLevel.Text = "Level"
        '
        'cboLevel
        '
        Me.cboLevel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboLevel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(91, 33)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(184, 21)
        Me.cboLevel.TabIndex = 0
        '
        'objbtnSearchLevel
        '
        Me.objbtnSearchLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLevel.BorderSelected = False
        Me.objbtnSearchLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLevel.Location = New System.Drawing.Point(281, 33)
        Me.objbtnSearchLevel.Name = "objbtnSearchLevel"
        Me.objbtnSearchLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLevel.TabIndex = 87
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(636, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(612, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnActive)
        Me.objFooter.Controls.Add(Me.btnInactive)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 390)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(687, 55)
        Me.objFooter.TabIndex = 8
        '
        'btnActive
        '
        Me.btnActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActive.BackColor = System.Drawing.Color.White
        Me.btnActive.BackgroundImage = CType(resources.GetObject("btnActive.BackgroundImage"), System.Drawing.Image)
        Me.btnActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActive.BorderColor = System.Drawing.Color.Empty
        Me.btnActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActive.FlatAppearance.BorderSize = 0
        Me.btnActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActive.ForeColor = System.Drawing.Color.Black
        Me.btnActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Location = New System.Drawing.Point(371, 13)
        Me.btnActive.Name = "btnActive"
        Me.btnActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Size = New System.Drawing.Size(98, 30)
        Me.btnActive.TabIndex = 124
        Me.btnActive.Text = "Set &Active"
        Me.btnActive.UseVisualStyleBackColor = True
        '
        'btnInactive
        '
        Me.btnInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInactive.BackColor = System.Drawing.Color.White
        Me.btnInactive.BackgroundImage = CType(resources.GetObject("btnInactive.BackgroundImage"), System.Drawing.Image)
        Me.btnInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInactive.BorderColor = System.Drawing.Color.Empty
        Me.btnInactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInactive.FlatAppearance.BorderSize = 0
        Me.btnInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInactive.ForeColor = System.Drawing.Color.Black
        Me.btnInactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Location = New System.Drawing.Point(371, 13)
        Me.btnInactive.Name = "btnInactive"
        Me.btnInactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Size = New System.Drawing.Size(98, 30)
        Me.btnInactive.TabIndex = 123
        Me.btnInactive.Text = "Set &Inactive"
        Me.btnInactive.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(475, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(268, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 121
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(165, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(578, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvApproverLevelList
        '
        Me.lvApproverLevelList.BackColorOnChecked = True
        Me.lvApproverLevelList.ColumnHeaders = Nothing
        Me.lvApproverLevelList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhApproveLevelCode, Me.colhApproveLevelName, Me.colhApproveLevelPriority})
        Me.lvApproverLevelList.CompulsoryColumns = ""
        Me.lvApproverLevelList.FullRowSelect = True
        Me.lvApproverLevelList.GridLines = True
        Me.lvApproverLevelList.GroupingColumn = Nothing
        Me.lvApproverLevelList.HideSelection = False
        Me.lvApproverLevelList.Location = New System.Drawing.Point(12, 139)
        Me.lvApproverLevelList.MinColumnWidth = 50
        Me.lvApproverLevelList.MultiSelect = False
        Me.lvApproverLevelList.Name = "lvApproverLevelList"
        Me.lvApproverLevelList.OptionalColumns = ""
        Me.lvApproverLevelList.ShowMoreItem = False
        Me.lvApproverLevelList.ShowSaveItem = False
        Me.lvApproverLevelList.ShowSelectAll = True
        Me.lvApproverLevelList.ShowSizeAllColumnsToFit = True
        Me.lvApproverLevelList.Size = New System.Drawing.Size(662, 246)
        Me.lvApproverLevelList.Sortable = True
        Me.lvApproverLevelList.TabIndex = 10
        Me.lvApproverLevelList.UseCompatibleStateImageBehavior = False
        Me.lvApproverLevelList.View = System.Windows.Forms.View.Details
        '
        'colhApproveLevelCode
        '
        Me.colhApproveLevelCode.Tag = "colhApproveLevelCode"
        Me.colhApproveLevelCode.Text = "Level Code"
        Me.colhApproveLevelCode.Width = 150
        '
        'colhApproveLevelName
        '
        Me.colhApproveLevelName.Tag = "colhApproveLevelName"
        Me.colhApproveLevelName.Text = "Level Name"
        Me.colhApproveLevelName.Width = 400
        '
        'colhApproveLevelPriority
        '
        Me.colhApproveLevelPriority.Tag = "colhApproveLevelPriority"
        Me.colhApproveLevelPriority.Text = "Priority"
        Me.colhApproveLevelPriority.Width = 105
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(687, 60)
        Me.eZeeHeader.TabIndex = 9
        Me.eZeeHeader.Title = "Approver Level List"
        '
        'frmPaymentApproverLevelList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 445)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPaymentApproverLevelList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approver Level List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvApproverLevelList As eZee.Common.eZeeListView
    Friend WithEvents colhApproveLevelCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApproveLevelName As System.Windows.Forms.ColumnHeader
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents colhApproveLevelPriority As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblActiveInactive As System.Windows.Forms.Label
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnActive As eZee.Common.eZeeLightButton
    Friend WithEvents btnInactive As eZee.Common.eZeeLightButton
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Experience
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmployeeExperience = New eZee.Common.eZeeLine
        Me.lvJobHistory = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhSupervisor = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elEmployeeExperience
        '
        Me.elEmployeeExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmployeeExperience.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmployeeExperience.Location = New System.Drawing.Point(3, 11)
        Me.elEmployeeExperience.Name = "elEmployeeExperience"
        Me.elEmployeeExperience.Size = New System.Drawing.Size(575, 17)
        Me.elEmployeeExperience.TabIndex = 315
        Me.elEmployeeExperience.Text = "Employee Experience"
        Me.elEmployeeExperience.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvJobHistory
        '
        Me.lvJobHistory.BackColorOnChecked = True
        Me.lvJobHistory.ColumnHeaders = Nothing
        Me.lvJobHistory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhCompany, Me.colhJob, Me.colhStartDate, Me.colhEndDate, Me.colhSupervisor, Me.colhRemark})
        Me.lvJobHistory.CompulsoryColumns = ""
        Me.lvJobHistory.FullRowSelect = True
        Me.lvJobHistory.GridLines = True
        Me.lvJobHistory.GroupingColumn = Nothing
        Me.lvJobHistory.HideSelection = False
        Me.lvJobHistory.Location = New System.Drawing.Point(6, 31)
        Me.lvJobHistory.MinColumnWidth = 50
        Me.lvJobHistory.MultiSelect = False
        Me.lvJobHistory.Name = "lvJobHistory"
        Me.lvJobHistory.OptionalColumns = ""
        Me.lvJobHistory.ShowMoreItem = False
        Me.lvJobHistory.ShowSaveItem = False
        Me.lvJobHistory.ShowSelectAll = True
        Me.lvJobHistory.ShowSizeAllColumnsToFit = True
        Me.lvJobHistory.Size = New System.Drawing.Size(572, 375)
        Me.lvJobHistory.Sortable = True
        Me.lvJobHistory.TabIndex = 316
        Me.lvJobHistory.UseCompatibleStateImageBehavior = False
        Me.lvJobHistory.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhCompany
        '
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 100
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 100
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 90
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 90
        '
        'colhSupervisor
        '
        Me.colhSupervisor.Tag = "colhSupervisor"
        Me.colhSupervisor.Text = "Supervisor"
        Me.colhSupervisor.Width = 100
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 100
        '
        'objfrmDiary_Experience
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvJobHistory)
        Me.Controls.Add(Me.elEmployeeExperience)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Experience"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elEmployeeExperience As eZee.Common.eZeeLine
    Friend WithEvents lvJobHistory As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSupervisor As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
End Class

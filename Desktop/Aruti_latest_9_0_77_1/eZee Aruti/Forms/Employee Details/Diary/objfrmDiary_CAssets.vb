﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_CAssets

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_References_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvAssetRegister.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_References_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem
        Dim objAsset As New clsEmployee_Assets_Tran
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objAsset.GetList("AssetList", mintEmployeeId)
            dsList = objAsset.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "AssetList", , mintEmployeeId)
            'S.SANDEEP [04 JUN 2015] -- END

            lvAssetRegister.Items.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("Asset").ToString)
                lvItem.SubItems.Add(dtRow.Item("AssetNo").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString)

                lvItem.SubItems.Add(dtRow.Item("Status").ToString)
                lvItem.SubItems(colhStatus.Index).Tag = CInt(dtRow.Item("asset_statusunkid").ToString)

                lvItem.SubItems.Add(dtRow.Item("Condtion").ToString)
                lvItem.SubItems.Add(dtRow.Item("Remark").ToString)

                lvItem.Tag = dtRow.Item("assetstranunkid")

                lvAssetRegister.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvAssetRegister.Items.Count > 3 Then
                colhRemark.Width = 173 - 18
            Else
                colhRemark.Width = 173
            End If

            lvAssetRegister.GroupingColumn = colhEmployeeName
            lvAssetRegister.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Public Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)

            Me.elCompanyAssets.Text = Language._Object.getCaption(Me.elCompanyAssets.Name, Me.elCompanyAssets.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.colhAssetName.Text = Language._Object.getCaption(CStr(Me.colhAssetName.Tag), Me.colhAssetName.Text)
            Me.colhAssestNo.Text = Language._Object.getCaption(CStr(Me.colhAssestNo.Tag), Me.colhAssestNo.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhCondition.Text = Language._Object.getCaption(CStr(Me.colhCondition.Tag), Me.colhCondition.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
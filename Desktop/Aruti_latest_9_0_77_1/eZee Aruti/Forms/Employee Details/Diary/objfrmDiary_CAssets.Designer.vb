﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_CAssets
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elCompanyAssets = New eZee.Common.eZeeLine
        Me.lvAssetRegister = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployeeName = New System.Windows.Forms.ColumnHeader
        Me.colhAssetName = New System.Windows.Forms.ColumnHeader
        Me.colhAssestNo = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhCondition = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elCompanyAssets
        '
        Me.elCompanyAssets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elCompanyAssets.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elCompanyAssets.Location = New System.Drawing.Point(3, 12)
        Me.elCompanyAssets.Name = "elCompanyAssets"
        Me.elCompanyAssets.Size = New System.Drawing.Size(575, 17)
        Me.elCompanyAssets.TabIndex = 317
        Me.elCompanyAssets.Text = "Company Assets"
        Me.elCompanyAssets.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvAssetRegister
        '
        Me.lvAssetRegister.BackColorOnChecked = True
        Me.lvAssetRegister.ColumnHeaders = Nothing
        Me.lvAssetRegister.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployeeName, Me.colhAssetName, Me.colhAssestNo, Me.colhDate, Me.colhStatus, Me.colhCondition, Me.colhRemark})
        Me.lvAssetRegister.CompulsoryColumns = ""
        Me.lvAssetRegister.FullRowSelect = True
        Me.lvAssetRegister.GridLines = True
        Me.lvAssetRegister.GroupingColumn = Nothing
        Me.lvAssetRegister.HideSelection = False
        Me.lvAssetRegister.Location = New System.Drawing.Point(6, 32)
        Me.lvAssetRegister.MinColumnWidth = 50
        Me.lvAssetRegister.MultiSelect = False
        Me.lvAssetRegister.Name = "lvAssetRegister"
        Me.lvAssetRegister.OptionalColumns = ""
        Me.lvAssetRegister.ShowMoreItem = False
        Me.lvAssetRegister.ShowSaveItem = False
        Me.lvAssetRegister.ShowSelectAll = True
        Me.lvAssetRegister.ShowSizeAllColumnsToFit = True
        Me.lvAssetRegister.Size = New System.Drawing.Size(572, 374)
        Me.lvAssetRegister.Sortable = True
        Me.lvAssetRegister.TabIndex = 318
        Me.lvAssetRegister.UseCompatibleStateImageBehavior = False
        Me.lvAssetRegister.View = System.Windows.Forms.View.Details
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.Tag = "colhEmployeeName"
        Me.colhEmployeeName.Text = "Employee"
        Me.colhEmployeeName.Width = 0
        '
        'colhAssetName
        '
        Me.colhAssetName.Tag = "colhAssetName"
        Me.colhAssetName.Text = "Asset Name"
        Me.colhAssetName.Width = 120
        '
        'colhAssestNo
        '
        Me.colhAssestNo.Tag = "colhAssestNo"
        Me.colhAssestNo.Text = "Asset No."
        Me.colhAssestNo.Width = 100
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'colhCondition
        '
        Me.colhCondition.Tag = "colhCondition"
        Me.colhCondition.Text = "Condition"
        Me.colhCondition.Width = 100
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 100
        '
        'objfrmDiary_CAssets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvAssetRegister)
        Me.Controls.Add(Me.elCompanyAssets)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_CAssets"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elCompanyAssets As eZee.Common.eZeeLine
    Friend WithEvents lvAssetRegister As eZee.Common.eZeeListView
    Friend WithEvents colhEmployeeName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssetName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAssestNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCondition As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Leave
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.elPreviousLeave = New eZee.Common.eZeeLine
        Me.elForecastedLeave = New eZee.Common.eZeeLine
        Me.lvPreviousLeaveList = New System.Windows.Forms.ListView
        Me.colhPreLeaveType = New System.Windows.Forms.ColumnHeader
        Me.colhPreLeaveFrom = New System.Windows.Forms.ColumnHeader
        Me.colhPreStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhPreReturnDate = New System.Windows.Forms.ColumnHeader
        Me.colhPreStatus = New System.Windows.Forms.ColumnHeader
        Me.lvNextLeaveList = New System.Windows.Forms.ListView
        Me.colhNxtLeaveType = New System.Windows.Forms.ColumnHeader
        Me.colhNxtFormNo = New System.Windows.Forms.ColumnHeader
        Me.colhNxtStDate = New System.Windows.Forms.ColumnHeader
        Me.colhNxtEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhNxtStatus = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elPreviousLeave
        '
        Me.elPreviousLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPreviousLeave.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPreviousLeave.Location = New System.Drawing.Point(2, 6)
        Me.elPreviousLeave.Name = "elPreviousLeave"
        Me.elPreviousLeave.Size = New System.Drawing.Size(575, 17)
        Me.elPreviousLeave.TabIndex = 307
        Me.elPreviousLeave.Text = "Previous Leave"
        Me.elPreviousLeave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'elForecastedLeave
        '
        Me.elForecastedLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elForecastedLeave.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elForecastedLeave.Location = New System.Drawing.Point(2, 208)
        Me.elForecastedLeave.Name = "elForecastedLeave"
        Me.elForecastedLeave.Size = New System.Drawing.Size(575, 17)
        Me.elForecastedLeave.TabIndex = 393
        Me.elForecastedLeave.Text = "Forecasted Leave"
        Me.elForecastedLeave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvPreviousLeaveList
        '
        Me.lvPreviousLeaveList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPreLeaveType, Me.colhPreLeaveFrom, Me.colhPreStartDate, Me.colhPreReturnDate, Me.colhPreStatus})
        Me.lvPreviousLeaveList.FullRowSelect = True
        Me.lvPreviousLeaveList.Location = New System.Drawing.Point(5, 26)
        Me.lvPreviousLeaveList.Name = "lvPreviousLeaveList"
        Me.lvPreviousLeaveList.Size = New System.Drawing.Size(572, 177)
        Me.lvPreviousLeaveList.TabIndex = 394
        Me.lvPreviousLeaveList.UseCompatibleStateImageBehavior = False
        Me.lvPreviousLeaveList.View = System.Windows.Forms.View.Details
        '
        'colhPreLeaveType
        '
        Me.colhPreLeaveType.Tag = "colhPreLeaveType"
        Me.colhPreLeaveType.Text = "Leave Type"
        Me.colhPreLeaveType.Width = 195
        '
        'colhPreLeaveFrom
        '
        Me.colhPreLeaveFrom.Tag = "colhPreLeaveFrom"
        Me.colhPreLeaveFrom.Text = "Form No."
        Me.colhPreLeaveFrom.Width = 100
        '
        'colhPreStartDate
        '
        Me.colhPreStartDate.Tag = "colhPreStartDate"
        Me.colhPreStartDate.Text = "Start Date"
        Me.colhPreStartDate.Width = 85
        '
        'colhPreReturnDate
        '
        Me.colhPreReturnDate.Tag = "colhPreReturnDate"
        Me.colhPreReturnDate.Text = "Return Date"
        Me.colhPreReturnDate.Width = 85
        '
        'colhPreStatus
        '
        Me.colhPreStatus.Tag = "colhPreStatus"
        Me.colhPreStatus.Text = "Status"
        Me.colhPreStatus.Width = 100
        '
        'lvNextLeaveList
        '
        Me.lvNextLeaveList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhNxtLeaveType, Me.colhNxtFormNo, Me.colhNxtStDate, Me.colhNxtEndDate, Me.colhNxtStatus})
        Me.lvNextLeaveList.FullRowSelect = True
        Me.lvNextLeaveList.Location = New System.Drawing.Point(5, 230)
        Me.lvNextLeaveList.Name = "lvNextLeaveList"
        Me.lvNextLeaveList.Size = New System.Drawing.Size(572, 176)
        Me.lvNextLeaveList.TabIndex = 395
        Me.lvNextLeaveList.UseCompatibleStateImageBehavior = False
        Me.lvNextLeaveList.View = System.Windows.Forms.View.Details
        '
        'colhNxtLeaveType
        '
        Me.colhNxtLeaveType.Tag = "colhNxtLeaveType"
        Me.colhNxtLeaveType.Text = "Leave Type"
        Me.colhNxtLeaveType.Width = 195
        '
        'colhNxtFormNo
        '
        Me.colhNxtFormNo.Tag = "colhNxtFormNo"
        Me.colhNxtFormNo.Text = "Form No."
        Me.colhNxtFormNo.Width = 100
        '
        'colhNxtStDate
        '
        Me.colhNxtStDate.Tag = "colhNxtStDate"
        Me.colhNxtStDate.Text = "Start Date"
        Me.colhNxtStDate.Width = 85
        '
        'colhNxtEndDate
        '
        Me.colhNxtEndDate.Tag = "colhNxtEndDate"
        Me.colhNxtEndDate.Text = "Return Date"
        Me.colhNxtEndDate.Width = 85
        '
        'colhNxtStatus
        '
        Me.colhNxtStatus.Tag = "colhNxtStatus"
        Me.colhNxtStatus.Text = "Status"
        Me.colhNxtStatus.Width = 100
        '
        'objfrmDiary_Leave
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvNextLeaveList)
        Me.Controls.Add(Me.lvPreviousLeaveList)
        Me.Controls.Add(Me.elForecastedLeave)
        Me.Controls.Add(Me.elPreviousLeave)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Leave"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elPreviousLeave As eZee.Common.eZeeLine
    Friend WithEvents elForecastedLeave As eZee.Common.eZeeLine
    Friend WithEvents lvPreviousLeaveList As System.Windows.Forms.ListView
    Friend WithEvents lvNextLeaveList As System.Windows.Forms.ListView
    Friend WithEvents colhPreLeaveType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPreLeaveFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPreStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPreReturnDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPreStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNxtLeaveType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNxtFormNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNxtStDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNxtEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhNxtStatus As System.Windows.Forms.ColumnHeader
End Class

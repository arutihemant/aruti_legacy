﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Detail

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mobjEmpMaster As clsEmployee_Master
    Private mobjEmplMaster As clsCommon_Master
    Private mobjTitleMaster As clsCommon_Master
    Private mobjBranch As clsStation
    Private mobjDeptGrp As clsDepartmentGroup
    Private mobjDept As clsDepartment
    Private mobjSection As clsSections
    Private mobjUnit As clsUnits
    Private mobjJGrp As clsJobGroup
    Private mobjJob As clsJobs
    Private mobjGradeGrp As clsGradeGroup
    Private mobjGrade As clsGrade
    Private mobjGradeLevel As clsGradeLevel

    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes
    'Private mobjShift As clsshift_master
    Private mobjShift As clsNewshift_master
    'Pinkal (19-Nov-2012) -- End


    Private mobjClsGrp As clsClassGroup
    Private mobjCls As clsClass

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mobjEmpMaster = New clsEmployee_Master

        'Pinkal (09-May-2013) -- Start
        'Enhancement : TRA Changes

        mobjEmpMaster._Companyunkid = ConfigParameter._Object._Companyunkid
        mobjEmpMaster._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

        'Pinkal (09-May-2013) -- End


        mobjEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
        mobjEmplMaster = New clsCommon_Master
        mobjTitleMaster = New clsCommon_Master
        mobjBranch = New clsStation
        mobjDeptGrp = New clsDepartmentGroup
        mobjDept = New clsDepartment
        mobjSection = New clsSections
        mobjUnit = New clsUnits
        mobjJGrp = New clsJobGroup
        mobjJob = New clsJobs
        mobjGradeGrp = New clsGradeGroup
        mobjGrade = New clsGrade
        mobjGradeLevel = New clsGradeLevel


        'Pinkal (19-Nov-2012) -- Start
        'Enhancement : TRA Changes
        'mobjShift = New clsshift_master
        mobjShift = New clsNewshift_master
        'Pinkal (19-Nov-2012) -- End


        mobjClsGrp = New clsClassGroup
        mobjCls = New clsClass
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Detail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            'S.SANDEEP [11-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
            If User._Object.Privilege._AllowtoSeeEmployeeSignature Then
                If ConfigParameter._Object._IsSymmetryIntegrated Then
                    If ConfigParameter._Object._IsImgInDataBase Then
                        lnkShowImage.Visible = False
                        lnkShowSignature.Visible = True
                    End If
                Else
                    lnkShowImage.Visible = False
                    lnkShowSignature.Visible = False
                End If
            Else
                lnkShowImage.Visible = False
                lnkShowSignature.Visible = False
            End If
            'S.SANDEEP [11-AUG-2017] -- END

            imgImageControl._FilePath = ConfigParameter._Object._PhotoPath
            Call Fill_Info()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Detail_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Fucntions "

    Private Sub AssignValue()
        Try
            mobjEmplMaster._Masterunkid = mobjEmpMaster._Employmenttypeunkid
            mobjTitleMaster._Masterunkid = mobjEmpMaster._Titalunkid
            mobjBranch._Stationunkid = mobjEmpMaster._Stationunkid
            mobjDeptGrp._Deptgroupunkid = mobjEmpMaster._Deptgroupunkid
            mobjDept._Departmentunkid = mobjEmpMaster._Departmentunkid
            mobjSection._Sectionunkid = mobjEmpMaster._Sectionunkid
            mobjUnit._Unitunkid = mobjEmpMaster._Unitunkid
            mobjJGrp._Jobgroupunkid = mobjEmpMaster._Jobgroupunkid
            mobjJob._Jobunkid = mobjEmpMaster._Jobunkid
            mobjGradeGrp._Gradegroupunkid = mobjEmpMaster._Gradegroupunkid
            mobjGrade._Gradeunkid = mobjEmpMaster._Gradeunkid
            mobjGradeLevel._Gradelevelunkid = mobjEmpMaster._Gradelevelunkid
            mobjShift._Shiftunkid = mobjEmpMaster._Shiftunkid
            mobjClsGrp._Classgroupunkid = mobjEmpMaster._Classgroupunkid
            mobjCls._Classesunkid = mobjEmpMaster._Classunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AssignValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Info()
        Try
            Call AssignValue()



            'Pinkal (09-May-2013) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsImgInDataBase Then
                imgImageControl._Image = Nothing
                If mobjEmpMaster._Photo IsNot Nothing Then
                    Dim ms As New System.IO.MemoryStream(mobjEmpMaster._Photo, 0, mobjEmpMaster._Photo.Length)
                    imgImageControl._Image = Image.FromStream(ms)
                End If
            Else

                If mobjEmpMaster._ImagePath.Trim.Length > 0 Then
                    imgImageControl._FileName = imgImageControl._FileName & "\" & mobjEmpMaster._ImagePath
                Else
                    imgImageControl._FileName = ""
                End If

            End If

            'Pinkal (09-May-2013) -- End

            'S.SANDEEP [16-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 118
            If User._Object.Privilege._AllowtoSeeEmployeeSignature Then
                If ConfigParameter._Object._IsSymmetryIntegrated = False AndAlso mobjEmpMaster._EmpSignature IsNot Nothing Then
                    lnkShowImage.Visible = False
                    lnkShowSignature.Visible = True
                End If
            End If
            'S.SANDEEP [16-Jan-2018] -- END

            txtAppointedDate.Text = CStr(mobjEmpMaster._Appointeddate.Date)
            txtBranch.Text = mobjBranch._Name
            txtDeptGroup.Text = mobjDeptGrp._Name
            txtDepartment.Text = mobjDept._Name
            txtSection.Text = mobjSection._Name
            txtUnit.Text = mobjUnit._Name
            txtJobGroup.Text = mobjJGrp._Name
            txtJob.Text = mobjJob._Job_Name
            txtGradeGroup.Text = mobjGradeGrp._Name
            txtGrade.Text = mobjGrade._Name
            txtGradeLevel.Text = mobjGradeLevel._Name
            txtShift.Text = mobjShift._Shiftname
            txtClassGroup.Text = mobjClsGrp._Name
            txtClass.Text = mobjCls._Name
            txtCode.Text = mobjEmpMaster._Employeecode
            txtFirstname.Text = mobjEmpMaster._Firstname
            txtSurname.Text = mobjEmpMaster._Surname
            txtEmail.Text = mobjEmpMaster._Email
            txtEmployeeType.Text = mobjEmplMaster._Name
            txttitle.Text = mobjTitleMaster._Name
         

            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If User._Object.Privilege._AllowTo_View_Scale Then
                lblScale.Visible = True : txtScale.Visible = True
                Dim dsList As New DataSet
                Dim objMaster As New clsMasterData
                dsList = objMaster.Get_Current_Scale("List", mobjEmpMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)), ConfigParameter._Object._CurrentDateAndTime)
                If dsList.Tables(0).Rows.Count > 0 Then
                    txtScale.Text = Format(CDec(dsList.Tables("List").Rows(0).Item("newscale")), GUI.fmtCurrency).ToString
                End If
            Else
                lblScale.Visible = False : txtScale.Visible = False
            End If
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    'S.SANDEEP [11-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI SYMMETRY INTEGRATION
#Region " Link Event(s) "

    Private Sub lnkShowImage_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowImage.LinkClicked
        Try
            If ConfigParameter._Object._IsImgInDataBase Then
                imgImageControl._Image = Nothing
                If mobjEmpMaster._Photo IsNot Nothing Then
                    Dim ms As New System.IO.MemoryStream(mobjEmpMaster._Photo, 0, mobjEmpMaster._Photo.Length)
                    imgImageControl._Image = Image.FromStream(ms)
                End If
                lnkShowImage.Visible = False : lnkShowSignature.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowImage_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkShowSignature_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkShowSignature.LinkClicked
        Try
            'S.SANDEEP [16-Jan-2018] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 118
            'If ConfigParameter._Object._IsImgInDataBase Then
            '    imgImageControl._Image = Nothing
            '    If mobjEmpMaster._Signature IsNot Nothing Then
            '        Dim ms As New System.IO.MemoryStream(mobjEmpMaster._Signature, 0, mobjEmpMaster._Signature.Length)
            '        imgImageControl._Image = Image.FromStream(ms)
            '    End If
            '    lnkShowImage.Visible = True : lnkShowSignature.Visible = False
            'End If

            If ConfigParameter._Object._IsImgInDataBase Then
                imgImageControl._Image = Nothing
                Dim bytSignature As Byte() = Nothing
                If ConfigParameter._Object._IsSymmetryIntegrated Then
                If mobjEmpMaster._Signature IsNot Nothing Then
                        bytSignature = mobjEmpMaster._Signature
                    End If
                Else
                    bytSignature = mobjEmpMaster._EmpSignature
                End If

                If bytSignature Is Nothing AndAlso mobjEmpMaster._EmpSignature IsNot Nothing Then
                    bytSignature = mobjEmpMaster._EmpSignature
                End If

                If bytSignature IsNot Nothing Then
                    Dim ms As New System.IO.MemoryStream(bytSignature, 0, bytSignature.Length)
                    imgImageControl._Image = Image.FromStream(ms)
                End If
                lnkShowImage.Visible = True : lnkShowSignature.Visible = False
            Else
                imgImageControl._Image = Nothing
                If mobjEmpMaster._EmpSignature IsNot Nothing Then
                    Dim ms As New System.IO.MemoryStream(mobjEmpMaster._EmpSignature, 0, mobjEmpMaster._EmpSignature.Length)
                    imgImageControl._Image = Image.FromStream(ms)
                End If
                lnkShowImage.Visible = True : lnkShowSignature.Visible = False
            End If
            'S.SANDEEP [16-Jan-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkShowSignature_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [11-AUG-2017] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elPersonalInfo.Text = Language._Object.getCaption(Me.elPersonalInfo.Name, Me.elPersonalInfo.Text)
			Me.imgImageControl.Text = Language._Object.getCaption(Me.imgImageControl.Name, Me.imgImageControl.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
			Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
			Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.Name, Me.lblFirstName.Text)
			Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.Name, Me.lblSurname.Text)
			Me.lblAppointeddate.Text = Language._Object.getCaption(Me.lblAppointeddate.Name, Me.lblAppointeddate.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.elOfficialInfo.Text = Language._Object.getCaption(Me.elOfficialInfo.Name, Me.elOfficialInfo.Text)
			Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lblUnit.Text = Language._Object.getCaption(Me.lblUnit.Name, Me.lblUnit.Text)
			Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.lblEmployeeType.Text = Language._Object.getCaption(Me.lblEmployeeType.Name, Me.lblEmployeeType.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
			Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.Name, Me.lblScale.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
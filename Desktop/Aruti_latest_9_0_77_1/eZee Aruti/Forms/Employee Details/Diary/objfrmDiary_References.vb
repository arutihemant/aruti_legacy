﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_References

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_References_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvRefreeList.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_References_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Dim objReferee As New clsEmployee_Refree_tran
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objReferee.GetList("RefList")

            'If CInt(mintEmployeeId) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(mintEmployeeId) & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("RefList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("RefList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If

            dsList = objReferee.GetList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, True, _
                                        ConfigParameter._Object._IsIncludeInactiveEmp, "RefList", , , "hremployee_master.employeeunkid = " & CInt(mintEmployeeId) & " ")

            dtTable = New DataView(dsList.Tables("RefList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [04 JUN 2015] -- END


            

            lvRefreeList.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("RefName").ToString)
                lvItem.SubItems.Add(dtRow.Item("Country").ToString)
                lvItem.SubItems.Add(dtRow.Item("Company").ToString)
                lvItem.SubItems.Add(dtRow.Item("Email").ToString)
                lvItem.SubItems.Add(dtRow.Item("telephone_no").ToString)
                lvItem.SubItems.Add(dtRow.Item("mobile_no").ToString)

                lvItem.Tag = dtRow.Item("RefreeTranId")

                lvRefreeList.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvRefreeList.Items.Count > 5 Then
                colhRefreeName.Width = 200 - 20
            Else
                colhRefreeName.Width = 200
            End If

            lvRefreeList.GroupingColumn = colhEmployee
            lvRefreeList.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmployeeReferences.Text = Language._Object.getCaption(Me.elEmployeeReferences.Name, Me.elEmployeeReferences.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhRefreeName.Text = Language._Object.getCaption(CStr(Me.colhRefreeName.Tag), Me.colhRefreeName.Text)
			Me.colhCountry.Text = Language._Object.getCaption(CStr(Me.colhCountry.Tag), Me.colhCountry.Text)
			Me.colhIdNo.Text = Language._Object.getCaption(CStr(Me.colhIdNo.Tag), Me.colhIdNo.Text)
			Me.colhEmail.Text = Language._Object.getCaption(CStr(Me.colhEmail.Tag), Me.colhEmail.Text)
			Me.colhTelNo.Text = Language._Object.getCaption(CStr(Me.colhTelNo.Tag), Me.colhTelNo.Text)
			Me.colhMobile.Text = Language._Object.getCaption(CStr(Me.colhMobile.Tag), Me.colhMobile.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
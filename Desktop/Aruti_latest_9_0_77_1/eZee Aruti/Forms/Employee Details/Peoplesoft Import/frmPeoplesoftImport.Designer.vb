﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPeoplesoftImport
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPeoplesoftImport))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lblDependant = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dgvDependant = New System.Windows.Forms.DataGridView
        Me.dpcolhGlobalID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhLocalID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhEmpID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhOtherName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhSurname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhBirthDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhAddress = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhMobileNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhState = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhCity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dpcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhGlobalID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhOtherName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSurname = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppointedDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmplType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthCountry = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVillage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhMaritalStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAnniversaryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClassGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIncrementDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHeadType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLeavingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEOCDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRetirementDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReinstatementDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLastUpdateime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnHeadOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuShowSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowUnSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDependant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.lblDependant)
        Me.pnlMainInfo.Controls.Add(Me.Panel2)
        Me.pnlMainInfo.Controls.Add(Me.Panel1)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(834, 612)
        Me.pnlMainInfo.TabIndex = 3
        '
        'lblDependant
        '
        Me.lblDependant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependant.Location = New System.Drawing.Point(12, 414)
        Me.lblDependant.Name = "lblDependant"
        Me.lblDependant.Size = New System.Drawing.Size(350, 13)
        Me.lblDependant.TabIndex = 21
        Me.lblDependant.Text = "Dependants"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvDependant)
        Me.Panel2.Location = New System.Drawing.Point(12, 430)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(810, 121)
        Me.Panel2.TabIndex = 20
        '
        'dgvDependant
        '
        Me.dgvDependant.AllowUserToAddRows = False
        Me.dgvDependant.AllowUserToDeleteRows = False
        Me.dgvDependant.AllowUserToResizeRows = False
        Me.dgvDependant.BackgroundColor = System.Drawing.Color.White
        Me.dgvDependant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDependant.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dpcolhGlobalID, Me.dpcolhLocalID, Me.dpcolhRemark, Me.dpcolhEmpID, Me.dpcolhFirstName, Me.dpcolhOtherName, Me.dpcolhSurname, Me.dpcolhGender, Me.dpcolhRelation, Me.dpcolhBirthDate, Me.dpcolhAddress, Me.dpcolhMobileNo, Me.dpcolhCountry, Me.dpcolhState, Me.dpcolhCity, Me.dpcolhEmail})
        Me.dgvDependant.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDependant.Location = New System.Drawing.Point(0, 0)
        Me.dgvDependant.MultiSelect = False
        Me.dgvDependant.Name = "dgvDependant"
        Me.dgvDependant.ReadOnly = True
        Me.dgvDependant.RowHeadersVisible = False
        Me.dgvDependant.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDependant.Size = New System.Drawing.Size(810, 121)
        Me.dgvDependant.TabIndex = 22
        '
        'dpcolhGlobalID
        '
        Me.dpcolhGlobalID.Frozen = True
        Me.dpcolhGlobalID.HeaderText = "Global ID"
        Me.dpcolhGlobalID.Name = "dpcolhGlobalID"
        Me.dpcolhGlobalID.ReadOnly = True
        Me.dpcolhGlobalID.Width = 90
        '
        'dpcolhLocalID
        '
        Me.dpcolhLocalID.Frozen = True
        Me.dpcolhLocalID.HeaderText = "Local ID"
        Me.dpcolhLocalID.Name = "dpcolhLocalID"
        Me.dpcolhLocalID.ReadOnly = True
        Me.dpcolhLocalID.Width = 90
        '
        'dpcolhRemark
        '
        Me.dpcolhRemark.Frozen = True
        Me.dpcolhRemark.HeaderText = "Remark"
        Me.dpcolhRemark.Name = "dpcolhRemark"
        Me.dpcolhRemark.ReadOnly = True
        Me.dpcolhRemark.Width = 200
        '
        'dpcolhEmpID
        '
        Me.dpcolhEmpID.Frozen = True
        Me.dpcolhEmpID.HeaderText = "EmpID"
        Me.dpcolhEmpID.Name = "dpcolhEmpID"
        Me.dpcolhEmpID.ReadOnly = True
        Me.dpcolhEmpID.Visible = False
        '
        'dpcolhFirstName
        '
        Me.dpcolhFirstName.HeaderText = "First Name"
        Me.dpcolhFirstName.Name = "dpcolhFirstName"
        Me.dpcolhFirstName.ReadOnly = True
        '
        'dpcolhOtherName
        '
        Me.dpcolhOtherName.HeaderText = "Other Name"
        Me.dpcolhOtherName.Name = "dpcolhOtherName"
        Me.dpcolhOtherName.ReadOnly = True
        '
        'dpcolhSurname
        '
        Me.dpcolhSurname.HeaderText = "Surname"
        Me.dpcolhSurname.Name = "dpcolhSurname"
        Me.dpcolhSurname.ReadOnly = True
        '
        'dpcolhGender
        '
        Me.dpcolhGender.HeaderText = "Gender"
        Me.dpcolhGender.Name = "dpcolhGender"
        Me.dpcolhGender.ReadOnly = True
        Me.dpcolhGender.Width = 70
        '
        'dpcolhRelation
        '
        Me.dpcolhRelation.HeaderText = "Relation"
        Me.dpcolhRelation.Name = "dpcolhRelation"
        Me.dpcolhRelation.ReadOnly = True
        '
        'dpcolhBirthDate
        '
        Me.dpcolhBirthDate.HeaderText = "Birth Date"
        Me.dpcolhBirthDate.Name = "dpcolhBirthDate"
        Me.dpcolhBirthDate.ReadOnly = True
        '
        'dpcolhAddress
        '
        Me.dpcolhAddress.HeaderText = "Address"
        Me.dpcolhAddress.Name = "dpcolhAddress"
        Me.dpcolhAddress.ReadOnly = True
        '
        'dpcolhMobileNo
        '
        Me.dpcolhMobileNo.HeaderText = "Mobile No"
        Me.dpcolhMobileNo.Name = "dpcolhMobileNo"
        Me.dpcolhMobileNo.ReadOnly = True
        '
        'dpcolhCountry
        '
        Me.dpcolhCountry.HeaderText = "Country"
        Me.dpcolhCountry.Name = "dpcolhCountry"
        Me.dpcolhCountry.ReadOnly = True
        '
        'dpcolhState
        '
        Me.dpcolhState.HeaderText = "State"
        Me.dpcolhState.Name = "dpcolhState"
        Me.dpcolhState.ReadOnly = True
        '
        'dpcolhCity
        '
        Me.dpcolhCity.HeaderText = "City"
        Me.dpcolhCity.Name = "dpcolhCity"
        Me.dpcolhCity.ReadOnly = True
        '
        'dpcolhEmail
        '
        Me.dpcolhEmail.HeaderText = "Email"
        Me.dpcolhEmail.Name = "dpcolhEmail"
        Me.dpcolhEmail.ReadOnly = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.dgvImportInfo)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(810, 392)
        Me.Panel1.TabIndex = 19
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 5)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 22
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvImportInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhGlobalID, Me.dgcolhEmpCode, Me.dgcolhRemark, Me.dgcolhEmpID, Me.dgcolhTitle, Me.dgcolhFirstName, Me.dgcolhOtherName, Me.dgcolhSurname, Me.dgcolhGender, Me.dgcolhAppointedDate, Me.dgcolhEmplType, Me.dgcolhBirthDate, Me.dgcolhBirthCountry, Me.dgcolhVillage, Me.dgcolhMaritalStatus, Me.dgcolhAnniversaryDate, Me.dgcolhDept, Me.dgcolhJob, Me.dgcolhBranch, Me.dgcolhJobGroup, Me.dgcolhCCenter, Me.dgcolhClassGroup, Me.dgcolhIncrementDate, Me.dgcolhScale, Me.dgcolhTranHead, Me.dgcolhHeadType, Me.dgcolhLeavingDate, Me.dgcolhEOCDate, Me.dgcolhRetirementDate, Me.dgcolhReinstatementDate, Me.dgcolhLastUpdateime})
        Me.dgvImportInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvImportInfo.Location = New System.Drawing.Point(0, 0)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(810, 392)
        Me.dgvImportInfo.TabIndex = 21
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhGlobalID
        '
        Me.dgcolhGlobalID.Frozen = True
        Me.dgcolhGlobalID.HeaderText = "Global ID"
        Me.dgcolhGlobalID.Name = "dgcolhGlobalID"
        Me.dgcolhGlobalID.ReadOnly = True
        Me.dgcolhGlobalID.Width = 90
        '
        'dgcolhEmpCode
        '
        Me.dgcolhEmpCode.Frozen = True
        Me.dgcolhEmpCode.HeaderText = "Local ID"
        Me.dgcolhEmpCode.Name = "dgcolhEmpCode"
        Me.dgcolhEmpCode.ReadOnly = True
        Me.dgcolhEmpCode.Width = 90
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.Frozen = True
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.Width = 200
        '
        'dgcolhEmpID
        '
        Me.dgcolhEmpID.HeaderText = "EmpID"
        Me.dgcolhEmpID.Name = "dgcolhEmpID"
        Me.dgcolhEmpID.ReadOnly = True
        Me.dgcolhEmpID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEmpID.Visible = False
        '
        'dgcolhTitle
        '
        Me.dgcolhTitle.HeaderText = "Title"
        Me.dgcolhTitle.Name = "dgcolhTitle"
        Me.dgcolhTitle.ReadOnly = True
        Me.dgcolhTitle.Width = 60
        '
        'dgcolhFirstName
        '
        Me.dgcolhFirstName.HeaderText = "First Name"
        Me.dgcolhFirstName.Name = "dgcolhFirstName"
        Me.dgcolhFirstName.ReadOnly = True
        '
        'dgcolhOtherName
        '
        Me.dgcolhOtherName.HeaderText = "Other Name"
        Me.dgcolhOtherName.Name = "dgcolhOtherName"
        Me.dgcolhOtherName.ReadOnly = True
        '
        'dgcolhSurname
        '
        Me.dgcolhSurname.HeaderText = "Surname"
        Me.dgcolhSurname.Name = "dgcolhSurname"
        Me.dgcolhSurname.ReadOnly = True
        '
        'dgcolhGender
        '
        Me.dgcolhGender.HeaderText = "Gender"
        Me.dgcolhGender.Name = "dgcolhGender"
        Me.dgcolhGender.ReadOnly = True
        Me.dgcolhGender.Width = 70
        '
        'dgcolhAppointedDate
        '
        Me.dgcolhAppointedDate.HeaderText = "Appointed Date"
        Me.dgcolhAppointedDate.Name = "dgcolhAppointedDate"
        Me.dgcolhAppointedDate.ReadOnly = True
        '
        'dgcolhEmplType
        '
        Me.dgcolhEmplType.HeaderText = "Empl. Type"
        Me.dgcolhEmplType.Name = "dgcolhEmplType"
        Me.dgcolhEmplType.ReadOnly = True
        '
        'dgcolhBirthDate
        '
        Me.dgcolhBirthDate.HeaderText = "Birth Date"
        Me.dgcolhBirthDate.Name = "dgcolhBirthDate"
        Me.dgcolhBirthDate.ReadOnly = True
        '
        'dgcolhBirthCountry
        '
        Me.dgcolhBirthCountry.HeaderText = "Birth Country"
        Me.dgcolhBirthCountry.Name = "dgcolhBirthCountry"
        Me.dgcolhBirthCountry.ReadOnly = True
        '
        'dgcolhVillage
        '
        Me.dgcolhVillage.HeaderText = "Birth Village"
        Me.dgcolhVillage.Name = "dgcolhVillage"
        Me.dgcolhVillage.ReadOnly = True
        '
        'dgcolhMaritalStatus
        '
        Me.dgcolhMaritalStatus.HeaderText = "Marital Status"
        Me.dgcolhMaritalStatus.Name = "dgcolhMaritalStatus"
        Me.dgcolhMaritalStatus.ReadOnly = True
        '
        'dgcolhAnniversaryDate
        '
        Me.dgcolhAnniversaryDate.HeaderText = "Anniversary Date"
        Me.dgcolhAnniversaryDate.Name = "dgcolhAnniversaryDate"
        Me.dgcolhAnniversaryDate.ReadOnly = True
        '
        'dgcolhDept
        '
        Me.dgcolhDept.HeaderText = "Department"
        Me.dgcolhDept.Name = "dgcolhDept"
        Me.dgcolhDept.ReadOnly = True
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        '
        'dgcolhBranch
        '
        Me.dgcolhBranch.HeaderText = "Branch"
        Me.dgcolhBranch.Name = "dgcolhBranch"
        Me.dgcolhBranch.ReadOnly = True
        '
        'dgcolhJobGroup
        '
        Me.dgcolhJobGroup.HeaderText = "Job Group"
        Me.dgcolhJobGroup.Name = "dgcolhJobGroup"
        Me.dgcolhJobGroup.ReadOnly = True
        '
        'dgcolhCCenter
        '
        Me.dgcolhCCenter.HeaderText = "Cost Center"
        Me.dgcolhCCenter.Name = "dgcolhCCenter"
        Me.dgcolhCCenter.ReadOnly = True
        '
        'dgcolhClassGroup
        '
        Me.dgcolhClassGroup.HeaderText = "Class Group"
        Me.dgcolhClassGroup.Name = "dgcolhClassGroup"
        Me.dgcolhClassGroup.ReadOnly = True
        '
        'dgcolhIncrementDate
        '
        Me.dgcolhIncrementDate.HeaderText = "Increment Date"
        Me.dgcolhIncrementDate.Name = "dgcolhIncrementDate"
        Me.dgcolhIncrementDate.ReadOnly = True
        '
        'dgcolhScale
        '
        Me.dgcolhScale.HeaderText = "Scale"
        Me.dgcolhScale.Name = "dgcolhScale"
        Me.dgcolhScale.ReadOnly = True
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Salary Head"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        '
        'dgcolhHeadType
        '
        Me.dgcolhHeadType.HeaderText = "HeadType"
        Me.dgcolhHeadType.Name = "dgcolhHeadType"
        Me.dgcolhHeadType.ReadOnly = True
        Me.dgcolhHeadType.Visible = False
        '
        'dgcolhLeavingDate
        '
        Me.dgcolhLeavingDate.HeaderText = "Leaving Date"
        Me.dgcolhLeavingDate.Name = "dgcolhLeavingDate"
        Me.dgcolhLeavingDate.ReadOnly = True
        '
        'dgcolhEOCDate
        '
        Me.dgcolhEOCDate.HeaderText = "EOC Date"
        Me.dgcolhEOCDate.Name = "dgcolhEOCDate"
        Me.dgcolhEOCDate.ReadOnly = True
        '
        'dgcolhRetirementDate
        '
        Me.dgcolhRetirementDate.HeaderText = "Retirement Date"
        Me.dgcolhRetirementDate.Name = "dgcolhRetirementDate"
        Me.dgcolhRetirementDate.ReadOnly = True
        '
        'dgcolhReinstatementDate
        '
        Me.dgcolhReinstatementDate.HeaderText = "Reinstatement Date"
        Me.dgcolhReinstatementDate.Name = "dgcolhReinstatementDate"
        Me.dgcolhReinstatementDate.ReadOnly = True
        Me.dgcolhReinstatementDate.Width = 120
        '
        'dgcolhLastUpdateime
        '
        Me.dgcolhLastUpdateime.HeaderText = "LastUpdateime"
        Me.dgcolhLastUpdateime.Name = "dgcolhLastUpdateime"
        Me.dgcolhLastUpdateime.ReadOnly = True
        Me.dgcolhLastUpdateime.Visible = False
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnExport)
        Me.objefFormFooter.Controls.Add(Me.btnHeadOperations)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.btnImport)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 557)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(834, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(128, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(97, 30)
        Me.btnExport.TabIndex = 102
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'btnHeadOperations
        '
        Me.btnHeadOperations.BorderColor = System.Drawing.Color.Black
        Me.btnHeadOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHeadOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnHeadOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnHeadOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnHeadOperations.Name = "btnHeadOperations"
        Me.btnHeadOperations.ShowDefaultBorderColor = True
        Me.btnHeadOperations.Size = New System.Drawing.Size(110, 30)
        Me.btnHeadOperations.SplitButtonMenu = Me.cmnuOperations
        Me.btnHeadOperations.TabIndex = 100
        Me.btnHeadOperations.Text = "Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuShowSuccessful, Me.mnuShowUnSuccessful, Me.mnuShowAll})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(192, 70)
        '
        'mnuShowSuccessful
        '
        Me.mnuShowSuccessful.Name = "mnuShowSuccessful"
        Me.mnuShowSuccessful.Size = New System.Drawing.Size(191, 22)
        Me.mnuShowSuccessful.Text = "Show Successful Data"
        '
        'mnuShowUnSuccessful
        '
        Me.mnuShowUnSuccessful.Name = "mnuShowUnSuccessful"
        Me.mnuShowUnSuccessful.Size = New System.Drawing.Size(191, 22)
        Me.mnuShowUnSuccessful.Text = "Show Unsuccessful Data"
        '
        'mnuShowAll
        '
        Me.mnuShowAll.Name = "mnuShowAll"
        Me.mnuShowAll.Size = New System.Drawing.Size(191, 22)
        Me.mnuShowAll.Text = "Show All Data"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(725, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(622, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 0
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = False
        '
        'frmPeoplesoftImport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 612)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPeoplesoftImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Peoplesoft Import"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvDependant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnHeadOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuShowSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowUnSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblDependant As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvDependant As System.Windows.Forms.DataGridView
    Friend WithEvents dpcolhGlobalID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhLocalID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhEmpID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhOtherName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhSurname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhBirthDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhAddress As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhMobileNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhState As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhCity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dpcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhGlobalID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhOtherName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSurname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppointedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmplType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthCountry As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVillage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhMaritalStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAnniversaryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClassGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIncrementDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHeadType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLeavingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEOCDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRetirementDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReinstatementDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLastUpdateime As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPeoplesoftMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPeoplesoftMapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.lblFile = New System.Windows.Forms.Label
        Me.lblSalaryChangeReason = New System.Windows.Forms.Label
        Me.cboSalaryChangeReason = New System.Windows.Forms.ComboBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblUSDSalary = New System.Windows.Forms.Label
        Me.cboUSDSalary = New System.Windows.Forms.ComboBox
        Me.lblTZSSalary = New System.Windows.Forms.Label
        Me.cboTZSSalary = New System.Windows.Forms.ComboBox
        Me.lblIdentityType = New System.Windows.Forms.Label
        Me.cboIdentityType = New System.Windows.Forms.ComboBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.objBgWorker = New System.ComponentModel.BackgroundWorker
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkAssignDefaulTranHeads = New System.Windows.Forms.CheckBox
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.chkAssignDefaulTranHeads)
        Me.pnlMain.Controls.Add(Me.txtFile)
        Me.pnlMain.Controls.Add(Me.lblFile)
        Me.pnlMain.Controls.Add(Me.lblSalaryChangeReason)
        Me.pnlMain.Controls.Add(Me.cboSalaryChangeReason)
        Me.pnlMain.Controls.Add(Me.lblGradeLevel)
        Me.pnlMain.Controls.Add(Me.cboGradeLevel)
        Me.pnlMain.Controls.Add(Me.lblGrade)
        Me.pnlMain.Controls.Add(Me.cboGrade)
        Me.pnlMain.Controls.Add(Me.lblGradeGroup)
        Me.pnlMain.Controls.Add(Me.cboGradeGroup)
        Me.pnlMain.Controls.Add(Me.lblUSDSalary)
        Me.pnlMain.Controls.Add(Me.cboUSDSalary)
        Me.pnlMain.Controls.Add(Me.lblTZSSalary)
        Me.pnlMain.Controls.Add(Me.cboTZSSalary)
        Me.pnlMain.Controls.Add(Me.lblIdentityType)
        Me.pnlMain.Controls.Add(Me.cboIdentityType)
        Me.pnlMain.Controls.Add(Me.lblShift)
        Me.pnlMain.Controls.Add(Me.cboShift)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(386, 340)
        Me.pnlMain.TabIndex = 0
        Me.pnlMain.Tag = ""
        '
        'txtFile
        '
        Me.txtFile.Location = New System.Drawing.Point(51, 251)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.ReadOnly = True
        Me.txtFile.Size = New System.Drawing.Size(323, 21)
        Me.txtFile.TabIndex = 32
        '
        'lblFile
        '
        Me.lblFile.Location = New System.Drawing.Point(12, 254)
        Me.lblFile.Name = "lblFile"
        Me.lblFile.Size = New System.Drawing.Size(33, 13)
        Me.lblFile.TabIndex = 31
        Me.lblFile.Text = "File"
        '
        'lblSalaryChangeReason
        '
        Me.lblSalaryChangeReason.Location = New System.Drawing.Point(12, 204)
        Me.lblSalaryChangeReason.Name = "lblSalaryChangeReason"
        Me.lblSalaryChangeReason.Size = New System.Drawing.Size(138, 13)
        Me.lblSalaryChangeReason.TabIndex = 30
        Me.lblSalaryChangeReason.Text = "Salary Change Reason"
        '
        'cboSalaryChangeReason
        '
        Me.cboSalaryChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryChangeReason.FormattingEnabled = True
        Me.cboSalaryChangeReason.Location = New System.Drawing.Point(156, 201)
        Me.cboSalaryChangeReason.Name = "cboSalaryChangeReason"
        Me.cboSalaryChangeReason.Size = New System.Drawing.Size(218, 21)
        Me.cboSalaryChangeReason.TabIndex = 7
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Location = New System.Drawing.Point(12, 177)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(138, 13)
        Me.lblGradeLevel.TabIndex = 26
        Me.lblGradeLevel.Text = "Grade Level"
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(156, 174)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(218, 21)
        Me.cboGradeLevel.TabIndex = 6
        '
        'lblGrade
        '
        Me.lblGrade.Location = New System.Drawing.Point(12, 150)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(138, 13)
        Me.lblGrade.TabIndex = 24
        Me.lblGrade.Text = "Grade"
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(156, 147)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(218, 21)
        Me.cboGrade.TabIndex = 5
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Location = New System.Drawing.Point(12, 123)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(138, 13)
        Me.lblGradeGroup.TabIndex = 22
        Me.lblGradeGroup.Text = "Grade Group"
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(156, 120)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(218, 21)
        Me.cboGradeGroup.TabIndex = 4
        '
        'lblUSDSalary
        '
        Me.lblUSDSalary.Location = New System.Drawing.Point(12, 96)
        Me.lblUSDSalary.Name = "lblUSDSalary"
        Me.lblUSDSalary.Size = New System.Drawing.Size(138, 13)
        Me.lblUSDSalary.TabIndex = 20
        Me.lblUSDSalary.Text = "USD Salary Head"
        '
        'cboUSDSalary
        '
        Me.cboUSDSalary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUSDSalary.FormattingEnabled = True
        Me.cboUSDSalary.Location = New System.Drawing.Point(156, 93)
        Me.cboUSDSalary.Name = "cboUSDSalary"
        Me.cboUSDSalary.Size = New System.Drawing.Size(218, 21)
        Me.cboUSDSalary.TabIndex = 3
        '
        'lblTZSSalary
        '
        Me.lblTZSSalary.Location = New System.Drawing.Point(12, 69)
        Me.lblTZSSalary.Name = "lblTZSSalary"
        Me.lblTZSSalary.Size = New System.Drawing.Size(138, 13)
        Me.lblTZSSalary.TabIndex = 18
        Me.lblTZSSalary.Text = "TZS Salary Head"
        '
        'cboTZSSalary
        '
        Me.cboTZSSalary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTZSSalary.FormattingEnabled = True
        Me.cboTZSSalary.Location = New System.Drawing.Point(156, 66)
        Me.cboTZSSalary.Name = "cboTZSSalary"
        Me.cboTZSSalary.Size = New System.Drawing.Size(218, 21)
        Me.cboTZSSalary.TabIndex = 2
        '
        'lblIdentityType
        '
        Me.lblIdentityType.Location = New System.Drawing.Point(12, 42)
        Me.lblIdentityType.Name = "lblIdentityType"
        Me.lblIdentityType.Size = New System.Drawing.Size(138, 13)
        Me.lblIdentityType.TabIndex = 16
        Me.lblIdentityType.Text = "Identity Type"
        '
        'cboIdentityType
        '
        Me.cboIdentityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentityType.FormattingEnabled = True
        Me.cboIdentityType.Location = New System.Drawing.Point(156, 39)
        Me.cboIdentityType.Name = "cboIdentityType"
        Me.cboIdentityType.Size = New System.Drawing.Size(218, 21)
        Me.cboIdentityType.TabIndex = 1
        '
        'lblShift
        '
        Me.lblShift.Location = New System.Drawing.Point(12, 15)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(138, 13)
        Me.lblShift.TabIndex = 14
        Me.lblShift.Text = "Shift"
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(156, 12)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(218, 21)
        Me.cboShift.TabIndex = 0
        '
        'objlblProgress
        '
        Me.objlblProgress.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblProgress.Location = New System.Drawing.Point(32, 22)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(136, 13)
        Me.objlblProgress.TabIndex = 8
        Me.objlblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCaption
        '
        Me.lblCaption.Location = New System.Drawing.Point(12, 71)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(97, 15)
        Me.lblCaption.TabIndex = 7
        Me.lblCaption.Text = "Select Company"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objBgWorker
        '
        Me.objBgWorker.WorkerReportsProgress = True
        Me.objBgWorker.WorkerSupportsCancellation = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(277, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnOK)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Controls.Add(Me.objlblProgress)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 285)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(386, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(174, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(97, 30)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "&Ok"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'chkAssignDefaulTranHeads
        '
        Me.chkAssignDefaulTranHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssignDefaulTranHeads.Location = New System.Drawing.Point(156, 228)
        Me.chkAssignDefaulTranHeads.Name = "chkAssignDefaulTranHeads"
        Me.chkAssignDefaulTranHeads.Size = New System.Drawing.Size(218, 17)
        Me.chkAssignDefaulTranHeads.TabIndex = 325
        Me.chkAssignDefaulTranHeads.Text = "Assign Default Transaction Heads"
        Me.chkAssignDefaulTranHeads.UseVisualStyleBackColor = True
        '
        'frmPeoplesoftMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 340)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.lblCaption)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPeoplesoftMapping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aruti Peoplesoft Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents objBgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdentityType As System.Windows.Forms.Label
    Friend WithEvents cboIdentityType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTZSSalary As System.Windows.Forms.Label
    Friend WithEvents cboTZSSalary As System.Windows.Forms.ComboBox
    Friend WithEvents lblUSDSalary As System.Windows.Forms.Label
    Friend WithEvents cboUSDSalary As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblSalaryChangeReason As System.Windows.Forms.Label
    Friend WithEvents cboSalaryChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
    Friend WithEvents lblFile As System.Windows.Forms.Label
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents chkAssignDefaulTranHeads As System.Windows.Forms.CheckBox

End Class

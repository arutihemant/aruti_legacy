﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmShiftPolicyGlobalAssign
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmShiftPolicyGlobalAssign))
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvEmp = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblOperation = New System.Windows.Forms.Label
        Me.cboOperation = New System.Windows.Forms.ComboBox
        Me.chkFirstPolicy = New System.Windows.Forms.CheckBox
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.gbShiftAssignment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkOverwriteIfExist = New System.Windows.Forms.CheckBox
        Me.lvAssignedShift = New eZee.Common.eZeeListView(Me.components)
        Me.colhEffectiveDate = New System.Windows.Forms.ColumnHeader
        Me.colhShiftType = New System.Windows.Forms.ColumnHeader
        Me.colhShiftName = New System.Windows.Forms.ColumnHeader
        Me.lblShift = New System.Windows.Forms.Label
        Me.btnDeleteShift = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnAddShift = New eZee.Common.eZeeGradientButton
        Me.btnAssignShift = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.gbPolicy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddPolicy = New eZee.Common.eZeeGradientButton
        Me.lblPolicy = New System.Windows.Forms.Label
        Me.objbtnSearchPolicy = New eZee.Common.eZeeGradientButton
        Me.cboPolicy = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblValue = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objAlloacationReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchFilterShift = New eZee.Common.eZeeGradientButton
        Me.LblFilterShift = New System.Windows.Forms.Label
        Me.cboFilterShift = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFilterShiftType = New eZee.Common.eZeeGradientButton
        Me.LblFilterShiftType = New System.Windows.Forms.Label
        Me.cboFilterShiftType = New System.Windows.Forms.ComboBox
        Me.LblFilterPolicy = New System.Windows.Forms.Label
        Me.cboFilterPolicy = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFilterPolicy = New eZee.Common.eZeeGradientButton
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.gbFilter.SuspendLayout()
        Me.gbShiftAssignment.SuspendLayout()
        Me.gbPolicy.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbEmployeeInfo.SuspendLayout()
        CType(Me.objAlloacationReset, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearch, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel1, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(2, 82)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(389, 275)
        Me.tblpAssessorEmployee.TabIndex = 108
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(383, 21)
        Me.txtSearch.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkEmployee)
        Me.Panel1.Controls.Add(Me.dgvEmp)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(383, 243)
        Me.Panel1.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvEmp
        '
        Me.dgvEmp.AllowUserToAddRows = False
        Me.dgvEmp.AllowUserToDeleteRows = False
        Me.dgvEmp.AllowUserToResizeColumns = False
        Me.dgvEmp.AllowUserToResizeRows = False
        Me.dgvEmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvEmp.ColumnHeadersHeight = 21
        Me.dgvEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.dgcolhShift, Me.objdgcolhEmpId})
        Me.dgvEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmp.MultiSelect = False
        Me.dgvEmp.Name = "dgvEmp"
        Me.dgvEmp.RowHeadersVisible = False
        Me.dgvEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmp.Size = New System.Drawing.Size(383, 243)
        Me.dgvEmp.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEName.Width = 190
        '
        'dgcolhShift
        '
        Me.dgcolhShift.HeaderText = "Shift"
        Me.dgcolhShift.Name = "dgcolhShift"
        Me.dgcolhShift.ReadOnly = True
        Me.dgcolhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShift.Width = 150
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.gbFilter)
        Me.Panel2.Controls.Add(Me.gbShiftAssignment)
        Me.Panel2.Controls.Add(Me.gbPolicy)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(842, 512)
        Me.Panel2.TabIndex = 109
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.lblOperation)
        Me.gbFilter.Controls.Add(Me.cboOperation)
        Me.gbFilter.Controls.Add(Me.chkFirstPolicy)
        Me.gbFilter.Controls.Add(Me.dtpEffectiveDate)
        Me.gbFilter.Controls.Add(Me.lblEffectiveDate)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(2, 2)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(394, 88)
        Me.gbFilter.TabIndex = 112
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Employee"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOperation
        '
        Me.lblOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperation.Location = New System.Drawing.Point(8, 36)
        Me.lblOperation.Name = "lblOperation"
        Me.lblOperation.Size = New System.Drawing.Size(60, 15)
        Me.lblOperation.TabIndex = 182
        Me.lblOperation.Text = "Operation"
        Me.lblOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOperation
        '
        Me.cboOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperation.DropDownWidth = 350
        Me.cboOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperation.FormattingEnabled = True
        Me.cboOperation.Location = New System.Drawing.Point(74, 33)
        Me.cboOperation.Name = "cboOperation"
        Me.cboOperation.Size = New System.Drawing.Size(313, 21)
        Me.cboOperation.TabIndex = 181
        '
        'chkFirstPolicy
        '
        Me.chkFirstPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFirstPolicy.Location = New System.Drawing.Point(11, 61)
        Me.chkFirstPolicy.Name = "chkFirstPolicy"
        Me.chkFirstPolicy.Size = New System.Drawing.Size(115, 17)
        Me.chkFirstPolicy.TabIndex = 112
        Me.chkFirstPolicy.Text = "Assign First Policy"
        Me.chkFirstPolicy.UseVisualStyleBackColor = True
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(290, 60)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpEffectiveDate.TabIndex = 176
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(216, 62)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(68, 15)
        Me.lblEffectiveDate.TabIndex = 172
        Me.lblEffectiveDate.Text = "Effect. Date"
        Me.lblEffectiveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbShiftAssignment
        '
        Me.gbShiftAssignment.BorderColor = System.Drawing.Color.Black
        Me.gbShiftAssignment.Checked = False
        Me.gbShiftAssignment.CollapseAllExceptThis = False
        Me.gbShiftAssignment.CollapsedHoverImage = Nothing
        Me.gbShiftAssignment.CollapsedNormalImage = Nothing
        Me.gbShiftAssignment.CollapsedPressedImage = Nothing
        Me.gbShiftAssignment.CollapseOnLoad = False
        Me.gbShiftAssignment.Controls.Add(Me.chkOverwriteIfExist)
        Me.gbShiftAssignment.Controls.Add(Me.lvAssignedShift)
        Me.gbShiftAssignment.Controls.Add(Me.lblShift)
        Me.gbShiftAssignment.Controls.Add(Me.btnDeleteShift)
        Me.gbShiftAssignment.Controls.Add(Me.objbtnAddShift)
        Me.gbShiftAssignment.Controls.Add(Me.btnAssignShift)
        Me.gbShiftAssignment.Controls.Add(Me.cboShift)
        Me.gbShiftAssignment.Controls.Add(Me.objbtnSearchShift)
        Me.gbShiftAssignment.ExpandedHoverImage = Nothing
        Me.gbShiftAssignment.ExpandedNormalImage = Nothing
        Me.gbShiftAssignment.ExpandedPressedImage = Nothing
        Me.gbShiftAssignment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftAssignment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftAssignment.HeaderHeight = 25
        Me.gbShiftAssignment.HeaderMessage = ""
        Me.gbShiftAssignment.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbShiftAssignment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShiftAssignment.HeightOnCollapse = 0
        Me.gbShiftAssignment.LeftTextSpace = 0
        Me.gbShiftAssignment.Location = New System.Drawing.Point(402, 3)
        Me.gbShiftAssignment.Name = "gbShiftAssignment"
        Me.gbShiftAssignment.OpenHeight = 300
        Me.gbShiftAssignment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftAssignment.ShowBorder = True
        Me.gbShiftAssignment.ShowCheckBox = False
        Me.gbShiftAssignment.ShowCollapseButton = False
        Me.gbShiftAssignment.ShowDefaultBorderColor = True
        Me.gbShiftAssignment.ShowDownButton = False
        Me.gbShiftAssignment.ShowHeader = True
        Me.gbShiftAssignment.Size = New System.Drawing.Size(434, 382)
        Me.gbShiftAssignment.TabIndex = 1
        Me.gbShiftAssignment.Temp = 0
        Me.gbShiftAssignment.Text = "Shift Assignment"
        Me.gbShiftAssignment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOverwriteIfExist
        '
        Me.chkOverwriteIfExist.BackColor = System.Drawing.Color.Transparent
        Me.chkOverwriteIfExist.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwriteIfExist.Location = New System.Drawing.Point(295, 5)
        Me.chkOverwriteIfExist.Name = "chkOverwriteIfExist"
        Me.chkOverwriteIfExist.Size = New System.Drawing.Size(134, 17)
        Me.chkOverwriteIfExist.TabIndex = 184
        Me.chkOverwriteIfExist.Text = "Overwrite If Exist"
        Me.chkOverwriteIfExist.UseVisualStyleBackColor = False
        '
        'lvAssignedShift
        '
        Me.lvAssignedShift.BackColorOnChecked = False
        Me.lvAssignedShift.ColumnHeaders = Nothing
        Me.lvAssignedShift.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEffectiveDate, Me.colhShiftType, Me.colhShiftName})
        Me.lvAssignedShift.CompulsoryColumns = ""
        Me.lvAssignedShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAssignedShift.FullRowSelect = True
        Me.lvAssignedShift.GridLines = True
        Me.lvAssignedShift.GroupingColumn = Nothing
        Me.lvAssignedShift.HideSelection = False
        Me.lvAssignedShift.Location = New System.Drawing.Point(6, 60)
        Me.lvAssignedShift.MinColumnWidth = 50
        Me.lvAssignedShift.MultiSelect = False
        Me.lvAssignedShift.Name = "lvAssignedShift"
        Me.lvAssignedShift.OptionalColumns = ""
        Me.lvAssignedShift.ShowMoreItem = False
        Me.lvAssignedShift.ShowSaveItem = False
        Me.lvAssignedShift.ShowSelectAll = True
        Me.lvAssignedShift.ShowSizeAllColumnsToFit = True
        Me.lvAssignedShift.Size = New System.Drawing.Size(422, 316)
        Me.lvAssignedShift.Sortable = True
        Me.lvAssignedShift.TabIndex = 180
        Me.lvAssignedShift.UseCompatibleStateImageBehavior = False
        Me.lvAssignedShift.View = System.Windows.Forms.View.Details
        '
        'colhEffectiveDate
        '
        Me.colhEffectiveDate.Tag = "colhEffectiveDate"
        Me.colhEffectiveDate.Text = "Effective Date"
        Me.colhEffectiveDate.Width = 95
        '
        'colhShiftType
        '
        Me.colhShiftType.Tag = "colhShiftType"
        Me.colhShiftType.Text = "Shift Type"
        Me.colhShiftType.Width = 120
        '
        'colhShiftName
        '
        Me.colhShiftName.Tag = "colhShiftName"
        Me.colhShiftName.Text = "Shift Name"
        Me.colhShiftName.Width = 200
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(9, 35)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(42, 15)
        Me.lblShift.TabIndex = 175
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnDeleteShift
        '
        Me.btnDeleteShift.BackColor = System.Drawing.Color.White
        Me.btnDeleteShift.BackgroundImage = CType(resources.GetObject("btnDeleteShift.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteShift.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteShift.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteShift.FlatAppearance.BorderSize = 0
        Me.btnDeleteShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteShift.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteShift.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteShift.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteShift.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteShift.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteShift.Location = New System.Drawing.Point(348, 27)
        Me.btnDeleteShift.Name = "btnDeleteShift"
        Me.btnDeleteShift.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteShift.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteShift.Size = New System.Drawing.Size(80, 30)
        Me.btnDeleteShift.TabIndex = 179
        Me.btnDeleteShift.Text = "&Delete"
        Me.btnDeleteShift.UseVisualStyleBackColor = True
        '
        'objbtnAddShift
        '
        Me.objbtnAddShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddShift.BorderSelected = False
        Me.objbtnAddShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddShift.Location = New System.Drawing.Point(237, 32)
        Me.objbtnAddShift.Name = "objbtnAddShift"
        Me.objbtnAddShift.Size = New System.Drawing.Size(20, 21)
        Me.objbtnAddShift.TabIndex = 177
        '
        'btnAssignShift
        '
        Me.btnAssignShift.BackColor = System.Drawing.Color.White
        Me.btnAssignShift.BackgroundImage = CType(resources.GetObject("btnAssignShift.BackgroundImage"), System.Drawing.Image)
        Me.btnAssignShift.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssignShift.BorderColor = System.Drawing.Color.Empty
        Me.btnAssignShift.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssignShift.FlatAppearance.BorderSize = 0
        Me.btnAssignShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssignShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssignShift.ForeColor = System.Drawing.Color.Black
        Me.btnAssignShift.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssignShift.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssignShift.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssignShift.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssignShift.Location = New System.Drawing.Point(263, 27)
        Me.btnAssignShift.Name = "btnAssignShift"
        Me.btnAssignShift.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssignShift.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssignShift.Size = New System.Drawing.Size(80, 30)
        Me.btnAssignShift.TabIndex = 178
        Me.btnAssignShift.Text = "&Add"
        Me.btnAssignShift.UseVisualStyleBackColor = True
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 350
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(57, 32)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(148, 21)
        Me.cboShift.TabIndex = 173
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(210, 32)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(20, 21)
        Me.objbtnSearchShift.TabIndex = 174
        '
        'gbPolicy
        '
        Me.gbPolicy.BorderColor = System.Drawing.Color.Black
        Me.gbPolicy.Checked = False
        Me.gbPolicy.CollapseAllExceptThis = False
        Me.gbPolicy.CollapsedHoverImage = Nothing
        Me.gbPolicy.CollapsedNormalImage = Nothing
        Me.gbPolicy.CollapsedPressedImage = Nothing
        Me.gbPolicy.CollapseOnLoad = False
        Me.gbPolicy.Controls.Add(Me.objbtnAddPolicy)
        Me.gbPolicy.Controls.Add(Me.lblPolicy)
        Me.gbPolicy.Controls.Add(Me.objbtnSearchPolicy)
        Me.gbPolicy.Controls.Add(Me.cboPolicy)
        Me.gbPolicy.ExpandedHoverImage = Nothing
        Me.gbPolicy.ExpandedNormalImage = Nothing
        Me.gbPolicy.ExpandedPressedImage = Nothing
        Me.gbPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPolicy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPolicy.HeaderHeight = 25
        Me.gbPolicy.HeaderMessage = ""
        Me.gbPolicy.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPolicy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPolicy.HeightOnCollapse = 0
        Me.gbPolicy.LeftTextSpace = 0
        Me.gbPolicy.Location = New System.Drawing.Point(402, 388)
        Me.gbPolicy.Name = "gbPolicy"
        Me.gbPolicy.OpenHeight = 300
        Me.gbPolicy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPolicy.ShowBorder = True
        Me.gbPolicy.ShowCheckBox = False
        Me.gbPolicy.ShowCollapseButton = False
        Me.gbPolicy.ShowDefaultBorderColor = True
        Me.gbPolicy.ShowDownButton = False
        Me.gbPolicy.ShowHeader = True
        Me.gbPolicy.Size = New System.Drawing.Size(434, 65)
        Me.gbPolicy.TabIndex = 0
        Me.gbPolicy.Temp = 0
        Me.gbPolicy.Text = "Policy Assignment"
        Me.gbPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddPolicy
        '
        Me.objbtnAddPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddPolicy.BorderSelected = False
        Me.objbtnAddPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddPolicy.Location = New System.Drawing.Point(237, 33)
        Me.objbtnAddPolicy.Name = "objbtnAddPolicy"
        Me.objbtnAddPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddPolicy.TabIndex = 171
        '
        'lblPolicy
        '
        Me.lblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPolicy.Location = New System.Drawing.Point(10, 35)
        Me.lblPolicy.Name = "lblPolicy"
        Me.lblPolicy.Size = New System.Drawing.Size(43, 15)
        Me.lblPolicy.TabIndex = 169
        Me.lblPolicy.Text = "Policy"
        Me.lblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPolicy
        '
        Me.objbtnSearchPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPolicy.BorderSelected = False
        Me.objbtnSearchPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPolicy.Location = New System.Drawing.Point(210, 33)
        Me.objbtnSearchPolicy.Name = "objbtnSearchPolicy"
        Me.objbtnSearchPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPolicy.TabIndex = 168
        '
        'cboPolicy
        '
        Me.cboPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPolicy.DropDownWidth = 350
        Me.cboPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPolicy.FormattingEnabled = True
        Me.cboPolicy.Location = New System.Drawing.Point(58, 33)
        Me.cboPolicy.Name = "cboPolicy"
        Me.cboPolicy.Size = New System.Drawing.Size(148, 21)
        Me.cboPolicy.TabIndex = 167
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblValue)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 457)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(842, 55)
        Me.objFooter.TabIndex = 110
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlblValue.Location = New System.Drawing.Point(57, 20)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(494, 17)
        Me.objlblValue.TabIndex = 2
        Me.objlblValue.TabStop = True
        Me.objlblValue.Text = "########"
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(630, 7)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(733, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.objAlloacationReset)
        Me.gbEmployeeInfo.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchFilterShift)
        Me.gbEmployeeInfo.Controls.Add(Me.LblFilterShift)
        Me.gbEmployeeInfo.Controls.Add(Me.cboFilterShift)
        Me.gbEmployeeInfo.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchFilterShiftType)
        Me.gbEmployeeInfo.Controls.Add(Me.LblFilterShiftType)
        Me.gbEmployeeInfo.Controls.Add(Me.cboFilterShiftType)
        Me.gbEmployeeInfo.Controls.Add(Me.LblFilterPolicy)
        Me.gbEmployeeInfo.Controls.Add(Me.cboFilterPolicy)
        Me.gbEmployeeInfo.Controls.Add(Me.objbtnSearchFilterPolicy)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(2, 94)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(396, 360)
        Me.gbEmployeeInfo.TabIndex = 111
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Employee"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objAlloacationReset.Image = CType(resources.GetObject("objAlloacationReset.Image"), System.Drawing.Image)
        Me.objAlloacationReset.Location = New System.Drawing.Point(362, 0)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.ResultMessage = ""
        Me.objAlloacationReset.SearchMessage = ""
        Me.objAlloacationReset.Size = New System.Drawing.Size(24, 24)
        Me.objAlloacationReset.TabIndex = 314
        Me.objAlloacationReset.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(266, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(84, 17)
        Me.lnkAllocation.TabIndex = 308
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchFilterShift
        '
        Me.objbtnSearchFilterShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFilterShift.BorderSelected = False
        Me.objbtnSearchFilterShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFilterShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFilterShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFilterShift.Location = New System.Drawing.Point(369, 57)
        Me.objbtnSearchFilterShift.Name = "objbtnSearchFilterShift"
        Me.objbtnSearchFilterShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFilterShift.TabIndex = 182
        '
        'LblFilterShift
        '
        Me.LblFilterShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFilterShift.Location = New System.Drawing.Point(7, 60)
        Me.LblFilterShift.Name = "LblFilterShift"
        Me.LblFilterShift.Size = New System.Drawing.Size(68, 15)
        Me.LblFilterShift.TabIndex = 184
        Me.LblFilterShift.Text = "Shift"
        Me.LblFilterShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterShift
        '
        Me.cboFilterShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterShift.DropDownWidth = 350
        Me.cboFilterShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterShift.FormattingEnabled = True
        Me.cboFilterShift.Location = New System.Drawing.Point(81, 57)
        Me.cboFilterShift.Name = "cboFilterShift"
        Me.cboFilterShift.Size = New System.Drawing.Size(281, 21)
        Me.cboFilterShift.TabIndex = 182
        '
        'objbtnSearchFilterShiftType
        '
        Me.objbtnSearchFilterShiftType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShiftType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShiftType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterShiftType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFilterShiftType.BorderSelected = False
        Me.objbtnSearchFilterShiftType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFilterShiftType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFilterShiftType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFilterShiftType.Location = New System.Drawing.Point(369, 31)
        Me.objbtnSearchFilterShiftType.Name = "objbtnSearchFilterShiftType"
        Me.objbtnSearchFilterShiftType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFilterShiftType.TabIndex = 311
        '
        'LblFilterShiftType
        '
        Me.LblFilterShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFilterShiftType.Location = New System.Drawing.Point(7, 34)
        Me.LblFilterShiftType.Name = "LblFilterShiftType"
        Me.LblFilterShiftType.Size = New System.Drawing.Size(68, 15)
        Me.LblFilterShiftType.TabIndex = 312
        Me.LblFilterShiftType.Text = "Shift Type"
        Me.LblFilterShiftType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterShiftType
        '
        Me.cboFilterShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterShiftType.DropDownWidth = 350
        Me.cboFilterShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterShiftType.FormattingEnabled = True
        Me.cboFilterShiftType.Location = New System.Drawing.Point(81, 31)
        Me.cboFilterShiftType.Name = "cboFilterShiftType"
        Me.cboFilterShiftType.Size = New System.Drawing.Size(281, 21)
        Me.cboFilterShiftType.TabIndex = 310
        '
        'LblFilterPolicy
        '
        Me.LblFilterPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFilterPolicy.Location = New System.Drawing.Point(7, 34)
        Me.LblFilterPolicy.Name = "LblFilterPolicy"
        Me.LblFilterPolicy.Size = New System.Drawing.Size(68, 15)
        Me.LblFilterPolicy.TabIndex = 318
        Me.LblFilterPolicy.Text = "Policy"
        Me.LblFilterPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterPolicy
        '
        Me.cboFilterPolicy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterPolicy.DropDownWidth = 350
        Me.cboFilterPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterPolicy.FormattingEnabled = True
        Me.cboFilterPolicy.Location = New System.Drawing.Point(81, 31)
        Me.cboFilterPolicy.Name = "cboFilterPolicy"
        Me.cboFilterPolicy.Size = New System.Drawing.Size(272, 21)
        Me.cboFilterPolicy.TabIndex = 316
        '
        'objbtnSearchFilterPolicy
        '
        Me.objbtnSearchFilterPolicy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterPolicy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterPolicy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFilterPolicy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFilterPolicy.BorderSelected = False
        Me.objbtnSearchFilterPolicy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFilterPolicy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFilterPolicy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFilterPolicy.Location = New System.Drawing.Point(368, 31)
        Me.objbtnSearchFilterPolicy.Name = "objbtnSearchFilterPolicy"
        Me.objbtnSearchFilterPolicy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFilterPolicy.TabIndex = 317
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'frmShiftPolicyGlobalAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(842, 512)
        Me.Controls.Add(Me.gbEmployeeInfo)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmShiftPolicyGlobalAssign"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Shift/Policy Assignment"
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.gbFilter.ResumeLayout(False)
        Me.gbShiftAssignment.ResumeLayout(False)
        Me.gbPolicy.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbEmployeeInfo.ResumeLayout(False)
        CType(Me.objAlloacationReset, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvEmp As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents gbPolicy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbShiftAssignment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPolicy As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddShift As eZee.Common.eZeeGradientButton
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents btnDeleteShift As eZee.Common.eZeeLightButton
    Friend WithEvents btnAssignShift As eZee.Common.eZeeLightButton
    Friend WithEvents lvAssignedShift As eZee.Common.eZeeListView
    Friend WithEvents colhShiftType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhShiftName As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblOperation As System.Windows.Forms.Label
    Friend WithEvents cboOperation As System.Windows.Forms.ComboBox
    Friend WithEvents chkFirstPolicy As System.Windows.Forms.CheckBox
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents colhEffectiveDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objlblValue As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchFilterShift As eZee.Common.eZeeGradientButton
    Friend WithEvents LblFilterShift As System.Windows.Forms.Label
    Friend WithEvents cboFilterShift As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchFilterShiftType As eZee.Common.eZeeGradientButton
    Friend WithEvents LblFilterShiftType As System.Windows.Forms.Label
    Friend WithEvents cboFilterShiftType As System.Windows.Forms.ComboBox
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchFilterPolicy As eZee.Common.eZeeGradientButton
    Friend WithEvents LblFilterPolicy As System.Windows.Forms.Label
    Friend WithEvents cboFilterPolicy As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkOverwriteIfExist As System.Windows.Forms.CheckBox
End Class

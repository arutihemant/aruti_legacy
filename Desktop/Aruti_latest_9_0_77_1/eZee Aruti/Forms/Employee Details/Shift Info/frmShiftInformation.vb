﻿Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmShiftInformation

    Private ReadOnly mstrModuleName As String = "frmShiftInformation"
    Private mintShiftUnkid As Integer = -1
    Private objShift As New clsshift_master
    Private mblnCancel As Boolean = True

    Public Function displayDialog(ByRef intUnkId As Integer) As Boolean
        Try
            mintShiftUnkid = intUnkId

            Me.ShowDialog()

            intUnkId = mintShiftUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

    Public WriteOnly Property _ShiftUnkid()
        Set(ByVal value)
            mintShiftUnkid = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmShiftInformation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objShift = New clsshift_master
        Try
            Call Set_Logo(Me, gApplicationType)

            objShift._Shiftunkid = mintShiftUnkid
            txtShift.Text = objShift._Shiftname
            txtBreakHours.Text = objShift._Breaktime.ToString()
            Dim calcMinute = objShift._Workinghours / 60
            Dim calMinute As Double = calcMinute Mod 60
            Dim calHour As Double = calcMinute / 60
            txtShiftHours.Text = CStr(CDec(Int(calHour) + (calMinute / 100)))
            Call GetWorkingdays()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShiftInformation_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

    Private Sub GetWorkingdays()
        Try
            Dim ar As Array = objShift._Shiftdays.Split(CChar("|"))
            If ar.Length > 0 Then
                For i As Integer = 0 To lvWorkingDays.Items.Count - 1
                    For j As Integer = 0 To ar.Length - 1
                        If lvWorkingDays.Items(i).Text.Trim() = ar.GetValue(j).ToString() Then
                            lvWorkingDays.Items(i).Checked = True
                        Else
                            Continue For
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetWorkingdays", mstrModuleName)
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbShiftDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbShiftDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbShiftDetails.Text = Language._Object.getCaption(Me.gbShiftDetails.Name, Me.gbShiftDetails.Text)
			Me.eZeeBreakInfo.Text = Language._Object.getCaption(Me.eZeeBreakInfo.Name, Me.eZeeBreakInfo.Text)
			Me.eZeeShiftInfo.Text = Language._Object.getCaption(Me.eZeeShiftInfo.Name, Me.eZeeShiftInfo.Text)
			Me.eZeeWorkDays.Text = Language._Object.getCaption(Me.eZeeWorkDays.Name, Me.eZeeWorkDays.Text)
			Me.ColumnHeader1.Text = Language._Object.getCaption(CStr(Me.ColumnHeader1.Tag), Me.ColumnHeader1.Text)
			Me.lblBreakHours.Text = Language._Object.getCaption(Me.lblBreakHours.Name, Me.lblBreakHours.Text)
			Me.lblShiftHours.Text = Language._Object.getCaption(Me.lblShiftHours.Name, Me.lblShiftHours.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
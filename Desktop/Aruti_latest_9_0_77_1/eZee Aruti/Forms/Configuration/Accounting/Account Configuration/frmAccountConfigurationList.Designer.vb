﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAccountConfigurationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAccountConfigurationList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSetinactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbTranAccountList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlTranAccountList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvTrnHeadAccount = New eZee.Common.eZeeListView(Me.components)
        Me.colhID = New System.Windows.Forms.ColumnHeader
        Me.colhTransactionType = New System.Windows.Forms.ColumnHeader
        Me.colhTrnHead = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhAccountCode = New System.Windows.Forms.ColumnHeader
        Me.colhAccountName = New System.Windows.Forms.ColumnHeader
        Me.colhAccGroup = New System.Windows.Forms.ColumnHeader
        Me.colhShortname = New System.Windows.Forms.ColumnHeader
        Me.colhShortname2 = New System.Windows.Forms.ColumnHeader
        Me.colhShortname3 = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeGradientButton2 = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTransactionType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchAccount = New eZee.Common.eZeeGradientButton
        Me.cboTransactionType = New System.Windows.Forms.ComboBox
        Me.lblTransactionType = New System.Windows.Forms.Label
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.cboAccountName = New System.Windows.Forms.ComboBox
        Me.lblAccuntName = New System.Windows.Forms.Label
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.cboAccountGroup = New System.Windows.Forms.ComboBox
        Me.lblAccountGroup = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.lblactiveinactive = New System.Windows.Forms.Label
        Me.colhactiveinactive = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbTranAccountList.SuspendLayout()
        Me.pnlTranAccountList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbTranAccountList)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(894, 539)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSetinactive)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 484)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(894, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSetinactive
        '
        Me.btnSetinactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetinactive.BackColor = System.Drawing.Color.White
        Me.btnSetinactive.BackgroundImage = CType(resources.GetObject("btnSetinactive.BackgroundImage"), System.Drawing.Image)
        Me.btnSetinactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSetinactive.BorderColor = System.Drawing.Color.Empty
        Me.btnSetinactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSetinactive.FlatAppearance.BorderSize = 0
        Me.btnSetinactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetinactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetinactive.ForeColor = System.Drawing.Color.Black
        Me.btnSetinactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSetinactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnSetinactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetinactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSetinactive.Location = New System.Drawing.Point(12, 13)
        Me.btnSetinactive.Name = "btnSetinactive"
        Me.btnSetinactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetinactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSetinactive.Size = New System.Drawing.Size(97, 30)
        Me.btnSetinactive.TabIndex = 4
        Me.btnSetinactive.Text = "&Set Inactive"
        Me.btnSetinactive.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(682, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(579, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(476, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(785, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbTranAccountList
        '
        Me.gbTranAccountList.BorderColor = System.Drawing.Color.Black
        Me.gbTranAccountList.Checked = False
        Me.gbTranAccountList.CollapseAllExceptThis = False
        Me.gbTranAccountList.CollapsedHoverImage = Nothing
        Me.gbTranAccountList.CollapsedNormalImage = Nothing
        Me.gbTranAccountList.CollapsedPressedImage = Nothing
        Me.gbTranAccountList.CollapseOnLoad = False
        Me.gbTranAccountList.Controls.Add(Me.pnlTranAccountList)
        Me.gbTranAccountList.ExpandedHoverImage = Nothing
        Me.gbTranAccountList.ExpandedNormalImage = Nothing
        Me.gbTranAccountList.ExpandedPressedImage = Nothing
        Me.gbTranAccountList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTranAccountList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTranAccountList.HeaderHeight = 25
        Me.gbTranAccountList.HeaderMessage = ""
        Me.gbTranAccountList.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTranAccountList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTranAccountList.HeightOnCollapse = 0
        Me.gbTranAccountList.LeftTextSpace = 0
        Me.gbTranAccountList.Location = New System.Drawing.Point(12, 165)
        Me.gbTranAccountList.Name = "gbTranAccountList"
        Me.gbTranAccountList.OpenHeight = 300
        Me.gbTranAccountList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTranAccountList.ShowBorder = True
        Me.gbTranAccountList.ShowCheckBox = False
        Me.gbTranAccountList.ShowCollapseButton = False
        Me.gbTranAccountList.ShowDefaultBorderColor = True
        Me.gbTranAccountList.ShowDownButton = False
        Me.gbTranAccountList.ShowHeader = True
        Me.gbTranAccountList.Size = New System.Drawing.Size(870, 306)
        Me.gbTranAccountList.TabIndex = 1
        Me.gbTranAccountList.Temp = 0
        Me.gbTranAccountList.Text = "Transaction Head / Account Info"
        Me.gbTranAccountList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTranAccountList
        '
        Me.pnlTranAccountList.Controls.Add(Me.objchkSelectAll)
        Me.pnlTranAccountList.Controls.Add(Me.lvTrnHeadAccount)
        Me.pnlTranAccountList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlTranAccountList.Location = New System.Drawing.Point(3, 28)
        Me.pnlTranAccountList.Name = "pnlTranAccountList"
        Me.pnlTranAccountList.Size = New System.Drawing.Size(864, 278)
        Me.pnlTranAccountList.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 20
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvTrnHeadAccount
        '
        Me.lvTrnHeadAccount.BackColorOnChecked = False
        Me.lvTrnHeadAccount.CheckBoxes = True
        Me.lvTrnHeadAccount.ColumnHeaders = Nothing
        Me.lvTrnHeadAccount.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhID, Me.colhTransactionType, Me.colhTrnHead, Me.colhPeriod, Me.colhAccountCode, Me.colhAccountName, Me.colhAccGroup, Me.colhShortname, Me.colhShortname2, Me.colhShortname3, Me.colhactiveinactive})
        Me.lvTrnHeadAccount.CompulsoryColumns = ""
        Me.lvTrnHeadAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTrnHeadAccount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTrnHeadAccount.FullRowSelect = True
        Me.lvTrnHeadAccount.GridLines = True
        Me.lvTrnHeadAccount.GroupingColumn = Nothing
        Me.lvTrnHeadAccount.HideSelection = False
        Me.lvTrnHeadAccount.Location = New System.Drawing.Point(0, 0)
        Me.lvTrnHeadAccount.MinColumnWidth = 50
        Me.lvTrnHeadAccount.MultiSelect = False
        Me.lvTrnHeadAccount.Name = "lvTrnHeadAccount"
        Me.lvTrnHeadAccount.OptionalColumns = ""
        Me.lvTrnHeadAccount.ShowMoreItem = False
        Me.lvTrnHeadAccount.ShowSaveItem = False
        Me.lvTrnHeadAccount.ShowSelectAll = True
        Me.lvTrnHeadAccount.ShowSizeAllColumnsToFit = True
        Me.lvTrnHeadAccount.Size = New System.Drawing.Size(864, 278)
        Me.lvTrnHeadAccount.Sortable = True
        Me.lvTrnHeadAccount.TabIndex = 0
        Me.lvTrnHeadAccount.UseCompatibleStateImageBehavior = False
        Me.lvTrnHeadAccount.View = System.Windows.Forms.View.Details
        '
        'colhID
        '
        Me.colhID.Tag = ""
        Me.colhID.Text = ""
        Me.colhID.Width = 30
        '
        'colhTransactionType
        '
        Me.colhTransactionType.Tag = "colhTransactionType"
        Me.colhTransactionType.Text = "Transaction Type"
        Me.colhTransactionType.Width = 120
        '
        'colhTrnHead
        '
        Me.colhTrnHead.Tag = "colhTrnHead"
        Me.colhTrnHead.Text = "Transaction Head"
        Me.colhTrnHead.Width = 170
        '
        'colhPeriod
        '
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 100
        '
        'colhAccountCode
        '
        Me.colhAccountCode.Tag = "colhAccountCode"
        Me.colhAccountCode.Text = "Acc. Code"
        Me.colhAccountCode.Width = 70
        '
        'colhAccountName
        '
        Me.colhAccountName.Tag = "colhAccountName"
        Me.colhAccountName.Text = "Account Name"
        Me.colhAccountName.Width = 130
        '
        'colhAccGroup
        '
        Me.colhAccGroup.Tag = "colhAccGroup"
        Me.colhAccGroup.Text = "Account Group"
        Me.colhAccGroup.Width = 120
        '
        'colhShortname
        '
        Me.colhShortname.Tag = "colhShortname"
        Me.colhShortname.Text = "Short Name"
        Me.colhShortname.Width = 90
        '
        'colhShortname2
        '
        Me.colhShortname2.Tag = "colhShortname2"
        Me.colhShortname2.Text = "Short Name2"
        Me.colhShortname2.Width = 90
        '
        'colhShortname3
        '
        Me.colhShortname3.Tag = "colhShortname3"
        Me.colhShortname3.Text = "Short Name 3"
        Me.colhShortname3.Width = 90
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblactiveinactive)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeGradientButton2)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAccount)
        Me.gbFilterCriteria.Controls.Add(Me.cboTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.lblTransactionType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboAccountName)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccuntName)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboAccountGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccountGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(870, 93)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeGradientButton2
        '
        Me.EZeeGradientButton2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BackColor1 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BackColor2 = System.Drawing.Color.Transparent
        Me.EZeeGradientButton2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.EZeeGradientButton2.BorderSelected = False
        Me.EZeeGradientButton2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.EZeeGradientButton2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.EZeeGradientButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EZeeGradientButton2.Location = New System.Drawing.Point(540, 33)
        Me.EZeeGradientButton2.Name = "EZeeGradientButton2"
        Me.EZeeGradientButton2.Size = New System.Drawing.Size(21, 21)
        Me.EZeeGradientButton2.TabIndex = 114
        Me.EZeeGradientButton2.Visible = False
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(300, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(91, 15)
        Me.lblPeriod.TabIndex = 113
        Me.lblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(397, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(137, 21)
        Me.cboPeriod.TabIndex = 112
        '
        'objbtnSearchTransactionType
        '
        Me.objbtnSearchTransactionType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTransactionType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTransactionType.BorderSelected = False
        Me.objbtnSearchTransactionType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTransactionType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTransactionType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTransactionType.Location = New System.Drawing.Point(271, 33)
        Me.objbtnSearchTransactionType.Name = "objbtnSearchTransactionType"
        Me.objbtnSearchTransactionType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTransactionType.TabIndex = 99
        '
        'objbtnSearchAccount
        '
        Me.objbtnSearchAccount.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccount.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAccount.BorderSelected = False
        Me.objbtnSearchAccount.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAccount.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAccount.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAccount.Location = New System.Drawing.Point(822, 60)
        Me.objbtnSearchAccount.Name = "objbtnSearchAccount"
        Me.objbtnSearchAccount.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAccount.TabIndex = 97
        '
        'cboTransactionType
        '
        Me.cboTransactionType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboTransactionType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionType.FormattingEnabled = True
        Me.cboTransactionType.Location = New System.Drawing.Point(128, 33)
        Me.cboTransactionType.Name = "cboTransactionType"
        Me.cboTransactionType.Size = New System.Drawing.Size(137, 21)
        Me.cboTransactionType.TabIndex = 0
        '
        'lblTransactionType
        '
        Me.lblTransactionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionType.Location = New System.Drawing.Point(8, 36)
        Me.lblTransactionType.Name = "lblTransactionType"
        Me.lblTransactionType.Size = New System.Drawing.Size(114, 15)
        Me.lblTransactionType.TabIndex = 95
        Me.lblTransactionType.Text = "Transaction Type"
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(271, 60)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 93
        '
        'cboAccountName
        '
        Me.cboAccountName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountName.FormattingEnabled = True
        Me.cboAccountName.Location = New System.Drawing.Point(679, 60)
        Me.cboAccountName.Name = "cboAccountName"
        Me.cboAccountName.Size = New System.Drawing.Size(137, 21)
        Me.cboAccountName.TabIndex = 2
        '
        'lblAccuntName
        '
        Me.lblAccuntName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccuntName.Location = New System.Drawing.Point(559, 63)
        Me.lblAccuntName.Name = "lblAccuntName"
        Me.lblAccuntName.Size = New System.Drawing.Size(114, 15)
        Me.lblAccuntName.TabIndex = 90
        Me.lblAccuntName.Text = "Account Name"
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 63)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(114, 15)
        Me.lblTrnHead.TabIndex = 74
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'cboTrnHead
        '
        Me.cboTrnHead.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboTrnHead.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(128, 60)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(137, 21)
        Me.cboTrnHead.TabIndex = 1
        '
        'cboAccountGroup
        '
        Me.cboAccountGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountGroup.FormattingEnabled = True
        Me.cboAccountGroup.Location = New System.Drawing.Point(679, 33)
        Me.cboAccountGroup.Name = "cboAccountGroup"
        Me.cboAccountGroup.Size = New System.Drawing.Size(137, 21)
        Me.cboAccountGroup.TabIndex = 3
        '
        'lblAccountGroup
        '
        Me.lblAccountGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountGroup.Location = New System.Drawing.Point(559, 36)
        Me.lblAccountGroup.Name = "lblAccountGroup"
        Me.lblAccountGroup.Size = New System.Drawing.Size(114, 15)
        Me.lblAccountGroup.TabIndex = 16
        Me.lblAccountGroup.Text = "Account Group"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(843, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(819, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(894, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Account Configuration List"
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(397, 63)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(137, 21)
        Me.cboActiveInactive.TabIndex = 116
        '
        'lblactiveinactive
        '
        Me.lblactiveinactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblactiveinactive.Location = New System.Drawing.Point(300, 66)
        Me.lblactiveinactive.Name = "lblactiveinactive"
        Me.lblactiveinactive.Size = New System.Drawing.Size(91, 15)
        Me.lblactiveinactive.TabIndex = 117
        Me.lblactiveinactive.Text = "Inactive Status"
        '
        'colhactiveinactive
        '
        Me.colhactiveinactive.Text = "Inactive Status"
        '
        'frmAccountConfigurationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 539)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAccountConfigurationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Account Configuration List"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbTranAccountList.ResumeLayout(False)
        Me.pnlTranAccountList.ResumeLayout(False)
        Me.pnlTranAccountList.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbTranAccountList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlTranAccountList As System.Windows.Forms.Panel
    Friend WithEvents lvTrnHeadAccount As eZee.Common.eZeeListView
    Friend WithEvents colhID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTrnHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccountName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAccGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboAccountName As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccuntName As System.Windows.Forms.Label
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents cboAccountGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents colhTransactionType As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboTransactionType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAccount As eZee.Common.eZeeGradientButton
    Friend WithEvents colhAccountCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhShortname As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchTransactionType As eZee.Common.eZeeGradientButton
    Friend WithEvents colhShortname2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhShortname3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeGradientButton2 As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnSetinactive As eZee.Common.eZeeLightButton
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents lblactiveinactive As System.Windows.Forms.Label
    Friend WithEvents colhactiveinactive As System.Windows.Forms.ColumnHeader
End Class

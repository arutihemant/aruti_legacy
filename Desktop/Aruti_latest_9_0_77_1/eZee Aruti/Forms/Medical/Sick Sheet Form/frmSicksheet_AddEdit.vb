﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSicksheet_AddEdit

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmSicksheet_AddEdit"
    Private mblnCancel As Boolean = True
    Private objsicksheet As clsmedical_sicksheet
    Private menAction As enAction = enAction.ADD_ONE
    Private mintSicksheetunkid As Integer = -1
    Private objJob As clsJobs
    Private dsEmpList As DataSet = Nothing

    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private mstrDependantIds As String = ""
    'Pinkal (12-Sep-2014) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSicksheetunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintSicksheetunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub setColor()
        Try
            txtSheetNo.BackColor = GUI.ColorComp
            txtEmployeeCode.BackColor = GUI.ColorComp
            txtEmployeeName.BackColor = GUI.ColorComp
            cboProvider.BackColor = GUI.ColorComp
            txtjob.BackColor = GUI.ColorComp
            cboMedicalCover.BackColor = GUI.ColorComp

            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If ConfigParameter._Object._SickSheetAllocationId > 0 Then
                cboSickSheetAllocation.BackColor = GUI.ColorComp
            End If
            'Pinkal (12-Sep-2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try


            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'Dim objprovider As New clsinstitute_master
            'dsFill = objprovider.getListForCombo(True, "Provider", True, 1, True)

            Dim objUserMapping As New clsprovider_mapping
            dsFill = objUserMapping.GetUserProvider("Provider", , User._Object._Userunkid, True)

            Dim drRow As DataRow = dsFill.Tables(0).NewRow
            drRow("providerunkid") = 0
            drRow("name") = Language.getMessage(mstrModuleName, 7, "Select")
            dsFill.Tables(0).Rows.InsertAt(drRow, 0)

            'Pinkal (19-Nov-2012) -- End


            cboProvider.DisplayMember = "name"
            cboProvider.ValueMember = "providerunkid"
            cboProvider.DataSource = dsFill.Tables("Provider")

            dsFill = Nothing
            Dim objMaster As New clsmedical_master
            dsFill = objMaster.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
            cboMedicalCover.DisplayMember = "name"
            cboMedicalCover.ValueMember = "mdmasterunkid"
            cboMedicalCover.DataSource = dsFill.Tables(0)


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If ConfigParameter._Object._SickSheetAllocationId > 0 Then

                Select Case ConfigParameter._Object._SickSheetAllocationId

                    Case enAllocation.BRANCH

                        Dim objBranch As New clsStation
                        dsFill = objBranch.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "stationunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objBranch = Nothing

                    Case enAllocation.DEPARTMENT_GROUP

                        Dim objDeptGroup As New clsDepartmentGroup
                        dsFill = objDeptGroup.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "deptgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objDeptGroup = Nothing

                    Case enAllocation.DEPARTMENT

                        Dim objDepartment As New clsDepartment
                        dsFill = objDepartment.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "departmentunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objDepartment = Nothing

                    Case enAllocation.SECTION_GROUP

                        Dim objSectionGrp As New clsSectionGroup
                        dsFill = objSectionGrp.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "sectiongroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objSectionGrp = Nothing

                    Case enAllocation.SECTION

                        Dim objSection As New clsSections
                        dsFill = objSection.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "sectionunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objSection = Nothing

                    Case enAllocation.UNIT_GROUP

                        Dim objUnitGrp As New clsUnitGroup
                        dsFill = objUnitGrp.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "unitgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objUnitGrp = Nothing

                    Case enAllocation.UNIT

                        Dim objUnit As New clsUnits
                        dsFill = objUnit.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "unitunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objUnit = Nothing

                    Case enAllocation.TEAM

                        Dim objTeam As New clsTeams
                        dsFill = objTeam.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "teamunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objTeam = Nothing

                    Case enAllocation.JOB_GROUP

                        Dim objJobGrp As New clsJobGroup
                        dsFill = objJobGrp.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "jobgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objJobGrp = Nothing

                    Case enAllocation.JOBS

                        Dim objJob As New clsJobs
                        dsFill = objJob.getComboList("List", True, , , , , , , "1=1")
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "jobunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objJob = Nothing

                    Case enAllocation.CLASS_GROUP

                        Dim objClassGrp As New clsClassGroup
                        dsFill = objClassGrp.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "classgroupunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objClassGrp = Nothing

                    Case enAllocation.CLASSES

                        Dim objClass As New clsClass
                        dsFill = objClass.getComboList("List", True)
                        cboSickSheetAllocation.DisplayMember = "name"
                        cboSickSheetAllocation.ValueMember = "classesunkid"
                        cboSickSheetAllocation.DataSource = dsFill.Tables("List")
                        objClass = Nothing

                End Select

                cboSickSheetAllocation.SelectedValue = 0

            End If

            'Pinkal (12-Sep-2014) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub Getvalue()
        Try

            txtSheetNo.Text = objsicksheet._Sicksheetno


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'If objsicksheet._Sicksheetdate.Date <> Nothing Then
            '    dtpsheetDate.Value = objsicksheet._Sicksheetdate.Date
            'Else
            '    dtpsheetDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'End If

            If objsicksheet._Sicksheetdate.Date <> Nothing Then
                dtpsheetDate.Value = objsicksheet._Sicksheetdate
            Else
                dtpsheetDate.Value = ConfigParameter._Object._CurrentDateAndTime
            End If

            'Pinkal (20-Jan-2012) -- End

            cboProvider.SelectedValue = objsicksheet._Instituteunkid

            Dim objemployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objemployee._Employeeunkid = objsicksheet._Employeeunkid

            'txtEmployeeCode.Text = objemployee._Employeecode
            'txtEmployeeName.Tag = objemployee._Employeeunkid

            objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objsicksheet._Employeeunkid

            txtEmployeeCode.Text = objemployee._Employeecode
            txtEmployeeName.Tag = objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'S.SANDEEP [04 JUN 2015] -- END
            
            txtEmployeeName.Text = objemployee._Firstname & "  " & objemployee._Othername & "  " & objemployee._Surname

            objJob._Jobunkid = objsicksheet._Jobunkid

            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'txtjob.Text = objJob._Job_Name & " " & Language.getMessage("clsJobs", 5, "Level") & " " & objJob._Job_Level
            txtjob.Text = objJob._Job_Name
            'Pinkal (07-Jan-2012) -- End

            txtjob.Tag = objJob._Jobunkid

            cboMedicalCover.SelectedValue = objsicksheet._Medicalcoverunkid


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            mstrDependantIds = objsicksheet._Dependantunkid
            If mstrDependantIds.Trim.Length > 0 Then
                Dim arDependent() As String = mstrDependantIds.Trim.Split(CChar(","))
                For i As Integer = 0 To arDependent.Length - 1
                    For j As Integer = 0 To lvDependantsList.Items.Count - 1
                        If CInt(arDependent(i)) = CInt(lvDependantsList.Items(j).Tag) Then
                            lvDependantsList.Items(j).Checked = True
                        End If
                    Next
                Next

                If lvDependantsList.CheckedItems.Count <= 0 Then
                    objChkAll.Checked = False
                ElseIf lvDependantsList.CheckedItems.Count < lvDependantsList.Items.Count Then
                    objChkAll.CheckState = CheckState.Indeterminate
                ElseIf lvDependantsList.Items.Count = lvDependantsList.CheckedItems.Count Then
                    objChkAll.Checked = True
                End If

            End If

            If cboSickSheetAllocation.Items.Count > 0 Then
                cboSickSheetAllocation.SelectedValue = objsicksheet._Allocationtranunkid
            End If

            'Pinkal (12-Sep-2014) -- End




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Getvalue", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            objsicksheet._Sicksheetno = txtSheetNo.Text.Trim

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'objsicksheet._Sicksheetdate = dtpsheetDate.Value.Date
            objsicksheet._Sicksheetdate = dtpsheetDate.Value
            'Pinkal (20-Jan-2012) -- End


            objsicksheet._Instituteunkid = CInt(cboProvider.SelectedValue)
            objsicksheet._Employeeunkid = CInt(txtEmployeeName.Tag)
            objsicksheet._Jobunkid = CInt(txtjob.Tag)
            objsicksheet._Medicalcoverunkid = CInt(cboMedicalCover.SelectedValue)


            If lvDependantsList.Items.Count > 0 Then
                Dim mstrDependants As String = ""

                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

                'For i As Integer = 0 To lvDependantsList.Items.Count - 1
                '    mstrDependants &= CInt(lvDependantsList.Items(i).Tag) & ","
                'Next


                'Pinkal (30-Dec-2015) -- Start
                'Enhancement - SOLVED BUG WHEN CHECKED DEPENDENT NOT UPDATED WHEN SICK SHEET UPDATE.
                'For i As Integer = 0 To lvDependantsList.CheckedItems.Count - 1
                '    mstrDependants &= CInt(lvDependantsList.Items(i).Tag) & ","
                'Next

                For i As Integer = 0 To lvDependantsList.CheckedItems.Count - 1
                    mstrDependants &= CInt(lvDependantsList.CheckedItems(i).Tag) & ","
                Next

                'Pinkal (30-Dec-2015) -- End



                'Pinkal (12-Sep-2014) -- End


                If mstrDependants.Trim.Length > 0 Then
                    objsicksheet._Dependantunkid = mstrDependants.Trim.Substring(0, mstrDependants.Trim.Length - 1)
                Else
                    objsicksheet._Dependantunkid = mstrDependants
                End If

            End If

            objsicksheet._Userunkid = User._Object._Userunkid


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            objsicksheet._AllocationtypeId = ConfigParameter._Object._SickSheetAllocationId
            If cboSickSheetAllocation.DataSource IsNot Nothing Then
                objsicksheet._Allocationtranunkid = CInt(cboSickSheetAllocation.SelectedValue)
            Else
                objsicksheet._Allocationtranunkid = 0
            End If
            'Pinkal (12-Sep-2014) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub FillDependantList()
        Try

            Dim objDependant As New clsDependants_Beneficiary_tran

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'Dim dsList As DataSet = objDependant.GetQualifiedDepedant(CInt(txtEmployeeName.Tag))
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dsList As DataSet = objDependant.GetQualifiedDepedant(CInt(txtEmployeeName.Tag), True, False, dtpsheetDate.Value.Date)
            Dim dsList As DataSet = objDependant.GetQualifiedDepedant(CInt(txtEmployeeName.Tag), True, False, dtpsheetDate.Value.Date, dtAsOnDate:=dtpsheetDate.Value.Date)
            'Sohail (18 May 2019) -- End
            'Pinkal (21-Jul-2014) -- End


                lvDependantsList.Items.Clear()

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim lvItem As ListViewItem
                For Each dr As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = CInt(dr("dependent_Id").ToString())


                    'Pinkal (12-Sep-2014) -- Start
                    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                    'lvItem.Text = dr("dependants").ToString()
                    lvItem.Text = ""
                    lvItem.SubItems.Add(dr("dependants").ToString())
                    'Pinkal (12-Sep-2014) -- End

                    lvItem.SubItems.Add(dr("gender").ToString())
                    lvItem.SubItems.Add(dr("age").ToString())

                    'Pinkal (19-Jun-2014) -- Start
                    'Enhancement : TRA Changes Leave Enhancement
                    lvItem.SubItems.Add(dr("Months").ToString())
                    'Pinkal (19-Jun-2014) -- End

                    lvItem.SubItems.Add(dr("relation").ToString())
                    lvDependantsList.Items.Add(lvItem)
                Next


                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

                'If lvDependantsList.Items.Count > 10 Then
                '    colhEmpName.Width = 150 - 18
                'Else
                '    colhEmpName.Width = 150
                'End If

                If lvDependantsList.Items.Count > 7 Then
                    colhRelation.Width = 129 - 18
                Else
                    colhRelation.Width = 129
                End If

                'Pinkal (12-Sep-2014) -- End

                
                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                If menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE Then
                    If lvDependantsList.Items.Count > 0 Then objChkAll.Checked = True
                End If
                'Pinkal (12-Sep-2014) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDependantList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetProcessVisiblity()
        Try
            If ConfigParameter._Object._SickSheetNotype = 1 Then
                txtSheetNo.Enabled = False
            Else
                txtSheetNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProcessVisiblity", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmSicksheet_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objsicksheet = New clsmedical_sicksheet
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            SetProcessVisiblity()
            FillCombo()
            objJob = New clsJobs


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If ConfigParameter._Object._SickSheetAllocationId <= 0 Then
                LblSickSheetAllocation.Visible = False
                cboSickSheetAllocation.Visible = False
                objbtnSearchSickSheetAllocation.Visible = False
            ElseIf ConfigParameter._Object._SickSheetAllocationId > 0 Then
                Dim objMst As New clsMasterData
                Dim dsList As DataSet = objMst.GetEAllocation_Notification("List", ConfigParameter._Object._SickSheetAllocationId.ToString(), True)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    LblSickSheetAllocation.Text = dsList.Tables(0).Rows(0)("Name").ToString()
                End If
                objMst = Nothing
            End If

            'Pinkal (12-Sep-2014) -- End


            If menAction = enAction.EDIT_ONE Then
                objsicksheet._Sicksheetunkid = mintSicksheetunkid
                Getvalue()
                txtSheetNo.ReadOnly = True
                txtEmployeeCode.ReadOnly = True
                txtEmployeeName.ReadOnly = True
                objbtnSearchEmpCode.Enabled = False
                objbtnSearchEmp.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSicksheet_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSicksheet_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSicksheet_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSicksheet_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objsicksheet = Nothing
    End Sub
    
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_sicksheet.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_sicksheet"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnflag As Boolean
        Try

            If ConfigParameter._Object._SickSheetNotype = 0 And txtSheetNo.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sick Sheet No cannot be blank.Sick Sheet No is required information."), enMsgBoxStyle.Information)
                txtSheetNo.Focus()
                Exit Sub

            ElseIf CInt(cboProvider.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Provider is compulsory information.Please Select Provider."), enMsgBoxStyle.Information)
                cboProvider.Focus()
                Exit Sub

            ElseIf txtEmployeeCode.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is compulsory information.Please Select Employee Code."), enMsgBoxStyle.Information)
                txtEmployeeCode.Focus()
                Exit Sub

            ElseIf txtEmployeeName.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Name is compulsory information.Please Select Employee Name."), enMsgBoxStyle.Information)
                txtEmployeeName.Focus()
                Exit Sub

            ElseIf CInt(txtjob.Tag) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Job / Position is compulsory information.Please Select Job / Position."), enMsgBoxStyle.Information)
                txtjob.Focus()
                Exit Sub

            ElseIf CInt(cboMedicalCover.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Medical Cover is compulsory information.Please Select Medical Cover."), enMsgBoxStyle.Information)
                cboMedicalCover.Focus()
                Exit Sub
            End If



            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If cboSickSheetAllocation.Visible AndAlso CInt(cboSickSheetAllocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(LblSickSheetAllocation.Text & " " & Language.getMessage(mstrModuleName, 9, "is compulsory information.Please select") & " " & LblSickSheetAllocation.Text & ".", enMsgBoxStyle.Information)
                cboSickSheetAllocation.Select()
                Exit Sub
            End If

            If lvDependantsList.Items.Count > 0 AndAlso lvDependantsList.CheckedItems.Count < lvDependantsList.Items.Count Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Some of the depedants are not ticked.They will not be included in this sick sheet.Are you sure you want to save this sick sheet ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If

            'Pinkal (12-Sep-2014) -- End


            SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objsicksheet._FormName = mstrModuleName
            objsicksheet._LoginEmployeeunkid = 0
            objsicksheet._ClientIP = getIP()
            objsicksheet._HostName = getHostName()
            objsicksheet._FromWeb = False
            objsicksheet._AuditUserId = User._Object._Userunkid
objsicksheet._CompanyUnkid = Company._Object._Companyunkid
            objsicksheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnflag = objsicksheet.Update()
            Else
                blnflag = objsicksheet.Insert()
            End If

            If blnflag = False And objsicksheet._Message <> "" Then
                eZeeMsgBox.Show(objsicksheet._Message, enMsgBoxStyle.Information)
            End If

            If blnflag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objsicksheet = Nothing
                    objsicksheet = New clsmedical_sicksheet
                    'Pinkal (07-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    txtjob.Text = ""
                    'Pinkal (07-Jan-2012) -- End

                    Call Getvalue()
                    txtSheetNo.Select()
                Else
                    mintSicksheetunkid = objsicksheet._Sicksheetunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchEmpCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmpCode.Click, objbtnSearchEmp.Click
        Try
            If dsEmpList Is Nothing Then
                Dim objEmployee As New clsEmployee_Master

                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                'dsEmpList = objEmployee.GetList("Employee", False, True, ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If User._Object.Privilege._RevokeUserAccessOnSicksheet Then
                '    dsEmpList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , " AND 1=1")
                'Else
                '    dsEmpList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'End If

                dsEmpList = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, True, _
                                                True, _
                                                "Employee", _
                                                ConfigParameter._Object._ShowFirstAppointmentDate, , , , , Not User._Object.Privilege._RevokeUserAccessOnSicksheet)
                'S.SANDEEP [04 JUN 2015] -- END
                
                'Pinkal (12-Sep-2014) -- End

            End If

            Dim frm As New frmCommonSearch
            frm.DataSource = dsEmpList.Tables(0)
            frm.DisplayMember = "name"
            frm.ValueMember = "employeeunkid"
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog() Then
                txtEmployeeName.Tag = CInt(frm.SelectedValue)
                txtEmployeeCode.Text = frm.SelectedAlias
                txtEmployeeName.Text = frm.SelectedText

                Dim drRow As DataRow() = dsEmpList.Tables(0).Select("employeeunkid = " & CInt(frm.SelectedValue))
                If drRow.Length > 0 Then
                    objJob._Jobunkid = CInt(drRow(0)("jobunkid"))

                    'Pinkal (07-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    'txtjob.Text = objJob._Job_Name & " " & Language.getMessage("clsJobs", 5, "Level") & " " & objJob._Job_Level
                    txtjob.Text = objJob._Job_Name
                    'Pinkal (07-Jan-2012) -- End

                    txtjob.Tag = objJob._Jobunkid
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmpCode_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboProvider.DataSource, DataTable)
            frm.DisplayMember = cboProvider.DisplayMember
            frm.ValueMember = cboProvider.ValueMember
            If frm.DisplayDialog() Then
                cboProvider.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private Sub objbtnSearchSickSheetAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSickSheetAllocation.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboSickSheetAllocation.DataSource, DataTable)
            frm.DisplayMember = cboSickSheetAllocation.DisplayMember
            frm.ValueMember = cboSickSheetAllocation.ValueMember
            If frm.DisplayDialog() Then
                cboSickSheetAllocation.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSickSheetAllocation_Click", mstrModuleName)
        End Try
    End Sub
    'Pinkal (12-Sep-2014) -- End


#End Region

#Region " TextBox's Event(s) "

    Private Sub txtEmployeeName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmployeeName.TextChanged
        Try
            FillDependantList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEmployeeName_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

#Region "Datepicker Event"

    Private Sub dtpsheetDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpsheetDate.ValueChanged
        Try
            If CInt(txtEmployeeName.Tag) > 0 Then
                FillDependantList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpsheetDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jul-2014) -- End


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

#Region "CheckBox Event"

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvDependantsList.ItemChecked, AddressOf lvDependantsList_ItemChecked
            For Each lvItem As ListViewItem In lvDependantsList.Items
                lvItem.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvDependantsList.ItemChecked, AddressOf lvDependantsList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvDependantsList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDependantsList.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvDependantsList.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvDependantsList.CheckedItems.Count < lvDependantsList.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvDependantsList.CheckedItems.Count = lvDependantsList.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDependantsList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (12-Sep-2014) -- End





    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDependantsList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDependantsList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

			
    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblSheetNo.Text = Language._Object.getCaption(Me.lblSheetNo.Name, Me.lblSheetNo.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.lblEmployeecode.Text = Language._Object.getCaption(Me.lblEmployeecode.Name, Me.lblEmployeecode.Text)
            Me.lblCover.Text = Language._Object.getCaption(Me.lblCover.Name, Me.lblCover.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.gbDependantsList.Text = Language._Object.getCaption(Me.gbDependantsList.Name, Me.gbDependantsList.Text)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.Name, Me.lblProvider.Text)
			Me.colhGender.Text = Language._Object.getCaption(CStr(Me.colhGender.Tag), Me.colhGender.Text)
			Me.colhAge.Text = Language._Object.getCaption(CStr(Me.colhAge.Tag), Me.colhAge.Text)
			Me.colhRelation.Text = Language._Object.getCaption(CStr(Me.colhRelation.Tag), Me.colhRelation.Text)
			Me.colhMonth.Text = Language._Object.getCaption(CStr(Me.colhMonth.Tag), Me.colhMonth.Text)
			Me.LblSickSheetAllocation.Text = Language._Object.getCaption(Me.LblSickSheetAllocation.Name, Me.LblSickSheetAllocation.Text)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sick Sheet No cannot be blank.Sick Sheet No is required information.")
            Language.setMessage(mstrModuleName, 2, "Provider is compulsory information.Please Select Provider.")
            Language.setMessage(mstrModuleName, 3, "Employee Code is compulsory information.Please Select Employee Code.")
            Language.setMessage(mstrModuleName, 4, "Employee Name is compulsory information.Please Select Employee Name.")
	    Language.setMessage(mstrModuleName, 5, "Job / Position is compulsory information.Please Select Job / Position.")
            Language.setMessage(mstrModuleName, 6, "Medical Cover is compulsory information.Please Select Medical Cover.")
    	    Language.setMessage(mstrModuleName, 7, "Select")
	    Language.setMessage(mstrModuleName, 8, "Some of the depedants are not ticked.They will not be included in this sick sheet.Are you sure you want to save this sick sheet ?")
	    Language.setMessage(mstrModuleName, 9, "is compulsory information.Please select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub

#End Region 'Language & UI Settings
	'</Language>
End Class
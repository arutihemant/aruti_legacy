﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayment_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayment_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblMessage = New System.Windows.Forms.Label
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPayment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.txtPayYear = New eZee.TextBox.AlphanumericTextBox
        Me.lblPayYear = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.cboAccountNo = New System.Windows.Forms.ComboBox
        Me.lblAccountNo = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lblPayment = New System.Windows.Forms.Label
        Me.lblScheme = New System.Windows.Forms.Label
        Me.txtScheme = New eZee.TextBox.AlphanumericTextBox
        Me.lblLoanAmount = New System.Windows.Forms.Label
        Me.txtBalanceDue = New eZee.TextBox.NumericTextBox
        Me.txtValue = New eZee.TextBox.NumericTextBox
        Me.lblValue = New System.Windows.Forms.Label
        Me.lblBalanceDue = New System.Windows.Forms.Label
        Me.lblVoucher = New System.Windows.Forms.Label
        Me.txtAgainstVoucher = New eZee.TextBox.AlphanumericTextBox
        Me.txtVoucher = New eZee.TextBox.AlphanumericTextBox
        Me.txtPaymentOf = New eZee.TextBox.AlphanumericTextBox
        Me.lblAgainstVoucher = New System.Windows.Forms.Label
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.txtPayPeriod = New eZee.TextBox.AlphanumericTextBox
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtChequeNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCheque = New System.Windows.Forms.Label
        Me.lblPaymentBy = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboPaymentBy = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBankGroup = New System.Windows.Forms.ComboBox
        Me.lblBankGroup = New System.Windows.Forms.Label
        Me.lblPaymentOf = New System.Windows.Forms.Label
        Me.lblPaymentMode = New System.Windows.Forms.Label
        Me.lblPaidTo = New System.Windows.Forms.Label
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.cboPaymentMode = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.lblPaymentDate = New System.Windows.Forms.Label
        Me.dtpPaymentDate = New System.Windows.Forms.DateTimePicker
        Me.lblReceivedFrom = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbPayment.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbPayment)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(670, 344)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblMessage)
        Me.objFooter.Controls.Add(Me.objlblExRate)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 289)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(670, 55)
        Me.objFooter.TabIndex = 1
        '
        'objlblMessage
        '
        Me.objlblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblMessage.Location = New System.Drawing.Point(12, 30)
        Me.objlblMessage.Name = "objlblMessage"
        Me.objlblMessage.Size = New System.Drawing.Size(440, 16)
        Me.objlblMessage.TabIndex = 162
        Me.objlblMessage.Text = "1"
        Me.objlblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(12, 11)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(440, 16)
        Me.objlblExRate.TabIndex = 161
        Me.objlblExRate.Text = "1"
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(458, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(561, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbPayment
        '
        Me.gbPayment.BorderColor = System.Drawing.Color.Black
        Me.gbPayment.Checked = False
        Me.gbPayment.CollapseAllExceptThis = False
        Me.gbPayment.CollapsedHoverImage = Nothing
        Me.gbPayment.CollapsedNormalImage = Nothing
        Me.gbPayment.CollapsedPressedImage = Nothing
        Me.gbPayment.CollapseOnLoad = False
        Me.gbPayment.Controls.Add(Me.cboPeriod)
        Me.gbPayment.Controls.Add(Me.lblPeriod)
        Me.gbPayment.Controls.Add(Me.txtPayYear)
        Me.gbPayment.Controls.Add(Me.lblPayYear)
        Me.gbPayment.Controls.Add(Me.txtRemarks)
        Me.gbPayment.Controls.Add(Me.lblRemarks)
        Me.gbPayment.Controls.Add(Me.cboAccountNo)
        Me.gbPayment.Controls.Add(Me.lblAccountNo)
        Me.gbPayment.Controls.Add(Me.cboCurrency)
        Me.gbPayment.Controls.Add(Me.lblCurrency)
        Me.gbPayment.Controls.Add(Me.lblPayment)
        Me.gbPayment.Controls.Add(Me.lblScheme)
        Me.gbPayment.Controls.Add(Me.txtScheme)
        Me.gbPayment.Controls.Add(Me.lblLoanAmount)
        Me.gbPayment.Controls.Add(Me.txtBalanceDue)
        Me.gbPayment.Controls.Add(Me.txtValue)
        Me.gbPayment.Controls.Add(Me.lblValue)
        Me.gbPayment.Controls.Add(Me.lblBalanceDue)
        Me.gbPayment.Controls.Add(Me.lblVoucher)
        Me.gbPayment.Controls.Add(Me.txtAgainstVoucher)
        Me.gbPayment.Controls.Add(Me.txtVoucher)
        Me.gbPayment.Controls.Add(Me.txtPaymentOf)
        Me.gbPayment.Controls.Add(Me.lblAgainstVoucher)
        Me.gbPayment.Controls.Add(Me.txtEmployee)
        Me.gbPayment.Controls.Add(Me.txtPayPeriod)
        Me.gbPayment.Controls.Add(Me.txtAmount)
        Me.gbPayment.Controls.Add(Me.lblAmount)
        Me.gbPayment.Controls.Add(Me.txtChequeNo)
        Me.gbPayment.Controls.Add(Me.lblCheque)
        Me.gbPayment.Controls.Add(Me.lblPaymentBy)
        Me.gbPayment.Controls.Add(Me.cboBranch)
        Me.gbPayment.Controls.Add(Me.cboPaymentBy)
        Me.gbPayment.Controls.Add(Me.lblBranch)
        Me.gbPayment.Controls.Add(Me.cboBankGroup)
        Me.gbPayment.Controls.Add(Me.lblBankGroup)
        Me.gbPayment.Controls.Add(Me.lblPaymentOf)
        Me.gbPayment.Controls.Add(Me.lblPaymentMode)
        Me.gbPayment.Controls.Add(Me.lblPaidTo)
        Me.gbPayment.Controls.Add(Me.objelLine1)
        Me.gbPayment.Controls.Add(Me.cboPaymentMode)
        Me.gbPayment.Controls.Add(Me.lblPayPeriod)
        Me.gbPayment.Controls.Add(Me.lblPaymentDate)
        Me.gbPayment.Controls.Add(Me.dtpPaymentDate)
        Me.gbPayment.Controls.Add(Me.lblReceivedFrom)
        Me.gbPayment.ExpandedHoverImage = Nothing
        Me.gbPayment.ExpandedNormalImage = Nothing
        Me.gbPayment.ExpandedPressedImage = Nothing
        Me.gbPayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPayment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPayment.HeaderHeight = 25
        Me.gbPayment.HeaderMessage = ""
        Me.gbPayment.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbPayment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPayment.HeightOnCollapse = 0
        Me.gbPayment.LeftTextSpace = 0
        Me.gbPayment.Location = New System.Drawing.Point(12, 12)
        Me.gbPayment.Name = "gbPayment"
        Me.gbPayment.OpenHeight = 300
        Me.gbPayment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPayment.ShowBorder = True
        Me.gbPayment.ShowCheckBox = False
        Me.gbPayment.ShowCollapseButton = False
        Me.gbPayment.ShowDefaultBorderColor = True
        Me.gbPayment.ShowDownButton = False
        Me.gbPayment.ShowHeader = True
        Me.gbPayment.Size = New System.Drawing.Size(646, 272)
        Me.gbPayment.TabIndex = 0
        Me.gbPayment.Temp = 0
        Me.gbPayment.Text = "Payment Information"
        Me.gbPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(345, 115)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(97, 21)
        Me.cboPeriod.TabIndex = 210
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(260, 117)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(79, 16)
        Me.lblPeriod.TabIndex = 211
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPayYear
        '
        Me.txtPayYear.Flags = 0
        Me.txtPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayYear.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPayYear.Location = New System.Drawing.Point(532, 36)
        Me.txtPayYear.Name = "txtPayYear"
        Me.txtPayYear.ReadOnly = True
        Me.txtPayYear.Size = New System.Drawing.Size(96, 21)
        Me.txtPayYear.TabIndex = 6
        Me.txtPayYear.Visible = False
        '
        'lblPayYear
        '
        Me.lblPayYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYear.Location = New System.Drawing.Point(455, 38)
        Me.lblPayYear.Name = "lblPayYear"
        Me.lblPayYear.Size = New System.Drawing.Size(71, 16)
        Me.lblPayYear.TabIndex = 148
        Me.lblPayYear.Text = "Pay Year"
        Me.lblPayYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayYear.Visible = False
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(96, 240)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(155, 27)
        Me.txtRemarks.TabIndex = 16
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(8, 242)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(82, 16)
        Me.lblRemarks.TabIndex = 208
        Me.lblRemarks.Text = "Remarks"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAccountNo
        '
        Me.cboAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountNo.FormattingEnabled = True
        Me.cboAccountNo.Location = New System.Drawing.Point(96, 213)
        Me.cboAccountNo.Name = "cboAccountNo"
        Me.cboAccountNo.Size = New System.Drawing.Size(155, 21)
        Me.cboAccountNo.TabIndex = 13
        '
        'lblAccountNo
        '
        Me.lblAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountNo.Location = New System.Drawing.Point(8, 215)
        Me.lblAccountNo.Name = "lblAccountNo"
        Me.lblAccountNo.Size = New System.Drawing.Size(82, 16)
        Me.lblAccountNo.TabIndex = 205
        Me.lblAccountNo.Text = "Account No."
        Me.lblAccountNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(541, 159)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(97, 21)
        Me.cboCurrency.TabIndex = 10
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(455, 161)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(80, 16)
        Me.lblCurrency.TabIndex = 202
        Me.lblCurrency.Text = "Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayment
        '
        Me.lblPayment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayment.Location = New System.Drawing.Point(8, 117)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(82, 16)
        Me.lblPayment.TabIndex = 200
        Me.lblPayment.Text = "Payment"
        Me.lblPayment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPayment.Visible = False
        '
        'lblScheme
        '
        Me.lblScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblScheme.Name = "lblScheme"
        Me.lblScheme.Size = New System.Drawing.Size(82, 16)
        Me.lblScheme.TabIndex = 198
        Me.lblScheme.Text = "Scheme"
        Me.lblScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtScheme
        '
        Me.txtScheme.Flags = 0
        Me.txtScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtScheme.Location = New System.Drawing.Point(96, 61)
        Me.txtScheme.Name = "txtScheme"
        Me.txtScheme.ReadOnly = True
        Me.txtScheme.Size = New System.Drawing.Size(532, 21)
        Me.txtScheme.TabIndex = 1
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmount.Location = New System.Drawing.Point(8, 117)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(82, 16)
        Me.lblLoanAmount.TabIndex = 195
        Me.lblLoanAmount.Text = "Loan Amount"
        Me.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBalanceDue
        '
        Me.txtBalanceDue.AllowNegative = True
        Me.txtBalanceDue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtBalanceDue.DigitsInGroup = 0
        Me.txtBalanceDue.Flags = 0
        Me.txtBalanceDue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalanceDue.Location = New System.Drawing.Point(96, 115)
        Me.txtBalanceDue.MaxDecimalPlaces = 6
        Me.txtBalanceDue.MaxWholeDigits = 21
        Me.txtBalanceDue.Name = "txtBalanceDue"
        Me.txtBalanceDue.Prefix = ""
        Me.txtBalanceDue.RangeMax = 1.7976931348623157E+308
        Me.txtBalanceDue.RangeMin = -1.7976931348623157E+308
        Me.txtBalanceDue.ReadOnly = True
        Me.txtBalanceDue.Size = New System.Drawing.Size(155, 21)
        Me.txtBalanceDue.TabIndex = 3
        Me.txtBalanceDue.Text = "0"
        Me.txtBalanceDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtValue
        '
        Me.txtValue.AllowNegative = True
        Me.txtValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtValue.DigitsInGroup = 0
        Me.txtValue.Flags = 0
        Me.txtValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValue.Location = New System.Drawing.Point(345, 240)
        Me.txtValue.MaxDecimalPlaces = 6
        Me.txtValue.MaxWholeDigits = 21
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Prefix = ""
        Me.txtValue.RangeMax = 1.7976931348623157E+308
        Me.txtValue.RangeMin = -1.7976931348623157E+308
        Me.txtValue.Size = New System.Drawing.Size(104, 21)
        Me.txtValue.TabIndex = 17
        Me.txtValue.Text = "0"
        Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblValue
        '
        Me.lblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValue.Location = New System.Drawing.Point(257, 242)
        Me.lblValue.Name = "lblValue"
        Me.lblValue.Size = New System.Drawing.Size(82, 16)
        Me.lblValue.TabIndex = 184
        Me.lblValue.Text = "Percent(%)"
        Me.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBalanceDue
        '
        Me.lblBalanceDue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceDue.Location = New System.Drawing.Point(8, 117)
        Me.lblBalanceDue.Name = "lblBalanceDue"
        Me.lblBalanceDue.Size = New System.Drawing.Size(82, 16)
        Me.lblBalanceDue.TabIndex = 191
        Me.lblBalanceDue.Text = "Due Balance"
        Me.lblBalanceDue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVoucher
        '
        Me.lblVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucher.Location = New System.Drawing.Point(257, 90)
        Me.lblVoucher.Name = "lblVoucher"
        Me.lblVoucher.Size = New System.Drawing.Size(81, 16)
        Me.lblVoucher.TabIndex = 181
        Me.lblVoucher.Text = "Voucher"
        Me.lblVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAgainstVoucher
        '
        Me.txtAgainstVoucher.Flags = 0
        Me.txtAgainstVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAgainstVoucher.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAgainstVoucher.Location = New System.Drawing.Point(96, 159)
        Me.txtAgainstVoucher.Name = "txtAgainstVoucher"
        Me.txtAgainstVoucher.Size = New System.Drawing.Size(155, 21)
        Me.txtAgainstVoucher.TabIndex = 8
        '
        'txtVoucher
        '
        Me.txtVoucher.Flags = 0
        Me.txtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucher.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucher.Location = New System.Drawing.Point(345, 88)
        Me.txtVoucher.Name = "txtVoucher"
        Me.txtVoucher.ReadOnly = True
        Me.txtVoucher.Size = New System.Drawing.Size(104, 21)
        Me.txtVoucher.TabIndex = 4
        '
        'txtPaymentOf
        '
        Me.txtPaymentOf.Flags = 0
        Me.txtPaymentOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaymentOf.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPaymentOf.Location = New System.Drawing.Point(96, 88)
        Me.txtPaymentOf.Name = "txtPaymentOf"
        Me.txtPaymentOf.ReadOnly = True
        Me.txtPaymentOf.Size = New System.Drawing.Size(155, 21)
        Me.txtPaymentOf.TabIndex = 2
        '
        'lblAgainstVoucher
        '
        Me.lblAgainstVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAgainstVoucher.Location = New System.Drawing.Point(8, 161)
        Me.lblAgainstVoucher.Name = "lblAgainstVoucher"
        Me.lblAgainstVoucher.Size = New System.Drawing.Size(82, 16)
        Me.lblAgainstVoucher.TabIndex = 181
        Me.lblAgainstVoucher.Text = "Voucher #"
        Me.lblAgainstVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployee
        '
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(96, 34)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(532, 21)
        Me.txtEmployee.TabIndex = 0
        '
        'txtPayPeriod
        '
        Me.txtPayPeriod.Flags = 0
        Me.txtPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayPeriod.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPayPeriod.Location = New System.Drawing.Point(532, 88)
        Me.txtPayPeriod.Name = "txtPayPeriod"
        Me.txtPayPeriod.ReadOnly = True
        Me.txtPayPeriod.Size = New System.Drawing.Size(96, 21)
        Me.txtPayPeriod.TabIndex = 7
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(541, 240)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(97, 21)
        Me.txtAmount.TabIndex = 18
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(455, 242)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(80, 16)
        Me.lblAmount.TabIndex = 171
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtChequeNo
        '
        Me.txtChequeNo.Flags = 0
        Me.txtChequeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChequeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtChequeNo.Location = New System.Drawing.Point(345, 213)
        Me.txtChequeNo.Name = "txtChequeNo"
        Me.txtChequeNo.Size = New System.Drawing.Size(104, 21)
        Me.txtChequeNo.TabIndex = 14
        '
        'lblCheque
        '
        Me.lblCheque.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCheque.Location = New System.Drawing.Point(257, 215)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(82, 16)
        Me.lblCheque.TabIndex = 166
        Me.lblCheque.Text = "Cheque No."
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaymentBy
        '
        Me.lblPaymentBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentBy.Location = New System.Drawing.Point(455, 215)
        Me.lblPaymentBy.Name = "lblPaymentBy"
        Me.lblPaymentBy.Size = New System.Drawing.Size(80, 16)
        Me.lblPaymentBy.TabIndex = 169
        Me.lblPaymentBy.Text = "Payment By"
        Me.lblPaymentBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(345, 186)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(104, 21)
        Me.cboBranch.TabIndex = 12
        '
        'cboPaymentBy
        '
        Me.cboPaymentBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentBy.FormattingEnabled = True
        Me.cboPaymentBy.Location = New System.Drawing.Point(541, 213)
        Me.cboPaymentBy.Name = "cboPaymentBy"
        Me.cboPaymentBy.Size = New System.Drawing.Size(97, 21)
        Me.cboPaymentBy.TabIndex = 15
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(257, 188)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(82, 16)
        Me.lblBranch.TabIndex = 164
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBankGroup
        '
        Me.cboBankGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankGroup.FormattingEnabled = True
        Me.cboBankGroup.Location = New System.Drawing.Point(96, 186)
        Me.cboBankGroup.Name = "cboBankGroup"
        Me.cboBankGroup.Size = New System.Drawing.Size(155, 21)
        Me.cboBankGroup.TabIndex = 11
        '
        'lblBankGroup
        '
        Me.lblBankGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankGroup.Location = New System.Drawing.Point(8, 188)
        Me.lblBankGroup.Name = "lblBankGroup"
        Me.lblBankGroup.Size = New System.Drawing.Size(82, 16)
        Me.lblBankGroup.TabIndex = 162
        Me.lblBankGroup.Text = "Bank Group"
        Me.lblBankGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaymentOf
        '
        Me.lblPaymentOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentOf.Location = New System.Drawing.Point(8, 90)
        Me.lblPaymentOf.Name = "lblPaymentOf"
        Me.lblPaymentOf.Size = New System.Drawing.Size(82, 16)
        Me.lblPaymentOf.TabIndex = 152
        Me.lblPaymentOf.Text = "Payment Of"
        Me.lblPaymentOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaymentMode
        '
        Me.lblPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentMode.Location = New System.Drawing.Point(257, 161)
        Me.lblPaymentMode.Name = "lblPaymentMode"
        Me.lblPaymentMode.Size = New System.Drawing.Size(85, 16)
        Me.lblPaymentMode.TabIndex = 160
        Me.lblPaymentMode.Text = "Payment Mode"
        Me.lblPaymentMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaidTo
        '
        Me.lblPaidTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaidTo.Location = New System.Drawing.Point(8, 36)
        Me.lblPaidTo.Name = "lblPaidTo"
        Me.lblPaidTo.Size = New System.Drawing.Size(82, 16)
        Me.lblPaidTo.TabIndex = 5
        Me.lblPaidTo.Text = "Paid To"
        Me.lblPaidTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine1
        '
        Me.objelLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(11, 139)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(617, 17)
        Me.objelLine1.TabIndex = 154
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentMode
        '
        Me.cboPaymentMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentMode.FormattingEnabled = True
        Me.cboPaymentMode.Location = New System.Drawing.Point(345, 159)
        Me.cboPaymentMode.Name = "cboPaymentMode"
        Me.cboPaymentMode.Size = New System.Drawing.Size(104, 21)
        Me.cboPaymentMode.TabIndex = 9
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(455, 90)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(71, 16)
        Me.lblPayPeriod.TabIndex = 149
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPaymentDate
        '
        Me.lblPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDate.Location = New System.Drawing.Point(458, 117)
        Me.lblPaymentDate.Name = "lblPaymentDate"
        Me.lblPaymentDate.Size = New System.Drawing.Size(68, 16)
        Me.lblPaymentDate.TabIndex = 3
        Me.lblPaymentDate.Text = "Date"
        Me.lblPaymentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPaymentDate
        '
        Me.dtpPaymentDate.Checked = False
        Me.dtpPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPaymentDate.Location = New System.Drawing.Point(532, 115)
        Me.dtpPaymentDate.Name = "dtpPaymentDate"
        Me.dtpPaymentDate.Size = New System.Drawing.Size(96, 21)
        Me.dtpPaymentDate.TabIndex = 5
        '
        'lblReceivedFrom
        '
        Me.lblReceivedFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedFrom.Location = New System.Drawing.Point(8, 36)
        Me.lblReceivedFrom.Name = "lblReceivedFrom"
        Me.lblReceivedFrom.Size = New System.Drawing.Size(82, 16)
        Me.lblReceivedFrom.TabIndex = 194
        Me.lblReceivedFrom.Text = "Received From"
        Me.lblReceivedFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmPayment_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(670, 344)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayment_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbPayment.ResumeLayout(False)
        Me.gbPayment.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPayment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPaymentOf As System.Windows.Forms.Label
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPayYear As System.Windows.Forms.Label
    Friend WithEvents lblPaidTo As System.Windows.Forms.Label
    Friend WithEvents dtpPaymentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPaymentDate As System.Windows.Forms.Label
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents cboPaymentMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaymentMode As System.Windows.Forms.Label
    Friend WithEvents cboBankGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankGroup As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents txtChequeNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents cboPaymentBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaymentBy As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPaymentOf As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtVoucher As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVoucher As System.Windows.Forms.Label
    Friend WithEvents txtAgainstVoucher As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAgainstVoucher As System.Windows.Forms.Label
    Friend WithEvents txtPayPeriod As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPayYear As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblValue As System.Windows.Forms.Label
    Friend WithEvents txtBalanceDue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBalanceDue As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents lblReceivedFrom As System.Windows.Forms.Label
    Friend WithEvents lblScheme As System.Windows.Forms.Label
    Friend WithEvents txtScheme As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents cboAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountNo As System.Windows.Forms.Label
    Friend WithEvents objlblMessage As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
End Class

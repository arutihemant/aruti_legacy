﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmATPaymentTranReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.lblType = New System.Windows.Forms.Label
        Me.cboType = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lblPaymentBy = New System.Windows.Forms.Label
        Me.cboPaymentBy = New System.Windows.Forms.ComboBox
        Me.lblPaymentMode = New System.Windows.Forms.Label
        Me.cboPaymentMode = New System.Windows.Forms.ComboBox
        Me.lblAppliedTo = New System.Windows.Forms.Label
        Me.lblPayDateFrom = New System.Windows.Forms.Label
        Me.dtpPayTo = New System.Windows.Forms.DateTimePicker
        Me.dtpPayFrom = New System.Windows.Forms.DateTimePicker
        Me.lblAuditUser = New System.Windows.Forms.Label
        Me.lblAuditType = New System.Windows.Forms.Label
        Me.cboAuditUser = New System.Windows.Forms.ComboBox
        Me.cboAuditType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblTo = New System.Windows.Forms.Label
        Me.dtpATDTo = New System.Windows.Forms.DateTimePicker
        Me.lblAuditFromDate = New System.Windows.Forms.Label
        Me.dtpATDFrom = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 516)
        Me.NavPanel.Size = New System.Drawing.Size(763, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.gbFilterCriteria.Controls.Add(Me.lblType)
        Me.gbFilterCriteria.Controls.Add(Me.cboType)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblPaymentBy)
        Me.gbFilterCriteria.Controls.Add(Me.cboPaymentBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblPaymentMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboPaymentMode)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppliedTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPayTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPayFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditUser)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditUser)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpATDTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpATDFrom)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(446, 224)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(228, 197)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(184, 16)
        Me.chkIncludeInactiveEmp.TabIndex = 180
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'lblType
        '
        Me.lblType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(225, 171)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(66, 15)
        Me.lblType.TabIndex = 177
        Me.lblType.Text = "Type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboType
        '
        Me.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(294, 168)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(118, 21)
        Me.cboType.TabIndex = 178
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 171)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(86, 15)
        Me.lblSection.TabIndex = 175
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(100, 168)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(117, 21)
        Me.cboSection.TabIndex = 176
        '
        'lblPaymentBy
        '
        Me.lblPaymentBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentBy.Location = New System.Drawing.Point(225, 144)
        Me.lblPaymentBy.Name = "lblPaymentBy"
        Me.lblPaymentBy.Size = New System.Drawing.Size(66, 15)
        Me.lblPaymentBy.TabIndex = 173
        Me.lblPaymentBy.Text = "Payment By"
        Me.lblPaymentBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentBy
        '
        Me.cboPaymentBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentBy.FormattingEnabled = True
        Me.cboPaymentBy.Location = New System.Drawing.Point(294, 141)
        Me.cboPaymentBy.Name = "cboPaymentBy"
        Me.cboPaymentBy.Size = New System.Drawing.Size(118, 21)
        Me.cboPaymentBy.TabIndex = 174
        '
        'lblPaymentMode
        '
        Me.lblPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentMode.Location = New System.Drawing.Point(8, 144)
        Me.lblPaymentMode.Name = "lblPaymentMode"
        Me.lblPaymentMode.Size = New System.Drawing.Size(86, 15)
        Me.lblPaymentMode.TabIndex = 171
        Me.lblPaymentMode.Text = "Payment Mode"
        Me.lblPaymentMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentMode
        '
        Me.cboPaymentMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentMode.FormattingEnabled = True
        Me.cboPaymentMode.Location = New System.Drawing.Point(100, 141)
        Me.cboPaymentMode.Name = "cboPaymentMode"
        Me.cboPaymentMode.Size = New System.Drawing.Size(117, 21)
        Me.cboPaymentMode.TabIndex = 172
        '
        'lblAppliedTo
        '
        Me.lblAppliedTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliedTo.Location = New System.Drawing.Point(225, 117)
        Me.lblAppliedTo.Name = "lblAppliedTo"
        Me.lblAppliedTo.Size = New System.Drawing.Size(66, 15)
        Me.lblAppliedTo.TabIndex = 97
        Me.lblAppliedTo.Text = "To"
        Me.lblAppliedTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayDateFrom
        '
        Me.lblPayDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayDateFrom.Location = New System.Drawing.Point(8, 117)
        Me.lblPayDateFrom.Name = "lblPayDateFrom"
        Me.lblPayDateFrom.Size = New System.Drawing.Size(86, 15)
        Me.lblPayDateFrom.TabIndex = 96
        Me.lblPayDateFrom.Text = "Pay Date From"
        Me.lblPayDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPayTo
        '
        Me.dtpPayTo.Checked = False
        Me.dtpPayTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPayTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPayTo.Location = New System.Drawing.Point(295, 114)
        Me.dtpPayTo.Name = "dtpPayTo"
        Me.dtpPayTo.ShowCheckBox = True
        Me.dtpPayTo.Size = New System.Drawing.Size(117, 21)
        Me.dtpPayTo.TabIndex = 95
        '
        'dtpPayFrom
        '
        Me.dtpPayFrom.Checked = False
        Me.dtpPayFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPayFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPayFrom.Location = New System.Drawing.Point(100, 114)
        Me.dtpPayFrom.Name = "dtpPayFrom"
        Me.dtpPayFrom.ShowCheckBox = True
        Me.dtpPayFrom.Size = New System.Drawing.Size(117, 21)
        Me.dtpPayFrom.TabIndex = 94
        '
        'lblAuditUser
        '
        Me.lblAuditUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditUser.Location = New System.Drawing.Point(8, 93)
        Me.lblAuditUser.Name = "lblAuditUser"
        Me.lblAuditUser.Size = New System.Drawing.Size(86, 15)
        Me.lblAuditUser.TabIndex = 93
        Me.lblAuditUser.Text = "Audit User"
        Me.lblAuditUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAuditType
        '
        Me.lblAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditType.Location = New System.Drawing.Point(225, 90)
        Me.lblAuditType.Name = "lblAuditType"
        Me.lblAuditType.Size = New System.Drawing.Size(66, 15)
        Me.lblAuditType.TabIndex = 92
        Me.lblAuditType.Text = "Audit Type"
        Me.lblAuditType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAuditUser
        '
        Me.cboAuditUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditUser.DropDownWidth = 180
        Me.cboAuditUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditUser.FormattingEnabled = True
        Me.cboAuditUser.Location = New System.Drawing.Point(100, 87)
        Me.cboAuditUser.Name = "cboAuditUser"
        Me.cboAuditUser.Size = New System.Drawing.Size(117, 21)
        Me.cboAuditUser.TabIndex = 91
        '
        'cboAuditType
        '
        Me.cboAuditType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditType.DropDownWidth = 180
        Me.cboAuditType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditType.FormattingEnabled = True
        Me.cboAuditType.Location = New System.Drawing.Point(295, 87)
        Me.cboAuditType.Name = "cboAuditType"
        Me.cboAuditType.Size = New System.Drawing.Size(117, 21)
        Me.cboAuditType.TabIndex = 90
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(418, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 89
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployee.TabIndex = 87
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(100, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(312, 21)
        Me.cboEmployee.TabIndex = 88
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(225, 36)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(66, 15)
        Me.lblTo.TabIndex = 86
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpATDTo
        '
        Me.dtpATDTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpATDTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpATDTo.Location = New System.Drawing.Point(294, 33)
        Me.dtpATDTo.Name = "dtpATDTo"
        Me.dtpATDTo.Size = New System.Drawing.Size(118, 21)
        Me.dtpATDTo.TabIndex = 85
        '
        'lblAuditFromDate
        '
        Me.lblAuditFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblAuditFromDate.Name = "lblAuditFromDate"
        Me.lblAuditFromDate.Size = New System.Drawing.Size(86, 15)
        Me.lblAuditFromDate.TabIndex = 84
        Me.lblAuditFromDate.Text = "Audit Date"
        Me.lblAuditFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpATDFrom
        '
        Me.dtpATDFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpATDFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpATDFrom.Location = New System.Drawing.Point(100, 33)
        Me.dtpATDFrom.Name = "dtpATDFrom"
        Me.dtpATDFrom.Size = New System.Drawing.Size(117, 21)
        Me.dtpATDFrom.TabIndex = 83
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 296)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(446, 63)
        Me.gbSortBy.TabIndex = 22
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(418, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(82, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(100, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(312, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(100, 195)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(117, 21)
        Me.cboBranch.TabIndex = 182
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 197)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(86, 15)
        Me.lblBranch.TabIndex = 183
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmATPaymentTranReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 571)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmATPaymentTranReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpATDTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAuditFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpATDFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAuditUser As System.Windows.Forms.Label
    Friend WithEvents lblAuditType As System.Windows.Forms.Label
    Friend WithEvents cboAuditUser As System.Windows.Forms.ComboBox
    Friend WithEvents cboAuditType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAppliedTo As System.Windows.Forms.Label
    Friend WithEvents lblPayDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpPayTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPayFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPaymentBy As System.Windows.Forms.Label
    Friend WithEvents cboPaymentBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaymentMode As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents cboType As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
End Class

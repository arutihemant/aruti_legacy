﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayementVoucherReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtAdvanceVoucherNo = New System.Windows.Forms.TextBox
        Me.LblAdvanceVoucherNo = New System.Windows.Forms.Label
        Me.dtpVoucherToDate = New System.Windows.Forms.DateTimePicker
        Me.LblVoucherToDate = New System.Windows.Forms.Label
        Me.dtpVoucherFromDate = New System.Windows.Forms.DateTimePicker
        Me.LblFromPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 419)
        Me.NavPanel.Size = New System.Drawing.Size(645, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtAdvanceVoucherNo)
        Me.gbFilterCriteria.Controls.Add(Me.LblAdvanceVoucherNo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpVoucherToDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblVoucherToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpVoucherFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(6, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(455, 116)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAdvanceVoucherNo
        '
        Me.txtAdvanceVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdvanceVoucherNo.Location = New System.Drawing.Point(128, 59)
        Me.txtAdvanceVoucherNo.Name = "txtAdvanceVoucherNo"
        Me.txtAdvanceVoucherNo.Size = New System.Drawing.Size(295, 21)
        Me.txtAdvanceVoucherNo.TabIndex = 213
        '
        'LblAdvanceVoucherNo
        '
        Me.LblAdvanceVoucherNo.BackColor = System.Drawing.Color.Transparent
        Me.LblAdvanceVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAdvanceVoucherNo.Location = New System.Drawing.Point(10, 61)
        Me.LblAdvanceVoucherNo.Name = "LblAdvanceVoucherNo"
        Me.LblAdvanceVoucherNo.Size = New System.Drawing.Size(110, 16)
        Me.LblAdvanceVoucherNo.TabIndex = 217
        Me.LblAdvanceVoucherNo.Text = "Advance Voucher No"
        '
        'dtpVoucherToDate
        '
        Me.dtpVoucherToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpVoucherToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpVoucherToDate.Location = New System.Drawing.Point(316, 86)
        Me.dtpVoucherToDate.Name = "dtpVoucherToDate"
        Me.dtpVoucherToDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpVoucherToDate.TabIndex = 216
        '
        'LblVoucherToDate
        '
        Me.LblVoucherToDate.BackColor = System.Drawing.Color.Transparent
        Me.LblVoucherToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVoucherToDate.Location = New System.Drawing.Point(249, 88)
        Me.LblVoucherToDate.Name = "LblVoucherToDate"
        Me.LblVoucherToDate.Size = New System.Drawing.Size(50, 16)
        Me.LblVoucherToDate.TabIndex = 215
        Me.LblVoucherToDate.Text = "To Date"
        '
        'dtpVoucherFromDate
        '
        Me.dtpVoucherFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpVoucherFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpVoucherFromDate.Location = New System.Drawing.Point(128, 86)
        Me.dtpVoucherFromDate.Name = "dtpVoucherFromDate"
        Me.dtpVoucherFromDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpVoucherFromDate.TabIndex = 214
        '
        'LblFromPeriod
        '
        Me.LblFromPeriod.BackColor = System.Drawing.Color.Transparent
        Me.LblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromPeriod.Location = New System.Drawing.Point(10, 88)
        Me.LblFromPeriod.Name = "LblFromPeriod"
        Me.LblFromPeriod.Size = New System.Drawing.Size(110, 16)
        Me.LblFromPeriod.TabIndex = 209
        Me.LblFromPeriod.Text = "Voucher From Date"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(429, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 212
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(10, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(110, 16)
        Me.lblEmployee.TabIndex = 210
        Me.lblEmployee.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(128, 32)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(295, 21)
        Me.cboEmployee.TabIndex = 211
        '
        'frmPayementVoucherReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(645, 474)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmPayementVoucherReport"
        Me.Text = ""
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Public WithEvents txtAdvanceVoucherNo As System.Windows.Forms.TextBox
    Private WithEvents LblAdvanceVoucherNo As System.Windows.Forms.Label
    Friend WithEvents dtpVoucherToDate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblVoucherToDate As System.Windows.Forms.Label
    Friend WithEvents dtpVoucherFromDate As System.Windows.Forms.DateTimePicker
    Private WithEvents LblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
End Class

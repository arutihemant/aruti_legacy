'************************************************************************************************************************************
'Class Name : clsEmployeeWithoutED.vb
'Purpose    :
'Date       :06/04/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmployeeWithoutED
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeWithoutED"
    Private mstrReportId As String = enArutiReport.EmployeeWithoutED
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintTranHeadUnkId As Integer = 0
    Private mstrTranHeadName As String = ""

    Private mintEmployeeUnkId As Integer = 0
    Private mstrEmployeeName As String = ""

    Private mblnIsActive As Boolean = True

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End

    'Sohail (08 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    'Sohail (08 May 2012) -- End

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Properties "
    Public WriteOnly Property _TranHeadUnkId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadUnkId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadName() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property


    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property


    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End

    'Sohail (08 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property
    'Sohail (08 May 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintTranHeadUnkId = 0
            mstrTranHeadName = ""
            mintEmployeeUnkId = 0
            mstrEmployeeName = ""

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintTranHeadUnkId > 0 Then
                objDataOperation.AddParameter("@TranHeadUnkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadUnkId)

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Transaction Head : ") & " " & mstrTranHeadName & " "
            End If

            'Sohail (08 May 2012) -- Start
            'TRA - ENHANCEMENT
            If mintPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, " Period : ") & " " & mstrPeriodName & " "
            End If
            'Sohail (08 May 2012) -- End

            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= "AND hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Employee : ") & " " & mstrEmployeeName & " "
            End If

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (20 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Order By :") & " " & Me.OrderByDisplay
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objUser As New clsUserAddEdit

        Try
            objUser._Userunkid = xUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objUser._Username)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CONDITION; Module Name: " & mstrModuleName)
        Finally
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("deptname", Language.getMessage(mstrModuleName, 6, "Department")))
            iColumn_DetailReport.Add(New IColumn("jobname", Language.getMessage(mstrModuleName, 7, "Job Title")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strUserName As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, strDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            'Sohail (08 May 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = mintPeriodId
            objPeriod._Periodunkid(strDatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End
            Dim strEndDate As String = eZeeDate.convertDate(objPeriod._End_Date)
            objPeriod = Nothing
            'Sohail (08 May 2012) -- End

            StrQ = "SELECT  hremployee_master.employeeunkid " & _
                          ", hremployee_master.employeecode "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", hremployee_master.surname + ' ' + hremployee_master.firstname + ' ' + hremployee_master.othername AS employeename "
            Else
                StrQ &= ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename "
            End If

            StrQ &= ", ISNULL(a.name, '') AS deptname " & _
                          ", ISNULL(j.job_name, '') AS jobname " & _
                          ", ISNULL(prearningdeduction_master.periodunkid, -1) AS EDPeriodUnkId " & _
                          ", " & strEndDate & " AS end_date " 'Sohail (08 May 2012) - [EDPeriodUnkId,end_date]
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM    hremployee_master " & _
                            "LEFT JOIN prearningdeduction_master ON hremployee_master.employeeunkid = prearningdeduction_master.employeeunkid " & _
                                                                 "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
                                                                 "AND prearningdeduction_master.tranheadunkid = @tranheadunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS Job ON Job.employeeunkid = hremployee_master.employeeunkid AND Job.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "        LEFT JOIN hrdepartment_master AS a ON a.departmentunkid =  T.departmentunkid " & _
                            "LEFT JOIN hrjob_master AS j ON j.jobunkid = Job.jobunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prearningdeduction_master.periodunkid " & _
                                                                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " AND cfcommon_period_tran.isactive = 1 " 'Sohail (08 May 2012) - [LEFT JOIN cfcommon_period_tran]
            StrQ &= mstrAnalysis_Join
            'Sohail (08 May 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= "WHERE   prearningdeduction_master.edunkid IS NULL "
            StrQ &= "WHERE   CONVERT(CHAR(8),ISNULL(cfcommon_period_tran.end_date, '" & strEndDate & "'),112) <= '" & strEndDate & "' "
            'Sohail (08 May 2012) -- End


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End


            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (20 Apr 2012) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            'Sohail (08 May 2012) -- Start
            'TRA - ENHANCEMENT
            'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

            '    Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

            '    rpt_Rows.Item("Column1") = dtRow.Item("Id").ToString
            '    rpt_Rows.Item("Column2") = dtRow.Item("GName").ToString
            '    rpt_Rows.Item("Column3") = dtRow.Item("employeeunkid").ToString
            '    rpt_Rows.Item("Column4") = dtRow.Item("employeecode").ToString
            '    rpt_Rows.Item("Column5") = dtRow.Item("employeename").ToString
            '    rpt_Rows.Item("Column6") = dtRow.Item("deptname").ToString
            '    rpt_Rows.Item("Column7") = dtRow.Item("jobname").ToString


            '    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            'Next
            Dim objED As New clsEarningDeduction
            Dim dtEmp As DataTable = New DataView(dsList.Tables("DataTable")).ToTable(True, "employeeunkid", "employeecode", "employeename", "deptname", "jobname", "Id", "GName", "end_date")
            Dim dRow As DataRow()
            Dim intEdPeriodId As Integer

            For Each dtRow As DataRow In dtEmp.Rows

                intEdPeriodId = objED.GetCurrentSlabEDPeriodUnkID(CInt(dtRow.Item("employeeunkid")), eZeeDate.convertDate(dtRow.Item("end_date").ToString))

                dRow = dsList.Tables("DataTable").Select("employeeunkid = " & CInt(dtRow.Item("employeeunkid")) & " AND EDPeriodUnkId = " & intEdPeriodId & " ")

                If dRow.Length <= 0 Then

                    Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("Id").ToString
                    rpt_Rows.Item("Column2") = dtRow.Item("GName").ToString
                    rpt_Rows.Item("Column3") = dtRow.Item("employeeunkid").ToString
                    rpt_Rows.Item("Column4") = dtRow.Item("employeecode").ToString
                    rpt_Rows.Item("Column5") = dtRow.Item("employeename").ToString
                    rpt_Rows.Item("Column6") = dtRow.Item("deptname").ToString
                    rpt_Rows.Item("Column7") = dtRow.Item("jobname").ToString


                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                End If
            Next
            'Sohail (08 May 2012) -- End

            objRpt = New ArutiReport.Designer.rptEmployeeWithoutED

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 8, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 9, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 10, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 11, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If



            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            If mintViewIndex > 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
            End If
            Call ReportFunction.TextChange(objRpt, "txtEmpNo", Language.getMessage(mstrModuleName, 5, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 6, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 7, "Job Title"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, " Transaction Head :")
            Language.setMessage(mstrModuleName, 2, " Employee :")
            Language.setMessage(mstrModuleName, 3, "Order By :")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "Department")
            Language.setMessage(mstrModuleName, 7, "Job Title")
            Language.setMessage(mstrModuleName, 8, "Prepared By :")
            Language.setMessage(mstrModuleName, 9, "Checked By :")
            Language.setMessage(mstrModuleName, 10, "Approved By :")
            Language.setMessage(mstrModuleName, 11, "Received By :")
            Language.setMessage(mstrModuleName, 12, " Period :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

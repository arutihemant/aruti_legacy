#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
#End Region


Public Class frmLoanAdvanceSaving

#Region "Private Variables"

    'Anjan [04 June 2014] -- Start
    'Issue : language was not populating in web report.
    'Private ReadOnly mstrModuleName As String = "frmLoanAdvanceSaving"
    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceSaving"
    'Anjan [04 June 2014 ] -- End


    Private objLoan As clsLoanAdvanceSaving

    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mblnIsLoanReportType As Boolean = True
    'Anjan (20 Mar 2012)-End 

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End
    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mdec_ExRate As Decimal = 1
    Private mintPaidCurrencyId As Integer = 0
    'Sohail (07 May 2015) -- End
    Private dvScheme As DataView 'Varsha (06-Oct-2017)
    Private mstrSearchSchemeText As String = "" 'Varsha (06-Oct-2017)
    Private mintCount As Integer = 0 'Varsha (06-Oct-2017)

#End Region

#Region "Constructor"
    Public Sub New()
        objLoan = New clsLoanAdvanceSaving(User._Object._Languageunkid,Company._Object._Companyunkid)
        objLoan.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End

        'Sohail (11 Feb 2016) -- Start
        'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
        _Show_ExcelExtra_Menu = True
        'Sohail (11 Feb 2016) -- End

    End Sub
#End Region

#Region "Private Function"
    Public Sub FillCombo()
        Try

            Dim objperiod As New clscommom_period_Tran
            Dim objEmp As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objSavingScheme As New clsSavingScheme
            Dim objMaster As New clsMasterData 'Sohail (18 Jan 2016)
            Dim dsList As New DataSet

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            Dim objBranch As New clsStation
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, _
                                               FinancialYear._Object._YearUnkid, _
                                               FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, _
                                               "Period", True)
            'Nilay (10-Oct-2015) -- End

            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objperiod = Nothing
            dsList = Nothing

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsList = objEmp.GetEmployeeList("Employee", True, True)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Employee", True, False)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, True, "Employee", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End

            'Pinkal (24-Jun-2011) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing


            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            objBranch = Nothing
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            dsList = Nothing


            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Loan Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Advance Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Saving Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Loan Register Report")) 'Sohail (26 Aug 2013)


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Loan Scheme With Bank Account Report"))
                'Pinkal (12-Jun-2014) -- End

                'Shani(07-May-2015) -- Start
                'Enhancement - New Loan History Report to show only loan date wise transactions like interest rate changed, 
                'emi changed, top up given and receipt taken.
                .Items.Add(Language.getMessage(mstrModuleName, 12, "Loan History Report"))
                'Shani(07-May-2015) -- End

                'Sohail (11 Feb 2016) -- Start
                'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
                .Items.Add(Language.getMessage(mstrModuleName, 13, "Loan By product"))
                'Sohail (11 Feb 2016) -- End

                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                .Items.Add(Language.getMessage(mstrModuleName, 14, "Saving By product"))
                'Sohail (04 Mar 2016) -- End

                'Sohail (13 Dec 2016) -- Start
                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
                .Items.Add(Language.getMessage(mstrModuleName, 15, "Loan Repayment Schedule Report"))
                'Sohail (13 Dec 2016) -- End

                'Nilay (08 Apr 2017) -- Start
                'Enhancements: New Loan Tracking Report for PACRA
                .Items.Add(Language.getMessage(mstrModuleName, 16, "Loan Tracking Report"))
                'Nilay (08 Apr 2017) -- End

            End With


            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            Dim objBank As New clspayrollgroup_master
            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Pinkal (12-Jun-2014) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency")
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .SelectedValue = 0
            End With


            Dim dtTable As DataTable = New DataView(dsList.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If
            'Sohail (07 May 2015) -- End

            'Sohail (18 Jan 2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'dsList = objMaster.GetLoan_Saving_Status("Status")
            dsList = objMaster.GetLoan_Saving_Status("Status", , True)
            'Sohail (29 Jan 2016) -- End
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
                'Sohail (29 Jan 2016) -- Start
                'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
                '.SelectedValue = 0
                .SelectedValue = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED
                'Sohail (29 Jan 2016) -- End
            End With
            'Sohail (18 Jan 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboEmployee.SelectedValue = 0

            cboPeriod.SelectedValue = 0
            cboScheme.SelectedValue = 0

            objLoan.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objLoan.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            cboBranch.SelectedValue = 0
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (11 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            chkShowInterestInfo.Checked = False
            'Sohail (11 Apr 2012) -- End

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            chkIgnoreZeroSavingDeduction.Checked = False
            'Sohail (10 Aug 2012) -- End

            chkShowLoanReceipt.Checked = False 'Sohail (09 Oct 2012)

            'Sohail (29 Jun 2015) -- Start
            'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            chkShowOnHold.Checked = False
            'Sohail (29 Jun 2015) -- End

            'Sohail (17 Feb 2017) -- Start
            'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
            chkShowPrincipleBFCF.Checked = False
            'Sohail (17 Feb 2017) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End

            'Sohail (18 Jan 2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            'cboStatus.SelectedValue = 0
            'Sohail (11 Feb 2016) -- Start
            'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
            If cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanReport OrElse cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanByproduct Then 'Loan Report, Loan Byproduct Report
                cboStatus.SelectedValue = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED
            Else
                cboStatus.SelectedValue = 0
            End If

            Select Case ConfigParameter._Object._LogoOnPayslip
                Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                    radLogoCompanyInfo.Checked = True
                Case enLogoOnPayslip.COMPANY_DETAILS
                    radCompanyDetails.Checked = True
                Case enLogoOnPayslip.LOGO
                    radLogo.Checked = True
            End Select
            'Sohail (11 Feb 2016) -- End
            'Sohail (18 Jan 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (26 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If (cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 2) AndAlso lvSchemes.CheckedItems.Count <= 0 Then '0=Loan or 2=Saving

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

            'Sohail (11 Feb 2016) -- Start
            'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
            'If (cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 2 OrElse cboReportType.SelectedIndex = 3 OrElse cboReportType.SelectedIndex = 4 OrElse cboReportType.SelectedIndex = 5) AndAlso lvSchemes.CheckedItems.Count <= 0 Then '0=Loan or 2=Saving or 3=LoanRegister 4 = Loan Scheme with Bank Account 
            'Sohail (04 Mar 2016) -- Start
            'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            'If (cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 2 OrElse cboReportType.SelectedIndex = 3 OrElse cboReportType.SelectedIndex = 4 OrElse cboReportType.SelectedIndex = 5 OrElse cboReportType.SelectedIndex = 6) AndAlso lvSchemes.CheckedItems.Count <= 0 Then '0=Loan or 2=Saving or 3=LoanRegister 4 = Loan Scheme with Bank Account , 5 = Loan History, 6 = Loan Byproduct
            'Sohail (13 Dec 2016) -- Start
            'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
            'If (cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 2 OrElse cboReportType.SelectedIndex = 3 OrElse cboReportType.SelectedIndex = 4 OrElse cboReportType.SelectedIndex = 5 OrElse cboReportType.SelectedIndex = 6 OrElse cboReportType.SelectedIndex = 7) AndAlso lvSchemes.CheckedItems.Count <= 0 Then '0=Loan or 2=Saving or 3=LoanRegister 4 = Loan Scheme with Bank Account , 5 = Loan History, 6 = Loan Byproduct
            If (cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanByproduct OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingByproduct OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanTrackingReport) AndAlso _
                dvScheme.Table.Select("IsChecked = True").Length <= 0 Then 'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'lvSchemes.CheckedItems.Count <= 0 Then '0=Loan or 2=Saving or 3=LoanRegister 4 = Loan Scheme with Bank Account , 5 = Loan History, 6 = Loan Byproduct


                'Varsha Rana (06-Oct-2017) -- End

                'Nilay (08 Apr 2017) -- [cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanTrackingReport]
                'Sohail (13 Dec 2016) -- End
                'Sohail (04 Mar 2016) -- End
                'Sohail (11 Feb 2016) -- End
                'Pinkal (12-Jun-2014) -- End
                'Sohail (26 Aug 2013) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select atleast One Scheme from the list."), enMsgBoxStyle.Information)
                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'lvSchemes.Focus()
                dgScheme.Focus()
                'Varsha Rana (06-Oct-2017) -- End

                Return False
            End If
            'Sohail (10 Aug 2012) -- End

            objLoan.SetDefaultValue()

            objLoan._PeriodId = cboPeriod.SelectedValue
            objLoan._PeriodName = cboPeriod.Text

            objLoan._SchemeId = cboScheme.SelectedValue
            objLoan._SchemeName = cboScheme.Text

            objLoan._EmployeeId = cboEmployee.SelectedValue
            objLoan._EmployeeName = cboEmployee.Text

            objLoan._ReportId = cboReportType.SelectedIndex
            objLoan._ReportTypeName = cboReportType.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objLoan._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            objLoan._BranchId = cboBranch.SelectedValue
            objLoan._Branch_Name = cboBranch.Text
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            objLoan._ShowInterestInformation = chkShowInterestInfo.Checked 'Sohail (11 Apr 2012)
            objLoan._ShowLoanReceipt = chkShowLoanReceipt.Checked  'Sohail (09 Oct 2012)

            'Sohail (10 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            objLoan._IgnoreZeroSavingDeduction = chkIgnoreZeroSavingDeduction.Checked

            'Sohail (29 Jun 2015) -- Start
            'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
            objLoan._ShowOnHold = chkShowOnHold.Checked
            'Sohail (29 Jun 2015) -- End
            'Sohail (17 Feb 2017) -- Start
            'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
            objLoan._ShowPrincipleBFCF = chkShowPrincipleBFCF.Checked
            'Sohail (17 Feb 2017) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objLoan._BaseCurrencySign = mstrBaseCurrSign
            objLoan._CurrencyId = CInt(cboCurrency.SelectedValue)
            objLoan._CurrencyName = cboCurrency.Text
            'Sohail (07 May 2015) -- End

            Dim strIDs As String = ""
            Dim strName As String = "" 'Sohail (26 Aug 2013)
            'Sohail (11 Feb 2016) -- Start
            'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
            'For Each lvItem As ListViewItem In lvSchemes.CheckedItems
            '    If strIDs = "" Then
            '        strIDs = lvItem.Tag.ToString
            '        strName = lvItem.SubItems(colhName.Index).Text 'Sohail (26 Aug 2013)
            '    Else
            '        strIDs &= ", " & lvItem.Tag.ToString
            '        strName &= ", " & lvItem.SubItems(colhName.Index).Text 'Sohail (26 Aug 2013)
            '    End If
            'Next

            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'strIDs = String.Join(",", (From p In lvSchemes.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
            'strName = String.Join(",", (From p In lvSchemes.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhName.Index).Text)).ToArray)
            If (cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRegisterReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanHistoryReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanByproduct OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport OrElse _
                cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanTrackingReport) Then

                strIDs = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanschemeunkid").ToString)).ToArray)
                strName = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("name").ToString)).ToArray)
            ElseIf (cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingReport OrElse _
                  cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingByproduct) Then

                strIDs = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("savingschemeunkid").ToString)).ToArray)
                strName = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("name").ToString)).ToArray)

            End If
            'Varsha Rana (06-Oct-2017) -- End

            'Sohail (11 Feb 2016) -- End
            objLoan._SchemeListIDs = strIDs
            objLoan._SchemeName = strName 'Sohail (26 Aug 2013)
            'Sohail (10 Aug 2012) -- End

            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'objLoan._SchemeListCode = String.Join(",", (From p In lvSchemes.CheckedItems.Cast(Of ListViewItem)() Select (p.Text.ToString)).ToArray) 'Sohail (11 Feb 2016)
            objLoan._SchemeListCode = String.Join(",", (From p In dvScheme.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("Code").ToString)).ToArray)
            'Varsha Rana (06-Oct-2017) -- End



            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                objLoan._PeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                objLoan._PeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Else
                Dim objPeriod As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Nilay (10-Oct-2015) -- End

                objLoan._PeriodStartDate = objPeriod._Start_Date
                objLoan._PeriodEndDate = objPeriod._End_Date
                objPeriod = Nothing
            End If
            'Sohail (17 Apr 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objLoan._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            'Sohail (11 Feb 2016) -- Start
            'Enhancement - 3 Logo options on Loan Advance Saving Report for KBC.
            objLoan._IsLogoCompanyInfo = radLogoCompanyInfo.Checked
            objLoan._IsOnlyLogo = radLogo.Checked
            objLoan._IsOnlyCompanyInfo = radCompanyDetails.Checked
            objLoan._PayslipTemplate = ConfigParameter._Object._PayslipTemplate
            'Sohail (11 Feb 2016) -- End

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            objLoan._BankGroupId = CInt(cboBankName.SelectedValue)
            objLoan._BankGroup = cboBankName.Text
            objLoan._BankBranchId = CInt(cboBankBranch.SelectedValue)
            objLoan._BankBranch = cboBankBranch.Text
            'Pinkal (12-Jun-2014) -- End

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objLoan._CurrentDateTime = ConfigParameter._Object._CurrentDateAndTime.Date
            'Nilay (10-Oct-2015) -- End

            'Sohail (18 Jan 2016) -- Start
            'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
            objLoan._LoanStatusId = CInt(cboStatus.SelectedValue)
            objLoan._LoanStatusName = cboStatus.Text
            'Sohail (18 Jan 2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    'Sohail (10 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    Public Sub FillLoanScheme()
        Dim objLoanScheme As New clsLoan_Scheme
        Dim dsList As DataSet
        Try

            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'lvSchemes.Items.Clear()
            'Dim lvItem As ListViewItem
            dgScheme.DataSource = Nothing
            RemoveHandler txtSearchScheme.TextChanged, AddressOf txtSearchScheme_TextChanged
            Call SetDefaultSearchSchemeText()
            AddHandler txtSearchScheme.TextChanged, AddressOf txtSearchScheme_TextChanged
            'Varsha Rana (06-Oct-2017) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(False, "Loan")
            dsList = objLoanScheme.getComboList(False, "Loan", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'For Each dsRow As DataRow In dsList.Tables("Loan").Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = dsRow.Item("Code").ToString
            '    lvItem.Tag = CInt(dsRow.Item("loanschemeunkid"))

            '    lvItem.SubItems.Add(dsRow.Item("name").ToString)

            '    RemoveHandler lvSchemes.ItemChecked, AddressOf lvSchemes_ItemChecked
            '    lvSchemes.Items.Add(lvItem)
            '    AddHandler lvSchemes.ItemChecked, AddressOf lvSchemes_ItemChecked
            'Next

            'If lvSchemes.Items.Count > 10 Then
            '    colhName.Width = 155 - 18
            'Else
            '    colhName.Width = 155
            'End If

            Dim dtTable As DataTable = New DataView(dsList.Tables("Loan"), "", "name", DataViewRowState.CurrentRows).ToTable

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvScheme = dtTable.DefaultView
            dgScheme.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhunkid.DataPropertyName = "loanschemeunkid"
            dgColhSchemeCode.DataPropertyName = "Code"
            dgColhScheme.DataPropertyName = "name"

            dgScheme.DataSource = dvScheme
            'dvScheme.Sort = "IsChecked DESC, name "
            dvScheme.Sort = "name "
            'Varsha Rana (06-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanScheme", mstrModuleName)
        Finally
            objLoanScheme = Nothing
        End Try
    End Sub

    Public Sub FillSavingScheme()
        Dim objSavingScheme As New clsSavingScheme
        Dim dsList As DataSet
        Try
            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'lvSchemes.Items.Clear()
            'Dim lvItem As ListViewItem
            dgScheme.DataSource = Nothing
            RemoveHandler txtSearchScheme.TextChanged, AddressOf txtSearchScheme_TextChanged
            Call SetDefaultSearchSchemeText()
            AddHandler txtSearchScheme.TextChanged, AddressOf txtSearchScheme_TextChanged
            'Varsha Rana (06-Oct-2017) -- End
            dsList = objSavingScheme.getComboList(False, "Loan")
            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'For Each dsRow As DataRow In dsList.Tables("Loan").Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = dsRow.Item("Code").ToString
            '    lvItem.Tag = CInt(dsRow.Item("savingschemeunkid"))

            '    lvItem.SubItems.Add(dsRow.Item("name").ToString)

            '    RemoveHandler lvSchemes.ItemChecked, AddressOf lvSchemes_ItemChecked
            '    lvSchemes.Items.Add(lvItem)
            '    AddHandler lvSchemes.ItemChecked, AddressOf lvSchemes_ItemChecked
            'Next

            'If lvSchemes.Items.Count > 10 Then
            '    colhName.Width = 155 - 18
            'Else
            '    colhName.Width = 155
            'End If

            Dim dtTable As DataTable = New DataView(dsList.Tables("Loan"), "", "name", DataViewRowState.CurrentRows).ToTable

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dvScheme = dtTable.DefaultView
            dgScheme.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhunkid.DataPropertyName = "savingschemeunkid"
            dgColhSchemeCode.DataPropertyName = "Code"
            dgColhScheme.DataPropertyName = "name"

            dgScheme.DataSource = dvScheme
            'dvScheme.Sort = "IsChecked DESC, name "
            dvScheme.Sort = "name "
            'Varsha Rana (06-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSavingScheme", mstrModuleName)
        Finally
            objSavingScheme = Nothing
        End Try
    End Sub
    'Sohail (10 Aug 2012) -- End


    'Pinkal (12-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
    Public Function Valiadation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboPeriod.Select()
                mblnFlag = False
            ElseIf CInt(cboBankName.SelectedValue) > 0 AndAlso CInt(cboBankBranch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Bank Branch is compulsory information.Please Select Bank Branch."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                cboBankBranch.Select()
                mblnFlag = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valiadation", mstrModuleName)
        End Try
        Return mblnFlag
    End Function
    'Pinkal (12-Jun-2014) -- End


    'Varsha Rana (06-Oct-2017) -- Start
    'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
    Private Sub SetDefaultSearchSchemeText()
        Try
            mstrSearchSchemeText = lblSearchScheme.Text
            With txtSearchScheme
                .ForeColor = Color.Gray
                .Text = mstrSearchSchemeText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchSchemeText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            Dim blnIsChecked As Boolean = Convert.ToBoolean(dgScheme.CurrentRow.Cells(objdgcolhCheck.Index).Value)

            If blnIsChecked = True Then
                mintCount += 1
            Else
                mintCount -= 1
            End If

            If mintCount <= 0 Then
                objchkSelectAll.CheckState = CheckState.Unchecked
            ElseIf mintCount < dgScheme.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf mintCount = dgScheme.Rows.Count Then
                objchkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (06-Oct-2017) -- End


#End Region

#Region "Form's Events"

    Private Sub frmLoanAdvanceSaving_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLoan = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objLoan._ReportName
            Me._Message = objLoan._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmLoanAdvanceSaving_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"
    Private Sub frmLoanAdvanceSaving_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport Then
                If Valiadation() = False Then Exit Sub
                'Sohail (11 Feb 2016) -- Start
                'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'ElseIf CInt(cboReportType.SelectedIndex) = 6 Then
            ElseIf CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then
                'Sohail (04 Mar 2016) -- End
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                    cboPeriod.Select()
                    Exit Sub
                End If
                'Sohail (11 Feb 2016) -- End
            End If
            'Pinkal (12-Jun-2014) -- End


            If SetFilter() = False Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objLoan.generateReport(0, e.Type, enExportAction.None)
            Dim dtPeriodStartDate As Date
            Dim dtPeriodEndDate As Date
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                dtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Else
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtPeriodStartDate = objPeriod._Start_Date
                dtPeriodEndDate = objPeriod._End_Date
                objPeriod = Nothing
            End If
            objLoan.generatereportnew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      dtPeriodStartDate, _
                                      dtPeriodEndDate, _
                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                      ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport Then
                If Valiadation() = False Then Exit Sub
                'Sohail (11 Feb 2016) -- Start
                'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
                'ElseIf CInt(cboReportType.SelectedIndex) = 6 Then
            ElseIf CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then
                'Sohail (04 Mar 2016) -- End
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly)
                    cboPeriod.Select()
                    Exit Sub
                End If
                'Sohail (11 Feb 2016) -- End
            End If
            'Pinkal (12-Jun-2014) -- End

            If SetFilter() = False Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objLoan.generateReport(0, enPrintAction.None, e.Type)
            Dim dtPeriodStartDate As Date
            Dim dtPeriodEndDate As Date
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                dtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Else
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtPeriodStartDate = objPeriod._Start_Date
                dtPeriodEndDate = objPeriod._End_Date
                objPeriod = Nothing
            End If
            objLoan.generatereportnew(FinancialYear._Object._DatabaseName, _
                                      User._Object._Userunkid, _
                                      FinancialYear._Object._YearUnkid, _
                                      Company._Object._Companyunkid, _
                                      dtPeriodStartDate, _
                                      dtPeriodEndDate, _
                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                      ConfigParameter._Object._ExportReportPath, _
                                      ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanAdvanceSaving_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceSaving_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            'Sohail (08 Nov 2011) -- Start
            'Issue : error when click on cancel button on search employee form
            If frm.DisplayDialog Then
                'If frm.ShowDialog Then
                'Sohail (08 Nov 2011) -- End
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 17 AUG 2011 ] -- END 


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes


    Private Sub frmLoanAdvanceSaving_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoanAdvanceSaving.SetMessages()
            objfrm._Other_ModuleNames = "clsLoanAdvanceSaving"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLoanAdvanceSaving_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End



    'Pinkal (12-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

    Private Sub objbtnSearchBankName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankName.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboBankName.DataSource
            frm.DisplayMember = cboBankName.DisplayMember
            frm.ValueMember = cboBankName.ValueMember
            If frm.DisplayDialog Then
                cboBankName.SelectedValue = frm.SelectedValue
                cboBankName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankName_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchBankBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankBranch.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboBankBranch.DataSource
            frm.DisplayMember = cboBankBranch.DisplayMember
            frm.ValueMember = cboBankBranch.ValueMember
            If frm.DisplayDialog Then
                cboBankBranch.SelectedValue = frm.SelectedValue
                cboBankBranch.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankBranch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (12-Jun-2014) -- End



#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objLoan.setOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objLoan.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged

        'Vimal (21 Dec 2010) -- Start 
        'Try
        '    If cboReportType.SelectedIndex = 0 Then 'Loan 
        '        lblScheme.Location = New Point(226, 64)
        '        cboScheme.Location = New Point(313, 61)

        '        lblPeriod.Visible = True
        '        cboPeriod.Visible = True

        '        cboReportType.DataSource = Nothing
        '        lblScheme.Visible = True
        '        cboScheme.Visible = True
        '        lblScheme.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")

        '        cboScheme.DataSource = (New clsLoan_Scheme).getComboList(True, "Loan", ).Tables(0)
        '        cboScheme.ValueMember = "loanschemeunkid"
        '        cboScheme.DisplayMember = "name"

        '    ElseIf cboReportType.SelectedIndex = 1 Then 'Advance
        '        lblScheme.Location = New Point(226, 64)
        '        cboScheme.Location = New Point(313, 61)

        '        lblPeriod.Visible = True
        '        cboPeriod.Visible = True

        '        cboScheme.DataSource = Nothing
        '        lblScheme.Visible = False
        '        cboScheme.Visible = False
        '        lblScheme.Text = Language.getMessage(mstrModuleName, 5, " ")

        '    ElseIf cboReportType.SelectedIndex = 2 Then 'Saving
        '        lblScheme.Location = New Point(8, 64)
        '        cboScheme.Location = New Point(98, 61)

        '        lblPeriod.Visible = False
        '        cboPeriod.Visible = False

        '        lblScheme.Visible = True
        '        cboScheme.Visible = True
        '        lblScheme.Text = Language.getMessage(mstrModuleName, 6, "Saving Scheme")

        '        cboScheme.DataSource = (New clsSavingScheme).getComboList(True, "Saving", ).Tables(0)
        '        cboScheme.ValueMember = "savingschemeunkid"
        '        cboScheme.DisplayMember = "name"


        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        'End Try

        'Sohail (29 Jun 2015) -- Start
        'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
        chkShowOnHold.Checked = False
        'Sohail (29 Jun 2015) -- End
        'Sohail (17 Feb 2017) -- Start
        'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
        chkShowPrincipleBFCF.Checked = False
        chkShowPrincipleBFCF.Visible = False
        'Sohail (17 Feb 2017) -- End

        'Sohail (07 May 2015) -- Start
        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
        cboCurrency.SelectedIndex = -1
        cboCurrency.Visible = False
        lblCurrency.Visible = False
        'Sohail (07 May 2015) -- End

        'Shani(07-May-2015) -- Start
        'Enhancement - New Loan History Report to show only loan date wise transactions like interest rate changed, 
        'emi changed, top up given and receipt taken.
        'chkSelectallSchemeList.Visible = True
        chkIgnoreZeroSavingDeduction.Visible = True
        chkShowInterestInfo.Visible = True
        chkShowOnHold.Visible = True
        chkShowLoanReceipt.Visible = True

        cboPeriod.Enabled = True
        lblBranch.Visible = True
        cboBranch.Visible = True
        'Shani(07-May-2015) -- End

        'Sohail (18 Jan 2016) -- Start
        'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
        cboStatus.SelectedValue = 0
        cboStatus.Visible = False
        lblStatus.Visible = False
        'Sohail (18 Jan 2016) -- End

        'Sohail (13 Dec 2016) -- Start
        'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
        cboPeriod.Enabled = True
        'Sohail (13 Dec 2016) -- End

        Try
            If cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanReport Then 'Loan 
                cboReportType.DataSource = Nothing
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'lblScheme.Visible = True
                'cboScheme.Visible = True
                'lblScheme.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")

                'cboScheme.DataSource = (New clsLoan_Scheme).getComboList(True, "Loan", ).Tables(0)
                'cboScheme.ValueMember = "loanschemeunkid"
                'cboScheme.DisplayMember = "name"
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()
                'Sohail (10 Aug 2012) -- End

                'Anjan (20 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'objbtnReportTypeSearch.Visible = True
                objbtnReportTypeSearch.Visible = False
                'Sohail (10 Aug 2012) -- End
                mblnIsLoanReportType = True
                'Anjan (20 Mar 2012)-End 

                chkShowInterestInfo.Visible = True 'Sohail (11 Apr 2012)
                'Sohail (09 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                chkShowLoanReceipt.Location = chkIgnoreZeroSavingDeduction.Location
                chkShowLoanReceipt.Visible = True
                'Sohail (09 Oct 2012) -- End

                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False
                'Sohail (10 Aug 2012) -- End

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                chkShowOnHold.Visible = True
                'Sohail (29 Jun 2015) -- End


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End


                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Sohail (07 May 2015) -- End

                'Sohail (18 Jan 2016) -- Start
                'Enhancement - Loan Status filter on Loan Advance Saving Report in 58.1.
                cboStatus.Visible = True
                lblStatus.Visible = True
                'Sohail (18 Jan 2016) -- End

                'Sohail (11 Feb 2016) -- Start
                'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
                cboStatus.SelectedValue = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED
                'Sohail (11 Feb 2016) -- End

            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.AdvanceReport Then 'Advance
                cboScheme.DataSource = Nothing
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'lblScheme.Visible = False
                'cboScheme.Visible = False
                'lblScheme.Text = Language.getMessage(mstrModuleName, 5, " ")
                lblSchemes.Text = Language.getMessage(mstrModuleName, 5, " ")
                'Sohail (10 Aug 2012) -- End

                'Anjan (20 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                objbtnReportTypeSearch.Visible = False
                'Anjan (20 Mar 2012)-End 

                'Sohail (11 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False
                'Sohail (11 Apr 2012) -- End
                'Sohail (09 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False
                'Sohail (09 Oct 2012) -- End

                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False
                gbFilterCriteriaScheme.Enabled = False
                txtSearchScheme.Text = ""
                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'lvSchemes.Items.Clear()
                dgScheme.DataSource = Nothing
                'Varsha Rana (06-Oct-2017) -- End
                'Sohail (10 Aug 2012) -- End

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                chkShowOnHold.Visible = False
                'Sohail (29 Jun 2015) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End


                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankName.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Sohail (07 May 2015) -- End

            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingReport Then 'Saving
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'lblScheme.Visible = True
                'cboScheme.Visible = True
                'lblScheme.Text = Language.getMessage(mstrModuleName, 6, "Saving Scheme")

                'cboScheme.DataSource = (New clsSavingScheme).getComboList(True, "Saving", ).Tables(0)
                'cboScheme.ValueMember = "savingschemeunkid"
                'cboScheme.DisplayMember = "name"
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 6, "Saving Scheme")
                Call FillSavingScheme()
                'Sohail (10 Aug 2012) -- End

                'Anjan (20 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                'objbtnReportTypeSearch.Visible = True
                objbtnReportTypeSearch.Visible = False
                'Sohail (10 Aug 2012) -- End
                mblnIsLoanReportType = False
                'Anjan (20 Mar 2012)-End 

                chkShowInterestInfo.Visible = True 'Sohail (11 Apr 2012)
                'Sohail (09 Oct 2012) -- Start
                'TRA - ENHANCEMENT
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False
                'Sohail (09 Oct 2012) -- End

                'Sohail (10 Aug 2012) -- Start
                'TRA - ENHANCEMENT
                chkIgnoreZeroSavingDeduction.Visible = True
                'Sohail (10 Aug 2012) -- End

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                chkShowOnHold.Visible = False
                'Sohail (29 Jun 2015) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End

                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankBranch.SelectedIndex = 0
                cboBankName.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End



                'Sohail (26 Aug 2013) -- Start
                'TRA - ENHANCEMENT
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRegisterReport Then 'Loan Register
                cboReportType.DataSource = Nothing

                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                objbtnReportTypeSearch.Visible = False
                mblnIsLoanReportType = True

                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False

                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False

                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False

                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'chkShowOnHold.Visible = False
                chkShowOnHold.Visible = True
                'Sohail (07 May 2015) -- End
                'Sohail (29 Jun 2015) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End
                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Sohail (07 May 2015) -- End
                'Nilay (18-Feb-2016) -- Start
                lblStatus.Visible = True
                cboStatus.Visible = True
                'Nilay (18-Feb-2016) -- End

            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport Then 'Loan Scheme With Bank Account Report

                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = False
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = False
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End
                lblBankName.Visible = True
                cboBankName.Visible = True
                objbtnSearchBankName.Visible = True
                LblBankBranch.Visible = True
                cboBankBranch.Visible = True
                objbtnSearchBankBranch.Visible = True
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0
                'Pinkal (12-Jun-2014) -- End
                'Sohail (29 Jun 2015) -- Start
                'Enhancement - Show Loan Current Balance, Paid Loan and Total for each column on Loan Advance List screen and Show On Hold transaction on Loan Report.
                chkShowOnHold.Visible = False
                'Sohail (29 Jun 2015) -- End

                'Nilay (18-Feb-2016) -- Start
                lblStatus.Visible = True
                cboStatus.Visible = True
                'Nilay (18-Feb-2016) -- End

                'Nilay (23-Feb-2016) -- Start
                'Enhancement - Add Excel Advance
                chkShowInterestInfo.Visible = False
                chkShowLoanReceipt.Visible = False
                chkIgnoreZeroSavingDeduction.Visible = False
                'Nilay (23-Feb-2016) -- End

                'Shani(07-May-2015) -- Start
                'Enhancement - New Loan History Report to show only loan date wise transactions like interest rate changed, 
                'emi changed, top up given and receipt taken.
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanHistoryReport Then 'Loan History Report
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Checked = False
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Visible = True
                objchkSelectAll.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Visible = True
                'Varsha Rana (06-Oct-2017) -- End

                chkIgnoreZeroSavingDeduction.Visible = False
                chkShowInterestInfo.Visible = False
                chkShowOnHold.Visible = False
                chkShowLoanReceipt.Visible = False

                cboPeriod.Enabled = False
                cboPeriod.SelectedValue = 0

                cboBranch.Visible = False
                cboBranch.SelectedIndex = 0
                lblBranch.Visible = False

                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False

                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False

                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                'Sohail (07 May 2015) -- End

                'Sohail (11 Feb 2016) -- Start
                'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanByproduct Then 'Loan Byproduct Report
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = False
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Checked = False
                objchkSelectAll.Enabled = False
                'Varsha Rana (06-Oct-2017) -- End
                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True
                cboStatus.Visible = True
                lblStatus.Visible = True
                cboStatus.SelectedValue = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED

                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False

                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False

                chkShowOnHold.Checked = False
                chkShowOnHold.Visible = False

                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False
                'Sohail (11 Feb 2016) -- End

                'Sohail (04 Mar 2016) -- Start
                'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.SavingByproduct Then 'Saving Byproduct
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 6, "Saving Scheme")
                Call FillSavingScheme()

                objbtnReportTypeSearch.Visible = False
                mblnIsLoanReportType = False
                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False
                chkIgnoreZeroSavingDeduction.Visible = True
                chkShowOnHold.Visible = False
                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = False
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Checked = False
                objchkSelectAll.Enabled = False
                'Varsha Rana (06-Oct-2017) -- End
                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankBranch.SelectedIndex = 0
                cboBankName.SelectedIndex = 0

                'Sohail (13 Dec 2016) -- Start
                'Enhancement #10 - 64.1 - Loan Schedule Report for TUJIJENGE TANZANIA LIMITED (New Report type 'Loan Repayment Schedule Report', Both crystal report and Excel Advance, Employee selection optional) .
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanRepaymentScheduleReport Then 'Loan Repayment Schedule Report
                cboReportType.DataSource = Nothing
                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                objbtnReportTypeSearch.Visible = False
                mblnIsLoanReportType = True

                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False
                chkShowOnHold.Visible = False
                chkShowOnHold.Checked = False

                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False


                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End
                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True
                lblCurrency.Visible = True

                cboPeriod.Enabled = False
                cboPeriod.SelectedValue = 0
                'Sohail (13 Dec 2016) - End
                'Sohail (17 Feb 2017) -- Start
                'Tujijenge Enhancement - 65.1 - add Sr No column in loan repayment schedule report.
                chkShowPrincipleBFCF.Visible = True
                'Sohail (17 Feb 2017) -- End

                'Nilay (08 Apr 2017) -- Start
                'Enhancements: New Loan Tracking Report for PACRA
            ElseIf cboReportType.SelectedIndex = enLoanAdvanceSavindReportType.LoanTrackingReport Then

                gbFilterCriteriaScheme.Enabled = True
                lblSchemes.Text = Language.getMessage(mstrModuleName, 4, "Loan Scheme")
                Call FillLoanScheme()

                objbtnReportTypeSearch.Visible = False
                mblnIsLoanReportType = True

                chkShowInterestInfo.Visible = False
                chkShowInterestInfo.Checked = False
                chkShowLoanReceipt.Checked = False
                chkShowLoanReceipt.Visible = False

                chkShowOnHold.Visible = True
                chkShowOnHold.Checked = False

                chkIgnoreZeroSavingDeduction.Checked = False
                chkIgnoreZeroSavingDeduction.Visible = False

                'Varsha Rana (06-Oct-2017) -- Start
                'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
                'chkSelectallSchemeList.Enabled = True
                'chkSelectallSchemeList.Checked = False
                objchkSelectAll.Enabled = True
                objchkSelectAll.Checked = False
                'Varsha Rana (06-Oct-2017) -- End

                lblBankName.Visible = False
                cboBankName.Visible = False
                objbtnSearchBankName.Visible = False
                LblBankBranch.Visible = False
                cboBankBranch.Visible = False
                objbtnSearchBankBranch.Visible = False
                cboBankName.SelectedIndex = 0
                cboBankBranch.SelectedIndex = 0

                lblCurrency.Visible = True
                cboCurrency.SelectedValue = mintBaseCurrId
                cboCurrency.Visible = True

                lblStatus.Visible = True
                cboStatus.Visible = True
                cboStatus.SelectedValue = enLoanStatus.IN_PROGRESS_WITH_LOAN_DEDUCTED

                cboPeriod.Enabled = True
                cboPeriod.SelectedValue = 0

                chkShowPrincipleBFCF.Visible = False
                'Nilay (08 Apr 2017) -- End
            End If

            objLoan.setDefaultOrderBy(cboReportType.SelectedIndex)
            txtOrderBy.Text = objLoan.OrderByDisplay

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
        'Vimal (21 Dec 2010) -- End



    End Sub

    'Pinkal (12-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]

    Private Sub cboBankName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        Try
            If cboBankName.DataSource IsNot Nothing Then
                Dim objBankBranch As New clsbankbranch_master
                Dim dsList As DataSet = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
                With cboBankBranch
                    .ValueMember = "branchunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankName_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (06-Oct-2017) -- Start
    'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 

    'Private Sub lvSchemes_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
    '    Try
    '        'Sohail (11 Feb 2016) -- Start
    '        'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
    '        'If CInt(cboReportType.SelectedIndex) = 4 Then  'Loan Scheme With Bank Account Report
    '        'Sohail (04 Mar 2016) -- Start
    '        'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
    '        'If CInt(cboReportType.SelectedIndex) = 4 OrElse CInt(cboReportType.SelectedIndex) = 6 Then  'Loan Scheme With Bank Account Report, Loan Byproduct
    '        If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then  'Loan Scheme With Bank Account Report, Loan Byproduct
    '            'Sohail (04 Mar 2016) -- End
    '            'Sohail (11 Feb 2016) -- End
    '            If e.NewValue = CheckState.Checked Then


    '                For Each lvItem As ListViewItem In lvSchemes.CheckedItems
    '                    lvItem.Checked = False
    '                Next



    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSchemes_ItemCheck", mstrModuleName)
    '    End Try
    'End Sub
    'Varsha Rana (06-Oct-2017) -- End


    'Pinkal (12-Jun-2014) -- End


    'Sohail (10 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Varsha Rana (06-Oct-2017) -- Start
    'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
    'Private Sub lvSchemes_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '    Try


    '        'Pinkal (12-Jun-2014) -- Start
    '        'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
    '        'Sohail (11 Feb 2016) -- Start
    '        'Enhancement - New Report Type Loan Byproduct in Loan Advance Saving Report in 58.1 for KBC.
    '        'If CInt(cboReportType.SelectedIndex) = 4 Then 'Loan Scheme With Bank Account Report   
    '        'Sohail (04 Mar 2016) -- Start
    '        'Enhancement - New Report type Saving Byproduct in Loan Advance Saving Report with Advance Excel in Desktop and Self Service as well in 58.1 SP.
    '        'If CInt(cboReportType.SelectedIndex) = 4 OrElse CInt(cboReportType.SelectedIndex) = 6 Then 'Loan Scheme With Bank Account Report, Loan Byproduct
    '        If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then 'Loan Scheme With Bank Account Report, Loan Byproduct
    '            'Sohail (04 Mar 2016) -- End
    '            'Sohail (11 Feb 2016) -- End
    '            Exit Sub
    '        End If
    '        'Pinkal (12-Jun-2014) -- End


    '        If lvSchemes.CheckedItems.Count <= 0 Then
    '            RemoveHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '            chkSelectallSchemeList.CheckState = CheckState.Unchecked
    '            AddHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '        ElseIf lvSchemes.CheckedItems.Count < lvSchemes.Items.Count Then
    '            RemoveHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '            chkSelectallSchemeList.CheckState = CheckState.Indeterminate
    '            AddHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '        ElseIf lvSchemes.CheckedItems.Count = lvSchemes.Items.Count Then
    '            RemoveHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '            chkSelectallSchemeList.CheckState = CheckState.Checked
    '            AddHandler chkSelectallSchemeList.CheckedChanged, AddressOf chkSelectallSchemeList_CheckedChanged
    '        End If

    '        'Pinkal (12-Jun-2014) -- End
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvSchemes_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub


    'Private Sub chkSelectallSchemeList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectallSchemeList.CheckedChanged
    '    Try
    '        Call CheckAllScheme(chkSelectallSchemeList.Checked)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "chkSelectallSchemeList_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Varsha Rana (06-Oct-2017) -- End

    Private Sub txtSearchScheme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Varsha Rana (06-Oct-2017) -- Start
            'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
            'If lvSchemes.Items.Count <= 0 Then Exit Sub

            'lvSchemes.SelectedIndices.Clear()
            'Dim lvFoundItem As ListViewItem = lvSchemes.FindItemWithText(txtSearchScheme.Text, True, 0, True)
            'If lvFoundItem IsNot Nothing Then
            '    lvSchemes.TopItem = lvFoundItem
            '    lvFoundItem.Selected = True
            'End If
            If txtSearchScheme.Text.Trim = mstrSearchSchemeText Then Exit Sub
            If dvScheme IsNot Nothing Then
                dvScheme.RowFilter = "Code LIKE '%" & txtSearchScheme.Text.Replace("'", "''") & "%'  OR name LIKE '%" & txtSearchScheme.Text.Replace("'", "''") & "%'"
                dgScheme.Refresh()
            End If
            'Varsha Rana (06-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Aug 2012) -- End

    Private Sub objbtnReportTypeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReportTypeSearch.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet = Nothing

        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            With cboScheme
                objfrm.DataSource = CType(cboScheme.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReportTypeSearch_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (08 Apr 2017) -- Start
    'Enhancements: New Loan Tracking Report for PACRA
    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) >= 0 Then
                If CInt(cboStatus.SelectedValue) > 0 Then
                    Select Case CInt(cboReportType.SelectedIndex)
                        Case enLoanAdvanceSavindReportType.LoanReport, enLoanAdvanceSavindReportType.LoanTrackingReport
                            If CInt(cboStatus.SelectedValue) = enLoanStatus.ON_HOLD Then
                                chkShowOnHold.Checked = True
                            End If
                    End Select
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (08 Apr 2017) -- End


    'Varsha Rana (06-Oct-2017) -- Start
    'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
    Private Sub CheckAllScheme(ByVal blnCheckAll As Boolean)
        Try

            If dvScheme IsNot Nothing Then
                For Each dr As DataRowView In dvScheme
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvScheme.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllScheme", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchScheme_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchScheme.Leave
        Try
            If txtSearchScheme.Text.Trim = "" Then
                Call SetDefaultSearchSchemeText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchScheme.GotFocus
        Try
            With txtSearchScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchSchemeText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_GotFocus", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (06-Oct-2017) -- End


#End Region

    'Varsha Rana (06-Oct-2017) -- Start
    'Enhancement - Change ListView to GridView Loan/Advance and Saving Report. 
#Region " CheckBox's Events "
    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllScheme(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " GridView Events "

    Private Sub dgScheme_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgScheme.CurrentCellDirtyStateChanged
        Try
            If dgScheme.IsCurrentCellDirty Then
                dgScheme.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CurrentCellDirtyStateChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgScheme_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgScheme.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhCheck.Index Then
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgScheme_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgScheme.CellContentClick, dgScheme.CellContentDoubleClick
        Try

            If CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanSchemeWithBankAccountReport OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.LoanByproduct OrElse CInt(cboReportType.SelectedIndex) = enLoanAdvanceSavindReportType.SavingByproduct Then  'Loan Scheme With Bank Account Report, Loan Byproduct
                'Check the column index and if the check box is checked.
                If e.ColumnIndex = objdgcolhCheck.Index Then
                    Dim isChecked As Boolean = CType(Me.dgScheme(e.ColumnIndex, e.RowIndex).Value, Boolean)
                    If isChecked Then
                        'If check box is checked, uncheck all the rows, the current row would be checked later.
                        Dim intUnkId As Integer = CInt(dgScheme.Rows(e.RowIndex).Cells(objdgcolhunkid.Index).Value)
                        For Each row As DataGridViewRow In Me.dgScheme.Rows
                            If e.RowIndex <> dgScheme.Rows.IndexOf(row) Then
                                row.Cells(e.ColumnIndex).Value = False
                            End If
                        Next
                        'For Each row As DataRow In dvScheme.Table.Rows
                        '    If CInt(row.Item(0)) <> intUnkId Then
                        '        row.Item("IsChecked") = False
                        '    End If

                        'Next
                    End If
                End If
            End If
            Dim mintCount As Integer = dvScheme.ToTable.Select("IsChecked=True").Length

            If mintCount <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf mintCount < dgScheme.Rows.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            ElseIf mintCount = dgScheme.Rows.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgScheme_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region
    'Varsha Rana (06-Oct-2017) -- End


#Region "Messages"
    '1, "Loan Report"
    '2, "Advance Report"
    '3, "Saving Report"
    '4, "Loan Scheme"
    '5, " "
    '6, "Saving Scheme"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteriaScheme.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteriaScheme.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLogoSettings.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLogoSettings.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblScheme.Text = Language._Object.getCaption(Me.lblScheme.Name, Me.lblScheme.Text)
            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.chkShowInterestInfo.Text = Language._Object.getCaption(Me.chkShowInterestInfo.Name, Me.chkShowInterestInfo.Text)
            Me.chkIgnoreZeroSavingDeduction.Text = Language._Object.getCaption(Me.chkIgnoreZeroSavingDeduction.Name, Me.chkIgnoreZeroSavingDeduction.Text)
            Me.gbFilterCriteriaScheme.Text = Language._Object.getCaption(Me.gbFilterCriteriaScheme.Name, Me.gbFilterCriteriaScheme.Text)
            Me.lblSearchScheme.Text = Language._Object.getCaption(Me.lblSearchScheme.Name, Me.lblSearchScheme.Text)
            Me.chkSelectallPeriodList.Text = Language._Object.getCaption(Me.chkSelectallPeriodList.Name, Me.chkSelectallPeriodList.Text)
            Me.lblPeriods.Text = Language._Object.getCaption(Me.lblPeriods.Name, Me.lblPeriods.Text)
            Me.ColumnHeader3.Text = Language._Object.getCaption(CStr(Me.ColumnHeader3.Tag), Me.ColumnHeader3.Text)
            Me.colhStart.Text = Language._Object.getCaption(CStr(Me.colhStart.Tag), Me.colhStart.Text)
            Me.colhEnd.Text = Language._Object.getCaption(CStr(Me.colhEnd.Tag), Me.colhEnd.Text)
            Me.chkSelectallSchemeList.Text = Language._Object.getCaption(Me.chkSelectallSchemeList.Name, Me.chkSelectallSchemeList.Text)
            Me.lblSchemes.Text = Language._Object.getCaption(Me.lblSchemes.Name, Me.lblSchemes.Text)
            Me.chkShowLoanReceipt.Text = Language._Object.getCaption(Me.chkShowLoanReceipt.Name, Me.chkShowLoanReceipt.Text)
            Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
            Me.LblBankBranch.Text = Language._Object.getCaption(Me.LblBankBranch.Name, Me.LblBankBranch.Text)
            Me.chkShowOnHold.Text = Language._Object.getCaption(Me.chkShowOnHold.Name, Me.chkShowOnHold.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.radLogo.Text = Language._Object.getCaption(Me.radLogo.Name, Me.radLogo.Text)
            Me.radCompanyDetails.Text = Language._Object.getCaption(Me.radCompanyDetails.Name, Me.radCompanyDetails.Text)
            Me.radLogoCompanyInfo.Text = Language._Object.getCaption(Me.radLogoCompanyInfo.Name, Me.radLogoCompanyInfo.Text)
            Me.gbLogoSettings.Text = Language._Object.getCaption(Me.gbLogoSettings.Name, Me.gbLogoSettings.Text)
            Me.chkShowPrincipleBFCF.Text = Language._Object.getCaption(Me.chkShowPrincipleBFCF.Name, Me.chkShowPrincipleBFCF.Text)
            Me.dgColhSchemeCode.HeaderText = Language._Object.getCaption(Me.dgColhSchemeCode.Name, Me.dgColhSchemeCode.HeaderText)
            Me.dgColhScheme.HeaderText = Language._Object.getCaption(Me.dgColhScheme.Name, Me.dgColhScheme.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan Report")
            Language.setMessage(mstrModuleName, 2, "Advance Report")
            Language.setMessage(mstrModuleName, 3, "Saving Report")
            Language.setMessage(mstrModuleName, 4, "Loan Scheme")
            Language.setMessage(mstrModuleName, 5, "")
            Language.setMessage(mstrModuleName, 6, "Saving Scheme")
            Language.setMessage(mstrModuleName, 7, "Please select atleast One Scheme from the list.")
            Language.setMessage(mstrModuleName, 8, "Loan Register Report")
            Language.setMessage(mstrModuleName, 9, "Loan Scheme With Bank Account Report")
            Language.setMessage(mstrModuleName, 10, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 11, "Bank Branch is compulsory information.Please Select Bank Branch.")
            Language.setMessage(mstrModuleName, 12, "Loan History Report")
            Language.setMessage(mstrModuleName, 13, "Loan By product")
            Language.setMessage(mstrModuleName, 14, "Saving By product")
            Language.setMessage(mstrModuleName, 15, "Loan Repayment Schedule Report")
            Language.setMessage(mstrModuleName, 16, "Loan Tracking Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

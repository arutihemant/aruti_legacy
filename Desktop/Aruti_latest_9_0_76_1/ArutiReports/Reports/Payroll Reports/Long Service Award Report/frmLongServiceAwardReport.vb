'************************************************************************************************************************************
'Class Name : frmLongServiceAwardReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmLongServiceAwardReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLongServiceAwardReport"
    Private objAgeAnalysis As clsLongServiceAwardReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
#End Region

#Region " Contructor "

    Public Sub New()
        objAgeAnalysis = New clsLongServiceAwardReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAgeAnalysis.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objHead As New clsTransactionHead
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objHead.getComboList("Head", True, , , enTypeOf.Allowance)
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , enTypeOf.Allowance)
            'Sohail (21 Aug 2015) -- End
            With cboTranhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Head")
                .SelectedValue = 0
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(True, True, , True)
            dsList = objMaster.GetCondition(True, True, True, False, True)
            'Nilay (10-Nov-2016) -- End
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objMaster = Nothing


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboEmployee.SelectedValue = 0
            cboCondition.SelectedValue = 0
            nudYear.Value = 0
            nudToYear.Value = 0
            chkInactiveemp.Checked = False

            objAgeAnalysis.setDefaultOrderBy(0)
            txtOrderBy.Text = objAgeAnalysis.OrderByDisplay
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAgeAnalysis.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Transaction Head."), enMsgBoxStyle.Information)
                cboTranhead.Focus()
                Return False
            ElseIf nudYear.Value > 0 AndAlso CInt(cboCondition.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select condition."), enMsgBoxStyle.Information)
                cboCondition.Focus()
                Return False
            ElseIf nudToYear.Value > 0 AndAlso CInt(cboCondition.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select condition."), enMsgBoxStyle.Information)
                cboCondition.Focus()
                Return False
            ElseIf CInt(cboCondition.SelectedValue) = enComparison_Operator.BETWEEN AndAlso nudToYear.Value < nudYear.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Ending Year cannot be less than Starting Year."), enMsgBoxStyle.Information)
                nudToYear.Focus()
                Return False
            End If

            objAgeAnalysis._PeriodId = CInt(cboPeriod.SelectedValue)
            objAgeAnalysis._PeriodName = cboPeriod.Text
            objAgeAnalysis._PeriodStartDate = mdtPeriodStartDate
            objAgeAnalysis._PeriodEndDate = mdtPeriodEndDate

            objAgeAnalysis._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAgeAnalysis._EmployeeName = cboEmployee.Text

            objAgeAnalysis._Tranheadunkid = CInt(cboTranhead.SelectedValue)
            objAgeAnalysis._Tranheadname = cboTranhead.Text

            objAgeAnalysis._ConditionId = CInt(cboCondition.SelectedValue)
            objAgeAnalysis._Condition = cboCondition.Text

            objAgeAnalysis._StartingYear = nudYear.Value
            objAgeAnalysis._EndingYear = nudToYear.Value

            objAgeAnalysis._ViewByIds = mstrStringIds
            objAgeAnalysis._ViewIndex = mintViewIdx
            objAgeAnalysis._ViewByName = mstrStringName
            objAgeAnalysis._Analysis_Fields = mstrAnalysis_Fields
            objAgeAnalysis._Analysis_Join = mstrAnalysis_Join
            objAgeAnalysis._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAgeAnalysis._Report_GroupName = mstrReport_GroupName

            objAgeAnalysis._IsActive = chkInactiveemp.Checked

            objAgeAnalysis._Advance_Filter = mstrAdvanceFilter

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objAgeAnalysis._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmLongServiceAwardReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objAgeAnalysis = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmLongServiceAwardReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLongServiceAwardReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objAgeAnalysis._ReportName
            Me._Message = objAgeAnalysis._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLongServiceAwardReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim ObjEmp As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                mdtPeriodStartDate = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date"))
                mdtPeriodEndDate = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date"))
                'dsCombo = ObjEmp.GetEmployeeList("Emp", True, ConfigParameter._Object._IsIncludeInactiveEmp, , , , , , , , , , , , mdtPeriodStartDate, mdtPeriodEndDate)
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                'dsCombo = ObjEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            End If

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               mdtPeriodStartDate, _
            '                               mdtPeriodEndDate, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            dsCombo = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtPeriodStartDate, _
                                           mdtPeriodEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub cboCondition_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCondition.SelectedIndexChanged
        Try
            If CInt(cboCondition.SelectedValue) = enComparison_Operator.BETWEEN Then
                lblAnd.Visible = True
                nudToYear.Visible = True
            Else
                lblAnd.Visible = False
                nudToYear.Visible = False
                nudToYear.Value = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCondition_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles MyBase.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAgeAnalysis.generateReport(0, e.Type, enExportAction.None)
            objAgeAnalysis.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles MyBase.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAgeAnalysis.generateReport(0, enPrintAction.None, e.Type)
            objAgeAnalysis.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, _
                                             0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If cboEmployee.DataSource Is Nothing Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- END

            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTranhead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchTranhead.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTranhead.DataSource
            frm.ValueMember = cboTranhead.ValueMember
            frm.DisplayMember = cboTranhead.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboTranhead.SelectedValue = frm.SelectedValue
                cboTranhead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranhead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeAgeAnalysisReport.SetMessages()
            objfrm._Other_ModuleNames = "clsLongServiceAwardReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objAgeAnalysis.setOrderBy(0)
            txtOrderBy.Text = objAgeAnalysis.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
			Me.lblAnd.Text = Language._Object.getCaption(Me.lblAnd.Name, Me.lblAnd.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblTranhead.Text = Language._Object.getCaption(Me.lblTranhead.Name, Me.lblTranhead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Period.")
			Language.setMessage(mstrModuleName, 2, "Please select Transaction Head.")
			Language.setMessage(mstrModuleName, 3, "Please select condition.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Ending Year cannot be less than Starting Year.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'************************************************************************************************************************************
'Class Name : clsEmployee_without_Bank.vb
'Purpose    :
'Date          :09/04/2012
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsEmployee_without_Bank
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_without_Bank"
    Private mstrReportId As String = CStr(enArutiReport.EmployeeWithoutBank)  '82
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnIsActive As Boolean = True
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (20 Apr 2012) -- End

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    'Sohail (20 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (20 Apr 2012) -- End

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (16 May 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintEmployeeUnkid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee :") & " " & mstrEmployeeName & " "
            End If

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (20 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objUser As New clsUserAddEdit

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objRpt = Generate_DetailReport()
            objUser._Userunkid = xUserUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objUser._Username)
            'Sohail (21 Aug 2015) -- End
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(employeecode,'')", Language.getMessage(mstrModuleName, 3, "Employee Code")))

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'hremployee_master.surname+' ' + hremployee_master.firstname AS empname
            If mblnFirstNamethenSurname = False Then
                'iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
                iColumn_DetailReport.Add(New IColumn("ISNULL(surname,'')+' '+ ISNULL(firstname,'')+' '+ISNULL(othername,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_DetailReport.Add(New IColumn("ISNULL(firstname,'')+' '+ ISNULL(othername,'')+' '+ISNULL(surname,'')", Language.getMessage(mstrModuleName, 4, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 5, "Department")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Job.job_name,'')", Language.getMessage(mstrModuleName, 6, "Job Title")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xYearUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strUsername As String _
                                          ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strUsername]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim exForce As Exception
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ = "SELECT "
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= "  hrstation_master.name AS GName, "
            '    Case enAnalysisReport.Department
            '        StrQ &= "  hrdepartment_master.name AS GName, "
            '    Case enAnalysisReport.Section
            '        StrQ &= "  hrsection_master.name AS GName, "
            '    Case enAnalysisReport.Unit
            '        StrQ &= "  hrunit_master.name AS GName, "
            '    Case enAnalysisReport.Job
            '        StrQ &= " hrjob_master.job_name AS GName, "
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " prcostcenter_master.costcentername AS GName, "
            '    Case enAnalysisReport.SectionGroup
            '        StrQ &= " hrsectiongroup_master.name AS GName,"
            '    Case enAnalysisReport.UnitGroup
            '        StrQ &= " hrunitgroup_master.name AS GName, "
            '    Case enAnalysisReport.Team
            '        StrQ &= " hrteam_master.name AS GName, "
            '    Case Else
            '        StrQ &= " '' AS GName, "
            'End Select
            'Sohail (16 May 2012) -- End

            StrQ &= "SELECT employeecode AS EMP_CODE "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME " 
            If mblnFirstNamethenSurname = False Then
                StrQ &= ",ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS EMP_NAME "
            Else
                StrQ &= ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EMP_NAME "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= ",ISNULL(Dept.name,'') AS DEPT_NAME " & _
                    ",ISNULL(Job.job_name,'') AS JOB_NAME "

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (16 May 2012) -- End

            StrQ &= "FROM hremployee_master "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "           LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         stationunkid " & _
                               "        ,deptgroupunkid " & _
                               "        ,departmentunkid " & _
                               "        ,sectiongroupunkid " & _
                               "        ,sectionunkid " & _
                               "        ,unitgroupunkid " & _
                               "        ,unitunkid " & _
                               "        ,teamunkid " & _
                               "        ,classgroupunkid " & _
                               "        ,classunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                               "    FROM hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                               "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         jobgroupunkid " & _
                                "        ,jobunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM hremployee_categorization_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "JOIN hrjob_master AS Job ON J.jobunkid = Job.jobunkid " & _
                    "JOIN hrdepartment_master AS Dept ON T.departmentunkid = Dept.departmentunkid "
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= " JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  "
            '    Case enAnalysisReport.Department
            '        StrQ &= " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "
            '    Case enAnalysisReport.Section
            '        StrQ &= " JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid "
            '    Case enAnalysisReport.Unit
            '        StrQ &= " JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid "
            '    Case enAnalysisReport.Job
            '        StrQ &= " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid "
            '    Case enAnalysisReport.SectionGroup
            '        StrQ &= " JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid "
            '    Case enAnalysisReport.UnitGroup
            '        StrQ &= " JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid "
            '    Case enAnalysisReport.Team
            '        StrQ &= " JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid "
            'End Select
            StrQ &= mstrAnalysis_Join
            'Sohail (16 May 2012) -- End
            'Sohail (14 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= " WHERE employeeunkid NOT IN (SELECT employeeunkid FROM premployee_bank_tran WHERE isvoid = 0) "
            StrQ &= " WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM premployee_bank_tran " & _
                                                    "JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
                                                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON cfpayrollgroup_master.groupmasterunkid = cfbankbranch_master.bankgroupunkid AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " " & _
                                                    "WHERE isvoid = 0) "
            'Sohail (14 Aug 2012) -- End


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End


            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (20 Apr 2012) -- End

            If mintEmployeeUnkid > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeUnkid & "' "
            End If

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        StrQ &= " AND hrstation_master.stationunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.Department
            '        StrQ &= " AND hrdepartment_master.departmentunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.Section
            '        StrQ &= "AND hrsection_master.sectionunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.Unit
            '        StrQ &= " AND hrunit_master.unitunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.Job
            '        StrQ &= " AND hrjob_master.jobunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.CostCenter
            '        StrQ &= " AND prcostcenter_master.costcenterunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.SectionGroup
            '        StrQ &= " AND hrsectiongroup_master.sectiongroupunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.UnitGroup
            '        StrQ &= " AND hrunitgroup_master.unitgroupunkid in (" & mstrViewByIds & ")"
            '    Case enAnalysisReport.Team
            '        StrQ &= " AND hrteam_master.teamunkid in (" & mstrViewByIds & ")"
            'End Select
            'Sohail (16 May 2012) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("EMP_CODE")
                rpt_Rows.Item("Column3") = dtRow.Item("EMP_NAME")
                rpt_Rows.Item("Column4") = dtRow.Item("DEPT_NAME")
                rpt_Rows.Item("Column5") = dtRow.Item("JOB_NAME")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeWOBankReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUsername)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 3, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 4, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 5, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 6, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 11, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 12, "Grand Total :"))

            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Select Case mintViewIndex
            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 207, "Branch :"))
            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 208, "Department :"))
            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 209, "Section :"))
            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 210, "Unit :"))
            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 211, "Job :"))
            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 212, "Cost Center :"))
            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 213, "Section Group :"))
            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 214, "Unit Group :"))
            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 215, "Team :"))
            '    Case Else
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            'End Select
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Sohail (16 May 2012) -- End

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee :")
            Language.setMessage(mstrModuleName, 2, "Order By :")
            Language.setMessage(mstrModuleName, 3, "Employee Code")
            Language.setMessage(mstrModuleName, 4, "Employee Name")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Job Title")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Checked By :")
            Language.setMessage(mstrModuleName, 9, "Approved By :")
            Language.setMessage(mstrModuleName, 10, "Received By :")
            Language.setMessage(mstrModuleName, 11, "Sub Total :")
            Language.setMessage(mstrModuleName, 12, "Grand Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'************************************************************************************************************************************
'Class Name : clsPaymentJVReport.vb
'Purpose    :
'Date       :22/03/2014
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsPaymentJVReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPaymentJVReport"
    Private mstrReportId As String = enArutiReport.Payment_Journal_Voucher_Report

    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END

        Call Create_OnMainLedgerDetailReport()
        Call Create_OnStaffLedgerDetailReport()
        Call Create_OnCostCenterLedgerDetailReport()
        Call Create_OnCombinedJVReport()
    End Sub
#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = String.Empty

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty

    Private mdblDebitAmountFrom As Decimal = 0
    Private mdblDebitAmountTo As Decimal = 0

    Private mdblCreditAmountFrom As Decimal = 0
    Private mdblCreditAmountTo As Decimal = 0

    Private mstrOrderByQuery As String = ""

    Private mblnIsActive As Boolean = True

    Private mblnShowSummaryAtBootom As Boolean = False
    Private mblnShowSummaryOnNextPage As Boolean = False

    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty

    Private mblnShowGroupByCCenterGroup As Boolean = False

    Private mintCCenterGroupId As Integer = 0
    Private mstrCCenterGroupName As String = String.Empty

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mblnIncludeEmployerContribution As Boolean = False
    Private mblnIgnoreZero As Boolean = True

    Private mintCustomCCetnterId As Integer = 0
    Private mstrCustomCCetnterName As String = String.Empty
    Private mstrInvoiceReference As String = String.Empty
    Private mblnShowColumnHeader As Boolean = True

    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintBaseCurrencyId As Integer = 0
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mstrTransactionReference As String = ""
    Private mstrJournalType As String = ""
    Private mstrJVGroupCode As String = ""

#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _DebitAmountFrom() As String
        Set(ByVal value As String)
            mdblDebitAmountFrom = value
        End Set
    End Property

    Public WriteOnly Property _DebitAmountTo() As String
        Set(ByVal value As String)
            mdblDebitAmountTo = value
        End Set
    End Property

    Public WriteOnly Property _CreditAmountFrom() As String
        Set(ByVal value As String)
            mdblCreditAmountFrom = value
        End Set
    End Property

    Public WriteOnly Property _CreditAmountTo() As String
        Set(ByVal value As String)
            mdblCreditAmountTo = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    Public WriteOnly Property _ShowSummaryAtBootom() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSummaryAtBootom = value
        End Set
    End Property

    Public WriteOnly Property _ShowSummaryOnNextPage() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSummaryOnNextPage = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _ShowGroupByCCenerGroup() As Boolean
        Set(ByVal value As Boolean)
            mblnShowGroupByCCenterGroup = value
        End Set
    End Property

    Public WriteOnly Property _CCenterGroupId() As Integer
        Set(ByVal value As Integer)
            mintCCenterGroupId = value
        End Set
    End Property

    Public WriteOnly Property _CCenterGroup_Name() As String
        Set(ByVal value As String)
            mstrCCenterGroupName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _IncludeEmployerContribution() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeEmployerContribution = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreZero() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZero = value
        End Set
    End Property

    Public WriteOnly Property _CustomCCetnterId() As Integer
        Set(ByVal value As Integer)
            mintCustomCCetnterId = value
        End Set
    End Property

    Public WriteOnly Property _CustomCCetnterName() As String
        Set(ByVal value As String)
            mstrCustomCCetnterName = value
        End Set
    End Property

    Public WriteOnly Property _InvoiceReference() As String
        Set(ByVal value As String)
            mstrInvoiceReference = value
        End Set
    End Property

    Public WriteOnly Property _ShowColumnHeader() As Boolean
        Set(ByVal value As Boolean)
            mblnShowColumnHeader = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _Accounting_TransactionReference() As String
        Set(ByVal value As String)
            mstrTransactionReference = value
        End Set
    End Property

    Public WriteOnly Property _Accounting_JournalType() As String
        Set(ByVal value As String)
            mstrJournalType = value
        End Set
    End Property

    Public WriteOnly Property _Accounting_JVGroupCode() As String
        Set(ByVal value As String)
            mstrJVGroupCode = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintPeriodId = 0
            mstrPeriodName = ""

            mdblDebitAmountFrom = 0
            mdblDebitAmountFrom = 0

            mdblCreditAmountFrom = 0
            mdblCreditAmountFrom = 0

            mstrOrderByQuery = ""

            mblnIsActive = True

            mintBranchId = -1
            mstrBranchName = ""

            mblnIncludeEmployerContribution = False
            mblnIgnoreZero = True

            mintCustomCCetnterId = 0
            mstrCustomCCetnterName = ""
            mstrInvoiceReference = ""
            mblnShowColumnHeader = False

            mstrCurrency_Sign = ""
            mdecEx_Rate = 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""


        Try
            If mdblDebitAmountFrom > 0 Then
                Me._FilterQuery &= " AND Dr >= @Debit "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, " Debit Amount >= ") & " " & mdblDebitAmountFrom & " "
                objDataOperation.AddParameter("@Debit", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDebitAmountFrom)
            End If
            If mdblDebitAmountTo > 0 Then
                Me._FilterQuery &= " AND Dr <= @DebitTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " Debit Amount <= ") & " " & mdblDebitAmountTo & " "
                objDataOperation.AddParameter("@DebitTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblDebitAmountTo)
            End If
            If mdblCreditAmountFrom > 0 Then
                Me._FilterQuery &= " AND Cr >= @Credit "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, " Credit Amount >= ") & " " & mdblCreditAmountFrom & " "
                objDataOperation.AddParameter("@Credit", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblCreditAmountFrom)
            End If
            If mdblCreditAmountTo > 0 Then
                Me._FilterQuery &= " AND Cr <= @CreditTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, " Credit Amount <= ") & " " & mdblCreditAmountTo & " "
                objDataOperation.AddParameter("@CreditTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblCreditAmountTo)
            End If

            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Branch :") & " " & mstrBranchName & " "
            End If

            If mblnShowGroupByCCenterGroup = True AndAlso mintCCenterGroupId > 0 Then
                Me._FilterQuery &= " AND groupmasterunkid = " & mintCCenterGroupId
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Cost Center Group :") & " " & mstrCCenterGroupName & " "
            End If

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 36, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If

            If Me.OrderByQuery <> "" Then
                If mblnShowGroupByCCenterGroup = True Then
                    mstrOrderByQuery &= " ORDER BY groupmasterunkid, a.costcenterunkid, " & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, " Order By : ") & " " & Me.OrderByDisplay
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    If mintReportId = 0 Then 'Main Ledger
        '        objRpt = Generate_MainDetailReport()
        '    ElseIf mintReportId = 1 Then 'General Staff Ledger
        '        objRpt = Generate_StaffDetailReport_New()
        '    ElseIf mintReportId = 2 Then 'Cost Center Ledger
        '        objRpt = Generate_CostCenterDetailReport_New()
        '    ElseIf mintReportId = 3 Then 'Combined JV
        '        objRpt = Generate_CombinedJVReport()
        '    End If


        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, "", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objConfig As New clsConfigOptions
        Dim objUser As New clsUserAddEdit
        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            objConfig._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mintReportId = 0 Then 'Main Ledger
                objRpt = Generate_MainDetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objConfig._Base_CurrencyId, objUser._Username)
            ElseIf mintReportId = 1 Then 'General Staff Ledger
                objRpt = Generate_StaffDetailReport_New(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objConfig._Base_CurrencyId, objUser._Username)
            ElseIf mintReportId = 2 Then 'Cost Center Ledger
                objRpt = Generate_CostCenterDetailReport_New(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objConfig._Base_CurrencyId, objUser._Username)
            ElseIf mintReportId = 3 Then 'Combined JV
                objRpt = Generate_CombinedJVReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, mblnIsActive, True, objConfig._CurrencyFormat, objConfig._Base_CurrencyId, objUser._Username)
            End If


            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, "", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
            objConfig = Nothing
            objUser = Nothing
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            If intReportType = 0 Then 'Main Ledger
                OrderByDisplay = iColumn_MainDetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_MainDetailReport.ColumnItem(0).Name
            ElseIf intReportType = 1 Then 'General Staff Ledger
                OrderByDisplay = iColumn_StaffDetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_StaffDetailReport.ColumnItem(0).Name
            ElseIf intReportType = 2 Then 'Cost Center Ledger
                OrderByDisplay = iColumn_CostCenterDetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_CostCenterDetailReport.ColumnItem(0).Name
            ElseIf intReportType = 3 Then 'Combined JV
                OrderByDisplay = iColumn_CombinedJVReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_CombinedJVReport.ColumnItem(0).Name
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            If intReportType = 0 Then 'Main Ledger
                Call OrderByExecute(iColumn_MainDetailReport)
            ElseIf intReportType = 1 Then 'General Staff Ledger
                Call OrderByExecute(iColumn_StaffDetailReport)
            ElseIf intReportType = 2 Then 'Cost Center Ledger
                Call OrderByExecute(iColumn_CostCenterDetailReport)
            ElseIf intReportType = 3 Then 'Combined JV
                Call OrderByExecute(iColumn_CombinedJVReport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

#Region " Main Ledger "
    Dim iColumn_MainDetailReport As New IColumnCollection
    Public Property Field_OnMainDetailReport() As IColumnCollection
        Get
            Return iColumn_MainDetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_MainDetailReport = value
        End Set
    End Property

    Private Sub Create_OnMainLedgerDetailReport()
        Try
            iColumn_MainDetailReport.Clear()
            iColumn_MainDetailReport.Add(New IColumn("Account_code", Language.getMessage(mstrModuleName, 6, "Account No.")))
            iColumn_MainDetailReport.Add(New IColumn("Account_Name", Language.getMessage(mstrModuleName, 7, "Account Name")))
            iColumn_MainDetailReport.Add(New IColumn("Debit", Language.getMessage(mstrModuleName, 8, "Debit Amount")))
            iColumn_MainDetailReport.Add(New IColumn("Credit", Language.getMessage(mstrModuleName, 9, "Credit Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnMainLedgerDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_MainDetailReport(ByVal xDatabaseName As String _
                                               , ByVal xUserUnkid As Integer _
                                               , ByVal xYearUnkid As Integer _
                                               , ByVal xCompanyUnkid As Integer _
                                               , ByVal xPeriodStart As Date _
                                               , ByVal xPeriodEnd As Date _
                                               , ByVal xUserModeSetting As String _
                                               , ByVal xOnlyApproved As Boolean _
                                               , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                               , ByVal blnApplyUserAccessFilter As Boolean _
                                               , ByVal strfmtCurrency As String _
                                               , ByVal intBaseCurrencyId As Integer _
                                               , ByVal strUserName As String _
                                               ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBaseCurrencyId, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mintBaseCurrencyId = intBaseCurrencyId
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            StrQ = "SELECT  payperiodunkid  " & _
                "      , period_name  " & _
                "      , account_code  " & _
                "      , account_name  " & _
                "      , tranheadcode " & _
                          ", trnheadname " & _
                          ", 0 AS Unkid " & _
                          ", '' AS Code " & _
                          ", '' NAME " & _
                          ", DrCr " & _
                    ", (Dr * " & mdecEx_Rate & ") AS Debit  " & _
                    ", (Cr * " & mdecEx_Rate & ") AS Credit  " & _
                          ", referencecodeid " & _
                          ", referencenameid " & _
                          ", referencetypeid " & _
                          ", shortname "


            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName " & _
                         ", ISNULL(a.costcenterunkid, 0) AS CCId " & _
                         ", ISNULL(a.costcentercode, '') AS CCCode " & _
                         ", ISNULL(a.costcentername, '') AS CCName "

            End If

            StrQ &= "FROM    ( SELECT    payperiodunkid  " & _
                "                  , period_name  " & _
                "                  , accountunkid  " & _
                "                  , account_code  " & _
                "                  , account_name  " & _
                "                  , tranheadcode " & _
                                      ", vwCompPaymentJV.trnheadname " & _
                                      ", DrCr " & _
                                      ", CASE WHEN DrCr = 'Dr' THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) ELSE 0 END AS Dr " & _
                                      ", CASE WHEN DrCr = 'Cr' THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) ELSE 0 END AS Cr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
            If mblnShowGroupByCCenterGroup Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwCompPaymentJV.costcenterunkid " & _
                '        ", costcentercode " & _
                '        ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                       ", CC.costcentercode " & _
                       ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= "          FROM      vwCompPaymentJV  " & _
                    "          JOIN hremployee_master ON hremployee_master.employeeunkid = vwCompPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            If mblnIncludeEmployerContribution = False Then
                StrQ &= " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwCompPaymentJV.tranheadunkid " & _
                                    "AND vwCompPaymentJV.transactiontype_Id = 1 " 'Group = Transaction Head
            End If

            StrQ &= "          WHERE     payperiodunkid = @payperiodunkid  "

            If mblnIncludeEmployerContribution = False Then
                StrQ &= "  AND ISNULL(prtranhead_master.typeof_id, 0) <> " & enTypeOf.Employers_Statutory_Contribution & " " & _
                        "  AND ISNULL(prtranhead_master.reftranheadid, 0) NOT IN (SELECT  tranheadunkid FROM    prtranhead_master WHERE ISNULL(isvoid, 0) = 0 AND  typeof_id = " & enTypeOf.Employers_Statutory_Contribution & " ) "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwCompPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwCompPaymentJV.amount <> 0 "
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          GROUP BY  payperiodunkid  " & _
                "                  , period_name  " & _
                "                  , accountunkid  " & _
                "                  , account_code  " & _
                "                  , account_name  " & _
                 "                 , tranheadcode " & _
                                      ", vwCompPaymentJV.trnheadname " & _
                              ", DrCr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
            If mblnShowGroupByCCenterGroup = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwCompPaymentJV.costcenterunkid " & _
                '          ", costcentercode " & _
                '          ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                          ", CC.costcentercode " & _
                          ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= "        ) AS a  "

            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If

            StrQ &= "WHERE 1 = 1  "


            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim mdecDebitTot As Decimal = 0
            Dim mdecCreditTot As Decimal = 0
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecDebitTot = dsList.Tables("DataTable").Compute("SUM(Debit)", "")
            End If
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecCreditTot = dsList.Tables("DataTable").Compute("SUM(Credit)", "")
            End If


            Dim decDebitCCGrpTot As Decimal = 0
            Dim decCreditCCGrpTot As Decimal = 0
            Dim decDebitCCTot As Decimal = 0
            Dim decCreditCCTot As Decimal = 0
            Dim strPrevCCGroupId As String = ""
            Dim strPrevCCId As String = ""
            Dim strPrevDetail As String = ""
            Dim strCurrDetail As String = ""

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                strCurrDetail = dtRow.Item("DrCr") + "_" + dtRow.Item("account_code").ToString + "_" + dtRow.Item("account_name").ToString + "_" + dtRow.Item("referencecodeid").ToString + "_" + dtRow.Item("referencenameid").ToString
                If strPrevDetail = strCurrDetail AndAlso CInt(dtRow.Item("referencecodeid")) = enJVCompanyConfigRefCode.AccountCode AndAlso CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.AccountName Then
                    Dim intPrevRow As Integer = rpt_Data.Tables("ArutiTable").Rows.Count - 1
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column5") = Format(CDec(rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column5")) + CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                    'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column6") = Format(CDec(rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column6")) + CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                    rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column5") = Format(CDec(rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column5")) + CDec(dtRow.Item("Debit")), strfmtCurrency)
                    rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column6") = Format(CDec(rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column6")) + CDec(dtRow.Item("Credit")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    If mblnShowGroupByCCenterGroup = True Then
                        If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString OrElse strPrevCCId <> dtRow.Item("CCId").ToString Then
                            decDebitCCTot = CDec(dtRow.Item("Debit"))
                            decCreditCCTot = CDec(dtRow.Item("Credit"))
                            strPrevCCId = dtRow.Item("CCId").ToString
                        Else
                            decDebitCCTot += CDec(dtRow.Item("Debit"))
                            decCreditCCTot += CDec(dtRow.Item("Credit"))
                        End If

                        If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString Then
                            decDebitCCGrpTot = CDec(dtRow.Item("Debit"))
                            decCreditCCGrpTot = CDec(dtRow.Item("Credit"))
                            strPrevCCGroupId = dtRow.Item("CCGroupId").ToString
                        Else
                            decDebitCCGrpTot += CDec(dtRow.Item("Debit"))
                            decCreditCCGrpTot += CDec(dtRow.Item("Credit"))
                        End If

                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column11") = dtRow.Item("CCGroupId")
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column12") = dtRow.Item("CCGroupCode")
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column13") = dtRow.Item("CCGroupName")
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column14") = dtRow.Item("CCId")
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column15") = dtRow.Item("CCCode")
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column16") = dtRow.Item("CCName")
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column17") = Format(decDebitCCGrpTot, GUI.fmtCurrency)
                        'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column18") = Format(decCreditCCGrpTot, GUI.fmtCurrency)
                        'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column19") = Format(decDebitCCTot, GUI.fmtCurrency)
                        'rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column20") = Format(decCreditCCTot, GUI.fmtCurrency)
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column17") = Format(decDebitCCGrpTot, strfmtCurrency)
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column18") = Format(decCreditCCGrpTot, strfmtCurrency)
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column19") = Format(decDebitCCTot, strfmtCurrency)
                        rpt_Data.Tables("ArutiTable").Rows(intPrevRow).Item("Column20") = Format(decCreditCCTot, strfmtCurrency)
                        'Sohail (21 Aug 2015) -- End
                    End If

                    strPrevDetail = dtRow.Item("DrCr") + "_" + dtRow.Item("account_code").ToString + "_" + dtRow.Item("account_name").ToString + "_" + dtRow.Item("referencecodeid").ToString + "_" + dtRow.Item("referencenameid").ToString
                    Continue For
                End If


                Dim rpt_Rows As DataRow

                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVCompanyConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCompanyConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                If CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.AccountName Then
                    rpt_Rows.Item("Column4") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column4") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName Then
                    rpt_Rows.Item("Column4") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column4") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column4") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                Else
                    rpt_Rows.Item("Column4") = dtRow.Item("account_name")
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column9") = Format(mdecDebitTot, GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(mdecCreditTot, GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(mdecDebitTot, strfmtCurrency)
                rpt_Rows.Item("Column10") = Format(mdecCreditTot, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If mblnShowGroupByCCenterGroup = True Then
                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString OrElse strPrevCCId <> dtRow.Item("CCId").ToString Then
                        decDebitCCTot = CDec(dtRow.Item("Debit"))
                        decCreditCCTot = CDec(dtRow.Item("Credit"))
                        strPrevCCId = dtRow.Item("CCId").ToString
                    Else
                        decDebitCCTot += CDec(dtRow.Item("Debit"))
                        decCreditCCTot += CDec(dtRow.Item("Credit"))
                    End If

                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString Then
                        decDebitCCGrpTot = CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot = CDec(dtRow.Item("Credit"))
                        strPrevCCGroupId = dtRow.Item("CCGroupId").ToString
                    Else
                        decDebitCCGrpTot += CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot += CDec(dtRow.Item("Credit"))
                    End If
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("CCId")
                    rpt_Rows.Item("Column15") = dtRow.Item("CCCode")
                    rpt_Rows.Item("Column16") = dtRow.Item("CCName")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column19") = Format(decDebitCCTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column20") = Format(decCreditCCTot, GUI.fmtCurrency)
                    rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column19") = Format(decDebitCCTot, strfmtCurrency)
                    rpt_Rows.Item("Column20") = Format(decCreditCCTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                    rpt_Rows.Item("Column17") = ""
                    rpt_Rows.Item("Column18") = ""
                    rpt_Rows.Item("Column19") = ""
                    rpt_Rows.Item("Column20") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                strPrevDetail = dtRow.Item("DrCr") + "_" + dtRow.Item("account_code").ToString + "_" + dtRow.Item("account_name").ToString + "_" + dtRow.Item("referencecodeid").ToString + "_" + dtRow.Item("referencenameid").ToString
            Next

            objRpt = New ArutiReport.Designer.rptMainLedger

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 29, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtAccCode", Language.getMessage(mstrModuleName, 6, "Account No."))
            Call ReportFunction.TextChange(objRpt, "txtAccName", Language.getMessage(mstrModuleName, 7, "Account Name"))
            Call ReportFunction.TextChange(objRpt, "txtDebit", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtCredit", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName", Language.getMessage(mstrModuleName, 10, "Period :"))

            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 11, "Total"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            If mblnShowGroupByCCenterGroup = True Then
                Call ReportFunction.TextChange(objRpt, "txtCCGroup", Language.getMessage(mstrModuleName, 24, "Cost Center Group :"))
                Call ReportFunction.TextChange(objRpt, "txtCC", Language.getMessage(mstrModuleName, 25, "Cost Center :"))

                Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 26, "Sub Total :"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 27, "Group Total :"))

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", False)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", True)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "Section3", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)

            If mblnShowSummaryAtBootom = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            ElseIf mblnShowSummaryOnNextPage = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "True"
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            End If

            Call ReportFunction.SetRptDecimal(objRpt, "FldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol52")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol61")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol62")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol61")



            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_MainDetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#Region " Staff Ledger "
    Dim iColumn_StaffDetailReport As New IColumnCollection
    Public Property Field_OnStaffDetailReport() As IColumnCollection
        Get
            Return iColumn_StaffDetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_StaffDetailReport = value
        End Set
    End Property

    Private Sub Create_OnStaffLedgerDetailReport()
        Try
            iColumn_StaffDetailReport.Clear()
            iColumn_StaffDetailReport.Add(New IColumn("Account_code", Language.getMessage(mstrModuleName, 6, "Account No.")))
            iColumn_StaffDetailReport.Add(New IColumn("Account_Name", Language.getMessage(mstrModuleName, 7, "Account Name")))
            iColumn_StaffDetailReport.Add(New IColumn("employeename", Language.getMessage(mstrModuleName, 15, "Employee")))
            iColumn_StaffDetailReport.Add(New IColumn("Debit", Language.getMessage(mstrModuleName, 8, "Debit Amount")))
            iColumn_StaffDetailReport.Add(New IColumn("Credit", Language.getMessage(mstrModuleName, 9, "Credit Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnStaffLedgerDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_StaffDetailReport_New(ByVal xDatabaseName As String _
                                                    , ByVal xUserUnkid As Integer _
                                                    , ByVal xYearUnkid As Integer _
                                                    , ByVal xCompanyUnkid As Integer _
                                                    , ByVal xPeriodStart As Date _
                                                    , ByVal xPeriodEnd As Date _
                                                    , ByVal xUserModeSetting As String _
                                                    , ByVal xOnlyApproved As Boolean _
                                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                    , ByVal blnApplyUserAccessFilter As Boolean _
                                                    , ByVal strfmtCurrency As String _
                                                    , ByVal intBaseCurrencyId As Integer _
                                                    , ByVal strUserName As String _
                                                    ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBaseCurrencyId, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptSubreport_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mintBaseCurrencyId = intBaseCurrencyId
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            StrQ = "SELECT  payperiodunkid  " & _
            "      , period_name  " & _
            "      , account_code  " & _
            "      , account_name  " & _
                            ", tranheadcode " & _
                            ", trnheadname " & _
                            ", employeeunkid AS Unkid " & _
                            ", employeecode AS Code " & _
                            ", employeename AS Name  " & _
                    ", (Dr * " & mdecEx_Rate & ") AS Debit  " & _
                    ", (Cr * " & mdecEx_Rate & ") AS Credit  " & _
                            ", referencecodeid " & _
                            ", referencenameid " & _
                            ", referencetypeid " & _
                            ", shortname "


            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName " & _
                         ", ISNULL(a.costcenterunkid, 0) AS CCId " & _
                         ", ISNULL(a.costcentercode, '') AS CCCode " & _
                         ", ISNULL(a.costcentername, '') AS CCName "
            End If
            StrQ &= "FROM    ( SELECT    payperiodunkid  " & _
            "                  , period_name  " & _
            "                  , accountunkid  " & _
            "                  , account_code  " & _
                            ", account_name  " & _
                            ", tranheadcode " & _
                            ", trnheadname " & _
            "                  , vwEmpPaymentJV.employeeunkid  " & _
            "                  , vwEmpPaymentJV.employeecode  " & _
            "                  , employeename  " & _
                            ", add_deduct  " & _
                            ", CASE WHEN add_deduct = 1  " & _
                                        "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))  " & _
                                    "ELSE 0  " & _
            "                    END AS Dr  " & _
            "                  , CASE WHEN add_deduct = 2  " & _
            "                              OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))  " & _
            "                         ELSE 0  " & _
            "                    END AS Cr  " & _
                            ", referencecodeid " & _
                            ", referencenameid " & _
                            ", referencetypeid " & _
                            ", shortname "
            If mblnShowGroupByCCenterGroup Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                '        ", costcentercode " & _
                '        ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                      ", CC.costcentercode " & _
                      ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If
            StrQ &= "          FROM      vwEmpPaymentJV  " & _
                    "          JOIN hremployee_master ON hremployee_master.employeeunkid = vwEmpPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          WHERE     payperiodunkid = @payperiodunkid  "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If mblnIsActive = False Then
                'StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwEmpPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwEmpPaymentJV.amount <> 0 "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          GROUP BY  payperiodunkid  " & _
            "                  , period_name  " & _
            "                  , accountunkid  " & _
            "                  , account_code  " & _
            "                  , account_name  " & _
                                     ", tranheadcode " & _
                                     ", trnheadname " & _
            "                  , vwEmpPaymentJV.employeeunkid  " & _
            "                  , vwEmpPaymentJV.employeecode  " & _
            "                  , employeename  " & _
            "                  , add_deduct  " & _
                                     ", referencecodeid " & _
                                     ", referencenameid " & _
                                     ", referencetypeid " & _
                                     ", shortname "
            If mblnShowGroupByCCenterGroup = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                '          ", costcentercode " & _
                '          ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                         ", CC.costcentercode " & _
                         ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If
            StrQ &= "        ) AS a  "
            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If
            StrQ &= "WHERE 1 = 1  "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim mdecDebitTot As Decimal = 0
            Dim mdecCreditTot As Decimal = 0
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecDebitTot = dsList.Tables("DataTable").Compute("SUM(Debit)", "")
            End If
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecCreditTot = dsList.Tables("DataTable").Compute("SUM(Credit)", "")
            End If

            Dim decDebitCCGrpTot As Decimal = 0
            Dim decCreditCCGrpTot As Decimal = 0
            Dim decDebitCCTot As Decimal = 0
            Dim decCreditCCTot As Decimal = 0
            Dim strPrevCCGroupId As String = ""
            Dim strPrevCCId As String = ""

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim objIdentity As clsIdentity_tran
            Dim dsID As DataSet
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.IDType Then
                    objIdentity = New clsIdentity_tran
                    dsID = objIdentity.GetIdentity_tranList("ID", CInt(dtRow.Item("Unkid")), CInt(dtRow.Item("referencetypeid")))
                    If dsID.Tables("ID").Rows.Count > 0 Then
                        rpt_Rows.Item("Column3") = dsID.Tables("ID").Rows(0).Item("identity_no").ToString
                    Else
                        rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                    End If
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.EmployeeCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.AccountCode_EmployeeCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code") & " - " & dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.EmployeeCode_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code") & " - " & dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.ShortName_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("account_code")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                rpt_Rows.Item("Column4") = dtRow.Item("Code") 'dtRow.Item("account_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))
                rpt_Rows.Item("Column7") = dtRow.Item("Unkid")
                If CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName_EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.TranHeadName_EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column9") = Format(mdecDebitTot, GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(mdecCreditTot, GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(mdecDebitTot, strfmtCurrency)
                rpt_Rows.Item("Column10") = Format(mdecCreditTot, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If mblnShowGroupByCCenterGroup = True Then
                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString OrElse strPrevCCId <> dtRow.Item("CCId").ToString Then
                        decDebitCCTot = CDec(dtRow.Item("Debit"))
                        decCreditCCTot = CDec(dtRow.Item("Credit"))
                        strPrevCCId = dtRow.Item("CCId").ToString
                    Else
                        decDebitCCTot += CDec(dtRow.Item("Debit"))
                        decCreditCCTot += CDec(dtRow.Item("Credit"))
                    End If

                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString Then
                        decDebitCCGrpTot = CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot = CDec(dtRow.Item("Credit"))
                        strPrevCCGroupId = dtRow.Item("CCGroupId").ToString
                    Else
                        decDebitCCGrpTot += CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot += CDec(dtRow.Item("Credit"))
                    End If
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("CCId")
                    rpt_Rows.Item("Column15") = dtRow.Item("CCCode")
                    rpt_Rows.Item("Column16") = dtRow.Item("CCName")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column19") = Format(decDebitCCTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column20") = Format(decCreditCCTot, GUI.fmtCurrency)
                    rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column19") = Format(decDebitCCTot, strfmtCurrency)
                    rpt_Rows.Item("Column20") = Format(decCreditCCTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                    rpt_Rows.Item("Column17") = ""
                    rpt_Rows.Item("Column18") = ""
                    rpt_Rows.Item("Column19") = ""
                    rpt_Rows.Item("Column20") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptMainLedger

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 29, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mblnShowGroupByCCenterGroup = True Then
                Call ReportFunction.TextChange(objRpt, "txtCCGroup", Language.getMessage(mstrModuleName, 24, "Cost Center Group :"))
                Call ReportFunction.TextChange(objRpt, "txtCC", Language.getMessage(mstrModuleName, 25, "Cost Center :"))

                Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 26, "Sub Total :"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 27, "Group Total :"))

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", False)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", True)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            '*** For Summary Sub Report
            If mblnShowSummaryAtBootom = True OrElse mblnShowSummaryOnNextPage = True Then
                StrQ = "SELECT TableDr.employeeunkid " & _
                          ", TableDr.employeecode " & _
                          ", TableDr.employeename " & _
                          ", TableDr.DrCr AS Dr " & _
                          ", TableDr.amount AS DebitAmount " & _
                          ", TableCr.DrCr AS Cr " & _
                          ", TableCr.amount AS CreditAmount " & _
                           "FROM " & _
                    "( " & _
                    "SELECT vwEmpPaymentJV.employeeunkid, vwEmpPaymentJV.employeecode, employeename, DrCr, SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS amount  FROM vwEmpPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwEmpPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE payperiodunkid = " & mintPeriodId & " " & _
                    "AND DrCr = 'Dr' "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "GROUP BY vwEmpPaymentJV.employeeunkid, vwEmpPaymentJV.employeecode, employeename, DrCr " & _
                    ") AS TableDr " & _
                    "JOIN " & _
                    "( " & _
                    "SELECT vwEmpPaymentJV.employeeunkid, vwEmpPaymentJV.employeecode, employeename, DrCr, SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS amount  FROM vwEmpPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwEmpPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         stationunkid " & _
                           "        ,deptgroupunkid " & _
                           "        ,departmentunkid " & _
                           "        ,sectiongroupunkid " & _
                           "        ,sectionunkid " & _
                           "        ,unitgroupunkid " & _
                           "        ,unitunkid " & _
                           "        ,teamunkid " & _
                           "        ,classgroupunkid " & _
                           "        ,classunkid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_transfer_tran " & _
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE payperiodunkid = " & mintPeriodId & " " & _
                    "AND DrCr = 'Cr' "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '             " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND vwEmpPaymentJV.BranchId = " & mintBranchId
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                    'Sohail (21 Aug 2015) -- End
                End If

                If mblnIgnoreZero = True Then
                    StrQ &= " AND vwEmpPaymentJV.amount <> 0 "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "GROUP BY vwEmpPaymentJV.employeeunkid, vwEmpPaymentJV.employeecode, employeename, DrCr " & _
                    ") AS TableCr " & _
                    "ON TableDr.employeeunkid = TableCr.employeeunkid "

                dsList = objDataOperation.ExecQuery(StrQ, "SubDataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim mdecSumDebitTot As Decimal = 0
                Dim mdecSumCreditTot As Decimal = 0
                If dsList.Tables("SubDataTable").Rows.Count > 0 Then
                    mdecSumDebitTot = dsList.Tables("SubDataTable").Compute("SUM(DebitAmount)", "")
                End If
                If dsList.Tables("SubDataTable").Rows.Count > 0 Then
                    mdecSumCreditTot = dsList.Tables("SubDataTable").Compute("SUM(CreditAmount)", "")
                End If

                rptSubreport_Data = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("SubDataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rptSubreport_Data.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("employeeunkid")
                    rpt_Rows.Item("Column2") = dtRow.Item("employeecode")
                    rpt_Rows.Item("Column3") = dtRow.Item("employeename")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("DebitAmount")), GUI.fmtCurrency)
                    'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("CreditAmount")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("DebitAmount")), strfmtCurrency)
                    rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("CreditAmount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    rpt_Rows.Item("Column81") = CDec(dtRow.Item("DebitAmount"))
                    rpt_Rows.Item("Column82") = CDec(dtRow.Item("CreditAmount"))
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column9") = Format(mdecSumDebitTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column10") = Format(mdecSumCreditTot, GUI.fmtCurrency)
                    rpt_Rows.Item("Column9") = Format(mdecSumDebitTot, strfmtCurrency)
                    rpt_Rows.Item("Column10") = Format(mdecSumCreditTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    rptSubreport_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

                objRpt.Subreports("Summary").SetDataSource(rptSubreport_Data)

                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtCode", Language.getMessage(mstrModuleName, 31, "Emp. Code"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtName", Language.getMessage(mstrModuleName, 30, "Employee Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtDebitAmt", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtCreditAmt", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "TxtSummaryTotal", Language.getMessage(mstrModuleName, 11, "Total "))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtSummary", "General Staff Ledger Summary")
            End If

            Call ReportFunction.TextChange(objRpt, "txtAccCode1", Language.getMessage(mstrModuleName, 6, "Account No."))
            Call ReportFunction.TextChange(objRpt, "txtAccName1", Language.getMessage(mstrModuleName, 31, "Emp. Code"))
            Call ReportFunction.TextChange(objRpt, "txtDebit1", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtCredit1", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName1", Language.getMessage(mstrModuleName, 10, "Period :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 18, "Description"))

            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 11, "Total "))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "Section3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)

            If mblnShowSummaryAtBootom = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            ElseIf mblnShowSummaryOnNextPage = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "True"
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            End If

            Call ReportFunction.SetRptDecimal(objRpt, "FldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol52")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol61")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol62")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol61")


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_StaffDetailReport_New; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#Region " CostCenter Ledger "
    Dim iColumn_CostCenterDetailReport As New IColumnCollection
    Public Property Field_OnCostCenterDetailReport() As IColumnCollection
        Get
            Return iColumn_CostCenterDetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_CostCenterDetailReport = value
        End Set
    End Property

    Private Sub Create_OnCostCenterLedgerDetailReport()
        Try
            iColumn_CostCenterDetailReport.Clear()
            iColumn_CostCenterDetailReport.Add(New IColumn("Account_code", Language.getMessage(mstrModuleName, 6, "Account No.")))
            iColumn_CostCenterDetailReport.Add(New IColumn("Account_Name", Language.getMessage(mstrModuleName, 7, "Account Name")))
            iColumn_CostCenterDetailReport.Add(New IColumn("costcentername", Language.getMessage(mstrModuleName, 16, "Cost Center")))
            iColumn_CostCenterDetailReport.Add(New IColumn("Debit", Language.getMessage(mstrModuleName, 8, "Debit Amount")))
            iColumn_CostCenterDetailReport.Add(New IColumn("Credit", Language.getMessage(mstrModuleName, 9, "Credit Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnCostCenterLedgerDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_CostCenterDetailReport_New(ByVal xDatabaseName As String _
                                                         , ByVal xUserUnkid As Integer _
                                                         , ByVal xYearUnkid As Integer _
                                                         , ByVal xCompanyUnkid As Integer _
                                                         , ByVal xPeriodStart As Date _
                                                         , ByVal xPeriodEnd As Date _
                                                         , ByVal xUserModeSetting As String _
                                                         , ByVal xOnlyApproved As Boolean _
                                                         , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                         , ByVal blnApplyUserAccessFilter As Boolean _
                                                         , ByVal strfmtCurrency As String _
                                                         , ByVal intBaseCurrencyId As Integer _
                                                         , ByVal strUserName As String _
                                                         ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBaseCurrencyId, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rptSubreport_Data As ArutiReport.Designer.dsArutiReport
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mintBaseCurrencyId = intBaseCurrencyId
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId


            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            StrQ = "SELECT  payperiodunkid  " & _
            "      , period_name  " & _
            "      , account_code  " & _
            "      , account_name  " & _
                            ", tranheadcode " & _
                            ", trnheadname " & _
                            ", a.costcenterunkid AS Unkid " & _
                            ", a.costcentercode AS Code " & _
                            ", a.costcentername AS Name  " & _
                    ", (Dr * " & mdecEx_Rate & ") AS Debit  " & _
                    ", (Cr * " & mdecEx_Rate & ") AS Credit  " & _
                            ", referencecodeid " & _
                            ", referencenameid " & _
                            ", referencetypeid " & _
                            ", shortname "

            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName "
            End If
            StrQ &= "FROM    ( SELECT    payperiodunkid  " & _
            "                  , period_name  " & _
            "                  , accountunkid  " & _
            "                  , account_code  " & _
                            ", account_name  " & _
                            ", tranheadcode " & _
                            ", trnheadname " & _
            "                  , C.costcenterunkid  " & _
            "                  , CC.costcentercode  " & _
            "                  , CC.costcentername                    " & _
                            ", add_deduct  " & _
                            ", CASE WHEN add_deduct = 1  " & _
                                        "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))  " & _
                                    "ELSE 0  " & _
            "                    END AS Dr  " & _
            "                  , CASE WHEN add_deduct = 2  " & _
            "                              OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))  " & _
            "                         ELSE 0  " & _
            "                    END AS Cr  " & _
                            ", referencecodeid " & _
                            ", referencenameid " & _
                            ", referencetypeid " & _
                            ", shortname " & _
            "          FROM      vwCCPaymentJV  " & _
            "          JOIN hremployee_master ON hremployee_master.employeeunkid = vwCCPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          WHERE     payperiodunkid = @payperiodunkid  "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            ''Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwCCPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwCCPaymentJV.amount <> 0 "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          GROUP BY  payperiodunkid  " & _
            "                  , period_name  " & _
            "                  , accountunkid  " & _
            "                  , account_code  " & _
            "                  , account_name  " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
            "                  , C.costcenterunkid  " & _
            "                  , CC.costcentercode  " & _
            "                  , CC.costcentername  " & _
            "                  , add_deduct  " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname " & _
            "        ) AS a  "
            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If
            StrQ &= "WHERE 1 = 1  "

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim mdecDebitTot As Decimal = 0
            Dim mdecCreditTot As Decimal = 0
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecDebitTot = dsList.Tables("DataTable").Compute("SUM(Debit)", "")
            End If
            If dsList.Tables("DataTable").Rows.Count > 0 Then
                mdecCreditTot = dsList.Tables("DataTable").Compute("SUM(Credit)", "")
            End If

            Dim decDebitCCGrpTot As Decimal = 0
            Dim decCreditCCGrpTot As Decimal = 0
            Dim decDebitCCTot As Decimal = 0
            Dim decCreditCCTot As Decimal = 0
            Dim strPrevCCGroupId As String = ""
            Dim strPrevCCId As String = ""

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.AccountCode_CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code") & " - " & dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName_CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("Code")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                rpt_Rows.Item("Column4") = dtRow.Item("Code")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))
                rpt_Rows.Item("Column7") = dtRow.Item("Unkid")
                If CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.TranHeadName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("Name")
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column9") = Format(mdecDebitTot, GUI.fmtCurrency)
                'rpt_Rows.Item("Column10") = Format(mdecCreditTot, GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(mdecDebitTot, strfmtCurrency)
                rpt_Rows.Item("Column10") = Format(mdecCreditTot, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If mblnShowGroupByCCenterGroup = True Then
                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString OrElse strPrevCCId <> dtRow.Item("UnkId").ToString Then
                        decDebitCCTot = CDec(dtRow.Item("Debit"))
                        decCreditCCTot = CDec(dtRow.Item("Credit"))
                        strPrevCCId = dtRow.Item("UnkId").ToString
                    Else
                        decDebitCCTot += CDec(dtRow.Item("Debit"))
                        decCreditCCTot += CDec(dtRow.Item("Credit"))
                    End If

                    If strPrevCCGroupId <> dtRow.Item("CCGroupId").ToString Then
                        decDebitCCGrpTot = CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot = CDec(dtRow.Item("Credit"))
                        strPrevCCGroupId = dtRow.Item("CCGroupId").ToString
                    Else
                        decDebitCCGrpTot += CDec(dtRow.Item("Debit"))
                        decCreditCCGrpTot += CDec(dtRow.Item("Credit"))
                    End If
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("UnkId")
                    rpt_Rows.Item("Column15") = dtRow.Item("Code")
                    rpt_Rows.Item("Column16") = dtRow.Item("Name")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column19") = Format(decDebitCCTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column20") = Format(decCreditCCTot, GUI.fmtCurrency)
                    rpt_Rows.Item("Column17") = Format(decDebitCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column18") = Format(decCreditCCGrpTot, strfmtCurrency)
                    rpt_Rows.Item("Column19") = Format(decDebitCCTot, strfmtCurrency)
                    rpt_Rows.Item("Column20") = Format(decCreditCCTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                    rpt_Rows.Item("Column17") = ""
                    rpt_Rows.Item("Column18") = ""
                    rpt_Rows.Item("Column19") = ""
                    rpt_Rows.Item("Column20") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptMainLedger

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 29, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If mblnShowGroupByCCenterGroup = True Then
                Call ReportFunction.TextChange(objRpt, "txtCCGroup", Language.getMessage(mstrModuleName, 24, "Cost Center Group :"))
                Call ReportFunction.TextChange(objRpt, "txtCC", Language.getMessage(mstrModuleName, 25, "Cost Center :"))

                Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 26, "Sub Total :"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 27, "Group Total :"))

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", False)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", True)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            '*** For Summary Sub Report
            If mblnShowSummaryAtBootom = True OrElse mblnShowSummaryOnNextPage = True Then

                StrQ = "SELECT TableDr.costcenterunkid " & _
                          ", TableDr.costcentercode " & _
                          ", TableDr.costcentername " & _
                          ", TableDr.DrCr AS Dr " & _
                          ", TableDr.amount AS DebitAmount " & _
                          ", TableCr.DrCr AS Cr " & _
                          ", TableCr.amount AS CreditAmount " & _
                           "FROM " & _
                    "( " & _
                    "SELECT vwCCPaymentJV.costcenterunkid, costcentercode, costcentername, DrCr, SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS amount  FROM vwCCPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwCCPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE payperiodunkid = " & mintPeriodId & " " & _
                    "AND DrCr = 'Dr' "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mintBranchId > 0 Then
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "GROUP BY vwCCPaymentJV.costcenterunkid, costcentercode, costcentername, DrCr " & _
                    ") AS TableDr " & _
                    "JOIN " & _
                    "( " & _
                    "SELECT vwCCPaymentJV.costcenterunkid, costcentercode, costcentername, DrCr, SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS amount  FROM vwCCPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwCCPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "WHERE payperiodunkid = " & mintPeriodId & " " & _
                    "AND DrCr = 'Cr' "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND vwCCPaymentJV.BranchId = " & mintBranchId
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                    'Sohail (21 Aug 2015) -- End
                End If

                If mblnIgnoreZero = True Then
                    StrQ &= " AND vwCCPaymentJV.amount <> 0 "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End


                StrQ &= "GROUP BY vwCCPaymentJV.costcenterunkid, costcentercode, costcentername, DrCr " & _
                    ") AS TableCr " & _
                    "ON TableDr.costcenterunkid = TableCr.costcenterunkid "

                dsList = objDataOperation.ExecQuery(StrQ, "SubDataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim mdecSumDebitTot As Decimal = 0
                Dim mdecSumCreditTot As Decimal = 0
                If dsList.Tables("SubDataTable").Rows.Count > 0 Then
                    mdecSumDebitTot = dsList.Tables("SubDataTable").Compute("SUM(DebitAmount)", "")
                End If
                If dsList.Tables("SubDataTable").Rows.Count > 0 Then
                    mdecSumCreditTot = dsList.Tables("SubDataTable").Compute("SUM(CreditAmount)", "")
                End If

                rptSubreport_Data = New ArutiReport.Designer.dsArutiReport

                For Each dtRow As DataRow In dsList.Tables("SubDataTable").Rows
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rptSubreport_Data.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("costcenterunkid")
                    rpt_Rows.Item("Column2") = dtRow.Item("costcentercode")
                    rpt_Rows.Item("Column3") = dtRow.Item("costcentername")
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("DebitAmount")), GUI.fmtCurrency)
                    'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("CreditAmount")), GUI.fmtCurrency)
                    rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("DebitAmount")), strfmtCurrency)
                    rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("CreditAmount")), strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    rpt_Rows.Item("Column81") = CDec(dtRow.Item("DebitAmount"))
                    rpt_Rows.Item("Column82") = CDec(dtRow.Item("CreditAmount"))
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Rows.Item("Column9") = Format(mdecSumDebitTot, GUI.fmtCurrency)
                    'rpt_Rows.Item("Column10") = Format(mdecSumCreditTot, GUI.fmtCurrency)
                    rpt_Rows.Item("Column9") = Format(mdecSumDebitTot, strfmtCurrency)
                    rpt_Rows.Item("Column10") = Format(mdecSumCreditTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End

                    rptSubreport_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                Next

                objRpt.Subreports("Summary").SetDataSource(rptSubreport_Data)

                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtCode", Language.getMessage(mstrModuleName, 34, "CC. Code"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtName", Language.getMessage(mstrModuleName, 28, "Cost Center Name"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtDebitAmt", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtCreditAmt", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "TxtSummaryTotal", Language.getMessage(mstrModuleName, 11, "Total "))
                Call ReportFunction.TextChange(objRpt.Subreports("Summary"), "txtSummary", "Cost Center Ledger Summary")
            End If

            Call ReportFunction.TextChange(objRpt, "txtAccCode1", Language.getMessage(mstrModuleName, 6, "Account No."))
            Call ReportFunction.TextChange(objRpt, "txtAccName1", Language.getMessage(mstrModuleName, 34, "CC. Code"))
            Call ReportFunction.TextChange(objRpt, "txtDebit1", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtCredit1", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName1", Language.getMessage(mstrModuleName, 10, "Period :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 18, "Description"))

            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 11, "Total "))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))


            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "Section3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)

            If mblnShowSummaryAtBootom = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            ElseIf mblnShowSummaryOnNextPage = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "True"
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            End If

            Call ReportFunction.SetRptDecimal(objRpt, "FldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol52")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol61")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol62")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol61")

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CostCenterDetailReport_New; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#Region " Combined JV "
    Dim iColumn_CombinedJVReport As New IColumnCollection
    Public Property Field_OnCombinedJVReport() As IColumnCollection
        Get
            Return iColumn_CombinedJVReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_CostCenterDetailReport = value
        End Set
    End Property

    Private Sub Create_OnCombinedJVReport()
        Try
            iColumn_CombinedJVReport.Clear()
            iColumn_CombinedJVReport.Add(New IColumn("Account_code", Language.getMessage(mstrModuleName, 6, "Account No.")))
            iColumn_CombinedJVReport.Add(New IColumn("Account_Name", Language.getMessage(mstrModuleName, 7, "Account Name")))
            iColumn_CombinedJVReport.Add(New IColumn("Name", Language.getMessage(mstrModuleName, 18, "Description")))
            iColumn_CombinedJVReport.Add(New IColumn("Debit", Language.getMessage(mstrModuleName, 8, "Debit Amount")))
            iColumn_CombinedJVReport.Add(New IColumn("Credit", Language.getMessage(mstrModuleName, 9, "Credit Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnCombinedJVReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_CombinedJVReport(ByVal xDatabaseName As String _
                                               , ByVal xUserUnkid As Integer _
                                               , ByVal xYearUnkid As Integer _
                                               , ByVal xCompanyUnkid As Integer _
                                               , ByVal xPeriodStart As Date _
                                               , ByVal xPeriodEnd As Date _
                                               , ByVal xUserModeSetting As String _
                                               , ByVal xOnlyApproved As Boolean _
                                               , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                               , ByVal blnApplyUserAccessFilter As Boolean _
                                               , ByVal strfmtCurrency As String _
                                               , ByVal intBaseCurrencyId As Integer _
                                               , ByVal strUserName As String _
                                               ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strfmtCurrency, intBaseCurrencyId, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim objTranHead As New clsTransactionHead
        Dim strMainIDs, strEmpIDs, strCCIDs As String 'Tran. Head Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainOthersIDs, strEmpOthersIDs, strCCOthersIDs As String 'Tran. Head NOT Used in Comp., Emp., CCenter Account Conffig.
        Dim intLoanIDusedinJV As Integer = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
        Dim intAdvanceIDusedinJV As Integer = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
        Dim intSavingIDusedinJV As Integer = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
        Dim intCashIDusedinJV As Integer = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
        Dim intBankIDusedinJV As Integer = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
        Dim strMainJVIDs As String = ""
        Dim strEmpJVIDs As String = ""
        Dim strCCJVIDs As String = ""
        Dim objLoan As New clsLoan_Scheme
        Dim objSaving As New clsSavingScheme
        Dim objPPA As New clsActivity_Master
        Dim strMainLoanIDs, strEmpLoanIDs, strCCLoanIDs As String 'Loan Scheme Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainOthersLoanIDs, strEmpOthersLoanIDs, strCCOthersLoanIDs As String 'Loan Scheme NOT Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainSavingIDs, strEmpSavingIDs, strCCSavingIDs As String 'Loan Scheme Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainOthersSavingIDs, strEmpOthersSavingIDs, strCCOthersSavingIDs As String 'Loan Scheme NOT Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainActivityIDs, strEmpActivityIDs, strCCActivityIDs As String 'Loan Scheme Used in Comp., Emp., CCenter Account Conffig.
        Dim strMainOthersActivityIDs, strEmpOthersActivityIDs, strCCOthersActivityIDs As String 'Loan Scheme NOT Used in Comp., Emp., CCenter Account Conffig.

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mintBaseCurrencyId = intBaseCurrencyId
            'Sohail (21 Aug 2015) -- End

            '**********************************************************************************************************
            '*****   PLEASE MAKE EQUAL CHANGES IN Sun_JV_Export_CombinedJVReport  AND  iScala_JV_Export_CombinedJVReport  *****
            '**********************************************************************************************************

            strMainIDs = objTranHead.GetTranHeadIDUsedInJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpIDs = objTranHead.GetTranHeadIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCIDs = objTranHead.GetTranHeadIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            strMainOthersIDs = objTranHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpOthersIDs = objTranHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCOthersIDs = objTranHead.GetTranHeadIDUsedInOtherJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            '*** Loan
            strMainLoanIDs = objLoan.GetLoanSchemeIDUsedInJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpLoanIDs = objLoan.GetLoanSchemeIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCLoanIDs = objLoan.GetLoanSchemeIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            strMainOthersLoanIDs = objLoan.GetLoanSchemeIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpOthersLoanIDs = objLoan.GetLoanSchemeIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCOthersLoanIDs = objLoan.GetLoanSchemeIDUsedInOtherJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            '*** Saving
            strMainSavingIDs = objSaving.GetSavingSchemeIDUsedInJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpSavingIDs = objSaving.GetSavingSchemeIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCSavingIDs = objSaving.GetSavingSchemeIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            strMainOthersSavingIDs = objSaving.GetSavingSchemeIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpOthersSavingIDs = objSaving.GetSavingSchemeIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCOthersSavingIDs = objSaving.GetSavingSchemeIDUsedInOtherJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            '*** Pay Per Activity
            strMainActivityIDs = objPPA.GetActivityIDUsedInJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpActivityIDs = objPPA.GetActivityIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCActivityIDs = objPPA.GetActivityIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            strMainOthersActivityIDs = objPPA.GetActivityIDUsedInOtherJV(enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strEmpOthersActivityIDs = objPPA.GetActivityIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, xPeriodEnd)
            strCCOthersActivityIDs = objPPA.GetActivityIDUsedInOtherJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, xPeriodEnd)
            'Sohail (25 Jul 2020) - [xPeriodEnd]

            '*** Advance
            If objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.ADVANCE, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intAdvanceIDusedinJV = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION
                strEmpJVIDs &= ", " & CStr(enJVTransactionType.ADVANCE)
            ElseIf objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, enJVTransactionType.ADVANCE, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intAdvanceIDusedinJV = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION
                strCCJVIDs &= ", " & CStr(enJVTransactionType.ADVANCE)
            Else 'enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                intAdvanceIDusedinJV = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                strMainJVIDs &= ", " & CStr(enJVTransactionType.ADVANCE)
            End If

            '*** CASH
            If objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.CASH, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intCashIDusedinJV = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION
                strEmpJVIDs &= ", " & CStr(enJVTransactionType.CASH)
            ElseIf objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, enJVTransactionType.CASH, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intCashIDusedinJV = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION
                strCCJVIDs &= ", " & CStr(enJVTransactionType.CASH)
            Else 'enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                intCashIDusedinJV = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                strMainJVIDs &= ", " & CStr(enJVTransactionType.CASH)
            End If

            '*** BANK
            If objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intBankIDusedinJV = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION
                strEmpJVIDs &= ", " & CStr(enJVTransactionType.BANK)
            ElseIf objTranHead.IsTranHeadIDUsedInJV(enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION, enJVTransactionType.BANK, 0, xPeriodEnd) = True Then
                'Sohail (25 Jul 2020) - [xPeriodEnd]
                intBankIDusedinJV = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION
                strCCJVIDs &= ", " & CStr(enJVTransactionType.BANK)
            Else 'enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                intBankIDusedinJV = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION
                strMainJVIDs &= ", " & CStr(enJVTransactionType.BANK)
            End If

            If strMainJVIDs.Length > 0 Then
                strMainJVIDs = Mid(strMainJVIDs, 3)
            Else
                strMainJVIDs = "0"
            End If
            If strEmpJVIDs.Length > 0 Then
                strEmpJVIDs = Mid(strEmpJVIDs, 3)
            Else
                strEmpJVIDs = "0"
            End If
            If strCCJVIDs.Length > 0 Then
                strCCJVIDs = Mid(strCCJVIDs, 3)
            Else
                strCCJVIDs = "0"
            End If

            objDataOperation = New clsDataOperation


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            If mintBaseCurrencyId <= 0 Then
                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            End If
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            Call FilterTitleAndFilterQuery()

            '*** Main Ledger

            StrQ = "SELECT  payperiodunkid " & _
                          ", period_name " & _
                          ", account_code " & _
                          ", account_name " & _
                          ", tranheadcode " & _
                          ", trnheadname " & _
                          ", 0 AS Unkid " & _
                          ", '' AS Code " & _
                          ", '' Name " & _
                   ", (Dr * " & mdecEx_Rate & ") AS Debit " & _
                   ", (Cr * " & mdecEx_Rate & ") AS Credit " & _
                          ", referencecodeid " & _
                          ", referencenameid " & _
                          ", referencetypeid " & _
                          ", shortname "


            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName " & _
                         ", ISNULL(a.costcenterunkid, 0) AS CCId " & _
                         ", ISNULL(a.costcentercode, '') AS CCCode " & _
                         ", ISNULL(a.costcentername, '') AS CCName "
            End If

            StrQ &= "FROM    ( SELECT    payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", add_deduct " & _
                                      ", CASE WHEN add_deduct = 1 " & _
                                                  "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Dr " & _
                                      ", CASE WHEN add_deduct = 2 " & _
                                                  "OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Cr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
            If mblnShowGroupByCCenterGroup Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwCompPaymentJV.costcenterunkid " & _
                '        ", costcentercode " & _
                '        ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                       ", CC.costcentercode " & _
                       ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= "FROM      vwCompPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwCompPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "          WHERE     payperiodunkid = @payperiodunkid " & _
                              "AND  ((transactiontype_Id = " & enJVTransactionType.TRANSACTION_HEAD & " AND tranheadunkid NOT IN (" & strMainOthersIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.LOAN & " AND tranheadunkid NOT IN (" & strMainOthersLoanIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.SAVINGS & " AND tranheadunkid NOT IN (" & strMainOthersSavingIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.PAY_PER_ACTIVITY & " AND tranheadunkid NOT IN (" & strMainOthersActivityIDs & ")) " & _
                                        "OR  transactiontype_Id IN (" & strMainJVIDs & ") " & _
                                    ") "
            If intAdvanceIDusedinJV <> enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then
                StrQ &= "AND transactiontype_Id <> " & enJVTransactionType.ADVANCE & " "
            End If
            If intCashIDusedinJV <> enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then
                StrQ &= "AND transactiontype_Id <> " & enJVTransactionType.CASH & " "
            End If
            If intBankIDusedinJV <> enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then
                StrQ &= "AND transactiontype_Id <> " & enJVTransactionType.BANK & " "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwCompPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwCompPaymentJV.amount <> 0 "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "GROUP BY  payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", add_deduct " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
            If mblnShowGroupByCCenterGroup = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwCompPaymentJV.costcenterunkid " & _
                '          ", costcentercode " & _
                '          ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                          ", CC.costcentercode " & _
                          ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If

            If strEmpIDs.Length > 0 Then
                StrQ &= "UNION ALL " & _
                              "SELECT    payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", add_deduct " & _
                                      ", CASE WHEN add_deduct = 1 " & _
                                                  "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Dr " & _
                                      ", CASE WHEN add_deduct = 2 " & _
                                                  "OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Cr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
                If mblnShowGroupByCCenterGroup Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                    '        ", costcentercode " & _
                    '        ", costcentername "
                    StrQ &= ", C.costcenterunkid " & _
                            ", CC.costcentercode " & _
                            ", CC.costcentername "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= "FROM      vwEmpPaymentJV " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = vwEmpPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "      WHERE     payperiodunkid = @payperiodunkid " & _
                              "AND  ((transactiontype_Id = " & enJVTransactionType.TRANSACTION_HEAD & " AND tranheadunkid IN (" & strEmpIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.LOAN & " AND tranheadunkid IN (" & strEmpLoanIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.SAVINGS & " AND tranheadunkid IN (" & strEmpSavingIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.PAY_PER_ACTIVITY & " AND tranheadunkid IN (" & strEmpActivityIDs & ")) " & _
                                        "OR  transactiontype_Id IN (" & strEmpJVIDs & ") " & _
                                        ") " & _
                              "AND accountunkid <= 0 "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND vwEmpPaymentJV.BranchId = " & mintBranchId
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                    'Sohail (21 Aug 2015) -- End
                End If

                If mblnIgnoreZero = True Then
                    StrQ &= " AND vwEmpPaymentJV.amount <> 0 "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "GROUP BY  payperiodunkid " & _
                                          ", period_name " & _
                                          ", accountunkid " & _
                                          ", account_code " & _
                                          ", account_name " & _
                                          ", tranheadcode " & _
                                          ", trnheadname " & _
                                          ", add_deduct " & _
                                          ", referencecodeid " & _
                                          ", referencenameid " & _
                                          ", referencetypeid " & _
                                          ", shortname "
                If mblnShowGroupByCCenterGroup = True Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                    '          ", costcentercode " & _
                    '          ", costcentername "
                    StrQ &= ", C.costcenterunkid " & _
                              ", CC.costcentercode " & _
                              ", CC.costcentername "
                    'Sohail (21 Aug 2015) -- End
                End If
            End If

            If strCCIDs.Length > 0 Then
                StrQ &= "UNION ALL " & _
                              "SELECT    payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", add_deduct " & _
                                      ", CASE WHEN add_deduct = 1 " & _
                                                  "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Dr " & _
                                      ", CASE WHEN add_deduct = 2 " & _
                                                  "OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Cr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "

                If mblnShowGroupByCCenterGroup Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= ", vwCCPaymentJV.costcenterunkid " & _
                    '        ", costcentercode " & _
                    '        ", costcentername "
                    StrQ &= ", C.costcenterunkid " & _
                           ", CC.costcentercode " & _
                           ", CC.costcentername "
                    'Sohail (21 Aug 2015) -- End
                End If

                StrQ &= "FROM      vwCCPaymentJV " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = vwCCPaymentJV.employeeunkid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "      WHERE     payperiodunkid = @payperiodunkid " & _
                              "AND  ((transactiontype_Id = " & enJVTransactionType.TRANSACTION_HEAD & " AND tranheadunkid IN (" & strCCIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.LOAN & " AND tranheadunkid IN (" & strCCLoanIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.SAVINGS & " AND tranheadunkid IN (" & strCCSavingIDs & ")) " & _
                                        "OR (transactiontype_Id = " & enJVTransactionType.PAY_PER_ACTIVITY & " AND tranheadunkid IN (" & strCCActivityIDs & ")) " & _
                                        "OR  transactiontype_Id IN (" & strCCJVIDs & ") " & _
                                        ") " & _
                              "AND accountunkid <= 0 "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIsActive = False Then
                '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                If mintBranchId > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= " AND vwCCPaymentJV.BranchId = " & mintBranchId
                    StrQ &= " AND T.stationunkid = " & mintBranchId
                    'Sohail (21 Aug 2015) -- End
                End If

                If mblnIgnoreZero = True Then
                    StrQ &= " AND vwCCPaymentJV.amount <> 0 "
                End If

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mstrUserAccessFilter = "" Then
                '    If UserAccessLevel._AccessLevel.Length > 0 Then
                '        StrQ &= UserAccessLevel._AccessLevelFilterString
                '    End If
                'Else
                '    StrQ &= mstrUserAccessFilter
                'End If
                'Sohail (21 Aug 2015) -- End

                StrQ &= "GROUP BY  payperiodunkid " & _
                                          ", period_name " & _
                                          ", accountunkid " & _
                                          ", account_code " & _
                                          ", account_name " & _
                                          ", tranheadcode " & _
                                          ", trnheadname " & _
                                          ", add_deduct " & _
                                          ", referencecodeid " & _
                                          ", referencenameid " & _
                                          ", referencetypeid " & _
                                          ", shortname "
                If mblnShowGroupByCCenterGroup = True Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrQ &= ", vwCCPaymentJV.costcenterunkid " & _
                    '          ", costcentercode " & _
                    '          ", costcentername "
                    StrQ &= ", C.costcenterunkid " & _
                            ", CC.costcentercode " & _
                            ", CC.costcentername "
                    'Sohail (21 Aug 2015) -- End
                End If
            End If
            StrQ &= ") AS a "
            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If
            StrQ &= "WHERE   1 = 1 "

            StrQ &= Me._FilterQuery
            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable1")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable1").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVCompanyConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCompanyConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                rpt_Rows.Item("Column4") = "----" ' dtRow.Item("account_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))
                rpt_Rows.Item("Column7") = "----" 'dtRow.Item("Unkid")
                If CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.AccountName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCompanyConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                End If
                If mblnShowGroupByCCenterGroup = True Then
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("CCId")
                    rpt_Rows.Item("Column15") = dtRow.Item("CCCode")
                    rpt_Rows.Item("Column16") = dtRow.Item("CCName")
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                End If
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            StrQ = "SELECT  payperiodunkid " & _
                  ", period_name " & _
                  ", account_code " & _
                  ", account_name " & _
                  ", tranheadcode " & _
                 ", trnheadname " & _
                  ", employeeunkid AS Unkid " & _
                  ", employeecode AS Code " & _
                  ", employeename AS Name " & _
                  ", Dr AS Debit " & _
                  ", Cr AS Credit " & _
                 ", referencecodeid " & _
                 ", referencenameid " & _
                 ", referencetypeid " & _
                 ", shortname "
            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName " & _
                         ", ISNULL(a.costcenterunkid, 0) AS CCId " & _
                         ", ISNULL(a.costcentercode, '') AS CCCode " & _
                         ", ISNULL(a.costcentername, '') AS CCName "
            End If
            StrQ &= "FROM    ( SELECT    payperiodunkid " & _
                              ", period_name " & _
                              ", accountunkid " & _
                              ", account_code " & _
                              ", tranheadcode " & _
                             ", trnheadname " & _
                             ", account_name " & _
                              ", vwEmpPaymentJV.employeeunkid " & _
                              ", vwEmpPaymentJV.employeecode " & _
                              ", employeename " & _
                              ", add_deduct " & _
                              ", CASE WHEN add_deduct = 1 " & _
                                          "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                     "ELSE 0 " & _
                                "END AS Dr " & _
                              ", CASE WHEN add_deduct = 2 " & _
                                          "OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                     "ELSE 0 " & _
                                "END AS Cr " & _
                             ", referencecodeid " & _
                             ", referencenameid " & _
                             ", referencetypeid " & _
                             ", shortname "
            If mblnShowGroupByCCenterGroup Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                '        ", costcentercode " & _
                '        ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                       ", CC.costcentercode " & _
                       ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If

            StrQ &= "FROM      vwEmpPaymentJV " & _
                    "JOIN hremployee_master ON hremployee_master.employeeunkid = vwEmpPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= " WHERE     payperiodunkid = @payperiodunkid " & _
                     "AND ((transactiontype_Id = " & enJVTransactionType.TRANSACTION_HEAD & " AND tranheadunkid  IN (" & strEmpIDs & ")) " & _
                              "OR (transactiontype_Id = " & enJVTransactionType.LOAN & " AND tranheadunkid  IN (" & strEmpLoanIDs & ")) " & _
                              "OR (transactiontype_Id = " & enJVTransactionType.SAVINGS & " AND tranheadunkid  IN (" & strEmpSavingIDs & ")) " & _
                              "OR (transactiontype_Id = " & enJVTransactionType.PAY_PER_ACTIVITY & " AND tranheadunkid  IN (" & strEmpActivityIDs & ")) " & _
                              "OR  transactiontype_Id IN (" & strEmpJVIDs & ") " & _
                          ") " & _
                      "AND accountunkid > 0 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwEmpPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwEmpPaymentJV.amount <> 0 "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "GROUP BY  payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", vwEmpPaymentJV.employeeunkid " & _
                                      ", vwEmpPaymentJV.employeecode " & _
                                      ", employeename " & _
                                      ", add_deduct " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname "
            If mblnShowGroupByCCenterGroup = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= ", vwEmpPaymentJV.costcenterunkid " & _
                '          ", costcentercode " & _
                '          ", costcentername "
                StrQ &= ", C.costcenterunkid " & _
                          ", CC.costcentercode " & _
                          ", CC.costcentername "
                'Sohail (21 Aug 2015) -- End
            End If
            StrQ &= ") AS a "

            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If
            StrQ &= "WHERE   1 = 1 "

            StrQ &= Me._FilterQuery
            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable2")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim objIdentity As clsIdentity_tran
            Dim dsID As DataSet
            For Each dtRow As DataRow In dsList.Tables("DataTable2").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.IDType Then
                    objIdentity = New clsIdentity_tran
                    dsID = objIdentity.GetIdentity_tranList("ID", CInt(dtRow.Item("Unkid")), CInt(dtRow.Item("referencetypeid")))
                    If dsID.Tables("ID").Rows.Count > 0 Then
                        rpt_Rows.Item("Column3") = dsID.Tables("ID").Rows(0).Item("identity_no").ToString
                    Else
                        rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                    End If
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.EmployeeCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.AccountCode_EmployeeCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code") & " - " & dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.EmployeeCode_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code") & " - " & dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVEmployeeConfigRefCode.ShortName_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("account_code")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                rpt_Rows.Item("Column4") = dtRow.Item("Code") 'dtRow.Item("account_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))
                rpt_Rows.Item("Column7") = "----" 'dtRow.Item("Unkid")
                If CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.AccountName_EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.TranHeadName_EmployeeName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVEmployeeConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                End If

                If mblnShowGroupByCCenterGroup = True Then
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("CCId")
                    rpt_Rows.Item("Column15") = dtRow.Item("CCCode")
                    rpt_Rows.Item("Column16") = dtRow.Item("CCName")
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            StrQ = "SELECT  payperiodunkid " & _
                          ", period_name " & _
                          ", account_code " & _
                          ", account_name " & _
                          ", tranheadcode " & _
                          ", trnheadname " & _
                          ", a.costcenterunkid AS Unkid " & _
                          ", a.costcentercode AS Code " & _
                          ", a.costcentername AS Name " & _
                          ", Dr AS Debit " & _
                          ", Cr AS Credit " & _
                          ", referencecodeid " & _
                          ", referencenameid " & _
                          ", referencetypeid " & _
                          ", shortname "
            If mblnShowGroupByCCenterGroup Then
                StrQ &= ", ISNULL(groupmasterunkid, 0) AS CCGroupId " & _
                         ", ISNULL(groupcode, '') AS CCGroupCode " & _
                         ", ISNULL(groupname, '') AS CCGroupName "
            End If
            StrQ &= "FROM    ( SELECT    payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", account_name " & _
                                      ", C.costcenterunkid " & _
                                      ", CC.costcentercode " & _
                                      ", CC.costcentername " & _
                                      ", add_deduct " & _
                                      ", CASE WHEN add_deduct = 1 " & _
                                                  "OR add_deduct = 3 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Dr " & _
                                      ", CASE WHEN add_deduct = 2 " & _
                                                  "OR add_deduct = 4 THEN SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) " & _
                                             "ELSE 0 " & _
                                        "END AS Cr " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname " & _
                              "FROM      vwCCPaymentJV " & _
                              "JOIN hremployee_master ON hremployee_master.employeeunkid = vwCCPaymentJV.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         stationunkid " & _
                            "        ,deptgroupunkid " & _
                            "        ,departmentunkid " & _
                            "        ,sectiongroupunkid " & _
                            "        ,sectionunkid " & _
                            "        ,unitgroupunkid " & _
                            "        ,unitunkid " & _
                            "        ,teamunkid " & _
                            "        ,classgroupunkid " & _
                            "        ,classunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_transfer_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         cctranheadvalueid AS costcenterunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_cctranhead_tran " & _
                            "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                    "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "WHERE     payperiodunkid = @payperiodunkid " & _
                              "AND ((transactiontype_Id = " & enJVTransactionType.TRANSACTION_HEAD & " AND tranheadunkid  IN (" & strCCIDs & ")) " & _
                                      "OR (transactiontype_Id = " & enJVTransactionType.LOAN & " AND tranheadunkid  IN (" & strCCLoanIDs & ")) " & _
                                      "OR (transactiontype_Id = " & enJVTransactionType.SAVINGS & " AND tranheadunkid  IN (" & strCCSavingIDs & ")) " & _
                                      "OR (transactiontype_Id = " & enJVTransactionType.PAY_PER_ACTIVITY & " AND tranheadunkid  IN (" & strCCActivityIDs & ")) " & _
                                      "OR  transactiontype_Id IN (" & strCCJVIDs & ") " & _
                                  ") " & _
                              "AND accountunkid > 0 "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIsActive = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            If mintBranchId > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'StrQ &= " AND vwCCPaymentJV.BranchId = " & mintBranchId
                StrQ &= " AND T.stationunkid = " & mintBranchId
                'Sohail (21 Aug 2015) -- End
            End If

            If mblnIgnoreZero = True Then
                StrQ &= " AND vwCCPaymentJV.amount <> 0 "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "GROUP BY  payperiodunkid " & _
                                      ", period_name " & _
                                      ", accountunkid " & _
                                      ", account_code " & _
                                      ", account_name " & _
                                      ", tranheadcode " & _
                                      ", trnheadname " & _
                                      ", C.costcenterunkid " & _
                                      ", CC.costcentercode " & _
                                      ", CC.costcentername " & _
                                      ", add_deduct " & _
                                      ", referencecodeid " & _
                                      ", referencenameid " & _
                                      ", referencetypeid " & _
                                      ", shortname " & _
                    ") AS a "
            If mblnShowGroupByCCenterGroup = True Then
                StrQ &= "LEFT JOIN prcostcenter_master ON a.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid=prcostcenter_master.costcentergroupmasterunkid "
            End If
            StrQ &= "WHERE   1 = 1 "

            StrQ &= Me._FilterQuery
            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable2")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dtRow As DataRow In dsList.Tables("DataTable2").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow


                rpt_Rows.Item("Column1") = dtRow.Item("payperiodunkid")
                rpt_Rows.Item("Column2") = dtRow.Item("period_name")
                If CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.AccountCode_CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code") & " - " & dtRow.Item("Code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName_AccountCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("account_code")
                ElseIf CInt(dtRow.Item("referencecodeid")) = enJVCostCenterloyeeConfigRefCode.ShortName_CostCenterCode Then
                    rpt_Rows.Item("Column3") = dtRow.Item("shortname") & " - " & dtRow.Item("Code")
                Else
                    rpt_Rows.Item("Column3") = dtRow.Item("account_code")
                End If
                rpt_Rows.Item("Column4") = "----" 'dtRow.Item("account_name")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), GUI.fmtCurrency)
                'rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("Debit")), strfmtCurrency)
                rpt_Rows.Item("Column6") = Format(CDec(dtRow.Item("Credit")), strfmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Rows.Item("Column81") = CDec(dtRow.Item("Debit"))
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Credit"))
                rpt_Rows.Item("Column7") = dtRow.Item("Code") ' dtRow.Item("Unkid")
                If CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.AccountName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.TranHeadName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("trnheadname") & " - " & dtRow.Item("Name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_AccountName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("account_name")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_TranHeadName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("trnheadname")
                ElseIf CInt(dtRow.Item("referencenameid")) = enJVCostCenterloyeeConfigRefName.ShortName_CostCenterName Then
                    rpt_Rows.Item("Column8") = dtRow.Item("shortname") & " - " & dtRow.Item("Name")
                Else
                    rpt_Rows.Item("Column8") = dtRow.Item("account_name")
                End If

                If mblnShowGroupByCCenterGroup = True Then
                    rpt_Rows.Item("Column11") = dtRow.Item("CCGroupId")
                    rpt_Rows.Item("Column12") = dtRow.Item("CCGroupCode")
                    rpt_Rows.Item("Column13") = dtRow.Item("CCGroupName")
                    rpt_Rows.Item("Column14") = dtRow.Item("Unkid")
                    rpt_Rows.Item("Column15") = dtRow.Item("Code")
                    rpt_Rows.Item("Column16") = dtRow.Item("Name")
                Else
                    rpt_Rows.Item("Column11") = ""
                    rpt_Rows.Item("Column12") = ""
                    rpt_Rows.Item("Column13") = ""
                    rpt_Rows.Item("Column14") = ""
                    rpt_Rows.Item("Column15") = ""
                    rpt_Rows.Item("Column16") = ""
                End If

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            Dim mdecDebitTot As Decimal = 0
            Dim mdecCreditTot As Decimal = 0
            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                mdecDebitTot = rpt_Data.Tables("ArutiTable").Compute("SUM(Column81)", "")
            End If
            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                mdecCreditTot = rpt_Data.Tables("ArutiTable").Compute("SUM(Column82)", "")
            End If

            Dim decDebitCCGrpTot As Decimal = 0
            Dim decCreditCCGrpTot As Decimal = 0
            Dim decDebitCCTot As Decimal = 0
            Dim decCreditCCTot As Decimal = 0
            Dim strPrevCCGroupId As String = ""
            Dim strPrevCCId As String = ""

            If mblnShowGroupByCCenterGroup = True Then
                Dim dtTable As New DataTable("ArutiTable")
                dtTable = New DataView(rpt_Data.Tables("ArutiTable"), "", "Column11, Column14 ", DataViewRowState.CurrentRows).ToTable
                rpt_Data.Tables.Remove("ArutiTable")
                rpt_Data.Tables.Add(dtTable)
                rpt_Data.AcceptChanges()
            End If

            For Each dtRow As DataRow In rpt_Data.Tables("ArutiTable").Rows
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dtRow.Item("Column9") = Format(mdecDebitTot, GUI.fmtCurrency)
                'dtRow.Item("Column10") = Format(mdecCreditTot, GUI.fmtCurrency)
                dtRow.Item("Column9") = Format(mdecDebitTot, strfmtCurrency)
                dtRow.Item("Column10") = Format(mdecCreditTot, strfmtCurrency)
                'Sohail (21 Aug 2015) -- End

                If mblnShowGroupByCCenterGroup = True Then
                    If strPrevCCGroupId <> dtRow.Item("Column11").ToString OrElse strPrevCCId <> dtRow.Item("Column14").ToString Then
                        decDebitCCTot = CDec(dtRow.Item("Column5"))
                        decCreditCCTot = CDec(dtRow.Item("Column6"))
                        strPrevCCId = dtRow.Item("Column14").ToString
                    Else
                        decDebitCCTot += CDec(dtRow.Item("Column5"))
                        decCreditCCTot += CDec(dtRow.Item("Column6"))
                    End If

                    If strPrevCCGroupId <> dtRow.Item("Column11").ToString Then
                        decDebitCCGrpTot = CDec(dtRow.Item("Column5"))
                        decCreditCCGrpTot = CDec(dtRow.Item("Column6"))
                        strPrevCCGroupId = dtRow.Item("Column11").ToString
                    Else
                        decDebitCCGrpTot += CDec(dtRow.Item("Column5"))
                        decCreditCCGrpTot += CDec(dtRow.Item("Column6"))
                    End If
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dtRow.Item("Column17") = Format(decDebitCCGrpTot, GUI.fmtCurrency)
                    'dtRow.Item("Column18") = Format(decCreditCCGrpTot, GUI.fmtCurrency)
                    'dtRow.Item("Column19") = Format(decDebitCCTot, GUI.fmtCurrency)
                    'dtRow.Item("Column20") = Format(decCreditCCTot, GUI.fmtCurrency)
                    dtRow.Item("Column17") = Format(decDebitCCGrpTot, strfmtCurrency)
                    dtRow.Item("Column18") = Format(decCreditCCGrpTot, strfmtCurrency)
                    dtRow.Item("Column19") = Format(decDebitCCTot, strfmtCurrency)
                    dtRow.Item("Column20") = Format(decCreditCCTot, strfmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                Else
                    dtRow.Item("Column17") = ""
                    dtRow.Item("Column18") = ""
                    dtRow.Item("Column19") = ""
                    dtRow.Item("Column20") = ""
                End If

                dtRow.AcceptChanges()
            Next

            objRpt = New ArutiReport.Designer.rptMainLedger

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 17, "Prepared By :"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", strUserName)
                'Sohail (21 Aug 2015) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 29, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 19, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 20, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtMainAccCode", Language.getMessage(mstrModuleName, 32, "ACCOUNT #"))
            Call ReportFunction.TextChange(objRpt, "txtEmpAccCode", Language.getMessage(mstrModuleName, 33, "EMP. CODE"))
            Call ReportFunction.TextChange(objRpt, "txtCCAccCode", Language.getMessage(mstrModuleName, 21, "CCENTER CODE"))
            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 22, "Description"))
            Call ReportFunction.TextChange(objRpt, "txtDebit2", Language.getMessage(mstrModuleName, 8, "Debit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtCredit2", Language.getMessage(mstrModuleName, 9, "Credit Amount"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName2", Language.getMessage(mstrModuleName, 10, "Period :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 14, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 11, "Total "))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "Section3", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection1", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", False)
            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", False)

            If mblnShowSummaryAtBootom = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            ElseIf mblnShowSummaryOnNextPage = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", False)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "True"
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
                objRpt.DataDefinition.FormulaFields("fmlShowOnNewPage").Text = "False"
            End If

            Call ReportFunction.SetRptDecimal(objRpt, "FldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol52")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol61")
            Call ReportFunction.SetRptDecimal(objRpt, "FldCol62")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol51")
            Call ReportFunction.SetRptDecimal(objRpt, "SumofFldCol61")

            If mblnShowGroupByCCenterGroup = True Then
                Call ReportFunction.TextChange(objRpt, "txtCCGroup", Language.getMessage(mstrModuleName, 24, "Cost Center Group :"))
                Call ReportFunction.TextChange(objRpt, "txtCC", Language.getMessage(mstrModuleName, 25, "Cost Center :"))

                Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 26, "Sub Total :"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 27, "Group Total :"))

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", False)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection4", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection5", True)

                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CombinedJVReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, " Debit Amount >=")
            Language.setMessage(mstrModuleName, 2, " Debit Amount <=")
            Language.setMessage(mstrModuleName, 3, " Credit Amount >=")
            Language.setMessage(mstrModuleName, 4, " Credit Amount <=")
            Language.setMessage(mstrModuleName, 5, " Order By :")
            Language.setMessage(mstrModuleName, 6, "Account No.")
            Language.setMessage(mstrModuleName, 7, "Account Name")
            Language.setMessage(mstrModuleName, 8, "Debit Amount")
            Language.setMessage(mstrModuleName, 9, "Credit Amount")
            Language.setMessage(mstrModuleName, 10, "Period :")
            Language.setMessage(mstrModuleName, 11, "Total")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Page :")
            Language.setMessage(mstrModuleName, 15, "Employee")
            Language.setMessage(mstrModuleName, 16, "Cost Center")
            Language.setMessage(mstrModuleName, 17, "Prepared By :")
            Language.setMessage(mstrModuleName, 18, "Description")
            Language.setMessage(mstrModuleName, 19, "Approved By :")
            Language.setMessage(mstrModuleName, 20, "Received By :")
            Language.setMessage(mstrModuleName, 21, "CCENTER CODE")
            Language.setMessage(mstrModuleName, 22, "Description")
            Language.setMessage(mstrModuleName, 23, "Branch :")
            Language.setMessage(mstrModuleName, 24, "Cost Center Group :")
            Language.setMessage(mstrModuleName, 25, "Cost Center :")
            Language.setMessage(mstrModuleName, 26, "Sub Total :")
            Language.setMessage(mstrModuleName, 27, "Group Total :")
            Language.setMessage(mstrModuleName, 28, "Cost Center Name")
            Language.setMessage(mstrModuleName, 29, "Checked By :")
            Language.setMessage(mstrModuleName, 30, "Employee Name")
            Language.setMessage(mstrModuleName, 31, "Emp. Code")
            Language.setMessage(mstrModuleName, 32, "ACCOUNT #")
            Language.setMessage(mstrModuleName, 33, "EMP. CODE")
            Language.setMessage(mstrModuleName, 34, "CC. Code")
            Language.setMessage(mstrModuleName, 35, "Currency :")
            Language.setMessage(mstrModuleName, 36, "Exchange Rate:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

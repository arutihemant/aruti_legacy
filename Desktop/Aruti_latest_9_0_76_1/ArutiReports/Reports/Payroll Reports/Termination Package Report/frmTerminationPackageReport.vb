#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmTerminationPackageReport

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmTerminationPackageReport"
    Private objTPackage As clsTerminationPackageReport
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty

    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0

    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

    'Shani [ 24 NOV 2014 ] -- START
    Private mdtHourAmtMapping As New DataTable
    Private mstrNewHrsAmtMapping As String = ""
    Private mstrDeductionHeadIDs As String = ""
    Private mstrContributaionHeadIDs As String = ""
    'Shani [ 24 NOV 2014 ] -- END
    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    Private dtViewHead As DataView
    Private marrBonusHead() As String = {}
    'Sohail (08 Jul 2016) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()
        objTPackage = New clsTerminationPackageReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objTPackage.SetDefaultValue()

        _Show_ExcelExtra_Menu = False

        _Show_AdvanceFilter = True

    End Sub

#End Region

#Region " Private Enum "

    Private Enum enHeadTypeId
        Salary_Head = 1
        Days_In_Period = 2
        Conge_Preavis_Head = 3
        Base_Taxable_Head = 4
        Net_Pay_Head = 5
        Notification_Allowance = 6
        Head_13Th_Check = 7
        Base_Imposable_Head = 8
        Hours_And_Amount_Mapping = 9
        Deduction_Head_IDs = 10
        Contributaion_Head_IDs = 11
        Leave_Type = 12
        Termination_Factor = 13
        Child_Head = 14
        Dependent_Head = 15
        Currency = 16
        'Sohail (08 Jul 2016) -- Start
        'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
        Family_Allowance = 17
        Logement = 18
        IPRNet = 19
        Loan_Repayment = 20
        Advance_Repayment = 21
        Other_Deductions = 22
        Rate_of_Notice = 23
        Bonus_Head_IDs = 24
        INSS_35 = 25
        'Sohail (08 Jul 2016) -- End
    End Enum

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objLeave As New clsleavetype_master
        Dim objExRate As New clsExchangeRate
        Dim objTranHead As New clsTransactionHead
        Dim dsCombo As DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombo = objLeave.getListForCombo("Leave", True)
            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Leave")
                .SelectedValue = 0
            End With

            dsCombo = objExRate.getComboList("Currency", True)
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If
            'Sohail (08 Jul 2016) -- End
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombo.Tables("Currency")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , " calctype_id NOT IN (" & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Hours
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Shani [ 24 NOV 2014 ] -- START
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, , , , , , "typeof_id IN (" & enTypeOf.Salary & ", " & enTypeOf.Other_Earnings & ")  ")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id IN (" & enTypeOf.Salary & ", " & enTypeOf.Other_Earnings & ")  ")
            'Sohail (21 Aug 2015) -- End
            With cboSalaryHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True)
            'Sohail (21 Aug 2015) -- End
            With cboCongePreavisHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objTranHead.getComboList("Heads", True, enTranHeadType.Informational)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboDaysInPeriod
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboNotificationAllowance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With CboCheckHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (08 Jul 2016) -- Start
            ''Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            'With cboBaseTaxablehead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("Heads").Copy
            '    .SelectedValue = 0
            'End With

            'With cboBaseImposablehead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("Heads").Copy
            '    .SelectedValue = 0
            'End With
            'Sohail (08 Jul 2016) -- End

            With cboNetPayhead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboTerminationFactor
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboChildHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboDependentHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With
            'Shani [ 24 NOV 2014 ] -- END

            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            With cboRateOfNotice
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboIPRNet
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboINSS35
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With
            'Sohail (08 Jul 2016) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTranHead.getComboList("Heads", True, , , , , , "trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ") AND typeof_id NOT IN (" & enTypeOf.Salary & ")")
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ") AND typeof_id NOT IN (" & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            With cboOT1Amount
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & " ) AND typeof_id NOT IN (" & enTypeOf.Salary & ")")
            With cboBaseTaxablehead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboFamilyAllowance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboLogement
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            dsCombo = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "trnheadtype_id IN (" & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ")")
            With cboBaseImposablehead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboLoanRepayment
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboAdvanceRepayment
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With

            With cboOtherDeductions
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Heads").Copy
                .SelectedValue = 0
            End With
            'Sohail (08 Jul 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
            objLeave = Nothing
            objExRate = Nothing
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            objTPackage.setDefaultOrderBy(0)
            txtOrderBy.Text = objTPackage.OrderByDisplay
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboLeaveType.SelectedValue = 0
            mstrAdvanceFilter = ""

            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty
            mstrCurrency_Rate = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim objEmp As New clsEmployee_Master
        'Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0

        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Function
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Function
            ElseIf mdecEx_Rate <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, No exchange rate defined for this currency for the period selected."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Function
            ElseIf lvDeductionHead.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Select Deduction Head."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf lvContributaionHead.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Select Contributaion Head."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Select Currency."), enMsgBoxStyle.Information)
                cboCurrency.Focus() 'Sohail (17 May 2015)
                Exit Function
                'Sohail (17 May 2015) -- Start
                'Enhancement - 60.1 - Show Saving Balance and Saving Interest balance in separate row on Payslip for FDRC.
            ElseIf CInt(cboDaysInPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Select Head for Days In Period."), enMsgBoxStyle.Information)
                cboDaysInPeriod.Select()
                Exit Function
                'Sohail (17 May 2015) -- End
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            Dim objPeriod As New clscommom_period_Tran

            If objEmp._Isexclude_Payroll = True Then

                Dim dsPrevPeriod As DataSet = objPeriod.GetPreviousPeriod("PrevPeriod", enModuleReference.Payroll, CInt(cboPeriod.SelectedValue))
                Dim intPrevPeriod As Integer = CInt(cboPeriod.SelectedValue)
                If dsPrevPeriod.Tables("PrevPeriod").Rows.Count > 0 Then
                    intPrevPeriod = CInt(dsPrevPeriod.Tables("PrevPeriod").Rows(0).Item("periodunkid"))
                End If

                objTPackage._ProcessPeriodUnkID = intPrevPeriod
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPrevPeriod
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                'Sohail (21 Aug 2015) -- End
                objTPackage._ProcessPeriodName = objPeriod._Period_Name


                objTPackage._ProcessPeriodStartDate = objPeriod._Start_Date
                objTPackage._ProcessPeriodEndDate = objPeriod._End_Date

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTPackage._ProcessDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid)
                objTPackage._ProcessDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                'Sohail (21 Aug 2015) -- End
            Else
                objTPackage._ProcessPeriodUnkID = CInt(cboPeriod.SelectedValue)
                objTPackage._ProcessPeriodName = cboPeriod.Text

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                objTPackage._ProcessPeriodStartDate = objPeriod._Start_Date
                objTPackage._ProcessPeriodEndDate = objPeriod._End_Date

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTPackage._ProcessDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid)
                objTPackage._ProcessDatabaseName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                'Sohail (21 Aug 2015) -- End
            End If

            objTPackage._PeriodUnkID = CInt(cboPeriod.SelectedValue)
            objTPackage._PeriodName = cboPeriod.Text

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objTPackage._PeriodStartDate = objPeriod._Start_Date
            objTPackage._PeriodEndDate = objPeriod._End_Date

            objPeriod = Nothing

            objTPackage._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objTPackage._Emp_Name = cboEmployee.Text

            objTPackage._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objTPackage._LeaveTypeName = cboLeaveType.Text

            objTPackage._CurrencyCountryId = CInt(cboCurrency.SelectedValue)
            objTPackage._CurrencyName = cboCurrency.Text

            objTPackage._Ex_Rate = mdecEx_Rate
            objTPackage._Currency_Sign = mstrCurr_Sign
            objTPackage._Currency_Rate = mstrCurrency_Rate
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            objTPackage._BaseCurrency_Sign = mstrBaseCurrSign
            'Sohail (08 Jul 2016) -- End

            objTPackage._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objTPackage._IsFin_Close = FinancialYear._Object._IsFin_Close
            objTPackage._YearUnkId = FinancialYear._Object._YearUnkid
            objTPackage._DatabaseStartDate = FinancialYear._Object._Database_Start_Date
            objTPackage._DatabaseEndDate = FinancialYear._Object._Database_End_Date


            objTPackage._ViewByIds = mstrStringIds
            objTPackage._ViewIndex = mintViewIdx
            objTPackage._ViewByName = mstrStringName
            objTPackage._Analysis_Fields = mstrAnalysis_Fields
            objTPackage._Analysis_Join = mstrAnalysis_Join
            objTPackage._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTPackage._Report_GroupName = mstrReport_GroupName

            objTPackage._Advance_Filter = mstrAdvanceFilter

            'Shani [ 24 NOV 2014 ] -- START
            objTPackage._SalaryHeadID = CInt(cboSalaryHead.SelectedValue)
            objTPackage._CongePreavisHeadID = CInt(cboCongePreavisHead.SelectedValue)
            objTPackage._NotificationAllowanceHeadID = CInt(cboNotificationAllowance.SelectedValue)
            objTPackage._DaysInPeriodHeadID = CInt(cboDaysInPeriod.SelectedValue)
            objTPackage._BaseImposableHeadId = CInt(cboBaseImposablehead.SelectedValue)
            objTPackage._BaseTaxableHeadID = CInt(cboBaseTaxablehead.SelectedValue)
            objTPackage._NetPayHeadID = CInt(cboNetPayhead.SelectedValue)
            objTPackage._NewHrsAmtMapping = mstrNewHrsAmtMapping

            Dim lst As List(Of String) = (From p In lvContributaionHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            mstrContributaionHeadIDs = String.Join(",", lst.ToArray())
            objTPackage._ContributionIDs = mstrContributaionHeadIDs

            lst = Nothing
            lst = (From p In lvDeductionHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString())).ToList
            mstrDeductionHeadIDs = String.Join(",", lst.ToArray())
            objTPackage._DeductionIDs = mstrDeductionHeadIDs

            objTPackage._TerminationFactorID = CInt(cboTerminationFactor.SelectedValue)
            objTPackage._TerminationFactorName = cboTerminationFactor.Text
            objTPackage._NoticeDate = dtpNoticeDate.Value.Date

            objPeriod = New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objTPackage._PeriodStartDate = objPeriod._Start_Date
            objTPackage._PeriodEndDate = objPeriod._End_Date

            objTPackage._ChildHead_ID = CInt(cboChildHead.SelectedValue)
            objTPackage._DependentHead_ID = CInt(cboDependentHead.SelectedValue)
            'Shani [ 24 NOV 2014 ] -- END

            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            objTPackage._FamilyAllowanceHead_ID = CInt(cboFamilyAllowance.SelectedValue)
            objTPackage._LogementHead_ID = CInt(cboLogement.SelectedValue)
            objTPackage._LoanRepaymentHead_ID = CInt(cboLoanRepayment.SelectedValue)
            objTPackage._LoanRepaymentHeadName = cboLoanRepayment.Text
            objTPackage._AdvanceRepaymentHead_ID = CInt(cboAdvanceRepayment.SelectedValue)
            objTPackage._AdvanceRepaymentHeadName = cboAdvanceRepayment.Text
            objTPackage._OtherDeductionsHead_ID = CInt(cboOtherDeductions.SelectedValue)
            objTPackage._OtherDeductionsHeadName = cboOtherDeductions.Text
            objTPackage._RateOfNoticeHead_ID = CInt(cboRateOfNotice.SelectedValue)
            objTPackage._IPRNetHead_ID = CInt(cboIPRNet.SelectedValue)
            objTPackage._IPRNetHeadName = cboIPRNet.Text
            objTPackage._INSS35Head_ID = CInt(cboINSS35.SelectedValue)
            objTPackage._INSS35HeadName = cboINSS35.Text

            marrBonusHead = (From p In dtViewHead.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToList.ToArray
            objTPackage._BonusHead_IDs = marrBonusHead
            'Sohail (08 Jul 2016) -- End



            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objTPackage._LeaveAccrueTenureSetting = ConfigParameter._Object._LeaveAccrueTenureSetting
            objTPackage._LeaveAccrueDaysAfterEachMonth = ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth
            'Pinkal (18-Nov-2016) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


    'Shani [ 24 NOV 2014 ] -- START

    Private Sub Fill_HourAmtMapping()
        Dim lvItem As ListViewItem
        Try

            lvHourAmtMapping.Items.Clear() : mstrNewHrsAmtMapping = ""

            For Each dtRow As DataRow In mdtHourAmtMapping.Rows
                If dtRow.Item("AUD").ToString = "D" Then Continue For

                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dtRow.Item("SrNo"))

                lvItem.SubItems.Add(dtRow.Item("HourName").ToString)
                lvItem.SubItems(colhHour.Index).Tag = CInt(dtRow.Item("HourId"))

                lvItem.SubItems.Add(dtRow.Item("AmountName").ToString)
                lvItem.SubItems(colhAmount.Index).Tag = CInt(dtRow.Item("AmountId"))

                lvHourAmtMapping.Items.Add(lvItem)

                mstrNewHrsAmtMapping &= "|" & dtRow.Item("HourId").ToString & "," & dtRow.Item("AmountId").ToString
            Next

            If mstrNewHrsAmtMapping.Trim.Length > 0 Then mstrNewHrsAmtMapping = Mid(mstrNewHrsAmtMapping, 2)

            cboOT1Hours.SelectedValue = 0
            cboOT1Amount.SelectedValue = 0
            lnkAddHourAmtMapping.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_HourAmtMapping", mstrModuleName)
        End Try
    End Sub

    Private Function Is_MappingValid() As Boolean
        Try
            If CInt(cboOT1Hours.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Head for an Hour."), enMsgBoxStyle.Information)
                cboOT1Hours.Focus()
                Return False
            ElseIf CInt(cboOT1Amount.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Head for amount."), enMsgBoxStyle.Information)
                'Sohail (02 Nov 2018) -- Start
                'FDRC Issue - Support Issue Id # 0003057 Employee details were not coming on Termination Package report in 75.1.
                'cboOT1Hours.Focus()
                cboOT1Amount.Focus()
                'Sohail (02 Nov 2018) -- End
                Return False
            End If


            Dim lstCount As List(Of DataRow) = (From p In mdtHourAmtMapping Where p.Item("AUD").ToString <> "D" AndAlso (CInt(p.Item("AmountId")) = CInt(cboOT1Amount.SelectedValue)) Select p).ToList
            If lstCount.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Selected Hour Head or Amount Head is already mapped. Please select different head."), enMsgBoxStyle.Information)
                cboOT1Hours.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_MappingValid", mstrModuleName)
        End Try
    End Function

    Private Sub Create_MappingTable()
        Try
            mdtHourAmtMapping = New DataTable("HourAmtMapping")

            mdtHourAmtMapping.Columns.Add("SrNo", System.Type.GetType("System.Int32"))
            mdtHourAmtMapping.Columns("SrNo").AutoIncrementSeed = 1
            mdtHourAmtMapping.Columns("SrNo").AutoIncrement = True

            mdtHourAmtMapping.Columns.Add("HourId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtHourAmtMapping.Columns.Add("HourName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtHourAmtMapping.Columns.Add("AmountId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtHourAmtMapping.Columns.Add("AmountName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtHourAmtMapping.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Create_MappingTable", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim objTranHead As New clsTransactionHead
        Dim lvItem As ListViewItem
        Dim dsFill As New DataSet
        Try

            lvContributaionHead.Items.Clear()

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objTranHead.getComboList("Heads", False, enTranHeadType.EmployersStatutoryContributions)
            dsFill = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, enTranHeadType.EmployersStatutoryContributions)
            'Sohail (21 Aug 2015) -- End
            For Each dtRow As DataRow In dsFill.Tables("Heads").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("code").ToString)
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = CInt(dtRow.Item("tranheadunkid"))
                If mstrContributaionHeadIDs.Trim <> "" Then
                    If mstrContributaionHeadIDs.Split(",").Contains(dtRow.Item("tranheadunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If
                lvContributaionHead.Items.Add(lvItem)
            Next

            If lvContributaionHead.Items.Count > 6 Then
                colhCName.Width = 273 - 18
            Else
                colhCName.Width = 273
            End If

            lvDeductionHead.Items.Clear()
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objTranHead.getComboList("Heads", False, , , , , , "trnheadtype_id in (" & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ")")
            dsFill = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, , , , , , "trnheadtype_id in (" & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ")")
            'Sohail (21 Aug 2015) -- End
            For Each dtRow As DataRow In dsFill.Tables("Heads").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = CInt(dtRow.Item("tranheadunkid"))
                lvItem.SubItems.Add(dtRow.Item("code").ToString)
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                If mstrDeductionHeadIDs.Trim <> "" Then
                    If mstrDeductionHeadIDs.Split(",").Contains(dtRow.Item("tranheadunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If
                lvDeductionHead.Items.Add(lvItem)
            Next

            If lvDeductionHead.Items.Count > 6 Then
                colhDName.Width = 273 - 18
            Else
                colhDName.Width = 273
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Termination_Package_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))
                        Case enHeadTypeId.Salary_Head
                            cboSalaryHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Days_In_Period
                            cboDaysInPeriod.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Conge_Preavis_Head
                            cboCongePreavisHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Base_Taxable_Head
                            cboBaseTaxablehead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Net_Pay_Head
                            cboNetPayhead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Notification_Allowance
                            cboNotificationAllowance.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Head_13Th_Check
                            CboCheckHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Base_Imposable_Head
                            cboBaseImposablehead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Hours_And_Amount_Mapping
                            mstrNewHrsAmtMapping = dsRow.Item("transactionheadid").ToString()

                        Case enHeadTypeId.Deduction_Head_IDs
                            mstrDeductionHeadIDs = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.Contributaion_Head_IDs
                            mstrContributaionHeadIDs = dsRow.Item("transactionheadid").ToString()

                        Case enHeadTypeId.Leave_Type
                            cboLeaveType.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Termination_Factor
                            cboTerminationFactor.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Child_Head
                            cboChildHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Dependent_Head
                            cboDependentHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                            'Sohail (08 Jul 2016) -- Start
                            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                        Case enHeadTypeId.Family_Allowance
                            cboFamilyAllowance.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Logement
                            cboLogement.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IPRNet
                            cboIPRNet.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Loan_Repayment
                            cboLoanRepayment.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Advance_Repayment
                            cboAdvanceRepayment.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Other_Deductions
                            cboOtherDeductions.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Rate_of_Notice
                            cboRateOfNotice.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Bonus_Head_IDs
                            marrBonusHead = dsRow.Item("transactionheadid").ToString().Split(",")

                        Case enHeadTypeId.INSS_35
                            cboINSS35.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (08 Jul 2016) -- End

                    End Select
                Next

            End If

            If mstrNewHrsAmtMapping.Trim.Length > 0 Then
                If mdtHourAmtMapping IsNot Nothing Then
                    For Each iPipe As String In mstrNewHrsAmtMapping.Split(CChar("|"))
                        Dim iData() As String = iPipe.Split(CChar(","))
                        If iData.Length > 0 Then
                            cboOT1Hours.SelectedValue = CInt(iData(0))
                            cboOT1Amount.SelectedValue = CInt(iData(1))
                            Dim lstCount As List(Of DataRow) = (From p In mdtHourAmtMapping Where p.Item("AUD").ToString <> "D" AndAlso (CInt(p.Item("AmountId")) = CInt(cboOT1Amount.SelectedValue)) Select p).ToList
                            If lstCount.Count <= 0 Then
                                Dim dr As DataRow = mdtHourAmtMapping.NewRow
                                With dr
                                    .Item("HourId") = CInt(cboOT1Hours.SelectedValue)
                                    .Item("HourName") = cboOT1Hours.Text
                                    .Item("AmountId") = CInt(cboOT1Amount.SelectedValue)
                                    .Item("AmountName") = cboOT1Amount.Text
                                    .Item("AUD") = "A"
                                    cboOT1Hours.SelectedValue = 0
                                    cboOT1Amount.SelectedValue = 0
                                End With
                                mdtHourAmtMapping.Rows.Add(dr)
                            End If
                        End If
                    Next
                    Call Fill_HourAmtMapping()
                End If
            Else
                mdtHourAmtMapping.Rows.Clear()
                lvHourAmtMapping.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Shani [ 24 NOV 2014 ] -- END

    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    Private Sub FillBonusHeadList()
        Dim objHead As New clsTransactionHead
        Dim dsList As DataSet
        Try

            dsList = objHead.GetList("BonusHead", , enTranHeadType.Informational)
            Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            dtCol.AllowDBNull = False
            dtCol.DefaultValue = False
            dsList.Tables("BonusHead").Columns.Add(dtCol)

            dgvBonusHead.AutoGenerateColumns = False

            objcolhIsCheck.DataPropertyName = "IsChecked"
            objcolhTranheadUnkId.DataPropertyName = "tranheadunkid"
            colhTranHeadCode.DataPropertyName = "trnheadcode"
            colhTranHeadName.DataPropertyName = "trnheadname"

            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables("BonusHead") Where (marrBonusHead.Contains(CInt(p.Item("tranheadunkid"))) = True) Select (p)).ToList

            If lstRow.Count > 0 Then
                For Each dsRow As DataRow In lstRow
                    dsRow.Item("IsChecked") = True
                Next
                dsList.Tables("BonusHead").AcceptChanges()
            End If

            dtViewHead = New DataView(dsList.Tables("BonusHead"))
            dtViewHead.Sort = "IsChecked DESC, trnheadname"
            dgvBonusHead.DataSource = dtViewHead


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBonusHeadList", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Jul 2016) -- End

#End Region

#Region " Form Events "

    Private Sub frmTerminationPackageReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objTPackage = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminationPackageReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTerminationPackageReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Me._Title = objTPackage._ReportName
            Me._Message = objTPackage._ReportDesc

            Call FillCombo()
            Call Create_MappingTable()
            Call ResetValue()
            Call Fill_List()
            'Sohail (08 Jul 2016) -- Start
            'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
            Call FillBonusHeadList()
            'Sohail (08 Jul 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTerminationPackageReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'objTPackage.generateReport(0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            'Dim objEmp As New clsEmployee_Master
            'Dim objPeriod As New clscommom_period_Tran
            'objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'If objEmp._Isexclude_Payroll = True Then

            '    Dim dsPrevPeriod As DataSet = objPeriod.GetPreviousPeriod("PrevPeriod", enModuleReference.Payroll, CInt(cboPeriod.SelectedValue))
            '    Dim intPrevPeriod As Integer = CInt(cboPeriod.SelectedValue)
            '    If dsPrevPeriod.Tables("PrevPeriod").Rows.Count > 0 Then
            '        intPrevPeriod = CInt(dsPrevPeriod.Tables("PrevPeriod").Rows(0).Item("periodunkid"))

            '        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
            '    End If

            'Else

            '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'End If
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (08 Jul 2016) -- End
            'Sohail (21 Aug 2015) -- End
            objTPackage.generateReportNew(objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid), _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'objTPackage.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Sohail (08 Jul 2016) -- Start
            'Enhancement -  Termination Package Report changes as per rutta's request. (From: Anatory Rutta; Sent: Friday, June 03, 2016 11:04 AM; To: 'ezee : Anjan Marfatia'; Cc:         'Sohail Aruti'; Subject: FINCA DRC TERMINATION PACKAGES)
            'Dim objEmp As New clsEmployee_Master
            'Dim objPeriod As New clscommom_period_Tran
            'objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'If objEmp._Isexclude_Payroll = True Then

            '    Dim dsPrevPeriod As DataSet = objPeriod.GetPreviousPeriod("PrevPeriod", enModuleReference.Payroll, CInt(cboPeriod.SelectedValue))
            '    Dim intPrevPeriod As Integer = CInt(cboPeriod.SelectedValue)
            '    If dsPrevPeriod.Tables("PrevPeriod").Rows.Count > 0 Then
            '        intPrevPeriod = CInt(dsPrevPeriod.Tables("PrevPeriod").Rows(0).Item("periodunkid"))

            '        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
            '    End If

            'Else

            '    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            'End If
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (08 Jul 2016) -- End
            'Sohail (21 Aug 2015) -- End
            objTPackage.generateReportNew(objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid), _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, _
                                          ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.SelectedValue = cboEmployee.SelectedValue
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub objbtnSearchReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frm As New frmCommonSearch
        'Try
        '    frm.DataSource = cboReason.DataSource
        '    frm.SelectedValue = cboReason.SelectedValue
        '    frm.ValueMember = cboReason.ValueMember
        '    frm.DisplayMember = cboReason.DisplayMember
        '    If frm.DisplayDialog Then
        '        cboReason.SelectedValue = frm.SelectedValue
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "objbtnSearchReason_Click", mstrModuleName)
        'End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTerminationPackageReport.SetMessages()
            objfrm._Other_ModuleNames = "clsTerminationPackageReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()

                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.Termination_Package_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType
                    Case enHeadTypeId.Salary_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboSalaryHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Days_In_Period
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDaysInPeriod.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Conge_Preavis_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCongePreavisHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Base_Taxable_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBaseTaxablehead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Net_Pay_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNetPayhead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Notification_Allowance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNotificationAllowance.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Head_13Th_Check
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CboCheckHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Base_Imposable_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboBaseImposablehead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Hours_And_Amount_Mapping
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrNewHrsAmtMapping

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Deduction_Head_IDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrDeductionHeadIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Contributaion_Head_IDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrContributaionHeadIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Leave_Type
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLeaveType.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Termination_Factor
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTerminationFactor.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Child_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboChildHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Dependent_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboDependentHead.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                        'Sohail (08 Jul 2016) -- Start
                        'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
                    Case enHeadTypeId.Family_Allowance
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboFamilyAllowance.SelectedValue.ToString

                    Case enHeadTypeId.Logement
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLogement.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IPRNet
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIPRNet.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Loan_Repayment
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboLoanRepayment.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Advance_Repayment
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAdvanceRepayment.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Deductions
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherDeductions.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Rate_of_Notice
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboRateOfNotice.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Bonus_Head_IDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = String.Join(",", marrBonusHead.ToArray())

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.INSS_35
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboINSS35.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.Termination_Package_Report, 0, 0, intHeadType)
                        'Sohail (08 Jul 2016) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Selection Saved Successfully"), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    'Sohail (17 May 2015) -- Start
    'Enhancement - 60.1 - Changes in Termination Package Report as per 58.1 Allocation table changes and channging all drop down list to searchable drop down list for FDRC.
    Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress _
                                                                                                                    , cboEmployee.KeyPress _
                                                                                                                    , cboLeaveType.KeyPress _
                                                                                                                    , cboDaysInPeriod.KeyPress _
                                                                                                                    , cboSalaryHead.KeyPress _
                                                                                                                    , cboNotificationAllowance.KeyPress _
                                                                                                                    , cboCongePreavisHead.KeyPress _
                                                                                                                    , CboCheckHead.KeyPress _
                                                                                                                    , cboBaseTaxablehead.KeyPress _
                                                                                                                    , cboBaseImposablehead.KeyPress _
                                                                                                                    , cboNetPayhead.KeyPress _
                                                                                                                    , cboTerminationFactor.KeyPress _
                                                                                                                    , cboChildHead.KeyPress _
                                                                                                                    , cboDependentHead.KeyPress _
                                                                                                                    , cboOT1Hours.KeyPress _
                                                                                                                    , cboFamilyAllowance.KeyPress _
                                                                                                                    , cboLogement.KeyPress _
                                                                                                                    , cboLoanRepayment.KeyPress _
                                                                                                                    , cboAdvanceRepayment.KeyPress _
                                                                                                                    , cboIPRNet.KeyPress _
                                                                                                                    , cboOtherDeductions.KeyPress _
                                                                                                                    , cboRateOfNotice.KeyPress _
                                                                                                                    , cboINSS35.KeyPress _
                                                                                                                    , cboOT1Amount.KeyPress
        'Sohail (02 Nov 2018) - [cboOT1Amount.KeyPress]

        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)

                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    ElseIf cbo.Name = cboLeaveType.Name Then
                        .CodeMember = ""
                    Else
                        .CodeMember = "code"
                    End If

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_KeyPress", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 May 2015) -- End

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim dsCombo As DataSet
        Dim strFilter As String = ""
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'strFilter = " ( CONVERT(CHAR(8), termination_from_date, 112) BETWEEN @startdate AND @enddate OR CONVERT(CHAR(8), termination_to_date, 112) BETWEEN @startdate AND @enddate OR CONVERT(CHAR(8), empl_enddate, 112) BETWEEN @startdate AND @enddate ) "

                Dim strDate1, strDate2 As String : strDate1 = "" : strDate2 = ""
                strDate1 = "'" & CType(cboPeriod.SelectedItem, DataRowView).Item("start_date") & "'"
                strDate2 = "'" & CType(cboPeriod.SelectedItem, DataRowView).Item("end_date") & "'"

                strFilter = " ( CONVERT(CHAR(8), TRM.LEAVING, 112) BETWEEN " & strDate1 & " AND " & strDate2 & " OR CONVERT(CHAR(8), RET.RETIRE, 112) " & _
                            " BETWEEN " & strDate1 & " AND " & strDate2 & " OR CONVERT(CHAR(8), TRM.EOC, 112) BETWEEN " & strDate1 & " AND " & strDate2 & " ) "
                'S.SANDEEP [04 JUN 2015] -- END
            End If

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date")), eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date")), , , , , strFilter)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date")), _
                                           eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date")), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, True, "Emp", True, , , , , , , , , , , , , , , strFilter)
            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Call cboCurrency_SelectedIndexChanged(sender, e)
            If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim dtAsOnDate As Date = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date")).Date

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objPeriod As New clscommom_period_Tran

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                    'S.SANDEEP [04 JUN 2015] -- END

                    If objEmp._Isexclude_Payroll = True Then
                        Dim dsPrevPeriod As DataSet = objPeriod.GetPreviousPeriod("PrevPeriod", enModuleReference.Payroll, CInt(cboPeriod.SelectedValue))
                        Dim intPrevPeriod As Integer = CInt(cboPeriod.SelectedValue)
                        If dsPrevPeriod.Tables("PrevPeriod").Rows.Count > 0 Then
                            intPrevPeriod = CInt(dsPrevPeriod.Tables("PrevPeriod").Rows(0).Item("periodunkid"))
                        End If
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPeriod._Periodunkid = intPrevPeriod
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                        'Sohail (21 Aug 2015) -- End
                        dtAsOnDate = objPeriod._End_Date
                    End If
                End If

                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtAsOnDate, True)
                If dsList.Tables("ExRate").Rows.Count > 0 Then
                    mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                    mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign")
                    lblExRate.Text = Language.getMessage(mstrModuleName, 11, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                    mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " "
                Else
                    lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                End If
            Else
                mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                mstrCurrency_Rate = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
#Region " DataGridView Events "
    Private Sub dgvBonusHead_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvBonusHead.DataError
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvBonusHead_DataError", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (08 Jul 2016) -- End

#Region " Other Control's Events "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objTPackage.setOrderBy(0)
            txtOrderBy.Text = objTPackage.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


    'Shani [ 24 NOV 2014 ] -- START

    Private Sub lvHourAmtMapping_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvHourAmtMapping.SelectedIndexChanged
        Try
            If lvHourAmtMapping.SelectedItems.Count > 0 Then
                cboOT1Hours.SelectedValue = lvHourAmtMapping.SelectedItems(0).SubItems(colhHour.Index).Tag
                cboOT1Amount.SelectedValue = lvHourAmtMapping.SelectedItems(0).SubItems(colhAmount.Index).Tag
                lnkAddHourAmtMapping.Enabled = False
            Else
                lnkAddHourAmtMapping.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvHourAmtMapping_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAddHourAmtMapping_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddHourAmtMapping.LinkClicked
        Try
            If Is_MappingValid() = False Then Exit Sub

            Dim dr As DataRow = mdtHourAmtMapping.NewRow

            With dr
                .Item("HourId") = CInt(cboOT1Hours.SelectedValue)
                .Item("HourName") = cboOT1Hours.Text
                .Item("AmountId") = CInt(cboOT1Amount.SelectedValue)
                .Item("AmountName") = cboOT1Amount.Text
                .Item("AUD") = "A"
            End With

            mdtHourAmtMapping.Rows.Add(dr)

            Call Fill_HourAmtMapping()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddHourAmtMapping_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkDeleteHourAmtMapping_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkDeleteHourAmtMapping.LinkClicked
        Try
            If lvHourAmtMapping.SelectedItems.Count > 0 Then
                Dim dr() As DataRow = mdtHourAmtMapping.Select("SrNo = '" & CInt(lvHourAmtMapping.SelectedItems(0).Tag) & "'")
                If dr.Length > 0 Then
                    dr(0).Item("AUD") = "D"
                End If
                mdtHourAmtMapping.AcceptChanges()
                Call Fill_HourAmtMapping()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objallchkContributaion_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objallchkContributaion.CheckedChanged
        Try
            RemoveHandler lvContributaionHead.ItemChecked, AddressOf lvContributaionHead_ItemChecked
            For Each lvItm As ListViewItem In lvContributaionHead.Items
                lvItm.Checked = CBool(objallchkContributaion.CheckState)
            Next
            AddHandler lvContributaionHead.ItemChecked, AddressOf lvContributaionHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objallchkContributaion_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkallDeducation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkallDeducation.CheckedChanged
        Try
            RemoveHandler lvDeductionHead.ItemChecked, AddressOf lvDeductionHead_ItemChecked
            For Each lvItm As ListViewItem In lvDeductionHead.Items
                lvItm.Checked = CBool(objchkallDeducation.CheckState)
            Next
            AddHandler lvDeductionHead.ItemChecked, AddressOf lvDeductionHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkallDeducation_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvContributaionHead_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Try
            RemoveHandler objallchkContributaion.CheckedChanged, AddressOf objallchkContributaion_CheckedChanged
            If lvContributaionHead.CheckedItems.Count <= 0 Then
                objallchkContributaion.CheckState = CheckState.Unchecked
            ElseIf lvContributaionHead.CheckedItems.Count < lvContributaionHead.Items.Count Then
                objallchkContributaion.CheckState = CheckState.Indeterminate
            ElseIf lvContributaionHead.CheckedItems.Count = lvContributaionHead.Items.Count Then
                objallchkContributaion.CheckState = CheckState.Checked
            End If
            AddHandler objallchkContributaion.CheckedChanged, AddressOf objallchkContributaion_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvContributaionHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvDeductionHead_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Try
            RemoveHandler objchkallDeducation.CheckedChanged, AddressOf objchkallDeducation_CheckedChanged
            If lvDeductionHead.CheckedItems.Count <= 0 Then
                objchkallDeducation.CheckState = CheckState.Unchecked
            ElseIf lvDeductionHead.CheckedItems.Count < lvDeductionHead.Items.Count Then
                objchkallDeducation.CheckState = CheckState.Indeterminate
            ElseIf lvDeductionHead.CheckedItems.Count = lvDeductionHead.Items.Count Then
                objchkallDeducation.CheckState = CheckState.Checked
            End If
            AddHandler objchkallDeducation.CheckedChanged, AddressOf objchkallDeducation_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDeductionHead_ItemChecked", mstrModuleName)
        End Try
    End Sub

    'Shani [ 24 NOV 2014 ] -- END

    'Sohail (08 Jul 2016) -- Start
    'Enhancement - 63.1 - Termination Package changes as per email. (From: MUEMBIA Daddy; Sent: Tuesday, June 28, 2016 1:26 AM; To: rutta.anatory@aruti.com ; 'Muembia Daddy'; Cc: 'ezee : Anjan Marfatia'; Subject: Termination package comments from Finca DRC)
    Private Sub txtSearchBonusHeads_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchBonusHeads.TextChanged
        Try
            If txtSearchBonusHeads.Text.Trim <> "" Then
                dtViewHead.RowFilter = "trnheadcode LIKE '%" & txtSearchBonusHeads.Text & "%' OR trnheadname LIKE '%" & txtSearchBonusHeads.Text & "%'"
            Else
                dtViewHead.RowFilter = ""
            End If
            dgvBonusHead.DataSource = dtViewHead
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchBonusHeads_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Jul 2016) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
            Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.Name, Me.lblExRate.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.lblHourHead.Text = Language._Object.getCaption(Me.lblHourHead.Name, Me.lblHourHead.Text)
            Me.lnHourAmtMapping.Text = Language._Object.getCaption(Me.lnHourAmtMapping.Name, Me.lnHourAmtMapping.Text)
            Me.lnkDeleteHourAmtMapping.Text = Language._Object.getCaption(Me.lnkDeleteHourAmtMapping.Name, Me.lnkDeleteHourAmtMapping.Text)
            Me.lnkAddHourAmtMapping.Text = Language._Object.getCaption(Me.lnkAddHourAmtMapping.Name, Me.lnkAddHourAmtMapping.Text)
            Me.colhHour.Text = Language._Object.getCaption(CStr(Me.colhHour.Tag), Me.colhHour.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.lblSalaryHead.Text = Language._Object.getCaption(Me.lblSalaryHead.Name, Me.lblSalaryHead.Text)
            Me.lblCongePreavisHead.Text = Language._Object.getCaption(Me.lblCongePreavisHead.Name, Me.lblCongePreavisHead.Text)
            Me.lbldaysinPeriod.Text = Language._Object.getCaption(Me.lbldaysinPeriod.Name, Me.lbldaysinPeriod.Text)
            Me.lblNotificationAllowance.Text = Language._Object.getCaption(Me.lblNotificationAllowance.Name, Me.lblNotificationAllowance.Text)
            Me.lblCheckHead.Text = Language._Object.getCaption(Me.lblCheckHead.Name, Me.lblCheckHead.Text)
            Me.lblBaseImposableHead.Text = Language._Object.getCaption(Me.lblBaseImposableHead.Name, Me.lblBaseImposableHead.Text)
            Me.lblBaseTaxablehead.Text = Language._Object.getCaption(Me.lblBaseTaxablehead.Name, Me.lblBaseTaxablehead.Text)
            Me.lblNetPayhead.Text = Language._Object.getCaption(Me.lblNetPayhead.Name, Me.lblNetPayhead.Text)
            Me.lblDeductionHead.Text = Language._Object.getCaption(Me.lblDeductionHead.Name, Me.lblDeductionHead.Text)
            Me.lblContribution.Text = Language._Object.getCaption(Me.lblContribution.Name, Me.lblContribution.Text)
            Me.colhDCode.Text = Language._Object.getCaption(CStr(Me.colhDCode.Tag), Me.colhDCode.Text)
            Me.colhDName.Text = Language._Object.getCaption(CStr(Me.colhDName.Tag), Me.colhDName.Text)
            Me.colhCCode.Text = Language._Object.getCaption(CStr(Me.colhCCode.Tag), Me.colhCCode.Text)
            Me.colhCName.Text = Language._Object.getCaption(CStr(Me.colhCName.Tag), Me.colhCName.Text)
            Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
            Me.lblTerminationFactor.Text = Language._Object.getCaption(Me.lblTerminationFactor.Name, Me.lblTerminationFactor.Text)
            Me.lblNoticeDate.Text = Language._Object.getCaption(Me.lblNoticeDate.Name, Me.lblNoticeDate.Text)
            Me.lblDependentHead.Text = Language._Object.getCaption(Me.lblDependentHead.Name, Me.lblDependentHead.Text)
            Me.lblChildHead.Text = Language._Object.getCaption(Me.lblChildHead.Name, Me.lblChildHead.Text)
            Me.lblLoanRepayment.Text = Language._Object.getCaption(Me.lblLoanRepayment.Name, Me.lblLoanRepayment.Text)
            Me.lblLogement.Text = Language._Object.getCaption(Me.lblLogement.Name, Me.lblLogement.Text)
            Me.lblFamilyAllowance.Text = Language._Object.getCaption(Me.lblFamilyAllowance.Name, Me.lblFamilyAllowance.Text)
            Me.lblIPRNet.Text = Language._Object.getCaption(Me.lblIPRNet.Name, Me.lblIPRNet.Text)
            Me.lblAdvanceRepayment.Text = Language._Object.getCaption(Me.lblAdvanceRepayment.Name, Me.lblAdvanceRepayment.Text)
            Me.lblRateOfNotice.Text = Language._Object.getCaption(Me.lblRateOfNotice.Name, Me.lblRateOfNotice.Text)
            Me.colhTranHeadCode.HeaderText = Language._Object.getCaption(Me.colhTranHeadCode.Name, Me.colhTranHeadCode.HeaderText)
            Me.colhTranHeadName.HeaderText = Language._Object.getCaption(Me.colhTranHeadName.Name, Me.colhTranHeadName.HeaderText)
            Me.lblBonusHeads.Text = Language._Object.getCaption(Me.lblBonusHeads.Name, Me.lblBonusHeads.Text)
            Me.lblOtherDeductions.Text = Language._Object.getCaption(Me.lblOtherDeductions.Name, Me.lblOtherDeductions.Text)
            Me.lblINSS35.Text = Language._Object.getCaption(Me.lblINSS35.Name, Me.lblINSS35.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select From Period.")
            Language.setMessage(mstrModuleName, 2, "Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "Sorry, No exchange rate defined for this currency for the period selected.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Select Deduction Head.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Select Contributaion Head.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Select Currency.")
            Language.setMessage(mstrModuleName, 7, "Please select Head for an Hour.")
            Language.setMessage(mstrModuleName, 8, "Please select Head for amount.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Selected Hour Head or Amount Head is already mapped. Please select different head.")
            Language.setMessage(mstrModuleName, 10, "Selection Saved Successfully")
            Language.setMessage(mstrModuleName, 11, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 12, "Please Select Head for Days In Period.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

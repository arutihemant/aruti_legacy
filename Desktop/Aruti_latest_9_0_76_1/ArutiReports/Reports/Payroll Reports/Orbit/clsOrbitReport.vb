'************************************************************************************************************************************
'Class Name : clsOrbitReport.vb
'Purpose    :
'Date       :24/12/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsOrbitReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsOrbitReport"
    Private mstrReportId As String = enArutiReport.Orbit
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()

        'S.SANDEEP [ 03 OCT 2014 ] -- START
        Call Create_Orbit3DetailReport()
        'S.SANDEEP [ 03 OCT 2014 ] -- END

    End Sub
#End Region

#Region " Private Variables "
    Private mintReportId As Integer = 0
    'Sohail (12 Aug 2014) -- Start
    'Enhancement - New Orbit2 Report.
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    'Sohail (12 Aug 2014) -- End

    Private mstrEmpCode As String = String.Empty
    Private mstrEmpName As String = String.Empty
    Private mintEmployeeunkid As Integer = -1
    Private mstrBankName As String = String.Empty
    Private mintBankId As Integer = -1
    Private mstrBranchName As String = String.Empty
    Private mintBranchId As Integer = -1
    Private mintCountryunkid As Integer = -1
    Private mstrCountryName As String = String.Empty
    Private mstrBatchName As String = String.Empty
    Private mdblAmount As Decimal = 0
    Private mdblAmountTo As Decimal = 0
    Private mintExchangeRateId As Integer = 1
    Private mstrPeriodId As String = String.Empty
    Private mstrPeriodName As String = ""

    Private mblnIncludeInactiveEmp As Boolean = True
    Private mblnIncludeReportColumnHeader As Boolean = True
    Private mintCurrencyId As Integer = 0
    Private mstrCurrencySign As String = ""

    Private mintCompanyBranchId As Integer = -1
    Private mstrCompanyBranchName As String = String.Empty
    Private mstrCompanyBankAccountNo As String = String.Empty
    Private mintCompanyBankAccountTypeunkid As Integer = -1
    Private mstrCompanyBankAccountTypeCode As String = String.Empty
    Private mstrCompanyBankAccountTypeName As String = String.Empty
    Private mstrCompanyBankSwiftCode As String = String.Empty

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mdtExcelTable As DataTable = Nothing
#End Region

#Region " Properties "

    'Sohail (12 Aug 2014) -- Start
    'Enhancement - New Orbit2 Report.
    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property
    'Sohail (12 Aug 2014) -- End

    Public WriteOnly Property _EmpCode() As String
        Set(ByVal value As String)
            mstrEmpCode = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _BankName() As String
        Set(ByVal value As String)
            mstrBankName = value
        End Set
    End Property

    Public WriteOnly Property _BankBranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property
    Public WriteOnly Property _CountryName() As String
        Set(ByVal value As String)
            mstrCountryName = value
        End Set
    End Property

    Public WriteOnly Property _BatchName() As String
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankAccountTypeId() As Integer
        Set(ByVal value As Integer)
            mintCompanyBankAccountTypeunkid = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankAccountTypeCode() As String
        Set(ByVal value As String)
            mstrCompanyBankAccountTypeCode = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankAccountTypeName() As String
        Set(ByVal value As String)
            mstrCompanyBankAccountTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankSwiftCode() As String
        Set(ByVal value As String)
            mstrCompanyBankSwiftCode = value
        End Set
    End Property

    Public WriteOnly Property _Amount() As Double
        Set(ByVal value As Double)
            mdblAmount = value
        End Set
    End Property
    Public WriteOnly Property _AmountTo() As Double
        Set(ByVal value As Double)
            mdblAmountTo = value
        End Set
    End Property
    Public WriteOnly Property _BankId() As Integer
        Set(ByVal value As Integer)
            mintBankId = value
        End Set
    End Property
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property
    Public WriteOnly Property _ExchangeRateId() As Integer
        Set(ByVal value As Integer)
            mintExchangeRateId = value
        End Set
    End Property
    Public WriteOnly Property _PeriodId() As String
        Set(ByVal value As String)
            mstrPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _IncludeReportColumnHeader() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeReportColumnHeader = value
        End Set
    End Property

    Public WriteOnly Property _CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _CurrencySign() As String
        Set(ByVal value As String)
            mstrCurrencySign = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBranchId() As Integer
        Set(ByVal value As Integer)
            mintCompanyBranchId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankBranchName() As String
        Set(ByVal value As String)
            mstrCompanyBranchName = value
        End Set
    End Property

    Public WriteOnly Property _CompanyBankAccountNo() As String
        Set(ByVal value As String)
            mstrCompanyBankAccountNo = value
        End Set
    End Property
#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()

        Try

            'Sohail (12 Aug 2014) -- Start
            'Enhancement - New Orbit2 Report.
            mintReportTypeId = 0
            mstrReportTypeName = ""
            'Sohail (12 Aug 2014) -- End

            mstrEmpCode = ""
            mstrEmpName = ""
            mintEmployeeunkid = -1
            mstrBankName = ""
            mstrBranchName = ""
            mintCountryunkid = -1
            mstrCountryName = ""
            mstrBatchName = ""
            mintCompanyBankAccountTypeunkid = -1
            mstrCompanyBankAccountTypeCode = ""
            mstrCompanyBankAccountTypeName = ""
            mdblAmount = 0
            mdblAmountTo = 0
            mintExchangeRateId = 1
            mstrPeriodId = ""

            mblnIncludeInactiveEmp = True
            mblnIncludeReportColumnHeader = True

            mintBankId = 0
            mintBranchId = 0
            mintCurrencyId = 0
            mstrCurrencySign = ""

            mintCompanyBranchId = 0
            mstrCompanyBranchName = ""
            mstrCompanyBankAccountNo = ""
            mintCompanyBankAccountTypeunkid = 0
            mstrCompanyBankAccountTypeCode = ""
            mstrCompanyBankAccountTypeName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Private Sub FilterTitleAndFilterQuery(Optional ByVal blnIncludeOrderbyQuery As Boolean = True)
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try

            objDataOperation.AddParameter("@Cr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Cr"))
            objDataOperation.AddParameter("@Dr", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Dr"))

            'If mstrEmpCode.Trim <> "" Then
            '    objDataOperation.AddParameter("@EmpCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "%" & mstrEmpCode & "%")
            '    Me._FilterQuery &= " AND hremployee_master.employeecode LIKE @EmpCode "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Employee Code : ") & mstrEmpCode & " "
            'End If

            'If mintEmployeeunkid > 0 Then
            '    objDataOperation.AddParameter("@Employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            '    Me._FilterQuery &= " AND hremployee_master.employeeunkid = @Employeeunkid "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Employee Name : ") & mstrEmpName & " "
            'End If

            If mintBankId > 0 Then
                objDataOperation.AddParameter("@BankId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankId)
                Me._FilterQuery &= " AND hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = @BankId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Bank Name : ") & mstrBankName & " "
            End If

            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchName", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                Me._FilterQuery &= " AND hrmsConfiguration..cfbankbranch_master.branchunkid = @BranchName "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Branch Name : ") & mstrBranchName & " "
            End If

            'If mintCountryunkid > 0 Then
            '    objDataOperation.AddParameter("@Countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            '    Me._FilterQuery &= " AND hrmsConfiguration..cfcountry_master.countryunkid = @Countryunkid "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "Country : ") & mstrCountryName & " "
            'End If


            If mintCompanyBranchId > 0 Then
                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyBranchId)
                Me._FilterQuery &= " AND prpayment_tran.branchunkid = @branchunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Company Bank Branch : ") & mstrCompanyBranchName & " "
            End If

            If mstrCompanyBankAccountNo.Trim <> "" Then
                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompanyBankAccountNo)
                Me._FilterQuery &= " AND prpayment_tran.accountno = @accountno "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Company Bank Account : ") & mstrCompanyBankAccountNo & " "
            End If


            If mdblAmount > 0 And mdblAmountTo > 0 Then
                objDataOperation.AddParameter("@Amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmount)
                objDataOperation.AddParameter("@AmountTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmountTo)
                Me._FilterQuery &= " AND ISNULL(prempsalary_tran.expaidamt,0) BETWEEN  @Amount AND @AmountTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Amount From : ") & mdblAmount & Language.getMessage(mstrModuleName, 20, " To ") & mdblAmountTo & " "
            End If


            If mintCurrencyId > 0 Then
                objDataOperation.AddParameter("@Currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyId)
                Me._FilterQuery &= " AND prpayment_tran.countryunkid = @Currencyunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Paid Currency : ") & mstrCurrencySign & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If Me.OrderByQuery <> "" AndAlso blnIncludeOrderbyQuery = True Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Order By :") & " " & Me.OrderByDisplay

                'Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try

    End Sub
#End Region

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            'S.SANDEEP [ 03 OCT 2014 ] -- START
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            Select Case intReportType
                Case 3, 4   'LOAN & ADVANCE
                    OrderByDisplay = iColumn_Orbit3_DetailReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_Orbit3_DetailReport.ColumnItem(0).Name
                Case Else
                    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            End Select
            'S.SANDEEP [ 03 OCT 2014 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            'S.SANDEEP [ 03 OCT 2014 ] -- START
            'Call OrderByExecute(iColumn_DetailReport)
            Select Case intReportType
                Case 3, 4   'LOAN & ADVANCE
                    Call OrderByExecute(iColumn_Orbit3_DetailReport)
                Case Else
                    Call OrderByExecute(iColumn_DetailReport)
            End Select
            'S.SANDEEP [ 03 OCT 2014 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailProperty() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()

        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112)", Language.getMessage(mstrModuleName, 2, "Effective Date")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(premployee_bank_tran.accountno, '')", Language.getMessage(mstrModuleName, 7, "Employee Bank Account")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(prempsalary_tran.amount,0)", Language.getMessage(mstrModuleName, 8, "Salary")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(prpayment_tran.chequeno, '')", Language.getMessage(mstrModuleName, 11, "ChequeNo")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                          , ByVal xUserUnkid As Integer _
                                          , ByVal xYearUnkid As Integer _
                                          , ByVal xCompanyUnkid As Integer _
                                          , ByVal xPeriodStart As Date _
                                          , ByVal xPeriodEnd As Date _
                                          , ByVal xUserModeSetting As String _
                                          , ByVal xOnlyApproved As Boolean _
                                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                          , ByVal blnApplyUserAccessFilter As Boolean _
                                          , ByVal strFmtCurrency As String _
                                          , ByVal intBase_CurrencyId As Integer _
                                          , ByVal strExportReportPath As String _
                                          , ByVal blnOpenAfterExport As Boolean _
                                          ) As Boolean
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, intBase_CurrencyId, strExportReportPath, blnOpenAfterExport]

        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        Dim mdecTotal As Decimal = 0
        Dim blnFlag As Boolean = False

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation
        Try

            mdtExcelTable = New DataTable("Orbit")
            Dim dCol As DataColumn

            dCol = New DataColumn("BatchName")
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "Batch Name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EffectiveDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "Effective Date")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("TransactionDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "Transaction Date")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("BaseCurrency")
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Currency")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("PaidCurrency")
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Currency")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("CCenterCode")
            'Sohail (12 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'dCol.Caption = Language.getMessage(mstrModuleName, 5, "Cost Center Code")
            dCol.Caption = Language.getMessage(mstrModuleName, 5, "Swift Code")
            'Sohail (12 Jan 2013) -- End
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("AccTypeCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 6, "Account Type Code")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpBankAccount")
            dCol.Caption = Language.getMessage(mstrModuleName, 7, "Employee Bank Account")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Amount")
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "Salary")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Narration")
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Narration")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Description")
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "Description")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("ChequeNo")
            dCol.Caption = Language.getMessage(mstrModuleName, 11, "ChequeNo")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)


            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBase_CurrencyId
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            strQ = "SELECT  hremployee_master.employeecode AS EmpCode " & _
                       "	,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName  " & _
                          ", CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) AS  PaymentDate " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid, 0) AS BGID " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupcode, '') AS BGCode " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname, '') AS BankName " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.swiftcode, '') AS EmpSwiftCode " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchunkid, 0) AS BRID " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchcode, '') AS BRCode " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchname, '') AS BranchName " & _
                          ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS CountryName  " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttypeunkid, 0) AS accounttypeunkid " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_code,'') AS AcTypeCode  " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_name,'') AS AcTypeName  " & _
                          ", ISNULL(premployee_bank_tran.accountno, '') AS EmpAccountNo  " & _
                          ", ISNULL(prpayment_tran.chequeno, '') AS ChequeNo " & _
                          ", ISNULL(prempsalary_tran.amount,0) AS Amount  " & _
                          ", prpayment_tran.paidcurrencyid " & _
                          ", cfexchange_rate.currency_name " & _
                          ", cfexchange_rate.currency_sign " & _
                          ", ISNULL(prempsalary_tran.expaidamt, 0 ) AS expaidamt " & _
                          ", ISNULL(cfbankbranch_master.sortcode, '') AS sortcode " & _
                          ", ISNULL(prpayment_tran.isauthorized, 0) AS isauthorized " & _
                          ", ECCT.costcenterunkid " & _
                          ", prcostcenter_master.costcentercode " & _
                          ", prcostcenter_master.costcentername " & _
                          ", @Cr AS Narration " & _
                    "FROM    prpayment_tran " & _
                        "JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                        "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                        "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid  " & _
                        "JOIN hrmsConfiguration..cfbankacctype_master ON hrmsConfiguration..cfbankacctype_master.accounttypeunkid = premployee_bank_tran.accounttypeunkid  " & _
                        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid  " & _
                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid  " & _
                        "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                        "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = ECCT.costcenterunkid " & _
                    "WHERE  ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                    "AND prempsalary_tran.isvoid = 0 " & _
                    "AND premployee_bank_tran.isvoid = 0  " & _
                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1  " & _
                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodId & ") "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            Call FilterTitleAndFilterQuery()

            strQ &= Me._FilterQuery

            If Me.OrderByQuery <> "" Then
                strQ &= " ORDER BY " & Me.OrderByQuery
            End If

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = mdtExcelTable.NewRow

                rpt_Row.Item("BatchName") = mstrBatchName
                rpt_Row.Item("EffectiveDate") = eZeeDate.convertDate(dtRow.Item("PaymentDate").ToString).ToString("MM/dd/yyyy")
                rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")

                rpt_Row.Item("BaseCurrency") = dtRow.Item("currency_sign")
                rpt_Row.Item("PaidCurrency") = dtRow.Item("currency_sign")

                rpt_Row.Item("CCenterCode") = dtRow.Item("EmpSwiftCode")

                rpt_Row.Item("AccTypeCode") = dtRow.Item("AcTypeCode")
                rpt_Row.Item("EmpBankAccount") = dtRow.Item("EmpAccountNo")

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Amount") = Format(dtRow.Item("expaidamt"), GUI.fmtCurrency)
                rpt_Row.Item("Amount") = Format(dtRow.Item("expaidamt"), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Narration") = dtRow.Item("Narration")

                rpt_Row.Item("Description") = mstrPeriodName & " " & Language.getMessage(mstrModuleName, 8, "Salary")

                rpt_Row.Item("ChequeNo") = dtRow.Item("ChequeNo")


                'mdecTotal += CDec(Format(dtRow.Item("expaidamt"), GUI.fmtCurrency))

                mdtExcelTable.Rows.Add(rpt_Row)
            Next

            If mdtExcelTable.Rows.Count > 0 Then

                objDataOperation.ClearParameters()

                strQ = "SELECT  SUM(CAST(ISNULL(prempsalary_tran.expaidamt, 0)AS DECIMAL(36, " & decDecimalPlaces & "))) AS TotalAmt " & _
                                         ", ISNULL(prpayment_tran.chequeno, '') AS ChequeNo " & _
                                         ", @Dr AS Narration " & _
                                   "FROM    prpayment_tran " & _
                                       "JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                       "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                       "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                                       "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid  " & _
                                       "JOIN hrmsConfiguration..cfbankacctype_master ON hrmsConfiguration..cfbankacctype_master.accounttypeunkid = premployee_bank_tran.accounttypeunkid  " & _
                                       "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid  " & _
                                       "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid  " & _
                                       "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                                       "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                strQ &= "WHERE  ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                       "AND prempsalary_tran.isvoid = 0 " & _
                                       "AND premployee_bank_tran.isvoid = 0  " & _
                                       "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                                       "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1  " & _
                                       "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                       "AND prpayment_tran.periodunkid IN (" & mstrPeriodId & ") "
                'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIncludeInactiveEmp = False Then
                '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    strQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        strQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                Call FilterTitleAndFilterQuery()

                strQ &= Me._FilterQuery

                strQ &= "GROUP BY prpayment_tran.chequeno "

                dsList = objDataOperation.ExecQuery(strQ, "Summary")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dtRow As DataRow In dsList.Tables("Summary").Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = mdtExcelTable.NewRow

                    rpt_Row.Item("BatchName") = mstrBatchName
                    rpt_Row.Item("EffectiveDate") = DateAndTime.Now.ToString("MM/dd/yyyy")
                    rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")

                    rpt_Row.Item("BaseCurrency") = mstrCurrencySign
                    rpt_Row.Item("PaidCurrency") = mstrCurrencySign

                    rpt_Row.Item("CCenterCode") = mstrCompanyBankSwiftCode

                    rpt_Row.Item("AccTypeCode") = mstrCompanyBankAccountTypeCode
                    rpt_Row.Item("EmpBankAccount") = mstrCompanyBankAccountNo

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Amount") = Format(CDec(dtRow.Item("TotalAmt")), GUI.fmtCurrency)
                    rpt_Row.Item("Amount") = Format(CDec(dtRow.Item("TotalAmt")), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    rpt_Row.Item("Narration") = Language.getMessage(mstrModuleName, 14, "Dr")

                    rpt_Row.Item("Description") = mstrPeriodName & " " & Language.getMessage(mstrModuleName, 8, "Salary")

                    rpt_Row.Item("ChequeNo") = dtRow.Item("ChequeNo")


                    mdtExcelTable.Rows.Add(rpt_Row)
                Next
            Else
                '*** Add blank 
                Dim rpt_Row As DataRow
                rpt_Row = mdtExcelTable.NewRow

                mdtExcelTable.Rows.Add(rpt_Row)
            End If

            Dim intArrayColumnWidth As Integer() = {120, 80, 80, 80, 80, 120, 120, 120, 120, 80, 120, 120}
            'Sohail (12 Aug 2014) -- Start
            'Enhancement - New Orbit2 Report.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False, , mstrReportTypeName)
            'Sohail (12 Aug 2014) -- End

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList = Nothing
        End Try

    End Function

    'Sohail (12 Aug 2014) -- Start
    'Enhancement - New Orbit2 Report.
    Public Function Generate_DetailReport2(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           , ByVal strFmtCurrency As String _
                                           , ByVal intCurrencyId As Integer _
                                           , ByVal strExportReportPath As String _
                                           , ByVal blnOpenAfterExport As Boolean _
                                           ) As Boolean
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, intCurrencyId, strExportReportPath, blnOpenAfterExport]

        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        Dim mdecTotal As Decimal = 0
        Dim blnFlag As Boolean = False

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation
        Try

            mdtExcelTable = New DataTable("Orbit")
            Dim dCol As DataColumn

            dCol = New DataColumn("BatchName")
            dCol.Caption = Language.getMessage(mstrModuleName, 22, "ref")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EffectiveDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 23, "Create_dt")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("TransactionDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "Effective_dt")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpBankAccount")
            dCol.Caption = Language.getMessage(mstrModuleName, 25, "acc_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("AccTypeName")
            dCol.Caption = Language.getMessage(mstrModuleName, 26, "acc_type")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Narration")
            dCol.Caption = Language.getMessage(mstrModuleName, 27, "tr_typ")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Amount")
            dCol.Caption = Language.getMessage(mstrModuleName, 28, "amt")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("ChequeNo")
            dCol.Caption = Language.getMessage(mstrModuleName, 29, "chqno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Description")
            dCol.Caption = Language.getMessage(mstrModuleName, 30, "descr")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("AccTypeCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 31, "tran_cod")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)










            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intCurrencyId
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End


            strQ = "SELECT  hremployee_master.employeecode AS EmpCode " & _
                       "	,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName  " & _
                          ", CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) AS  PaymentDate " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid, 0) AS BGID " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupcode, '') AS BGCode " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname, '') AS BankName " & _
                          ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.swiftcode, '') AS EmpSwiftCode " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchunkid, 0) AS BRID " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchcode, '') AS BRCode " & _
                          ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchname, '') AS BranchName " & _
                          ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS CountryName  " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttypeunkid, 0) AS accounttypeunkid " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_code,'') AS AcTypeCode  " & _
                          ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_name,'') AS AcTypeName  " & _
                          ", ISNULL(premployee_bank_tran.accountno, '') AS EmpAccountNo  " & _
                          ", ISNULL(prpayment_tran.chequeno, '') AS ChequeNo " & _
                          ", ISNULL(prempsalary_tran.amount,0) AS Amount  " & _
                          ", prpayment_tran.paidcurrencyid " & _
                          ", cfexchange_rate.currency_name " & _
                          ", cfexchange_rate.currency_sign " & _
                          ", ISNULL(prempsalary_tran.expaidamt, 0 ) AS expaidamt " & _
                          ", ISNULL(cfbankbranch_master.sortcode, '') AS sortcode " & _
                          ", ISNULL(prpayment_tran.isauthorized, 0) AS isauthorized " & _
                          ", ECCT.costcenterunkid " & _
                          ", prcostcenter_master.costcentercode " & _
                          ", prcostcenter_master.costcentername " & _
                          ", @Cr AS Narration " & _
                    "FROM    prpayment_tran " & _
                        "JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                        "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                        "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                        "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid  " & _
                        "JOIN hrmsConfiguration..cfbankacctype_master ON hrmsConfiguration..cfbankacctype_master.accounttypeunkid = premployee_bank_tran.accounttypeunkid  " & _
                        "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid  " & _
                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid  " & _
                        "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                        "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            strQ &= "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = ECCT.costcenterunkid " & _
                    "WHERE  ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                    "AND prempsalary_tran.isvoid = 0 " & _
                    "AND premployee_bank_tran.isvoid = 0  " & _
                    "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                    "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1  " & _
                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    "AND prpayment_tran.periodunkid IN (" & mstrPeriodId & ") "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    strQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            Call FilterTitleAndFilterQuery()

            strQ &= Me._FilterQuery

            If Me.OrderByQuery <> "" Then
                strQ &= " ORDER BY " & Me.OrderByQuery
            End If

            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = mdtExcelTable.NewRow

                rpt_Row.Item("BatchName") = mstrBatchName
                rpt_Row.Item("EffectiveDate") = eZeeDate.convertDate(dtRow.Item("PaymentDate").ToString).ToString("MM/dd/yyyy")
                rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")

                'rpt_Row.Item("BaseCurrency") = dtRow.Item("currency_sign")
                'rpt_Row.Item("PaidCurrency") = dtRow.Item("currency_sign")

                'rpt_Row.Item("CCenterCode") = dtRow.Item("EmpSwiftCode")

                rpt_Row.Item("AccTypeCode") = dtRow.Item("AcTypeCode")
                rpt_Row.Item("AccTypeName") = dtRow.Item("AcTypeName")
                rpt_Row.Item("EmpBankAccount") = dtRow.Item("EmpAccountNo")

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Amount") = Format(dtRow.Item("expaidamt"), GUI.fmtCurrency)
                rpt_Row.Item("Amount") = Format(dtRow.Item("expaidamt"), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Narration") = dtRow.Item("Narration")

                rpt_Row.Item("Description") = mstrPeriodName & " " & Language.getMessage(mstrModuleName, 8, "Salary")

                rpt_Row.Item("ChequeNo") = dtRow.Item("ChequeNo")


                'mdecTotal += CDec(Format(dtRow.Item("expaidamt"), GUI.fmtCurrency))

                mdtExcelTable.Rows.Add(rpt_Row)
            Next

            If mdtExcelTable.Rows.Count > 0 Then

                objDataOperation.ClearParameters()

                strQ = "SELECT  SUM(CAST(ISNULL(prempsalary_tran.expaidamt, 0)AS DECIMAL(36, " & decDecimalPlaces & "))) AS TotalAmt " & _
                                         ", ISNULL(prpayment_tran.chequeno, '') AS ChequeNo " & _
                                         ", @Dr AS Narration " & _
                                   "FROM    prpayment_tran " & _
                                       "JOIN prempsalary_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                       "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                       "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid  " & _
                                       "JOIN premployee_bank_tran ON prempsalary_tran.empbanktranid = premployee_bank_tran.empbanktranunkid  " & _
                                       "JOIN hrmsConfiguration..cfbankacctype_master ON hrmsConfiguration..cfbankacctype_master.accounttypeunkid = premployee_bank_tran.accounttypeunkid  " & _
                                       "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid  " & _
                                       "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid  " & _
                                       "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfbankbranch_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                                       "LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = prpayment_tran.paidcurrencyid "

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If
                'Sohail (21 Aug 2015) -- End

                strQ &= "WHERE  ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                       "AND prempsalary_tran.isvoid = 0 " & _
                                       "AND premployee_bank_tran.isvoid = 0  " & _
                                       "AND hrmsConfiguration..cfbankbranch_master.isactive = 1  " & _
                                       "AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1  " & _
                                       "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                       "AND prpayment_tran.periodunkid IN (" & mstrPeriodId & ") "
                'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If mblnIncludeInactiveEmp = False Then
                '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
                'End If

                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    strQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        strQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If
                'Sohail (21 Aug 2015) -- End

                Call FilterTitleAndFilterQuery()

                strQ &= Me._FilterQuery

                strQ &= "GROUP BY prpayment_tran.chequeno "

                dsList = objDataOperation.ExecQuery(strQ, "Summary")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dtRow As DataRow In dsList.Tables("Summary").Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = mdtExcelTable.NewRow

                    rpt_Row.Item("BatchName") = mstrBatchName
                    rpt_Row.Item("EffectiveDate") = DateAndTime.Now.ToString("MM/dd/yyyy")
                    rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")

                    'rpt_Row.Item("BaseCurrency") = mstrCurrencySign
                    'rpt_Row.Item("PaidCurrency") = mstrCurrencySign

                    'rpt_Row.Item("CCenterCode") = mstrCompanyBankSwiftCode

                    rpt_Row.Item("AccTypeCode") = mstrCompanyBankAccountTypeCode
                    rpt_Row.Item("AccTypeName") = mstrCompanyBankAccountTypeName
                    rpt_Row.Item("EmpBankAccount") = mstrCompanyBankAccountNo

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'rpt_Row.Item("Amount") = Format(CDec(dtRow.Item("TotalAmt")), GUI.fmtCurrency)
                    rpt_Row.Item("Amount") = Format(CDec(dtRow.Item("TotalAmt")), strFmtCurrency)
                    'Sohail (21 Aug 2015) -- End
                    rpt_Row.Item("Narration") = Language.getMessage(mstrModuleName, 14, "Dr")

                    rpt_Row.Item("Description") = mstrPeriodName & " " & Language.getMessage(mstrModuleName, 8, "Salary")

                    rpt_Row.Item("ChequeNo") = dtRow.Item("ChequeNo")


                    mdtExcelTable.Rows.Add(rpt_Row)
                Next
            Else
                '*** Add blank 
                Dim rpt_Row As DataRow
                rpt_Row = mdtExcelTable.NewRow

                mdtExcelTable.Rows.Add(rpt_Row)
            End If

            Dim intArrayColumnWidth As Integer() = {120, 80, 80, 120, 80, 80, 120, 80, 120, 80}
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False, , mstrReportTypeName)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False, , mstrReportTypeName)
            'Sohail (21 Aug 2015) -- End

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport2; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            dsList = Nothing
        End Try

    End Function
    'Sohail (12 Aug 2014) -- End


    'S.SANDEEP [ 03 OCT 2014 ] -- START
    Dim iColumn_Orbit3_DetailReport As New IColumnCollection
    Public Property Field_OnObbit3DetailProperty() As IColumnCollection
        Get
            Return iColumn_Orbit3_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_Orbit3_DetailReport = value
        End Set
    End Property

    Private Sub Create_Orbit3DetailReport()

        Try
            iColumn_Orbit3_DetailReport.Clear()
            iColumn_Orbit3_DetailReport.Add(New IColumn("ISNULL(prpayrollprocess_tran.amount,0)", Language.getMessage(mstrModuleName, 32, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Function Generate_Orbit3_DetailReport(ByVal strDatabaseName As String _
                                                 , ByVal xUserUnkid As Integer _
                                                 , ByVal xYearUnkid As Integer _
                                                 , ByVal xCompanyUnkid As Integer _
                                                 , ByVal xPeriodStart As Date _
                                                 , ByVal xPeriodEnd As Date _
                                                 , ByVal xUserModeSetting As String _
                                                 , ByVal xOnlyApproved As Boolean _
                                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                 , ByVal blnApplyUserAccessFilter As Boolean _
                                                 , ByVal strFmtCurrency As String _
                                                 , ByVal intCurrencyId As Integer _
                                                 , ByVal strExportReportPath As String _
                                                 , ByVal blnOpenAfterExport As Boolean _
                                                 ) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFmtCurrency, intCurrencyId, strExportReportPath, blnOpenAfterExport]

        Dim StrQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        Dim mdecTotal As Decimal = 0
        Dim blnFlag As Boolean = False

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , strDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, strDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, strDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation
        Try
            mdtExcelTable = New DataTable("Orbit")
            Dim dCol As DataColumn

            dCol = New DataColumn("BatchName")
            dCol.Caption = Language.getMessage(mstrModuleName, 22, "ref")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EffectiveDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 23, "Create_dt")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("TransactionDate")
            dCol.Caption = Language.getMessage(mstrModuleName, 24, "Effective_dt")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("EmpBankAccount")
            dCol.Caption = Language.getMessage(mstrModuleName, 25, "acc_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("AccTypeName")
            dCol.Caption = Language.getMessage(mstrModuleName, 26, "acc_type")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Narration")
            dCol.Caption = Language.getMessage(mstrModuleName, 27, "tr_typ")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Amount")
            dCol.Caption = Language.getMessage(mstrModuleName, 28, "amt")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("ChequeNo")
            dCol.Caption = Language.getMessage(mstrModuleName, 29, "chqno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("Description")
            dCol.Caption = Language.getMessage(mstrModuleName, 30, "descr")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn("AccTypeCode")
            dCol.Caption = Language.getMessage(mstrModuleName, 31, "tran_cod")
            dCol.DataType = System.Type.GetType("System.String")
            mdtExcelTable.Columns.Add(dCol)

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intCurrencyId
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            StrQ = "SELECT " & _
                   "     hremployee_master.employeecode AS EmpCode " & _
                   "    ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, '') AS EmpName " & _
                   "    ,'' AS PaymentDate " & _
                   "    ,ISNULL(AccNo.AccNo,'') AS EmpAccountNo " & _
                   "    ,ISNULL(AccNo.Acc_Code,'') AS AcTypeCode " & _
                   "    ,ISNULL(AccNo.Acc_Name,'') AS AcTypeName " & _
                   "    ,'' AS ChequeNo " & _
                   "    ,ISNULL(prpayrollprocess_tran.amount,0) AS Amount " & _
                   "    ,ECCT.costcenterunkid " & _
                   "    ,prcostcenter_master.costcentercode " & _
                   "    ,prcostcenter_master.costcentername " & _
                   "    ,@Cr AS Narration " & _
                   "FROM prpayrollprocess_tran " & _
                   "    JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                   "    JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hremployee_master.employeeunkid AS EmpId " & _
                   "            ,STUFF((SELECT ','+ accountno FROM premployee_bank_tran AS t1 WHERE premployee_bank_tran.employeeunkid = t1.employeeunkid FOR XML PATH('')),1,1,'') AS AccNo " & _
                   "            ,STUFF((SELECT ','+ accounttype_code FROM hrmsConfiguration..cfbankacctype_master AS t1 WHERE premployee_bank_tran.accounttypeunkid = t1.accounttypeunkid FOR XML PATH('')),1,1,'') AS Acc_Code " & _
                   "            ,STUFF((SELECT ','+ accounttype_name FROM hrmsConfiguration..cfbankacctype_master AS t1 WHERE premployee_bank_tran.accounttypeunkid = t1.accounttypeunkid FOR XML PATH('')),1,1,'') AS Acc_Name " & _
                   "        FROM premployee_bank_tran " & _
                   "            JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_bank_tran.employeeunkid " & _
                   "            JOIN hrmsConfiguration..cfbankacctype_master AS ACTYP ON ACTYP.accounttypeunkid = premployee_bank_tran.accounttypeunkid " & _
                   "            JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                   "            JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                   "        WHERE isvoid = 0 AND hrmsConfiguration..cfbankbranch_master.isactive = 1 AND hrmsConfiguration..cfpayrollgroup_master.isactive = 1 "

            If mintBankId > 0 Then
                'objDataOperation.AddParameter("@BankId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBankId) 'Sohail (29 Oct 2014) - Error: Parameter is already exist
                StrQ &= " AND hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = @BankId "
            End If

            If mintBranchId > 0 Then
                'objDataOperation.AddParameter("@BranchName", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId) 'Sohail (29 Oct 2014) - Error: Parameter is already exist
                StrQ &= " AND hrmsConfiguration..cfbankbranch_master.branchunkid = @BranchName "
            End If

            StrQ &= "    ) AS AccNo ON AccNo.EmpId = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "    LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = ECCT.costcenterunkid " & _
                    "WHERE lnloan_advance_tran.isvoid = 0 AND prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                    "AND payperiodunkid IN (" & mstrPeriodId & ") AND prpayrollprocess_tran.loanadvancetranunkid > 0 "

            Select Case mintReportTypeId
                Case 3  'LOAN 
                    StrQ &= "AND isloan = 1 "
                Case 4  'ADVANCE
                    StrQ &= "AND isloan = 0 "
            End Select

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If
            'Sohail (21 Aug 2015) -- End

            If mdblAmount > 0 And mdblAmountTo > 0 Then
                objDataOperation.AddParameter("@Amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmount)
                objDataOperation.AddParameter("@AmountTo", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblAmountTo)
                Me._FilterQuery &= " AND ISNULL(prpayrollprocess_tran.amount,0) BETWEEN  @Amount AND @AmountTo "
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End

            Call FilterTitleAndFilterQuery()



            If Me.OrderByQuery <> "" Then
                StrQ &= " ORDER BY " & Me.OrderByQuery
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim xPrdEndDate As Date = Nothing
            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mstrPeriodId
            objPrd._Periodunkid(strDatabaseName) = mstrPeriodId
            'Sohail (21 Aug 2015) -- End
            xPrdEndDate = objPrd._End_Date
            objPrd = Nothing

            Dim rpt_Row As DataRow = Nothing
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                rpt_Row = mdtExcelTable.NewRow
                rpt_Row.Item("BatchName") = mstrBatchName
                rpt_Row.Item("EffectiveDate") = xPrdEndDate.ToString("MM/dd/yyyy")
                rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")
                rpt_Row.Item("AccTypeCode") = dtRow.Item("AcTypeCode")
                rpt_Row.Item("AccTypeName") = dtRow.Item("AcTypeName")
                rpt_Row.Item("EmpBankAccount") = dtRow.Item("EmpAccountNo")
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Amount") = Format(dtRow.Item("Amount"), GUI.fmtCurrency)
                rpt_Row.Item("Amount") = Format(dtRow.Item("Amount"), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Narration") = dtRow.Item("Narration")
                rpt_Row.Item("Description") = mstrPeriodName
                rpt_Row.Item("ChequeNo") = dtRow.Item("ChequeNo")
                mdtExcelTable.Rows.Add(rpt_Row)
            Next
            If mdtExcelTable.Rows.Count > 0 Then
                rpt_Row = mdtExcelTable.NewRow
                rpt_Row.Item("BatchName") = mstrBatchName
                rpt_Row.Item("EffectiveDate") = DateAndTime.Now.ToString("MM/dd/yyyy")
                rpt_Row.Item("TransactionDate") = DateAndTime.Now.ToString("MM/dd/yyyy")
                rpt_Row.Item("AccTypeCode") = mstrCompanyBankAccountTypeCode
                rpt_Row.Item("AccTypeName") = mstrCompanyBankAccountTypeName
                rpt_Row.Item("EmpBankAccount") = mstrCompanyBankAccountNo
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'rpt_Row.Item("Amount") = Format(CDec(mdtExcelTable.Compute("SUM(Amount)", "")), GUI.fmtCurrency)
                rpt_Row.Item("Amount") = Format(CDec(mdtExcelTable.Compute("SUM(Amount)", "")), strFmtCurrency)
                'Sohail (21 Aug 2015) -- End
                rpt_Row.Item("Narration") = Language.getMessage(mstrModuleName, 14, "Dr")
                rpt_Row.Item("Description") = mstrPeriodName
                rpt_Row.Item("ChequeNo") = ""
                mdtExcelTable.Rows.Add(rpt_Row)
            Else
                '*** Add blank 
                rpt_Row = mdtExcelTable.NewRow
                mdtExcelTable.Rows.Add(rpt_Row)
            End If
            Dim intArrayColumnWidth As Integer() = {120, 80, 80, 120, 80, 80, 120, 80, 120, 80}
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False, , mstrReportTypeName)
            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, mdtExcelTable, intArrayColumnWidth, mblnIncludeReportColumnHeader, mblnIncludeReportColumnHeader, False, , mstrReportTypeName)
            'Sohail (21 Aug 2015) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Orbit3_Loan; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 03 OCT 2014 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Batch Name")
            Language.setMessage(mstrModuleName, 2, "Effective Date")
            Language.setMessage(mstrModuleName, 3, "Transaction Date")
            Language.setMessage(mstrModuleName, 4, "Currency")
            Language.setMessage(mstrModuleName, 5, "Swift Code")
            Language.setMessage(mstrModuleName, 6, "Account Type Code")
            Language.setMessage(mstrModuleName, 7, "Employee Bank Account")
            Language.setMessage(mstrModuleName, 8, "Salary")
            Language.setMessage(mstrModuleName, 9, "Narration")
            Language.setMessage(mstrModuleName, 10, "Description")
            Language.setMessage(mstrModuleName, 11, "ChequeNo")
            Language.setMessage(mstrModuleName, 12, "Order By :")
            Language.setMessage(mstrModuleName, 13, "Cr")
            Language.setMessage(mstrModuleName, 14, "Dr")
            Language.setMessage(mstrModuleName, 15, "Bank Name :")
            Language.setMessage(mstrModuleName, 16, "Branch Name :")
            Language.setMessage(mstrModuleName, 17, "Company Bank Branch :")
            Language.setMessage(mstrModuleName, 18, "Company Bank Account :")
            Language.setMessage(mstrModuleName, 19, "Amount From :")
            Language.setMessage(mstrModuleName, 20, " To")
            Language.setMessage(mstrModuleName, 21, "Paid Currency :")
            Language.setMessage(mstrModuleName, 22, "ref")
            Language.setMessage(mstrModuleName, 23, "Create_dt")
            Language.setMessage(mstrModuleName, 24, "Effective_dt")
            Language.setMessage(mstrModuleName, 25, "acc_no")
            Language.setMessage(mstrModuleName, 26, "acc_type")
            Language.setMessage(mstrModuleName, 27, "tr_typ")
            Language.setMessage(mstrModuleName, 28, "amt")
            Language.setMessage(mstrModuleName, 29, "chqno")
            Language.setMessage(mstrModuleName, 30, "descr")
            Language.setMessage(mstrModuleName, 31, "tran_cod")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

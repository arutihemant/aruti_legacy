﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantCVReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.chkShowAttachments = New System.Windows.Forms.CheckBox
        Me.chkShowReferences = New System.Windows.Forms.CheckBox
        Me.chkShowJobHistory = New System.Windows.Forms.CheckBox
        Me.chkShowShortCourses = New System.Windows.Forms.CheckBox
        Me.chkShowProfessionalQualification = New System.Windows.Forms.CheckBox
        Me.chkShowOtherSkills = New System.Windows.Forms.CheckBox
        Me.chkShpwProfessionalSkills = New System.Windows.Forms.CheckBox
        Me.objbtnSearchApplicant = New eZee.Common.eZeeGradientButton
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.LalblVacancy = New System.Windows.Forms.Label
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 443)
        Me.NavPanel.Size = New System.Drawing.Size(724, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowAttachments)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowReferences)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowJobHistory)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowShortCourses)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowProfessionalQualification)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowOtherSkills)
        Me.gbFilterCriteria.Controls.Add(Me.chkShpwProfessionalSkills)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.cboApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplicant)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(430, 320)
        Me.gbFilterCriteria.TabIndex = 3
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 118)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(92, 15)
        Me.lblStatus.TabIndex = 111
        Me.lblStatus.Text = "Approval Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(391, 87)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 34
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 255
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(106, 115)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(280, 21)
        Me.cboStatus.TabIndex = 110
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(6, 62)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(92, 15)
        Me.lblVacancyType.TabIndex = 30
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 350
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(106, 32)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(280, 21)
        Me.cboReportType.TabIndex = 39
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(6, 35)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(92, 15)
        Me.lblReportType.TabIndex = 38
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowAttachments
        '
        Me.chkShowAttachments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowAttachments.Location = New System.Drawing.Point(108, 298)
        Me.chkShowAttachments.Name = "chkShowAttachments"
        Me.chkShowAttachments.Size = New System.Drawing.Size(290, 15)
        Me.chkShowAttachments.TabIndex = 36
        Me.chkShowAttachments.Text = "Show Attachments"
        Me.chkShowAttachments.UseVisualStyleBackColor = True
        '
        'chkShowReferences
        '
        Me.chkShowReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReferences.Location = New System.Drawing.Point(108, 278)
        Me.chkShowReferences.Name = "chkShowReferences"
        Me.chkShowReferences.Size = New System.Drawing.Size(290, 15)
        Me.chkShowReferences.TabIndex = 34
        Me.chkShowReferences.Text = "Show References"
        Me.chkShowReferences.UseVisualStyleBackColor = True
        '
        'chkShowJobHistory
        '
        Me.chkShowJobHistory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowJobHistory.Location = New System.Drawing.Point(108, 257)
        Me.chkShowJobHistory.Name = "chkShowJobHistory"
        Me.chkShowJobHistory.Size = New System.Drawing.Size(290, 15)
        Me.chkShowJobHistory.TabIndex = 33
        Me.chkShowJobHistory.Text = "Show Job History"
        Me.chkShowJobHistory.UseVisualStyleBackColor = True
        '
        'chkShowShortCourses
        '
        Me.chkShowShortCourses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowShortCourses.Location = New System.Drawing.Point(108, 236)
        Me.chkShowShortCourses.Name = "chkShowShortCourses"
        Me.chkShowShortCourses.Size = New System.Drawing.Size(290, 15)
        Me.chkShowShortCourses.TabIndex = 32
        Me.chkShowShortCourses.Text = "Show Other Qualification / Short Courses"
        Me.chkShowShortCourses.UseVisualStyleBackColor = True
        '
        'chkShowProfessionalQualification
        '
        Me.chkShowProfessionalQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowProfessionalQualification.Location = New System.Drawing.Point(108, 215)
        Me.chkShowProfessionalQualification.Name = "chkShowProfessionalQualification"
        Me.chkShowProfessionalQualification.Size = New System.Drawing.Size(290, 15)
        Me.chkShowProfessionalQualification.TabIndex = 31
        Me.chkShowProfessionalQualification.Text = "Show Professional Qualification"
        Me.chkShowProfessionalQualification.UseVisualStyleBackColor = True
        '
        'chkShowOtherSkills
        '
        Me.chkShowOtherSkills.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowOtherSkills.Location = New System.Drawing.Point(108, 194)
        Me.chkShowOtherSkills.Name = "chkShowOtherSkills"
        Me.chkShowOtherSkills.Size = New System.Drawing.Size(290, 15)
        Me.chkShowOtherSkills.TabIndex = 30
        Me.chkShowOtherSkills.Text = "Show Other Skills"
        Me.chkShowOtherSkills.UseVisualStyleBackColor = True
        '
        'chkShpwProfessionalSkills
        '
        Me.chkShpwProfessionalSkills.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShpwProfessionalSkills.Location = New System.Drawing.Point(108, 173)
        Me.chkShpwProfessionalSkills.Name = "chkShpwProfessionalSkills"
        Me.chkShpwProfessionalSkills.Size = New System.Drawing.Size(290, 15)
        Me.chkShpwProfessionalSkills.TabIndex = 29
        Me.chkShpwProfessionalSkills.Text = "Shpw Professional Skills"
        Me.chkShpwProfessionalSkills.UseVisualStyleBackColor = True
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApplicant.BorderSelected = False
        Me.objbtnSearchApplicant.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApplicant.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(391, 143)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApplicant.TabIndex = 2
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.DropDownWidth = 350
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(106, 143)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(280, 21)
        Me.cboApplicant.TabIndex = 1
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(6, 146)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(92, 15)
        Me.lblApplicant.TabIndex = 0
        Me.lblApplicant.Text = "Applicant"
        Me.lblApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 450
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.IntegralHeight = False
        Me.cboVacancy.Location = New System.Drawing.Point(118, 153)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(280, 21)
        Me.cboVacancy.TabIndex = 33
        '
        'LalblVacancy
        '
        Me.LalblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LalblVacancy.Location = New System.Drawing.Point(19, 155)
        Me.LalblVacancy.Name = "LalblVacancy"
        Me.LalblVacancy.Size = New System.Drawing.Size(92, 15)
        Me.LalblVacancy.TabIndex = 32
        Me.LalblVacancy.Text = "Vacancy"
        Me.LalblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(118, 126)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(280, 21)
        Me.cboVacancyType.TabIndex = 31
        '
        'frmApplicantCVReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 498)
        Me.Controls.Add(Me.cboVacancy)
        Me.Controls.Add(Me.LalblVacancy)
        Me.Controls.Add(Me.cboVacancyType)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantCVReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.cboVacancyType, 0)
        Me.Controls.SetChildIndex(Me.LalblVacancy, 0)
        Me.Controls.SetChildIndex(Me.cboVacancy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkShowReferences As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowJobHistory As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowShortCourses As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowProfessionalQualification As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowOtherSkills As System.Windows.Forms.CheckBox
    Friend WithEvents chkShpwProfessionalSkills As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents chkShowAttachments As System.Windows.Forms.CheckBox
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LalblVacancy As System.Windows.Forms.Label
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
End Class

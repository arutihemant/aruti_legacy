Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

Public Class clsMedicalClaimReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMedicalClaimReport"
    Private mstrReportId As String = enArutiReport.TRAMedical_Claim_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintProviderunkid As Integer = 0
    Private mstrProviderName As String = ""

    Private mstrPeriodunkIds As String = ""
    Private mstrPeriodName As String = ""
    Private mstrOrderByQuery As String = ""

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty


    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIncludeInactiveEmp As Boolean = False
    'Pinkal (11-MAY-2012) -- End


    'Pinkal (15-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (15-MAY-2012) -- End



    'Pinkal (03-Jun-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnMyInvoiceOnly As Boolean = True
    'Pinkal (03-Jun-2012) -- End

    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (19-Nov-2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End



#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ProviderId() As Integer
        Set(ByVal value As Integer)
            mintProviderunkid = value
        End Set
    End Property

    Public WriteOnly Property _ProviderName() As String
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodunkIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    'Pinkal (11-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'Pinkal (11-MAY-2012) -- End


    'Pinkal (15-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'Pinkal (15-MAY-2012) -- End



    'Pinkal (03-Jun-2012) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _MyInvoiceOnly() As Boolean
        Set(ByVal value As Boolean)
            mblnMyInvoiceOnly = value
        End Set
    End Property

    'Pinkal (03-Jun-2012) -- End


    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (19-Nov-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtAsOnDate As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
    Public WriteOnly Property _AsOnDate() As Date
        Set(ByVal value As Date)
            mdtAsOnDate = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintProviderunkid = 0
            mstrProviderName = ""
            mstrPeriodunkIds = ""
            mstrPeriodName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mblnIncludeInactiveEmp = False
            'Pinkal (11-MAY-2012) -- End


            'Pinkal (03-Jun-2012) -- Start
            'Enhancement : TRA Changes
            mblnMyInvoiceOnly = True
            'Pinkal (03-Jun-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

            'Pinkal (14-Sep-2015) -- Start
            'Enhancement - Bug Solved for 58.1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Pinkal (14-Sep-2015) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintProviderunkid > 0 Then
                Me._FilterQuery &= " AND mdmedical_claim_master.instituteunkid = @instituteunkid "
                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProviderunkid)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Provider : ") & " " & mstrProviderName & " "
            End If

            If mstrPeriodunkIds.Trim.Length > 0 Then
                Me._FilterQuery &= " AND mdmedical_claim_master.periodunkid in (" & mstrPeriodunkIds & ")"
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Period: ") & " " & mstrPeriodName & " "
            End If

            'Pinkal (11-MAY-2012) -- Start
            'Enhancement : TRA Changes

            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            'Pinkal (11-MAY-2012) -- End

            If Me.OrderByQuery <> "" Then
                mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (19-Nov-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (19-Nov-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyUnkid As Integer = 0)
        Dim ObjRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid < 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid < 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If
            User._Object._Userunkid = mintUserUnkid

            ObjRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)

            Rpt = ObjRpt
            If Not IsNothing(ObjRpt) Then
                Call ReportExecute(ObjRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan [08 September 2015] -- End
    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("period_name", Language.getMessage(mstrModuleName, 24, "Period")))
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 25, "Payroll No")))
            iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 26, "Staff Name")))
            iColumn_DetailReport.Add(New IColumn("depedents", Language.getMessage(mstrModuleName, 7, "Name")))
            iColumn_DetailReport.Add(New IColumn("claimdate", Language.getMessage(mstrModuleName, 6, "Date")))

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'iColumn_DetailReport.Add(New IColumn("amount1", Language.getMessage(mstrModuleName, 9, "80%")))
            'iColumn_DetailReport.Add(New IColumn("amount2", Language.getMessage(mstrModuleName, 10, "20%")))

            iColumn_DetailReport.Add(New IColumn("amount1", Language.getMessage(mstrModuleName, 9, "Employer")))
            iColumn_DetailReport.Add(New IColumn("amount2", Language.getMessage(mstrModuleName, 10, "Employee")))
            'Pinkal (18-Dec-2012) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Anjan [08 September 2015] -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Anjan [08 September 2015] -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Anjan [08 September 2015] -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@self", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "SELF"))

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mdtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= "SELECT DISTINCT "


            StrQ &= " mdmedical_claim_master.instituteunkid " & _
                      ",hrinstitute_master.institute_name " & _
                      ",mdmedical_claim_master.periodunkid " & _
                      ",cfcommon_period_tran.period_name " & _
                      ",mdmedical_claim_master.invoiceno " & _
                      ",mdmedical_claim_tran.employeeunkid " & _
                      ",hremployee_master.employeecode " & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') employee " & _
                      ",CONVERT(VARCHAR(10),mdmedical_claim_tran.claimdate,112) claimdate " & _
                      ",CASE WHEN ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'') <> '' THEN " & _
                      "         ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name,'')  " & _
                      " ELSE  @Self  END depedents" & _
                      ", mdmedical_claim_master.invoice_amt " & _
                      ", mdmedical_claim_tran.amount "

            StrQ &= ", CASE WHEN isempexempted = 1 THEN mdmedical_claim_tran.amount  ELSE (mdmedical_claim_tran.amount * 80 / 100) END  AS amount1 " & _
                         ", CASE WHEN isempexempted = 1 THEN 0.00 ELSE (mdmedical_claim_tran.amount * 20 / 100) END AS amount2 " & _
                      ",mdmedical_claim_master.statusunkid "



            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM  mdmedical_claim_master " & _
                      " JOIN mdmedical_claim_tran ON dbo.mdmedical_claim_master.claimunkid = dbo.mdmedical_claim_tran.claimunkid " & _
                      " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdmedical_claim_master.instituteunkid " & _
                      " JOIN cfcommon_period_tran ON mdmedical_claim_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = mdmedical_claim_tran.employeeunkid " & _
                      " LEFT JOIN hrdependants_beneficiaries_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_claim_tran.dependantsunkid " & _
                      " LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid "
            'Sohail (18 May 2019) - [JOIN #TableDepn]

            'Pinkal (03-Jun-2012) -- Start
            'Enhancement : TRA Changes

            If mblnMyInvoiceOnly Then
                If User._Object._Userunkid > 1 Then

                    'Pinkal (06 DEC 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                    'StrQ &= " JOIN mdprovider_mapping on mdprovider_mapping.providerunkid = mdmedical_claim_master.instituteunkid  And mdprovider_mapping.userunkid = " & User._Object._Userunkid
                    StrQ &= " JOIN mdprovider_mapping on mdprovider_mapping.providerunkid = mdmedical_claim_master.instituteunkid  And mdmedical_claim_master.userunkid = " & User._Object._Userunkid
                    'Pinkal (06 DEC 2012)-End 


                End If
            Else
                If User._Object._Userunkid > 1 Then
                    StrQ &= " JOIN mdprovider_mapping on mdprovider_mapping.providerunkid = mdmedical_claim_master.instituteunkid  "
                End If
            End If


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'Anjan [08 September 2015] -- End


            StrQ &= mstrAnalysis_Join


            StrQ &= " WHERE mdmedical_claim_master.isvoid = 0  AND isfinal = 1 " & _
                    " AND ISNULL(#TableDepn.isactive, 1) = 1 "
            'Sohail (18 May 2019) - [isactive]

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'Pinkal (27-Feb-2013) -- End

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= "AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If
            'Anjan [08 September 2015] -- End



            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mintEmployeeunkid As Integer = 0
            Dim mintPeriodunkid As Integer = 0
            Dim mdblAmount1 As Decimal = 0
            Dim mdblAmount2 As Decimal = 0
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("period_name")
                rpt_Row.Item("Column2") = dtRow.Item("employee")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("claimdate").ToString()).ToShortDateString()
                rpt_Row.Item("Column5") = dtRow.Item("depedents")
                rpt_Row.Item("Column6") = CDec(Format(dtRow.Item("amount"), GUI.fmtCurrency))
                rpt_Row.Item("Column7") = CDec(Format(dtRow.Item("amount1"), GUI.fmtCurrency))
                rpt_Row.Item("Column8") = CDec(Format(dtRow.Item("amount2"), GUI.fmtCurrency))
                rpt_Row.Item("Column9") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", "employeeunkid =" & CInt(dtRow("employeeunkid")) & " AND periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                rpt_Row.Item("Column10") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", "employeeunkid =" & CInt(dtRow("employeeunkid")) & " AND periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                rpt_Row.Item("Column11") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", "employeeunkid =" & CInt(dtRow("employeeunkid")) & " AND periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))


                'Pinkal (14-Sep-2015) -- Start
                'Enhancement - Bug Solved for 58.1

                'rpt_Row.Item("Column12") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                'rpt_Row.Item("Column13") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                'rpt_Row.Item("Column14") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))

                If CInt(dtRow.Item("Id")) > 0 Then
                    rpt_Row.Item("Column12") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", "periodunkid = " & CInt(dtRow("periodunkid")) & " AND Id = " & CInt(dtRow.Item("Id"))), GUI.fmtCurrency))
                    rpt_Row.Item("Column13") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", "periodunkid = " & CInt(dtRow("periodunkid")) & " AND Id = " & CInt(dtRow.Item("Id"))), GUI.fmtCurrency))
                    rpt_Row.Item("Column14") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", "periodunkid = " & CInt(dtRow("periodunkid")) & " AND Id = " & CInt(dtRow.Item("Id"))), GUI.fmtCurrency))
                Else
                    rpt_Row.Item("Column12") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                    rpt_Row.Item("Column13") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                    rpt_Row.Item("Column14") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", "periodunkid = " & CInt(dtRow("periodunkid"))), GUI.fmtCurrency))
                End If
                'Pinkal (14-Sep-2015) -- End


                rpt_Row.Item("Column15") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", ""), GUI.fmtCurrency))
                rpt_Row.Item("Column16") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", ""), GUI.fmtCurrency))
                rpt_Row.Item("Column17") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", ""), GUI.fmtCurrency))
                rpt_Row.Item("Column23") = dtRow.Item("GName")
                rpt_Row.Item("Column24") = CDec(Format(dsList.Tables(0).Compute("SUM(amount)", "Id=" & CInt(dtRow("Id"))), GUI.fmtCurrency))
                rpt_Row.Item("Column25") = CDec(Format(dsList.Tables(0).Compute("SUM(amount1)", "Id=" & CInt(dtRow("Id"))), GUI.fmtCurrency))
                rpt_Row.Item("Column26") = CDec(Format(dsList.Tables(0).Compute("SUM(amount2)", "Id=" & CInt(dtRow("Id"))), GUI.fmtCurrency))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptTRAMedicalClaim

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 2, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 3, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 4, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 5, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtdate", Language.getMessage(mstrModuleName, 6, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtname", Language.getMessage(mstrModuleName, 7, "Name "))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 8, "Amount"))


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'Call ReportFunction.TextChange(objRpt, "txtAmount1", Language.getMessage(mstrModuleName, 9, "80%"))
            'Call ReportFunction.TextChange(objRpt, "txtAmount2", Language.getMessage(mstrModuleName, 10, "20%"))

            Call ReportFunction.TextChange(objRpt, "txtAmount1", Language.getMessage(mstrModuleName, 9, "Employer"))
            Call ReportFunction.TextChange(objRpt, "txtAmount2", Language.getMessage(mstrModuleName, 10, "Employee"))

            'Pinkal (18-Dec-2012) -- End

            Call ReportFunction.TextChange(objRpt, "txtPeriodName", Language.getMessage(mstrModuleName, 11, "Period : "))
            Call ReportFunction.TextChange(objRpt, "txtPayrollNo", Language.getMessage(mstrModuleName, 12, "Payroll No : "))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 13, "Staff Name : "))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 15, "Group Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 16, "Grand Total : "))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 17, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 18, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 19, "Page :"))



            'Pinkal (15-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'Select Case mintViewIndex

            '    Case enAnalysisReport.Branch
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 20, "Branch :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 32, "Branch Total :"))

            '    Case enAnalysisReport.Department
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 21, "Department :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 33, "Department Total :"))

            '    Case enAnalysisReport.Section
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 22, "Section :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 34, "Section Total :"))

            '    Case enAnalysisReport.Unit
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 23, "Unit :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 35, "Unit Total :"))

            '    Case enAnalysisReport.Job
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 24, "Job :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 36, "Job Total :"))

            '    Case enAnalysisReport.CostCenter
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 25, "Cost Center :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 37, "Cost Center Total :"))

            '    Case enAnalysisReport.SectionGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 26, "Section Group :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 38, "Section Total :"))

            '    Case enAnalysisReport.UnitGroup
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 27, "Unit Group :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 39, "Unit Total :"))

            '    Case enAnalysisReport.Team
            '        Call ReportFunction.TextChange(objRpt, "txtGroupName", Language.getMessage(mstrModuleName, 28, "Team :"))
            '        Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", Language.getMessage(mstrModuleName, 40, "Team Total :"))

            '    Case Else
            '        'Call ReportFunction.TextChange(objRpt, "txtGroupName", "")
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
            '        ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            'End Select

            If mstrReport_GroupName <> Nothing AndAlso mstrReport_GroupName.Trim.Length > 0 Then

                Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
                Call ReportFunction.TextChange(objRpt, "txtAllocationTotal", mstrReport_GroupName.Trim.Replace(":", "") & Language.getMessage(mstrModuleName, 20, " Total : "))

            Else
                ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection3", True)
                ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection3", True)
            End If

            'Pinkal (15-MAY-2012) -- End


            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "SELF")
            Language.setMessage(mstrModuleName, 2, "Prepared By :")
            Language.setMessage(mstrModuleName, 3, "Checked By :")
            Language.setMessage(mstrModuleName, 4, "Approved By :")
            Language.setMessage(mstrModuleName, 5, "Received By :")
            Language.setMessage(mstrModuleName, 6, "Date")
            Language.setMessage(mstrModuleName, 7, "Name")
            Language.setMessage(mstrModuleName, 8, "Amount")
            Language.setMessage(mstrModuleName, 9, "Employer")
            Language.setMessage(mstrModuleName, 10, "Employee")
            Language.setMessage(mstrModuleName, 11, "Period :")
            Language.setMessage(mstrModuleName, 12, "Payroll No :")
            Language.setMessage(mstrModuleName, 13, "Staff Name :")
            Language.setMessage(mstrModuleName, 14, "Sub Total :")
            Language.setMessage(mstrModuleName, 15, "Group Total :")
            Language.setMessage(mstrModuleName, 16, "Grand Total :")
            Language.setMessage(mstrModuleName, 17, "Printed By :")
            Language.setMessage(mstrModuleName, 18, "Printed Date :")
            Language.setMessage(mstrModuleName, 19, "Page :")
            Language.setMessage(mstrModuleName, 20, " Total :")
            Language.setMessage(mstrModuleName, 21, "Provider :")
            Language.setMessage(mstrModuleName, 22, "Period:")
            Language.setMessage(mstrModuleName, 23, " Order By :")
            Language.setMessage(mstrModuleName, 24, "Period")
            Language.setMessage(mstrModuleName, 25, "Payroll No")
            Language.setMessage(mstrModuleName, 26, "Staff Name")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

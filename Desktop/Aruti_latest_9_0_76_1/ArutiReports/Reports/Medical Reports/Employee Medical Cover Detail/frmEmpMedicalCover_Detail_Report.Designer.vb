﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpMedicalCover_Detail_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpMedicalCover_Detail_Report))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.objelSep1 = New eZee.Common.eZeeLine
        Me.objbtnSearchChild = New eZee.Common.eZeeGradientButton
        Me.cboChild = New System.Windows.Forms.ComboBox
        Me.LblChild = New System.Windows.Forms.Label
        Me.objbtnSearchSpouse = New eZee.Common.eZeeGradientButton
        Me.cboSpouse = New System.Windows.Forms.ComboBox
        Me.LblSpouse = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objSpc2 = New System.Windows.Forms.SplitContainer
        Me.txtSearchAlloctation = New System.Windows.Forms.TextBox
        Me.objchkAllocAll = New System.Windows.Forms.CheckBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.objcolhCheckAllocAll = New System.Windows.Forms.ColumnHeader
        Me.objcolhAllocation = New System.Windows.Forms.ColumnHeader
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.objSpc1 = New System.Windows.Forms.SplitContainer
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvServiceProvider = New System.Windows.Forms.ListView
        Me.objcolhCheckAll = New System.Windows.Forms.ColumnHeader
        Me.colhServiceProvider = New System.Windows.Forms.ColumnHeader
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.objSpc2.Panel1.SuspendLayout()
        Me.objSpc2.Panel2.SuspendLayout()
        Me.objSpc2.SuspendLayout()
        Me.objSpc1.Panel1.SuspendLayout()
        Me.objSpc1.Panel2.SuspendLayout()
        Me.objSpc1.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 568)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(706, 55)
        Me.EZeeFooter1.TabIndex = 5
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(413, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(509, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(605, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(706, 60)
        Me.eZeeHeader.TabIndex = 6
        Me.eZeeHeader.Title = "Medical Cover Detail Report"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSave)
        Me.gbFilterCriteria.Controls.Add(Me.objelSep1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchChild)
        Me.gbFilterCriteria.Controls.Add(Me.cboChild)
        Me.gbFilterCriteria.Controls.Add(Me.LblChild)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.cboSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.LblSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.objSpc2)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.objSpc1)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(620, 389)
        Me.gbFilterCriteria.TabIndex = 7
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(329, 137)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(282, 17)
        Me.EZeeLine1.TabIndex = 236
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSave
        '
        Me.lnkSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.Location = New System.Drawing.Point(530, 61)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(84, 17)
        Me.lnkSave.TabIndex = 235
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "Save Selection"
        '
        'objelSep1
        '
        Me.objelSep1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objelSep1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelSep1.Location = New System.Drawing.Point(329, 61)
        Me.objelSep1.Name = "objelSep1"
        Me.objelSep1.Size = New System.Drawing.Size(195, 17)
        Me.objelSep1.TabIndex = 234
        Me.objelSep1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchChild
        '
        Me.objbtnSearchChild.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchChild.BorderSelected = False
        Me.objbtnSearchChild.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchChild.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchChild.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchChild.Location = New System.Drawing.Point(590, 113)
        Me.objbtnSearchChild.Name = "objbtnSearchChild"
        Me.objbtnSearchChild.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchChild.TabIndex = 232
        '
        'cboChild
        '
        Me.cboChild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChild.DropDownWidth = 120
        Me.cboChild.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChild.FormattingEnabled = True
        Me.cboChild.Location = New System.Drawing.Point(388, 113)
        Me.cboChild.Name = "cboChild"
        Me.cboChild.Size = New System.Drawing.Size(196, 21)
        Me.cboChild.TabIndex = 231
        '
        'LblChild
        '
        Me.LblChild.BackColor = System.Drawing.Color.Transparent
        Me.LblChild.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblChild.Location = New System.Drawing.Point(329, 116)
        Me.LblChild.Name = "LblChild"
        Me.LblChild.Size = New System.Drawing.Size(53, 15)
        Me.LblChild.TabIndex = 230
        Me.LblChild.Text = "Child"
        '
        'objbtnSearchSpouse
        '
        Me.objbtnSearchSpouse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSpouse.BorderSelected = False
        Me.objbtnSearchSpouse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSpouse.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSpouse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSpouse.Location = New System.Drawing.Point(590, 86)
        Me.objbtnSearchSpouse.Name = "objbtnSearchSpouse"
        Me.objbtnSearchSpouse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSpouse.TabIndex = 229
        '
        'cboSpouse
        '
        Me.cboSpouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSpouse.DropDownWidth = 120
        Me.cboSpouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSpouse.FormattingEnabled = True
        Me.cboSpouse.Location = New System.Drawing.Point(388, 86)
        Me.cboSpouse.Name = "cboSpouse"
        Me.cboSpouse.Size = New System.Drawing.Size(196, 21)
        Me.cboSpouse.TabIndex = 228
        '
        'LblSpouse
        '
        Me.LblSpouse.BackColor = System.Drawing.Color.Transparent
        Me.LblSpouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSpouse.Location = New System.Drawing.Point(329, 89)
        Me.LblSpouse.Name = "LblSpouse"
        Me.LblSpouse.Size = New System.Drawing.Size(53, 15)
        Me.LblSpouse.TabIndex = 227
        Me.LblSpouse.Text = "Spouse"
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(337, 35)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(274, 17)
        Me.chkInactiveemp.TabIndex = 212
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objSpc2
        '
        Me.objSpc2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpc2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSpc2.IsSplitterFixed = True
        Me.objSpc2.Location = New System.Drawing.Point(75, 87)
        Me.objSpc2.Name = "objSpc2"
        Me.objSpc2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSpc2.Panel1
        '
        Me.objSpc2.Panel1.Controls.Add(Me.txtSearchAlloctation)
        Me.objSpc2.Panel1MinSize = 21
        '
        'objSpc2.Panel2
        '
        Me.objSpc2.Panel2.Controls.Add(Me.objchkAllocAll)
        Me.objSpc2.Panel2.Controls.Add(Me.lvAllocation)
        Me.objSpc2.Size = New System.Drawing.Size(248, 295)
        Me.objSpc2.SplitterDistance = 22
        Me.objSpc2.SplitterWidth = 1
        Me.objSpc2.TabIndex = 210
        '
        'txtSearchAlloctation
        '
        Me.txtSearchAlloctation.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSearchAlloctation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchAlloctation.Location = New System.Drawing.Point(0, 0)
        Me.txtSearchAlloctation.Name = "txtSearchAlloctation"
        Me.txtSearchAlloctation.Size = New System.Drawing.Size(248, 21)
        Me.txtSearchAlloctation.TabIndex = 66
        '
        'objchkAllocAll
        '
        Me.objchkAllocAll.AutoSize = True
        Me.objchkAllocAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAllocAll.Location = New System.Drawing.Point(6, 3)
        Me.objchkAllocAll.Name = "objchkAllocAll"
        Me.objchkAllocAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocAll.TabIndex = 208
        Me.objchkAllocAll.UseVisualStyleBackColor = True
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheckAllocAll, Me.objcolhAllocation})
        Me.lvAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(0, 0)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(248, 272)
        Me.lvAllocation.TabIndex = 64
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'objcolhCheckAllocAll
        '
        Me.objcolhCheckAllocAll.Tag = "objcolhCheckAllocAll"
        Me.objcolhCheckAllocAll.Text = ""
        Me.objcolhCheckAllocAll.Width = 25
        '
        'objcolhAllocation
        '
        Me.objcolhAllocation.Tag = "objcolhAllocation"
        Me.objcolhAllocation.Text = ""
        Me.objcolhAllocation.Width = 200
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(8, 63)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(61, 15)
        Me.lblAllocation.TabIndex = 209
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(75, 60)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(248, 21)
        Me.cboAllocations.TabIndex = 208
        '
        'objSpc1
        '
        Me.objSpc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objSpc1.IsSplitterFixed = True
        Me.objSpc1.Location = New System.Drawing.Point(329, 157)
        Me.objSpc1.Name = "objSpc1"
        Me.objSpc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSpc1.Panel1
        '
        Me.objSpc1.Panel1.Controls.Add(Me.txtSearch)
        Me.objSpc1.Panel1MinSize = 21
        '
        'objSpc1.Panel2
        '
        Me.objSpc1.Panel2.Controls.Add(Me.objchkAll)
        Me.objSpc1.Panel2.Controls.Add(Me.lvServiceProvider)
        Me.objSpc1.Size = New System.Drawing.Size(282, 225)
        Me.objSpc1.SplitterDistance = 22
        Me.objSpc1.SplitterWidth = 1
        Me.objSpc1.TabIndex = 206
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSearch.Location = New System.Drawing.Point(0, 0)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(282, 21)
        Me.txtSearch.TabIndex = 206
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(6, 3)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 207
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvServiceProvider
        '
        Me.lvServiceProvider.CheckBoxes = True
        Me.lvServiceProvider.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheckAll, Me.colhServiceProvider})
        Me.lvServiceProvider.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvServiceProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvServiceProvider.FullRowSelect = True
        Me.lvServiceProvider.Location = New System.Drawing.Point(0, 0)
        Me.lvServiceProvider.Name = "lvServiceProvider"
        Me.lvServiceProvider.Size = New System.Drawing.Size(282, 202)
        Me.lvServiceProvider.TabIndex = 205
        Me.lvServiceProvider.UseCompatibleStateImageBehavior = False
        Me.lvServiceProvider.View = System.Windows.Forms.View.Details
        '
        'objcolhCheckAll
        '
        Me.objcolhCheckAll.Tag = "objcolhCheckAll"
        Me.objcolhCheckAll.Text = ""
        Me.objcolhCheckAll.Width = 25
        '
        'colhServiceProvider
        '
        Me.colhServiceProvider.Tag = "colhServiceProvider"
        Me.colhServiceProvider.Text = "Service Provider"
        Me.colhServiceProvider.Width = 200
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(75, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(248, 21)
        Me.cboPeriod.TabIndex = 201
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(61, 15)
        Me.lblPeriod.TabIndex = 202
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEmpMedicalCover_Dtetail_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 623)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmEmpMedicalCover_Dtetail_Report"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.objSpc2.Panel1.ResumeLayout(False)
        Me.objSpc2.Panel1.PerformLayout()
        Me.objSpc2.Panel2.ResumeLayout(False)
        Me.objSpc2.Panel2.PerformLayout()
        Me.objSpc2.ResumeLayout(False)
        Me.objSpc1.Panel1.ResumeLayout(False)
        Me.objSpc1.Panel1.PerformLayout()
        Me.objSpc1.Panel2.ResumeLayout(False)
        Me.objSpc1.Panel2.PerformLayout()
        Me.objSpc1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lvServiceProvider As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhServiceProvider As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objSpc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents objSpc2 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtSearchAlloctation As System.Windows.Forms.TextBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents objcolhCheckAllocAll As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAllocAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchChild As eZee.Common.eZeeGradientButton
    Public WithEvents cboChild As System.Windows.Forms.ComboBox
    Private WithEvents LblChild As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSpouse As eZee.Common.eZeeGradientButton
    Public WithEvents cboSpouse As System.Windows.Forms.ComboBox
    Private WithEvents LblSpouse As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents objelSep1 As eZee.Common.eZeeLine
End Class

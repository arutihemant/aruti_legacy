'************************************************************************************************************************************
'Class Name : clsSickSheetReport.vb
'Purpose    :
'Date       :16/01/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsSickSheetReport

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsSickSheetReport"
    Private mstrsicksheetUnkid As String = ""
    Private mstrsicksheetNo As String = String.Empty
    Private mstrEmployeeUnkid As String = ""
    Private mstrEmployeeName As String = ""
    Private mstrEmployeeCode As String = ""
    Private mintJobunkid As Integer = -1
    Private mstrJobTitle As String = ""
    Private mintMedicalCoverunkid As Integer = -1
    Private mstrMedicalCover As String = ""
    Private mintProviderunkid As Integer = -1
    Private mstrProviderName As String = ""
    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass 'Sohail (11 Apr 2012)
    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Sohail (23 Apr 2012) -- End


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Private mdtSicksheetDate As Date = Nothing
    'Pinkal (21-Jul-2014) -- End


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Private mintSickSheetTemplate As Integer = 1
    'Pinkal (12-Sep-2014) -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _SickSheetunkid() As String
        Set(ByVal value As String)
            mstrsicksheetUnkid = value
        End Set
    End Property

    Public WriteOnly Property _SickSheetNo() As String
        Set(ByVal value As String)
            mstrsicksheetNo = value
        End Set
    End Property

    Public WriteOnly Property _Employeeunkid() As String
        Set(ByVal value As String)
            mstrEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeCode() As String
        Set(ByVal value As String)
            mstrEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _Jobunkid() As Integer
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    Public WriteOnly Property _JobTitle() As String
        Set(ByVal value As String)
            mstrJobTitle = value
        End Set
    End Property

    Public WriteOnly Property _MedicalCoverUnkid() As Integer
        Set(ByVal value As Integer)
            mintMedicalCoverunkid = value
        End Set
    End Property

    Public WriteOnly Property _MedicalCover() As String
        Set(ByVal value As String)
            mstrMedicalCover = value
        End Set
    End Property

    Public WriteOnly Property _Providerunkid() As Integer
        Set(ByVal value As Integer)
            mintProviderunkid = value
        End Set
    End Property

    Public WriteOnly Property _ProviderName() As String
        Set(ByVal value As String)
            mstrProviderName = value
        End Set
    End Property

    'Sohail (11 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property
    'Sohail (11 Apr 2012) -- End

    'Sohail (23 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'Sohail (23 Apr 2012) -- End


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Public WriteOnly Property _SicksheetDate() As Date
        Set(ByVal value As Date)
            mdtSicksheetDate = value
        End Set
    End Property
    'Pinkal (21-Jul-2014) -- End


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    Public WriteOnly Property _SicksheetTemplate() As Integer
        Set(ByVal value As Integer)
            mintSickSheetTemplate = value
        End Set
    End Property
    'Pinkal (12-Sep-2014) -- End


#End Region

#Region " Private Methods "


    'Pinkal (12-Sep-2014) -- Start
    'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

    'Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim StrQFilter As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim mstrEmpName As String = ""
    '    Dim mstrTitle As String = ""
    '    Try

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()


    '        'Anjan (17 Apr 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        'StrQ = " SELECT hremployee_master.employeeunkid  " & _
    '        '            ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
    '        '            ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee  " & _
    '        '            ", mdmedical_sicksheet.sicksheetunkid  " & _
    '        '            ", mdmedical_sicksheet.sicksheetno  " & _
    '        '            ", mdmedical_sicksheet.sicksheetdate " & _
    '        '            ", hrinstitute_master.instituteunkid  " & _
    '        '            ", hrinstitute_master.institute_name " & _
    '        '            ", ISNULL(hrinstitute_master.institute_email,'') institute_email " & _
    '        '            ", hrjob_master.job_name AS job  " & _
    '        '            ", mdmedical_master.mdmastername AS cover " & _
    '        '            ", ISNULL(emp.dependent_Id,0)  dependent_Id " & _
    '        '            ", ISNULL(emp.dependants,'') dependants " & _
    '        '            ", ISNULL(emp.age,0) AS age  " & _
    '        '            ", ISNULL(emp.relation,'') AS relation " & _
    '        '            ", ISNULL(emp.gender,'') AS gender " & _
    '        '            ", ISNULL(hrmsConfiguration..cfuser_master.username,'') username " & _
    '        '            " FROM mdmedical_sicksheet " & _
    '        '            " JOIN hremployee_master ON hremployee_master.employeeunkid = mdmedical_sicksheet.employeeunkid " & _
    '        '            " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdmedical_sicksheet.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
    '        '            " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid = mdmedical_sicksheet.instituteunkid AND (mdprovider_mapping.userunkid =" & User._Object._Userunkid & " OR mdprovider_mapping.userunkid = 1) " & _
    '        '            " JOIN mdmedical_master ON mdmedical_master.mdmasterunkid = mdmedical_sicksheet.medicalcoverunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & _
    '        '            " JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '        '            " JOIN hrmsConfiguration..cfuser_master ON  hrmsConfiguration..cfuser_master.userunkid = mdmedical_sicksheet.userunkid " & _
    '        '            " LEFT JOIN ( SELECT  employeeunkid  " & _
    '        '                                ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS dependent_Id  " & _
    '        '                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') dependants " & _
    '        '                                ",YEAR(GETDATE()) - YEAR(birthdate) as age " & _
    '        '                                ",cfcommon_master.name AS relation " & _
    '        '                                ",CASE WHEN gender = 1 then @male " & _
    '        '                                "          WHEN gender = 2 then @female " & _
    '        '                                "		   WHEN gender = 3 then @other " & _
    '        '                                " ELSE  ''  END as gender " & _
    '        '                                " FROM hrdependants_beneficiaries_tran " & _
    '        '                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype =  " & clsCommon_Master.enCommonMaster.RELATIONS & _
    '        '                                " WHERE  ((YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) ) <= ISNULL(cfcommon_master.dependant_limit,0) OR ISNULL(cfcommon_master.dependant_limit,0) = 0)" & _
    '        '                                " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
    '        '                            ") AS emp ON emp.employeeunkid = mdmedical_sicksheet.employeeunkid " & _
    '        '            " WHERE mdmedical_sicksheet.sicksheetunkid IN (" & mstrsicksheetUnkid & ") 
    'StrQ = " SELECT hremployee_master.employeeunkid  " & _
    '            ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
    '            ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee  " & _
    '            ", mdmedical_sicksheet.sicksheetunkid  " & _
    '            ", mdmedical_sicksheet.sicksheetno  " & _
    '            ", mdmedical_sicksheet.sicksheetdate " & _
    '            ", hrinstitute_master.instituteunkid  " & _
    '            ", hrinstitute_master.institute_name " & _
    '            ", ISNULL(hrinstitute_master.institute_email,'') institute_email " & _
    '            ", hrjob_master.job_name AS job  " & _
    '            ", mdmedical_master.mdmastername AS cover " & _
    '            ", ISNULL(emp.dependent_Id,0)  dependent_Id " & _
    '            ", ISNULL(emp.dependants,'') dependants " & _
    '            ", ISNULL(emp.age,0) AS age  " & _
    '            ", ISNULL(emp.relation,'') AS relation " & _
    '            ", ISNULL(emp.gender,'') AS gender " & _
    '                   ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') +' '+ ISNULL(hrmsConfiguration..cfuser_master.lastname,'') username " & _
    '                   ", ISNULL(cfcommon_master.name,'') as Title " & _
    '                   ", ISNULL(mdmedical_sicksheet.dependantunkid,'') AS dependantunkids " & _
    '            " FROM mdmedical_sicksheet " & _
    '            " JOIN hremployee_master ON hremployee_master.employeeunkid = mdmedical_sicksheet.employeeunkid " & _
    '            " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdmedical_sicksheet.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
    '                   " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid = mdmedical_sicksheet.instituteunkid "



    '        'Pinkal (19-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'AND (mdprovider_mapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " OR mdprovider_mapping.userunkid = 1) " & _

    '        If mintUserUnkid <= 0 Then
    '            StrQ &= " AND mdprovider_mapping.userunkid =" & User._Object._Userunkid
    '        Else
    '            StrQ &= " AND mdprovider_mapping.userunkid =" & mintUserUnkid
    '        End If

    '        'Pinkal (19-Nov-2012) -- End


    '        StrQ &= " JOIN mdmedical_master ON mdmedical_master.mdmasterunkid = mdmedical_sicksheet.medicalcoverunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & _
    '            " JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '            " JOIN hrmsConfiguration..cfuser_master ON  hrmsConfiguration..cfuser_master.userunkid = mdmedical_sicksheet.userunkid " & _
    '                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype =  " & clsCommon_Master.enCommonMaster.TITLE & _
    '            " LEFT JOIN ( SELECT  employeeunkid  " & _
    '                                ", hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS dependent_Id  " & _
    '                                       ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') dependants " & _
    '                                ",YEAR(GETDATE()) - YEAR(birthdate) as age " & _
    '                                ",cfcommon_master.name AS relation " & _
    '                                ",CASE WHEN gender = 1 then @male " & _
    '                                "          WHEN gender = 2 then @female " & _
    '                                "		   WHEN gender = 3 then @other " & _
    '                                " ELSE  ''  END as gender " & _
    '                                " FROM hrdependants_beneficiaries_tran " & _
    '                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype =  " & clsCommon_Master.enCommonMaster.RELATIONS & _
    '                                " WHERE  ((YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) ) <= ISNULL(cfcommon_master.dependant_limit,0) OR ISNULL(cfcommon_master.dependant_limit,0) = 0)" & _
    '                                       " AND hrdependants_beneficiaries_tran.isvoid = 0 "


    '        'Pinkal (06-Mar-2013) -- Start
    '        'Enhancement : TRA Changes

    '        StrQ &= " AND cfcommon_master.ismedicalbenefit = 1 " & _
    '                     " UNION " & _
    '                     "  SELECT " & _
    '                     " mddependant_exception.employeeunkid " & _
    '                     ", mddependant_exception.dependantunkid AS dependent_Id  " & _
    '                     ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
    '                     ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
    '                     ", ISNULL(cfcommon_master.name, '') AS relation " & _
    '                     ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
    '                     "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
    '                     "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
    '                     " ELSE  ''  " & _
    '                     " END as gender " & _
    '                     " FROM  mddependant_exception " & _
    '                     " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
    '                     " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
    '                     " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
    '                     " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
    '                     " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
    '                     " WHERE mddependant_exception.isvoid = 0 "

    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '        StrQ &= " AND mddependant_exception.ismedical = 1 AND (mddependant_exception.mdstopdate IS NULL OR  Convert(Char(8),mddependant_exception.mdstopdate,112) >= @sicksheetdate)"
    '        'Pinkal (21-Jul-2014) -- End


    '        StrQ &= ") AS emp ON emp.employeeunkid = mdmedical_sicksheet.employeeunkid " & _
    '                   " WHERE mdmedical_sicksheet.sicksheetunkid IN (" & mstrsicksheetUnkid & ") "

    '        'Pinkal (06-Mar-2013) -- End



    '        'Pinkal (21-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    '        objDataOperation.AddParameter("@sicksheetdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtSicksheetDate))
    '        'Pinkal (21-Jul-2014) -- End


    '        objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Male"))
    '        objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "Female"))
    '        objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Other"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '            Dim i As Integer = 1
    '            For Each dtRow As DataRow In dsList.Tables(0).Rows
    '                Dim rpt_Row As DataRow
    '                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '                rpt_Row.Item("Column1") = dtRow("sicksheetno").ToString()
    '                rpt_Row.Item("Column2") = dtRow("institute_name").ToString()
    '                rpt_Row.Item("Column3") = dtRow("employee").ToString()
    '                rpt_Row.Item("Column4") = dtRow("job").ToString()
    '                rpt_Row.Item("Column5") = dtRow("cover").ToString()
    '                rpt_Row.Item("Column8") = dtRow("employeecode").ToString()
    '                rpt_Row.Item("Column12") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString
    '                rpt_Row.Item("Column13") = dtRow("username").ToString()
    '                rpt_Row.Item("Column14") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString
    '                If dtRow("dependants").ToString() <> "" Then
    '                    rpt_Row.Item("Column6") = i & "."
    '                    rpt_Row.Item("Column7") = dtRow("dependants").ToString()
    '                    rpt_Row.Item("Column9") = dtRow("age").ToString()
    '                    rpt_Row.Item("Column10") = dtRow("gender").ToString()
    '                    rpt_Row.Item("Column11") = dtRow("relation").ToString()
    '                    i += 1
    '                End If


    '                'Anjan (17 Apr 2013)-Start
    '                'ENHANCEMENT : TRA COMMENTS on Rutta's Request

    '                'Anjan (17 Apr 2012)-Start
    '                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '                'mstrTitle = dtRow("Title").ToString
    '                'mstrEmpName = dtRow("employee").ToString()
    '                mstrTitle = ""
    '                mstrEmpName = ""
    '                'Anjan (17 Apr 2012)-End 
    '                'Anjan (17 Apr 2013)-End


    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next
    '        End If


    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        Dim objImg(0) As Object
    '        If Company._Object._Image IsNot Nothing Then
    '            objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
    '        Else
    '            Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
    '            objImg(0) = eZeeDataType.image2Data(imgBlank)
    '        End If
    '        arrImageRow.Item("arutiLogo") = objImg(0)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If


    '        'Pinkal (12-Sep-2014) -- Start
    '        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    '        If mintSickSheetTemplate = 1 Then  'TRA
    '            objRpt = New ArutiReport.Designer.rptEmployeeSickSheet
    '        ElseIf mintSickSheetTemplate = 2 Then  'TANAPA
    '            objRpt = New ArutiReport.Designer.rpt_TANAPA_EmployeeSickSheet
    '        End If
    '        'Pinkal (12-Sep-2014) -- End


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtCompany", Company._Object._Name)

    '        'Pinkal (22-Mar-2012) -- Start
    '        'Enhancement : TRA Changes

    '        Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)

    '        Dim mstrAddress As String = String.Empty

    '        If Company._Object._Address2.ToString.Trim <> "" Then
    '            mstrAddress = Company._Object._Address2.ToString.Trim
    '        End If

    '        If Company._Object._Country_Name.ToString.Trim <> "" Then
    '            mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
    '        End If

    '        If Company._Object._State_Name.ToString.Trim <> "" Then
    '            mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
    '        End If

    '        If Company._Object._City_Name.ToString.Trim <> "" Then
    '            mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
    '        End If

    '        If Company._Object._Post_Code_No.ToString.Trim <> "" Then
    '            mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
    '        End If


    '        Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)


    '        Call ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 4, "No :"))

    '        If mintSickSheetTemplate = 1 Then 'TRA
    '            Call ReportFunction.TextChange(objRpt, "txtSickSheet", Language.getMessage(mstrModuleName, 5, "MEDICAL SICK SHEET"))
    '            Call ReportFunction.TextChange(objRpt, "txtHospital", Language.getMessage(mstrModuleName, 6, "To: Medical Officer in Charge/ Hospital Name :"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", mstrTitle.ToString & " : ")
    '            Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 8, "Position :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMedicalCover", Language.getMessage(mstrModuleName, 9, "Has been brought to you for medical treatment and is entitled to medical cover level :"))
    '            Call ReportFunction.TextChange(objRpt, "txtAttend", Language.getMessage(mstrModuleName, 10, "Please attend ") & mstrTitle.ToString & " " & mstrEmpName.ToString & ".")
    '            Call ReportFunction.TextChange(objRpt, "txtHospitalDate", Language.getMessage(mstrModuleName, 11, "Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtHRofficerName", Language.getMessage(mstrModuleName, 12, "(Name of HR officer issuing the SS)"))
    '            Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 13, "To: Commissioner General,") & Company._Object._Name)
    '            Call ReportFunction.TextChange(objRpt, "txtDeclareName", Language.getMessage(mstrModuleName, 14, "I hereby declare that ") & mstrTitle.ToString & " :")
    '            Call ReportFunction.TextChange(objRpt, "txtDeclareName1", Language.getMessage(mstrModuleName, 14, "I hereby declare that ") & mstrTitle.ToString & " :")
    '            Call ReportFunction.TextChange(objRpt, "txttreated", Language.getMessage(mstrModuleName, 15, "Has been treated/attended accordingly. I do suggest that ") & mstrTitle.ToString & " " & mstrEmpName.ToString & Language.getMessage(mstrModuleName, 37, " Should continue with work/take a rest for "))
    '            Call ReportFunction.TextChange(objRpt, "txtWork", Language.getMessage(mstrModuleName, 18, "Can now continue work normally"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmpName2", mstrEmpName)
    '            Call ReportFunction.TextChange(objRpt, "txtEmpName3", mstrEmpName)
    '            Call ReportFunction.TextChange(objRpt, "txttreatment", Language.getMessage(mstrModuleName, 16, "days/ be admitted at the hospital for further treatments*."))
    '            Call ReportFunction.TextChange(objRpt, "txtTRADate", Language.getMessage(mstrModuleName, 11, "Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMedicalOfficeName", Language.getMessage(mstrModuleName, 17, "(Name and Signature of medical officer)"))
    '            Call ReportFunction.TextChange(objRpt, "txtDate3", Language.getMessage(mstrModuleName, 11, "Date :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMedicalOfficeName1", Language.getMessage(mstrModuleName, 17, "(Name and Signature of medical officer)"))
    '            Call ReportFunction.TextChange(objRpt, "txtMedicalService", Language.getMessage(mstrModuleName, 19, "Please note that the following List of Dependants are qualified for medical services under this employee’s cover :"))
    '            Call ReportFunction.TextChange(objRpt, "txtOriginalCopy", Language.getMessage(mstrModuleName, 20, "Orignal Copy must be attached with Hospital Invoices."))
    '            Call ReportFunction.TextChange(objRpt, "txtGreenCopy", Language.getMessage(mstrModuleName, 21, "Copy for Hospital records."))
    '            Call ReportFunction.TextChange(objRpt, "txtYellowCopy", Language.getMessage(mstrModuleName, 22, "Copy for ") & Company._Object._Code & Language.getMessage(mstrModuleName, 36, " HR department."))
    '            Call ReportFunction.TextChange(objRpt, "txtPayrollNo", Language.getMessage(mstrModuleName, 23, "Payroll No :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 24, "Printed Date :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 25, "Page :"))
    '            Call ReportFunction.TextChange(objRpt, "txtBranchName", Language.getMessage(mstrModuleName, 26, "Branch Name :"))
    '            Call ReportFunction.TextChange(objRpt, "txtDepedentName", Language.getMessage(mstrModuleName, 27, "Dependent Name"))
    '            Call ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 28, "Age"))
    '            Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 29, "Gender"))
    '            Call ReportFunction.TextChange(objRpt, "txtRelation", Language.getMessage(mstrModuleName, 30, "Relation"))
    '            Call ReportFunction.TextChange(objRpt, "txtValidSSInfo", Language.getMessage(mstrModuleName, 31, "This Sick Sheet is valid from date above to the last date of the month."))
    '            Call ReportFunction.TextChange(objRpt, "txtSickSheetDate", Language.getMessage(mstrModuleName, 32, "Sick Sheet Date :"))

    '        ElseIf mintSickSheetTemplate = 2 Then 'TANAPA


    '        End If

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", ConfigParameter._Object._CurrentDateAndTime)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function


    Public Function Generate_DetailReport(ByVal mdtEmployeeAsonDate As Date) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim mstrEmpName As String = ""
        Dim mstrTitle As String = ""
        Try



            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ' No need to add new active employee condition as employee and sicksheetunkid is passed from sick sheet list screen.

            'Anjan [08 September 2015] -- End


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            StrQ = " SELECT hremployee_master.employeeunkid  " & _
                       ", ISNULL(hremployee_master.employeecode, '') employeecode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') employee  " & _
                       ", mdmedical_sicksheet.sicksheetunkid  " & _
                       ", mdmedical_sicksheet.sicksheetno  " & _
                       ", mdmedical_sicksheet.sicksheetdate " & _
                       ", hrinstitute_master.instituteunkid  " & _
                       ", hrinstitute_master.institute_name " & _
                       ", ISNULL(hrinstitute_master.institute_email,'') institute_email " & _
                       ", hrjob_master.job_name AS job  " & _
                       ", hrdepartment_master.name AS department  " & _
                       ", mdmedical_master.mdmastername AS cover " & _
                       ", ISNULL(mdmedical_sicksheet.dependantunkid,'') AS dependantunkids " & _
                       ", ISNULL(hrmsConfiguration..cfuser_master.firstname,'') +' '+ ISNULL(hrmsConfiguration..cfuser_master.lastname,'') username " & _
                       " FROM mdmedical_sicksheet " & _
                       " JOIN hremployee_master ON hremployee_master.employeeunkid = mdmedical_sicksheet.employeeunkid " & _
                       " JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = mdmedical_sicksheet.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
                       " LEFT JOIN mdprovider_mapping ON mdprovider_mapping.providerunkid = mdmedical_sicksheet.instituteunkid "


            If mintUserUnkid <= 0 Then
                StrQ &= " AND mdprovider_mapping.userunkid =" & User._Object._Userunkid
            Else
                StrQ &= " AND mdprovider_mapping.userunkid =" & mintUserUnkid
            End If


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.

            'StrQ &= " JOIN mdmedical_master ON mdmedical_master.mdmasterunkid = mdmedical_sicksheet.medicalcoverunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & _
            '           " JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '           " JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
            '           " JOIN hrmsConfiguration..cfuser_master ON  hrmsConfiguration..cfuser_master.userunkid = mdmedical_sicksheet.userunkid " & _
            '           " WHERE mdmedical_sicksheet.sicksheetunkid IN (" & mstrsicksheetUnkid & ") AND mdmedical_sicksheet.isvoid =  0 "

            StrQ &= " JOIN mdmedical_master ON mdmedical_master.mdmasterunkid = mdmedical_sicksheet.medicalcoverunkid AND mdmedical_master.mdmastertype_id = " & enMedicalMasterType.Cover & _
                         "   LEFT JOIN " & _
                         "   ( " & _
                         "    SELECT " & _
                         "         jobunkid " & _
                         "        ,jobgroupunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                         "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                         "    JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                         "    LEFT JOIN " & _
                         "    ( " & _
                         "      SELECT " & _
                         "         departmentunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM hremployee_transfer_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         "   JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                       " JOIN hrmsConfiguration..cfuser_master ON  hrmsConfiguration..cfuser_master.userunkid = mdmedical_sicksheet.userunkid " & _
                       " WHERE mdmedical_sicksheet.sicksheetunkid IN (" & mstrsicksheetUnkid & ") AND mdmedical_sicksheet.isvoid =  0 "

            'Pinkal (16-Apr-2016) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim i As Integer = 1
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    If dtRow("dependantunkids").ToString().Trim <> "" Then
                        Dim arDependents() As String = dtRow("dependantunkids").ToString().Split(",")
                        For J As Integer = 0 To arDependents.Length - 1
                            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                            rpt_Row.Item("Column1") = dtRow("sicksheetno").ToString()
                            rpt_Row.Item("Column2") = dtRow("institute_name").ToString()
                            rpt_Row.Item("Column3") = dtRow("employee").ToString()


                            'Pinkal (25-Nov-2014) -- Start
                            'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
                            If mintSickSheetTemplate = 1 OrElse mintSickSheetTemplate = 3 Then 'TRA OR VFT
                                rpt_Row.Item("Column4") = dtRow("job").ToString()
                            ElseIf mintSickSheetTemplate = 2 Then 'TANAPA
                                rpt_Row.Item("Column4") = ""
                            End If
                            'Pinkal (25-Nov-2014) -- End

                            rpt_Row.Item("Column5") = dtRow("cover").ToString()
                            rpt_Row.Item("Column8") = dtRow("employeecode").ToString()
                            rpt_Row.Item("Column12") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString
                            rpt_Row.Item("Column13") = dtRow("username").ToString()
                            rpt_Row.Item("Column14") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString

                            Dim objCommonMst As New clsCommon_Master
                            Dim objDependent As New clsDependants_Beneficiary_tran
                            objDependent._Dpndtbeneficetranunkid = CInt(arDependents(J))
                            rpt_Row.Item("Column6") = i & "."
                            rpt_Row.Item("Column7") = objDependent._First_Name & " " & objDependent._Middle_Name & " " & objDependent._Last_Name
                            If objDependent._Birthdate <> Nothing Then
                                rpt_Row.Item("Column9") = IIf(IsDBNull(DateDiff(DateInterval.Year, objDependent._Birthdate, ConfigParameter._Object._CurrentDateAndTime.Date)), "", DateDiff(DateInterval.Year, objDependent._Birthdate, ConfigParameter._Object._CurrentDateAndTime.Date))
                            Else
                                rpt_Row.Item("Column9") = 0
                            End If

                            If objDependent._Gender = 1 Then
                                rpt_Row.Item("Column10") = Language.getMessage(mstrModuleName, 33, "Male")
                            ElseIf objDependent._Gender = 2 Then
                                rpt_Row.Item("Column10") = Language.getMessage(mstrModuleName, 34, "Female")
                            ElseIf objDependent._Gender = 3 Then
                                rpt_Row.Item("Column10") = Language.getMessage(mstrModuleName, 35, "Other")
                            Else
                                rpt_Row.Item("Column10") = ""
                            End If
                            If objDependent._Relationunkid > 0 Then
                                objCommonMst._Masterunkid = objDependent._Relationunkid
                                rpt_Row.Item("Column11") = objCommonMst._Name
                            Else
                                rpt_Row.Item("Column11") = ""
                            End If
                            i += 1
                            rpt_Row.Item("Column15") = ""

                            'Pinkal (25-Nov-2014) -- Start
                            'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
                            rpt_Row.Item("Column16") = dtRow("department").ToString()
                            'Pinkal (25-Nov-2014) -- End
                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                        Next
                    Else
                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Row.Item("Column1") = dtRow("sicksheetno").ToString()
                        rpt_Row.Item("Column2") = dtRow("institute_name").ToString()

                        If mintSickSheetTemplate = 1 Then 'TRA
                            rpt_Row.Item("Column3") = dtRow("employee").ToString()
                        ElseIf mintSickSheetTemplate = 2 Then 'TANAPA
                            rpt_Row.Item("Column7") = dtRow("employee").ToString()
                            rpt_Row.Item("Column15") = dtRow("employee").ToString()
                            rpt_Row.Item("Column3") = ""

                            'Pinkal (25-Nov-2014) -- Start
                            'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
                        ElseIf mintSickSheetTemplate = 3 Then 'VFT
                            rpt_Row.Item("Column7") = dtRow("employee").ToString()
                            'Pinkal (25-Nov-2014) -- End
                        End If

                        rpt_Row.Item("Column4") = dtRow("job").ToString()
                        rpt_Row.Item("Column5") = dtRow("cover").ToString()
                        rpt_Row.Item("Column8") = dtRow("employeecode").ToString()
                        rpt_Row.Item("Column12") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString
                        rpt_Row.Item("Column13") = dtRow("username").ToString()
                        rpt_Row.Item("Column14") = CDate(dtRow("sicksheetdate").ToString()).ToShortDateString

                        'Pinkal (25-Nov-2014) -- Start
                        'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
                        rpt_Row.Item("Column16") = dtRow("department").ToString()
                        'Pinkal (25-Nov-2014) -- End


                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                    End If
                Next
            End If


            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            Dim objImg(0) As Object
            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If
            arrImageRow.Item("arutiLogo") = objImg(0)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If mintSickSheetTemplate = 1 Then  'TRA
                objRpt = New ArutiReport.Designer.rptEmployeeSickSheet
            ElseIf mintSickSheetTemplate = 2 Then  'TANAPA
                objRpt = New ArutiReport.Designer.rpt_TANAPA_EmployeeSickSheet

                'Pinkal (25-Nov-2014) -- Start
                'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
            ElseIf mintSickSheetTemplate = 3 Then  'VFT
                objRpt = New ArutiReport.Designer.rpt_VFT_EmployeeSickSheet
                'Pinkal (25-Nov-2014) -- End

            End If
            'Pinkal (12-Sep-2014) -- End


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompany", Company._Object._Name)

            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes

            Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1)

            Dim mstrAddress As String = String.Empty

            If Company._Object._Address2.ToString.Trim <> "" Then
                mstrAddress = Company._Object._Address2.ToString.Trim
            End If


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If Company._Object._Post_Code_No.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Post_Code_No.ToString.Trim
            End If

            If Company._Object._City_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._City_Name.ToString.Trim
            End If

            If Company._Object._State_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._State_Name.ToString.Trim
            End If

            If Company._Object._Country_Name.ToString.Trim <> "" Then
                mstrAddress &= " , " & Company._Object._Country_Name.ToString.Trim
            End If

            'Pinkal (12-Sep-2014) -- End


            Call ReportFunction.TextChange(objRpt, "txtAddress2", mstrAddress)


            'Pinkal (25-Nov-2014) -- Start
            'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
            If mintSickSheetTemplate <> 3 Then
                Call ReportFunction.TextChange(objRpt, "txtNo", Language.getMessage(mstrModuleName, 4, "No :"))
            End If
            'Pinkal (25-Nov-2014) -- End




            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            If mintSickSheetTemplate = 1 Then  'TRA
                Call ReportFunction.TextChange(objRpt, "txtSickSheet", Language.getMessage(mstrModuleName, 5, "MEDICAL SICK SHEET"))
            ElseIf mintSickSheetTemplate = 2 Then  'TRA
                Call ReportFunction.TextChange(objRpt, "txtSickSheet", Language.getMessage(mstrModuleName, 38, "SICK SHEET FORM"))
            End If
            'Pinkal (12-Sep-2014) -- End



            If mintSickSheetTemplate = 1 Then 'TRA
                Call ReportFunction.TextChange(objRpt, "txtHospital", Language.getMessage(mstrModuleName, 6, "To: Medical Officer in Charge/ Hospital Name :"))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeName", mstrTitle.ToString & " : ")
                Call ReportFunction.TextChange(objRpt, "txtPosition", Language.getMessage(mstrModuleName, 8, "Position :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalCover", Language.getMessage(mstrModuleName, 9, "Has been brought to you for medical treatment and is entitled to medical cover level :"))
                Call ReportFunction.TextChange(objRpt, "txtAttend", Language.getMessage(mstrModuleName, 10, "Please attend ") & mstrTitle.ToString & " " & mstrEmpName.ToString & ".")
                Call ReportFunction.TextChange(objRpt, "txtHospitalDate", Language.getMessage(mstrModuleName, 11, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtHRofficerName", Language.getMessage(mstrModuleName, 12, "(Name of HR officer issuing the SS)"))
                Call ReportFunction.TextChange(objRpt, "txtTRA", Language.getMessage(mstrModuleName, 13, "To: Commissioner General,") & Company._Object._Name)
                Call ReportFunction.TextChange(objRpt, "txtDeclareName", Language.getMessage(mstrModuleName, 14, "I hereby declare that ") & mstrTitle.ToString & " :")
                Call ReportFunction.TextChange(objRpt, "txtDeclareName1", Language.getMessage(mstrModuleName, 14, "I hereby declare that ") & mstrTitle.ToString & " :")
                Call ReportFunction.TextChange(objRpt, "txttreated", Language.getMessage(mstrModuleName, 15, "Has been treated/attended accordingly. I do suggest that ") & mstrTitle.ToString & " " & mstrEmpName.ToString & Language.getMessage(mstrModuleName, 37, " Should continue with work/take a rest for "))
                Call ReportFunction.TextChange(objRpt, "txtWork", Language.getMessage(mstrModuleName, 18, "Can now continue work normally"))
                Call ReportFunction.TextChange(objRpt, "txtEmpName2", mstrEmpName)
                Call ReportFunction.TextChange(objRpt, "txtEmpName3", mstrEmpName)
                Call ReportFunction.TextChange(objRpt, "txttreatment", Language.getMessage(mstrModuleName, 16, "days/ be admitted at the hospital for further treatments*."))
                Call ReportFunction.TextChange(objRpt, "txtTRADate", Language.getMessage(mstrModuleName, 11, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficeName", Language.getMessage(mstrModuleName, 17, "(Name and Signature of medical officer)"))
                Call ReportFunction.TextChange(objRpt, "txtDate3", Language.getMessage(mstrModuleName, 11, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficeName1", Language.getMessage(mstrModuleName, 17, "(Name and Signature of medical officer)"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalService", Language.getMessage(mstrModuleName, 19, "Please note that the following List of Dependants are qualified for medical services under this employee’s cover :"))
                Call ReportFunction.TextChange(objRpt, "txtOriginalCopy", Language.getMessage(mstrModuleName, 20, "Orignal Copy must be attached with Hospital Invoices."))
                Call ReportFunction.TextChange(objRpt, "txtGreenCopy", Language.getMessage(mstrModuleName, 21, "Copy for Hospital records."))
                Call ReportFunction.TextChange(objRpt, "txtYellowCopy", Language.getMessage(mstrModuleName, 22, "Copy for ") & Company._Object._Code & Language.getMessage(mstrModuleName, 36, " HR department."))
                Call ReportFunction.TextChange(objRpt, "txtPayrollNo", Language.getMessage(mstrModuleName, 23, "Payroll No :"))
                Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 24, "Printed Date :"))
                Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 25, "Page :"))
                Call ReportFunction.TextChange(objRpt, "txtBranchName", Language.getMessage(mstrModuleName, 26, "Branch Name :"))
                Call ReportFunction.TextChange(objRpt, "txtDepedentName", Language.getMessage(mstrModuleName, 27, "Dependent Name"))
                Call ReportFunction.TextChange(objRpt, "txtAge", Language.getMessage(mstrModuleName, 28, "Age"))
                Call ReportFunction.TextChange(objRpt, "txtGender", Language.getMessage(mstrModuleName, 29, "Gender"))
                Call ReportFunction.TextChange(objRpt, "txtRelation", Language.getMessage(mstrModuleName, 30, "Relation"))
                Call ReportFunction.TextChange(objRpt, "txtValidSSInfo", Language.getMessage(mstrModuleName, 31, "This Sick Sheet is valid from date above to the last date of the month."))
                Call ReportFunction.TextChange(objRpt, "txtSickSheetDate", Language.getMessage(mstrModuleName, 32, "Sick Sheet Date :"))

            ElseIf mintSickSheetTemplate = 2 Then 'TANAPA
                Call ReportFunction.TextChange(objRpt, "txtOfficeInCharge", Language.getMessage(mstrModuleName, 39, "The Medical Officer in Charge"))
                Call ReportFunction.TextChange(objRpt, "txtRuralHealthCenter", Language.getMessage(mstrModuleName, 40, "Hospital / Clinic / Dispensary / Rural Health Center."))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeTitle", Language.getMessage(mstrModuleName, 41, "Mr. / Mrs / Dr / Miss / MS"))
                Call ReportFunction.TextChange(objRpt, "txtDesignation", Language.getMessage(mstrModuleName, 42, "Designation"))
                Call ReportFunction.TextChange(objRpt, "txtHospitalCardNo", Language.getMessage(mstrModuleName, 43, "Hospital Card No"))
                Call ReportFunction.TextChange(objRpt, "txtIsSentto", Language.getMessage(mstrModuleName, 44, "is sent to"))
                Call ReportFunction.TextChange(objRpt, "txtExpenseTANAPA", Language.getMessage(mstrModuleName, 45, "your for treatment,  on the expense of TANAPA."))
                Call ReportFunction.TextChange(objRpt, "txtwifeHusband", Language.getMessage(mstrModuleName, 46, "Mr. / Miss / Mrs. / He / She is a Son / Daughter , Wife / Husband of "))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeTitle1", Language.getMessage(mstrModuleName, 47, "Mr. / Mrs /  Miss / MS"))
                Call ReportFunction.TextChange(objRpt, "txtSignStamp", Language.getMessage(mstrModuleName, 48, "Signature and Stamp"))
                Call ReportFunction.TextChange(objRpt, "txtSignStampDate", Language.getMessage(mstrModuleName, 49, "Date"))
                Call ReportFunction.TextChange(objRpt, "txtSignStampTime", Language.getMessage(mstrModuleName, 50, "Time"))
                Call ReportFunction.TextChange(objRpt, "txtForDirectorGeneral", Language.getMessage(mstrModuleName, 51, "FOR : DIRECTOR GENERAL"))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeCertify", Language.getMessage(mstrModuleName, 52, "I"))
                Call ReportFunction.TextChange(objRpt, "txtCeritfyattended", Language.getMessage(mstrModuleName, 53, "certify that I have attended"))
                Call ReportFunction.TextChange(objRpt, "txtTreatmentCost", Language.getMessage(mstrModuleName, 54, "treatment at this hospital at a cost of Tshs"))
                Call ReportFunction.TextChange(objRpt, "txtMedicineIncludes", Language.getMessage(mstrModuleName, 55, "Medicine given includes :-"))
                Call ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 56, "Name :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicineSign", Language.getMessage(mstrModuleName, 57, "Signature :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicineDate", Language.getMessage(mstrModuleName, 58, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtTheDirectorGeneral", Language.getMessage(mstrModuleName, 59, "The DIRECTOR GENERAL"))
                Call ReportFunction.TextChange(objRpt, "txtCertifyEmpTitle", Language.getMessage(mstrModuleName, 60, "I Certify that Mr. /  Miss. / MS."))
                Call ReportFunction.TextChange(objRpt, "txtUndertreatment", Language.getMessage(mstrModuleName, 61, "Is under treatment and able / unable to attend his / her duties. He / She is admited in hospital."))
                Call ReportFunction.TextChange(objRpt, "txtContinueAttending", Language.getMessage(mstrModuleName, 62, "He / She is to continue to attending "))
                Call ReportFunction.TextChange(objRpt, "txtFortreatment", Language.getMessage(mstrModuleName, 63, "for treatment."))
                Call ReportFunction.TextChange(objRpt, "txtReferred", Language.getMessage(mstrModuleName, 64, "He / She is to referred to "))
                Call ReportFunction.TextChange(objRpt, "txtReferredfor", Language.getMessage(mstrModuleName, 65, "for "))
                Call ReportFunction.TextChange(objRpt, "txtNameMedicalOfficer", Language.getMessage(mstrModuleName, 66, "Name of Medical Officer"))
                Call ReportFunction.TextChange(objRpt, "txtHerebyEmpTitle", Language.getMessage(mstrModuleName, 67, "I hereby  Certify that Mr. / Mrs. /  Miss / MS."))
                Call ReportFunction.TextChange(objRpt, "txtHasnow", Language.getMessage(mstrModuleName, 68, "has now"))
                Call ReportFunction.TextChange(objRpt, "txtResumeduties", Language.getMessage(mstrModuleName, 69, "sufficiently recovered to resume his / her duties."))
                Call ReportFunction.TextChange(objRpt, "txtExempted", Language.getMessage(mstrModuleName, 70, "He / She has been exempted from duty for"))
                Call ReportFunction.TextChange(objRpt, "txtDays", Language.getMessage(mstrModuleName, 71, "days."))
                Call ReportFunction.TextChange(objRpt, "txtSignStampMdOfficer", Language.getMessage(mstrModuleName, 72, "Name and Signature of Medical Officer"))
                Call ReportFunction.TextChange(objRpt, "txtMdSignDate", Language.getMessage(mstrModuleName, 49, "Date"))
                Call ReportFunction.TextChange(objRpt, "txtMdSigntime", Language.getMessage(mstrModuleName, 50, "Time"))
                Call ReportFunction.TextChange(objRpt, "txtSickSheetValid", Language.getMessage(mstrModuleName, 73, "NB : THIS SICK SHEET FORM IS VALID FOR ONLY 24 HOURS."))

                'Pinkal (25-Nov-2014) -- Start
                'Enhancement -  ADDED NEW SICK SHEET FORMAT FOR VISION FUND TANZANIA.
            ElseIf mintSickSheetTemplate = 3 Then  'VISION FUND TANZANIA

                Call ReportFunction.TextChange(objRpt, "txtSickSheetTreatment", Language.getMessage(mstrModuleName, 74, "SICK SHEET"))
                Call ReportFunction.TextChange(objRpt, "txtSickSheetTreatment1", Language.getMessage(mstrModuleName, 75, "To be filled in when"))
                Call ReportFunction.TextChange(objRpt, "txtSickSheetTreatment2", Language.getMessage(mstrModuleName, 76, "an employee Or his / her"))
                Call ReportFunction.TextChange(objRpt, "txtSickSheetTreatment3", Language.getMessage(mstrModuleName, 77, "family member is sent for treatment"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalRecords", Language.getMessage(mstrModuleName, 78, "This sick sheet must be filed in a medical records file at the end of treatment"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficer", Language.getMessage(mstrModuleName, 79, "The Medical Officer in charge of "))
                Call ReportFunction.TextChange(objRpt, "txtHealthClinic", Language.getMessage(mstrModuleName, 80, "Hospital / Health Centre / Clinic / Dispensary "))
                Call ReportFunction.TextChange(objRpt, "txtNameofPatient", Language.getMessage(mstrModuleName, 81, "Name of Patient :"))
                Call ReportFunction.TextChange(objRpt, "txtMrMiss", Language.getMessage(mstrModuleName, 82, "Mr. / Mrs. / Miss"))
                Call ReportFunction.TextChange(objRpt, "txtEmployeeDepartment", Language.getMessage(mstrModuleName, 83, "Employee's / Department / Branch VFT Head Office"))
                Call ReportFunction.TextChange(objRpt, "txtDesignation", Language.getMessage(mstrModuleName, 84, "Designation : "))
                Call ReportFunction.TextChange(objRpt, "txtRelationship", Language.getMessage(mstrModuleName, 85, "Relationship to employee (if not staff)"))
                Call ReportFunction.TextChange(objRpt, "txtAuthorizedDate", Language.getMessage(mstrModuleName, 58, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtAuthorizedTime", Language.getMessage(mstrModuleName, 86, "Time :"))
                Call ReportFunction.TextChange(objRpt, "txtSignature", Language.getMessage(mstrModuleName, 87, "Authorized / Signature and Stamp"))
                Call ReportFunction.TextChange(objRpt, "txtReportMedicalOfficer", Language.getMessage(mstrModuleName, 88, "Report From Medical Officer :"))
                Call ReportFunction.TextChange(objRpt, "txtCertify", Language.getMessage(mstrModuleName, 89, "I certify that Mr. / Mrs / Miss"))
                Call ReportFunction.TextChange(objRpt, "txtHasnow", Language.getMessage(mstrModuleName, 90, "has now"))
                Call ReportFunction.TextChange(objRpt, "txtworkOccupation", Language.getMessage(mstrModuleName, 91, "sufficiently recovered to resume his/her work / occupation."))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficerSignDate", Language.getMessage(mstrModuleName, 58, "Date :"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficerSignTime", Language.getMessage(mstrModuleName, 86, "Time :"))
                Call ReportFunction.TextChange(objRpt, "txtAttandances", Language.getMessage(mstrModuleName, 92, "RECORDS OF ATTENDANCES AND VISITS"))
                Call ReportFunction.TextChange(objRpt, "txtAttendancesDate", Language.getMessage(mstrModuleName, 93, "DATE"))
                Call ReportFunction.TextChange(objRpt, "txtAttendancesTime", Language.getMessage(mstrModuleName, 94, "TIME"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOffRemark", Language.getMessage(mstrModuleName, 95, "MEDICAL OFFICER'S REMARKS"))
                Call ReportFunction.TextChange(objRpt, "txtEDLD", Language.getMessage(mstrModuleName, 96, "ED OR LD"))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOfficerSignature", Language.getMessage(mstrModuleName, 97, "MEDICAL OFFICER'S SIGNATURE"))
                Call ReportFunction.TextChange(objRpt, "txtSupervisorSignature", Language.getMessage(mstrModuleName, 98, "SUPERVISOR'S SIGNATURE AFTER EMPLOYEE'S RETURN"))
                Call ReportFunction.TextChange(objRpt, "txtInstructions", Language.getMessage(mstrModuleName, 99, "INSTURCTIONS"))
                Call ReportFunction.TextChange(objRpt, "txta", Language.getMessage(mstrModuleName, 100, "a)"))
                Call ReportFunction.TextChange(objRpt, "txtb", Language.getMessage(mstrModuleName, 101, "b)"))
                Call ReportFunction.TextChange(objRpt, "txtC", Language.getMessage(mstrModuleName, 102, "c)"))
                Call ReportFunction.TextChange(objRpt, "txtd", Language.getMessage(mstrModuleName, 103, "d)"))
                Call ReportFunction.TextChange(objRpt, "txtInstructionA", Language.getMessage(mstrModuleName, 104, "The Sick sheet is to be used in all departments for all eligible members of VFT medical service to include registered staff and those members of staff family with records in VFT."))
                Call ReportFunction.TextChange(objRpt, "txtInstructionB", Language.getMessage(mstrModuleName, 105, "Staff or eligible member of staff family attending treatment shall make sure that the used sheet is returned to VFT for filling. Medical officer must have signed in the returned sheet; otherwise it will be counted null and void."))
                Call ReportFunction.TextChange(objRpt, "txtInstructionC", Language.getMessage(mstrModuleName, 106, "For control paper of use, staff attending treatment in appointment can proceed with the used sheet up to the complete bill circle after which each new illness will require a fresh sick sheet to be issued. However the sick sheet must be returned to file every time patient is coming form treatment"))
                Call ReportFunction.TextChange(objRpt, "txtInstructionD", Language.getMessage(mstrModuleName, 107, "It is not allowed for staff to have sick sheets at their homes, if there is an emergency during holidays or after office hours,staff shall go with the identity card,which will be retained at the hospital waiting for the respective staff to submit sick sheet the next working day."))
                Call ReportFunction.TextChange(objRpt, "txtMedicalOffSignature", Language.getMessage(mstrModuleName, 108, "Medical Officer's Signature :"))
                Call ReportFunction.TextChange(objRpt, "txtCurrentDate", ConfigParameter._Object._CurrentDateAndTime.Date)
                Call ReportFunction.TextChange(objRpt, "txtCurrentTime", ConfigParameter._Object._CurrentDateAndTime.ToLongTimeString)

                'Pinkal (25-Nov-2014) -- End

            End If

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", ConfigParameter._Object._CurrentDateAndTime)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (12-Sep-2014) -- End

    Public Sub generateReport(ByVal mdtEmployeeAsonDate As Date, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

        'Pinkal (16-Apr-2016) --  'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.['ByVal mdtEmployeeAsonDate As Date]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then mintCompanyUnkid = Company._Object._Companyunkid
            Company._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then mintUserUnkid = User._Object._Userunkid
            User._Object._Userunkid = mintUserUnkid


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'objRpt = Generate_DetailReport()
            objRpt = Generate_DetailReport(mdtEmployeeAsonDate)
            'Pinkal (16-Apr-2016) -- End


            Rpt = objRpt
            If Not IsNothing(objRpt) Then

                If PrintAction = enPrintAction.Print Then
                    Dim prd As New System.Drawing.Printing.PrintDocument
                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    objRpt.PrintOptions.PaperSize = prd.PrinterSettings.DefaultPageSettings.PaperSize.RawKind
                    objRpt.PrintToPrinter(3, False, 0, 0)

                ElseIf PrintAction = enPrintAction.Preview Then
                    Dim objReportViewer As New ArutiReportViewer
                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
                    objReportViewer._Heading = Language.getMessage(mstrModuleName, 5, "MEDICAL SICK SHEET")
                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt
                    objReportViewer.Show()

                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Send_ESickSheet(ByVal mdtEmployeeAsonDate As Date, ByVal StrFileName As String, ByVal StrFilePath As String)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'objRpt = Generate_DetailReport()
            objRpt = Generate_DetailReport(mdtEmployeeAsonDate)
            'Pinkal (16-Apr-2016) -- End
            If Not IsNothing(objRpt) Then
                Dim oStream As New IO.MemoryStream
                oStream = objRpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat)
                Dim fs As New System.IO.FileStream(StrFilePath & "\" & StrFileName & ".pdf", IO.FileMode.Create, IO.FileAccess.Write)
                Dim data As Byte() = oStream.ToArray()
                fs.Write(data, 0, data.Length)
                fs.Close()
                fs.Dispose()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_ESickSheet; Module Name: " & mstrModuleName)
        Finally
            objRpt.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 4, "No :")
            Language.setMessage(mstrModuleName, 5, "MEDICAL SICK SHEET")
            Language.setMessage(mstrModuleName, 6, "To: Medical Officer in Charge/ Hospital Name :")
            Language.setMessage(mstrModuleName, 8, "Position :")
            Language.setMessage(mstrModuleName, 9, "Has been brought to you for medical treatment and is entitled to medical cover level :")
            Language.setMessage(mstrModuleName, 10, "Please attend")
            Language.setMessage(mstrModuleName, 11, "Date :")
            Language.setMessage(mstrModuleName, 12, "(Name of HR officer issuing the SS)")
            Language.setMessage(mstrModuleName, 13, "To: Commissioner General,")
            Language.setMessage(mstrModuleName, 14, "I hereby declare that")
            Language.setMessage(mstrModuleName, 15, "Has been treated/attended accordingly. I do suggest that")
            Language.setMessage(mstrModuleName, 16, "days/ be admitted at the hospital for further treatments*.")
            Language.setMessage(mstrModuleName, 17, "(Name and Signature of medical officer)")
            Language.setMessage(mstrModuleName, 18, "Can now continue work normally")
            Language.setMessage(mstrModuleName, 19, "Please note that the following List of Dependants are qualified for medical services under this employee’s cover :")
            Language.setMessage(mstrModuleName, 20, "Orignal Copy must be attached with Hospital Invoices.")
            Language.setMessage(mstrModuleName, 21, "Copy for Hospital records.")
            Language.setMessage(mstrModuleName, 22, "Copy for")
            Language.setMessage(mstrModuleName, 23, "Payroll No :")
            Language.setMessage(mstrModuleName, 24, "Printed Date :")
            Language.setMessage(mstrModuleName, 25, "Page :")
            Language.setMessage(mstrModuleName, 26, "Branch Name :")
            Language.setMessage(mstrModuleName, 27, "Dependent Name")
            Language.setMessage(mstrModuleName, 28, "Age")
            Language.setMessage(mstrModuleName, 29, "Gender")
            Language.setMessage(mstrModuleName, 30, "Relation")
            Language.setMessage(mstrModuleName, 31, "This Sick Sheet is valid from date above to the last date of the month.")
            Language.setMessage(mstrModuleName, 32, "Sick Sheet Date :")
            Language.setMessage(mstrModuleName, 33, "Male")
            Language.setMessage(mstrModuleName, 34, "Female")
            Language.setMessage(mstrModuleName, 35, "Other")
            Language.setMessage(mstrModuleName, 36, " HR department.")
            Language.setMessage(mstrModuleName, 37, " Should continue with work/take a rest for")
            Language.setMessage(mstrModuleName, 38, "SICK SHEET FORM")
            Language.setMessage(mstrModuleName, 39, "The Medical Officer in Charge")
            Language.setMessage(mstrModuleName, 40, "Hospital / Clinic / Dispensary / Rural Health Center.")
            Language.setMessage(mstrModuleName, 41, "Mr. / Mrs / Dr / Miss / MS")
            Language.setMessage(mstrModuleName, 42, "Designation")
            Language.setMessage(mstrModuleName, 43, "Hospital Card No")
            Language.setMessage(mstrModuleName, 44, "is sent to")
            Language.setMessage(mstrModuleName, 45, "your for treatment,  on the expense of TANAPA.")
            Language.setMessage(mstrModuleName, 46, "Mr. / Miss / Mrs. / He / She is a Son / Daughter , Wife / Husband of")
            Language.setMessage(mstrModuleName, 47, "Mr. / Mrs /  Miss / MS")
            Language.setMessage(mstrModuleName, 48, "Signature and Stamp")
            Language.setMessage(mstrModuleName, 49, "Date")
            Language.setMessage(mstrModuleName, 50, "Time")
            Language.setMessage(mstrModuleName, 51, "FOR : DIRECTOR GENERAL")
            Language.setMessage(mstrModuleName, 52, "I")
            Language.setMessage(mstrModuleName, 53, "certify that I have attended")
            Language.setMessage(mstrModuleName, 54, "treatment at this hospital at a cost of Tshs")
            Language.setMessage(mstrModuleName, 55, "Medicine given includes :-")
            Language.setMessage(mstrModuleName, 56, "Name :")
            Language.setMessage(mstrModuleName, 57, "Signature :")
            Language.setMessage(mstrModuleName, 58, "Date :")
            Language.setMessage(mstrModuleName, 59, "The DIRECTOR GENERAL")
            Language.setMessage(mstrModuleName, 60, "I Certify that Mr. /  Miss. / MS.")
            Language.setMessage(mstrModuleName, 61, "Is under treatment and able / unable to attend his / her duties. He / She is admited in hospital.")
            Language.setMessage(mstrModuleName, 62, "He / She is to continue to attending")
            Language.setMessage(mstrModuleName, 63, "for treatment.")
            Language.setMessage(mstrModuleName, 64, "He / She is to referred to")
            Language.setMessage(mstrModuleName, 65, "for")
            Language.setMessage(mstrModuleName, 66, "Name of Medical Officer")
            Language.setMessage(mstrModuleName, 67, "I hereby  Certify that Mr. / Mrs. /  Miss / MS.")
            Language.setMessage(mstrModuleName, 68, "has now")
            Language.setMessage(mstrModuleName, 69, "sufficiently recovered to resume his / her duties.")
            Language.setMessage(mstrModuleName, 70, "He / She has been exempted from duty for")
            Language.setMessage(mstrModuleName, 71, "days.")
            Language.setMessage(mstrModuleName, 72, "Name and Signature of Medical Officer")
            Language.setMessage(mstrModuleName, 73, "NB : THIS SICK SHEET FORM IS VALID FOR ONLY 24 HOURS.")
            Language.setMessage(mstrModuleName, 74, "SICK SHEET")
            Language.setMessage(mstrModuleName, 75, "To be filled in when")
            Language.setMessage(mstrModuleName, 76, "an employee Or his / her")
            Language.setMessage(mstrModuleName, 77, "family member is sent for treatment")
            Language.setMessage(mstrModuleName, 78, "This sick sheet must be filed in a medical records file at the end of treatment")
            Language.setMessage(mstrModuleName, 79, "The Medical Officer in charge of")
            Language.setMessage(mstrModuleName, 80, "Hospital / Health Centre / Clinic / Dispensary")
            Language.setMessage(mstrModuleName, 81, "Name of Patient :")
            Language.setMessage(mstrModuleName, 82, "Mr. / Mrs. / Miss")
            Language.setMessage(mstrModuleName, 83, "Employee's / Department / Branch VFT Head Office")
            Language.setMessage(mstrModuleName, 84, "Designation :")
            Language.setMessage(mstrModuleName, 85, "Relationship to employee (if not staff)")
            Language.setMessage(mstrModuleName, 86, "Time :")
            Language.setMessage(mstrModuleName, 87, "Authorized / Signature and Stamp")
            Language.setMessage(mstrModuleName, 88, "Report From Medical Officer :")
            Language.setMessage(mstrModuleName, 89, "I certify that Mr. / Mrs / Miss")
            Language.setMessage(mstrModuleName, 90, "has now")
            Language.setMessage(mstrModuleName, 91, "sufficiently recovered to resume his/her work / occupation.")
            Language.setMessage(mstrModuleName, 92, "RECORDS OF ATTENDANCES AND VISITS")
            Language.setMessage(mstrModuleName, 93, "DATE")
            Language.setMessage(mstrModuleName, 94, "TIME")
            Language.setMessage(mstrModuleName, 95, "MEDICAL OFFICER'S REMARKS")
            Language.setMessage(mstrModuleName, 96, "ED OR LD")
            Language.setMessage(mstrModuleName, 97, "MEDICAL OFFICER'S SIGNATURE")
            Language.setMessage(mstrModuleName, 98, "SUPERVISOR'S SIGNATURE AFTER EMPLOYEE'S RETURN")
            Language.setMessage(mstrModuleName, 99, "INSTURCTIONS")
            Language.setMessage(mstrModuleName, 100, "a)")
            Language.setMessage(mstrModuleName, 101, "b)")
            Language.setMessage(mstrModuleName, 102, "c)")
            Language.setMessage(mstrModuleName, 103, "d)")
            Language.setMessage(mstrModuleName, 104, "The Sick sheet is to be used in all departments for all eligible members of VFT medical service to include registered staff and those members of staff family with records in VFT.")
            Language.setMessage(mstrModuleName, 105, "Staff or eligible member of staff family attending treatment shall make sure that the used sheet is returned to VFT for filling. Medical officer must have signed in the returned sheet; otherwise it will be counted null and void.")
            Language.setMessage(mstrModuleName, 106, "For control paper of use, staff attending treatment in appointment can proceed with the used sheet up to the complete bill circle after which each new illness will require a fresh sick sheet to be issued. However the sick sheet must be returned to file every time patient is coming form treatment")
            Language.setMessage(mstrModuleName, 107, "It is not allowed for staff to have sick sheets at their homes, if there is an emergency during holidays or after office hours,staff shall go with the identity card,which will be retained at the hospital waiting for the respective staff to submit sick sheet the next working day.")
            Language.setMessage(mstrModuleName, 108, "Medical Officer's Signature :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

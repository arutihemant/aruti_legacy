'************************************************************************************************************************************
'Class Name : frmIncomeTaxDeductionP9.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmIncomeTaxDeductionP9

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmIncomeTaxDeductionP9"
    Private objIncomeTaxP9 As clsIncomeTaxDeductionP9

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrPeriodsName As String = ""
    'S.SANDEEP [ 12 MAY 2012 ] -- END


    'S.SANDEEP [ 02 AUG 2012 ] -- START
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 02 AUG 2012 ] -- END
    Private mstrAnalysis_OrderBy_GroupName As String = "" 'Sohail (30 Nov 2013)

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtPeriodEndDate As DateTime
    'S.SANDEEP [ 16 JAN 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        objIncomeTaxP9 = New clsIncomeTaxDeductionP9(User._Object._Languageunkid,Company._Object._Companyunkid)
        objIncomeTaxP9.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim dsCombos As New DataSet

        'S.SANDEEP [ 16 JAN 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objExcessSlan As New clsTranheadInexcessslabTran 'Sohail (04 Dec 2014)
        'S.SANDEEP [ 16 JAN 2013 ] -- END

        Try

            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 24 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            'lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            'For Each dtRow As DataRow In dsCombos.Tables(0).Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = ""
            '    lvItem.SubItems.Add(dtRow.Item("name").ToString)
            '    lvItem.Tag = dtRow.Item("periodunkid")
            '    lvPeriod.Items.Add(lvItem)
            '    lvItem = Nothing
            'Next
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            dsCombos = objTranHead.GetHeadListTypeWise(enTranHeadType.EmployeesStatutoryDeductions, enTypeOf.Employee_Statutory_Contributions)
            lvDeduction.Items.Clear()
            For Each dtRow As DataRow In dsCombos.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.SubItems.Add(dtRow.Item("TypeOf").ToString)
                lvItem.Tag = dtRow.Item("Id")
                lvDeduction.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvDeduction.GroupingColumn = objcolhTypeOf
            lvDeduction.DisplayGroups(True)
            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If lvDeduction.Items.Count > 4 Then
                colhTranHeadName.Width = 209 - 18
            Else
                colhTranHeadName.Width = 209
            End If
            'Sohail (28 Aug 2013) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Housing", True, enTranHeadType.EarningForEmployees)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Put all earnings head except salary on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Housing", True, enTranHeadType.EarningForEmployees)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Housing", True, enTranHeadType.EarningForEmployees, , , , , "typeof_id NOT IN ( " & enTypeOf.Salary & ")")
            'Sohail (21 Aug 2015) -- End
            'Dim dtTable As DataTable = Nothing
            'Dim strTypeOfIds As String = enTypeOf.Salary & "," & enTypeOf.Allowance & "," & enTypeOf.BENEFIT
            'dtTable = New DataView(dsCombos.Tables(0), "typeof_id NOT IN ( " & strTypeOfIds & ")", "", DataViewRowState.CurrentRows).ToTable
            'Anjan [26 September 2016] -- End

            With cboHousing
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With


            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Sohail (04 Dec 2014) -- Start
            'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
            'dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
            'With cboSlabEffectivePeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Slab")
            '    .SelectedValue = 0
            'End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("inexcess", False, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "inexcess", False, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            'Sohail (21 Aug 2015) -- End
            With cboEffectSlabHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("inexcess")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            'Sohail (04 Dec 2014) -- End
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End


            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'Sohail (28 Aug 2013) -- End

            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Income Tax Deduction Card P9 Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "P.A.Y.E - DETAILS of Payment with Tax Withheld"))
            'Sohail (07 Sep 2015) -- Start
            'Enhancement - New Report type P9 Annualization Report in P9 Report.
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 8, "P9 Annualization Report"))
            'Sohail (07 Sep 2015) -- End
            cboReportType.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboHousing.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0 'Sohail (28 Aug 2013)
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport 'Sohail (07 Sep 2013)
            chkShowGroupByCCenter.Checked = False 'Sohail (30 Nov 2013)
            chkShowPublicAddressCity.Checked = False 'Sohail (18 Apr 2014)

            Call DoOperation(lvDeduction, False)
            Call DoOperation(lvPeriod, False)
            'S.SANDEEP [ 02 AUG 2012 ] -- START
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 02 AUG 2012 ] -- END
            mstrAnalysis_OrderBy_GroupName = "" 'Sohail (30 Nov 2013)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objIncomeTaxP9.SetDefaultValue()

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrPeriodsName = String.Empty
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If cboHousing.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Housing Head is compulsory information.Please select Housing Head to continue."), enMsgBoxStyle.Information)
                cboHousing.Focus()
                Return False
            End If

            If lvDeduction.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one Employee statutary deduction head."), enMsgBoxStyle.Information)
                lvDeduction.Focus()
                Return False
            End If

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If cboReportType.SelectedIndex = 1 Then
            '    If lvPeriod.CheckedItems.Count <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check atleast one Period to view report."), enMsgBoxStyle.Information)
            '        lvPeriod.Focus()
            '        Return False
            '    End If
            'End If
            If CInt(cboSlabEffectivePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                cboSlabEffectivePeriod.Focus()
                Return False
            End If
                If lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please check Periods to view report."), enMsgBoxStyle.Information)
                    lvPeriod.Focus()
                    Return False
                End If
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'Sohail (28 Aug 2013) -- End


            objIncomeTaxP9._ReportTypeId = cboReportType.SelectedIndex
            objIncomeTaxP9._ReportTypeName = cboReportType.Text

            objIncomeTaxP9._EmpId = cboEmployee.SelectedValue
            objIncomeTaxP9._EmpName = cboEmployee.Text

            objIncomeTaxP9._HousingTranId = cboHousing.SelectedValue

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True Then
                objIncomeTaxP9._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objIncomeTaxP9._OtherEarningTranId = 0
            End If
            'Sohail (28 Aug 2013) -- End

            'Sohail (30 Nov 2013) -- Start
            'Enhancement - Oman
            objIncomeTaxP9._ShowGroupByCostCenter = chkShowGroupByCCenter.Checked
            objIncomeTaxP9._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (30 Nov 2013) -- End

            'Select Case cboReportType.SelectedIndex
            '    Case 0  'Income Tax Deduction Report P9
            '        Dim strSatutaryIds As String = ""
            '        Call GetCommaSeparatedTags(lvDeduction, strSatutaryIds)
            '        objIncomeTaxP9._StatutoryIds = strSatutaryIds
            '    Case 1  'P.A.Y.E DETAILS
            '        Dim strSatutaryIds As String = ""
            '        Call GetCommaSeparatedTags(lvDeduction, strSatutaryIds)
            '        objIncomeTaxP9._StatutoryIds = strSatutaryIds

            '        Dim strPreriodIds As String = ""
            '        Call GetCommaSeparatedTags(lvPeriod, strPreriodIds)
            '        objIncomeTaxP9._PeriodIds = strPreriodIds
            'End Select

            Dim strSatutaryIds As String = ""
            Call GetCommaSeparatedTags(lvDeduction, strSatutaryIds)
            objIncomeTaxP9._StatutoryIds = strSatutaryIds

            Dim strPreriodIds As String = ""
            Call GetCommaSeparatedTags(lvPeriod, strPreriodIds)
            objIncomeTaxP9._PeriodIds = strPreriodIds

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIncomeTaxP9._PeriodNames = mstrPeriodsName
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIncomeTaxP9._PeriodEndDate = mdtPeriodEndDate
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            objIncomeTaxP9._ShowPublicAddressCity = chkShowPublicAddressCity.Checked 'Sohail (18 Apr 2014)

            'S.SANDEEP [ 02 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIncomeTaxP9._ViewByIds = mstrStringIds
            objIncomeTaxP9._ViewIndex = mintViewIdx
            objIncomeTaxP9._ViewByName = mstrStringName
            objIncomeTaxP9._Analysis_Fields = mstrAnalysis_Fields
            objIncomeTaxP9._Analysis_Join = mstrAnalysis_Join
            objIncomeTaxP9._Analysis_OrderBy = mstrAnalysis_OrderBy
            objIncomeTaxP9._Report_GroupName = mstrReport_GroupName
            'S.SANDEEP [ 02 AUG 2012 ] -- END

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objIncomeTaxP9._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub DoOperation(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub GetCommaSeparatedTags(ByVal LV As ListView, ByRef mstrIds As String)
        Try
            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim iCnt As Integer = 1
            'S.SANDEEP [ 16 JAN 2013 ] -- END
            For Each Item As ListViewItem In LV.CheckedItems
                mstrIds &= "," & Item.Tag.ToString
                'S.SANDEEP [ 12 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If LV.Name.ToUpper = "LVPERIOD" Then
                    mstrPeriodsName &= "," & Item.SubItems(colhPeriodName.Index).Text.ToString
                    'S.SANDEEP [ 16 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If iCnt = LV.CheckedItems.Count Then
                        mdtPeriodEndDate = eZeeDate.convertDate(Item.SubItems(colhPeriodName.Index).Tag.ToString)
                    End If
                    iCnt += 1
                    'S.SANDEEP [ 16 JAN 2013 ] -- END
                End If
                'S.SANDEEP [ 12 MAY 2012 ] -- END
            Next
            mstrIds = Mid(mstrIds, 2)
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrPeriodsName = Mid(mstrPeriodsName, 2)
            'S.SANDEEP [ 12 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCommaSeparatedTags", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Private Sub frmIncomeTaxDeductionP9_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objIncomeTaxP9 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxDeductionP9_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmIncomeTaxDeductionP9_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)


            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END


            Me._Title = objIncomeTaxP9._ReportName
            Me._Message = objIncomeTaxP9._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmIncomeTaxDeductionP9_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIncomeTaxP9.generateReport(cboReportType.SelectedIndex, e.Type, enExportAction.None)
            objIncomeTaxP9.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIncomeTaxP9.generateReport(cboReportType.SelectedIndex, enPrintAction.None, e.Type)
            objIncomeTaxP9.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, ConfigParameter._Object._ExportReportPath, _
                                             ConfigParameter._Object._OpenAfterExport, cboReportType.SelectedIndex, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsIncomeTaxDeductionP9.SetMessages()
            objfrm._Other_ModuleNames = "clsIncomeTaxDeductionP9"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END
#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(lvPeriod, CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkAllTranHead_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllTranHead.CheckedChanged
        Try
            Call DoOperation(lvDeduction, CBool(objchkAllTranHead.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllTranHead_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvDeduction_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDeduction.ItemChecked
        Try
            RemoveHandler objchkAllTranHead.CheckedChanged, AddressOf objchkAllTranHead_CheckedChanged
            If lvDeduction.CheckedItems.Count <= 0 Then
                objchkAllTranHead.CheckState = CheckState.Unchecked
            ElseIf lvDeduction.CheckedItems.Count < lvDeduction.Items.Count Then
                objchkAllTranHead.CheckState = CheckState.Indeterminate
            ElseIf lvDeduction.CheckedItems.Count = lvDeduction.Items.Count Then
                objchkAllTranHead.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllTranHead.CheckedChanged, AddressOf objchkAllTranHead_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDeduction_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        'Try
        '    Select Case cboReportType.SelectedIndex
        '        Case 0  'Income Tax Deduction p9
        '            lvPeriod.Enabled = False
        '            objchkAllPeriod.Enabled = False
        '            Call DoOperation(lvPeriod, False)
        '        Case 1
        '            lvPeriod.Enabled = True
        '            objchkAllPeriod.Enabled = True
        '    End Select
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        'End Try
        'Sohail (30 Nov 2013) -- Start
        'Enhancement - Oman
        chkShowGroupByCCenter.Checked = False
        chkShowGroupByCCenter.Enabled = False
        'Sohail (18 Apr 2014) -- Start
        'Enhancement - Show / Hide Address City option on PAYE Detail Report
        chkShowPublicAddressCity.Checked = False
        chkShowPublicAddressCity.Visible = False
        'Sohail (18 Apr 2014) -- End
        Select Case cboReportType.SelectedIndex
            Case 0  'Income Tax Deduction p9

            Case 1 'PAYEE Detail
                chkShowGroupByCCenter.Enabled = True
                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show / Hide Address City option on PAYE Detail Report
                chkShowPublicAddressCity.Visible = True
                chkShowPublicAddressCity.Checked = True
                'Sohail (18 Apr 2014) -- End

                'Sohail (07 Sep 2015) -- Start
                'Enhancement - New Report type P9 Annualization Report in P9 Report.
            Case 2 'P9 Annualization Report
                'Sohail (07 Sep 2015) -- End

        End Select
        'Sohail (30 Nov 2013) -- End


    End Sub

    'S.SANDEEP [ 02 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            'Anjan (17 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Anjan (17 May 2012)-End 
            mstrAnalysis_OrderBy_GroupName = frm._Analysis_OrderBy_GName 'Sohail (30 Nov 2013)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 02 AUG 2012 ] -- END

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End


                'Sohail (04 Dec 2014) -- Start
                'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
                'Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0)
                Dim ds As DataSet = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboEffectSlabHead.SelectedValue))
                'Sohail (04 Dec 2014) -- End
                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
                dtTable.Rows.Clear()
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 6 Then
                colhPeriodName.Width = 205 - 18
            Else
                colhPeriodName.Width = 205
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 JAN 2013 ] -- END

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (28 Aug 2013) -- End

    'Sohail (04 Dec 2014) -- Start
    'Enhancement - Effective Slab Head selection for Effective Period (issue was for more than one in excess slab heads).
    Private Sub cboEffectSlabHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEffectSlabHead.SelectedIndexChanged
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Try
            dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboEffectSlabHead.SelectedValue))
            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEffectSlabHead_SelectedIndexChanged", mstrModuleName)
        Finally
            objExcessSlan = Nothing
        End Try
    End Sub
    'Sohail (04 Dec 2014) -- End

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblHousing.Text = Language._Object.getCaption(Me.lblHousing.Name, Me.lblHousing.Text)
			Me.lblDeduction.Text = Language._Object.getCaption(Me.lblDeduction.Name, Me.lblDeduction.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.colhTranHeadName.Text = Language._Object.getCaption(CStr(Me.colhTranHeadName.Tag), Me.colhTranHeadName.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.chkShowGroupByCCenter.Text = Language._Object.getCaption(Me.chkShowGroupByCCenter.Name, Me.chkShowGroupByCCenter.Text)
			Me.lblEffectSlabHead.Text = Language._Object.getCaption(Me.lblEffectSlabHead.Name, Me.lblEffectSlabHead.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Income Tax Deduction Card P9 Report")
			Language.setMessage(mstrModuleName, 2, "P.A.Y.E - DETAILS of Payment with Tax Withheld")
			Language.setMessage(mstrModuleName, 3, "Housing Head is compulsory information.Please select Housing Head to continue.")
			Language.setMessage(mstrModuleName, 4, "Please check atleast one Employee statutary deduction head.")
			Language.setMessage(mstrModuleName, 5, "Please check Periods to view report.")
			Language.setMessage(mstrModuleName, 6, "Effective Period is mandatory information. Please select Effective Period.")
			Language.setMessage(mstrModuleName, 7, "Please select transaction head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

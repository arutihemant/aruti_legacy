﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMauritius_NPF_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMauritius_NPF_Report))
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblMembership = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboEmpContribution = New System.Windows.Forms.ComboBox
        Me.lblEmpContribution = New System.Windows.Forms.Label
        Me.lnkSaveSelection = New System.Windows.Forms.LinkLabel
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(143, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(244, 21)
        Me.cboPeriod.TabIndex = 101
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(14, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(123, 17)
        Me.lblPeriod.TabIndex = 100
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(14, 64)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(123, 17)
        Me.lblMembership.TabIndex = 99
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboEmpContribution)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpContribution)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(403, 137)
        Me.gbFilterCriteria.TabIndex = 109
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpContribution
        '
        Me.cboEmpContribution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpContribution.DropDownWidth = 180
        Me.cboEmpContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpContribution.FormattingEnabled = True
        Me.cboEmpContribution.Location = New System.Drawing.Point(143, 88)
        Me.cboEmpContribution.Name = "cboEmpContribution"
        Me.cboEmpContribution.Size = New System.Drawing.Size(244, 21)
        Me.cboEmpContribution.TabIndex = 110
        '
        'lblEmpContribution
        '
        Me.lblEmpContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContribution.Location = New System.Drawing.Point(14, 91)
        Me.lblEmpContribution.Name = "lblEmpContribution"
        Me.lblEmpContribution.Size = New System.Drawing.Size(123, 17)
        Me.lblEmpContribution.TabIndex = 111
        Me.lblEmpContribution.Text = "Employee Contribution"
        Me.lblEmpContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSaveSelection
        '
        Me.lnkSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSaveSelection.BackColor = System.Drawing.Color.Transparent
        Me.lnkSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSaveSelection.Location = New System.Drawing.Point(260, 115)
        Me.lnkSaveSelection.Name = "lnkSaveSelection"
        Me.lnkSaveSelection.Size = New System.Drawing.Size(127, 17)
        Me.lnkSaveSelection.TabIndex = 108
        Me.lnkSaveSelection.TabStop = True
        Me.lnkSaveSelection.Text = "[ Save Selection ]"
        Me.lnkSaveSelection.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(143, 61)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(244, 21)
        Me.cboMembership.TabIndex = 98
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(304, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 78
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(712, 60)
        Me.eZeeHeader.TabIndex = 107
        Me.eZeeHeader.Title = "NPF Report"
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 432)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(712, 55)
        Me.EZeeFooter1.TabIndex = 108
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(419, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(515, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(611, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(9, 209)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(403, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 110
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbBasicSalaryOtherEarning.Visible = False
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(399, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(364, 6)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(141, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(217, 21)
        Me.cboOtherEarning.TabIndex = 56
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(12, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(107, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmMauritius_NPF_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(712, 487)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMauritius_NPF_Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "NPF Report"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents lnkSaveSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEmpContribution As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpContribution As System.Windows.Forms.Label
End Class

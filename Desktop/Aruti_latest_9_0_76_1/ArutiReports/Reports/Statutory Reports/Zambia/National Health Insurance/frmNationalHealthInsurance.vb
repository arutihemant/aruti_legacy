﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmNationalHealthInsurance

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmNationalHealthInsurance"
    Private objNHI As clsNationalHealthInsurance
    Private mintFirstPeriodId As Integer = 0

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objNHI = New clsNationalHealthInsurance(User._Object._Languageunkid, Company._Object._Companyunkid)
        objNHI.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        BasicSalaryFormula = 2
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                         True, True, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objMaster = Nothing
            objPeriod = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboMembership.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstPeriodId
            cboEmployee.SelectedValue = 0

            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.National_Health_Insurance_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.BasicSalaryFormula
                            txtBasicSalaryFormula.Text = dsRow.Item("transactionheadid").ToString

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objNHI.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If


            objNHI._MembershipId = CInt(cboMembership.SelectedValue)
            objNHI._MembershipName = cboMembership.Text
            objNHI._PeriodId = CInt(cboPeriod.SelectedValue)
            objNHI._PeriodName = cboPeriod.Text
            objNHI._EmployeeId = CInt(cboEmployee.SelectedValue)
            objNHI._EmployeeName = cboEmployee.Text
            objNHI._BasicSalaryFormula = txtBasicSalaryFormula.Text

            objNHI._ViewByIds = mstrStringIds
            objNHI._ViewIndex = mintViewIdx
            objNHI._ViewByName = mstrStringName
            objNHI._Analysis_Fields = mstrAnalysis_Fields
            objNHI._Analysis_Join = mstrAnalysis_Join


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmNationalHealthInsurance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNHI = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNationalHealthInsurance_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNationalHealthInsurance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNationalHealthInsurance_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Try

            SaveDialog.FileName = ""
            SaveDialog.Filter = "Excel File|*.xlsx"
            SaveDialog.FilterIndex = 0

            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = cboPeriod.SelectedValue

            If objNHI.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._CurrencyFormat, SaveDialog.FileName, ConfigParameter._Object._OpenAfterExport) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data exported successfully!"), enMsgBoxStyle.Information)

                If ConfigParameter._Object._OpenAfterExport = True Then
                    Process.Start(SaveDialog.FileName)
                End If
            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.National_Health_Insurance_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.National_Health_Insurance_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.BasicSalaryFormula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtBasicSalaryFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.National_Health_Insurance_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsNationalHealthInsurance.SetMessages()
            objfrm._Other_ModuleNames = "clsNationalHealthInsurance"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnKeywordsBS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsBS.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 15, "Awailable Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#      (e.g. #010#)")

            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnKeywordsBS_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Other Control's Events "
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
            Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblBasicSalaryFormula.Text = Language._Object.getCaption(Me.lblBasicSalaryFormula.Name, Me.lblBasicSalaryFormula.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue.")
            Language.setMessage(mstrModuleName, 3, "Data exported successfully!")
            Language.setMessage(mstrModuleName, 4, "Selection Saved Successfully")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
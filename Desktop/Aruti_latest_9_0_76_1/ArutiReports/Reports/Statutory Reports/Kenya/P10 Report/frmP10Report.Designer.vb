﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmP10Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmP10Report))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objelLineOptional = New eZee.Common.eZeeLine
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnSearchFringeBenefit = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchAuditTax = New eZee.Common.eZeeGradientButton
        Me.cboFringeBenefit = New System.Windows.Forms.ComboBox
        Me.lblFringeBenefit = New System.Windows.Forms.Label
        Me.cboAuditTax = New System.Windows.Forms.ComboBox
        Me.lblAuditTax = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.lblPAYE = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(670, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objelLineOptional)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAuditTax)
        Me.gbFilterCriteria.Controls.Add(Me.cboFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.lblFringeBenefit)
        Me.gbFilterCriteria.Controls.Add(Me.cboAuditTax)
        Me.gbFilterCriteria.Controls.Add(Me.lblAuditTax)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(643, 196)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLineOptional
        '
        Me.objelLineOptional.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLineOptional.Location = New System.Drawing.Point(8, 152)
        Me.objelLineOptional.Name = "objelLineOptional"
        Me.objelLineOptional.Size = New System.Drawing.Size(595, 13)
        Me.objelLineOptional.TabIndex = 114
        Me.objelLineOptional.Text = "Optional Details"
        Me.objelLineOptional.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.objelLineOptional.Visible = False
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(121, 114)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 113
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'objbtnSearchFringeBenefit
        '
        Me.objbtnSearchFringeBenefit.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFringeBenefit.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFringeBenefit.BorderSelected = False
        Me.objbtnSearchFringeBenefit.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFringeBenefit.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFringeBenefit.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFringeBenefit.Location = New System.Drawing.Point(294, 87)
        Me.objbtnSearchFringeBenefit.Name = "objbtnSearchFringeBenefit"
        Me.objbtnSearchFringeBenefit.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchFringeBenefit.TabIndex = 100
        '
        'objbtnSearchAuditTax
        '
        Me.objbtnSearchAuditTax.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAuditTax.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAuditTax.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAuditTax.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAuditTax.BorderSelected = False
        Me.objbtnSearchAuditTax.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAuditTax.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAuditTax.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAuditTax.Location = New System.Drawing.Point(609, 60)
        Me.objbtnSearchAuditTax.Name = "objbtnSearchAuditTax"
        Me.objbtnSearchAuditTax.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAuditTax.TabIndex = 99
        '
        'cboFringeBenefit
        '
        Me.cboFringeBenefit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFringeBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFringeBenefit.FormattingEnabled = True
        Me.cboFringeBenefit.Location = New System.Drawing.Point(121, 87)
        Me.cboFringeBenefit.Name = "cboFringeBenefit"
        Me.cboFringeBenefit.Size = New System.Drawing.Size(167, 21)
        Me.cboFringeBenefit.TabIndex = 4
        '
        'lblFringeBenefit
        '
        Me.lblFringeBenefit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFringeBenefit.Location = New System.Drawing.Point(8, 89)
        Me.lblFringeBenefit.Name = "lblFringeBenefit"
        Me.lblFringeBenefit.Size = New System.Drawing.Size(107, 15)
        Me.lblFringeBenefit.TabIndex = 95
        Me.lblFringeBenefit.Text = "Fringe Benefit"
        Me.lblFringeBenefit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAuditTax
        '
        Me.cboAuditTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAuditTax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAuditTax.FormattingEnabled = True
        Me.cboAuditTax.Location = New System.Drawing.Point(436, 60)
        Me.cboAuditTax.Name = "cboAuditTax"
        Me.cboAuditTax.Size = New System.Drawing.Size(167, 21)
        Me.cboAuditTax.TabIndex = 3
        '
        'lblAuditTax
        '
        Me.lblAuditTax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAuditTax.Location = New System.Drawing.Point(323, 62)
        Me.lblAuditTax.Name = "lblAuditTax"
        Me.lblAuditTax.Size = New System.Drawing.Size(107, 15)
        Me.lblAuditTax.TabIndex = 93
        Me.lblAuditTax.Text = "Audit Tax"
        Me.lblAuditTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(436, 33)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(323, 36)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblToPeriod.TabIndex = 90
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(121, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboFromPeriod.TabIndex = 0
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblFromPeriod.TabIndex = 87
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPAYE
        '
        Me.lblPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYE.Location = New System.Drawing.Point(8, 62)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(107, 15)
        Me.lblPAYE.TabIndex = 81
        Me.lblPAYE.Text = "P.A.Y.E."
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPAYE.DropDownWidth = 180
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(121, 60)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(167, 21)
        Me.cboPAYE.TabIndex = 2
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(294, 168)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 56
        Me.objbtnSearchEmployee.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(121, 168)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(167, 21)
        Me.cboEmployee.TabIndex = 9
        Me.cboEmployee.Visible = False
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 170)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(107, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEmployee.Visible = False
        '
        'frmP10Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(670, 567)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmP10Report"
        Me.Text = "frmP10Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objelLineOptional As eZee.Common.eZeeLine
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchFringeBenefit As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchAuditTax As eZee.Common.eZeeGradientButton
    Friend WithEvents cboFringeBenefit As System.Windows.Forms.ComboBox
    Friend WithEvents lblFringeBenefit As System.Windows.Forms.Label
    Friend WithEvents cboAuditTax As System.Windows.Forms.ComboBox
    Friend WithEvents lblAuditTax As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
End Class

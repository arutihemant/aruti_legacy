'************************************************************************************************************************************
'Class Name : clsNHIFReport.vb
'Purpose    :
'Date       :19/08/2013
'Written By : Pinkal Jariwala.
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala.
''' </summary>

Public Class clsNHIFReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsNHIFReport"
    Private mstrReportId As String = enArutiReport.KN_NHIFReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty
    Private mintNonPayrollMembershipId As Integer = -1
    Private mstrNonPayrollMembershipName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mbIncludeInactiveEmp As Boolean = True
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""
    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mstrAdvance_Filter As String = ""
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    Private mstrAddress As String

    'Anjan [23 February 2015] -- Start
    'ENHANCEMENT : Implementing NHIF report changes,requested by Rutta.
    Private mblnShowRemark As Boolean = False
    'Anjan [23 February 2015] -- End


    'SHANI (02 JUL 2015) -- Start
    'AKFK Enhancement - Provide Ignore Zero option on NSSF Report and NHIF Report
    Private mblnIgnoreZero As Boolean = False
    'SHANI (02 JUL 2015) -- End 

    'Sohail (28 Jan 2016) -- Start
    'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
    Private mstrPeriodCode As String = String.Empty
    Private mblnShowReportNameOnReport As Boolean = False
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'Sohail (28 Jan 2016) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _NonPayrollMembershipName() As String
        Set(ByVal value As String)
            mstrNonPayrollMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mbIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Anjan [23 February 2015] -- Start
    'ENHANCEMENT : Implementing NHIF report changes,requested by Rutta.
    Public WriteOnly Property _ShowRemark() As Boolean
        Set(ByVal value As Boolean)
            mblnShowRemark = value
        End Set
    End Property
    'Anjan [23 February 2015] -- End


    'SHANI (02 JUL 2015) -- Start
    'AKFK Enhancement - Provide Ignore Zero option on NSSF Report and NHIF Report
    Public WriteOnly Property _IgnoreZero() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZero = value
        End Set
    End Property
    'SHANI (02 JUL 2015) -- End 

    'Sohail (28 Jan 2016) -- Start
    'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
    Public WriteOnly Property _PeriodCode() As String
        Set(ByVal value As String)
            mstrPeriodCode = value
        End Set
    End Property

    Public WriteOnly Property _ShowReportNameOnReport() As Boolean
        Set(ByVal value As Boolean)
            mblnShowReportNameOnReport = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'Sohail (28 Jan 2016) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = ""
            mintNonPayrollMembershipId = 0
            mstrNonPayrollMembershipName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mbIncludeInactiveEmp = True
            mintReportId = -1
            mstrReportTypeName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            mstrPeriodCode = ""
            mblnShowReportNameOnReport = False
            'Sohail (28 Jan 2016) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    objRpt = Generate_DetailReport()


        '    If Not IsNothing(objRpt) Then
        '        Dim intArrayColumnWidth As Integer() = {130, 200, 100, 110, 110, 110, 120, 150}
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim row As ExcelWriter.WorksheetRow
        '        Dim wcell As WorksheetCell
        '        If mdtTableExcel IsNot Nothing Then

        '            If Company._Object._Countryunkid = 112 Then 'Kenya
        '                mstrReportTypeName = Me._ReportName

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Employer Name : "), "s9bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(Company._Object._Name & " " & mstrAddress, "s9w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "Contributions Period : "), "s9bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell(mstrPeriodName, "s9w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayHeader.Add(row)
        '                '--------------------

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s9w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                rowsArrayHeader.Add(row)
        '                '--------------------
        '            End If

        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)

        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)


            If Not IsNothing(objRpt) Then
                'Sohail (28 Jan 2016) -- Start
                'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                'Dim intArrayColumnWidth As Integer() = {130, 200, 100, 110, 110, 110, 120, 150}
                Dim intArrayColumnWidth As Integer() = {130, 100, 100, 110, 110, 110, 120, 150}
                'Sohail (28 Jan 2016) -- End
                Dim rowsArrayHeader As New ArrayList
                Dim row As ExcelWriter.WorksheetRow
                Dim wcell As WorksheetCell
                Dim objCompany As New clsCompany_Master
                objCompany._Companyunkid = xCompanyUnkid

                If menExportAction <> enExportAction.ExcelXLSX Then
                If mdtTableExcel IsNot Nothing Then

                    If objCompany._Countryunkid = 112 Then  'KENYA
                        mstrReportTypeName = Me._ReportName
                        'Sohail (28 Jan 2016) -- Start
                        'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                        If mblnShowReportNameOnReport = True Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(Me._ReportName, "s12bw")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayHeader.Add(row)
                            '--------------------

                            row = New WorksheetRow()
                            wcell = New WorksheetCell("", "s9w")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayHeader.Add(row)
                            '--------------------
                        End If

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Employer Code : "), "s9bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Ppfno, "s9w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayHeader.Add(row)
                        '--------------------
                        'Sohail (28 Jan 2016) -- End

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 1, "Employer Name : "), "s9bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Company._Object._Name & " " & mstrAddress, "s9w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 2, "MONTH OF CONTRIBUTION : "), "s9bw")
                        row.Cells.Add(wcell)

                        'Sohail (28 Jan 2016) -- Start
                        'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                        'wcell = New WorksheetCell(mstrPeriodName, "s9w")
                        wcell = New WorksheetCell(mstrPeriodCode, "s9w")
                        'Sohail (28 Jan 2016) -- End
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                        rowsArrayHeader.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s9w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        rowsArrayHeader.Add(row)
                        '--------------------
                    End If

                End If

                'Sohail (28 Jan 2016) -- Start
                'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                'Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, False, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)
                'Sohail (28 Jan 2016) -- End
                ElseIf menExportAction = enExportAction.ExcelXLSX Then

                    Using package As New OfficeOpenXml.ExcelPackage()
                        Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                        worksheet.Cells(1, 1).Value = Language.getMessage(mstrModuleName, 100, "Employer Code").Trim()
                        worksheet.Cells(1, 2).Value = objCompany._Ppfno.ToString().Trim()
                        worksheet.Cells(2, 1).Value = Language.getMessage(mstrModuleName, 101, "Employer Name").Trim()
                        worksheet.Cells(2, 2).Value = objCompany._Name.ToString().Trim()
                        Dim objPrd As New clscommom_period_Tran : objPrd._Periodunkid(xDatabaseName) = mintPeriodId
                        worksheet.Cells(3, 1).Value = Language.getMessage(mstrModuleName, 102, "MONTH OF CONTRIBUTION").Trim()
                        worksheet.Cells(3, 2).Value = Format(objPrd._Start_Date, "yyyy-MM").Trim()
                        Dim xTable As DataTable = mdtTableExcel.Copy
                        For Each iCol As DataColumn In xTable.Columns
                            xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                        Next
                        xTable.Rows.Add(xTable.NewRow())
                        xTable.Rows(xTable.Rows.Count - 1).Item(xTable.Columns.Count - 2) = Language.getMessage(mstrModuleName, 103, "TOTAL").Trim()
                        xTable.Rows(xTable.Rows.Count - 1).Item(xTable.Columns.Count - 1) = xTable.Compute("SUM(" & xTable.Columns(xTable.Columns.Count - 1).ColumnName & ")", "")
                        worksheet.Cells("A5").LoadFromDataTable(xTable, True)
                        worksheet.Cells("A1:XFD").AutoFilter = False
                        worksheet.Cells(6, xTable.Columns.Count, (xTable.Rows.Count - 1) + 6, xTable.Columns.Count).Style.Numberformat.Format = GUI.fmtCurrency
                        worksheet.Cells((xTable.Rows.Count - 1) + 6, xTable.Columns.Count - 1).Style.Font.Bold = True
                        worksheet.Cells((xTable.Rows.Count - 1) + 6, xTable.Columns.Count).Style.Font.Bold = True
                        worksheet.Cells.AutoFitColumns(0)

                        If Me._ReportName.Contains("/") = True Then
                            Me._ReportName = Me._ReportName.Replace("/", "_")
                        End If

                        If Me._ReportName.Contains("\") Then
                            Me._ReportName = Me._ReportName.Replace("\", "_")
                        End If

                        If Me._ReportName.Contains(":") Then
                            Me._ReportName = Me._ReportName.Replace(":", "_")
                        End If
                        Dim strExportFileName As String = ""
                        strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                        xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                        Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                        package.SaveAs(fi)

                    End Using

                    If IO.File.Exists(xExportReportPath) Then
                        If xOpenReportAfterExport Then
                            Process.Start(xExportReportPath)
                        End If
                    End If
                    


                    'Dim xTable As DataTable = mdtTableExcel.Clone()
                    'xTable.Columns(xTable.Columns.Count - 1).DataType = GetType(System.String)

                    'xTable.Rows.Add(xTable.NewRow())
                    'xTable.Rows(xTable.Rows.Count - 1).Item(0) = Language.getMessage(mstrModuleName, 100, "Employer Code").Trim()
                    'xTable.Rows(xTable.Rows.Count - 1).Item(1) = objCompany._Ppfno.ToString().Trim()

                    'xTable.Rows.Add(xTable.NewRow())
                    'xTable.Rows(xTable.Rows.Count - 1).Item(0) = Language.getMessage(mstrModuleName, 101, "Employer Name").Trim()
                    'xTable.Rows(xTable.Rows.Count - 1).Item(1) = objCompany._Name.ToString().Trim()

                    'xTable.Rows.Add(xTable.NewRow())
                    'xTable.Rows(xTable.Rows.Count - 1).Item(0) = Language.getMessage(mstrModuleName, 102, "MONTH OF CONTRIBUTION").Trim()
                    'Dim objPrd As New clscommom_period_Tran
                    'objPrd._Periodunkid(xDatabaseName) = mintPeriodId
                    'xTable.Rows(xTable.Rows.Count - 1).Item(1) = Format(objPrd._Start_Date, "yyyy-MM").Trim()
                    'objPrd = Nothing

                    'xTable.Rows.Add(xTable.NewRow())
                    'For Each iCol As DataColumn In xTable.Columns
                    '    xTable.Rows(xTable.Rows.Count - 1).Item(iCol.Ordinal) = iCol.Caption.Trim()
                    'Next
                    'Dim xTotal, xValue As Decimal
                    'xTotal = 0 : xValue = 0
                    'For Each iRow As DataRow In mdtTableExcel.Rows
                    '    xTable.Rows.Add(xTable.NewRow())
                    '    For Each iCol As DataColumn In xTable.Columns
                    '        xTable.Rows(xTable.Rows.Count - 1).Item(iCol.Ordinal) = iRow(iCol.Ordinal)
                    '    Next
                    '    Try
                    '        Decimal.TryParse(xTable.Rows(xTable.Rows.Count - 1).Item(xTable.Columns.Count - 1), xValue)
                    '        xTotal = xTotal + xValue
                    '    Catch ex As Exception

                    '    End Try
                    'Next
                    'xTable.Rows.Add(xTable.NewRow())
                    'xTable.Rows(xTable.Rows.Count - 1).Item(xTable.Columns.Count - 2) = Language.getMessage(mstrModuleName, 103, "TOTAL").Trim()                    
                    'xTable.Rows(xTable.Rows.Count - 1).Item(xTable.Columns.Count - 1) = xTotal

                    'If System.IO.Directory.Exists(xExportReportPath) = False Then
                    '    Dim dig As New System.Windows.Forms.FolderBrowserDialog
                    '    dig.Description = "Select Folder Where to export report."

                    '    If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    '        xExportReportPath = dig.SelectedPath
                    '    Else
                    '        Exit Sub
                    '    End If
                    'End If

                    'If Me._ReportName.Contains("/") = True Then
                    '    Me._ReportName = Me._ReportName.Replace("/", "_")
                    'End If

                    'If Me._ReportName.Contains("\") Then
                    '    Me._ReportName = Me._ReportName.Replace("\", "_")
                    'End If

                    'If Me._ReportName.Contains(":") Then
                    '    Me._ReportName = Me._ReportName.Replace(":", "_")
                    'End If

                    'Dim ds As New DataSet
                    'ds.Tables.Add(xTable)
                    'OpenXML_Export(xExportReportPath, ds, False)
            End If


                objCompany = Nothing
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            Dim strENameQ As String = ""
            If mblnFirstNamethenSurname = True Then
                strENameQ &= ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            Else
                strENameQ &= ", ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName " & _
                             ", ISNULL(hremployee_master.firstname,'') AS firstname " & _
                             ", ISNULL(hremployee_master.surname,'') AS surname "
            End If
            'Sohail (28 Jan 2016) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ISNULL(CAmount,0) AS employercontribution " & _
                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+ISNULL(CAmount,0) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                             ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " "
                'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "       LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                 ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                           "FROM    prpayrollprocess_tran " & _
                                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                   "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                   "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                   WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                           "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                           "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                           "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                        "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                        "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                        "AND hrmembership_master.membershipunkid = @MemId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                      strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
                    'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                    StrQ &= mstrAnalysis_Join

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                StrQ &= "ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                StrQ = "SELECT hremployee_master.employeecode AS employeecode " & _
                             strENameQ & _
                             ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount,0) AS employercontribution " & _
                             ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +  ISNULL(CAmount,0)) AS Amount " & _
                             ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS EmpId " & _
                             ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                             ", '' AS OtherMembership " & _
                             ", '' AS Remarks " & _
                              ", 0 As NonPayrollMembership " & _
                       "FROM    prpayrollprocess_tran " & _
                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                               "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                               "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                "AND prpayment_tran.countryunkid = " & mintCountryId & " "
                'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                             ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                       "FROM    prpayrollprocess_tran " & _
                                               "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                               "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                               "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                               "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "               WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                               "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                               "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                    "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "AND hrmembership_master.membershipunkid = @MemId " & _
                                    "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0)+  ISNULL(CAmount,0)) AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                             "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "
                'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                     "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                     "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                     "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                     "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                     "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                            "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                            "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                            "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "            ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                         "WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                         "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                         "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                         "AND hrmembership_master.membershipunkid = @MemId " & _
                                         "AND prpayment_tran.periodunkid = @PeriodId "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "UNION ALL " & _
                        "SELECT hremployee_master.employeecode AS employeecode " & _
                              strENameQ & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno,'') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount,0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) +   ISNULL(CAmount,0))  AS Amount " & _
                              ", ISNULL(cfcommon_period_tran.period_name,'') AS PeriodName " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(cfcommon_master.name, '') AS MemshipCategory " & _
                              ", '' AS OtherMembership " & _
                              ", '' AS Remarks " & _
                               ", 0 As NonPayrollMembership " & _
                        "FROM    prpayrollprocess_tran " & _
                                "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0" & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 " & _
                                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrmembership_master.membershipcategoryunkid " & _
                                                "AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & " " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                                "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "
                'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End


                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")),0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                    "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                    "AND ISNULL(cfcommon_period_tran.isactive,0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "              ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                          " WHERE   ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                          " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                          " AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                          " AND hrmembership_master.membershipunkid = @MemId " & _
                                          " AND prpayment_tran.periodunkid IS NULL "

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                If mintNonPayrollMembershipId > 0 Then

                    StrQ &= "UNION ALL " & _
                                "SELECT hremployee_master.employeecode AS employeecode " & _
                                     strENameQ & _
                                      ",'' AS MemshipNo " & _
                                      ", 0 AS empcontribution " & _
                                      ", 0 AS employercontribution " & _
                                      ", 0 AS Amount " & _
                                      ", '' AS PeriodName " & _
                                      ", hremployee_master.employeeunkid AS EmpId " & _
                                      ", '' AS MemshipCategory " & _
                                      ",ISNULL(hremployee_meminfo_tran.membershipno,'') AS OtherMembership " & _
                                      ", '' AS Remarks " & _
                                      ", 1 AS NonPayrollMembership  " & _
                                      "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                        "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                        "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid "
                    'Sohail (28 Jan 2016) - [", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _] = [strENameQ]

                    StrQ &= mstrAnalysis_Join

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END

                    StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                    If mstrAdvance_Filter.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrAdvance_Filter
                    End If

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''Pinkal (15-Oct-2014) -- Start
                    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    StrQ &= UserAccessLevel._AccessLevelFilterString
                    'End If
                    ''Pinkal (15-Oct-2014) -- End

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                    'S.SANDEEP [04 JUN 2015] -- END

                End If

                StrQ &= "ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim mdtData As DataTable = New DataView(dsList.Tables(0), "", "employeecode, Amount DESC", DataViewRowState.CurrentRows).ToTable
            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn81Total As Decimal


            For Each dtRow As DataRow In mdtData.Rows
                StrECode = dtRow.Item("employeecode").ToString.Trim

                If StrPrevECode <> StrECode Then
                    'SHANI (02 JUL 2015) -- Start
                    'AKFK Enhancement - Provide Ignore Zero option on NSSF Report and NHIF Report
                    If mblnIgnoreZero = True Then
                        If CDec(dtRow.Item("Amount")) = 0 Then Continue For
                    End If
                    'SHANI (02 JUL 2015) -- End
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    Else
                        rpt_Row.Item("Column5") = ""
                    End If

                    iCnt += 1
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                    rpt_Row.Item("Column5") = dtRow.Item("OtherMembership")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode

                    Continue For
                End If

                rpt_Row.Item("Column1") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")
                'Sohail (28 Jan 2016) -- Start
                'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                rpt_Row.Item("Column6") = dtRow.Item("surname").ToString
                rpt_Row.Item("Column7") = dtRow.Item("firstname").ToString
                'Sohail (28 Jan 2016) -- End

                If CBool(dtRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row.Item("Column3") = ""
                Else
                    'Sohail (28 Jan 2016) -- Start
                    'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                    'rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                    If dtRow.Item("MemshipNo").ToString.Trim = "0" Then
                        rpt_Row.Item("Column3") = ""
                    Else
                        rpt_Row.Item("Column3") = dtRow.Item("MemshipNo")
                    End If
                    'Sohail (28 Jan 2016) -- End
                End If

                rpt_Row.Item("Column81") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = dtRow.Item("Remarks").ToString

                decColumn81Total = decColumn81Total + CDec(dtRow.Item("Amount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                StrPrevECode = StrECode
            Next

            mstrAddress = Company._Object._Address1 & " " & Company._Object._Address2
            mstrAddress &= " " & Company._Object._City_Name & " " & Company._Object._State_Name & " " & Company._Object._Post_Code_No & " " & Company._Object._Country_Name

            objRpt = New ArutiReport.Designer.rptKenyaNHIF_Report
            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 1, "Employer Name : "))
            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            'Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)
            objRpt.DataDefinition.FormulaFields("frmlAddress").Text = "'" & Company._Object._Name & "<BR>" & mstrAddress & "'"
            'Sohail (28 Jan 2016) -- End

            'Anjan [23 February 2015] -- Start
            'ENHANCEMENT : Implementing NHIF report changes,requested by Rutta.
            Call ReportFunction.TextChange(objRpt, "txtAddress", mstrAddress)
            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            'Call ReportFunction.TextChange(objRpt, "lblEmployerNo", Language.getMessage(mstrModuleName, 10, "Employer Number : "))
            Call ReportFunction.TextChange(objRpt, "lblEmployerNo", Language.getMessage(mstrModuleName, 10, "Employer Code : "))
            'Sohail (28 Jan 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtEmployerNumber", Company._Object._Ppfno)
            If mblnShowRemark = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtRemarks", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column41", True)

                Call ReportFunction.EnableSuppress(objRpt, "Line16", True)
                Call ReportFunction.EnableSuppress(objRpt, "Line23", True)
                Call ReportFunction.EnableSuppress(objRpt, "Line32", True)


            End If


            'Anjan [23 February 2015] -- End



            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            'Call ReportFunction.TextChange(objRpt, "lblMonthOfContribution", Language.getMessage(mstrModuleName, 2, "Contributions Period : "))
            'Call ReportFunction.TextChange(objRpt, "txtMonthofContribution", mstrPeriodName)
            Call ReportFunction.TextChange(objRpt, "lblMonthOfContribution", Language.getMessage(mstrModuleName, 2, "MONTH OF CONTRIBUTION: "))
            Call ReportFunction.TextChange(objRpt, "txtMonthofContribution", mstrPeriodCode)
            'Sohail (28 Jan 2016) -- End

            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            'Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "Payroll Number"))
            'Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 5, "Employee's Name"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 4, "PAYROLL NO"))
            Call ReportFunction.TextChange(objRpt, "txtFirstName", Language.getMessage(mstrModuleName, 11, "FIRST NAME"))
            Call ReportFunction.TextChange(objRpt, "txtLastName", Language.getMessage(mstrModuleName, 12, "LAST NAME"))
            'Sohail (28 Jan 2016) -- End
            Call ReportFunction.TextChange(objRpt, "txtNHIFNo", Language.getMessage(mstrModuleName, 6, "NHIF NO"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 7, "AMOUNT"))
            Call ReportFunction.TextChange(objRpt, "txtName", mstrNonPayrollMembershipName)
            Call ReportFunction.TextChange(objRpt, "txtRemarks", Language.getMessage(mstrModuleName, 8, "REMARKS"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtTotalAmount", Format(decColumn81Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 9, "Total"))


            Dim intColIndex As Integer = -1
            If menExportAction = enExportAction.ExcelExtra Or menExportAction = enExportAction.ExcelXLSX Then
                mdtTableExcel = rpt_Data.Tables(0)

                'Sohail (28 Jan 2016) -- Start
                'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                'mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 4, "Payroll Number")
                mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 4, "PAYROLL NO")
                'Sohail (28 Jan 2016) -- End
                intColIndex += 1
                mdtTableExcel.Columns("Column1").SetOrdinal(intColIndex)

                'Sohail (28 Jan 2016) -- Start
                'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
                'mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 5, "Employee's Name")
                'intColIndex += 1
                'mdtTableExcel.Columns("Column2").SetOrdinal(intColIndex)
                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 12, "LAST NAME")
                intColIndex += 1
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 11, "FIRST NAME")
                intColIndex += 1
                mdtTableExcel.Columns("Column7").SetOrdinal(intColIndex)
                'Sohail (28 Jan 2016) -- End

                mdtTableExcel.Columns("Column5").Caption = mstrNonPayrollMembershipName
                intColIndex += 1
                mdtTableExcel.Columns("Column5").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 6, "NHIF NO")
                intColIndex += 1
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIndex)

                mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 7, "AMOUNT")
                intColIndex += 1
                mdtTableExcel.Columns("Column81").SetOrdinal(intColIndex)

                If mblnShowRemark = True Then 'Sohail (28 Jan 2016)
                    mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 8, "REMARKS")
                    intColIndex += 1
                    mdtTableExcel.Columns("Column4").SetOrdinal(intColIndex)
                End If 'Sohail (28 Jan 2016)

                For i = intColIndex + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIndex + 1)
                Next

            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employer Name :")
            Language.setMessage(mstrModuleName, 2, "MONTH OF CONTRIBUTION :")
            Language.setMessage(mstrModuleName, 4, "PAYROLL NO")
            Language.setMessage(mstrModuleName, 6, "NHIF NO")
            Language.setMessage(mstrModuleName, 7, "AMOUNT")
            Language.setMessage(mstrModuleName, 8, "REMARKS")
            Language.setMessage(mstrModuleName, 9, "Total")
            Language.setMessage(mstrModuleName, 10, "Employer Code :")
            Language.setMessage(mstrModuleName, 11, "FIRST NAME")
            Language.setMessage(mstrModuleName, 12, "LAST NAME")
            Language.setMessage(mstrModuleName, 2, "MONTH OF CONTRIBUTION:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

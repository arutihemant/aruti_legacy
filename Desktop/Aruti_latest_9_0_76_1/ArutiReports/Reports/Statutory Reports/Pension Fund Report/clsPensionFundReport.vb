'************************************************************************************************************************************
'Class Name : clsStatutoryContributionReport.vb
'Purpose    :
'Date       :22/11/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsPensionFundReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPensionFundReport"
    Private mstrReportId As String = enArutiReport.PensionFundReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
        Call Create_OnMonthReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintEmpId As Integer = -1
    Private mstrEmpName As String = String.Empty
    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    'Private mintMembershipId As Integer = 1
    'Private mstrMembershipName As String = String.Empty
    Private mstrMembershipIDs As String = String.Empty
    Private mstrMembershipNAMEs As String = String.Empty
    'Sohail (18 May 2013) -- End
    Private mintNonPayrollMembershipId As Integer = -1 'Sohail (18 Jul 2013)
    Private mintOtherEarningTranId As Integer = 0 'Sohail (28 Aug 2013)
    'Sohail (27 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Private mintEmpHeadTypeId As Integer = -1
    'Private mintEmpTypeOfId As Integer = -1
    'Private mintEmprHeadTypeId As Integer = -1
    'Private mintEmprTypeOfId As Integer = -1
    'Sohail (27 Aug 2012) -- End
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mstrGrossPayFrom As String = String.Empty
    Private mstrGrossPayTo As String = String.Empty
    'Sohail (27 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Private mstrEmpContribFrom As String = String.Empty
    'Private mstrEmpContribTo As String = String.Empty
    'Private mstrEmplrContribFrom As String = String.Empty
    'Private mstrEmplrContribTo As String = String.Empty
    'Sohail (27 Aug 2012) -- End
    Private mintEmplrNumTypeId As Integer = -1
    Private mstrEmplrNumTypeName As String = String.Empty
    Private mstrEmplrNumber As String = String.Empty
    Private mstrMembershipNo As String = String.Empty
    Private mblnShowBasicSalary As Boolean = False
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    'Sohail (31 Mar 2014) -- Start
    'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
    Private mblnShowEmployeeCode As Boolean = True
    Private mblnShowOtherMembershipNo As Boolean = True
    'Sohail (31 Mar 2014) -- End

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'Sohail (12 Jan 2013) -- Start
    'TRA - ENHANCEMENT
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Sohail (12 Jan 2013) -- End

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    'Sohail (18 Mar 2013) -- End
    Private mstrReportBy_Name As String = String.Empty 'Hemant (19 Jul 2019)


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    'Pinkal (02-May-2013) -- End


    'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
    Private mintEmplrContrib1Id As Integer = 0
    Private mintEmplrContrib2Id As Integer = 0
    Private mstrEmplrContrib1_Text As String = String.Empty
    Private mstrEmplrContrib2_Text As String = String.Empty
    'S.SANDEEP [ 20 NOV 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Anjan [12 November 2014] -- Start
    'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
    Private mblnDoNotShowBasic_GrossSalary As Boolean = False
    'Anjan [12 November 2014] -- End

    Private mblnShowGroupByCostCenter As Boolean = False 'Sohail (18 Apr 2014)

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtDatabase_Start_Date As Date = FinancialYear._Object._Database_Start_Date
    'Sohail (21 Aug 2015) -- End


    'Anjan [07 October 2016] -- Start
    'ENHANCEMENT : Include Ignore Zero Value.
    Private mblnIgnoreZeroValue As Boolean = False
    'Anjan [07 October 2016] -- End


#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    'Public WriteOnly Property _MembershipId() As Integer
    '    Set(ByVal value As Integer)
    '        mintMembershipId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _MembershipName() As String
    '    Set(ByVal value As String)
    '        mstrMembershipName = value
    '    End Set
    'End Property

    Public WriteOnly Property _MembershipIDs() As String
        Set(ByVal value As String)
            mstrMembershipIDs = value
        End Set
    End Property

    Public WriteOnly Property _MembershipNAMEs() As String
        Set(ByVal value As String)
            mstrMembershipNAMEs = value
        End Set
    End Property
    'Sohail (18 May 2013) -- End

    'Sohail (18 Jul 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _NonPayrollMembershipId() As Integer
        Set(ByVal value As Integer)
            mintNonPayrollMembershipId = value
        End Set
    End Property
    'Sohail (18 Jul 2013) -- End

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    'Sohail (28 Aug 2013) -- End

    'Sohail (27 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Public WriteOnly Property _EmpHeadTypeId() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmpHeadTypeId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmpTypeOfId() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmpTypeOfId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmprHeadTypeId() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmprHeadTypeId = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmprTypeOfId() As Integer
    '    Set(ByVal value As Integer)
    '        mintEmprTypeOfId = value
    '    End Set
    'End Property
    'Sohail (27 Aug 2012) -- End

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayFrom() As String
        Set(ByVal value As String)
            mstrGrossPayFrom = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayTo() As String
        Set(ByVal value As String)
            mstrGrossPayTo = value
        End Set
    End Property

    'Sohail (27 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Public WriteOnly Property _EmpContribFrom() As String
    '    Set(ByVal value As String)
    '        mstrEmpContribFrom = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmpContribTo() As String
    '    Set(ByVal value As String)
    '        mstrEmpContribTo = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmplrContribFrom() As String
    '    Set(ByVal value As String)
    '        mstrEmplrContribFrom = value
    '    End Set
    'End Property

    'Public WriteOnly Property _EmplrContribTo() As String
    '    Set(ByVal value As String)
    '        mstrEmplrContribTo = value
    '    End Set
    'End Property
    'Sohail (27 Aug 2012) -- End

    Public WriteOnly Property _EmplrNumTypeId() As Integer
        Set(ByVal value As Integer)
            mintEmplrNumTypeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumTypeName() As String
        Set(ByVal value As String)
            mstrEmplrNumTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmplrNumber() As String
        Set(ByVal value As String)
            mstrEmplrNumber = value
        End Set
    End Property

    Public WriteOnly Property _MembershipNo() As String
        Set(ByVal value As String)
            mstrMembershipNo = value
        End Set
    End Property

    Public WriteOnly Property _ShowBasicSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowBasicSalary = value
        End Set
    End Property

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (18 Mar 2013) -- End


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property
    'Pinkal (02-May-2013) -- End

    'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
    Public WriteOnly Property _EmplrContrib1Id() As Integer
        Set(ByVal value As Integer)
            mintEmplrContrib1Id = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContrib2Id() As Integer
        Set(ByVal value As Integer)
            mintEmplrContrib2Id = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContrib1_Text() As String
        Set(ByVal value As String)
            mstrEmplrContrib1_Text = value
        End Set
    End Property

    Public WriteOnly Property _EmplrContrib2_Text() As String
        Set(ByVal value As String)
            mstrEmplrContrib2_Text = value
        End Set
    End Property
    'S.SANDEEP [ 20 NOV 2013 ] -- END

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Sohail (31 Mar 2014) -- Start
    'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
    Public WriteOnly Property _ShowEmployeeCode() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployeeCode = value
        End Set
    End Property

    Public WriteOnly Property _ShowOtherMembershipNo() As Boolean
        Set(ByVal value As Boolean)
            mblnShowOtherMembershipNo = value
        End Set
    End Property
    'Sohail (31 Mar 2014) -- End

    'Sohail (18 Apr 2014) -- Start
    'Enhancement - Show Group by Cost Center option on Pension Fund Report
    Public WriteOnly Property _ShowGroupByCostCenter() As Boolean
        Set(ByVal value As Boolean)
            mblnShowGroupByCostCenter = value
        End Set
    End Property
    'Sohail (18 Apr 2014) -- End

    'Anjan [12 November 2014] -- Start
    'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
    Public WriteOnly Property _DoNotShowBasic_GrossSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnDoNotShowBasic_GrossSalary = value
        End Set
    End Property
    'Anjan [12 November 2014] -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public WriteOnly Property _Database_Start_Date() As Date
        Set(ByVal value As Date)
            mdtDatabase_Start_Date = value
        End Set
    End Property
    'Sohail (21 Aug 2015) -- End

    'Anjan [07 October 2016] -- Start
    'ENHANCEMENT : Include Ignore Zero Value.
    Public WriteOnly Property _IgnoreZeroValue() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZeroValue = value
        End Set
    End Property
    'Anjan [07 October 2016] -- End

    'Hemant (19 Jul 2019) -- Start
    'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
    Public WriteOnly Property _ReportBy_Name() As String
        Set(ByVal value As String)
            mstrReportBy_Name = value
        End Set
    End Property
    'Hemant (19 Jul 2019) -- End
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'mintMembershipId = 0
            'mstrMembershipName = ""
            mstrMembershipIDs = ""
            mstrMembershipNAMEs = ""
            'Sohail (18 May 2013) -- End
            mintNonPayrollMembershipId = 0  'Sohail (18 Jul 2013)
            mintOtherEarningTranId = 0 'Sohail (28 Aug 2013)
            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'mintEmpHeadTypeId = 0
            'mintEmpTypeOfId = 0
            'mintEmprHeadTypeId = 0
            'mintEmprTypeOfId = 0
            'Sohail (27 Aug 2012) -- End
            mintPeriodId = 0
            mstrPeriodName = ""
            mstrGrossPayFrom = ""
            mstrGrossPayTo = ""
            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'mstrEmpContribFrom = ""
            'mstrEmpContribTo = ""
            'mstrEmplrContribFrom = ""
            'mstrEmplrContribTo = ""
            'Sohail (27 Aug 2012) -- End
            mintEmplrNumTypeId = 0
            mstrEmplrNumTypeName = ""
            mstrEmplrNumber = ""
            mstrMembershipNo = ""
            mblnShowBasicSalary = False
            mintReportId = -1
            mstrReportTypeName = ""
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'Sohail (31 Mar 2014) -- Start
            'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
            mblnShowEmployeeCode = True
            mblnShowOtherMembershipNo = True
            'Sohail (31 Mar 2014) -- End

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Sohail (18 Mar 2013) -- End


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""
            'Pinkal (02-May-2013) -- End

            'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
            mintEmplrContrib1Id = 0
            mintEmplrContrib2Id = 0
            mstrEmplrContrib1_Text = String.Empty
            mstrEmplrContrib2_Text = String.Empty
            'S.SANDEEP [ 20 NOV 2013 ] -- END
            mblnShowGroupByCostCenter = False 'Sohail (18 Apr 2014)


            'Anjan [12 November 2014] -- Start
            'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
            mblnDoNotShowBasic_GrossSalary = False
            'Anjan [12 November 2014] -- End

            'Anjan [07 October 2016] -- Start
            'ENHANCEMENT : Include Ignore Zero Value.
            mblnIgnoreZeroValue = False
            'Anjan [07 October 2016] -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)'Sohail (18 May 2013)
            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@EHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpHeadTypeId)
            'objDataOperation.AddParameter("@ETypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpTypeOfId)
            'objDataOperation.AddParameter("@CHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprHeadTypeId)
            'objDataOperation.AddParameter("@CTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmprTypeOfId)
            'Sohail (27 Aug 2012) -- End
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            'S.SANDEEP [ 20 NOV 2013 ] -- START
            objDataOperation.AddParameter("@ETrnHead1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmplrContrib1Id)
            objDataOperation.AddParameter("@ETrnHead2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmplrContrib2Id)
            'S.SANDEEP [ 20 NOV 2013 ] -- END



            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (28 Aug 2013) -- End

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Membership :") & " " & mstrMembershipName & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Membership :") & " " & mstrMembershipNAMEs & " "
            'Sohail (18 May 2013) -- End

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= "AND EmpId = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Employee :") & " " & mstrEmpName & " "
            End If

            If mstrGrossPayFrom.Trim <> "" AndAlso mstrGrossPayTo.Trim <> "" Then
                If Convert.ToDecimal(mstrGrossPayFrom) >= 0 AndAlso Convert.ToDecimal(mstrGrossPayTo) >= 0 Then
                    objDataOperation.AddParameter("@GrossPayFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrGrossPayFrom))
                    objDataOperation.AddParameter("@GrossPayTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrGrossPayTo))
                    Me._FilterQuery &= "AND GrossPay BETWEEN @GrossPayFrom AND @GrossPayTo "
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Gross Pay From : ") & " " & mstrGrossPayFrom & " " & _
                                                Language.getMessage(mstrModuleName, 11, "to") & " " & mstrGrossPayTo & " "
                End If
            End If

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'If mstrEmpContribFrom.Trim <> "" AndAlso mstrEmpContribTo.Trim <> "" Then
            '    If Convert.ToDecimal(mstrEmpContribFrom) >= 0 AndAlso Convert.ToDecimal(mstrEmpContribTo) >= 0 Then
            '        objDataOperation.AddParameter("@EmpContribFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmpContribFrom))
            '        objDataOperation.AddParameter("@EmpContribTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmpContribTo))
            '        Me._FilterQuery &= "AND EmpContrib BETWEEN @EmpContribFrom AND @EmpContribTo "
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Employee Contribution From : ") & " " & mstrEmpContribFrom & " " & _
            '                                    Language.getMessage(mstrModuleName, 11, "to") & " " & mstrEmpContribTo & " "
            '    End If
            'End If

            'If mstrEmplrContribFrom.Trim <> "" AndAlso mstrEmplrContribTo.Trim <> "" Then
            '    If Convert.ToDecimal(mstrEmplrContribFrom) >= 0 AndAlso Convert.ToDecimal(mstrEmplrContribTo) >= 0 Then
            '        objDataOperation.AddParameter("@EmplrContribFrom", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmplrContribFrom))
            '        objDataOperation.AddParameter("@EmplrContribTo", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, Convert.ToDecimal(mstrEmplrContribTo))
            '        Me._FilterQuery &= "AND EmprContrib BETWEEN @EmplrContribFrom AND @EmplrContribTo "
            '        Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Employer Contribution From : ") & " " & mstrEmpContribFrom & " " & _
            '                                    Language.getMessage(mstrModuleName, 11, "to") & " " & mstrEmpContribTo & " "
            '    End If
            'End If
            'Sohail (27 Aug 2012) -- End


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If mintReportId = 1 AndAlso mstrExchangeRate.Trim.Length > 0 Then 'PERIOD WISE
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Exchange Rate : ") & mstrExchangeRate
            End If

            'Pinkal (02-May-2013) -- End

            'Hemant (19 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT#3984(Eko Supreme) - Allocation Captions Not Showing on Pension.
            If mstrReport_GroupName.Trim.Length > 0 AndAlso mstrViewByName.Trim.Length > 0 Then
                Me._FilterTitle &= mstrReport_GroupName & " " & mstrViewByName & " "
            End If
            'Hemant (19 Jul 2019) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Order By : ") & " " & Me.OrderByDisplay
                'S.SANDEEP [ 05 JULY 2011 ] -- START
                'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
                'Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                'S.SANDEEP [ 05 JULY 2011 ] -- END 
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    'Sohail (12 Jan 2013) -- Start
        '    'TRA - ENHANCEMENT
        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing
        '    'Sohail (12 Jan 2013) -- End

        '    'S.SANDEEP [ 20 NOV 2013 ] -- START
        '    If Company._Object._Countryunkid = 129 Then
        '        objRpt = Generate_DetailReport_Malawi()
        '    Else
        '        objRpt = Generate_DetailReport()
        '    End If
        '    'S.SANDEEP [ 20 NOV 2013 ] -- END


        '    If Not IsNothing(objRpt) Then

        '        'Sohail (12 Jan 2013) -- Start
        '        'TRA - ENHANCEMENT
        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '        Dim intArrayColumnWidth As Integer() = {130, 100, 200, 150, 110, 110, 120, 120, 110}
        '        Dim strArrayGrpCols() As String = {"Column1"}
        '        Dim strExtraTitle As String = Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName & "|" & Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber
        '        If mdtTableExcel IsNot Nothing Then
        '            If pintReportType <> 1 Then 'NOT MONTH WISE
        '                'Sohail (18 Apr 2014) -- Start
        '                'Enhancement - Show Group by Cost Center option on Pension Fund Report
        '                'strArrayGrpCols = Nothing
        '                If mblnShowGroupByCostCenter = True Then
        '                    Dim s() As String = {"Column14"}
        '                    strArrayGrpCols = s
        '                Else
        '                    strArrayGrpCols = Nothing
        '                End If
        '                'Sohail (18 Apr 2014) -- End
        '            Else
        '                strExtraTitle = ""
        '            End If
        '        End If
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, , , , strArrayGrpCols, Me._ReportName & " ( " & mstrReportTypeName & " )", strExtraTitle, , Nothing) 'Language.getMessage(mstrModuleName, 23, "Group Total :")
        '        'Sohail (12 Jan 2013) -- End
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = xCompanyUnkid
            If objCompany._Countryunkid = 129 Then
                objRpt = Generate_DetailReport_Malawi(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            Else
                objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            End If
            objCompany = Nothing

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = {130, 100, 200, 150, 110, 110, 120, 120, 110}
                Dim strArrayGrpCols() As String = {"Column1"}
                Dim strExtraTitle As String = Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName & "|" & Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber
                If mdtTableExcel IsNot Nothing Then
                    If pintReportType <> 1 Then 'NOT MONTH WISE
                        If mblnShowGroupByCostCenter = True Then
                            Dim s() As String = {"Column14"}
                            strArrayGrpCols = s
                        Else
                            strArrayGrpCols = Nothing
                        End If
                    Else
                        strExtraTitle = ""
                    End If
                End If
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, , , , strArrayGrpCols, Me._ReportName & " ( " & mstrReportTypeName & " )", strExtraTitle, , Nothing) 'Language.getMessage(mstrModuleName, 23, "Group Total :")
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            Select Case intReportType
                Case 1
                    OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
                Case 2
                    OrderByDisplay = iColumn_MonthReport.ColumnItem(0).DisplayName
                    OrderByQuery = iColumn_MonthReport.ColumnItem(0).Name
            End Select

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Select Case intReportType
                Case 1
                    Call OrderByExecute(iColumn_DetailReport)
                Case 2
                    Call OrderByExecute(iColumn_MonthReport)
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function GetEmployer_No(Optional ByVal strList As String = "List") As DataSet
        Dim StrN As String = String.Empty
        Dim dsData As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrN &= "SELECT 0 As Id, @Select As Name " & _
            '             "UNION SELECT 1 AS Id, @NSSF As Name " & _
            '             "UNION SELECT 2 AS Id, @PPF As Name " & _
            '             "ORDER BY Id "

            StrN &= "SELECT 0 As Id, @Select As Name " & _
                         "UNION SELECT 1 AS Id, @NSSF As Name " & _
                         "UNION SELECT 2 AS Id, @PPF As Name "
            If Company._Object._Isreg1captionused Then
                StrN &= "UNION SELECT 3 AS Id, '" & Company._Object._Reg1_Caption & "' As Name "
            End If
            If Company._Object._Isreg2captionused Then
                StrN &= "UNION SELECT 4 AS Id, '" & Company._Object._Reg2_Caption & "' As Name "
            End If
            If Company._Object._Isreg3captionused Then
                StrN &= "UNION SELECT 5 AS Id, '" & Company._Object._Reg3_Caption & "' As Name "
            End If
            StrN &= "ORDER BY Id "
            'S.SANDEEP [ 17 OCT 2012 ] -- END



            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Select"))
            objDataOperation.AddParameter("@NSSF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "NSSF No."))
            objDataOperation.AddParameter("@PPF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "PPF No."))

            dsData = objDataOperation.ExecQuery(StrN, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsData

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployer_No; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetReportType(Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsReport As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 1 As Id, @Periodic As Name " & _
                       "UNION SELECT 2 As Id, @Monthly As Name "

            objDataOperation.AddParameter("@Periodic", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Period Wise"))
            objDataOperation.AddParameter("@Monthly", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Month Wise"))

            dsReport = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsReport

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReportType; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Dim iColumn_MonthReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Public Property Field_MonthReport() As IColumnCollection
        Get
            Return iColumn_MonthReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_MonthReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            'Pinkal (02-Oct-2012) -- Start
            'Enhancement : TRA Changes
            iColumn_DetailReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 21, "Employee Code")))
            'Pinkal (02-Oct-2012) -- End


            'S.SANDEEP [ 08 June 2011 ] -- START
            'ISSUE : MEMBERSHIP MAPPED WITH TRANSHEAD
            'iColumn_DetailReport.Add(New IColumn("SrNo", Language.getMessage(mstrModuleName, 1, "Sr. No.")))
            'S.SANDEEP [ 08 June 2011 ] -- END 
            iColumn_DetailReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 2, "Employee Name")))



            iColumn_DetailReport.Add(New IColumn("MembershipNo", Language.getMessage(mstrModuleName, 3, "Member Number")))
            iColumn_DetailReport.Add(New IColumn("GrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay")))
            iColumn_DetailReport.Add(New IColumn("EmpContrib", Language.getMessage(mstrModuleName, 5, "Employee Contribution")))
            iColumn_DetailReport.Add(New IColumn("EmprContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution")))
            iColumn_DetailReport.Add(New IColumn("Total", Language.getMessage(mstrModuleName, 7, "Total")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub Create_OnMonthReport()
        Try
            iColumn_MonthReport.Clear()
            iColumn_MonthReport.Add(New IColumn("EmpCode", Language.getMessage(mstrModuleName, 21, "Employee Code")))
            iColumn_MonthReport.Add(New IColumn("EmpName", Language.getMessage(mstrModuleName, 2, "Employee Name")))
            iColumn_MonthReport.Add(New IColumn("GrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay")))
            iColumn_MonthReport.Add(New IColumn("EmpContrib", Language.getMessage(mstrModuleName, 5, "Employee Contribution")))
            iColumn_MonthReport.Add(New IColumn("EmprContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution")))
            iColumn_MonthReport.Add(New IColumn("Total", Language.getMessage(mstrModuleName, 7, "Total")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnMonthReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport_Malawi(ByVal strDatabaseName As String, _
                                                  ByVal intUserUnkid As Integer, _
                                                  ByVal intYearUnkid As Integer, _
                                                  ByVal intCompanyUnkid As Integer, _
                                                  ByVal dtPeriodEnd As Date, _
                                                  ByVal strUserModeSetting As String, _
                                                  ByVal blnOnlyApproved As Boolean, _
                                                  ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport_Malawi() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim dtPeriod As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then 'FOR SAME CURRENCY

                StrQ = "SELECT  PeriodName AS PeriodName " & _
                       "    ,EmpId " & _
                       "    ,EmpName AS EmpName " & _
                       "    ,end_date " & _
                       "    ,MembershipNo AS MembershipNo " & _
                       "    ,MembershipName AS MembershipName " & _
                       "    ,GrossPay AS GrossPay " & _
                       "    ,EmpContrib AS EmpContrib " & _
                       "    ,EmprContrib AS EmprContrib " & _
                       "    ,Total AS Total " & _
                       "    ,BasicSal AS BasicSal " & _
                       "    ,EmpCode AS EmpCode " & _
                       "    ,NonPayrollMembership " & _
                       "    ,Emplr1_Amount " & _
                       "    ,Emplr2_Amount " & _
                           ", CCCode " & _
                           ", CCName " & _
                       "FROM " & _
                       " ( " & _
                       "    SELECT " & _
                       "         TableEmp_Contribution.Periodid " & _
                       "		,TableEmp_Contribution.PeriodName " & _
                       "        ,TableEmp_Contribution.end_date " & _
                       "        ,TableEmp_Contribution.EmpId " & _
                       "        ,TableEmp_Contribution.EmpName " & _
                       "        ,TableEmp_Contribution.Membership_Name AS MembershipName " & _
                       "        ,TableEmp_Contribution.Membership_No AS MembershipNo " & _
                       "        ,TableEmp_Contribution.MembershipUnkId AS MemId " & _
                       "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
                       "		,TableEmployer_Contribution.EmployerHead " & _
                       "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
                       "        ,ISNULL(Emplr1.Amount,0) AS Emplr1_Amount " & _
                       "        ,ISNULL(Emplr2.Amount,0) AS Emplr2_Amount " & _
                       "        ,ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) + ISNULL(Emplr1.Amount,0) + ISNULL(Emplr2.Amount,0) AS Total " & _
                       "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
                       "		,ISNULL(BasicSal, 0) AS BasicSal " & _
                       "        ,TableEmp_Contribution.EmpCode AS EmpCode " & _
                       "		,TableEmp_Contribution.StDate AS StDate " & _
                       "        ,TableEmp_Contribution.NonPayrollMembership " & _
                            ", TableEmp_Contribution.CCCode " & _
                            ", TableEmp_Contribution.CCName " & _
                       "    FROM " & _
                       "     ( " & _
                       "        SELECT " & _
                       "             ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                       "            ,ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                       "            ,CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'19000101'), 112) AS end_date " & _
                       "            ,hremployee_master.employeeunkid AS Empid " & _
                       "            ,hremployee_master.employeecode AS EmpCode "
                'Sohail (18 Apr 2014) - [CCCode, CCName]
                'S.SANDEEP [ 26 MAR 2014 ] -- START
                '"            ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                If mblnFirstNamethenSurname = True Then
                    StrQ &= "            ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= "            ,ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') AS EmpName "
                End If
                'S.SANDEEP [ 26 MAR 2014 ] -- END
                StrQ &= "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                       "            ,hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                       "            ,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                       "            ,hrmembership_master.membershipname AS Membership_Name " & _
                       "            ,cfcommon_period_tran.start_date AS StDate " & _
                       "            ,0 AS NonPayrollMembership " & _
                                ", CC.costcentercode AS CCCode " & _
                                ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "	JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "           AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        " AND cfcommon_period_tran.isactive = 1 " & _
                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End


                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS TableEmp_Contribution " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "       hremployee_master.employeeunkid AS Empid " & _
                        "	   ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "	   ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "      ,ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "	   ,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "      ,hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                        "      ,hrmembership_master.membershipunkid " & _
                        "      ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 " & _
                        "   AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
                        "   AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
                        "   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
                        "LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                If mintEmplrContrib1Id <= 0 Then
                    StrQ &= " AND 1 = 2 "
                End If
                'Sohail (31 Mar 2014) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead1 " & _
                        " ) AS Emplr1 ON TableEmp_Contribution.Empid = Emplr1.Empid " & _
                        "AND TableEmp_Contribution.Periodid = Emplr1.Periodid " & _
                        "LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                If mintEmplrContrib2Id <= 0 Then
                    StrQ &= " AND 1 = 2 "
                End If
                'Sohail (31 Mar 2014) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead2 " & _
                        " ) AS Emplr2 ON TableEmp_Contribution.Empid = Emplr2.Empid " & _
                        "   AND TableEmp_Contribution.Periodid = Emplr2.Periodid " & _
                        "LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            Gross.Periodid " & _
                        "			,Gross.PeriodName " & _
                        "			,Gross.Empid " & _
                        "			,SUM(Gross.Amount) AS GrossPay " & _
                        "       FROM " & _
                        "           ( " & _
                        "               SELECT " & _
                        "                    cfcommon_period_tran.periodunkid AS Periodid " & _
                        "				    ,cfcommon_period_tran.period_name AS PeriodName " & _
                        "				    ,hremployee_master.employeeunkid AS Empid " & _
                        "                   , CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= " FROM prpayrollprocess_tran " & _
                '        "	JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '        "	JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "	JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "	LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "       AND prtranhead_master.isvoid = 0 " & _
                            " LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                            "  AND cmclaim_process_tran.isvoid = 0 " & _
                            " LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                            "  AND cmexpense_master.isactive = 1 " & _
                        "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                        "  AND cmretire_process_tran.isvoid = 0 " & _
                        "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                        "  AND crretireexpense.isactive = 1 " & _
                        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= " WHERE prtranhead_master.trnheadtype_id = '" & enTranHeadType.EarningForEmployees & "' " & _
                '        "   AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '        "   AND cfcommon_period_tran.isactive = 1 "
                StrQ &= " WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                        "	AND prtnaleave_tran.isvoid = 0 " & _
                        "  AND cfcommon_period_tran.isactive = 1 " & _
                        "  AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   ) AS Gross " & _
                        "       GROUP BY Gross.Periodid " & _
                        "       ,Gross.PeriodName " & _
                        "       ,Gross.Empid " & _
                        ") AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid " & _
                        "   AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
                        "LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            cfcommon_period_tran.periodunkid AS Periodid " & _
                        "			,cfcommon_period_tran.period_name AS PeriodName " & _
                        "			,hremployee_master.employeeunkid AS Empid " & _
                        "           ,SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "	AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "	AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
                        ",cfcommon_period_tran.period_name " & _
                        ",hremployee_master.employeeunkid "



                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If



            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then    'FOR OTHER CURRENCY


                StrQ = "SELECT  PeriodName AS PeriodName " & _
                       "    ,EmpId " & _
                       "    ,EmpName AS EmpName " & _
                       "    ,end_date " & _
                       "    ,MembershipNo AS MembershipNo " & _
                       "    ,MembershipName AS MembershipName " & _
                       "    ,GrossPay AS GrossPay " & _
                       "    ,EmpContrib AS EmpContrib " & _
                       "    ,EmprContrib AS EmprContrib " & _
                       "    ,Total AS Total " & _
                       "    ,BasicSal AS BasicSal " & _
                       "    ,EmpCode AS EmpCode " & _
                       "    ,NonPayrollMembership " & _
                       "    ,Emplr1_Amount " & _
                       "    ,Emplr2_Amount " & _
                           ", CCCode " & _
                           ", CCName " & _
                       "FROM " & _
                       " ( " & _
                       "    SELECT " & _
                       "         TableEmp_Contribution.Periodid " & _
                       "		,TableEmp_Contribution.PeriodName " & _
                       "        ,TableEmp_Contribution.end_date " & _
                       "        ,TableEmp_Contribution.EmpId " & _
                       "        ,TableEmp_Contribution.EmpName " & _
                       "        ,TableEmp_Contribution.Membership_Name AS MembershipName " & _
                       "        ,TableEmp_Contribution.Membership_No AS MembershipNo " & _
                       "        ,TableEmp_Contribution.MembershipUnkId AS MemId " & _
                       "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
                       "		,TableEmployer_Contribution.EmployerHead " & _
                       "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
                       "        ,ISNULL(Emplr1.Amount,0) AS Emplr1_Amount " & _
                       "        ,ISNULL(Emplr2.Amount,0) AS Emplr2_Amount " & _
                       "        ,ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) + ISNULL(Emplr1.Amount,0) + ISNULL(Emplr2.Amount,0) AS Total " & _
                       "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
                       "		,ISNULL(BasicSal, 0) AS BasicSal " & _
                       "        ,TableEmp_Contribution.EmpCode AS EmpCode " & _
                       "		,TableEmp_Contribution.StDate AS StDate " & _
                       "        ,TableEmp_Contribution.NonPayrollMembership " & _
                            ", TableEmp_Contribution.CCCode " & _
                            ", TableEmp_Contribution.CCName " & _
                       "    FROM " & _
                       "     ( " & _
                       "        SELECT " & _
                       "             ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                       "            ,ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                       "            ,CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'19000101'), 112) AS end_date " & _
                       "            ,hremployee_master.employeeunkid AS Empid " & _
                       "            ,hremployee_master.employeecode AS EmpCode "
                'Sohail (28 Apr 2020) - [1900101]=[19000101]
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                'S.SANDEEP [ 26 MAR 2014 ] -- START
                '"            ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                If mblnFirstNamethenSurname = True Then
                    StrQ &= "            ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= "            ,ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' '+ ISNULL(hremployee_master.othername, '') AS EmpName "
                End If
                'S.SANDEEP [ 26 MAR 2014 ] -- END
                StrQ &= "			,(prpayment_tran.expaidrate/prpayment_tran.baseexchangerate) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                       "            ,hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                       "            ,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                       "            ,hrmembership_master.membershipname AS Membership_Name " & _
                       "            ,cfcommon_period_tran.start_date AS StDate " & _
                       "            ,0 AS NonPayrollMembership " & _
                                ", CC.costcentercode AS CCCode " & _
                                ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "	JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "           AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "	JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "	JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                        "       AND prpayment_tran.countryunkid = " & mintCountryId & " " & _
                        "   AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        "   AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        " AND cfcommon_period_tran.isactive = 1 " & _
                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL  " & _
                        "   SELECT " & _
                        "        ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "       ,ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                        "       ,CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '19000101'), 112) AS end_date " & _
                        "       ,hremployee_master.employeeunkid AS Empid " & _
                        "       ,hremployee_master.employeecode AS EmpCode " & _
                        "       ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount " & _
                        "       ,hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                        "       ,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                        "       ,hrmembership_master.membershipname AS Membership_Name " & _
                        "       ,cfcommon_period_tran.start_date AS StDate" & _
                        "       ,0 AS NonPayrollMembership " & _
                                ", CC.costcentercode AS CCCode " & _
                                ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM   prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid  " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                        "       AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
                        "   AND cfcommon_period_tran.isactive = 1 " & _
                        "   AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                        "   SELECT " & _
                        "        ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "       ,ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                        "       ,CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '19000101'), 112) AS end_date " & _
                        "       ,hremployee_master.employeeunkid AS Empid " & _
                        "       ,hremployee_master.employeecode AS EmpCode " & _
                        "       ,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "       ,hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                        "       ,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                        "       ,hrmembership_master.membershipname AS Membership_Name " & _
                        "       ,cfcommon_period_tran.start_date AS StDate" & _
                        "       ,0 AS NonPayrollMembership " & _
                                ", CC.costcentercode AS CCCode " & _
                                ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM   prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0 " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
                         " AND cfcommon_period_tran.isactive = 1 " & _
                         " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS TableEmp_Contribution " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "       hremployee_master.employeeunkid AS Empid " & _
                        "	   ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "	   ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "      ,ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "	   ,(prpayment_tran.expaidrate/prpayment_tran.baseexchangerate) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "      ,hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                        "      ,hrmembership_master.membershipunkid " & _
                        "      ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "       AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 " & _
                        "   AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " 	UNION ALL" & _
                        "       SELECT " & _
                        "            hremployee_master.employeeunkid AS Empid " & _
                        "           ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "           ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "           ,ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "           ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "           ,hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                        "           ,hrmembership_master.membershipunkid " & _
                        "           ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "       AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        " AND cfcommon_period_tran.isactive = 1 " & _
                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                        "   SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "       ,hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                        "       ,hrmembership_master.membershipunkid" & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "       AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "       AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                        "   JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                        "       AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        " AND cfcommon_period_tran.isactive = 1 " & _
                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
                        "   AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
                        "   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
                        "LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ,(prpayment_tran.expaidrate/prpayment_tran.baseexchangerate) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "      AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "      AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead1 " & _
                        " UNION ALL  SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "      AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "      AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead1 " & _
                        " UNION ALL  SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                If mintEmplrContrib1Id <= 0 Then
                    StrQ &= " AND 1 = 2 "
                End If
                'Sohail (31 Mar 2014) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead1 " & _
                        " ) AS Emplr1 ON TableEmp_Contribution.Empid = Emplr1.Empid " & _
                        "AND TableEmp_Contribution.Periodid = Emplr1.Periodid " & _
                        "LEFT JOIN " & _
                        " ( " & _
                        "   SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ,(prpayment_tran.expaidrate/prpayment_tran.baseexchangerate) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "      AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "      AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead2 " & _
                        " UNION ALL  SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        "      AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "      AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead2 " & _
                        " UNION ALL  SELECT " & _
                        "        hremployee_master.employeeunkid AS Empid " & _
                        "       ,prtranhead_master.trnheadname AS EmployerHead " & _
                        "       ,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                        "       ,ISNULL(cfcommon_period_tran.periodunkid,0) AS Periodid " & _
                        "       ," & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                        "       ,0 AS NonPayrollMembership "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                StrQ &= " FROM prpayrollprocess_tran " & _
                        "   JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "   JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "   JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                        " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                        " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                        "       AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                        "   AND cfcommon_period_tran.isactive = 1 "
                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Bank Reference No on Bank Payment List.
                If mintEmplrContrib2Id <= 0 Then
                    StrQ &= " AND 1 = 2 "
                End If
                'Sohail (31 Mar 2014) -- End

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "   AND prtranhead_master.tranheadunkid = @ETrnHead2 " & _
                        " ) AS Emplr2 ON TableEmp_Contribution.Empid = Emplr2.Empid " & _
                        "   AND TableEmp_Contribution.Periodid = Emplr2.Periodid " & _
                       " LEFT JOIN ( " & _
                             "                  SELECT  Gross.Periodid " & _
                             "                  , Gross.PeriodName " & _
                             "                  , Gross.Empid " & _
                             "                  , SUM(Gross.Amount) AS GrossPay " & _
                             "                  FROM (  " & _
                             "                               SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             "                               , cfcommon_period_tran.period_name AS PeriodName " & _
                             "                               , hremployee_master.employeeunkid AS Empid " & _
                             "                              , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "             FROM    prpayrollprocess_tran " & _
                '             "             JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '             "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                '             "             AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                '             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                '             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                '             "             AND prpayment_tran.countryunkid = " & mintCountryId
                StrQ &= "             FROM    prpayrollprocess_tran " & _
                             "             JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "             LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "               AND prtranhead_master.isvoid = 0 " & _
                            "             LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                            "               AND cmclaim_process_tran.isvoid = 0 " & _
                            "             LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                            "               AND cmexpense_master.isactive = 1 " & _
                            "           LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                            "               AND cmretire_process_tran.isvoid = 0 " & _
                            "           LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                            "               AND crretireexpense.isactive = 1 " & _
                             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             "             AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             "             AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "	WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             "	AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             "  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  AND cfcommon_period_tran.isactive = 1 "
                StrQ &= "	WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                             "	AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 " & _
                             "  AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL" & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             ", cfcommon_period_tran.period_name AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "  FROM  prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                             " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL" & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             " , cfcommon_period_tran.period_name AS PeriodName " & _
                             " , hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid	AND prpayment_tran.isvoid = 0 "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                             " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS Gross " & _
                              "GROUP BY Gross.Periodid " & _
                              ", Gross.PeriodName " & _
                              " , Gross.Empid " & _
                             " ) AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid" & _
                              " LEFT JOIN ( " & _
                              "                   SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                              "                  , cfcommon_period_tran.period_name AS PeriodName " & _
                              "                  , hremployee_master.employeeunkid AS Empid " & _
                              "                  , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate )	* SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM    prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
                             ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid " & _
                             ", prpayment_tran.expaidrate , prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If


                StrQ &= " UNION ALL " & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             ", cfcommon_period_tran.period_name AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM  prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	GROUP BY cfcommon_period_tran.periodunkid " & _
                             " , cfcommon_period_tran.period_name , hremployee_master.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= " UNION ALL " & _
                            " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                            ", cfcommon_period_tran.period_name AS PeriodName " & _
                            ", hremployee_master.employeeunkid AS Empid " & _
                            ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM    prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0 "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
                             ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid   "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If


            End If


            StrQ &= ") AS BAS ON BAS.Empid = TableEmp_Contribution.EmpId " & _
                                                              "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
                    ") AS Contribution " & _
                    "WHERE 1 = 1 "

            If mintReportId = 1 Then 'PERIOD WISE
                StrQ &= "AND Periodid = @PeriodId "
            End If


            'Anjan [07 October 2016] -- Start
            'ENHANCEMENT : Include Ignore Zero Value.
            If mblnIgnoreZeroValue = True Then
                StrQ &= " AND Contribution.Total <> 0 "
            End If

            'Anjan [07 October 2016] -- End


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As DataTable
            If mintReportId = 1 Then 'PERIOD WISE
                'Anjan [28 Mar 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                If Me.OrderByQuery.Trim <> "" Then
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                Else
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName, EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                End If
                'Anjan [28 Mar 2014 ] -- End
            Else 'MONTH WISE
                If Me.OrderByQuery.Trim <> "" Then
                    'Anjan [28 Mar 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta
                    'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable

                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName," & Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                    'Anjan [28 Mar 2014 ] -- End
                Else
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName, EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                End If
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            'Sohail (18 Apr 2014) -- Start
            'Enhancement - Show Group by Cost Center option on Pension Fund Report
            Dim StrCCCode As String = String.Empty
            Dim StrPrevCCCode As String = String.Empty
            'Sohail (18 Apr 2014) -- End
            Dim intCnt As Integer = 1
            Dim dtFilterTable As DataTable = Nothing
            Dim mdicIsEmpAdded As New Dictionary(Of String, String)
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn3Total, decColumn4Total, decColumn5Total, decColumn6Total, decColumn7Total, decColumn8Total, decColumn9Total As Decimal
            Dim decColumn3GrTotal, decColumn4GrTotal, decColumn5GrTotal, decColumn6GrTotal, decColumn7GrTotal, decColumn8GrTotal, decColumn9GrTotal As Decimal
            decColumn3GrTotal = 0 : decColumn4GrTotal = 0 : decColumn5GrTotal = 0 : decColumn6GrTotal = 0 : decColumn7GrTotal = 0 : decColumn8GrTotal = 0 : decColumn9GrTotal = 0
            Dim iCode As String = String.Empty
            Dim iDbl(7) As Double
            For Each dtFRow As DataRow In dtTable.Rows
                If IsDBNull(dtFRow.Item("PeriodName")) = True Then Continue For
                If iCode <> dtFRow.Item("EmpCode").ToString.Trim Then
                    decColumn3GrTotal = 0 : decColumn4GrTotal = 0 : decColumn5GrTotal = 0 : decColumn6GrTotal = 0 : decColumn7GrTotal = 0 : decColumn8GrTotal = 0 : decColumn9GrTotal = 0
                    iCode = dtFRow.Item("EmpCode").ToString.Trim
                End If

                StrECode = dtFRow.Item("EmpCode").ToString.Trim & "_" & dtFRow.Item("end_date").ToString.Trim
                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                If mintReportId = 1 Then
                    StrCCCode = dtFRow.Item("CCCode").ToString
                Else
                    StrCCCode = dtFRow.Item("EmpCode").ToString.Trim & "_" & dtFRow.Item("end_date").ToString.Trim
                End If
                If StrPrevCCCode <> StrCCCode Then
                    decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
                    decColumn3Total = 0 : decColumn9Total = 0 'Sohail (28 Aug 2014)
                End If
                'Sohail (18 Apr 2014) -- End

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'If StrPrevECode <> StrECode Then
                '    'If mintReportId = 2 Then 'MONTH WISE 'Sohail (18 Apr 2014)
                '        decColumn3Total = 0 : decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0 : decColumn9Total = 0
                '    'End If 'Sohail (18 Apr 2014)
                '    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                'Else
                '    StrPrevECode = StrECode
                '    StrPrevCCCode = StrCCCode 'Sohail (18 Apr 2014)
                '    Continue For
                'End If
                If StrPrevECode = StrECode AndAlso CBool(dtFRow.Item("NonPayrollMembership")) = True Then
                    StrPrevECode = StrECode
                    StrPrevCCCode = StrCCCode
                    Continue For
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                End If
                'Sohail (28 Aug 2014) -- End

                Select Case mintReportId
                    Case 1 'PERIOD WISE
                        rpt_Row.Item("Column1") = intCnt.ToString
                        rpt_Row.Item("Column2") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
                        'Sohail (28 Apr 2020) -- Start
                        'DEMETER Enhancement # 0004661 : Add membership number column on pension fund report for Malawi.
                        rpt_Row.Item("Column10") = dtFRow.Item("MembershipNo")
                        'Sohail (28 Apr 2020) -- End
                        intCnt += 1
                    Case 2 'MONTH WISE
                        'Anjan [03 Feb 2014] -- Start
                        'ENHANCEMENT : Requested by Rutta
                        'rpt_Row.Item("Column2") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
                        rpt_Row.Item("Column1") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
                        rpt_Row.Item("Column2") = dtFRow.Item("PeriodName")
                        'Anjan [03 Feb 2014 ] -- End
                End Select
                rpt_Row.Item("Column3") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column6") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtFRow.Item("Emplr1_Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtFRow.Item("Emplr2_Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)

                rpt_Row.Item("Column81") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
                rpt_Row.Item("Column82") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
                rpt_Row.Item("Column83") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column84") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column85") = Format(CDec(dtFRow.Item("Emplr1_Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column86") = Format(CDec(dtFRow.Item("Emplr2_Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column87") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)

                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                If mblnShowGroupByCostCenter = True Then
                    rpt_Row.Item("Column13") = dtFRow.Item("CCCode")
                    rpt_Row.Item("Column14") = dtFRow.Item("CCName")
                Else
                    rpt_Row.Item("Column13") = ""
                    rpt_Row.Item("Column14") = ""
                End If
                'Sohail (18 Apr 2014) -- End

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'decColumn3Total = decColumn3Total + CDec(dtFRow.Item("BasicSal"))
                'decColumn4Total = decColumn4Total + CDec(dtFRow.Item("GrossPay"))
                If StrPrevECode <> StrECode Then
                    decColumn3Total = decColumn3Total + CDec(dtFRow.Item("BasicSal"))
                    decColumn4Total = decColumn4Total + CDec(dtFRow.Item("GrossPay"))
                End If
                'Sohail (28 Aug 2014) -- End
                decColumn5Total = decColumn5Total + CDec(dtFRow.Item("EmpContrib"))
                decColumn6Total = decColumn6Total + CDec(dtFRow.Item("EmprContrib"))
                decColumn7Total = decColumn7Total + CDec(dtFRow.Item("Emplr1_Amount"))
                decColumn8Total = decColumn8Total + CDec(dtFRow.Item("Emplr2_Amount"))
                decColumn9Total = decColumn9Total + CDec(dtFRow.Item("Total"))

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'decColumn3GrTotal = decColumn3GrTotal + CDec(dtFRow.Item("BasicSal"))
                'decColumn4GrTotal = decColumn4GrTotal + CDec(dtFRow.Item("GrossPay"))
                If StrPrevECode <> StrECode Then
                    decColumn3GrTotal = decColumn3GrTotal + CDec(dtFRow.Item("BasicSal"))
                    decColumn4GrTotal = decColumn4GrTotal + CDec(dtFRow.Item("GrossPay"))
                End If
                'Sohail (28 Aug 2014) -- End
                decColumn5GrTotal = decColumn5GrTotal + CDec(dtFRow.Item("EmpContrib"))
                decColumn6GrTotal = decColumn6GrTotal + CDec(dtFRow.Item("EmprContrib"))
                decColumn7GrTotal = decColumn7GrTotal + CDec(dtFRow.Item("Emplr1_Amount"))
                decColumn8GrTotal = decColumn8GrTotal + CDec(dtFRow.Item("Emplr2_Amount"))
                decColumn9GrTotal = decColumn9GrTotal + CDec(dtFRow.Item("Total"))

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'iDbl(0) = iDbl(0) + CDec(dtFRow.Item("BasicSal"))
                'iDbl(1) = iDbl(1) + CDec(dtFRow.Item("GrossPay"))
                If StrPrevECode <> StrECode Then
                    iDbl(0) = iDbl(0) + CDec(dtFRow.Item("BasicSal"))
                    iDbl(1) = iDbl(1) + CDec(dtFRow.Item("GrossPay"))
                End If
                'Sohail (28 Aug 2014) -- End
                iDbl(2) = iDbl(2) + CDec(dtFRow.Item("EmpContrib"))
                iDbl(3) = iDbl(3) + CDec(dtFRow.Item("EmprContrib"))
                iDbl(4) = iDbl(4) + CDec(dtFRow.Item("Emplr1_Amount"))
                iDbl(5) = iDbl(5) + CDec(dtFRow.Item("Emplr2_Amount"))
                iDbl(6) = iDbl(6) + CDec(dtFRow.Item("Total"))

                'Anjan [03 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                'rpt_Row.Item("Column71") = Format(decColumn3Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column72") = Format(decColumn4Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column73") = Format(decColumn5Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column74") = Format(decColumn6Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column75") = Format(decColumn7Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column76") = Format(decColumn8Total, GUI.fmtCurrency)
                'rpt_Row.Item("Column77") = Format(decColumn9Total, GUI.fmtCurrency)
                If mintReportId = 1 Then
                    rpt_Row.Item("Column71") = Format(decColumn3Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column72") = Format(decColumn4Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column73") = Format(decColumn5Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column74") = Format(decColumn6Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column75") = Format(decColumn7Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column76") = Format(decColumn8Total, GUI.fmtCurrency)
                    rpt_Row.Item("Column77") = Format(decColumn9Total, GUI.fmtCurrency)
                Else
                    rpt_Row.Item("Column71") = Format(decColumn3GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column72") = Format(decColumn4GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column73") = Format(decColumn5GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column74") = Format(decColumn6GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column75") = Format(decColumn7GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column76") = Format(decColumn8GrTotal, GUI.fmtCurrency)
                    rpt_Row.Item("Column77") = Format(decColumn9GrTotal, GUI.fmtCurrency)
                End If
                'Anjan [03 Feb 2014 ] -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

                StrPrevECode = StrECode
                StrPrevCCCode = StrCCCode 'Sohail (18 Apr 2014)
            Next
            Select Case mintReportId
                Case 1
                    objRpt = New ArutiReport.Designer.rptMalawi_PensionFund
                    'Anjan [03 Feb 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta for malawi, comment on online document #343.
                    If mintEmplrContrib1Id <= 0 Then
                        Call ReportFunction.EnableSuppress(objRpt, "txtEmplr1", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column71", True)
                        Call ReportFunction.EnableSuppress(objRpt, "txtTotal7", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column751", True) 'Sohail (18 Apr 2014)
                    End If


                    If mintEmplrContrib2Id <= 0 Then
                        Call ReportFunction.EnableSuppress(objRpt, "txtEmplr2", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
                        Call ReportFunction.EnableSuppress(objRpt, "txtTotal8", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column761", True) 'Sohail (18 Apr 2014)
                    End If

                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    ReportFunction.TextChange(objRpt, "txtCostCenter", Language.getMessage(mstrModuleName, 37, "Cost Center : "))
                    ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 38, "Sub Total :"))
                    If mblnShowGroupByCostCenter = True Then
                        Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", False)
                        Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", True)
                        Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", True)
                    End If
                    'Sohail (18 Apr 2014) -- End
                Case 2
                    objRpt = New ArutiReport.Designer.rptMalawi_PensionFundReport_Month
                    'Anjan [03 Feb 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta for malawi, comment on online document #343.
                    If mintEmplrContrib1Id <= 0 Then
                        Call ReportFunction.EnableSuppress(objRpt, "txtEmplr1", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column71", True)
                        Call ReportFunction.EnableSuppress(objRpt, "txtTotal5", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column751", True)
                    End If


                    If mintEmplrContrib2Id <= 0 Then
                        Call ReportFunction.EnableSuppress(objRpt, "txtEmplr2", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
                        Call ReportFunction.EnableSuppress(objRpt, "txtTotal6", True)
                        Call ReportFunction.EnableSuppress(objRpt, "Column761", True)
                    End If

                    'Anjan [03 Feb 2014 ] -- End
            End Select



            'Anjan [03 Feb 2014 ] -- End
            objRpt.SetDataSource(rpt_Data)

            If mintReportId = 1 Then
                Call ReportFunction.TextChange(objRpt, "txtSchedular", Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName)
                Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber)
                Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 34, "Employee"))
                Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
                'Sohail (28 Apr 2020) -- Start
                'DEMETER Enhancement # 0004661 : Add membership number column on pension fund report for Malawi.
                Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 3, "Member Number"))
                'Sohail (28 Apr 2020) -- End
            Else
                Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee :"))
                Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 22, "Month"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 23, "Group Total :"))
            End If
            Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 24, "Basic Salary"))
            Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay"))
            Call ReportFunction.TextChange(objRpt, "txtMemberContrib", Language.getMessage(mstrModuleName, 35, "Personal"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerContrib", Language.getMessage(mstrModuleName, 36, "Pension Bill"))
            Call ReportFunction.TextChange(objRpt, "txtEmplr1", mstrEmplrContrib1_Text)
            Call ReportFunction.TextChange(objRpt, "txtEmplr2", mstrEmplrContrib2_Text)
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 28, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")



            If mintReportId = 1 Then
                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                'Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn3Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn4Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn5Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn6Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn7Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decColumn8Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decColumn9Total, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(iDbl(0), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(iDbl(1), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(iDbl(2), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(iDbl(3), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(iDbl(4), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(iDbl(5), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(iDbl(6), GUI.fmtCurrency))
                'Sohail (18 Apr 2014) -- End
            Else
                'Anjan [03 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta
                'Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn3GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn4GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn5GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn6GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn7GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn8GrTotal, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn9GrTotal, GUI.fmtCurrency))

                Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(iDbl(0), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(iDbl(1), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(iDbl(2), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(iDbl(3), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(iDbl(4), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(iDbl(5), GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(iDbl(6), GUI.fmtCurrency))
                'Anjan [03 Feb 2014 ] -- End
            End If

            If mblnShowBasicSalary = False Then
                Dim strCurrentHdr As String = String.Empty
                If mintReportId = 1 Then
                    ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                    ReportFunction.EnableSuppress(objRpt, "Column31", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal3", True)
                    ReportFunction.EnableSuppress(objRpt, "Column711", True) 'Sohail (18 Apr 2014)
                Else
                    ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                    ReportFunction.EnableSuppress(objRpt, "Column31", True)
                    ReportFunction.EnableSuppress(objRpt, "Column711", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
                End If
            Else
                If mintReportId = 1 Then
                    ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal4", True)
                    ReportFunction.EnableSuppress(objRpt, "Column41", True)
                    ReportFunction.EnableSuppress(objRpt, "Column721", True) 'Sohail (18 Apr 2014)
                Else
                    ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
                    ReportFunction.EnableSuppress(objRpt, "Column41", True)
                    ReportFunction.EnableSuppress(objRpt, "Column721", True)
                End If
            End If
            'Anjan [12 November 2014] -- Start
            'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
            If mblnDoNotShowBasic_GrossSalary = True Then
                Call ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                If mintReportId = 1 Then
                    Call ReportFunction.EnableSuppress(objRpt, "Column31", True)
                    objRpt.ReportDefinition.ReportObjects("txtEmployee").Width = objRpt.ReportDefinition.ReportObjects("txtBasicSal").Width + objRpt.ReportDefinition.ReportObjects("txtGrossPay").Width
                    objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width

                    Call ReportFunction.EnableSuppress(objRpt, "txtTotal3", True)
                    Call ReportFunction.EnableSuppress(objRpt, "txtTotal4", True)
                Else
                    '    Call ReportFunction.EnableSuppress(objRpt, "Column31", True)

                    objRpt.ReportDefinition.ReportObjects("txtMonth").Width = objRpt.ReportDefinition.ReportObjects("txtBasicSal").Width + objRpt.ReportDefinition.ReportObjects("txtGrossPay").Width
                    objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width

                    Call ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
                    Call ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
                End If

                Call ReportFunction.EnableSuppress(objRpt, "Column711", True)


                Call ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column41", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column721", True)

            End If
            'Anjan [12 November 2014] -- End

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport_Malawi; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 NOV 2013 ] -- END


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim dtPeriod As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then

                StrQ = "SELECT  PeriodName AS PeriodName " & _
                              ", EmpId " & _
                        "	,EmpName AS EmpName " & _
                              ", end_date " & _
                        "	,MembershipNo AS MembershipNo " & _
                        "	,MembershipName AS MembershipName " & _
                        "	,GrossPay AS GrossPay " & _
                        "	,EmpContrib AS EmpContrib " & _
                        "	,EmprContrib AS EmprContrib " & _
                        "	,Total AS Total " & _
                        "	,BasicSal AS BasicSal " & _
                        "	,EmpCode AS EmpCode " & _
                                    ", NonPayrollMembership " & _
                            ", CCCode " & _
                            ", CCName " & _
                        "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
                        "		,TableEmp_Contribution.PeriodName " & _
                                          ", TableEmp_Contribution.end_date " & _
                                          ", TableEmp_Contribution.EmpId " & _
                                          ", TableEmp_Contribution.EmpName " & _
                                          ", TableEmp_Contribution.Membership_Name AS MembershipName " & _
                                          ", TableEmp_Contribution.Membership_No AS MembershipNo " & _
                                          ", TableEmp_Contribution.MembershipUnkId AS MemId " & _
                        "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
                        "		,TableEmployer_Contribution.EmployerHead " & _
                        "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
                                                ", ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
                        "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
                        "		,ISNULL(BasicSal, 0) AS BasicSal " & _
                                          ", TableEmp_Contribution.EmpCode AS EmpCode " & _
                        "		,TableEmp_Contribution.StDate AS StDate " & _
                                                ", TableEmp_Contribution.NonPayrollMembership " & _
                                   ", TableEmp_Contribution.CCCode " & _
                                   ", TableEmp_Contribution.CCName " & _
                                  "FROM      ( SELECT  ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                                                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                                                              ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'19000101'), 112) AS end_date " & _
                        "			,hremployee_master.employeeunkid AS Empid " & _
                                                                   ", hremployee_master.employeecode AS EmpCode "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                'S.SANDEEP [ 26 MAR 2014 ] -- START
                '", ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " 
                If mblnFirstNamethenSurname = True Then
                    StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '') AS EmpName "
                End If
                'S.SANDEEP [ 26 MAR 2014 ] -- END


                StrQ &= ",CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                           ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                                           ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                                           ", hrmembership_master.membershipname AS Membership_Name " & _
                                                              ", cfcommon_period_tran.start_date AS StDate " & _
                                                              ", 0 AS NonPayrollMembership " & _
                                                              ", CC.costcentercode AS CCCode " & _
                                                              ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "		FROM prpayrollprocess_tran " & _
                        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                  "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                                                  "JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            "           LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "			AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= "                                      AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                If mintNonPayrollMembershipId > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    Dim objMaster As New clsMasterData
                    Dim dsCombo As DataSet
                    Dim dtTemp As DataTable = Nothing

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim intCurrentOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)
                    Dim intCurrentOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, 2) 'All Closed Period
                    dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, intYearUnkid, strDatabaseName, mdtDatabase_Start_Date, "Period", False, 2)
                    'Sohail (21 Aug 2015) -- End
                    dtPeriod = New DataView(dsCombo.Tables("Period")).ToTable

                    If intCurrentOpenPeriod > 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, 1) 'All Open Period
                        dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, intYearUnkid, strDatabaseName, mdtDatabase_Start_Date, "Period", False, 1)
                        'Sohail (21 Aug 2015) -- End
                        dtTemp = New DataView(dsCombo.Tables("Period"), "periodunkid = " & intCurrentOpenPeriod & " ", "", DataViewRowState.CurrentRows).ToTable

                        For Each dtRow As DataRow In dtTemp.Rows
                            dtPeriod.ImportRow(dtRow)
                        Next
                    End If
                    If mintReportId = 1 Then 'PERIOD WISE
                        dtPeriod = New DataView(dtPeriod, "periodunkid = " & mintPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
                    End If

                    For Each dtRow As DataRow In dtPeriod.Rows
                        StrQ &= "UNION ALL " & _
                                "SELECT    " & CInt(dtRow.Item("periodunkid")) & " AS Periodid " & _
                                          ", '" & dtRow.Item("name").ToString.Replace("'", "''") & "' AS PeriodName " & _
                                          ", '" & dtRow.Item("end_date").ToString & "' AS end_date " & _
                                          ", hremployee_master.employeeunkid AS Empid " & _
                                          ", hremployee_master.employeecode AS EmpCode " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                                          ", 0 AS Amount " & _
                                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                          ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                          ", hrmembership_master.membershipname AS Membership_Name " & _
                                          ", '" & dtRow.Item("start_date").ToString & "' AS StDate " & _
                                          ", 1 AS NonPayrollMembership " & _
                                          ", CC.costcentercode AS CCCode " & _
                                          ", CC.costcentername AS CCName "
                        'Nilay (13-Oct-2016) -- Start
                        'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
                        'REPLACED " & eZeeDate.convertDate(dtRow.Item("start_date").ToString) & " AS StDate WITH '" & dtRow.Item("start_date").ToString & "' AS StDate
                        'Nilay (13-Oct-2016) -- End

                        'Sohail (23 Aug 2014) - [CCCode, CCName]

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= "FROM      hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                        "JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                        'Sohail (23 Aug 2014) - [LEFT JOIN prcostcenter_master]

                        StrQ &= mstrAnalysis_Join

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END

                        StrQ &= "WHERE     hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                        ''Pinkal (15-Oct-2014) -- Start
                        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                        'If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    StrQ &= UserAccessLevel._AccessLevelFilterString
                        'End If
                        ''Pinkal (15-Oct-2014) -- End

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry
                        End If

                        'S.SANDEEP [04 JUN 2015] -- END

                    Next
                End If

                StrQ &= "                               ) AS TableEmp_Contribution " & _
                                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
                        "			,prtranhead_master.trnheadname AS EmployerHead " & _
                        "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                                                               ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                        "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                                               ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                                                    ", hrmembership_master.membershipunkid " & _
                                                                    ", 0 AS NonPayrollMembership "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "		FROM prpayrollprocess_tran " & _
                        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                  "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                                                                  "JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                  "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                                                  "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                  "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                  "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                  "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= "                                      AND prtnaleave_tran.payperiodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                If mintNonPayrollMembershipId > 0 Then
                    For Each dtRow As DataRow In dtPeriod.Rows
                        StrQ &= "UNION ALL " & _
                                "SELECT  hremployee_master.employeeunkid AS Empid " & _
                                      ", '' AS EmployerHead " & _
                                      ", 0 AS EmployerHeadId " & _
                                      ", " & CInt(dtRow.Item("periodunkid")) & " AS Periodid " & _
                                      ", 0 AS Amount " & _
                                      ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                      ", hrmembership_master.membershipunkid " & _
                                      ", 1 AS NonPayrollMembership "

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                              "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                              "0) = 0 " & _
                                        "JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "

                        StrQ &= mstrAnalysis_Join

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END

                        StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                        ''Pinkal (15-Oct-2014) -- Start
                        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                        'If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    StrQ &= UserAccessLevel._AccessLevelFilterString
                        'End If
                        ''Pinkal (15-Oct-2014) -- End

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry
                        End If

                        'S.SANDEEP [04 JUN 2015] -- END

                    Next
                End If

                StrQ &= "                               ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
                                                                                      "AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
                                                                                      "AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
                                            "LEFT JOIN ( SELECT  Gross.Periodid " & _
                        "			,Gross.PeriodName " & _
                        "			,Gross.Empid " & _
                        "			,SUM(Gross.Amount) AS GrossPay " & _
                                                        "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
                        "				,cfcommon_period_tran.period_name AS PeriodName " & _
                        "				,hremployee_master.employeeunkid AS Empid " & _
                                                                          ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "			FROM prpayrollprocess_tran " & _
                '        "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '        "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '        "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                StrQ &= "			FROM prpayrollprocess_tran " & _
                        "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "				LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "                   AND prtranhead_master.isvoid = 0 " & _
                        "               LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                        "                   AND cmclaim_process_tran.isvoid = 0 " & _
                        "               LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                        "                   AND cmexpense_master.isactive = 1 " & _
                         "              LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                        "                   AND cmretire_process_tran.isvoid = 0 " & _
                        "               LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                        "                   AND crretireexpense.isactive = 1 " & _
                        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
                '                                                            "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '                                                            "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '                                                            "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                                                            "AND cfcommon_period_tran.isactive = 1 "
                StrQ &= "			WHERE prpayrollprocess_tran.isvoid = 0 " & _
                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= "                                                AND cfcommon_period_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "                                        ) AS Gross " & _
                                                        "GROUP BY Gross.Periodid " & _
                                                              ", Gross.PeriodName " & _
                                                              ", Gross.Empid " & _
                                                      ") AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid " & _
                                                                         "AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
                                            "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                        "			,cfcommon_period_tran.period_name AS PeriodName " & _
                        "			,hremployee_master.employeeunkid AS Empid " & _
                                                              ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "		FROM prpayrollprocess_tran " & _
                        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
                '        "			AND prtranhead_master.typeof_id = 1 " & _
                '                                                "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '                                                "AND cfcommon_period_tran.isactive = 1 "
                StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                   "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                            "AND cfcommon_period_tran.isactive = 1 "

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If
                'Sohail (28 Aug 2013) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= "                                    AND cfcommon_period_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                               GROUP BY cfcommon_period_tran.periodunkid " & _
                                                              ", cfcommon_period_tran.period_name " & _
                                                              ", hremployee_master.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then  ' FOR DIFFERENT CURRENCY 

                StrQ = "SELECT  PeriodName AS PeriodName " & _
                           ", EmpId " & _
                           "	,EmpName AS EmpName " & _
                           ", end_date " & _
                           "	,MembershipNo AS MembershipNo " & _
                           "	,MembershipName AS MembershipName " & _
                           "	,GrossPay AS GrossPay " & _
                           "	,EmpContrib AS EmpContrib " & _
                           "	,EmprContrib AS EmprContrib " & _
                           "	,Total AS Total " & _
                           "	,BasicSal AS BasicSal " & _
                           "	,EmpCode AS EmpCode " & _
                                ", NonPayrollMembership " & _
                           ", CCCode " & _
                           ", CCName " & _
                     "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
                     "		,TableEmp_Contribution.PeriodName " & _
                                       ", TableEmp_Contribution.end_date " & _
                                       ", TableEmp_Contribution.EmpId " & _
                                       ", TableEmp_Contribution.EmpName " & _
                                       ", TableEmp_Contribution.Membership_Name AS MembershipName " & _
                                       ", TableEmp_Contribution.Membership_No AS MembershipNo " & _
                                       ", TableEmp_Contribution.MembershipUnkId AS MemId " & _
                     "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
                     "		,TableEmployer_Contribution.EmployerHead " & _
                     "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
                                        ", ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
                     "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
                     "		,ISNULL(BasicSal, 0) AS BasicSal " & _
                                       ", TableEmp_Contribution.EmpCode AS EmpCode " & _
                     "		,TableEmp_Contribution.StDate AS StDate " & _
                                        ", TableEmp_Contribution.NonPayrollMembership " & _
                           ", TableEmp_Contribution.CCCode " & _
                           ", TableEmp_Contribution.CCName " & _
                    " FROM (  " & _
                   "               SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                   "              , ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                   "              , CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '19000101'), 112) AS end_date " & _
                   "              , hremployee_master.employeeunkid AS Empid " & _
                   "              , hremployee_master.employeecode AS EmpCode "
                'Sohail (18 Apr 2014) - [CCCode, CCName]
                'S.SANDEEP [ 26 MAR 2014 ] -- START
                '"              , ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                If mblnFirstNamethenSurname = True Then
                    StrQ &= "              , ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName "
                Else
                    StrQ &= "              , ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' '  + ISNULL(hremployee_master.othername, '') AS EmpName "
                End If
                'S.SANDEEP [ 26 MAR 2014 ] -- END

                StrQ &= "              , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
                   "              , hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                   "              , hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                   "              , hrmembership_master.membershipname AS Membership_Name " & _
                                        ", cfcommon_period_tran.start_date AS StDate " & _
                                        ", 0 AS NonPayrollMembership " & _
                                        ", CC.costcentercode AS CCCode " & _
                                        ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM   prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             " JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                             " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                             " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                             " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId & " " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
                             " AND cfcommon_period_tran.isactive = 1 " & _
                             " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= " UNION ALL  " & _
                             " SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                             ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                             ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '19000101'), 112) AS end_date " & _
                             ", hremployee_master.employeeunkid AS Empid " & _
                             ", hremployee_master.employeecode AS EmpCode " & _
                             ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount " & _
                             ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                             ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                             ", hrmembership_master.membershipname AS Membership_Name " & _
                                    ", cfcommon_period_tran.start_date AS StDate" & _
                                    ", 0 AS NonPayrollMembership " & _
                                        ", CC.costcentercode AS CCCode " & _
                                        ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "  FROM   prpayrollprocess_tran " & _
                              " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                              " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                              " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                              " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                              "JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                              " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                              " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                              " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                              " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid  " & _
                              " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                              " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " " & _
                              " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                              " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                              "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
                           " AND cfcommon_period_tran.isactive = 1 " & _
                           " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                              " SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                              " , ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                              ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '19000101'), 112) AS end_date " & _
                              ", hremployee_master.employeeunkid AS Empid " & _
                              ", hremployee_master.employeecode AS EmpCode " & _
                              ", ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                              ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                              ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                              ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                              ", hrmembership_master.membershipname AS Membership_Name " & _
                                    ", cfcommon_period_tran.start_date AS StDate" & _
                                    ", 0 AS NonPayrollMembership " & _
                                        ", CC.costcentercode AS CCCode " & _
                                        ", CC.costcentername AS CCName "
                'Sohail (18 Apr 2014) - [CCCode, CCName]

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM   prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                             " JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                             " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                             " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0 " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]
                'Sohail (18 Apr 2014) - [LEFT JOIN prcostcenter_master]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
                         " AND cfcommon_period_tran.isactive = 1 " & _
                         " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                If mintNonPayrollMembershipId > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    Dim objMaster As New clsMasterData
                    Dim dsCombo As DataSet
                    Dim dtTemp As DataTable = Nothing

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim intCurrentOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1)
                    Dim intCurrentOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, 2) 'All Closed Period
                    dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, intYearUnkid, strDatabaseName, mdtDatabase_Start_Date, "Period", False, 2)
                    'Sohail (21 Aug 2015) -- End
                    dtPeriod = New DataView(dsCombo.Tables("Period")).ToTable

                    If intCurrentOpenPeriod > 0 Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, 1) 'All Open Period
                        dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, intYearUnkid, strDatabaseName, mdtDatabase_Start_Date, "Period", False, 1)
                        'Sohail (21 Aug 2015) -- End
                        dtTemp = New DataView(dsCombo.Tables("Period"), "periodunkid = " & intCurrentOpenPeriod & " ", "", DataViewRowState.CurrentRows).ToTable

                        For Each dtRow As DataRow In dtTemp.Rows
                            dtPeriod.ImportRow(dtRow)
                        Next
                    End If
                    If mintReportId = 1 Then 'PERIOD WISE
                        dtPeriod = New DataView(dtPeriod, "periodunkid = " & mintPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
                    End If

                    For Each dtRow As DataRow In dtPeriod.Rows
                        StrQ &= "UNION ALL " & _
                                "SELECT    " & CInt(dtRow.Item("periodunkid")) & " AS Periodid " & _
                                          ", '" & dtRow.Item("name").ToString.Replace("'", "''") & "' AS PeriodName " & _
                                          ", '" & dtRow.Item("end_date").ToString & "' AS end_date " & _
                                          ", hremployee_master.employeeunkid AS Empid " & _
                                          ", hremployee_master.employeecode AS EmpCode " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                                          ", 0 AS Amount " & _
                                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
                                          ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
                                          ", hrmembership_master.membershipname AS Membership_Name " & _
                                          ", " & eZeeDate.convertDate(dtRow.Item("start_date").ToString) & " AS StDate " & _
                                          ", 1 AS NonPayrollMembership " & _
                                          ", CC.costcentercode AS CCCode " & _
                                          ", CC.costcentername AS CCName "
                        'Sohail (23 Aug 2014) - [CCCode, CCName]

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= "FROM      hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                      "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                        "JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                                        "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid "
                        'Sohail (23 Aug 2014) - [LEFT JOIN prcostcenter_master]

                        StrQ &= mstrAnalysis_Join

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END

                        StrQ &= "WHERE     hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                        ''Pinkal (15-Oct-2014) -- Start
                        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                        'If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    StrQ &= UserAccessLevel._AccessLevelFilterString
                        'End If
                        ''Pinkal (15-Oct-2014) -- End

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry
                        End If

                        'S.SANDEEP [04 JUN 2015] -- END

                    Next
                End If

                StrQ &= ") AS TableEmp_Contribution " & _
                             " LEFT JOIN ( " & _
                                " SELECT  hremployee_master.employeeunkid AS Empid " & _
                                " , prtranhead_master.trnheadname AS EmployerHead " & _
                                ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                                ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                                ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                                ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                        ", hrmembership_master.membershipunkid " & _
                                        ", 0 AS NonPayrollMembership "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM  prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                            " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                            " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                            " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            " AND cfcommon_period_tran.isactive = 1 " & _
                            " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
                '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " 	UNION ALL" & _
                             " SELECT  hremployee_master.employeeunkid AS Empid " & _
                             ", prtranhead_master.trnheadname AS EmployerHead " & _
                             ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                             ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                             ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                    ", hrmembership_master.membershipunkid " & _
                                    ", 0 AS NonPayrollMembership "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                             " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
                             " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                             " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                             " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                            " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                            " AND cfcommon_period_tran.isactive = 1 " & _
                            " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL " & _
                            " SELECT  hremployee_master.employeeunkid AS Empid " & _
                            ", prtranhead_master.trnheadname AS EmployerHead " & _
                            ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
                            ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
                            ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                            " , hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                    ", hrmembership_master.membershipunkid" & _
                                    ", 0 AS NonPayrollMembership "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                            " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " AND ISNULL(hremployee_meminfo_tran.isdeleted,	0) = 0 " & _
                            " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                            " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                         " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                         " AND cfcommon_period_tran.isactive = 1 " & _
                         " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                If mintNonPayrollMembershipId > 0 Then
                    For Each dtRow As DataRow In dtPeriod.Rows
                        StrQ &= "UNION ALL " & _
                                "SELECT  hremployee_master.employeeunkid AS Empid " & _
                                      ", '' AS EmployerHead " & _
                                      ", 0 AS EmployerHeadId " & _
                                      ", " & CInt(dtRow.Item("periodunkid")) & " AS Periodid " & _
                                      ", 0 AS Amount " & _
                                      ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
                                      ", hrmembership_master.membershipunkid " & _
                                      ", 1 AS NonPayrollMembership "

                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Fields
                        Else
                            StrQ &= ", 0 AS Id, '' AS GName "
                        End If

                        StrQ &= "FROM    hremployee_master " & _
                                        "JOIN hremployee_meminfo_tran ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                              "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                              "0) = 0 " & _
                                        "JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "

                        StrQ &= mstrAnalysis_Join

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END

                        StrQ &= "WHERE   hrmembership_master.membershipunkid IN ( " & mintNonPayrollMembershipId.ToString & " ) "

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                        ''Pinkal (15-Oct-2014) -- Start
                        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                        'If UserAccessLevel._AccessLevel.Length > 0 Then
                        '    StrQ &= UserAccessLevel._AccessLevelFilterString
                        'End If
                        ''Pinkal (15-Oct-2014) -- End

                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry
                        End If

                        'S.SANDEEP [04 JUN 2015] -- END

                    Next
                End If

                StrQ &= " ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
                             " AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
                             " AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid" & _
                             " LEFT JOIN ( " & _
                             "                  SELECT  Gross.Periodid " & _
                             "                  , Gross.PeriodName " & _
                             "                  , Gross.Empid " & _
                             "                  , SUM(Gross.Amount) AS GrossPay " & _
                             "                  FROM (  " & _
                             "                               SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             "                               , cfcommon_period_tran.period_name AS PeriodName " & _
                             "                               , hremployee_master.employeeunkid AS Empid " & _
                             "                              , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "             FROM    prpayrollprocess_tran " & _
                '             "             JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '             "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                '             "             AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                '             "             AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                '             "             AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                '             "             AND prpayment_tran.countryunkid = " & mintCountryId
                StrQ &= "             FROM    prpayrollprocess_tran " & _
                             "             JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "             LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "                   AND prtranhead_master.isvoid = 0 " & _
                             "             LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                             "                   AND cmclaim_process_tran.isvoid = 0 " & _
                             "             LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                             "                   AND cmexpense_master.isactive = 1 " & _
                             "              LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                            "                   AND cmretire_process_tran.isvoid = 0 " & _
                            "               LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                            "                   AND crretireexpense.isactive = 1 " & _
                             "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             "             AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
                             "             AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             "             AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             "             AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "	WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             "	AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             "  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  AND cfcommon_period_tran.isactive = 1 "
                StrQ &= "	WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                             "	AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 " & _
                             "  AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL" & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             ", cfcommon_period_tran.period_name AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= "  FROM  prpayrollprocess_tran " & _
                '             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                '            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                '            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                '            " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId
                StrQ &= "  FROM  prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "   AND prtranhead_master.isvoid = 0 " & _
                            " LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                            "   AND cmclaim_process_tran.isvoid = 0 " & _
                            " LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                            "   AND cmexpense_master.isactive = 1 " & _
                            "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                            "  AND cmretire_process_tran.isvoid = 0 " & _
                            "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                            "  AND crretireexpense.isactive = 1 " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "
                StrQ &= " WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                             "	AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 " & _
                             "  AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " UNION ALL" & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             " , cfcommon_period_tran.period_name AS PeriodName " & _
                             " , hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= " FROM prpayrollprocess_tran " & _
                '            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                '            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                '            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '            " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                '            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                '            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                '            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid	AND prpayment_tran.isvoid = 0"
                StrQ &= " FROM prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "  AND prtranhead_master.isvoid = 0 " & _
                            " LEFT JOIN cmclaim_process_tran ON prpayrollprocess_tran.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                            "  AND cmclaim_process_tran.isvoid = 0 " & _
                            " LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                            "  AND cmexpense_master.isactive = 1 " & _
                            "LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                            "  AND cmretire_process_tran.isvoid = 0 " & _
                            "LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                            "  AND crretireexpense.isactive = 1 " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid	AND prpayment_tran.isvoid = 0"
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (09 Dec 2017) -- Start
                'SUPPORT 1722 - PACRA | STATUTORY REPORTS SHOWING LESSER GROSS PAY in 70.1.
                'StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                '             " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "
                StrQ &= " WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                             "	AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND cfcommon_period_tran.isactive = 1 " & _
                             "  AND ((prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (cmexpense_master.trnheadtype_id = " & CInt(enTranHeadType.EarningForEmployees) & ") OR (prpayrollprocess_tran.crretirementprocessunkid > 0 AND prpayrollprocess_tran.add_deduct IN (1)) ) "
                'Sohail (09 Jun 2021) - [crretirementprocessunkid]
                'Sohail (09 Dec 2017) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ) AS Gross " & _
                              "GROUP BY Gross.Periodid " & _
                              ", Gross.PeriodName " & _
                              " , Gross.Empid " & _
                             " ) AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid" & _
                              " LEFT JOIN ( " & _
                              "                   SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                              "                  , cfcommon_period_tran.period_name AS PeriodName " & _
                              "                  , hremployee_master.employeeunkid AS Empid " & _
                              "                  , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate )	* SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM    prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             " AND prtranhead_master.typeof_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             " AND cfcommon_period_tran.isactive = 1"
                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If
                'Sohail (28 Aug 2013) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
                             ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid " & _
                             ", prpayment_tran.expaidrate , prpayment_tran.baseexchangerate "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If


                StrQ &= " UNION ALL " & _
                             " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                             ", cfcommon_period_tran.period_name AS PeriodName " & _
                             ", hremployee_master.employeeunkid AS Empid " & _
                             ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= "	FROM  prpayrollprocess_tran " & _
                            " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                            " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                            " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                            " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                            " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                            " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             " AND prtranhead_master.typeof_id = 1 And ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             " AND cfcommon_period_tran.isactive = 1"
                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                             "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If
                'Sohail (28 Aug 2013) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	GROUP BY cfcommon_period_tran.periodunkid " & _
                             " , cfcommon_period_tran.period_name , hremployee_master.employeeunkid "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If

                StrQ &= " UNION ALL " & _
                            " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
                            ", cfcommon_period_tran.period_name AS PeriodName " & _
                            ", hremployee_master.employeeunkid AS Empid " & _
                            ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If

                StrQ &= " FROM    prpayrollprocess_tran " & _
                             " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                             " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
                             " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                             " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0 "
                'Sohail (21 Apr 2015) - [AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & ",  AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & "]

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (28 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
                '             " AND prtranhead_master.typeof_id = 1  AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                '             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                '             " AND cfcommon_period_tran.isactive = 1"
                StrQ &= " WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                             " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                            "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                             " AND cfcommon_period_tran.isactive = 1"

                If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                    StrQ &= "AND prpayrollprocess_tran.tranheadunkid = @OtherEarningTranId  "
                Else
                    StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
                End If
                'Sohail (28 Aug 2013) -- End

                If mintReportId = 1 Then 'PERIOD WISE
                    StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
                             ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid   "

                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
                End If


            End If


            StrQ &= ") AS BAS ON BAS.Empid = TableEmp_Contribution.EmpId " & _
                                                              "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
                    ") AS Contribution " & _
                    "WHERE 1 = 1 "


            If mintReportId = 1 Then 'PERIOD WISE
                StrQ &= "AND Periodid = @PeriodId "
            End If

            'Anjan [07 October 2016] -- Start
            'ENHANCEMENT : Include Ignore Zero Value.
            If mblnIgnoreZeroValue = True Then
                StrQ &= " AND Contribution.Total <> 0 "
            End If

            'Anjan [07 October 2016] -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Anjan [28 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            Dim dtTable As DataTable
            If mintReportId = 1 Then 'PERIOD WISE
                If Me.OrderByQuery.Trim <> "" Then
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                Else
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName, EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                End If
            Else 'MONTH WISE
                If Me.OrderByQuery.Trim <> "" Then
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName," & Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery & " ,end_date", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                Else
                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    'dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    If mblnShowGroupByCostCenter = True Then
                        dtTable = New DataView(dsList.Tables(0), "", "CCName, EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
                    End If
                    'Sohail (18 Apr 2014) -- End
                End If
            End If

            'Dim dtTable As DataTable
            'If mintReportId = 1 Then 'PERIOD WISE
            '    'Sohail (18 Jul 2013) -- Start
            '    'TRA - ENHANCEMENT
            '    'dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
            '    dtTable = New DataView(dsList.Tables(0), "", "EmpCode, Total DESC," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
            '    'Sohail (18 Jul 2013) -- End
            'Else 'MONTH WISE
            '    If Me.OrderByQuery.Trim <> "" Then
            '        'Sohail (18 Jul 2013) -- Start
            '        'TRA - ENHANCEMENT
            '        'dtTable = New DataView(dsList.Tables(0), "", "EmpCode," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
            '        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
            '        'Sohail (18 Jul 2013) -- End
            '    Else
            '        'Sohail (18 Jul 2013) -- Start
            '        'TRA - ENHANCEMENT
            '        'dtTable = New DataView(dsList.Tables(0), "", "EmpCode", DataViewRowState.CurrentRows).ToTable
            '        dtTable = New DataView(dsList.Tables(0), "", "EmpCode, end_date, Total DESC", DataViewRowState.CurrentRows).ToTable
            '        'Sohail (18 Jul 2013) -- End
            '    End If
            'End If
            'Anjan [28 Mar 2014] -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport


            Dim StrECode As String = String.Empty
            Dim StrPrevECode As String = String.Empty
            'Sohail (18 Apr 2014) -- Start
            'Enhancement - Show Group by Cost Center option on Pension Fund Report
            Dim StrCCCode As String = String.Empty
            Dim StrPrevCCCode As String = String.Empty
            'Sohail (18 Apr 2014) -- End
            Dim intCnt As Integer = 1
            Dim intEmpRowIdx As Integer = 0 'Sohail (28 Aug 2014)
            Dim dtFilterTable As DataTable = Nothing
            Dim mdicIsEmpAdded As New Dictionary(Of String, String)
            Dim rpt_Row As DataRow = Nothing
            Dim decColumn4Total, decColumn5Total, decColumn6Total, decColumn7Total, decColumn8Total As Decimal
            Dim decColumn4GrndTotal, decColumn5GrndTotal, decColumn6GrndTotal, decColumn7GrndTotal, decColumn8GrndTotal As Decimal

            decColumn4GrndTotal = 0 : decColumn5GrndTotal = 0 : decColumn6GrndTotal = 0 : decColumn7GrndTotal = 0 : decColumn8GrndTotal = 0

            For Each dtFRow As DataRow In dtTable.Rows
                If IsDBNull(dtFRow.Item("PeriodName")) = True Then Continue For

                StrECode = dtFRow.Item("EmpCode").ToString.Trim & "_" & dtFRow.Item("end_date").ToString.Trim
                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                If mintReportId = 1 Then
                    StrCCCode = dtFRow.Item("CCCode").ToString
                Else
                    'Sohail (28 Aug 2014) -- Start
                    'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                    'StrCCCode = ""
                    StrCCCode = dtFRow.Item("EmpCode").ToString.Trim
                    'Sohail (28 Aug 2014) -- End
                End If
                If StrPrevCCCode <> StrCCCode Then
                    decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
                End If
                'Sohail (18 Apr 2014) -- End

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'If StrPrevECode <> StrECode Then
                '    If mintReportId = 2 Then 'MONTH WISE
                '        decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
                '    End If
                '    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                '    If CBool(dtFRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                '        rpt_Row.Item("Column10") = dtFRow.Item("MembershipNo")
                '    Else
                '        rpt_Row.Item("Column10") = ""
                '    End If

                'Else
                '    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(rpt_Data.Tables("ArutiTable").Rows.Count - 1)
                '    rpt_Row.Item("Column10") = dtFRow.Item("MembershipNo")
                '    rpt_Row.AcceptChanges()

                '    StrPrevECode = StrECode
                '    StrPrevCCCode = StrCCCode 'Sohail (18 Apr 2014)
                '    Continue For

                'End If
                If StrPrevECode <> StrECode Then
                    intEmpRowIdx = rpt_Data.Tables("ArutiTable").Rows.Count
                End If
                If StrPrevECode = StrECode AndAlso CBool(dtFRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                    rpt_Row = rpt_Data.Tables("ArutiTable").Rows(intEmpRowIdx)

                    rpt_Row.Item("Column10") = dtFRow.Item("MembershipNo")
                    rpt_Row.AcceptChanges()

                    StrPrevECode = StrECode
                    StrPrevCCCode = StrCCCode
                    Continue For
                Else
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    If CBool(dtFRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                        rpt_Row.Item("Column10") = dtFRow.Item("MembershipNo")
                    Else
                        rpt_Row.Item("Column10") = ""
                    End If
                End If
                'Sohail (28 Aug 2014) -- End


                Select Case mintReportId
                    Case 1  'PERIOD WISE
                        rpt_Row.Item("Column1") = intCnt.ToString
                        rpt_Row.Item("Column8") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
                        rpt_Row.Item("Column3") = dtFRow.Item("EmpName")
                        If CBool(dtFRow.Item("NonPayrollMembership")) = True Then 'Non-Payroll Membership
                            rpt_Row.Item("Column2") = ""
                        Else
                            rpt_Row.Item("Column2") = dtFRow.Item("MembershipNo")
                        End If
                        intCnt += 1
                    Case 2  'MONTH WISE
                        rpt_Row.Item("Column1") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
                        rpt_Row.Item("Column3") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
                        rpt_Row.Item("Column2") = dtFRow.Item("PeriodName")
                End Select
                rpt_Row.Item("Column4") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column6") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = dtFRow.Item("EmpCode")

                rpt_Row.Item("Column81") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
                rpt_Row.Item("Column82") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
                rpt_Row.Item("Column83") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column84") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
                rpt_Row.Item("Column85") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)


                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                If mblnShowGroupByCostCenter = True Then
                    rpt_Row.Item("Column13") = dtFRow.Item("CCCode")
                    rpt_Row.Item("Column14") = dtFRow.Item("CCName")
                Else
                    rpt_Row.Item("Column13") = ""
                    rpt_Row.Item("Column14") = ""
                End If
                'Sohail (18 Apr 2014) -- End

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'decColumn4Total = decColumn4Total + CDec(dtFRow.Item("BasicSal"))
                'decColumn5Total = decColumn5Total + CDec(dtFRow.Item("GrossPay"))
                If StrPrevECode <> StrECode Then
                    decColumn4Total = decColumn4Total + CDec(dtFRow.Item("BasicSal"))
                    decColumn5Total = decColumn5Total + CDec(dtFRow.Item("GrossPay"))
                End If
                'Sohail (28 Aug 2014) -- End
                decColumn6Total = decColumn6Total + CDec(dtFRow.Item("EmpContrib"))
                decColumn7Total = decColumn7Total + CDec(dtFRow.Item("EmprContrib"))
                decColumn8Total = decColumn8Total + CDec(dtFRow.Item("Total"))

                'Sohail (28 Aug 2014) -- Start
                'Issue - Showing only only one Payroll Membership even if more than one Payroll Membership assigned.
                'decColumn4GrndTotal = decColumn4GrndTotal + CDec(dtFRow.Item("BasicSal"))
                'decColumn5GrndTotal = decColumn5GrndTotal + CDec(dtFRow.Item("GrossPay"))
                If StrPrevECode <> StrECode Then
                    decColumn4GrndTotal = decColumn4GrndTotal + CDec(dtFRow.Item("BasicSal"))
                    decColumn5GrndTotal = decColumn5GrndTotal + CDec(dtFRow.Item("GrossPay"))
                End If
                'Sohail (28 Aug 2014) -- End
                decColumn6GrndTotal = decColumn6GrndTotal + CDec(dtFRow.Item("EmpContrib"))
                decColumn7GrndTotal = decColumn7GrndTotal + CDec(dtFRow.Item("EmprContrib"))
                decColumn8GrndTotal = decColumn8GrndTotal + CDec(dtFRow.Item("Total"))

                rpt_Row.Item("Column76") = Format(decColumn4Total, GUI.fmtCurrency)
                rpt_Row.Item("Column77") = Format(decColumn5Total, GUI.fmtCurrency)
                rpt_Row.Item("Column78") = Format(decColumn6Total, GUI.fmtCurrency)
                rpt_Row.Item("Column79") = Format(decColumn7Total, GUI.fmtCurrency)
                rpt_Row.Item("Column80") = Format(decColumn8Total, GUI.fmtCurrency)

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

                StrPrevECode = StrECode
                StrPrevCCCode = StrCCCode 'Sohail (18 Apr 2014)

            Next

            Select Case mintReportId
                Case 1
                    objRpt = New ArutiReport.Designer.rptPensionFundReport
                Case 2
                    objRpt = New ArutiReport.Designer.rptPensionFundReport_Month
            End Select

            objRpt.SetDataSource(rpt_Data)

            If mintReportId = 1 Then
                Call ReportFunction.TextChange(objRpt, "txtSchedular", Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName)
                Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber)

                Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
                Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 3, "Member Number"))
                Call ReportFunction.TextChange(objRpt, "txtMemberName", Language.getMessage(mstrModuleName, 20, "Member's Name"))
                Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 21, "Employee Code"))
                Call ReportFunction.TextChange(objRpt, "txtNonPayrollMembership", Language.getMessage(mstrModuleName, 33, "Other Member Number"))
                'Sohail (31 Mar 2014) -- Start
                'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
                If mblnShowEmployeeCode = False Then
                    Call ReportFunction.EnableSuppress(objRpt, "txtEmpCode", True)
                    Call ReportFunction.EnableSuppress(objRpt, "Column91", True)
                    objRpt.ReportDefinition.ReportObjects("txtMemberNo").Width = objRpt.ReportDefinition.ReportObjects("txtMemberNo").Width + objRpt.ReportDefinition.ReportObjects("txtEmpCode").Width
                    objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column91").Width
                End If
                If mblnShowOtherMembershipNo = False Then
                    Call ReportFunction.EnableSuppress(objRpt, "txtNonPayrollMembership", True)
                    'Nilay (13-Oct-2016) -- Start
                    'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
                    Call ReportFunction.EnableSuppress(objRpt, "Column101", True)
                    'Nilay (13-Oct-2016) -- End
                End If
                'Sohail (31 Mar 2014) -- End
            Else
                Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee :"))
                Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 22, "Month"))
                Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 23, "Group Total :"))
            End If


            Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 24, "Basic Salary"))
            Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay"))
            Call ReportFunction.TextChange(objRpt, "txtMemberContrib", Language.getMessage(mstrModuleName, 25, "Member's Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 28, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

            If mintReportId = 1 Then
                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                'Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7Total, GUI.fmtCurrency))
                'Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8Total, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8GrndTotal, GUI.fmtCurrency))
                'Sohail (18 Apr 2014) -- End

                'Sohail (18 Apr 2014) -- Start
                'Enhancement - Show Group by Cost Center option on Pension Fund Report
                ReportFunction.TextChange(objRpt, "txtCostCenter", Language.getMessage(mstrModuleName, 37, "Cost Center : "))
                ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 38, "Sub Total :"))
                If mblnShowGroupByCostCenter = True Then
                    Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", False)
                    Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", False)
                Else
                    Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", True)
                    Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", True)
                End If
                'Sohail (18 Apr 2014) -- End
            Else
                Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7GrndTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8GrndTotal, GUI.fmtCurrency))
            End If


            If mblnShowBasicSalary = False Then
                Dim strCurrentHdr As String = String.Empty
                If mintReportId = 1 Then
                    ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
                    ReportFunction.EnableSuppress(objRpt, "Column81", True)
                    ReportFunction.EnableSuppress(objRpt, "Column761", True) 'Sohail (18 Apr 2014)
                    strCurrentHdr = "txtMemberName"

                    ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 1356)
                    ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
                    ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
                    ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay") 'Sohail (18 Apr 2014)
                Else
                    ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                    ReportFunction.EnableSuppress(objRpt, "Column31", True)
                    ReportFunction.EnableSuppress(objRpt, "Column761", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
                    strCurrentHdr = "txtMonth"

                    ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 2160)
                    ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
                    ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay")
                    ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
                End If
            Else
                If mintReportId = 1 Then
                    ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
                    ReportFunction.EnableSuppress(objRpt, "Column41", True)
                    ReportFunction.EnableSuppress(objRpt, "Column771", True) 'Sohail (18 Apr 2014)

                    ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMemberName", 10, 1356)
                    ReportFunction.SetColumnOn(objRpt, "Column81", "txtBasicSal")
                    ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
                    ReportFunction.SetColumnOn(objRpt, "Column761", "txtBasicSal") 'Sohail (18 Apr 2014)
                Else
                    ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                    ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
                    ReportFunction.EnableSuppress(objRpt, "Column41", True)
                    ReportFunction.EnableSuppress(objRpt, "Column771", True)

                    ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMonth", 10, 2160)
                    ReportFunction.SetColumnOn(objRpt, "Column31", "txtBasicSal")
                    ReportFunction.SetColumnOn(objRpt, "Column761", "txtBasicSal")
                    ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
                End If
            End If

            'Anjan [12 November 2014] -- Start
            'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
            If mblnDoNotShowBasic_GrossSalary = True Then
                Call ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
                If mintReportId = 1 Then
                    Call ReportFunction.EnableSuppress(objRpt, "Column81", True)
                    objRpt.ReportDefinition.ReportObjects("txtMemberName").Width = objRpt.ReportDefinition.ReportObjects("txtBasicSal").Width + objRpt.ReportDefinition.ReportObjects("txtGrossPay").Width
                    objRpt.ReportDefinition.ReportObjects("Column31").Width = objRpt.ReportDefinition.ReportObjects("Column31").Width + objRpt.ReportDefinition.ReportObjects("Column81").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
                Else
                    Call ReportFunction.EnableSuppress(objRpt, "Column31", True)

                    objRpt.ReportDefinition.ReportObjects("txtMonth").Width = objRpt.ReportDefinition.ReportObjects("txtBasicSal").Width + objRpt.ReportDefinition.ReportObjects("txtGrossPay").Width
                    objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column31").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width
                End If

                Call ReportFunction.EnableSuppress(objRpt, "Column761", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)

                Call ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column41", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column771", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
            End If
            'Anjan [12 November 2014] -- End



            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = rpt_Data.Tables(0)
                Dim intColIdx As Integer = -1

                Select Case mintReportId
                    Case 1  'PERIOD WISE
                        mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 1, "Sr. No.")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column1").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 3, "Member Number")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column2").SetOrdinal(intColIdx)

                        If mblnShowEmployeeCode = True Then 'Sohail (18 Apr 2014)
                            mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 21, "Employee Code")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column9").SetOrdinal(intColIdx)
                        End If 'Sohail (18 Apr 2014)

                        mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 20, "Member's Name")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column3").SetOrdinal(intColIdx)

                        If mblnShowBasicSalary = True Then
                            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 24, "Basic Salary")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column81").SetOrdinal(intColIdx)
                        Else
                            mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 4, "Gross Pay")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column82").SetOrdinal(intColIdx)
                        End If

                        mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 25, "Member's Contribution")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column83").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 6, "Employer's Contribution")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column84").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column85").Caption = Language.getMessage(mstrModuleName, 7, "Total")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column85").SetOrdinal(intColIdx)

                        If mblnShowOtherMembershipNo = True Then 'Sohail (18 Apr 2014)
                            mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 33, "Other Member Number")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column10").SetOrdinal(intColIdx)
                        End If 'Sohail (18 Apr 2014)

                        'Sohail (18 Apr 2014) -- Start
                        'Enhancement - Show Group by Cost Center option on Pension Fund Report
                        If mblnShowGroupByCostCenter = True Then
                            mdtTableExcel.Columns("Column14").Caption = Language.getMessage(mstrModuleName, 37, "Cost Center :")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column14").SetOrdinal(intColIdx)
                        End If
                        'Sohail (18 Apr 2014) -- End

                        For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                            mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
                        Next
                    Case 2  'MONTH WISE
                        mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Month")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column2").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 31, "Employee ")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column1").SetOrdinal(intColIdx)

                        If mblnShowBasicSalary = True Then
                            mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 24, "Basic Salary")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column81").SetOrdinal(intColIdx)
                        Else
                            mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 4, "Gross Pay")
                            intColIdx += 1
                            mdtTableExcel.Columns("Column82").SetOrdinal(intColIdx)
                        End If

                        mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 25, "Member's Contribution")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column83").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 6, "Employer's Contribution")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column84").SetOrdinal(intColIdx)

                        mdtTableExcel.Columns("Column85").Caption = Language.getMessage(mstrModuleName, 7, "Total")
                        intColIdx += 1
                        mdtTableExcel.Columns("Column85").SetOrdinal(intColIdx)


                        For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                            mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
                        Next
                End Select


            End If

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



#Region " Generate_DetailReport before generate Statutory reports with Include Membership Combo (Non Payroll Membership in seperate column) on (18 Jul 2012) "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal


    '        'Pinkal (02-May-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If mintBaseCurrId = mintPaidCurrencyId Then

    '            StrQ = "SELECT  PeriodName AS PeriodName " & _
    '                          ", EmpId " & _
    '                    "	,EmpName AS EmpName " & _
    '                          ", end_date " & _
    '                    "	,MembershipNo AS MembershipNo " & _
    '                    "	,MembershipName AS MembershipName " & _
    '                    "	,GrossPay AS GrossPay " & _
    '                    "	,EmpContrib AS EmpContrib " & _
    '                    "	,EmprContrib AS EmprContrib " & _
    '                    "	,Total AS Total " & _
    '                    "	,BasicSal AS BasicSal " & _
    '                    "	,EmpCode AS EmpCode " & _
    '                    "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
    '                    "		,TableEmp_Contribution.PeriodName " & _
    '                                      ", TableEmp_Contribution.end_date " & _
    '                                      ", TableEmp_Contribution.EmpId " & _
    '                                      ", TableEmp_Contribution.EmpName " & _
    '                                      ", TableEmp_Contribution.Membership_Name AS MembershipName " & _
    '                                      ", TableEmp_Contribution.Membership_No AS MembershipNo " & _
    '                                      ", TableEmp_Contribution.MembershipUnkId AS MemId " & _
    '                    "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '                    "		,TableEmployer_Contribution.EmployerHead " & _
    '                    "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '                                      ", ISNULL(TableEmp_Contribution.Amount, 0) " & _
    '                                        "+ ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '                    "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '                    "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '                                      ", TableEmp_Contribution.EmpCode AS EmpCode " & _
    '                    "		,TableEmp_Contribution.StDate AS StDate " & _
    '                              "FROM      ( SELECT  ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                                                          ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                                                          ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'01-Jan-1900'), 112) AS end_date " & _
    '                    "			,hremployee_master.employeeunkid AS Empid " & _
    '                                                           ", hremployee_master.employeecode AS EmpCode " & _
    '                                                           ", ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                    "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                           ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
    '                                                           ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '                                                           ", hrmembership_master.membershipname AS Membership_Name " & _
    '                    "			,cfcommon_period_tran.start_date AS StDate "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "		FROM prpayrollprocess_tran " & _
    '                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                              "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                                                                                                            "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                                                              "JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                              "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= mstrAnalysis_Join
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "			AND cfcommon_period_tran.isactive = 1 " & _
    '                                    "AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '"AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= "                                      AND prtnaleave_tran.payperiodunkid = @PeriodId "
    '            End If

    '            StrQ &= "                               ) AS TableEmp_Contribution " & _
    '                                        "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                    "			,prtranhead_master.trnheadname AS EmployerHead " & _
    '                    "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                                                           ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                    "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                           ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
    '                                                           ", hrmembership_master.membershipunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "		FROM prpayrollprocess_tran " & _
    '                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                              "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                    "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                                                            "AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                                                              "JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                                                              "AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                                                              "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                              "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= mstrAnalysis_Join
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                              "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                              "AND cfcommon_period_tran.isactive = 1 " & _
    '                                    "AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '"AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= "                                      AND prtnaleave_tran.payperiodunkid = @PeriodId "
    '            End If

    '            StrQ &= "                               ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
    '                                                                                  "AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
    '                                                                                  "AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '                                        "LEFT JOIN ( SELECT  Gross.Periodid " & _
    '                    "			,Gross.PeriodName " & _
    '                    "			,Gross.Empid " & _
    '                    "			,SUM(Gross.Amount) AS GrossPay " & _
    '                                                    "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
    '                    "				,cfcommon_period_tran.period_name AS PeriodName " & _
    '                    "				,hremployee_master.employeeunkid AS Empid " & _
    '                                                                      ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "			FROM prpayrollprocess_tran " & _
    '                    "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= mstrAnalysis_Join
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                                                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                                        "AND cfcommon_period_tran.isactive = 1 "

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= "                                                AND cfcommon_period_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= "                                        ) AS Gross " & _
    '                                                    "GROUP BY Gross.Periodid " & _
    '                                                          ", Gross.PeriodName " & _
    '                                                          ", Gross.Empid " & _
    '                                                  ") AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid " & _
    '                                                                     "AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
    '                                        "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                    "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '                    "			,hremployee_master.employeeunkid AS Empid " & _
    '                                                          ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "		FROM prpayrollprocess_tran " & _
    '                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= mstrAnalysis_Join
    '            'Sohail (18 Mar 2013) -- End

    '            StrQ &= "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                    "			AND prtranhead_master.typeof_id = 1 " & _
    '                                                            "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                            "AND cfcommon_period_tran.isactive = 1 "

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= "                                    AND cfcommon_period_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= "                               GROUP BY cfcommon_period_tran.periodunkid " & _
    '                                                          ", cfcommon_period_tran.period_name " & _
    '                                                          ", hremployee_master.employeeunkid "

    '            'Sohail (18 Mar 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If
    '            'Sohail (18 Mar 2013) -- End

    '        ElseIf mintBaseCurrId <> mintPaidCurrencyId Then  ' FOR DIFFERENT CURRENCY 

    '            StrQ = "SELECT  PeriodName AS PeriodName " & _
    '                       ", EmpId " & _
    '                       "	,EmpName AS EmpName " & _
    '                       ", end_date " & _
    '                       "	,MembershipNo AS MembershipNo " & _
    '                       "	,MembershipName AS MembershipName " & _
    '                       "	,GrossPay AS GrossPay " & _
    '                       "	,EmpContrib AS EmpContrib " & _
    '                       "	,EmprContrib AS EmprContrib " & _
    '                       "	,Total AS Total " & _
    '                       "	,BasicSal AS BasicSal " & _
    '                       "	,EmpCode AS EmpCode " & _
    '                 "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
    '                 "		,TableEmp_Contribution.PeriodName " & _
    '                                   ", TableEmp_Contribution.end_date " & _
    '                                   ", TableEmp_Contribution.EmpId " & _
    '                                   ", TableEmp_Contribution.EmpName " & _
    '                                   ", TableEmp_Contribution.Membership_Name AS MembershipName " & _
    '                                   ", TableEmp_Contribution.Membership_No AS MembershipNo " & _
    '                                   ", TableEmp_Contribution.MembershipUnkId AS MemId " & _
    '                 "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '                 "		,TableEmployer_Contribution.EmployerHead " & _
    '                 "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '                                   ", ISNULL(TableEmp_Contribution.Amount, 0) " & _
    '                                     "+ ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '                 "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '                 "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '                                   ", TableEmp_Contribution.EmpCode AS EmpCode " & _
    '                 "		,TableEmp_Contribution.StDate AS StDate " & _
    '                " FROM (  " & _
    '               "               SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '               "              , ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '               "              , CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '01-Jan-1900'), 112) AS end_date " & _
    '               "              , hremployee_master.employeeunkid AS Empid " & _
    '               "              , hremployee_master.employeecode AS EmpCode " & _
    '               "              , ISNULL(hremployee_master.firstname, '') + ' '  + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '               "              , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36," & decDecimalPlaces & ")) AS Amount " & _
    '               "              , hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
    '               "              , hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '               "              , hrmembership_master.membershipname AS Membership_Name " & _
    '               "              , cfcommon_period_tran.start_date AS StDate "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM   prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                         " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                         " JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                         " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                         " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '                         " AND cfcommon_period_tran.isactive = 1 " & _
    '                         " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= " UNION ALL  " & _
    '                         " SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                         ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                         ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '01-Jan-1900'), 112) AS end_date " & _
    '                         ", hremployee_master.employeeunkid AS Empid " & _
    '                         ", hremployee_master.employeecode AS EmpCode " & _
    '                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                         ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount " & _
    '                         ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
    '                         ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '                         ", hrmembership_master.membershipname AS Membership_Name " & _
    '                         ", cfcommon_period_tran.start_date AS StDate"

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "  FROM   prpayrollprocess_tran " & _
    '                          " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                          " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                          " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                          " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                          "JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                          " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                          " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                          " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                          " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid  " & _
    '                          " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                          " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '                       " AND cfcommon_period_tran.isactive = 1 " & _
    '                       " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId AND prpayment_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= " UNION ALL " & _
    '                          " SELECT ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                          " , ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                          ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date, '01-Jan-1900'), 112) AS end_date " & _
    '                          ", hremployee_master.employeeunkid AS Empid " & _
    '                          ", hremployee_master.employeecode AS EmpCode " & _
    '                          ", ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                          ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                          ", hremployee_meminfo_tran.membershipunkid AS MembershipUnkId " & _
    '                          ", hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '                          ", hrmembership_master.membershipname AS Membership_Name " & _
    '                          ", cfcommon_period_tran.start_date AS StDate"

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM   prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                         " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
    '                         " JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                         " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                         " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                         " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE  ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '                     " AND cfcommon_period_tran.isactive = 1 " & _
    '                     " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
    '            End If

    '            StrQ &= ") AS TableEmp_Contribution " & _
    '                         " LEFT JOIN ( " & _
    '                            " SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                            " , prtranhead_master.trnheadname AS EmployerHead " & _
    '                            ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                            ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                            ", ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                            ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
    '                            ", hrmembership_master.membershipunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM  prpayrollprocess_tran " & _
    '                        " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                        " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                        " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                        " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 " & _
    '                        " AND prpayment_tran.countryunkid = " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                        " AND cfcommon_period_tran.isactive = 1 " & _
    '                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= " 	UNION ALL" & _
    '                         " SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                         ", prtranhead_master.trnheadname AS EmployerHead " & _
    '                         ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                         ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                         ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                         ", hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
    '                         ", hrmembership_master.membershipunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                         " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " AND ISNULL(hremployee_meminfo_tran.isdeleted,0) = 0 " & _
    '                         " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                         " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                         " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                         " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                         " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                         " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                        " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                        " AND cfcommon_period_tran.isactive = 1 " & _
    '                        " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId "
    '            End If

    '            StrQ &= " UNION ALL " & _
    '                        " SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                        ", prtranhead_master.trnheadname AS EmployerHead " & _
    '                        ", prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                        ", ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                        ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                        " , hremployee_meminfo_tran.membershipunkid AS MembershipId " & _
    '                        ", hrmembership_master.membershipunkid"

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM prpayrollprocess_tran " & _
    '                        " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
    '                        " AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " AND ISNULL(hremployee_meminfo_tran.isdeleted,	0) = 0 " & _
    '                        " JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
    '                        " AND hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0 "

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                     " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                     " AND cfcommon_period_tran.isactive = 1 " & _
    '                     " AND hrmembership_master.membershipunkid IN (" & mstrMembershipIDs & ") "
    '            '" AND hrmembership_master.membershipunkid = @MemId " 'Sohail (18 May 2013)

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND prtnaleave_tran.payperiodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL "
    '            End If

    '            StrQ &= " ) AS TableEmployer_Contribution ON TableEmp_Contribution.Empid = TableEmployer_Contribution.Empid " & _
    '                         " AND TableEmp_Contribution.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
    '                         " AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid" & _
    '                         " LEFT JOIN ( " & _
    '                         "                  SELECT  Gross.Periodid " & _
    '                         "                  , Gross.PeriodName " & _
    '                         "                  , Gross.Empid " & _
    '                         "                  , SUM(Gross.Amount) AS GrossPay " & _
    '                         "                  FROM (  " & _
    '                         "                               SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                         "                               , cfcommon_period_tran.period_name AS PeriodName " & _
    '                         "                               , hremployee_master.employeeunkid AS Empid " & _
    '                         "                              , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate ) * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & " )) AS Amount "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "             FROM    prpayrollprocess_tran " & _
    '                         "             JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         "             JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                         "             JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         "             JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         "             JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                         "             AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid  AND prpayment_tran.isvoid = 0    " & _
    '                         "             AND prpayment_tran.countryunkid = " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= "	WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         "	AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         "  AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  AND cfcommon_period_tran.isactive = 1 "

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
    '            End If

    '            StrQ &= " UNION ALL" & _
    '                         " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                         ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                         ", hremployee_master.employeeunkid AS Empid " & _
    '                         ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "  FROM  prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                        " AND prpayment_tran.isvoid = 0  AND prpayment_tran.countryunkid <> " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
    '            End If

    '            StrQ &= " UNION ALL" & _
    '                         " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                         " , cfcommon_period_tran.period_name AS PeriodName " & _
    '                         " , hremployee_master.employeeunkid AS Empid " & _
    '                         ", " & mdecConversionRate & " * CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM prpayrollprocess_tran " & _
    '                        " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid	AND prpayment_tran.isvoid = 0"

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         " AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                         " AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 AND cfcommon_period_tran.isactive = 1 "

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
    '            End If

    '            StrQ &= " ) AS Gross " & _
    '                          "GROUP BY Gross.Periodid " & _
    '                          ", Gross.PeriodName " & _
    '                          " , Gross.Empid " & _
    '                         " ) AS TableGross ON TableEmp_Contribution.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid" & _
    '                          " LEFT JOIN ( " & _
    '                          "                   SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                          "                  , cfcommon_period_tran.period_name AS PeriodName " & _
    '                          "                  , hremployee_master.employeeunkid AS Empid " & _
    '                          "                  , ( prpayment_tran.expaidrate / prpayment_tran.baseexchangerate )	* SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM    prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                        " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         " AND prtranhead_master.typeof_id = 1 AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         " AND cfcommon_period_tran.isactive = 1"

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
    '            End If

    '            StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
    '                         ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid " & _
    '                         ", prpayment_tran.expaidrate , prpayment_tran.baseexchangerate "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If


    '            StrQ &= " UNION ALL " & _
    '                         " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                         ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                         ", hremployee_master.employeeunkid AS Empid " & _
    '                         ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "	FROM  prpayrollprocess_tran " & _
    '                        " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                        " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                        " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                        " JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                        " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
    '                        " AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid <> " & mintCountryId

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         " AND prtranhead_master.typeof_id = 1 And ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         " AND cfcommon_period_tran.isactive = 1"

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.periodunkid = @PeriodId  "
    '            End If

    '            StrQ &= "	GROUP BY cfcommon_period_tran.periodunkid " & _
    '                         " , cfcommon_period_tran.period_name , hremployee_master.employeeunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If

    '            StrQ &= " UNION ALL " & _
    '                        " SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                        ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                        ", hremployee_master.employeeunkid AS Empid " & _
    '                        ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ", 0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= " FROM    prpayrollprocess_tran " & _
    '                         " JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                         " JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                         " JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                         " JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                         " LEFT JOIN prpayment_tran ON prpayment_tran.employeeunkid = prtnaleave_tran.employeeunkid " & _
    '                         " AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid AND prpayment_tran.isvoid = 0"

    '            StrQ &= mstrAnalysis_Join

    '            StrQ &= " WHERE   prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & _
    '                         " AND prtranhead_master.typeof_id = 1  AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                         " AND ISNULL(prtranhead_master.isvoid, 0) = 0 AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                         " AND cfcommon_period_tran.isactive = 1"

    '            If mintReportId = 1 Then 'PERIOD WISE
    '                StrQ &= " AND cfcommon_period_tran.periodunkid = @PeriodId  AND prpayment_tran.paymenttranunkid IS NULL  "
    '            End If

    '            StrQ &= " GROUP BY cfcommon_period_tran.periodunkid " & _
    '                         ", cfcommon_period_tran.period_name, hremployee_master.employeeunkid   "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If


    '        End If


    '        StrQ &= ") AS BAS ON BAS.Empid = TableEmp_Contribution.EmpId " & _
    '                                                          "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '                ") AS Contribution " & _
    '                "WHERE 1 = 1 "


    '        If mintReportId = 1 Then 'PERIOD WISE
    '            StrQ &= "AND Periodid = @PeriodId "
    '        End If


    '        'Pinkal (02-May-2013) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtTable As DataTable
    '        If mintReportId = 1 Then 'PERIOD WISE
    '            dtTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
    '        Else 'MONTH WISE
    '            If Me.OrderByQuery.Trim <> "" Then
    '                dtTable = New DataView(dsList.Tables(0), "", "EmpCode," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = New DataView(dsList.Tables(0), "", "EmpCode", DataViewRowState.CurrentRows).ToTable
    '            End If
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim StrECode As String = String.Empty
    '        Dim StrPrevECode As String = String.Empty
    '        Dim intCnt As Integer = 1
    '        Dim dtFilterTable As DataTable = Nothing
    '        Dim mdicIsEmpAdded As New Dictionary(Of String, String)
    '        Dim rpt_Row As DataRow = Nothing
    '        Dim decColumn4Total, decColumn5Total, decColumn6Total, decColumn7Total, decColumn8Total As Decimal
    '        Dim decColumn4GrndTotal, decColumn5GrndTotal, decColumn6GrndTotal, decColumn7GrndTotal, decColumn8GrndTotal As Decimal

    '        decColumn4GrndTotal = 0 : decColumn5GrndTotal = 0 : decColumn6GrndTotal = 0 : decColumn7GrndTotal = 0 : decColumn8GrndTotal = 0

    '        For Each dtFRow As DataRow In dtTable.Rows
    '            StrECode = dtFRow.Item("EmpCode").ToString.Trim

    '            If mintReportId = 2 AndAlso StrPrevECode <> StrECode Then 'MONTH WISE
    '                decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
    '            End If

    '            If IsDBNull(dtFRow.Item("PeriodName")) = True Then Continue For

    '            rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '            Select Case mintReportId
    '                Case 1  'PERIOD WISE
    '                    rpt_Row.Item("Column1") = intCnt.ToString
    '                    rpt_Row.Item("Column8") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column3") = dtFRow.Item("EmpName")
    '                    rpt_Row.Item("Column2") = dtFRow.Item("MembershipNo")
    '                    intCnt += 1
    '                Case 2  'MONTH WISE
    '                    rpt_Row.Item("Column1") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
    '                    rpt_Row.Item("Column3") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column2") = dtFRow.Item("PeriodName")
    '            End Select
    '            rpt_Row.Item("Column4") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column5") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column6") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column7") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column9") = dtFRow.Item("EmpCode")

    '            'Sohail (12 Jan 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            rpt_Row.Item("Column81") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column82") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column83") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column84") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
    '            rpt_Row.Item("Column85") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)
    '            'Sohail (12 Jan 2013) -- End


    '            decColumn4Total = decColumn4Total + CDec(dtFRow.Item("BasicSal"))
    '            decColumn5Total = decColumn5Total + CDec(dtFRow.Item("GrossPay"))
    '            decColumn6Total = decColumn6Total + CDec(dtFRow.Item("EmpContrib"))
    '            decColumn7Total = decColumn7Total + CDec(dtFRow.Item("EmprContrib"))
    '            decColumn8Total = decColumn8Total + CDec(dtFRow.Item("Total"))

    '            decColumn4GrndTotal = decColumn4GrndTotal + CDec(dtFRow.Item("BasicSal"))
    '            decColumn5GrndTotal = decColumn5GrndTotal + CDec(dtFRow.Item("GrossPay"))
    '            decColumn6GrndTotal = decColumn6GrndTotal + CDec(dtFRow.Item("EmpContrib"))
    '            decColumn7GrndTotal = decColumn7GrndTotal + CDec(dtFRow.Item("EmprContrib"))
    '            decColumn8GrndTotal = decColumn8GrndTotal + CDec(dtFRow.Item("Total"))

    '            rpt_Row.Item("Column76") = Format(decColumn4Total, GUI.fmtCurrency)
    '            rpt_Row.Item("Column77") = Format(decColumn5Total, GUI.fmtCurrency)
    '            rpt_Row.Item("Column78") = Format(decColumn6Total, GUI.fmtCurrency)
    '            rpt_Row.Item("Column79") = Format(decColumn7Total, GUI.fmtCurrency)
    '            rpt_Row.Item("Column80") = Format(decColumn8Total, GUI.fmtCurrency)

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)


    '            StrPrevECode = StrECode

    '        Next

    '        Select Case mintReportId
    '            Case 1
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport
    '            Case 2
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport_Month
    '        End Select

    '        objRpt.SetDataSource(rpt_Data)

    '        If mintReportId = 1 Then
    '            Call ReportFunction.TextChange(objRpt, "txtSchedular", Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName)
    '            Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber)

    '            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 3, "Member Number"))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberName", Language.getMessage(mstrModuleName, 20, "Member's Name"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 21, "Employee Code"))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 22, "Month"))
    '            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 23, "Group Total :"))
    '        End If


    '        Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 24, "Basic Salary"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberContrib", Language.getMessage(mstrModuleName, 25, "Member's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 28, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

    '        If mintReportId = 1 Then
    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8Total, GUI.fmtCurrency))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8GrndTotal, GUI.fmtCurrency))
    '        End If


    '        If mblnShowBasicSalary = False Then
    '            Dim strCurrentHdr As String = String.Empty
    '            If mintReportId = 1 Then
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column81", True)
    '                strCurrentHdr = "txtMemberName"

    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 1356)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '            Else
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column31", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column761", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                strCurrentHdr = "txtMonth"

    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 2160)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '            End If
    '        Else
    '            If mintReportId = 1 Then
    '                ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column41", True)

    '                ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMemberName", 10, 1356)
    '                ReportFunction.SetColumnOn(objRpt, "Column81", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
    '            Else
    '                ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column41", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column771", True)

    '                ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMonth", 10, 2160)
    '                ReportFunction.SetColumnOn(objRpt, "Column31", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "Column761", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
    '            End If
    '        End If

    '        'Sohail (12 Jan 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If menExportAction = enExportAction.ExcelExtra Then
    '            mdtTableExcel = rpt_Data.Tables(0)
    '            Dim intColIdx As Integer = -1

    '            Select Case mintReportId
    '                Case 1  'PERIOD WISE
    '                    mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 1, "Sr. No.")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column1").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 3, "Member Number")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column2").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 21, "Employee Code")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column9").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 20, "Member's Name")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column3").SetOrdinal(intColIdx)

    '                    If mblnShowBasicSalary = True Then
    '                        mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 24, "Basic Salary")
    '                        intColIdx += 1
    '                        mdtTableExcel.Columns("Column81").SetOrdinal(intColIdx)
    '                    Else
    '                        mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 4, "Gross Pay")
    '                        intColIdx += 1
    '                        mdtTableExcel.Columns("Column82").SetOrdinal(intColIdx)
    '                    End If

    '                    mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 25, "Member's Contribution")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column83").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 6, "Employer's Contribution")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column84").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column85").Caption = Language.getMessage(mstrModuleName, 7, "Total")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column85").SetOrdinal(intColIdx)


    '                    For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
    '                        mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
    '                    Next
    '                Case 2  'MONTH WISE
    '                    mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 22, "Month")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column2").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column1").Caption = Language.getMessage(mstrModuleName, 31, "Employee ")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column1").SetOrdinal(intColIdx)

    '                    If mblnShowBasicSalary = True Then
    '                        mdtTableExcel.Columns("Column81").Caption = Language.getMessage(mstrModuleName, 24, "Basic Salary")
    '                        intColIdx += 1
    '                        mdtTableExcel.Columns("Column81").SetOrdinal(intColIdx)
    '                    Else
    '                        mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 4, "Gross Pay")
    '                        intColIdx += 1
    '                        mdtTableExcel.Columns("Column82").SetOrdinal(intColIdx)
    '                    End If

    '                    mdtTableExcel.Columns("Column83").Caption = Language.getMessage(mstrModuleName, 25, "Member's Contribution")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column83").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column84").Caption = Language.getMessage(mstrModuleName, 6, "Employer's Contribution")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column84").SetOrdinal(intColIdx)

    '                    mdtTableExcel.Columns("Column85").Caption = Language.getMessage(mstrModuleName, 7, "Total")
    '                    intColIdx += 1
    '                    mdtTableExcel.Columns("Column85").SetOrdinal(intColIdx)


    '                    For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
    '                        mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
    '                    Next
    '            End Select




    '        End If
    '        'Sohail (12 Jan 2013) -- End

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

#Region " Generate_DetailReport before generate Statutory reports from using membershiptranunkid from prpayrollprocess_tran table on (27 Nov 2012) "
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'Sohail (17 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Sohail (17 Aug 2012) -- End

    '        'S.SANDEEP [ 15 SEP 2011 ] -- START
    '        'ISSUE : BASIC SAL CONTRIBUTED FOR VARIOUS COSTCENTER
    '        'StrQ = "SELECT " & _
    '        '        "	 PeriodName AS PeriodName " & _
    '        '        "	,EmpName AS EmpName " & _
    '        '        "	,MembershipNo AS MembershipNo " & _
    '        '        "	,MembershipName AS MembershipName " & _
    '        '        "	,GrossPay AS GrossPay " & _
    '        '        "	,EmpContrib AS EmpContrib " & _
    '        '        "	,EmprContrib AS EmprContrib " & _
    '        '        "	,Total AS Total " & _
    '        '        "	,BasicSal AS BasicSal " & _
    '        '        "	,EmpCode AS EmpCode " & _
    '        '        "	,StDate AS StDate " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 TableEmp_Contribution.Periodid " & _
    '        '        "		,TableEmp_Contribution.PeriodName " & _
    '        '        "		,TableMain.EmpId " & _
    '        '        "		,TableMain.EmpName " & _
    '        '        "		,TableMain.Membership_Name AS MembershipName " & _
    '        '        "		,TableMain.Membership_No AS MembershipNo " & _
    '        '        "		,TableMain.MembershipUnkId AS MemId " & _
    '        '        "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '        '        "		,TableEmployer_Contribution.EmployerHead " & _
    '        '        "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '        '        "		,ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '        '        "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '        '        "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '        '        "		,EmpCode AS EmpCode " & _
    '        '        "		,TableEmp_Contribution.StDate AS StDate " & _
    '        '        "	FROM " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '        "			,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '        "			,hrmembership_master.membershipname AS Membership_Name " & _
    '        '        "			,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '        "			,hremployee_master.employeecode AS EmpCode " & _
    '        '        "		FROM hremployee_master " & _
    '        '        "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '        '        "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "		WHERE hrmembership_master.isactive = 1 " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableMain " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,prpayrollprocess_tran.amount AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "			,cfcommon_period_tran.start_date AS StDate " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = @EHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @ETypeId " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid	AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,prtranhead_master.trnheadname AS EmployerHead " & _
    '        '        "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '        "			,cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,prpayrollprocess_tran.amount AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 Gross.Periodid " & _
    '        '        "			,Gross.PeriodName " & _
    '        '        "			,Gross.Empid " & _
    '        '        "			,SUM(Gross.Amount) AS GrossPay " & _
    '        '        "		FROM " & _
    '        '        "		( " & _
    '        '        "			SELECT " & _
    '        '        "				 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "				,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "				,hremployee_master.employeeunkid AS Empid " & _
    '        '        "				,ISNULL(prpayrollprocess_tran.amount,0) AS Amount " & _
    '        '        "			FROM prpayrollprocess_tran " & _
    '        '        "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "				AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "				AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '        "				AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '        "				AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		) AS Gross  " & _
    '        '        "		GROUP BY Gross.Periodid,Gross.PeriodName,Gross.Empid " & _
    '        '        "	) AS TableGross ON TableMain.EmpId = TableGross.Empid AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,ISNULL(prpayrollprocess_tran.amount, 0) AS BasicSal " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "			AND prtranhead_master.typeof_id = 1 " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		GROUP BY cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,hremployee_master.employeeunkid,ISNULL(prpayrollprocess_tran.amount, 0) " & _
    '        '        "	) AS BAS ON BAS.Empid = TableMain.EmpId AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '        '        ") AS Contribution " & _
    '        '        "WHERE 1 = 1 "

    '        'Sohail (04 Feb 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ = "SELECT " & _
    '        '        "	 PeriodName AS PeriodName " & _
    '        '        "	,EmpName AS EmpName " & _
    '        '        "	,MembershipNo AS MembershipNo " & _
    '        '        "	,MembershipName AS MembershipName " & _
    '        '        "	,GrossPay AS GrossPay " & _
    '        '        "	,EmpContrib AS EmpContrib " & _
    '        '        "	,EmprContrib AS EmprContrib " & _
    '        '        "	,Total AS Total " & _
    '        '        "	,BasicSal AS BasicSal " & _
    '        '        "	,EmpCode AS EmpCode " & _
    '        '        "	,StDate AS StDate " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 TableEmp_Contribution.Periodid " & _
    '        '        "		,TableEmp_Contribution.PeriodName " & _
    '        '        "		,TableMain.EmpId " & _
    '        '        "		,TableMain.EmpName " & _
    '        '        "		,TableMain.Membership_Name AS MembershipName " & _
    '        '        "		,TableMain.Membership_No AS MembershipNo " & _
    '        '        "		,TableMain.MembershipUnkId AS MemId " & _
    '        '        "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '        '        "		,TableEmployer_Contribution.EmployerHead " & _
    '        '        "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '        '        "		,ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '        '        "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '        '        "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '        '        "		,EmpCode AS EmpCode " & _
    '        '        "		,TableEmp_Contribution.StDate AS StDate " & _
    '        '        "	FROM " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '        "			,ISNULL(hremployee_master.firstname, '') + ' '+ ISNULL(hremployee_master.othername, '')+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '        "			,hrmembership_master.membershipname AS Membership_Name " & _
    '        '        "			,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '        "			,hremployee_master.employeecode AS EmpCode " & _
    '        '        "		FROM hremployee_master " & _
    '        '        "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '        '        "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "		WHERE hrmembership_master.isactive = 1 " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableMain " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,prpayrollprocess_tran.amount AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "			,cfcommon_period_tran.start_date AS StDate " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = @EHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @ETypeId " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid	AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,prtranhead_master.trnheadname AS EmployerHead " & _
    '        '        "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '        "			,cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,prpayrollprocess_tran.amount AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 Gross.Periodid " & _
    '        '        "			,Gross.PeriodName " & _
    '        '        "			,Gross.Empid " & _
    '        '        "			,SUM(Gross.Amount) AS GrossPay " & _
    '        '        "		FROM " & _
    '        '        "		( " & _
    '        '        "			SELECT " & _
    '        '        "				 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "				,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "				,hremployee_master.employeeunkid AS Empid " & _
    '        '        "				,ISNULL(prpayrollprocess_tran.amount,0) AS Amount " & _
    '        '        "			FROM prpayrollprocess_tran " & _
    '        '        "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "				AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "				AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '        "				AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '        "				AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		) AS Gross  " & _
    '        '        "		GROUP BY Gross.Periodid,Gross.PeriodName,Gross.Empid " & _
    '        '        "	) AS TableGross ON TableMain.EmpId = TableGross.Empid AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
    '        '        "	LEFT JOIN " & _
    '        '        "	( " & _
    '        '        "		SELECT " & _
    '        '        "			 cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,SUM(ISNULL(prpayrollprocess_tran.amount, 0)) AS BasicSal " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "			AND prtranhead_master.typeof_id = 1 " & _
    '        '        "			AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		GROUP BY cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,hremployee_master.employeeunkid " & _
    '        '        "	) AS BAS ON BAS.Empid = TableMain.EmpId AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '        '        ") AS Contribution " & _
    '        '        "WHERE 1 = 1 "

    '        'Sohail (27 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ = "SELECT  PeriodName AS PeriodName " & _
    '        '              ", EmpId " & _
    '        '        "	,EmpName AS EmpName " & _
    '        '              ", end_date " & _
    '        '        "	,MembershipNo AS MembershipNo " & _
    '        '        "	,MembershipName AS MembershipName " & _
    '        '        "	,GrossPay AS GrossPay " & _
    '        '        "	,EmpContrib AS EmpContrib " & _
    '        '        "	,EmprContrib AS EmprContrib " & _
    '        '        "	,Total AS Total " & _
    '        '        "	,BasicSal AS BasicSal " & _
    '        '        "	,EmpCode AS EmpCode " & _
    '        '        "	,StDate AS StDate " & _
    '        '              ", EDPeriodUnkId " & _
    '        '        "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
    '        '        "		,TableEmp_Contribution.PeriodName " & _
    '        '                          ", TableEmp_Contribution.end_date " & _
    '        '        "		,TableMain.EmpId " & _
    '        '        "		,TableMain.EmpName " & _
    '        '        "		,TableMain.Membership_Name AS MembershipName " & _
    '        '        "		,TableMain.Membership_No AS MembershipNo " & _
    '        '        "		,TableMain.MembershipUnkId AS MemId " & _
    '        '        "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '        '        "		,TableEmployer_Contribution.EmployerHead " & _
    '        '        "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '        '                          ", ISNULL(TableEmp_Contribution.Amount, 0) " & _
    '        '                            "+ ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '        '        "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '        '        "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '        '        "		,EmpCode AS EmpCode " & _
    '        '        "		,TableEmp_Contribution.StDate AS StDate " & _
    '        '                          ", TableMain.EDPeriodUnkId " & _
    '        '                  "FROM      ( SELECT    DISTINCT " & _
    '        '        "			 hremployee_master.employeeunkid AS EmpId " & _
    '        '                                      ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '        '                                        "+ ISNULL(hremployee_master.othername, '') " & _
    '        '                                        "+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '        "			,hrmembership_master.membershipname AS Membership_Name " & _
    '        '        "			,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '        '        "			,hremployee_master.employeecode AS EmpCode " & _
    '        '                                      ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '        '                              "FROM      prearningdeduction_master " & _
    '        '                                        "JOIN hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '        '        "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "		WHERE hrmembership_master.isactive = 1 " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '        "	) AS TableMain " & _
    '        '                            "LEFT JOIN ( SELECT  ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '        '                                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '        '                                              ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'01-Jan-1900'), 112) AS end_date " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '        "			,cfcommon_period_tran.start_date AS StDate " & _
    '        '                                              ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                                                                      "AND ISNULL(hremployee_meminfo_tran.isactive, " & _
    '        '                                                                      "0) = 1 " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = @EHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @ETypeId " & _
    '        '                                                "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '        '                                                           "0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '                                                "AND ISNULL(prearningdeduction_master.isvoid, " & _
    '        '                                                           "0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '                                      ") AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid " & _
    '        '                                                                    "AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId " & _
    '        '                                                                    "AND TableMain.EDPeriodUnkId = TableEmp_Contribution.EDPeriodUnkId " & _
    '        '                            "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
    '        '        "			,prtranhead_master.trnheadname AS EmployerHead " & _
    '        '        "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '        '        "			,cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '                                              ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                                                                      "AND ISNULL(hremployee_meminfo_tran.isactive, " & _
    '        '                                                                      "0) = 1 " & _
    '        '        "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId " & _
    '        '        "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '                                                "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '        '                                                           "0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '                                                "AND ISNULL(prearningdeduction_master.isvoid, " & _
    '        '                                                           "0) = 0 " & _
    '        '        "			AND hrmembership_master.membershipunkid = @MemId " & _
    '        '                                      ") AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid " & _
    '        '                                                                      "AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
    '        '                                                                      "AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '                                                                      "AND TableMain.EDPeriodUnkId = TableEmployer_Contribution.EDPeriodUnkId " & _
    '        '                            "LEFT JOIN ( SELECT  Gross.Periodid " & _
    '        '        "			,Gross.PeriodName " & _
    '        '        "			,Gross.Empid " & _
    '        '        "			,SUM(Gross.Amount) AS GrossPay " & _
    '        '                                        "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "				,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "				,hremployee_master.employeeunkid AS Empid " & _
    '        '                                                          ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '        '        "			FROM prpayrollprocess_tran " & _
    '        '        "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '                                                            "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '        '                                                                      "0) = 0 " & _
    '        '                                                            "AND ISNULL(prtranhead_master.isvoid, " & _
    '        '                                                                      "0) = 0 " & _
    '        '                                                            "AND ISNULL(prtnaleave_tran.isvoid, " & _
    '        '                                                                      "0) = 0 " & _
    '        '        "				AND cfcommon_period_tran.isactive = 1 " & _
    '        '        "		) AS Gross  " & _
    '        '                                        "GROUP BY Gross.Periodid " & _
    '        '                                              ", Gross.PeriodName " & _
    '        '                                              ", Gross.Empid " & _
    '        '                                      ") AS TableGross ON TableMain.EmpId = TableGross.Empid " & _
    '        '                                                         "AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
    '        '                            "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '        "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '        '        "			,hremployee_master.employeeunkid AS Empid " & _
    '        '                                              ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '        "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '        "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '        "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '        "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '        "			AND prtranhead_master.typeof_id = 1 " & _
    '        '                                                "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '        '                                                           "0) = 0 " & _
    '        '        "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '        '        "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '        '        "			AND cfcommon_period_tran.isactive = 1 " & _
    '        '                                        "GROUP BY cfcommon_period_tran.periodunkid " & _
    '        '                                              ", cfcommon_period_tran.period_name " & _
    '        '                                              ", hremployee_master.employeeunkid " & _
    '        '                                      ") AS BAS ON BAS.Empid = TableMain.EmpId " & _
    '        '                                                  "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '        '        ") AS Contribution " & _
    '        '        "WHERE 1 = 1 "
    '        StrQ = "SELECT  PeriodName AS PeriodName " & _
    '                      ", EmpId " & _
    '                "	,EmpName AS EmpName " & _
    '                      ", end_date " & _
    '                "	,MembershipNo AS MembershipNo " & _
    '                "	,MembershipName AS MembershipName " & _
    '                "	,GrossPay AS GrossPay " & _
    '                "	,EmpContrib AS EmpContrib " & _
    '                "	,EmprContrib AS EmprContrib " & _
    '                "	,Total AS Total " & _
    '                "	,BasicSal AS BasicSal " & _
    '                "	,EmpCode AS EmpCode " & _
    '                "	,StDate AS StDate " & _
    '                      ", EDPeriodUnkId " & _
    '                "FROM    ( SELECT    TableEmp_Contribution.Periodid " & _
    '                "		,TableEmp_Contribution.PeriodName " & _
    '                                  ", TableEmp_Contribution.end_date " & _
    '                "		,TableMain.EmpId " & _
    '                "		,TableMain.EmpName " & _
    '                "		,TableMain.Membership_Name AS MembershipName " & _
    '                "		,TableMain.Membership_No AS MembershipNo " & _
    '                "		,TableMain.MembershipUnkId AS MemId " & _
    '                "		,ISNULL(TableEmp_Contribution.Amount, 0) AS EmpContrib " & _
    '                "		,TableEmployer_Contribution.EmployerHead " & _
    '                "		,ISNULL(TableEmployer_Contribution.Amount, 0) AS EmprContrib " & _
    '                                  ", ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '                "		,ISNULL(TableGross.GrossPay, 0) AS GrossPay " & _
    '                "		,ISNULL(BasicSal, 0) AS BasicSal " & _
    '                "		,EmpCode AS EmpCode " & _
    '                "		,TableEmp_Contribution.StDate AS StDate " & _
    '                                  ", TableMain.EDPeriodUnkId " & _
    '                          "FROM      ( SELECT    DISTINCT " & _
    '                "			 hremployee_master.employeeunkid AS EmpId " & _
    '                                              ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                                                "+ ISNULL(hremployee_master.othername, '') " & _
    '                                                "+ ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                "			,hrmembership_master.membershipname AS Membership_Name " & _
    '                "			,hremployee_Meminfo_tran.membershipno AS Membership_No " & _
    '                "			,hrmembership_master.membershipunkid AS MembershipUnkId " & _
    '                "			,hremployee_master.employeecode AS EmpCode " & _
    '                                              ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                                      "FROM      prearningdeduction_master " & _
    '                                                "JOIN hremployee_master ON prearningdeduction_master.employeeunkid = hremployee_master.employeeunkid " & _
    '                "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid " & _
    '                "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "		WHERE hrmembership_master.isactive = 1 " & _
    '                "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '                "			AND hrmembership_master.membershipunkid = @MemId " & _
    '                "	) AS TableMain " & _
    '                                    "LEFT JOIN ( SELECT  ISNULL(cfcommon_period_tran.periodunkid, 0) AS Periodid " & _
    '                                                      ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                                                      ", CONVERT(CHAR(8), ISNULL(cfcommon_period_tran.end_date,'01-Jan-1900'), 112) AS end_date " & _
    '                "			,hremployee_master.employeeunkid AS Empid " & _
    '                "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '                "			,cfcommon_period_tran.start_date AS StDate " & _
    '                                                      ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                "		FROM prpayrollprocess_tran " & _
    '                "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                              "AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '                "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "			AND cfcommon_period_tran.isactive = 1 " & _
    '                                                        "AND ISNULL(prearningdeduction_master.isvoid,  0) = 0 " & _
    '                "			AND hrmembership_master.membershipunkid = @MemId " & _
    '                                              ") AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid " & _
    '                                                                            "AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId " & _
    '                                                                            "AND TableMain.EDPeriodUnkId = TableEmp_Contribution.EDPeriodUnkId " & _
    '                                    "LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS Empid " & _
    '                "			,prtranhead_master.trnheadname AS EmployerHead " & _
    '                "			,prtranhead_master.tranheadunkid AS EmployerHeadId " & _
    '                "			,cfcommon_period_tran.periodunkid AS Periodid " & _
    '                "			,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '                                                      ", prearningdeduction_master.periodunkid AS EDPeriodUnkId " & _
    '                "		FROM prpayrollprocess_tran " & _
    '                "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "			JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                                              "AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '                "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                "WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                        "AND ISNULL(prearningdeduction_master.isvoid, 0) = 0 " & _
    '                "			AND hrmembership_master.membershipunkid = @MemId " & _
    '                                              ") AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid " & _
    '                                                                              "AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId " & _
    '                                                                              "AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '                                                                              "AND TableMain.EDPeriodUnkId = TableEmployer_Contribution.EDPeriodUnkId " & _
    '                                    "LEFT JOIN ( SELECT  Gross.Periodid " & _
    '                "			,Gross.PeriodName " & _
    '                "			,Gross.Empid " & _
    '                "			,SUM(Gross.Amount) AS GrossPay " & _
    '                                                "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
    '                "				,cfcommon_period_tran.period_name AS PeriodName " & _
    '                "				,hremployee_master.employeeunkid AS Empid " & _
    '                                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, " & _
    '                                                                              "0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                "			FROM prpayrollprocess_tran " & _
    '                "				JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "				JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "				JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "				JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "			WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "				AND cfcommon_period_tran.isactive = 1 " & _
    '                "		) AS Gross  " & _
    '                                                "GROUP BY Gross.Periodid " & _
    '                                                      ", Gross.PeriodName " & _
    '                                                      ", Gross.Empid " & _
    '                                              ") AS TableGross ON TableMain.EmpId = TableGross.Empid " & _
    '                                                                 "AND TableEmp_Contribution.Periodid = TableGross.Periodid " & _
    '                                    "LEFT JOIN ( SELECT  cfcommon_period_tran.periodunkid AS Periodid " & _
    '                "			,cfcommon_period_tran.period_name AS PeriodName " & _
    '                "			,hremployee_master.employeeunkid AS Empid " & _
    '                                                      ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
    '                "		FROM prpayrollprocess_tran " & _
    '                "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                "		WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                "			AND prtranhead_master.typeof_id = 1 " & _
    '                                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "			AND cfcommon_period_tran.isactive = 1 " & _
    '                                                "GROUP BY cfcommon_period_tran.periodunkid " & _
    '                                                      ", cfcommon_period_tran.period_name " & _
    '                                                      ", hremployee_master.employeeunkid " & _
    '                                              ") AS BAS ON BAS.Empid = TableMain.EmpId " & _
    '                                                          "AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '                ") AS Contribution " & _
    '                "WHERE 1 = 1 "
    '        'Sohail (27 Aug 2012) -- End
    '        'Sohail (04 Feb 2012) -- End
    '        'S.SANDEEP [ 15 SEP 2011 ] -- END

    '        If mintReportId = 1 Then
    '            StrQ &= "AND Periodid = @PeriodId "
    '        End If

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        Dim dtTable As DataTable = New DataView(dsList.Tables(0), "", "StDate," & Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        Dim StrECode As String = String.Empty
    '        Dim intCnt As Integer = 1
    '        Dim dtFilterTable As DataTable = Nothing
    '        Dim mdicIsEmpAdded As New Dictionary(Of String, String)
    '        Dim rpt_Row As DataRow = Nothing
    '        Dim decColumn4Total, decColumn5Total, decColumn6Total, decColumn7Total, decColumn8Total As Decimal
    '        Dim decColumn4GrndTotal, decColumn5GrndTotal, decColumn6GrndTotal, decColumn7GrndTotal, decColumn8GrndTotal As Decimal

    '        decColumn4GrndTotal = 0 : decColumn5GrndTotal = 0 : decColumn6GrndTotal = 0 : decColumn7GrndTotal = 0 : decColumn8GrndTotal = 0

    '        Dim objED As clsEarningDeduction 'Sohail (04 Feb 2012)
    '        For Each dtRow As DataRow In dtTable.Rows
    '            StrECode = dtRow.Item("EmpCode").ToString.Trim

    '            If mdicIsEmpAdded.ContainsKey(StrECode) Then Continue For

    '            mdicIsEmpAdded.Add(StrECode, StrECode)

    '            dtFilterTable = New DataView(dtTable, "EmpCode = '" & StrECode & "'", "", DataViewRowState.CurrentRows).ToTable

    '            If dtFilterTable.Rows.Count > 0 Then
    '                For Each dtFRow As DataRow In dtFilterTable.Rows

    '                    'Sohail (04 Feb 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    If IsDBNull(dtFRow.Item("PeriodName")) = True Then Continue For
    '                    objED = New clsEarningDeduction
    '                    If objED.GetCurrentSlabEDPeriodUnkID(CInt(dtFRow.Item("EmpId")), eZeeDate.convertDate(dtFRow.Item("end_date").ToString)) <> CInt(dtFRow.Item("EDPeriodUnkId")) Then
    '                        Continue For
    '                    End If
    '                    'Sohail (04 Feb 2012) -- End

    '                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '                    Select Case mintReportId
    '                        Case 1  'PERIOD WISE
    '                            rpt_Row.Item("Column1") = intCnt.ToString
    '                            rpt_Row.Item("Column8") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                            rpt_Row.Item("Column3") = dtFRow.Item("EmpName")
    '                            rpt_Row.Item("Column2") = dtRow.Item("MembershipNo")
    '                            intCnt += 1
    '                        Case 2  'MONTH WISE
    '                            rpt_Row.Item("Column1") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
    '                            rpt_Row.Item("Column3") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                            rpt_Row.Item("Column2") = dtFRow.Item("PeriodName")
    '                    End Select
    '                    rpt_Row.Item("Column4") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column5") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column6") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column7") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column9") = dtRow.Item("EmpCode")

    '                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '                    decColumn4Total = decColumn4Total + CDec(dtFRow.Item("BasicSal"))
    '                    decColumn5Total = decColumn5Total + CDec(dtFRow.Item("GrossPay"))
    '                    decColumn6Total = decColumn6Total + CDec(dtFRow.Item("EmpContrib"))
    '                    decColumn7Total = decColumn7Total + CDec(dtFRow.Item("EmprContrib"))
    '                    decColumn8Total = decColumn8Total + CDec(dtFRow.Item("Total"))

    '                    decColumn4GrndTotal = decColumn4GrndTotal + CDec(dtFRow.Item("BasicSal"))
    '                    decColumn5GrndTotal = decColumn5GrndTotal + CDec(dtFRow.Item("GrossPay"))
    '                    decColumn6GrndTotal = decColumn6GrndTotal + CDec(dtFRow.Item("EmpContrib"))
    '                    decColumn7GrndTotal = decColumn7GrndTotal + CDec(dtFRow.Item("EmprContrib"))
    '                    decColumn8GrndTotal = decColumn8GrndTotal + CDec(dtFRow.Item("Total"))
    '                Next
    '            End If
    '            Select Case mintReportId
    '                Case 2
    '                    Dim rpt_dtRow() As DataRow = Nothing
    '                    rpt_dtRow = rpt_Data.Tables("ArutiTable").Select("Column9 = '" & StrECode & "'")
    '                    If rpt_dtRow.Length > 0 Then
    '                        For i As Integer = 0 To rpt_dtRow.Length - 1
    '                            rpt_dtRow(i).Item("Column76") = Format(decColumn4Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column77") = Format(decColumn5Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column78") = Format(decColumn6Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column79") = Format(decColumn7Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column80") = Format(decColumn8Total, GUI.fmtCurrency)
    '                            rpt_Data.Tables("ArutiTable").AcceptChanges()
    '                        Next
    '                        decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
    '                    End If
    '            End Select
    '        Next
    '        Select Case mintReportId
    '            Case 1
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport
    '            Case 2
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport_Month
    '        End Select

    '        objRpt.SetDataSource(rpt_Data)

    '        If mintReportId = 1 Then
    '            Call ReportFunction.TextChange(objRpt, "txtSchedular", Language.getMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR ") & mstrPeriodName)
    '            Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 19, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber)

    '            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 3, "Member Number"))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberName", Language.getMessage(mstrModuleName, 20, "Member's Name"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 21, "Employee Code")) 'Sohail (13 Apr 2012)
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 9, "Employee :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 22, "Month"))
    '            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 23, "Group Total :"))
    '        End If


    '        Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 24, "Basic Salary"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberContrib", Language.getMessage(mstrModuleName, 25, "Member's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 26, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 27, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 28, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

    '        If mintReportId = 1 Then
    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8Total, GUI.fmtCurrency))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8GrndTotal, GUI.fmtCurrency))
    '        End If


    '        If mblnShowBasicSalary = False Then
    '            Dim strCurrentHdr As String = String.Empty
    '            If mintReportId = 1 Then
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column81", True)
    '                strCurrentHdr = "txtMemberName"

    '                'Sohail (13 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                'strCurrentHdr = "txtGrossPay"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal3", "txtMemberContrib")
    '                'strCurrentHdr = "txtMemberContrib"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal4", "txtEmployerContrib")
    '                'strCurrentHdr = "txtEmployerContrib"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal5", "txtTotal")
    '                'strCurrentHdr = "txtMemberContrib"
    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 1356)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                'Sohail (13 Apr 2012) -- End
    '            Else
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column31", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column761", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                strCurrentHdr = "txtMonth"

    '                'Sohail (13 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                'ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                'strCurrentHdr = "txtGrossPay"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "Column781", "txtMemberContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal3", "txtMemberContrib")
    '                'strCurrentHdr = "txtMemberContrib"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "Column791", "txtEmployerContrib")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal4", "txtEmployerContrib")
    '                'strCurrentHdr = "txtEmployerContrib"

    '                'ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '                'ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '                'ReportFunction.SetColumnOn(objRpt, "Column801", "txtTotal")
    '                'ReportFunction.SetColumnOn(objRpt, "txtTotal5", "txtTotal")
    '                'strCurrentHdr = "txtTotal"
    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10, 2160)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                'Sohail (13 Apr 2012) -- End
    '            End If
    '            'Sohail (13 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '        Else
    '            If mintReportId = 1 Then
    '                ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column41", True)

    '                ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMemberName", 10, 1356)
    '                ReportFunction.SetColumnOn(objRpt, "Column81", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
    '            Else
    '                ReportFunction.EnableSuppress(objRpt, "txtGrossPay", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal2", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column41", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column771", True)

    '                ReportFunction.SetColumnAfter(objRpt, "txtBasicSal", "txtMonth", 10, 2160)
    '                ReportFunction.SetColumnOn(objRpt, "Column31", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "Column761", "txtBasicSal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal1", "txtBasicSal")
    '            End If
    '            'Sohail (13 Apr 2012) -- End
    '        End If

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'Vimal (30 Nov 2010) -- Start 

    '        'StrQ = "SELECT  " & _
    '        '            "	 SrNo AS SrNo  " & _
    '        '            "	,PeriodName AS PeriodName  " & _
    '        '            "	,EmpName AS EmpName  " & _
    '        '            "	,MembershipNo AS MembershipNo  " & _
    '        '            "	,MembershipName AS MembershipName  " & _
    '        '            "	,GrossPay AS GrossPay  " & _
    '        '            "	,EmpContrib AS EmpContrib  " & _
    '        '            "	,EmprContrib AS EmprContrib  " & _
    '        '            "	,Total AS Total  " & _
    '        '            "   ,BasicSal As BasicSal " & _
    '        '            "   ,EmpCode As EmpCode " & _
    '        '            "FROM  " & _
    '        '            "(  " & _
    '        '            "	SELECT    " & _
    '        '            "	  ROW_NUMBER() OVER ( ORDER BY TableEmp_Contribution.Periodid ) AS SrNo  " & _
    '        '            "	, TableEmp_Contribution.Periodid  " & _
    '        '            "	, TableEmp_Contribution.PeriodName  " & _
    '        '            "	, TableMain.EmpId  " & _
    '        '            "	, TableMain.EmpName  " & _
    '        '            "	, TableMain.Membership_Name AS MembershipName  " & _
    '        '            "	, TableMain.Membership_No AS MembershipNo  " & _
    '        '            "	, TableMain.MembershipUnkId AS MemId  " & _
    '        '            "	, TableEmp_Contribution.EmpHead  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '        '            "	, TableEmployer_Contribution.EmployerHead  " & _
    '        '            "	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0)+ISNULL(TableEmployer_Contribution.Amount,0) AS Total  " & _
    '        '            "	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '        '            "   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '        '            "   ,EmpCode AS EmpCode " & _
    '        '            "FROM  " & _
    '        '            "	(  " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS EmpId  " & _
    '        '            "			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '            "			, hrmembership_master.membershipname AS Membership_Name  " & _
    '        '            "			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '        '            "			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '        '            "           , hremployee_master.employeecode As EmpCode " & _
    '        '            "		FROM hremployee_master  " & _
    '        '            "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '        '            "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '        '            "		WHERE   hremployee_master.isactive = 1 AND hrmembership_master.isactive = 1  " & _
    '        '            "	) AS TableMain  " & _
    '        '            "JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "			, hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmpHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			LEFT	JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid  " & _
    '        '            "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '        '            "		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '        '            "			AND 	ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        '            "JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			LEFT	JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid  " & _
    '        '            "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '        '            "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '        '            " JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT     " & _
    '        '            "			   Gross.Periodid  " & _
    '        '            "            , Gross.PeriodName  " & _
    '        '            "            , Gross.Empid  " & _
    '        '            "            , SUM(Gross.Amount) AS GrossPay  " & _
    '        '            "        FROM       " & _
    '        '            "			(   " & _
    '        '            "				SELECT      " & _
    '        '            "					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "                    , hremployee_master.employeeunkid AS Empid  " & _
    '        '            "                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '        '            "                FROM prpayrollprocess_tran  " & _
    '        '            "					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '        '            "                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '        '            "                    AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "                    AND hremployee_master.isactive = 1  " & _
    '        '            "             ) AS Gross  " & _
    '        '            "          GROUP BY Gross.Periodid  " & _
    '        '            "                , Gross.PeriodName  " & _
    '        '            "                , Gross.Empid  " & _
    '        '            "	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '        '            " LEFT JOIN " & _
    '        '            "   (" & _
    '        '            "       SELECT " & _
    '        '            "	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '        '            "	        , hremployee_master.employeeunkid AS Empid " & _
    '        '            "	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '        '            "       FROM dbo.prpayrollprocess_tran " & _
    '        '            "	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '            "	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '            "	        AND prtranhead_master.typeof_id = 1 " & _
    '        '            "	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '            "	        AND cfcommon_period_tran.isactive = 1 " & _
    '        '            "	        AND hremployee_master.isactive = 1 " & _
    '        '            "       GROUP BY cfcommon_period_tran.periodunkid " & _
    '        '            "	        ,cfcommon_period_tran.period_name " & _
    '        '            "	        ,hremployee_master.employeeunkid " & _
    '        '            "	        ,ISNULL(prpayrollprocess_tran.amount,0) " & _
    '        '            "   ) As BAS ON BAS.Empid = TableMain.EmpId  AND TableEmp_Contribution.Periodid = BAS.Periodid  " & _
    '        '            ") AS Contribution  " & _
    '        '            "WHERE MemId = @MemId "
    '        'If mintReportId = 1 Then
    '        '    StrQ &= "AND Periodid = @PeriodId "
    '        'End If


    '        'Sandeep [ 01 MARCH 2011 ] -- Start
    '        'StrQ = "SELECT  " & _
    '        '"	 SrNo AS SrNo  " & _
    '        '"	,PeriodName AS PeriodName  " & _
    '        '"	,EmpName AS EmpName  " & _
    '        '"	,MembershipNo AS MembershipNo  " & _
    '        '"	,MembershipName AS MembershipName  " & _
    '        '"	,GrossPay AS GrossPay  " & _
    '        '"	,EmpContrib AS EmpContrib  " & _
    '        '"	,EmprContrib AS EmprContrib  " & _
    '        '"	,Total AS Total  " & _
    '        '"   ,BasicSal As BasicSal " & _
    '        '"   ,EmpCode As EmpCode " & _
    '        '"FROM  " & _
    '        '"(  " & _
    '        '"	SELECT    " & _
    '        '"	  ROW_NUMBER() OVER ( ORDER BY TableEmp_Contribution.Periodid ) AS SrNo  " & _
    '        '"	, TableEmp_Contribution.Periodid  " & _
    '        '"	, TableEmp_Contribution.PeriodName  " & _
    '        '"	, TableMain.EmpId  " & _
    '        '"	, TableMain.EmpName  " & _
    '        '"	, TableMain.Membership_Name AS MembershipName  " & _
    '        '"	, TableMain.Membership_No AS MembershipNo  " & _
    '        '"	, TableMain.MembershipUnkId AS MemId  " & _
    '        '"	, TableEmp_Contribution.EmpHead  " & _
    '        '"	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '        '"	, TableEmployer_Contribution.EmployerHead  " & _
    '        '"	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '        '"	, ISNULL(TableEmp_Contribution.Amount,0)+ISNULL(TableEmployer_Contribution.Amount,0) AS Total  " & _
    '        '"	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '        '"   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '        '"   ,EmpCode AS EmpCode " & _
    '        '"FROM  " & _
    '        '"	(  " & _
    '        '"		SELECT    " & _
    '        '"			  hremployee_master.employeeunkid AS EmpId  " & _
    '        '"			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '"			, hrmembership_master.membershipname AS Membership_Name  " & _
    '        '"			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '        '"			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '        '"           , hremployee_master.employeecode As EmpCode " & _
    '        '"		FROM hremployee_master  " & _
    '        '"			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '        '"			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '        '"		WHERE   hremployee_master.isactive = 1 AND hrmembership_master.isactive = 1  " & _
    '        '"	) AS TableMain  " & _
    '        '"JOIN   " & _
    '        '"	(   " & _
    '        '"		SELECT    " & _
    '        '"			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '"			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '"			, hremployee_master.employeeunkid AS Empid  " & _
    '        '"			, prtranhead_master.trnheadname AS EmpHead  " & _
    '        '"			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '        '"			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '"			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '"		FROM    prpayrollprocess_tran  " & _
    '        '"			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '"           LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '"			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '"			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '"		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '        '"		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '        '"			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '"			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '"			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '"			AND cfcommon_period_tran.isactive = 1  " & _
    '        '"			AND hremployee_master.isactive = 1  " & _
    '        '"			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '"	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        '"JOIN   " & _
    '        '"	(   " & _
    '        '"		SELECT    " & _
    '        '"			  hremployee_master.employeeunkid AS Empid  " & _
    '        '"			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '        '"			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '        '"           , cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '"			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '"			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '"		FROM    prpayrollprocess_tran  " & _
    '        '"			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '"			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '"			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"           LEFT JOIN cfcommon_period_tran ON dbo.prtnaleave_tran.payperiodunkid=dbo.cfcommon_period_tran.periodunkid  " & _
    '        '"			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '"		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '        '"			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '"			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '"			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '"			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '"			AND hremployee_master.isactive = 1  " & _
    '        '"			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '"	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '        '"                                   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '" JOIN   " & _
    '        '"	(   " & _
    '        '"		SELECT     " & _
    '        '"			   Gross.Periodid  " & _
    '        '"            , Gross.PeriodName  " & _
    '        '"            , Gross.Empid  " & _
    '        '"            , SUM(Gross.Amount) AS GrossPay  " & _
    '        '"        FROM       " & _
    '        '"			(   " & _
    '        '"				SELECT      " & _
    '        '"					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '"                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '"                    , hremployee_master.employeeunkid AS Empid  " & _
    '        '"                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '        '"                FROM prpayrollprocess_tran  " & _
    '        '"					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '"                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '"                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '"                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '"                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '        '"                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '        '"                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '        '"                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '        '"                    AND cfcommon_period_tran.isactive = 1  " & _
    '        '"                    AND hremployee_master.isactive = 1  " & _
    '        '"             ) AS Gross  " & _
    '        '"          GROUP BY Gross.Periodid  " & _
    '        '"                , Gross.PeriodName  " & _
    '        '"                , Gross.Empid  " & _
    '        '"	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '        '" LEFT JOIN " & _
    '        '"   (" & _
    '        '"       SELECT " & _
    '        '"	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '"	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '        '"	        , hremployee_master.employeeunkid AS Empid " & _
    '        '"	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '        '"       FROM dbo.prpayrollprocess_tran " & _
    '        '"	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '"	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '"	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '"	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '"       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '"	        AND prtranhead_master.typeof_id = 1 " & _
    '        '"	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '"	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '"	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '"	        AND cfcommon_period_tran.isactive = 1 " & _
    '        '"	        AND hremployee_master.isactive = 1 " & _
    '        '"       GROUP BY cfcommon_period_tran.periodunkid " & _
    '        '"	        ,cfcommon_period_tran.period_name " & _
    '        '"	        ,hremployee_master.employeeunkid " & _
    '        '"	        ,ISNULL(prpayrollprocess_tran.amount,0) " & _
    '        '"   ) As BAS ON BAS.Empid = TableMain.EmpId  AND TableEmp_Contribution.Periodid = BAS.Periodid  " & _
    '        '") AS Contribution  " & _
    '        '"WHERE MemId = @MemId "
    '        'If mintReportId = 1 Then
    '        '    StrQ &= "AND Periodid = @PeriodId "
    '        'End If


    '        'Sandeep [ 16 MAY 2011 ] -- Start
    '        'ISSUE : DATA DUPLICATION DUE TO INACTIVE MEMBERSHIP
    '        '    StrQ = "SELECT  " & _
    '        '            "	 SrNo AS SrNo  " & _
    '        '            "	,PeriodName AS PeriodName  " & _
    '        '            "	,EmpName AS EmpName  " & _
    '        '            "	,MembershipNo AS MembershipNo  " & _
    '        '            "	,MembershipName AS MembershipName  " & _
    '        '            "	,GrossPay AS GrossPay  " & _
    '        '            "	,EmpContrib AS EmpContrib  " & _
    '        '            "	,EmprContrib AS EmprContrib  " & _
    '        '            "	,Total AS Total  " & _
    '        '            "   ,BasicSal As BasicSal " & _
    '        '            "   ,EmpCode As EmpCode " & _
    '        '            "FROM  " & _
    '        '            "(  " & _
    '        '            "	SELECT    " & _
    '        '            "	  ROW_NUMBER() OVER ( ORDER BY TableEmp_Contribution.Periodid ) AS SrNo  " & _
    '        '            "	, TableEmp_Contribution.Periodid  " & _
    '        '            "	, TableEmp_Contribution.PeriodName  " & _
    '        '            "	, TableMain.EmpId  " & _
    '        '            "	, TableMain.EmpName  " & _
    '        '            "	, TableMain.Membership_Name AS MembershipName  " & _
    '        '            "	, TableMain.Membership_No AS MembershipNo  " & _
    '        '            "	, TableMain.MembershipUnkId AS MemId  " & _
    '        '            "	, TableEmp_Contribution.EmpHead  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '        '            "	, TableEmployer_Contribution.EmployerHead  " & _
    '        '            "	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0)+ISNULL(TableEmployer_Contribution.Amount,0) AS Total  " & _
    '        '            "	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '        '            "   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '        '            "   ,EmpCode AS EmpCode " & _
    '        '            "FROM  " & _
    '        '            "	(  " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS EmpId  " & _
    '        '            "			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '            "			, hrmembership_master.membershipname AS Membership_Name  " & _
    '        '            "			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '        '            "			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '        '            "           , hremployee_master.employeecode As EmpCode " & _
    '        '            "		FROM hremployee_master  " & _
    '        '            "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '        '            "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '        '            "		WHERE   hremployee_master.isactive = 1 AND hrmembership_master.isactive = 1  " & _
    '        '            "	) AS TableMain  " & _
    '        '            "JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "			, hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmpHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "           LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '        '            "		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        '"LEFT JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '        '            "           , cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "           LEFT JOIN cfcommon_period_tran ON dbo.prtnaleave_tran.payperiodunkid=dbo.cfcommon_period_tran.periodunkid  " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '        '            "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '        '            "                                   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '            " JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT     " & _
    '        '            "			   Gross.Periodid  " & _
    '        '            "            , Gross.PeriodName  " & _
    '        '            "            , Gross.Empid  " & _
    '        '            "            , SUM(Gross.Amount) AS GrossPay  " & _
    '        '            "        FROM       " & _
    '        '            "			(   " & _
    '        '            "				SELECT      " & _
    '        '            "					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "                    , hremployee_master.employeeunkid AS Empid  " & _
    '        '            "                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '        '            "                FROM prpayrollprocess_tran  " & _
    '        '            "					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '        '            "                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '        '            "                    AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "                    AND hremployee_master.isactive = 1  " & _
    '        '            "             ) AS Gross  " & _
    '        '            "          GROUP BY Gross.Periodid  " & _
    '        '            "                , Gross.PeriodName  " & _
    '        '            "                , Gross.Empid  " & _
    '        '            "	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '        '            " LEFT JOIN " & _
    '        '            "   (" & _
    '        '            "       SELECT " & _
    '        '            "	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '        '            "	        , hremployee_master.employeeunkid AS Empid " & _
    '        '            "	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '        '            "       FROM dbo.prpayrollprocess_tran " & _
    '        '            "	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '            "	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '            "	        AND prtranhead_master.typeof_id = 1 " & _
    '        '            "	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '            "	        AND cfcommon_period_tran.isactive = 1 " & _
    '        '            "	        AND hremployee_master.isactive = 1 " & _
    '        '            "       GROUP BY cfcommon_period_tran.periodunkid " & _
    '        '            "	        ,cfcommon_period_tran.period_name " & _
    '        '            "	        ,hremployee_master.employeeunkid " & _
    '        '            "	        ,ISNULL(prpayrollprocess_tran.amount,0) " & _
    '        '            "   ) As BAS ON BAS.Empid = TableMain.EmpId  AND TableEmp_Contribution.Periodid = BAS.Periodid  " & _
    '        '            ") AS Contribution  " & _
    '        '            "WHERE MemId = @MemId "

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        'ISSUE : MEMBERSHIP MAPPED WITH TRANSHEAD
    '        'StrQ = "SELECT  " & _
    '        '            "	 SrNo AS SrNo  " & _
    '        '            "	,PeriodName AS PeriodName  " & _
    '        '            "	,EmpName AS EmpName  " & _
    '        '            "	,MembershipNo AS MembershipNo  " & _
    '        '            "	,MembershipName AS MembershipName  " & _
    '        '            "	,GrossPay AS GrossPay  " & _
    '        '            "	,EmpContrib AS EmpContrib  " & _
    '        '            "	,EmprContrib AS EmprContrib  " & _
    '        '            "	,Total AS Total  " & _
    '        '            "   ,BasicSal As BasicSal " & _
    '        '            "   ,EmpCode As EmpCode " & _
    '        '            "FROM  " & _
    '        '            "(  " & _
    '        '            "	SELECT    " & _
    '        '            "	  ROW_NUMBER() OVER ( ORDER BY TableEmp_Contribution.Periodid ) AS SrNo  " & _
    '        '            "	, TableEmp_Contribution.Periodid  " & _
    '        '            "	, TableEmp_Contribution.PeriodName  " & _
    '        '            "	, TableMain.EmpId  " & _
    '        '            "	, TableMain.EmpName  " & _
    '        '            "	, TableMain.Membership_Name AS MembershipName  " & _
    '        '            "	, TableMain.Membership_No AS MembershipNo  " & _
    '        '            "	, TableMain.MembershipUnkId AS MemId  " & _
    '        '            "	, TableEmp_Contribution.EmpHead  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '        '            "	, TableEmployer_Contribution.EmployerHead  " & _
    '        '            "	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0)+ISNULL(TableEmployer_Contribution.Amount,0) AS Total  " & _
    '        '            "	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '        '            "   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '        '            "   ,EmpCode AS EmpCode " & _
    '        '            "FROM  " & _
    '        '            "	(  " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS EmpId  " & _
    '        '            "			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '            "			, hrmembership_master.membershipname AS Membership_Name  " & _
    '        '            "			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '        '            "			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '        '            "           , hremployee_master.employeecode As EmpCode " & _
    '        '            "		FROM hremployee_master  " & _
    '        '            "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '        '            "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '        '        "		WHERE   hremployee_master.isactive = 1 AND hrmembership_master.isactive = 1 AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '            "	) AS TableMain  " & _
    '        '            "JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "			, hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmpHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "           LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '        "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '        '            "		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        '"LEFT JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '        '            "           , cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '            "			, prearningdeduction_master.vendorunkid AS MembershipId  " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid AND prtnaleave_tran.employeeunkid=prearningdeduction_master.employeeunkid  " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '        "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = prearningdeduction_master.vendorunkid AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '            "           LEFT JOIN cfcommon_period_tran ON dbo.prtnaleave_tran.payperiodunkid=dbo.cfcommon_period_tran.periodunkid  " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '        '            "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '        '            "                                   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '            " JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT     " & _
    '        '            "			   Gross.Periodid  " & _
    '        '            "            , Gross.PeriodName  " & _
    '        '            "            , Gross.Empid  " & _
    '        '            "            , SUM(Gross.Amount) AS GrossPay  " & _
    '        '            "        FROM       " & _
    '        '            "			(   " & _
    '        '            "				SELECT      " & _
    '        '            "					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "                    , hremployee_master.employeeunkid AS Empid  " & _
    '        '            "                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '        '            "                FROM prpayrollprocess_tran  " & _
    '        '            "					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '        '            "                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '        '            "                    AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "                    AND hremployee_master.isactive = 1  " & _
    '        '            "             ) AS Gross  " & _
    '        '            "          GROUP BY Gross.Periodid  " & _
    '        '            "                , Gross.PeriodName  " & _
    '        '            "                , Gross.Empid  " & _
    '        '            "	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '        '            " LEFT JOIN " & _
    '        '            "   (" & _
    '        '            "       SELECT " & _
    '        '            "	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '        '            "	        , hremployee_master.employeeunkid AS Empid " & _
    '        '            "	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '        '            "       FROM dbo.prpayrollprocess_tran " & _
    '        '            "	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '            "	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '            "	        AND prtranhead_master.typeof_id = 1 " & _
    '        '            "	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '            "	        AND cfcommon_period_tran.isactive = 1 " & _
    '        '            "	        AND hremployee_master.isactive = 1 " & _
    '        '            "       GROUP BY cfcommon_period_tran.periodunkid " & _
    '        '            "	        ,cfcommon_period_tran.period_name " & _
    '        '            "	        ,hremployee_master.employeeunkid " & _
    '        '            "	        ,ISNULL(prpayrollprocess_tran.amount,0) " & _
    '        '            "   ) As BAS ON BAS.Empid = TableMain.EmpId  AND TableEmp_Contribution.Periodid = BAS.Periodid  " & _
    '        '            ") AS Contribution  " & _
    '        '            "WHERE MemId = @MemId "


    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        '    StrQ = "SELECT  " & _
    '        '        "	 PeriodName AS PeriodName " & _
    '        '            "	,EmpName AS EmpName  " & _
    '        '            "	,MembershipNo AS MembershipNo  " & _
    '        '            "	,MembershipName AS MembershipName  " & _
    '        '            "	,GrossPay AS GrossPay  " & _
    '        '            "	,EmpContrib AS EmpContrib  " & _
    '        '            "	,EmprContrib AS EmprContrib  " & _
    '        '            "	,Total AS Total  " & _
    '        '        "	,BasicSal AS BasicSal " & _
    '        '        "	,EmpCode AS EmpCode " & _
    '        '            "FROM  " & _
    '        '            "(  " & _
    '        '            "	SELECT    " & _
    '        '        "	 TableEmp_Contribution.Periodid " & _
    '        '            "	, TableEmp_Contribution.PeriodName  " & _
    '        '            "	, TableMain.EmpId  " & _
    '        '            "	, TableMain.EmpName  " & _
    '        '            "	, TableMain.Membership_Name AS MembershipName  " & _
    '        '            "	, TableMain.Membership_No AS MembershipNo  " & _
    '        '            "	, TableMain.MembershipUnkId AS MemId  " & _
    '        '            "	, TableEmp_Contribution.EmpHead  " & _
    '        '            "	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '        '            "	, TableEmployer_Contribution.EmployerHead  " & _
    '        '            "	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '        '        "	,ISNULL(TableEmp_Contribution.Amount, 0) " & _
    '        '        "	+ ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '        '            "	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '        '            "   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '        '            "   ,EmpCode AS EmpCode " & _
    '        '            "FROM  " & _
    '        '            "	(  " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS EmpId  " & _
    '        '            "			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '        '            "			, hrmembership_master.membershipname AS Membership_Name  " & _
    '        '            "			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '        '            "			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '        '        "			,hremployee_master.employeecode AS EmpCode " & _
    '        '            "		FROM hremployee_master  " & _
    '        '            "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '        '            "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '        '        "		WHERE hremployee_master.isactive = 1 " & _
    '        '        "			AND hrmembership_master.isactive = 1 " & _
    '        '        "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '        '            "	) AS TableMain  " & _
    '        '            "JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "			, hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmpHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '        "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '        '            "		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        '"LEFT JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT    " & _
    '        '            "			  hremployee_master.employeeunkid AS Empid  " & _
    '        '            "			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '        '            "			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '        '            "           , cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "			, prpayrollprocess_tran.amount AS Amount  " & _
    '        '        "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '        '            "		FROM    prpayrollprocess_tran  " & _
    '        '            "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '        "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '        '            "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '        "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '        '        "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '        '        "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '        '        "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '        '            "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '        '            "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '        '            "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '        '            "			AND hremployee_master.isactive = 1  " & _
    '        '            "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0  " & _
    '        '            "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '        '            "                                   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '        '            " JOIN   " & _
    '        '            "	(   " & _
    '        '            "		SELECT     " & _
    '        '            "			   Gross.Periodid  " & _
    '        '            "            , Gross.PeriodName  " & _
    '        '            "            , Gross.Empid  " & _
    '        '            "            , SUM(Gross.Amount) AS GrossPay  " & _
    '        '            "        FROM       " & _
    '        '            "			(   " & _
    '        '            "				SELECT      " & _
    '        '            "					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '        '            "                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '        '            "                    , hremployee_master.employeeunkid AS Empid  " & _
    '        '            "                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '        '            "                FROM prpayrollprocess_tran  " & _
    '        '            "					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '        '            "                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '        '            "                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '        '            "                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '        '            "                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '        '            "                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '        '            "                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '        '            "                    AND cfcommon_period_tran.isactive = 1  " & _
    '        '            "                    AND hremployee_master.isactive = 1  " & _
    '        '            "             ) AS Gross  " & _
    '        '        "		GROUP BY Gross.Periodid,Gross.PeriodName,Gross.Empid " & _
    '        '            "	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '        '            " LEFT JOIN " & _
    '        '            "   (" & _
    '        '            "       SELECT " & _
    '        '            "	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '        '            "	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '        '            "	        , hremployee_master.employeeunkid AS Empid " & _
    '        '            "	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '        '        "		FROM prpayrollprocess_tran " & _
    '        '            "	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '            "	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '        '            "	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '        '            "       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '        '            "	        AND prtranhead_master.typeof_id = 1 " & _
    '        '            "	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '        '            "	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '        '            "	        AND cfcommon_period_tran.isactive = 1 " & _
    '        '            "	        AND hremployee_master.isactive = 1 " & _
    '        '        "		GROUP BY cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,hremployee_master.employeeunkid,ISNULL(prpayrollprocess_tran.amount, 0) " & _
    '        '        "	) AS BAS ON BAS.Empid = TableMain.EmpId AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '        '            ") AS Contribution  " & _
    '        '            "WHERE MemId = @MemId "

    '        StrQ = "SELECT  " & _
    '                "	 PeriodName AS PeriodName " & _
    '                    "	,EmpName AS EmpName  " & _
    '                    "	,MembershipNo AS MembershipNo  " & _
    '                    "	,MembershipName AS MembershipName  " & _
    '                    "	,GrossPay AS GrossPay  " & _
    '                    "	,EmpContrib AS EmpContrib  " & _
    '                    "	,EmprContrib AS EmprContrib  " & _
    '                    "	,Total AS Total  " & _
    '                "	,BasicSal AS BasicSal " & _
    '                "	,EmpCode AS EmpCode " & _
    '                    "FROM  " & _
    '                    "(  " & _
    '                    "	SELECT    " & _
    '                "	 TableEmp_Contribution.Periodid " & _
    '                    "	, TableEmp_Contribution.PeriodName  " & _
    '                    "	, TableMain.EmpId  " & _
    '                    "	, TableMain.EmpName  " & _
    '                    "	, TableMain.Membership_Name AS MembershipName  " & _
    '                    "	, TableMain.Membership_No AS MembershipNo  " & _
    '                    "	, TableMain.MembershipUnkId AS MemId  " & _
    '                    "	, TableEmp_Contribution.EmpHead  " & _
    '                    "	, ISNULL(TableEmp_Contribution.Amount,0) AS EmpContrib  " & _
    '                    "	, TableEmployer_Contribution.EmployerHead  " & _
    '                    "	, ISNULL(TableEmployer_Contribution.Amount,0) AS EmprContrib  " & _
    '                "		,ISNULL(TableEmp_Contribution.Amount, 0) + ISNULL(TableEmployer_Contribution.Amount, 0) AS Total " & _
    '                    "	, ISNULL(TableGross.GrossPay,0) AS GrossPay  " & _
    '                    "   ,ISNULL(BasicSal,0) AS BasicSal " & _
    '                    "   ,EmpCode AS EmpCode " & _
    '                    "FROM  " & _
    '                    "	(  " & _
    '                    "		SELECT    " & _
    '                    "			  hremployee_master.employeeunkid AS EmpId  " & _
    '                    "			, ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName  " & _
    '                    "			, hrmembership_master.membershipname AS Membership_Name  " & _
    '                    "			, hremployee_Meminfo_tran.membershipno AS Membership_No  " & _
    '                    "			, hrmembership_master.membershipunkid AS MembershipUnkId  " & _
    '                "			,hremployee_master.employeecode AS EmpCode " & _
    '                    "		FROM hremployee_master  " & _
    '                    "			JOIN hremployee_Meminfo_tran ON hremployee_master.employeeunkid = hremployee_Meminfo_tran.employeeunkid  " & _
    '                    "			JOIN hrmembership_master ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid    " & _
    '                "		WHERE 1 = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "       AND hremployee_master.isactive = 1 "
    '        End If
    '        StrQ &= "			AND hrmembership_master.isactive = 1 " & _
    '                "			AND ISNULL(hremployee_meminfo_tran.isactive, 0) = 1 " & _
    '                    "	) AS TableMain  " & _
    '                    "JOIN   " & _
    '                    "	(   " & _
    '                    "		SELECT    " & _
    '                    "			  cfcommon_period_tran.periodunkid AS Periodid  " & _
    '                    "			, cfcommon_period_tran.period_name AS PeriodName  " & _
    '                    "			, hremployee_master.employeeunkid AS Empid  " & _
    '                    "			, prtranhead_master.trnheadname AS EmpHead  " & _
    '                    "			, prtranhead_master.tranheadunkid AS EmpHeadId  " & _
    '                    "			, prpayrollprocess_tran.amount AS Amount  " & _
    '                "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '                    "		FROM    prpayrollprocess_tran  " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '                "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '                "			LEFT JOIN hrmembership_master ON hrmembership_master.emptranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "			LEFT JOIN hremployee_meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '                    "			JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '                    "		WHERE  prtranhead_master.trnheadtype_id = @EHeadId  " & _
    '                    "		    AND prtranhead_master.typeof_id = @ETypeId  " & _
    '                    "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '                    "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0  " & _
    '                "			AND cfcommon_period_tran.isactive = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "		AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '                    "	) AS TableEmp_Contribution ON TableMain.Empid = TableEmp_Contribution.Empid AND TableMain.MembershipUnkId = TableEmp_Contribution.MembershipId  " & _
    '        "LEFT JOIN   " & _
    '                    "	(   " & _
    '                    "		SELECT    " & _
    '                    "			  hremployee_master.employeeunkid AS Empid  " & _
    '                    "			, prtranhead_master.trnheadname AS EmployerHead  " & _
    '                    "			, prtranhead_master.tranheadunkid AS EmployerHeadId  " & _
    '                    "           , cfcommon_period_tran.periodunkid AS Periodid " & _
    '                    "			, prpayrollprocess_tran.amount AS Amount  " & _
    '                "			,hrmembership_master.membershipunkid AS MembershipId " & _
    '                    "		FROM    prpayrollprocess_tran  " & _
    '                    "			JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '                "			LEFT JOIN prearningdeduction_master ON prpayrollprocess_tran.tranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "				AND prtnaleave_tran.employeeunkid = prearningdeduction_master.employeeunkid " & _
    '                    "			JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '                "			LEFT JOIN hrmembership_master ON hrmembership_master.cotranheadunkid = prearningdeduction_master.tranheadunkid " & _
    '                "			LEFT JOIN hremployee_Meminfo_tran ON hremployee_Meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
    '                "				AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "				AND ISNULL(hremployee_meminfo_tran.isactive,0) = 1 " & _
    '                "			LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "			JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '                    "		WHERE   prtranhead_master.trnheadtype_id = @CHeadId  " & _
    '                    "			AND prtranhead_master.typeof_id = @CTypeId " & _
    '                    "			AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0  " & _
    '                    "			AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '                "			AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "		AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "			AND ISNULL(prearningdeduction_master.isvoid,0) = 0 " & _
    '                    "	) AS TableEmployer_Contribution ON TableMain.Empid = TableEmployer_Contribution.Empid AND TableMain.MembershipUnkId = TableEmployer_Contribution.MembershipId  " & _
    '                    "                                   AND TableEmp_Contribution.Periodid = TableEmployer_Contribution.Periodid " & _
    '                    " JOIN   " & _
    '                    "	(   " & _
    '                    "		SELECT     " & _
    '                    "			   Gross.Periodid  " & _
    '                    "            , Gross.PeriodName  " & _
    '                    "            , Gross.Empid  " & _
    '                    "            , SUM(Gross.Amount) AS GrossPay  " & _
    '                    "        FROM       " & _
    '                    "			(   " & _
    '                    "				SELECT      " & _
    '                    "					   cfcommon_period_tran.periodunkid AS Periodid  " & _
    '                    "                    , cfcommon_period_tran.period_name AS PeriodName  " & _
    '                    "                    , hremployee_master.employeeunkid AS Empid  " & _
    '                    "                    , ISNULL(prpayrollprocess_tran.amount,0) AS Amount  " & _
    '                    "                FROM prpayrollprocess_tran  " & _
    '                    "					 JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid  " & _
    '                    "                    JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid  " & _
    '                    "                    JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
    '                    "                    JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid  " & _
    '                    "                WHERE prtranhead_master.trnheadtype_id = 1  " & _
    '                    "                    AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0  " & _
    '                    "                    AND ISNULL(prtranhead_master.isvoid,0) = 0  " & _
    '                    "                    AND ISNULL(prtnaleave_tran.isvoid,0) = 0  " & _
    '                "				AND cfcommon_period_tran.isactive = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "			AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		) AS Gross " & _
    '                "		GROUP BY Gross.Periodid,Gross.PeriodName,Gross.Empid " & _
    '                    "	) AS TableGross ON TableMain.EmpId = TableGross.Empid  AND TableEmp_Contribution.Periodid = TableGross.Periodid  " & _
    '                    " LEFT JOIN " & _
    '                    "   (" & _
    '                    "       SELECT " & _
    '                    "	          cfcommon_period_tran.periodunkid AS Periodid " & _
    '                    "	        , cfcommon_period_tran.period_name AS PeriodName " & _
    '                    "	        , hremployee_master.employeeunkid AS Empid " & _
    '                    "	        , ISNULL(prpayrollprocess_tran.amount,0) AS BasicSal " & _
    '                "		FROM prpayrollprocess_tran " & _
    '                    "	        JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "	        JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                    "	        JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "	        JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
    '                    "       WHERE prtranhead_master.trnheadtype_id = 1 " & _
    '                    "	        AND prtranhead_master.typeof_id = 1 " & _
    '                    "	        AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
    '                    "	        AND ISNULL(prtranhead_master.isvoid,0) = 0 " & _
    '                    "	        AND ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '                "			AND cfcommon_period_tran.isactive = 1 "
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= "		AND hremployee_master.isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "		GROUP BY cfcommon_period_tran.periodunkid,cfcommon_period_tran.period_name,hremployee_master.employeeunkid,ISNULL(prpayrollprocess_tran.amount, 0) " & _
    '                "	) AS BAS ON BAS.Empid = TableMain.EmpId AND TableEmp_Contribution.Periodid = BAS.Periodid " & _
    '                    ") AS Contribution  " & _
    '                    "WHERE MemId = @MemId "
    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 


    '        'S.SANDEEP [ 08 June 2011 ] -- END


    '        'Sandeep [ 16 MAY 2011 ] -- End 


    '        If mintReportId = 1 Then
    '            StrQ &= "AND Periodid = @PeriodId "
    '        End If
    '        'Sandeep [ 01 MARCH 2011 ] -- End 




    '        'Vimal (30 Nov 2010) -- End





    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        Dim dtTable As DataTable = New DataView(dsList.Tables(0), "", Me.OrderByQuery, DataViewRowState.CurrentRows).ToTable


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim StrECode As String = String.Empty
    '        Dim intCnt As Integer = 1
    '        Dim dtFilterTable As DataTable = Nothing
    '        Dim mdicIsEmpAdded As New Dictionary(Of String, String)
    '        Dim rpt_Row As DataRow = Nothing
    '        Dim decColumn4Total, decColumn5Total, decColumn6Total, decColumn7Total, decColumn8Total As Decimal
    '        Dim decColumn4GrndTotal, decColumn5GrndTotal, decColumn6GrndTotal, decColumn7GrndTotal, decColumn8GrndTotal As Decimal

    '        decColumn4GrndTotal = 0 : decColumn5GrndTotal = 0 : decColumn6GrndTotal = 0 : decColumn7GrndTotal = 0 : decColumn8GrndTotal = 0

    '        'For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '        For Each dtRow As DataRow In dtTable.Rows
    '            StrECode = dtRow.Item("EmpCode").ToString.Trim

    '            If mdicIsEmpAdded.ContainsKey(StrECode) Then Continue For

    '            mdicIsEmpAdded.Add(StrECode, StrECode)

    '            dtFilterTable = New DataView(dtTable, "EmpCode = '" & StrECode & "'", "", DataViewRowState.CurrentRows).ToTable

    '            If dtFilterTable.Rows.Count > 0 Then
    '                For Each dtFRow As DataRow In dtFilterTable.Rows
    '                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '                    Select Case mintReportId
    '                        Case 1  'PERIOD WISE
    '                            rpt_Row.Item("Column1") = intCnt.ToString
    '                            rpt_Row.Item("Column8") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                            rpt_Row.Item("Column3") = dtFRow.Item("EmpName")
    '                            rpt_Row.Item("Column2") = dtRow.Item("MembershipNo")
    '                            intCnt += 1
    '                        Case 2  'MONTH WISE
    '                            rpt_Row.Item("Column1") = dtFRow.Item("EmpCode") & " - " & dtFRow.Item("EmpName")
    '                            rpt_Row.Item("Column3") = Format(CDec(dtFRow.Item("BasicSal")), GUI.fmtCurrency)
    '                            rpt_Row.Item("Column2") = dtFRow.Item("PeriodName")
    '                    End Select
    '                    rpt_Row.Item("Column4") = Format(CDec(dtFRow.Item("GrossPay")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column5") = Format(CDec(dtFRow.Item("EmpContrib")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column6") = Format(CDec(dtFRow.Item("EmprContrib")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column7") = Format(CDec(dtFRow.Item("Total")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column9") = dtRow.Item("EmpCode")

    '                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '                    decColumn4Total = decColumn4Total + CDec(dtFRow.Item("BasicSal"))
    '                    decColumn5Total = decColumn5Total + CDec(dtFRow.Item("GrossPay"))
    '                    decColumn6Total = decColumn6Total + CDec(dtFRow.Item("EmpContrib"))
    '                    decColumn7Total = decColumn7Total + CDec(dtFRow.Item("EmprContrib"))
    '                    decColumn8Total = decColumn8Total + CDec(dtFRow.Item("Total"))

    '                    decColumn4GrndTotal = decColumn4GrndTotal + CDec(dtFRow.Item("BasicSal"))
    '                    decColumn5GrndTotal = decColumn5GrndTotal + CDec(dtFRow.Item("GrossPay"))
    '                    decColumn6GrndTotal = decColumn6GrndTotal + CDec(dtFRow.Item("EmpContrib"))
    '                    decColumn7GrndTotal = decColumn7GrndTotal + CDec(dtFRow.Item("EmprContrib"))
    '                    decColumn8GrndTotal = decColumn8GrndTotal + CDec(dtFRow.Item("Total"))
    '                Next
    '            End If
    '            Select Case mintReportId
    '                Case 2
    '                    Dim rpt_dtRow() As DataRow = Nothing
    '                    rpt_dtRow = rpt_Data.Tables("ArutiTable").Select("Column9 = '" & StrECode & "'")
    '                    If rpt_dtRow.Length > 0 Then
    '                        For i As Integer = 0 To rpt_dtRow.Length - 1
    '                            rpt_dtRow(i).Item("Column76") = Format(decColumn4Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column77") = Format(decColumn5Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column78") = Format(decColumn6Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column79") = Format(decColumn7Total, GUI.fmtCurrency)
    '                            rpt_dtRow(i).Item("Column80") = Format(decColumn8Total, GUI.fmtCurrency)
    '                            rpt_Data.Tables("ArutiTable").AcceptChanges()
    '                        Next
    '                        decColumn4Total = 0 : decColumn5Total = 0 : decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0
    '                    End If
    '            End Select
    '        Next
    '        Select Case mintReportId
    '            Case 1
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport
    '            Case 2
    '                objRpt = New ArutiReport.Designer.rptPensionFundReport_Month
    '        End Select
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        'If mintReportId = 1 Then
    '        '    'S.SANDEEP [ 08 June 2011 ] -- START
    '        '    'ISSUE : MEMBERSHIP MAPPED WITH TRANSHEAD
    '        '    Dim intCnt As Integer = 1
    '        '    'S.SANDEEP [ 08 June 2011 ] -- END
    '        '    For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '        '        Dim rpt_Row As DataRow
    '        '        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '        '        rpt_Row.Item("Column1") = intCnt.ToString
    '        '        rpt_Row.Item("Column2") = dtRow.Item("MembershipNo")
    '        '        rpt_Row.Item("Column3") = dtRow.Item("EmpName")
    '        '        rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("GrossPay")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("EmpContrib")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("EmprContrib")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Total")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("BasicSal")), GUI.fmtCurrency)

    '        '        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        '        intCnt += 1
    '        '    Next
    '        '    objRpt = New ArutiReport.Designer.rptPensionFundReport
    '        'Else
    '        '    For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '        '        Dim rpt_Row As DataRow
    '        '        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '        '        rpt_Row.Item("Column1") = dtRow.Item("EmpCode") & " - " & dtRow.Item("EmpName")
    '        '        rpt_Row.Item("Column2") = dtRow.Item("PeriodName")
    '        '        rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("BasicSal")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("GrossPay")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("EmpContrib")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("EmprContrib")), GUI.fmtCurrency)
    '        '        rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("Total")), GUI.fmtCurrency)

    '        '        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '        '    Next
    '        '    objRpt = New ArutiReport.Designer.rptPensionFundReport_Month
    '        'End If



    '        objRpt.SetDataSource(rpt_Data)

    '        If mintReportId = 1 Then
    '            Call ReportFunction.TextChange(objRpt, "txtSchedular", Language.getMessage(mstrModuleName, 15, "MONTHLY SCHEDULE FOR ") & mstrPeriodName)
    '            Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 16, "EMPLOYER'S ") & mstrEmplrNumTypeName & " : " & mstrEmplrNumber)

    '            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 1, "Sr. No."))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberNo", Language.getMessage(mstrModuleName, 3, "Member Number"))
    '            Call ReportFunction.TextChange(objRpt, "txtMemberName", Language.getMessage(mstrModuleName, 17, "Member's Name"))
    '        Else
    '            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 30, "Employee :"))
    '            Call ReportFunction.TextChange(objRpt, "txtMonth", Language.getMessage(mstrModuleName, 29, "Month"))
    '            Call ReportFunction.TextChange(objRpt, "txtGroupTotal", Language.getMessage(mstrModuleName, 31, "Group Total :"))
    '        End If


    '        Call ReportFunction.TextChange(objRpt, "txtBasicSal", Language.getMessage(mstrModuleName, 27, "Basic Salary"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 4, "Gross Pay"))
    '        Call ReportFunction.TextChange(objRpt, "txtMemberContrib", Language.getMessage(mstrModuleName, 18, "Member's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployerContrib", Language.getMessage(mstrModuleName, 6, "Employer's Contribution"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 7, "Total"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 21, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 22, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " [ " & mstrReportTypeName & " ]")

    '        If mintReportId = 1 Then

    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot41")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot51")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot61")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot71")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot81")
    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7Total, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8Total, GUI.fmtCurrency))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        Else
    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpTot31")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpTot41")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpTot51")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpTot61")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrpTot71")

    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot31")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot41")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot51")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot61")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot71")

    '            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn4GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn6GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn7GrndTotal, GUI.fmtCurrency))
    '            Call ReportFunction.TextChange(objRpt, "txtTotal5", Format(decColumn8GrndTotal, GUI.fmtCurrency))
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '        End If


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        If mblnShowBasicSalary = False Then
    '            Dim strCurrentHdr As String = String.Empty
    '            If mintReportId = 1 Then
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column81", True)
    '                strCurrentHdr = "txtMemberName"

    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                strCurrentHdr = "txtGrossPay"

    '                ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal3", "txtMemberContrib")
    '                strCurrentHdr = "txtMemberContrib"

    '                ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal4", "txtEmployerContrib")
    '                strCurrentHdr = "txtEmployerContrib"

    '                ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal5", "txtTotal")
    '                strCurrentHdr = "txtMemberContrib"
    '            Else
    '                ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column31", True)
    '                ReportFunction.EnableSuppress(objRpt, "Column761", True)
    '                ReportFunction.EnableSuppress(objRpt, "txtTotal1", True)
    '                strCurrentHdr = "txtMonth"

    '                ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "Column771", "txtGrossPay")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal2", "txtGrossPay")
    '                strCurrentHdr = "txtGrossPay"

    '                ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '                ReportFunction.SetColumnOn(objRpt, "Column781", "txtMemberContrib")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal3", "txtMemberContrib")
    '                strCurrentHdr = "txtMemberContrib"

    '                ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '                ReportFunction.SetColumnOn(objRpt, "Column791", "txtEmployerContrib")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal4", "txtEmployerContrib")
    '                strCurrentHdr = "txtEmployerContrib"

    '                ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '                ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '                ReportFunction.SetColumnOn(objRpt, "Column801", "txtTotal")
    '                ReportFunction.SetColumnOn(objRpt, "txtTotal5", "txtTotal")
    '                strCurrentHdr = "txtTotal"
    '            End If
    '        End If
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        'If mblnShowBasicSalary = False Then
    '        '    Dim strCurrentHdr As String = String.Empty
    '        '    If mintReportId = 1 Then
    '        '        ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '        '        ReportFunction.EnableSuppress(objRpt, "frmlGrandTot81", True)
    '        '        ReportFunction.EnableSuppress(objRpt, "Column81", True)
    '        '        strCurrentHdr = "txtMemberName"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot41", "txtGrossPay")
    '        '        strCurrentHdr = "txtGrossPay"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot51", "txtMemberContrib")
    '        '        strCurrentHdr = "txtMemberContrib"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot61", "txtEmployerContrib")
    '        '        strCurrentHdr = "txtEmployerContrib"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot71", "txtTotal")
    '        '        strCurrentHdr = "txtMemberContrib"
    '        '    Else
    '        '        ReportFunction.EnableSuppress(objRpt, "txtBasicSal", True)
    '        '        ReportFunction.EnableSuppress(objRpt, "Column31", True)
    '        '        ReportFunction.EnableSuppress(objRpt, "frmlGrpTot31", True)
    '        '        ReportFunction.EnableSuppress(objRpt, "frmlGrandTot31", True)

    '        '        strCurrentHdr = "txtMonth"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtGrossPay", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column41", "txtGrossPay")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrpTot41", "txtGrossPay")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot41", "txtGrossPay")
    '        '        strCurrentHdr = "txtGrossPay"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtMemberContrib", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column51", "txtMemberContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrpTot51", "txtMemberContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot51", "txtMemberContrib")
    '        '        strCurrentHdr = "txtMemberContrib"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtEmployerContrib", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column61", "txtEmployerContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrpTot61", "txtEmployerContrib")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot61", "txtEmployerContrib")
    '        '        strCurrentHdr = "txtEmployerContrib"

    '        '        ReportFunction.SetColumnAfter(objRpt, "txtTotal", strCurrentHdr, 10)
    '        '        ReportFunction.SetColumnOn(objRpt, "Column71", "txtTotal")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrpTot71", "txtTotal")
    '        '        ReportFunction.SetColumnOn(objRpt, "frmlGrandTot71", "txtTotal")
    '        '        strCurrentHdr = "txtTotal"
    '        '    End If
    '        'End If

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sr. No.")
            Language.setMessage(mstrModuleName, 2, "Employee Name")
            Language.setMessage(mstrModuleName, 3, "Member Number")
            Language.setMessage(mstrModuleName, 4, "Gross Pay")
            Language.setMessage(mstrModuleName, 5, "Employee Contribution")
            Language.setMessage(mstrModuleName, 6, "Employer's Contribution")
            Language.setMessage(mstrModuleName, 7, "Total")
            Language.setMessage(mstrModuleName, 9, "Employee :")
            Language.setMessage(mstrModuleName, 10, "Gross Pay From :")
            Language.setMessage(mstrModuleName, 11, "to")
            Language.setMessage(mstrModuleName, 12, "Order By :")
            Language.setMessage(mstrModuleName, 13, "Select")
            Language.setMessage(mstrModuleName, 14, "NSSF No.")
            Language.setMessage(mstrModuleName, 15, "PPF No.")
            Language.setMessage(mstrModuleName, 16, "Period Wise")
            Language.setMessage(mstrModuleName, 17, "Month Wise")
            Language.setMessage(mstrModuleName, 18, "MONTHLY SCHEDULE FOR")
            Language.setMessage(mstrModuleName, 19, "EMPLOYER'S")
            Language.setMessage(mstrModuleName, 20, "Member's Name")
            Language.setMessage(mstrModuleName, 21, "Employee Code")
            Language.setMessage(mstrModuleName, 22, "Month")
            Language.setMessage(mstrModuleName, 23, "Group Total :")
            Language.setMessage(mstrModuleName, 24, "Basic Salary")
            Language.setMessage(mstrModuleName, 25, "Member's Contribution")
            Language.setMessage(mstrModuleName, 26, "Grand Total :")
            Language.setMessage(mstrModuleName, 27, "Printed Date :")
            Language.setMessage(mstrModuleName, 28, "Printed By :")
            Language.setMessage(mstrModuleName, 29, "Page :")
            Language.setMessage(mstrModuleName, 30, "Membership :")
            Language.setMessage(mstrModuleName, 31, "Employee")
            Language.setMessage(mstrModuleName, 32, "Exchange Rate :")
            Language.setMessage(mstrModuleName, 33, "Other Member Number")
            Language.setMessage(mstrModuleName, 34, "Employee")
            Language.setMessage(mstrModuleName, 35, "Personal")
            Language.setMessage(mstrModuleName, 36, "Pension Bill")
            Language.setMessage(mstrModuleName, 37, "Cost Center :")
            Language.setMessage(mstrModuleName, 38, "Sub Total :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

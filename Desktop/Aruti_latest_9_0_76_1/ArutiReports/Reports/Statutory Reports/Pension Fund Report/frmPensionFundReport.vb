'************************************************************************************************************************************
'Class Name : frmStatutoryContributionReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPensionFundReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPensionFundReport"
    Private objContribution As clsPensionFundReport

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    'Sohail (18 Mar 2013) -- End

    Private mintFirstPeriodId As Integer = 0 'Sohail (18 Jul 2013)

    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0
    'Pinkal (02-May-2013) -- End

    Private mstrMembershipIDs As String = ""
#End Region

#Region " Contructor "

    Public Sub New()
        objContribution = New clsPensionFundReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objContribution.SetDefaultValue()
        InitializeComponent()

        'S.SANDEEP [ 20 NOV 2013 ] -- START
        '_Show_ExcelExtra_Menu = True 'Sohail (12 Jan 2013)
        If Company._Object._Countryunkid = 129 Then 'Malawi
            _Show_ExcelExtra_Menu = False
            gbOtherEmplrContribution.Visible = True
            cboIncludeMembership.Enabled = False
        Else
            _Show_ExcelExtra_Menu = True
            gbOtherEmplrContribution.Visible = False
            cboIncludeMembership.Enabled = True
        End If
        'S.SANDEEP [ 20 NOV 2013 ] -- END
    End Sub

#End Region

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - SAVE Selection option on Pension Fund Report.
#Region " Private Enum "
    Private Enum enHeadTypeId
        Currency = 1
        Co_No_Type = 2
        Membership_IDs = 3
        Include_Membership = 4
        Show_EmployeeCode = 5
        Show_OtherMembership = 6
        GroupBy_CCenter = 7
        GrossPay_From = 8
        GrossPay_To = 9
        MembershipNo = 10
        Other_Earning = 11
        Emp_Contribution1 = 12
        Emp_Contribution2 = 13
    End Enum
#End Region
    'Sohail (03 Jul 2014) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMember As New clsmembership_master
        Dim objMaster As New clsMasterData
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead 'Sohail (28 Aug 2013)
        Try

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 05 JULY 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objMember.getListForCombo("Membership", True)
            'With cboMembership
            '    .ValueMember = "membershipunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            'End With
            'Sohail (18 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objMember.getListForCombo("Membership", False)
            dsCombos = objMember.getListForCombo("Membership", False, , 1)
            'Sohail (18 Jul 2013) -- End
            lvMembership.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dsRow As DataRow In dsCombos.Tables("Membership").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("membershipunkid"))

                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                lvMembership.Items.Add(lvItem)
            Next
            If lvMembership.Items.Count > 7 Then
                colhMembershipName.Width = 209 - 18
            Else
                colhMembershipName.Width = 209
            End If
            'Sohail (18 May 2013) -- End

            'Sohail (18 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            dsCombos = objMember.getListForCombo("Membership", True, , 2)
            With cboIncludeMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With
            'Sohail (18 Jul 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1) 'Sohail (18 Jul 2013)
            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (18 Jul 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                'Sohail (18 Jul 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                If mintFirstPeriodId > 0 Then
                    .SelectedValue = mintFirstPeriodId
                Else
                .SelectedValue = 0
                End If
                'Sohail (18 Jul 2013) -- End
            End With

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objMaster.getComboListForHeadType("HeadType")
            'With cboEmpHeadType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            'End With

            'With cboCoHeadType
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables(0).Copy
            '    .SelectedValue = 0
            'End With
            'Sohail (27 Aug 2012) -- End

            'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("List", True, enTranHeadType.EmployersStatutoryContributions, , , , True)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployersStatutoryContributions, , , , True)
            'Sohail (21 Aug 2015) -- End
            With cboEmplrContrib1
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
            End With
            With cboEmplrContrib2
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 20 NOV 2013 ] -- END

            dsCombos = objContribution.GetEmployer_No("EmprNo")
            With cboCoNumberType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objContribution.GetReportType("Type")
            With cboReportType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 1
            End With

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'Sohail (28 Aug 2013) -- End

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            'Pinkal (02-May-2013) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objEmp = Nothing
            dsCombos = Nothing
            objTranHead = Nothing 'Sohail (28 Aug 2013)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'cboCoHeadType.SelectedValue = 0 'Sohail (27 Aug 2012)
            cboCoNumberType.SelectedValue = 0
            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'cboCTypeOf.SelectedValue = 0
            'cboEmpHeadType.SelectedValue = 0
            'Sohail (27 Aug 2012) -- End
            cboEmployee.SelectedValue = 0
            'cboETypeOf.SelectedValue = 0 'Sohail (27 Aug 2012)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'cboMembership.SelectedValue = 0
            'Sohail (18 May 2013) -- End

            'Sohail (18 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstPeriodId
            cboIncludeMembership.SelectedValue = 0
            'Sohail (18 Jul 2013) -- End
            cboOtherEarning.SelectedValue = 0 'Sohail (28 Aug 2013)
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport 'Sohail (07 Sep 2013)

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'txtCoContFrom.Text = ""
            'txtCoContTo.Text = ""
            'txtEmpContFrom.Text = ""
            'txtEmpContTo.Text = ""
            'Sohail (27 Aug 2012) -- End
            txtGrossPayFrom.Text = ""
            txtGrossPayTo.Text = ""
            txtMembershipNo.Text = ""
            txtNumber.Text = ""
            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'chkShowBasicSal.Checked = True
            'Nilay (13-Oct-2016) -- End
            cboReportType.SelectedValue = 1
            objContribution.setDefaultOrderBy(CInt(cboReportType.SelectedValue))
            txtOrderBy.Text = objContribution.OrderByDisplay

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            'Sohail (18 Mar 2013) -- End


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            'Pinkal (02-May-2013) -- End

            'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
            cboEmplrContrib1.SelectedValue = 0
            cboEmplrContrib2.SelectedValue = 0
            'S.SANDEEP [ 20 NOV 2013 ] -- END

            'Sohail (31 Mar 2014) -- Start
            'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
            chkShowEmployeeCode.Checked = True
            chkShowOtherMembershipNo.Checked = True
            'Sohail (31 Mar 2014) -- End
            chkShowGroupByCCenter.Checked = False 'Sohail (18 Apr 2014)

            'Anjan [12 November 2014] -- Start
            'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
            chkDoNotShowBasicSalary.Checked = False
            'Anjan [12 November 2014] -- End

            'Anjan [07 October 2016] -- Start
            'ENHANCEMENT : Include Ignore Zero Value.
            chkIgnoreZero.Checked = False
            'Anjan [07 October 2016] -- End

            Call GetValue() 'Sohail (03 Jul 2014) 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - SAVE Selection option on Pension Fund Report.
    Private Sub FillList()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Try
            lvMembership.Items.Clear()
            Dim lvItem As ListViewItem

            dsCombos = objMember.getListForCombo("Membership", False, , 1)

            For Each dsRow As DataRow In dsCombos.Tables("Membership").Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("membershipunkid"))

                lvItem.SubItems.Add(dsRow.Item("name").ToString)

                If mstrMembershipIDs.Trim <> "" Then
                    If mstrMembershipIDs.Split(",").Contains(dsRow.Item("membershipunkid").ToString) = True Then
                        lvItem.Checked = True
                    End If
                End If

                lvMembership.Items.Add(lvItem)
            Next
            If lvMembership.Items.Count > 7 Then
                colhMembershipName.Width = 209 - 18
            Else
                colhMembershipName.Width = 209
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PensionFundReport)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.Co_No_Type
                            cboCoNumberType.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Membership_IDs
                            mstrMembershipIDs = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.Include_Membership
                            cboIncludeMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_EmployeeCode
                            chkShowEmployeeCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_OtherMembership
                            chkShowOtherMembershipNo.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.GroupBy_CCenter
                            chkShowGroupByCCenter.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.GrossPay_From
                            txtGrossPayFrom.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.GrossPay_To
                            txtGrossPayTo.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.MembershipNo
                            txtMembershipNo.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.Other_Earning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            If CInt(dsRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If
                        Case enHeadTypeId.Emp_Contribution1
                            cboEmplrContrib1.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Emp_Contribution2
                            cboEmplrContrib2.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (03 Jul 2014) -- End

    Private Function SetFilter() As Boolean
        Try
            Call objContribution.SetDefaultValue()

            mstrMembershipIDs = "" 'Sohail (03 Jul 2014)

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboMembership.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
            '    cboMembership.Focus()
            '    Return False
            'End If
            If lvMembership.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select atleast one Membership to continue."), enMsgBoxStyle.Information)
                lvMembership.Focus()
                Return False
            End If
            'Sohail (18 May 2013) -- End

            If cboReportType.SelectedValue = 1 Then
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Return False
                End If
            End If

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboEmpHeadType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
            '    cboEmpHeadType.Focus()
            '    Return False
            'End If

            'If CInt(cboETypeOf.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Contribution Type is mandatory information. Please select Employee Contribution Type to continue."), enMsgBoxStyle.Information)
            '    cboETypeOf.Focus()
            '    Return False
            'End If

            'If CInt(cboCoHeadType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employer Contribution is mandatory information. Please select Employer Contribution to continue."), enMsgBoxStyle.Information)
            '    cboCoHeadType.Focus()
            '    Return False
            'End If

            'If CInt(cboCTypeOf.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employer Contribution Type is mandatory information. Please select Employer Contribution Type to continue."), enMsgBoxStyle.Information)
            '    cboCTypeOf.Focus()
            '    Return False
            'End If
            'Sohail (27 Aug 2012) -- End

            If cboReportType.SelectedValue = 1 Then
                If CInt(cboCoNumberType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employer Number is mandatory information. Please select Employer Number to continue."), enMsgBoxStyle.Information)
                    cboCoNumberType.Focus()
                    Return False
                End If
            End If

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'Sohail (28 Aug 2013) -- End


            'S.SANDEEP [ 20 NOV 2013 ] -- START
            If gbOtherEmplrContribution.Visible = True Then

                'Anjan [03 Feb 2014] -- Start
                'ENHANCEMENT : Requested by Rutta for malawi, comment on online document #343.
                'If CInt(cboEmplrContrib1.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select Employer Contribution1 to continue."), enMsgBoxStyle.Information)
                '    cboEmplrContrib1.Focus()
                '    Return False
                'End If
                'If CInt(cboEmplrContrib2.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Employer Contribution2 to continue."), enMsgBoxStyle.Information)
                '    cboEmplrContrib2.Focus()
                '    Return False
                'End If
                'Anjan [03 Feb 2014 ] -- End


                If cboEmplrContrib1.SelectedIndex > 0 Then
                    objContribution._EmplrContrib1Id = CInt(cboEmplrContrib1.SelectedValue)
                    objContribution._EmplrContrib1_Text = cboEmplrContrib1.Text
                End If
                If cboEmplrContrib2.SelectedIndex > 0 Then
                    objContribution._EmplrContrib2Id = CInt(cboEmplrContrib2.SelectedValue)
                    objContribution._EmplrContrib2_Text = cboEmplrContrib2.Text
                End If

            End If
            'S.SANDEEP [ 20 NOV 2013 ] -- END


            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboMembership.SelectedValue) > 0 Then
            '    objContribution._MembershipId = cboMembership.SelectedValue
            '    objContribution._MembershipName = cboMembership.Text
            'End If
            Dim strIDs As String = ""
            Dim strNAMEs As String = ""
            For Each lvItem As ListViewItem In lvMembership.CheckedItems
                strIDs &= ", " & lvItem.Tag.ToString
                strNAMEs &= ", " & lvItem.SubItems(colhMembershipName.Index).Text
            Next
            'Sohail (18 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboIncludeMembership.SelectedValue) > 0 Then
                strNAMEs &= ", " & cboIncludeMembership.Text
            End If
            'Sohail (18 Jul 2013) -- End
            strIDs = strIDs.Substring(2)
            strNAMEs = strNAMEs.Substring(2)
            objContribution._MembershipIDs = strIDs
            objContribution._MembershipNAMEs = strNAMEs
            objContribution._NonPayrollMembershipId = CInt(cboIncludeMembership.SelectedValue) 'Sohail (18 Jul 2013)
            'Sohail (18 May 2013) -- End
            mstrMembershipIDs = strIDs.Replace(" ", "") 'Sohail (03 Jul 2014)

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objContribution._PeriodId = cboPeriod.SelectedValue
                objContribution._PeriodName = cboPeriod.Text
            End If

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboEmpHeadType.SelectedValue) > 0 Then
            '    objContribution._EmpHeadTypeId = cboEmpHeadType.SelectedValue
            'End If

            'If CInt(cboETypeOf.SelectedValue) > 0 Then
            '    objContribution._EmpTypeOfId = cboETypeOf.SelectedValue
            'End If

            'If CInt(cboCoHeadType.SelectedValue) > 0 Then
            '    objContribution._EmprHeadTypeId = cboCoHeadType.SelectedValue
            'End If

            'If CInt(cboCTypeOf.SelectedValue) > 0 Then
            '    objContribution._EmprTypeOfId = cboCTypeOf.SelectedValue
            'End If
            'Sohail (27 Aug 2012) -- End

            If CInt(cboCoNumberType.SelectedValue) > 0 Then
                objContribution._EmplrNumTypeId = cboCoNumberType.SelectedValue
                objContribution._EmplrNumTypeName = cboCoNumberType.Text
                objContribution._EmplrNumber = txtNumber.Text
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                objContribution._EmpId = cboEmployee.SelectedValue
                objContribution._EmpName = cboEmployee.Text
            End If

            If txtGrossPayFrom.Text.Trim <> "" AndAlso txtGrossPayTo.Text.Trim <> "" Then
                objContribution._GrossPayFrom = txtGrossPayFrom.Text
                objContribution._GrossPayTo = txtGrossPayTo.Text
            End If

            'Sohail (27 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            'If txtEmpContFrom.Text.Trim <> "" AndAlso txtEmpContTo.Text.Trim <> "" Then
            '    objContribution._EmpContribFrom = txtEmpContFrom.Text
            '    objContribution._EmpContribTo = txtEmpContTo.Text
            'End If

            'If txtCoContFrom.Text.Trim <> "" AndAlso txtCoContTo.Text.Trim <> "" Then
            '    objContribution._EmplrContribFrom = txtCoContFrom.Text
            '    objContribution._EmplrContribTo = txtCoContTo.Text
            'End If
            'Sohail (27 Aug 2012) -- End

            If txtMembershipNo.Text.Trim <> "" Then
                objContribution._MembershipNo = txtMembershipNo.Text
            End If

            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objContribution._ShowBasicSalary = CBool(chkShowBasicSal.CheckState)
            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'objContribution._ShowBasicSalary = ConfigParameter._Object._ShowBasicSalaryOnStatutoryReport
            objContribution._ShowBasicSalary = CBool(chkShowBasicSal.CheckState)
            'Nilay (13-Oct-2016) -- End
            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True Then
                objContribution._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objContribution._OtherEarningTranId = 0
            End If
            'Sohail (28 Aug 2013) -- End

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            objContribution._ViewByIds = mstrStringIds
            objContribution._ViewIndex = mintViewIdx
            objContribution._ViewByName = mstrStringName
            objContribution._Analysis_Fields = mstrAnalysis_Fields
            objContribution._Analysis_Join = mstrAnalysis_Join
            objContribution._Report_GroupName = mstrReport_GroupName
            objContribution._Analysis_OrderBy = mstrAnalysis_OrderBy_GName
            'Sohail (18 Mar 2013) -- End

            objContribution._ReportId = cboReportType.SelectedValue
            objContribution._ReportTypeName = cboReportType.Text


            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objContribution._CountryId = CInt(cboCurrency.SelectedValue)
            objContribution._BaseCurrencyId = mintBaseCurrId
            objContribution._PaidCurrencyId = mintPaidCurrencyId

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objContribution._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            objContribution._ExchangeRate = LblCurrencyRate.Text

            'Pinkal (02-May-2013) -- End

            'Sohail (31 Mar 2014) -- Start
            'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
            objContribution._ShowEmployeeCode = chkShowEmployeeCode.Checked
            objContribution._ShowOtherMembershipNo = chkShowOtherMembershipNo.Checked
            'Sohail (31 Mar 2014) -- End
            objContribution._ShowGroupByCostCenter = chkShowGroupByCCenter.Checked 'Sohail (18 Apr 2014)

            'Anjan [12 November 2014] -- Start
            'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
            objContribution._DoNotShowBasic_GrossSalary = chkDoNotShowBasicSalary.CheckState
            'Anjan [12 November 2014] -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            objContribution._Database_Start_Date = FinancialYear._Object._Database_Start_Date
            'Sohail (21 Aug 2015) -- End

            'Anjan [07 October 2016] -- Start
            'ENHANCEMENT : Include Ignore Zero Value.
            objContribution._IgnoreZeroValue = chkIgnoreZero.CheckState
            'Anjan [07 October 2016] -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter()", mstrModuleName)
        End Try
    End Function

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub DoOperation(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 May 2013) -- End

#End Region

#Region " Forms "

    Private Sub frmStatutoryContributionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objContribution = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmStatutoryContributionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatutoryContributionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objContribution._ReportName
            Me._Message = objContribution._ReportDesc

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Call FillCombo()
            Call ResetValue()
            Call FillList() 'Sohail (03 Jul 2014)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStatutoryContributionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim dtStart As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Dim dtEnd As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If cboReportType.SelectedValue = 1 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

                dtStart = objPeriod._Start_Date
                dtEnd = objPeriod._End_Date
            End If

            objContribution._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtEnd))
            'Sohail (03 Aug 2019) -- End

            'Sohail (12 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objContribution.generateReport(0, e.Type, enExportAction.None)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objContribution.generateReport(CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._ExportReportPath, _
            '                                  ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              dtStart, _
                                              dtEnd, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedIndex), e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (12 Jan 2013) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim dtStart As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Dim dtEnd As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If cboReportType.SelectedValue = 1 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

                dtStart = objPeriod._Start_Date
                dtEnd = objPeriod._End_Date
            End If

            objContribution._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtEnd))
            'Sohail (03 Aug 2019) -- End

            'Sohail (12 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'objContribution.generateReport(0, enPrintAction.None, e.Type)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objContribution.generateReport(CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                                  User._Object._Userunkid, _
            '                                  FinancialYear._Object._YearUnkid, _
            '                                  Company._Object._Companyunkid, _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                  ConfigParameter._Object._UserAccessModeSetting, _
            '                                  True, ConfigParameter._Object._ExportReportPath, _
            '                                  ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objContribution.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              dtStart, _
                                              dtEnd, _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, CInt(cboReportType.SelectedIndex), enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (12 Jan 2013) -- End
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPensionFundReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPensionFundReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'S.SANDEEP [ 20 NOV 2013 ] -- START   -- MALAWI PENSION FUND
    Private Sub objbtnSearch_Contrib2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch_Contrib2.Click, objbtnSearch_Contrib1.Click
        Dim frm As New frmCommonSearch
        Try
            Dim cbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearch_Contrib1.Name.ToUpper
                    cbo = cboEmplrContrib1
                Case objbtnSearch_Contrib2.Name.ToUpper
                    cbo = cboEmplrContrib2
            End Select
            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .CodeMember = "code"
                .DataSource = cbo.DataSource
                If .DisplayDialog = True Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Contrib2_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 NOV 2013 ] -- END

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - SAVE Selection option on Pension Fund Report.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To 13
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.PensionFundReport
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Co_No_Type
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCoNumberType.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Membership_IDs
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = mstrMembershipIDs

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIncludeMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_EmployeeCode
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowEmployeeCode.Checked

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Show_OtherMembership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowOtherMembershipNo.Checked

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.GroupBy_CCenter
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkShowGroupByCCenter.Checked

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.GrossPay_From
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtGrossPayFrom.Text

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.GrossPay_To
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtGrossPayTo.Text

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.MembershipNo
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtMembershipNo.Text

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Other_Earning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Emp_Contribution1
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmplrContrib1.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)

                    Case enHeadTypeId.Emp_Contribution2
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmplrContrib2.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PensionFundReport, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (03 Jul 2014) -- End

#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objContribution.setOrderBy(CInt(cboReportType.SelectedValue))
            txtOrderBy.Text = objContribution.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboCoNumberType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCoNumberType.SelectedIndexChanged
        Try
            If CInt(cboCoNumberType.SelectedValue) > 0 Then
                Select Case CInt(cboCoNumberType.SelectedValue)
                    Case 1     'NSSF
                        txtNumber.Text = Company._Object._Nssfno
                    Case 2     'PPF
                        txtNumber.Text = Company._Object._Ppfno

                        'S.SANDEEP [ 17 OCT 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case 3  'REG CAPTION 1
                        txtNumber.Text = Company._Object._Reg1_Value
                    Case 4  'REG CAPTION 2
                        txtNumber.Text = Company._Object._Reg2_Value
                    Case 5  'REG CAPTION 3
                        txtNumber.Text = Company._Object._Reg3_Value
                        'S.SANDEEP [ 17 OCT 2012 ] -- END
                End Select
            Else
                txtNumber.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCoNumberType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (27 Aug 2012) -- Start
    'TRA - ENHANCEMENT
    'Private Sub cboCoHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboCoHeadType.SelectedValue) > 0 Then
    '            Dim objMaster As New clsMasterData
    '            Dim dsList As New DataSet
    '            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboCoHeadType.SelectedValue))
    '            With cboCTypeOf
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsList.Tables(0)
    '                .SelectedValue = 0
    '            End With
    '            objMaster = Nothing
    '            dsList = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboCoHeadType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboEmpHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboEmpHeadType.SelectedValue) > 0 Then
    '            Dim objMaster As New clsMasterData
    '            Dim dsList As New DataSet
    '            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboEmpHeadType.SelectedValue))
    '            With cboETypeOf
    '                .ValueMember = "Id"
    '                .DisplayMember = "Name"
    '                .DataSource = dsList.Tables(0)
    '                .SelectedValue = 0
    '            End With
    '            objMaster = Nothing
    '            dsList = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmpHeadType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (27 Aug 2012) -- End

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case CInt(cboReportType.SelectedValue)
                Case 1     'Period Wise
                    cboPeriod.SelectedValue = 0
                    cboPeriod.Enabled = True
                    cboCoNumberType.SelectedValue = 0
                    cboCoNumberType.Enabled = True
                    txtMembershipNo.Text = ""
                    txtMembershipNo.Enabled = True

                    'Pinkal (02-May-2013) -- Start
                    'Enhancement : TRA Changes
                    cboCurrency.SelectedValue = 0
                    cboCurrency.Enabled = True
                    'Pinkal (02-May-2013) -- End

                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    chkShowGroupByCCenter.Visible = True
                    chkShowGroupByCCenter.Checked = False
                    'Sohail (18 Apr 2014) -- End

                    'Sohail (31 Mar 2014) -- Start
                    'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
                    If Company._Object._Countryunkid = 129 Then
                        chkShowEmployeeCode.Checked = False
                        chkShowOtherMembershipNo.Checked = False
                        chkShowEmployeeCode.Visible = False
                        chkShowOtherMembershipNo.Visible = False
                    Else
                        chkShowEmployeeCode.Checked = True
                        chkShowOtherMembershipNo.Checked = True
                        chkShowEmployeeCode.Visible = True
                        chkShowOtherMembershipNo.Visible = True
                    End If
                    'Sohail (31 Mar 2014) -- End

                Case 2     'Month Wise
                    cboPeriod.SelectedValue = 0
                    cboPeriod.Enabled = False
                    cboCoNumberType.SelectedValue = 0
                    cboCoNumberType.Enabled = False
                    txtMembershipNo.Text = ""
                    txtMembershipNo.Enabled = False

                    'Pinkal (02-May-2013) -- Start
                    'Enhancement : TRA Changes
                    cboCurrency.SelectedValue = 0
                    cboCurrency.Enabled = False
                    'Pinkal (02-May-2013) -- End

                    'Sohail (18 Apr 2014) -- Start
                    'Enhancement - Show Group by Cost Center option on Pension Fund Report
                    chkShowGroupByCCenter.Visible = False
                    chkShowGroupByCCenter.Checked = False
                    'Sohail (18 Apr 2014) -- End

                    'Sohail (31 Mar 2014) -- Start
                    'ENHANCEMENT - Show Employee Code and Other Membership No on Pension Fund Report.
                    chkShowEmployeeCode.Checked = False
                    chkShowOtherMembershipNo.Checked = False
                    chkShowEmployeeCode.Visible = False
                    chkShowOtherMembershipNo.Visible = False
                    'Sohail (31 Mar 2014) -- End

            End Select
            objContribution.setDefaultOrderBy(CInt(cboReportType.SelectedValue))
            txtOrderBy.Text = objContribution.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (18 Mar 2013) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objchkAllMembership_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllMembership.CheckedChanged
        Try
            Call DoOperation(lvMembership, CBool(objchkAllMembership.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllMembership_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 May 2013) -- End

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (28 Aug 2013) -- End



    'Anjan [12 November 2014] -- Start
    'ENHANCEMENT : included setting for basic/gross salary to be shown on report. Requested by Rutta.
#Region "Checkbox Event"

    Private Sub chkExcludeBasicSalary_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDoNotShowBasicSalary.CheckedChanged, _
                                                                                                                         chkShowBasicSal.CheckedChanged
        'Nilay (13-Oct-2016) -- [chkShowBasicSal.CheckedChanged]
        Try
            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'If chkDoNotShowBasicSalary.Checked Then
            '    gbBasicSalaryOtherEarning.Checked = False
            '    gbBasicSalaryOtherEarning.Enabled = False
            'Else
            '    gbBasicSalaryOtherEarning.Enabled = True
            'End If
            Select Case CType(sender, CheckBox).Name.ToUpper
                Case "CHKDONOTSHOWBASICSALARY"
            If chkDoNotShowBasicSalary.Checked Then
                gbBasicSalaryOtherEarning.Checked = False
                gbBasicSalaryOtherEarning.Enabled = False
                        chkShowBasicSal.Checked = False
            Else
                gbBasicSalaryOtherEarning.Enabled = True
            End If
                Case "CHKSHOWBASICSAL"
                    If chkShowBasicSal.Checked = True Then
                        chkDoNotShowBasicSalary.Checked = False
                    End If
            End Select
            'Nilay (13-Oct-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExcludeBasicSalary_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Anjan [12 November 2014] -- End

#End Region


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

#Region "ComboBox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                Dim dsList As DataSet = Nothing
                dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    LblCurrencyRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                    mintPaidCurrencyId = 0
                    LblCurrencyRate.Text = ""
                End If
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                mintPaidCurrencyId = 0
                LblCurrencyRate.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (02-May-2013) -- End


    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbOtherEmplrContribution.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOtherEmplrContribution.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblCoNunbertype.Text = Language._Object.getCaption(Me.lblCoNunbertype.Name, Me.lblCoNunbertype.Text)
			Me.lblGrossPayFrom.Text = Language._Object.getCaption(Me.lblGrossPayFrom.Name, Me.lblGrossPayFrom.Text)
			Me.lblGrossPayTo.Text = Language._Object.getCaption(Me.lblGrossPayTo.Name, Me.lblGrossPayTo.Text)
			Me.lblMembershipNo.Text = Language._Object.getCaption(Me.lblMembershipNo.Name, Me.lblMembershipNo.Text)
			Me.lblSortBy.Text = Language._Object.getCaption(Me.lblSortBy.Name, Me.lblSortBy.Text)
			Me.elLineSort.Text = Language._Object.getCaption(Me.elLineSort.Name, Me.elLineSort.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.chkShowBasicSal.Text = Language._Object.getCaption(Me.chkShowBasicSal.Name, Me.chkShowBasicSal.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.colhMembershipName.Text = Language._Object.getCaption(CStr(Me.colhMembershipName.Tag), Me.colhMembershipName.Text)
			Me.lblIncludeMembership.Text = Language._Object.getCaption(Me.lblIncludeMembership.Name, Me.lblIncludeMembership.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.gbOtherEmplrContribution.Text = Language._Object.getCaption(Me.gbOtherEmplrContribution.Name, Me.gbOtherEmplrContribution.Text)
			Me.lblEmplrContrib2.Text = Language._Object.getCaption(Me.lblEmplrContrib2.Name, Me.lblEmplrContrib2.Text)
			Me.lblEmplrContrib1.Text = Language._Object.getCaption(Me.lblEmplrContrib1.Name, Me.lblEmplrContrib1.Text)
			Me.chkShowOtherMembershipNo.Text = Language._Object.getCaption(Me.chkShowOtherMembershipNo.Name, Me.chkShowOtherMembershipNo.Text)
			Me.chkShowEmployeeCode.Text = Language._Object.getCaption(Me.chkShowEmployeeCode.Name, Me.chkShowEmployeeCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select atleast one Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 3, "Employer Number is mandatory information. Please select Employer Number to continue.")
			Language.setMessage(mstrModuleName, 4, "Please select transaction head.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

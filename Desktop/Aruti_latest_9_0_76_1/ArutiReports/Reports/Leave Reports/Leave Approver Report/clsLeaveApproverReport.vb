
Imports Aruti.Data
Imports eZeeCommonLib


Public Class clsLeaveApproverReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsLeaveApproverReport"
    Private mstrReportId As String = enArutiReport.LeaveApproverReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Create_OnApproverReport()

        'Pinkal (25-Apr-2014) -- Start
        'Enhancement : TRA Changes
        Create_OnEmpListWithApproverReport()
        'Pinkal (25-Apr-2014) -- End

        Create_OnEmpWiseApproverReport()
    End Sub

#End Region

#Region " Private Variables "
    Private mstrOrderByQuery As String = String.Empty
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintApproverId As Integer = -1
    Private mstrApproverName As String = String.Empty
    Private mintLevelId As Integer = -1
    Private mstrLevelName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintLeaveTypeId As Integer = -1
    Private mstrLeaveTypeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (28-Oct-2014) -- Start
    'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.
    Private mblnIsLeaveApprover_ForLeaveType As Boolean = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
    Private mstrUserAccessFilter As String = ""
    'Pinkal (28-Oct-2014) -- End

#End Region

#Region "Properties"

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _ApproverID() As Integer
        Set(ByVal value As Integer)
            mintApproverId = value
        End Set
    End Property

    Public WriteOnly Property _ApproverName() As String
        Set(ByVal value As String)
            mstrApproverName = value
        End Set
    End Property

    Public WriteOnly Property _LevelID() As Integer
        Set(ByVal value As Integer)
            mintLevelId = value
        End Set
    End Property

    Public WriteOnly Property _LevelName() As String
        Set(ByVal value As String)
            mstrLevelName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeID() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeID() As Integer
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LeaveTypeName() As String
        Set(ByVal value As String)
            mstrLeaveTypeName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End


    'Pinkal (28-Oct-2014) -- Start
    'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    Public WriteOnly Property _IsLeaveApprover_ForLeaveType() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLeaveApprover_ForLeaveType = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (28-Oct-2014) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mintApproverId = 0
            mstrApproverName = ""
            mintLevelId = 0
            mstrLevelName = ""
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintLeaveTypeId = 0
            mstrLeaveTypeName = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""


            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            If mintApproverId > 0 Then
                Me._FilterQuery &= " AND lvleaveapprover_master.leaveapproverunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Leave Approver : ") & " " & mstrApproverName & " "
            End If

            If mintLevelId > 0 Then
                Me._FilterQuery &= " AND lvapproverlevel_master.levelunkid = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Approver Level : ") & " " & mstrLevelName & " "
            End If

            If mintEmployeeId > 0 Then

                'Pinkal (28-Oct-2014) -- Start
                'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

                If mintReportTypeId = 0 Then
                    Me._FilterQuery &= " AND h2.employeeunkId = @employeeunkId "
                Else
                    Me._FilterQuery &= " AND hremployee_master.employeeunkId = @employeeunkId "
                End If
                'Pinkal (28-Oct-2014) -- End

                objDataOperation.AddParameter("@employeeunkId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Employee : ") & " " & mstrEmployeeName & " "
            End If

            If mintLeaveTypeId > 0 Then
                Me._FilterQuery &= " AND lvleavetype_master.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveTypeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Leave Type : ") & " " & mstrLeaveTypeName & " "
            End If

            If Me.OrderByQuery <> "" Then
                'Pinkal (10-Mar-2017) -- Start
                'Enhancement - Working On Leave Module Re-Engineering.
                'Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
                mstrOrderByQuery = " ORDER BY " & Me.OrderByQuery
                'Pinkal (10-Mar-2017) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order By : ") & " " & Me.OrderByDisplay
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            'If mintCompanyUnkid <= 0 Then
            '    mintCompanyUnkid = Company._Object._Companyunkid
            'End If
            'Company._Object._Companyunkid = mintCompanyUnkid
            'ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            'If mintUserUnkid <= 0 Then
            '    mintUserUnkid = User._Object._Userunkid
            'End If

            'User._Object._Userunkid = mintUserUnkid


            'If mintReportTypeId = 0 Then
            '    objRpt = Generate_ApproverReport()

            '    'Pinkal (25-Apr-2014) -- Start
            '    'Enhancement : TRA Changes

            'ElseIf mintReportTypeId = 1 Then
            '    objRpt = Generate_EmpListWithApproverReport()

            'ElseIf mintReportTypeId = 2 Then
            '    objRpt = Generate_EmpWiseApproverReport()

            '    'Pinkal (25-Apr-2014) -- End

            'End If
            'Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            If mintReportTypeId = 0 Then

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'objRpt = Generate_ApproverReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) , True, ConfigParameter._Object._UserAccessModeSetting)
                objRpt = Generate_ApproverReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtTransferAsOnDate, True, xUserModeSetting)
                'Pinkal (06-Jan-2016) -- End


            ElseIf mintReportTypeId = 1 Then
                objRpt = Generate_EmpListWithApproverReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtTransferAsOnDate, xOnlyApproved, xUserModeSetting)

            ElseIf mintReportTypeId = 2 Then
                objRpt = Generate_EmpWiseApproverReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtTransferAsOnDate, xOnlyApproved, xUserModeSetting)

            End If
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            If intReportType = 0 Then
                OrderByDisplay = iColumn_ApproverReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_ApproverReport.ColumnItem(0).Name

                'Pinkal (25-Apr-2014) -- Start
                'Enhancement : TRA Changes

            ElseIf intReportType = 1 Then
                OrderByDisplay = iColumn_EmpListWithApproverReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_EmpListWithApproverReport.ColumnItem(0).Name

                'Pinkal (25-Apr-2014) -- End
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            If intReportType = 0 Then
                Call OrderByExecute(iColumn_ApproverReport)

                'Pinkal (25-Apr-2014) -- Start
                'Enhancement : TRA Changes

            ElseIf intReportType = 1 Then
                Call OrderByExecute(iColumn_EmpListWithApproverReport)

                'Pinkal (25-Apr-2014) -- End

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Approver Report "

    Dim iColumn_ApproverReport As New IColumnCollection

    Public Property Field_OnApproverReport() As IColumnCollection
        Get
            Return iColumn_ApproverReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_ApproverReport = value
        End Set
    End Property

    Private Sub Create_OnApproverReport()
        Try
            iColumn_ApproverReport.Clear()
            iColumn_ApproverReport.Add(New IColumn(" ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')", Language.getMessage(mstrModuleName, 9, "Leave Approver")))
            iColumn_ApproverReport.Add(New IColumn("lvapproverlevel_master.levelname", Language.getMessage(mstrModuleName, 10, "Approver Level")))

            If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
                iColumn_ApproverReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename,'')", Language.getMessage(mstrModuleName, 11, "Leave Type")))
            End If

            iColumn_ApproverReport.Add(New IColumn("ISNULL(h2.employeecode, '')", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            iColumn_ApproverReport.Add(New IColumn("ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.othername, '') + ' ' + ISNULL(h2.surname, '')", Language.getMessage(mstrModuleName, 6, "Employee Name")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnApproverReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_ApproverReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT "

    '        StrQ &= "  ISNULL(h1.employeecode,'') ApproverCode " & _
    '                     ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  Approver " & _
    '                     ", lvapproverlevel_master.levelname AS 'Level' " & _
    '                     ", ISNULL(h2.employeecode, '') Employeecode " & _
    '                     ", ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.othername, '') + ' ' + ISNULL(h2.surname, '') Employee "

    '        'Pinkal (28-Oct-2014) -- Start
    '        'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '        'If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
    '        If mblnIsLeaveApprover_ForLeaveType Then
    '            StrQ &= ", ISNULL(lvleavetype_master.leavename,'') leavename " & _
    '                     ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'') +  SPACE(15) +  @Leavetype +  ISNULL(lvleavetype_master.leavename,'')  As GroupName "
    '        Else
    '            StrQ &= ", '' leavename " & _
    '                    ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'')  As GroupName "
    '        End If
    '        'Pinkal (28-Oct-2014) -- End

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM  lvleaveapprover_tran " & _
    '                     " JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0 " & _
    '                     " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
    '                     " JOIN hremployee_master h1 ON h1.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
    '                     " JOIN hremployee_master h2 ON h2.employeeunkid = lvleaveapprover_tran.employeeunkid "

    '        'Pinkal (28-Oct-2014) -- Start
    '        'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '        'If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
    '        If mblnIsLeaveApprover_ForLeaveType Then
    '            StrQ &= " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
    '                    " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "
    '        End If

    '        'Pinkal (28-Oct-2014) -- End

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "h2")
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (28-Oct-2014) -- Start
    '        'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString.Trim.Replace("hremployee_master", "h2")
    '        'End If

    '        If mstrUserAccessFilter.Trim = "" Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            mstrUserAccessFilter &= mstrUserAccessFilter.Trim.Replace("hremployee_master", "h2")
    '        End If

    '        'Pinkal (28-Oct-2014) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        StrQ &= mstrOrderByQuery

    '        objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Approver Level : "))
    '        objDataOperation.AddParameter("@Leavetype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Leave Type : "))
    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("GroupName")
    '            rpt_Rows.Item("Column3") = dtRow.Item("Approver")
    '            rpt_Rows.Item("Column4") = dtRow.Item("Level")

    '            rpt_Rows.Item("Column5") = dtRow.Item("leavename")
    '            rpt_Rows.Item("Column6") = dtRow.Item("Employeecode")
    '            rpt_Rows.Item("Column7") = dtRow.Item("Employee")

    '            If dtRow.Item("GName").ToString() <> "" Then
    '                Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
    '                rpt_Rows.Item("Column10") = drSubTotal.Length
    '            End If

    '            rpt_Rows.Item("Column10") = dsList.Tables("DataTable").Rows.Count

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next


    '        objRpt = New ArutiReport.Designer.rptLeaveApprover


    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 5, "Employee Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 6, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 16, "Leave Approver : "))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 15, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_ApproverReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_ApproverReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "FROM lvleaveapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE lvleaveapprover_master.isvoid = 0 AND lvleaveapprover_master.isswap = 0 " & _
                   "    AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isexternalapprover = 1 AND lvleaveapprover_master.leaveapproverunkid = " & mintApproverId

            dsCompany = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            'Pinkal (01-Mar-2016) -- End



            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)

            objDataOperation.ClearParameters()


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'StrQ = "SELECT "

            'StrQ &= "  ISNULL(h1.employeecode,'') ApproverCode " & _
            '             ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  Approver " & _
            '             ", lvapproverlevel_master.levelname AS 'Level' " & _
            '             ", ISNULL(h2.employeecode, '') Employeecode " & _
            '             ", ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.othername, '') + ' ' + ISNULL(h2.surname, '') Employee "

            'If mblnIsLeaveApprover_ForLeaveType Then
            '    StrQ &= ", ISNULL(lvleavetype_master.leavename,'') leavename " & _
            '             ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'') +  SPACE(15) +  @Leavetype +  ISNULL(lvleavetype_master.leavename,'')  As GroupName "
            'Else
            '    StrQ &= ", '' leavename " & _
            '            ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'')  As GroupName "
            'End If

            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= " FROM  lvleaveapprover_tran " & _
            '             " JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0 " & _
            '             " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
            '             " JOIN hremployee_master h1 ON h1.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
            '             " JOIN hremployee_master h2 ON h2.employeeunkid = lvleaveapprover_tran.employeeunkid "


            'If mblnIsLeaveApprover_ForLeaveType Then
            '    StrQ &= " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
            '            " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "
            'End If

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry.Replace("hremployee_master", "h2")
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry.Replace("hremployee_master", "h2")
            'End If

            'StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "h2")
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry.Replace("hremployee_master", "h2")
            'End If

            'Call FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery

            'StrQ &= mstrOrderByQuery


            StrInnerQry = "SELECT " & _
                          "  #APPR_CODE# ApproverCode " & _
                          ", #APPR_NAME# Approver " & _
                         ", lvapproverlevel_master.levelname AS 'Level' " & _
                         ", ISNULL(h2.employeecode, '') Employeecode " & _
                         ", ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.othername, '') + ' ' + ISNULL(h2.surname, '') Employee "


            'Pinkal (10-Mar-2017) -- Start
            'Enhancement - Working On Leave Module Re-Engineering.
            'If mblnIsLeaveApprover_ForLeaveType Then
            '    StrInnerQry &= ", ISNULL(lvleavetype_master.leavename,'') leavename " & _
            '             ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'') +  SPACE(15) +  @Leavetype +  ISNULL(lvleavetype_master.leavename,'')  As GroupName "
            'Else
            '    StrInnerQry &= ", '' leavename " & _
            '            ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'')  As GroupName "
            'End If

            StrInnerQry &= "#GroupName# As GroupName"

            'Pinkal (10-Mar-2017) -- End


            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Fields
            Else
                StrInnerQry &= ", 0 AS Id, '' AS GName "
            End If

            StrInnerQry &= " FROM  lvleaveapprover_tran " & _
                         " JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0 " & _
                         " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                           " #EMP_JOIN# " & _
                         " JOIN hremployee_master h2 ON h2.employeeunkid = lvleaveapprover_tran.employeeunkid "

            If mblnIsLeaveApprover_ForLeaveType Then
                StrInnerQry &= " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
                        " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "
            End If

            StrInnerQry &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrInnerQry &= xUACQry.Replace("hremployee_master", "h2")
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrInnerQry &= xAdvanceJoinQry.Replace("hremployee_master", "h2")
            End If


            StrConditionQry &= " #WHERE_CONDITION# AND lvleaveapprover_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                               " AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrConditionQry &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "h2")
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrConditionQry &= " AND " & xUACFiltrQry.Replace("hremployee_master", "h2")
            End If

            Call FilterTitleAndFilterQuery()

            StrConditionQry &= Me._FilterQuery

            StrQ = StrInnerQry
            StrQ &= StrConditionQry & mstrOrderByQuery

            StrQ = StrQ.Replace("#APPR_CODE#", "ISNULL(h1.employeecode,'') ")
            StrQ = StrQ.Replace("#APPR_NAME#", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '') ")


            'Pinkal (10-Mar-2017) -- Start
            'Enhancement - Working On Leave Module Re-Engineering.
            If mblnIsLeaveApprover_ForLeaveType Then
                StrQ = StrQ.Replace("#GroupName#", ", ISNULL(lvleavetype_master.leavename,'') leavename " & _
                                              ", ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'') +  SPACE(15) +  @Leavetype +  ISNULL(lvleavetype_master.leavename,'')")
            Else
                StrQ = StrQ.Replace("#GroupName#", ", '' leavename , ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'')")
            End If
            'Pinkal (10-Mar-2017) -- End


            StrQ = StrQ.Replace("#ExAppr#", "0")
            StrQ = StrQ.Replace("#DBName#", "")
            StrQ = StrQ.Replace("#EMP_JOIN#", "JOIN hremployee_master h1 ON h1.employeeunkid = lvleaveapprover_master.leaveapproverunkid ")
            StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE 1 = 1 ")



            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Approver Level : "))
            objDataOperation.AddParameter("@Leavetype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Leave Type : "))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_CODE#", " '' ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")

                    'Pinkal (10-Mar-2017) -- Start
                    'Enhancement - Working On Leave Module Re-Engineering.
                    If mblnIsLeaveApprover_ForLeaveType Then
                        StrQ = StrQ.Replace("#GroupName#", ", ISNULL(lvleavetype_master.leavename,'') leavename " & _
                                                      ",  ISNULL(UEmp.username,'')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'') +  SPACE(15) +  @Leavetype +  ISNULL(lvleavetype_master.leavename,'')")
                    Else
                        StrQ = StrQ.Replace("#GroupName#", ", '' leavename ,  ISNULL(UEmp.username,'')  +  SPACE(15)  +  @Level + ISNULL(lvapproverlevel_master.levelname,'')")
                    End If
                    'Pinkal (10-Mar-2017) -- End

                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid ")
                Else
                    StrQ = StrQ.Replace("#APPR_CODE#", " CASE WHEN ISNULL(h1.employeecode, '') = '' THEN '' ELSE ISNULL(h1.employeecode, '') END ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " CASE WHEN ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') END ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                                      " JOIN #DBName#hremployee_master h1 ON UEmp.employeeunkid = h1.employeeunkid ")
                    StrQ &= mstrOrderByQuery
                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DBName#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DBName#", "")
                End If

                StrQ = StrQ.Replace("#ExAppr#", "1")
                StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE UEmp.companyunkid = " & dr("companyunkid") & " ")

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If

            Next


            'Pinkal (01-Mar-2016) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("GroupName")
                rpt_Rows.Item("Column3") = dtRow.Item("Approver")
                rpt_Rows.Item("Column4") = dtRow.Item("Level")

                rpt_Rows.Item("Column5") = dtRow.Item("leavename")
                rpt_Rows.Item("Column6") = dtRow.Item("Employeecode")
                rpt_Rows.Item("Column7") = dtRow.Item("Employee")

                If dtRow.Item("GName").ToString() <> "" Then
                    Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column10") = drSubTotal.Length
                End If

                rpt_Rows.Item("Column10") = dsList.Tables("DataTable").Rows.Count

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptLeaveApprover


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 5, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 6, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 16, "Leave Approver : "))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 15, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_ApproverReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

#Region " Employee WiseApprover Report "

    Dim iColumn_EmpWiseApproverReport As New IColumnCollection

    Public Property Field_OnEmpWiseApproverReport() As IColumnCollection
        Get
            Return iColumn_EmpWiseApproverReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_EmpWiseApproverReport = value
        End Set
    End Property

    Private Sub Create_OnEmpWiseApproverReport()
        Try
            iColumn_EmpWiseApproverReport.Clear()
            'iColumn_EmpWiseApproverReport.Add(New IColumn("ISNULL(lvleavetype_master.leavename,'')", Language.getMessage(mstrModuleName, 11, "Leave Type")))
            'iColumn_EmpWiseApproverReport.Add(New IColumn("lvapproverlevel_master.levelname", Language.getMessage(mstrModuleName, 10, "Approver Level")))
            'iColumn_EmpWiseApproverReport.Add(New IColumn(" ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')", Language.getMessage(mstrModuleName, 9, "Leave Approver")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnEmpWiseApproverReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_EmpWiseApproverReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT "

    '        StrQ &= " ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
    '                     ",ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.othername,'') + ' ' + ISNULL(h1.surname,'') AS 'Approver' " & _
    '                     ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
    '                     ", ISNULL(lvleavetype_master.leavename,'') leavename "


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM lvleaveapprover_tran " & _
    '                     " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  " & _
    '                     " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND isactive = 1 " & _
    '                     " LEFT JOIN hremployee_master h1 ON lvleaveapprover_master.leaveapproverunkid= h1.employeeunkid " & _
    '                     " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
    '                     " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
    '                     " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "


    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "h1")
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        'Pinkal (28-Oct-2014) -- Start
    '        'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString.Trim
    '        'End If

    '        If mstrUserAccessFilter.Trim = "" Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            mstrUserAccessFilter &= mstrUserAccessFilter.Trim
    '        End If

    '        'Pinkal (28-Oct-2014) -- End

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery



    '        'If Me._FilterQuery.Contains("ORDER BY") Then
    '        '    If Me._FilterQuery.Contains("lvapproverlevel_master.levelname") Then
    '        '        StrQ &= ",ISNULL(lvleavetype_master.leavename,'')"
    '        '    Else
    '        '        StrQ &= ",lvapproverlevel_master.levelname"
    '        '    End If
    '        'Else
    '        '    StrQ &= " ORDER BY ISNULL(lvleavetype_master.leavename,''),lvapproverlevel_master.levelname"
    '        'End If


    '        'Pinkal (25-Apr-2014) -- Start
    '        'Enhancement : TRA Changes

    '        'StrQ &= " ORDER BY ISNULL(lvleavetype_master.leavename,''),lvapproverlevel_master.levelname,ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.othername,'') + ' ' + ISNULL(h1.surname,'')"

    '        'Pinkal (25-Apr-2014) -- End



    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim mstrLevel As String = String.Empty
    '        Dim mstrLeavename As String = String.Empty
    '        Dim mstrApprover As String = String.Empty

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("leavename")
    '            rpt_Rows.Item("Column3") = dtRow.Item("Level")


    '            mstrApprover = ""

    '            Dim drRow() As DataRow = dsList.Tables(0).Select("leavename = '" & dtRow.Item("leavename").ToString() & "' AND Level= '" & dtRow.Item("Level").ToString() & "'")

    '            If drRow.Length > 0 Then

    '                If mstrLeavename <> dtRow.Item("leavename").ToString() Then
    '                    mstrLevel = ""
    '                    mstrLeavename = dtRow.Item("leavename").ToString()
    '                Else
    '                    If mstrLevel = dtRow.Item("Level").ToString() Then
    '                        Continue For
    '                    End If
    '                End If

    '                For i As Integer = 0 To drRow.Length - 1
    '                    If mstrLevel <> dtRow.Item("Level").ToString() Then
    '                        mstrApprover &= drRow(i)("Approver").ToString().Trim & ","
    '                    Else
    '                        Continue For
    '                    End If
    '                Next

    '                If mstrApprover.Trim.Length > 0 Then
    '                    mstrApprover = mstrApprover.Substring(0, mstrApprover.Trim.Length - 1)
    '                Else
    '                    Continue For
    '                End If

    '                rpt_Rows.Item("Column4") = mstrApprover
    '                rpt_Rows.Item("Column5") = dtRow.Item("Employee")

    '            End If

    '            If dtRow.Item("GName").ToString() <> "" Then
    '                Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
    '                rpt_Rows.Item("Column6") = drSubTotal.Length
    '            End If

    '            rpt_Rows.Item("Column6") = dsList.Tables("DataTable").Rows.Count

    '            mstrLevel = dtRow.Item("Level").ToString()

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)



    '        Next




    '        objRpt = New ArutiReport.Designer.rptEmployeeWiseLeaveApprover


    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
    '        End If


    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 9, "Leave Approver"))
    '        Call ReportFunction.TextChange(objRpt, "txtApproverLevel", Language.getMessage(mstrModuleName, 10, "Approver Level"))
    '        Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 19, "Leave Type :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 15, "Grand Total :"))

    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpWiseApproverReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function


    Private Function Generate_EmpWiseApproverReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            Dim objLeaveApprover As New clsleaveapprover_master
            dsCompany = objLeaveApprover.GetExternalApproverList(objDataOperation, "List")
            objLeaveApprover = Nothing

            'Pinkal (01-Mar-2016) -- End



            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)

            objDataOperation.ClearParameters()


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'StrQ = "SELECT "

            'StrQ &= " ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
            '             ",ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.othername,'') + ' ' + ISNULL(h1.surname,'') AS 'Approver' " & _
            '             ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
            '             ", ISNULL(lvleavetype_master.leavename,'') leavename "


            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= " FROM lvleaveapprover_tran " & _
            '             " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  " & _
            '             " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
            '             " LEFT JOIN hremployee_master h1 ON lvleaveapprover_master.leaveapproverunkid= h1.employeeunkid " & _
            '             " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
            '             " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
            '             " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            'StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter.Replace("hremployee_master", "h1")
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If

            'Call FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery


            StrInnerQry = "SELECT " & _
                                 " ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
                                 ",#APPR_NAME# AS 'Approver' " & _
                         ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
                         ", ISNULL(lvleavetype_master.leavename,'') leavename "


            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Fields
            Else
                StrInnerQry &= ", 0 AS Id, '' AS GName "
            End If

            StrInnerQry &= " FROM lvleaveapprover_tran " & _
                                  " JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                                  "  AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                         " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                                  " #EMP_JOIN# " & _
                         " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
                         " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_tran.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
                         " JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvleavetype_master.isactive = 1 "

            StrInnerQry &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrInnerQry &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrInnerQry &= xAdvanceJoinQry
            End If

            StrConditionQry &= " WHERE lvleaveapprover_tran.isvoid = 0 "


            Call FilterTitleAndFilterQuery()

            StrConditionQry &= Me._FilterQuery

            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#APPR_CODE#", "ISNULL(h1.employeecode,'') ")
            StrQ = StrQ.Replace("#APPR_NAME#", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '') ")
            StrQ = StrQ.Replace("#ExAppr#", "0")
            StrQ = StrQ.Replace("#DBName#", "")
            StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hremployee_master h1 ON lvleaveapprover_master.leaveapproverunkid= h1.employeeunkid ")

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows

                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                StrQ &= StrConditionQry

                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_CODE#", " '' ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'')  ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid AND UEmp.companyunkid = " & dr("companyunkid"))
                Else
                    StrQ = StrQ.Replace("#APPR_CODE#", " CASE WHEN ISNULL(h1.employeecode, '') = '' THEN '' ELSE ISNULL(h1.employeecode, '') END ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " CASE WHEN ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') END ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid AND UEmp.companyunkid = " & dr("companyunkid") & " " & _
                                                  " JOIN #DBName#hremployee_master h1 ON UEmp.employeeunkid = h1.employeeunkid ")
                End If


                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DBName#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DBName#", "")
                End If

                StrQ = StrQ.Replace("#ExAppr#", "1")

                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If

            Next


            'Pinkal (10-Mar-2017) -- Start
            'Enhancement - Working On Leave Module Re-Engineering.
            'Dim dview = New DataView(dsList.Tables(0).Copy, "", "Level", DataViewRowState.CurrentRows)
            Dim dview = New DataView(dsList.Tables(0).Copy, "", "Level,Leavename", DataViewRowState.CurrentRows)
            'Pinkal (10-Mar-2017) -- End


            dsList.Tables(0).Clear()
            dsList.Tables(0).Merge(dview.ToTable.Copy)
            dview = Nothing


            'Pinkal (01-Mar-2016) -- End

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mstrLevel As String = String.Empty
            Dim mstrLeavename As String = String.Empty
            Dim mstrApprover As String = String.Empty

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("leavename")
                rpt_Rows.Item("Column3") = dtRow.Item("Level")


                mstrApprover = ""

                Dim drRow() As DataRow = dsList.Tables(0).Select("leavename = '" & dtRow.Item("leavename").ToString() & "' AND Level= '" & dtRow.Item("Level").ToString() & "'")

                If drRow.Length > 0 Then

                    If mstrLeavename <> dtRow.Item("leavename").ToString() Then
                        mstrLevel = ""
                        mstrLeavename = dtRow.Item("leavename").ToString()
                    Else
                        If mstrLevel = dtRow.Item("Level").ToString() Then
                            Continue For
                        End If
                    End If

                    For i As Integer = 0 To drRow.Length - 1
                        If mstrLevel <> dtRow.Item("Level").ToString() Then
                            mstrApprover &= drRow(i)("Approver").ToString().Trim & ","
                        Else
                            Continue For
                        End If
                    Next

                    If mstrApprover.Trim.Length > 0 Then
                        mstrApprover = mstrApprover.Substring(0, mstrApprover.Trim.Length - 1)
                    Else
                        Continue For
                    End If

                    rpt_Rows.Item("Column4") = mstrApprover
                    rpt_Rows.Item("Column5") = dtRow.Item("Employee")

                End If

                If dtRow.Item("GName").ToString() <> "" Then
                    Dim drSubTotal As DataRow() = dsList.Tables(0).Select("GName = '" & dtRow.Item("GName").ToString() & "'")
                    rpt_Rows.Item("Column6") = drSubTotal.Length
                End If

                rpt_Rows.Item("Column6") = dsList.Tables("DataTable").Rows.Count

                mstrLevel = dtRow.Item("Level").ToString()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)


            Next


            objRpt = New ArutiReport.Designer.rptEmployeeWiseLeaveApprover


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection5", True)
            End If


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 9, "Leave Approver"))
            Call ReportFunction.TextChange(objRpt, "txtApproverLevel", Language.getMessage(mstrModuleName, 10, "Approver Level"))
            Call ReportFunction.TextChange(objRpt, "txtLeaveType", Language.getMessage(mstrModuleName, 19, "Leave Type :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 14, "Sub Total :"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 15, "Grand Total :"))

            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpWiseApproverReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End



#End Region


    'Pinkal (25-Apr-2014) -- Start
    'Enhancement : TRA Changes

#Region " Employee With Their Leave Approver Report "

    Dim iColumn_EmpListWithApproverReport As New IColumnCollection

    Public Property Field_OnEmpListWithApproverReport() As IColumnCollection
        Get
            Return iColumn_EmpListWithApproverReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_EmpListWithApproverReport = value
        End Set
    End Property

    Private Sub Create_OnEmpListWithApproverReport()
        Try
            iColumn_EmpListWithApproverReport.Clear()
            iColumn_EmpListWithApproverReport.Add(New IColumn("lvapproverlevel_master.priority", Language.getMessage(mstrModuleName, 27, "Approver Priority")))
            iColumn_EmpListWithApproverReport.Add(New IColumn("lvapproverlevel_master.levelname", Language.getMessage(mstrModuleName, 10, "Approver Level")))
            iColumn_EmpListWithApproverReport.Add(New IColumn(" ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')", Language.getMessage(mstrModuleName, 9, "Leave Approver")))
            iColumn_EmpListWithApproverReport.Add(New IColumn("ISNULL(hremployee_master.employeecode, '')", Language.getMessage(mstrModuleName, 5, "Employee Code")))
            iColumn_EmpListWithApproverReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '')", Language.getMessage(mstrModuleName, 6, "Employee Name")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnEmpListWithApproverReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Private Function Generate_EmpListWithApproverReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '    Try
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        StrQ = "SELECT ISNULL(hremployee_master.employeecode,'') AS 'EmployeeCode' " & _
    '                     ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
    '                     ",ISNULL(h1.employeecode,'') AS 'ApproverCode' " & _
    '                     ",ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.othername,'') + ' ' + ISNULL(h1.surname,'') AS 'Approver' " & _
    '                     ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
    '                     ",ISNULL(dept.name,'') AS 'Department' " & _
    '                     ",ISNULL(jb.job_name,'') AS 'JobTitle' "


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If

    '        StrQ &= " FROM lvleaveapprover_tran " & _
    '                     " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  " & _
    '                     " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND isactive = 1 " & _
    '                     " LEFT JOIN hremployee_master h1 ON lvleaveapprover_master.leaveapproverunkid= h1.employeeunkid " & _
    '                     " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
    '                     " LEFT JOIN hrdepartment_master dept ON dept.departmentunkid= h1.departmentunkid " & _
    '                     " LEFT JOIN hrjob_master jb ON jb.jobunkid= h1.jobunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "

    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter.Trim
    '        End If

    '        'Pinkal (28-Oct-2014) -- Start
    '        'Enhancement -  ENHANCEMENT FOR REQUIRED BY TRA ON ONLINE LEAVE ISSUES.

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString.Trim
    '        'End If

    '        If mstrUserAccessFilter.Trim = "" Then
    '            mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '        End If

    '        If mstrUserAccessFilter.Trim.Length > 0 Then
    '            mstrUserAccessFilter &= mstrUserAccessFilter.Trim
    '        End If

    '        'Pinkal (28-Oct-2014) -- End


    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery


    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim mstrLevel As String = String.Empty
    '        Dim mstrLeavename As String = String.Empty
    '        Dim mstrApprover As String = String.Empty

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '            rpt_Rows.Item("Column1") = dtRow.Item("GName")
    '            rpt_Rows.Item("Column2") = dtRow.Item("EmployeeCode").ToString() & " : " & dtRow.Item("Employee").ToString()
    '            rpt_Rows.Item("Column3") = dtRow.Item("ApproverCode").ToString()
    '            rpt_Rows.Item("Column4") = dtRow.Item("Approver").ToString()
    '            rpt_Rows.Item("Column5") = dtRow.Item("Department").ToString()
    '            rpt_Rows.Item("Column6") = dtRow.Item("JobTitle").ToString()
    '            rpt_Rows.Item("Column7") = dtRow.Item("Level").ToString()
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptEmpListWithLeaveApprover

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex < 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '        End If


    '        objRpt.SetDataSource(rpt_Data)
    '        Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 21, "Employee : "))
    '        Call ReportFunction.TextChange(objRpt, "txtApproverCode", Language.getMessage(mstrModuleName, 22, "Approver Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 23, "Approver Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 24, "Department"))
    '        Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 25, "Job Title"))
    '        Call ReportFunction.TextChange(objRpt, "txtApproverLevel", Language.getMessage(mstrModuleName, 26, "Approver Level"))

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpListWithApproverReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Private Function Generate_EmpListWithApproverReport(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                                , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            Dim dsCompany As New DataSet
            Dim StrInnerQry, StrConditionQry As String

            StrInnerQry = "" : StrConditionQry = ""

            Dim objLeaveApprover As New clsleaveapprover_master
            dsCompany = objLeaveApprover.GetExternalApproverList(objDataOperation, "List")
            objLeaveApprover = Nothing

            'Pinkal (01-Mar-2016) -- End


            Dim xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)

            objDataOperation.ClearParameters()


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.


            'StrQ = "SELECT ISNULL(hremployee_master.employeecode,'') AS 'EmployeeCode' " & _
            '             ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
            '             ",ISNULL(h1.employeecode,'') AS 'ApproverCode' " & _
            '             ",ISNULL(h1.firstname,'') + ' ' + ISNULL(h1.othername,'') + ' ' + ISNULL(h1.surname,'') AS 'Approver' " & _
            '             ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
            '             ",ISNULL(dept.name,'') AS 'Department' " & _
            '             ",ISNULL(jb.job_name,'') AS 'JobTitle' "


            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If

            'StrQ &= " FROM lvleaveapprover_tran " & _
            '             " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  " & _
            '             " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
            '             " LEFT JOIN hremployee_master h1 ON lvleaveapprover_master.leaveapproverunkid= h1.employeeunkid " & _
            '             " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
            '             " LEFT JOIN " & _
            '                                            " ( " & _
            '                                            "    SELECT " & _
            '                                            "         departmentunkid " & _
            '                                            "        ,employeeunkid " & _
            '                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                                            "    FROM hremployee_transfer_tran " & _
            '                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
            '                                            " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
            '             " LEFT JOIN hrdepartment_master dept ON dept.departmentunkid= Alloc.departmentunkid " & _
            '            " LEFT JOIN " & _
            '             " ( " & _
            '             "         SELECT " & _
            '             "         jobunkid " & _
            '             "        ,employeeunkid " & _
            '             "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '             "    FROM hremployee_categorization_tran " & _
            '             "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
            '             " ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
            '             " LEFT JOIN hrjob_master jb ON jb.jobunkid= Jobs.jobunkid "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            'StrQ &= " WHERE lvleaveapprover_tran.isvoid = 0 "

            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter.Trim
            'End If


            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If


            'Call FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery



            StrInnerQry = "SELECT ISNULL(hremployee_master.employeecode,'') AS 'EmployeeCode' " & _
                         ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS 'Employee' " & _
                      ",#APPR_CODE# AS 'ApproverCode' " & _
                      ",#APPR_NAME# AS 'Approver' " & _
                         ",ISNULL(lvapproverlevel_master.levelname,'') AS 'Level' " & _
                      ",#EMPL_DEPT# AS 'Department' " & _
                      ",#EMPL_JOB# AS 'JobTitle' "


            If mintViewIndex > 0 Then
                StrInnerQry &= mstrAnalysis_Fields
            Else
                StrInnerQry &= ", 0 AS Id, '' AS GName "
            End If

            StrInnerQry &= " FROM lvleaveapprover_tran " & _
                         " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0  " & _
                         " LEFT JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid =  lvapproverlevel_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                         " LEFT JOIN hremployee_master ON lvleaveapprover_tran.employeeunkid= hremployee_master.employeeunkid " & _
                          " #EMP_JOIN# " & _
                         " LEFT JOIN " & _
                                                        " ( " & _
                                                        "    SELECT " & _
                                                        "         departmentunkid " & _
                                                        "        ,employeeunkid " & _
                                                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                        "    FROM #DBName#hremployee_transfer_tran " & _
                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                                                        " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN #DBName#hrdepartment_master dept ON dept.departmentunkid= Alloc.departmentunkid " & _
                        " LEFT JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "         jobunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "    FROM #DBName#hremployee_categorization_tran " & _
                         "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                         " ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                         " LEFT JOIN #DBName#hrjob_master jb ON jb.jobunkid= Jobs.jobunkid "

            StrInnerQry &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrInnerQry &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrInnerQry &= xAdvanceJoinQry
            End If

            StrConditionQry &= " #WHERE_CONDITION# AND lvleaveapprover_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = #ExAppr# " & _
                                         " AND lvleaveapprover_master.isswap = 0 AND lvleaveapprover_master.isactive = 1 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrConditionQry &= " AND " & mstrAdvance_Filter.Trim
            End If


            If xUACFiltrQry.Trim.Length > 0 Then
                StrConditionQry &= " AND " & xUACFiltrQry
            End If


            Call FilterTitleAndFilterQuery()

            StrConditionQry &= Me._FilterQuery

            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#APPR_CODE#", "ISNULL(h1.employeecode,'') ")
            StrQ = StrQ.Replace("#APPR_NAME#", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '') ")
            StrQ = StrQ.Replace("#ExAppr#", "0")
            StrQ = StrQ.Replace("#DBName#", "")
            StrQ = StrQ.Replace("#EMP_JOIN#", "JOIN hremployee_master h1 ON h1.employeeunkid = lvleaveapprover_master.leaveapproverunkid ")
            StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE 1 = 1 ")
            StrQ = StrQ.Replace("#EMPL_DEPT#", " ISNULL(dept.name,'') ")
            StrQ = StrQ.Replace("#EMPL_JOB#", " ISNULL(jb.job_name,'') ")

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                StrQ = StrInnerQry
                'Pinkal (10-Mar-2017) -- Start
                'Enhancement - Working On Leave Module Re-Engineering.
                StrConditionQry = StrConditionQry.Replace("ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.othername, '') + ' ' + ISNULL(h1.surname, '')", "ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.lastname, '')")
                'Pinkal (10-Mar-2017) -- End

                StrQ &= StrConditionQry


                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    StrQ = StrQ.Replace("#APPR_CODE#", " '' ")
                    'Pinkal (10-Mar-2017) -- Start
                    'Enhancement - Working On Leave Module Re-Engineering.
                    'StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
                    'StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid ")

                    StrQ = StrQ.Replace("#APPR_NAME#", " ISNULL(h1.username,'') ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS h1 ON h1.userunkid = lvleaveapprover_master.leaveapproverunkid ")
                    'Pinkal (10-Mar-2017) -- End


                    StrQ = StrQ.Replace("#EMPL_DEPT#", " '' ")
                    StrQ = StrQ.Replace("#EMPL_JOB#", " '' ")

                    'Pinkal (10-Mar-2017) -- Start
                    'Enhancement - Working On Leave Module Re-Engineering.
                    StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE h1.companyunkid = " & dr("companyunkid") & " ")
                    'Pinkal (10-Mar-2017) -- End

                Else
                    StrQ = StrQ.Replace("#APPR_CODE#", " CASE WHEN ISNULL(h1.employeecode, '') = '' THEN '' ELSE ISNULL(h1.employeecode, '') END ")
                    StrQ = StrQ.Replace("#APPR_NAME#", " CASE WHEN ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') END ")
                    StrQ = StrQ.Replace("#EMP_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                                                      " JOIN #DBName#hremployee_master h1 ON UEmp.employeeunkid = h1.employeeunkid ")
                    StrQ = StrQ.Replace("#EMPL_DEPT#", " ISNULL(dept.name,'') ")
                    StrQ = StrQ.Replace("#EMPL_JOB#", " ISNULL(jb.job_name,'') ")

                    'Pinkal (10-Mar-2017) -- Start
                    'Enhancement - Working On Leave Module Re-Engineering.
                    StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE UEmp.companyunkid = " & dr("companyunkid") & " ")
                    'Pinkal (10-Mar-2017) -- End
                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    StrQ = StrQ.Replace("#DBName#", dr("DName") & "..")
                Else
                    StrQ = StrQ.Replace("#DBName#", "")
                End If

                StrQ = StrQ.Replace("#ExAppr#", "1")

                'Pinkal (10-Mar-2017) -- Start
                'Enhancement - Working On Leave Module Re-Engineering.
                'StrQ = StrQ.Replace("#WHERE_CONDITION#", " WHERE UEmp.companyunkid = " & dr("companyunkid") & " ")
                'Pinkal (10-Mar-2017) -- End


                dstemp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If
            Next



            'Pinkal (01-Mar-2016) -- End



            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim mstrLevel As String = String.Empty
            Dim mstrLeavename As String = String.Empty
            Dim mstrApprover As String = String.Empty

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("GName")
                rpt_Rows.Item("Column2") = dtRow.Item("EmployeeCode").ToString() & " : " & dtRow.Item("Employee").ToString()
                rpt_Rows.Item("Column3") = dtRow.Item("ApproverCode").ToString()
                rpt_Rows.Item("Column4") = dtRow.Item("Approver").ToString()
                rpt_Rows.Item("Column5") = dtRow.Item("Department").ToString()
                rpt_Rows.Item("Column6") = dtRow.Item("JobTitle").ToString()
                rpt_Rows.Item("Column7") = dtRow.Item("Level").ToString()
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptEmpListWithLeaveApprover

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If


            objRpt.SetDataSource(rpt_Data)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 21, "Employee : "))
            Call ReportFunction.TextChange(objRpt, "txtApproverCode", Language.getMessage(mstrModuleName, 22, "Approver Code"))
            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 23, "Approver Name"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 24, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 25, "Job Title"))
            Call ReportFunction.TextChange(objRpt, "txtApproverLevel", Language.getMessage(mstrModuleName, 26, "Approver Level"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 12, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmpListWithApproverReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

#End Region

    'Pinkal (25-Apr-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Employee Code")
            Language.setMessage(mstrModuleName, 6, "Employee Name")
            Language.setMessage(mstrModuleName, 9, "Leave Approver")
            Language.setMessage(mstrModuleName, 10, "Approver Level")
            Language.setMessage(mstrModuleName, 11, "Leave Type")
            Language.setMessage(mstrModuleName, 12, "Printed By :")
            Language.setMessage(mstrModuleName, 13, "Printed Date :")
            Language.setMessage(mstrModuleName, 14, "Sub Total :")
            Language.setMessage(mstrModuleName, 15, "Grand Total :")
            Language.setMessage(mstrModuleName, 16, "Leave Approver :")
            Language.setMessage(mstrModuleName, 17, "Approver Level :")
            Language.setMessage(mstrModuleName, 18, "Employee :")
            Language.setMessage(mstrModuleName, 19, "Leave Type :")
            Language.setMessage(mstrModuleName, 20, " Order By :")
            Language.setMessage(mstrModuleName, 21, "Employee :")
            Language.setMessage(mstrModuleName, 22, "Approver Code")
            Language.setMessage(mstrModuleName, 23, "Approver Name")
            Language.setMessage(mstrModuleName, 24, "Department")
            Language.setMessage(mstrModuleName, 25, "Job Title")
            Language.setMessage(mstrModuleName, 26, "Approver Level")
            Language.setMessage(mstrModuleName, 27, "Approver Priority")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

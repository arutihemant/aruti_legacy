'************************************************************************************************************************************
'Class Name :clsHolidayAttendanceReport.vb
'Purpose    :
'Date       :15/11/2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsHolidayAttendanceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsHolidayAttendanceReport"
    Private mstrReportId As String = enArutiReport.Holiday_Attendance_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mstrOrderByQuery As String = ""
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintShiftId As Integer = 0
    Private mstrShiftName As String = String.Empty
    Private mintPolicyId As Integer = 0
    Private mstrPolicyName As String = String.Empty
    Private mblnIncludeDayOff As Boolean = False
    Private mDecWorkFromHrs As Decimal = 0
    Private mDecWorkToHrs As Decimal = 0
    Private mDecOT1FromHrs As Decimal = 0
    Private mDecOT1ToHrs As Decimal = 0
    Private mDecOT2FromHrs As Decimal = 0
    Private mDecOT2ToHrs As Decimal = 0
    Private mDecOT3FromHrs As Decimal = 0
    Private mDecOT3ToHrs As Decimal = 0
    Private mDecOT4FromHrs As Decimal = 0
    Private mDecOT4ToHrs As Decimal = 0
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty


    'Pinkal (13-Dec-2018) -- Start
    'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
    Private mblnPolicyManagement As Boolean = False
    'Pinkal (13-Dec-2018) -- End




#End Region

#Region " Properties "

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _ShiftId() As Integer
        Set(ByVal value As Integer)
            mintShiftId = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _PolicyId() As Integer
        Set(ByVal value As Integer)
            mintPolicyId = value
        End Set
    End Property

    Public WriteOnly Property _PolicyName() As String
        Set(ByVal value As String)
            mstrPolicyName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeDayOff() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeDayOff = value
        End Set
    End Property

    Public WriteOnly Property _WorkFromHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecWorkFromHrs = value
        End Set
    End Property

    Public WriteOnly Property _WorkToHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecWorkToHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT1FromHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT1FromHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT1ToHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT1ToHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT2FromHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT2FromHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT2ToHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT2ToHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT3FromHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT3FromHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT3ToHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT3ToHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT4FromHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT4FromHrs = value
        End Set
    End Property

    Public WriteOnly Property _OT4ToHrs() As Decimal
        Set(ByVal value As Decimal)
            mDecOT4ToHrs = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    'Pinkal (13-Dec-2018) -- Start
    'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
    Public WriteOnly Property _IsPolicManagement() As Boolean
        Set(ByVal value As Boolean)
            mblnPolicyManagement = value
        End Set
    End Property

    'Pinkal (13-Dec-2018) -- End
    
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintShiftId = 0
            mstrShiftName = String.Empty
            mintPolicyId = 0
            mstrPolicyName = String.Empty
            mblnIncludeDayOff = False
            mDecWorkFromHrs = 0
            mDecWorkToHrs = 0
            mDecOT1FromHrs = 0
            mDecOT1ToHrs = 0
            mDecOT2FromHrs = 0
            mDecOT2ToHrs = 0
            mDecOT3FromHrs = 0
            mDecOT3ToHrs = 0
            mDecOT4FromHrs = 0
            mDecOT4ToHrs = 0
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate1))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate2))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "From Date :") & " " & mdtDate1.ToShortDateString & " " & _
                               Language.getMessage(mstrModuleName, 17, "To ") & " " & mdtDate2.ToShortDateString & " "


            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND tnalogin_summary.employeeunkid = @EmpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintPolicyId > 0 Then
                objDataOperation.AddParameter("@PolicyId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyId)
                Me._FilterQuery &= " AND tnalogin_summary.policyunkid = @PolicyId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Policy :") & " " & mstrPolicyName & " "
            End If

            If mintShiftId > 0 Then
                objDataOperation.AddParameter("@ShiftId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId)
                Me._FilterQuery &= " AND tnalogin_summary.shiftunkid  = @ShiftId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Shift :") & " " & mstrShiftName & " "
            End If

            If mblnIncludeDayOff = True Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Day Off Included")
            End If

            'If mDecWorkFromHrs > 0 Then
            '    objDataOperation.AddParameter("@WorkFromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecWorkFromHrs)
            '    Me._FilterQuery &= " AND  >= @WorkFromHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Worked Hours From :") & " " & CDbl(mDecWorkFromHrs) & " "
            'End If

            'If mDecWorkToHrs > 0 Then
            '    objDataOperation.AddParameter("@WorkToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecWorkToHrs)
            '    Me._FilterQuery &= " AND  > 0 AND  <= @WorkToHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Worked Hours To :") & " " & CDbl(mDecWorkToHrs) & " "
            'End If

            'If mDecOT1FromHrs > 0 Then
            '    objDataOperation.AddParameter("@OT1FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT1FromHrs)
            '    Me._FilterQuery &= " AND  >= @OT1FromHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "OT1 From Hours :") & " " & CDbl(mDecOT1FromHrs) & " "
            'End If

            'If mDecOT1ToHrs > 0 Then
            '    objDataOperation.AddParameter("@OT1ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT1ToHrs)
            '    Me._FilterQuery &= " AND  > 0 AND  <= @OT1ToHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "OT1 To Hours :") & " " & CDbl(mDecOT1ToHrs) & " "
            'End If

            'If mDecOT2FromHrs > 0 Then
            '    objDataOperation.AddParameter("@OT2FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT2FromHrs)
            '    Me._FilterQuery &= " AND  >= @OT2FromHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "OT2 From Hours :") & " " & CDbl(mDecOT2FromHrs) & " "
            'End If

            'If mDecOT2ToHrs > 0 Then
            '    objDataOperation.AddParameter("@OT2ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT2ToHrs)
            '    Me._FilterQuery &= " AND  > 0 AND  <= @OT2ToHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "OT2 To Hours :") & " " & CDbl(mDecOT2ToHrs) & " "
            'End If

            'If mDecOT3FromHrs > 0 Then
            '    objDataOperation.AddParameter("@OT3FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT3FromHrs)
            '    Me._FilterQuery &= " AND  >= @OT3FromHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "OT3 From Hours :") & " " & CDbl(mDecOT3FromHrs) & " "
            'End If

            'If mDecOT3ToHrs > 0 Then
            '    objDataOperation.AddParameter("@OT3ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT3ToHrs)
            '    Me._FilterQuery &= " AND  > 0 AND  <= @OT3ToHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "OT3 To Hours :") & " " & CDbl(mDecOT3ToHrs) & " "
            'End If

            'If mDecOT4FromHrs > 0 Then
            '    objDataOperation.AddParameter("@OT4FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT4FromHrs)
            '    Me._FilterQuery &= " AND  >= @OT4FromHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "OT4 From Hours :") & " " & CDbl(mDecOT4FromHrs) & " "
            'End If

            'If mDecOT4ToHrs > 0 Then
            '    objDataOperation.AddParameter("@OT4ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecOT4ToHrs)
            '    Me._FilterQuery &= " AND  > 0 AND  <= @OT4ToHrs "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "OT4 To Hours :") & " " & CDbl(mDecOT4ToHrs) & " "
            'End If

            If Me.OrderByQuery <> "" Then

                'Pinkal (06-Aug-2019) -- Start
                'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "  ORDER BY CONVERT(VARCHAR(8), login_date, 112) , " & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                End If
                'Pinkal (06-Aug-2019) -- End


                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function ConvertTimeInToSecond(ByVal mdecTotalTime As Decimal) As Long
        Dim mintSecond As Long = 0
        Try
            Dim mintHour As Long = 0
            Dim mintMinute As Long = 0
            If mdecTotalTime.ToString.Contains(".") AndAlso mdecTotalTime > 0 Then
                mintHour = mdecTotalTime.ToString.Substring(0, mdecTotalTime.ToString.IndexOf("."))
                mintMinute = mdecTotalTime.ToString.Substring(mdecTotalTime.ToString.IndexOf(".") + 1, mdecTotalTime.ToString.Length - mdecTotalTime.ToString.IndexOf(".") - 1)
                mintSecond = (mintHour * 3600)
                mintSecond += (mintMinute * 60)
            ElseIf mdecTotalTime > 0 Then
                mintMinute = mdecTotalTime.ToString.Substring(0, mdecTotalTime.ToString.Length)
                mintSecond = mintMinute * 3600
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ConvertTimeInToSecond; Module Name: " & mstrModuleName)
        End Try
        Return mintSecond
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 1, "Code")))
            iColumn_DetailReport.Add(New IColumn("employee", Language.getMessage(mstrModuleName, 2, "Employee")))
            iColumn_DetailReport.Add(New IColumn("login_date", Language.getMessage(mstrModuleName, 3, "Date")))
            iColumn_DetailReport.Add(New IColumn("checkintime", Language.getMessage(mstrModuleName, 4, "In Time")))
            iColumn_DetailReport.Add(New IColumn("checkouttime", Language.getMessage(mstrModuleName, 5, "Out Time")))
            iColumn_DetailReport.Add(New IColumn("shiftname", Language.getMessage(mstrModuleName, 6, "Shift")))
            iColumn_DetailReport.Add(New IColumn("policyname", Language.getMessage(mstrModuleName, 7, "Policy")))
            iColumn_DetailReport.Add(New IColumn("workhour", Language.getMessage(mstrModuleName, 8, "Worked Hrs.")))
            iColumn_DetailReport.Add(New IColumn("ot1", Language.getMessage(mstrModuleName, 9, "OT1")))
            iColumn_DetailReport.Add(New IColumn("ot2", Language.getMessage(mstrModuleName, 10, "OT2")))
            iColumn_DetailReport.Add(New IColumn("ot3", Language.getMessage(mstrModuleName, 11, "OT3")))
            iColumn_DetailReport.Add(New IColumn("ot4", Language.getMessage(mstrModuleName, 12, "OT4")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@WK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "WK"))
            objDataOperation.AddParameter("@HL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "HL"))

            If mblnIncludeDayOff Then
                objDataOperation.AddParameter("@dayoff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "OF"))
            End If

            StrQ = " SELECT ROW_NUMBER() OVER (ORDER BY login_date) AS SrNo " & _
                       " , employeeunkid " & _
                       ", employeecode " & _
                       ", employee " & _
                       ", shiftunkid " & _
                       ", shiftcode " & _
                       ", shiftname "

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            If mblnPolicyManagement Then
                StrQ &= ", policycode " & _
                                ", policyname "
            End If
            'Pinkal (13-Dec-2018) -- End


            StrQ &= ", department " & _
                       ", login_date " & _
                       ", checkintime " & _
                       ", checkouttime " & _
                       ", breakhr " & _
                       ", Totalbreakhr " & _
                       ", basehrs " & _
                       ", workhour " & _
                       ", TotalHrs " & _
                       ", ot1 " & _
                       ", TotalOt1 " & _
                       ", ot2 " & _
                       ", TotalOt2 " & _
                       ", ot3 " & _
                       ", TotalOt3 " & _
                       ", ot4 " & _
                       ", TotalOt4 " & _
                       ", CASE WHEN isweekend = 1 THEN  @WK  WHEN isholiday = 1 THEN @HL  "

            If mblnIncludeDayOff Then
                StrQ &= " WHEN isdayoff = 1 THEN @dayoff "
            End If

            StrQ &= " ELSE ''  END	 AS	 AttenCode "

            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            StrQ &= ",Id,GName "
            'Pinkal (06-Aug-2019) -- End

            StrQ &= " FROM    " & _
                       "( SELECT    tnalogin_summary.employeeunkid " & _
                       " , ISNULL(hremployee_master.employeecode, '' ) AS employeecode " & _
                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                         ", Jobs.jobunkid " & _
                         ", ISNULL(tnashift_master.shiftunkid, 0) AS shiftunkid " & _
                         ", ISNULL(tnashift_master.shiftcode, '') AS shiftcode " & _
                         ", ISNULL(tnashift_master.shiftname, '') AS shiftname "

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            If mblnPolicyManagement Then
                StrQ &= ", ISNULL(tnapolicy_master.policycode, '') AS policycode " & _
                                 ", ISNULL(tnapolicy_master.policyname, '') AS policyname "
            End If
            'Pinkal (13-Dec-2018) -- End


            StrQ &= ", ISNULL(dept.name, '') AS department " & _
                         ", CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) AS login_date " & _
                         ", checkintime " & _
                         ", checkouttime " & _
                         ", ISNULL(logintran.breakhr, '') AS breakhr " & _
                         ", ISNULL(SUM(logintran.Totalbreakhr),0) AS Totalbreakhr  " & _
                         ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), tnashift_tran.workinghrs / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( tnashift_tran.workinghrs % 3600 ) / 60), 2), '00:00') AS basehrs " & _
                         ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.total_hrs) / 3600), 2) + ':' + RIGHT('00'  + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_hrs) % 3600 ) / 60), 2), '00:00') AS workhour " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.total_overtime) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.total_overtime) % 3600 ) / 60), 2) AS ot1 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot2) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot2) % 3600 ) / 60), 2) AS ot2 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot3) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot3) % 3600 ) / 60), 2) AS ot3 " & _
                         ", RIGHT('00' + CONVERT(VARCHAR(2), SUM(tnalogin_summary.ot4)  / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( SUM(tnalogin_summary.ot4) % 3600 ) / 60),2) AS ot4 " & _
                         ", SUM(tnalogin_summary.total_hrs) AS TotalHrs " & _
                         ", SUM(tnalogin_summary.total_overtime) AS TotalOt1" & _
                         ", SUM(tnalogin_summary.ot2) AS TotalOt2" & _
                         ", SUM(tnalogin_summary.ot3) AS TotalOt3" & _
                         ", SUM(tnalogin_summary.ot4) AS TotalOt4" & _
                         " , tnalogin_summary.isweekend AS isweekend " & _
                         ", tnalogin_summary.isholiday AS isholiday "

            If mblnIncludeDayOff Then
                StrQ &= ", tnalogin_summary.isdayoff AS isdayoff "
            End If


            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Pinkal (06-Aug-2019) -- End


            StrQ &= " FROM tnalogin_summary " & _
                        " JOIN hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                        " LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    " LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "           departmentunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                         " JOIN tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                         " JOIN tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 "

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            If mblnPolicyManagement Then
                StrQ &= " LEFT JOIN tnapolicy_master ON tnapolicy_master.policyunkid = tnalogin_summary.policyunkid AND tnapolicy_master.isvoid = 0 "
            End If
            'Pinkal (13-Dec-2018) -- End

            StrQ &= " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid,holdunkid " & _
                                                      ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), '') <>  ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '') THEN  " & _
                                                      "            ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '') + ' *'  " & _
                                                      "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.roundoff_intime), 100),8), '') END checkintime  " & _
                                                      ", CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '')  " & _
                                                      "           WHEN ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), '')  <> 	ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '') THEN " & _
                                                      "           ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '') + ' *'" & _
                                                      "  ELSE ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.roundoff_outtime), 100),8), '') END checkouttime  " & _
                                                      ", ISNULL( RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' + RIGHT('00'+ CONVERT(VARCHAR(2), SUM(tnalogin_tran.breakhr)% 3600 / 60), 2), '00:00') AS breakhr " & _
                                                      ", SUM(tnalogin_tran.breakhr) AS Totalbreakhr " & _
                                                      " FROM tnalogin_Tran " & _
                                                      " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN @FromDate and @ToDate AND isvoid = 0 GROUP BY logindate,employeeunkid,holdunkid " & _
                                ") AS logintran " & _
                                              "ON logintran.employeeunkid = tnalogin_summary.employeeunkid " & _
                    "AND CONVERT(VARCHAR(8),logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry & " "
            End If
            StrQ &= " WHERE 1 = 1"



            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If
            'Pinkal (06-Aug-2019) -- End


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            StrQ &= " AND CONVERT(VARCHAR(8),tnalogin_summary.login_date,112) between @FromDate and @ToDate  " & _
                                    " AND (tnalogin_summary.isweekend = 1 OR tnalogin_summary.isholiday = 1 "

            If mblnIncludeDayOff Then
                StrQ &= " OR tnalogin_summary.isdayoff = 1 "
            End If

            StrQ &= " )"

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= "  GROUP BY  CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) " & _
                         "  , ISNULL(hremployee_master.employeecode, '')" & _
                         "  , ISNULL(hremployee_master.firstname, '') " & _
                         "  , ISNULL(hremployee_master.surname, '') " & _
                         "  , ISNULL(tnashift_master.shiftcode, '') " & _
                         "  , ISNULL(tnashift_master.shiftname, '') "

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            If mblnPolicyManagement Then
                StrQ &= " , ISNULL(tnapolicy_master.policycode, '') " & _
                                " , ISNULL(tnapolicy_master.policyname, '')  "
            End If
            'Pinkal (13-Dec-2018) -- End


            StrQ &= "  , tnalogin_summary.employeeunkid " & _
                         "  , dept.departmentunkid " & _
                         "  , dept.name " & _
                         "  , hremployee_master.firstname " & _
                         "  , hremployee_master.surname " & _
                         "  , tnashift_master.shiftunkid " & _
                         "  , tnashift_tran.workinghrs " & _
                         "  , logintran.checkintime " & _
                         "  , logintran.checkouttime " & _
                         "  , logintran.breakhr " & _
                         "  , tnalogin_summary.isweekend " & _
                             "  , tnalogin_summary.isholiday " & _
                             " , Jobs.jobunkid "


            If mblnIncludeDayOff Then
                StrQ &= "  , tnalogin_summary.isdayoff "
            End If


            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            End If
            'Pinkal (06-Aug-2019) -- End


            Dim mstrCondition As String = ""

            If mDecWorkFromHrs > 0 Then
                objDataOperation.AddParameter("@WorkFromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecWorkFromHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.total_hrs) > 0 AND  SUM(tnalogin_summary.total_hrs) >= @WorkFromHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Worked Hours From :") & " " & CDbl(mDecWorkFromHrs) & "  "
            End If

            If mDecWorkToHrs > 0 Then
                objDataOperation.AddParameter("@WorkToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecWorkToHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.total_hrs) > 0 AND  SUM(tnalogin_summary.total_hrs) <= @WorkToHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Worked Hours To :") & " " & CDbl(mDecWorkToHrs) & "  "
            End If

            If mDecOT1FromHrs > 0 Then
                objDataOperation.AddParameter("@OT1FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT1FromHrs))
                Me._FilterQuery &= "AND SUM(tnalogin_summary.total_overtime) > 0 AND SUM(tnalogin_summary.total_overtime) >= @OT1FromHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "OT1 From Hours :") & " " & CDbl(mDecOT1FromHrs) & "  "
            End If

            If mDecOT1ToHrs > 0 Then
                objDataOperation.AddParameter("@OT1ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT1ToHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.total_overtime) > 0 AND SUM(tnalogin_summary.total_overtime)  <= @OT1ToHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 25, "OT1 To Hours :") & " " & CDbl(mDecOT1ToHrs) & "  "
            End If

            If mDecOT2FromHrs > 0 Then
                objDataOperation.AddParameter("@OT2FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT2FromHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot2) > 0 AND SUM(tnalogin_summary.ot2)  >= @OT2FromHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 26, "OT2 From Hours :") & " " & CDbl(mDecOT2FromHrs) & "  "
            End If

            If mDecOT2ToHrs > 0 Then
                objDataOperation.AddParameter("@OT2ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT2ToHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot2) > 0 AND SUM(tnalogin_summary.ot2)  <= @OT2ToHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 27, "OT2 To Hours :") & " " & CDbl(mDecOT2ToHrs) & "  "
            End If

            If mDecOT3FromHrs > 0 Then
                objDataOperation.AddParameter("@OT3FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT3FromHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot3) > 0 AND SUM(tnalogin_summary.ot3)  >= @OT3FromHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 28, "OT3 From Hours :") & " " & CDbl(mDecOT3FromHrs) & "  "
            End If

            If mDecOT3ToHrs > 0 Then
                objDataOperation.AddParameter("@OT3ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT3ToHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot3) > 0 AND SUM(tnalogin_summary.ot3)  <= @OT3ToHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 29, "OT3 To Hours :") & " " & CDbl(mDecOT3ToHrs) & "  "
            End If

            If mDecOT4FromHrs > 0 Then
                objDataOperation.AddParameter("@OT4FromHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT4FromHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot4) > 0 AND SUM(tnalogin_summary.ot4)  >= @OT4FromHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "OT4 From Hours :") & " " & CDbl(mDecOT4FromHrs) & "  "
            End If

            If mDecOT4ToHrs > 0 Then
                objDataOperation.AddParameter("@OT4ToHrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, ConvertTimeInToSecond(mDecOT4ToHrs))
                mstrCondition &= "AND SUM(tnalogin_summary.ot4) > 0 AND SUM(tnalogin_summary.ot4)  <= @OT4ToHrs "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "OT4 To Hours :") & " " & CDbl(mDecOT4ToHrs) & "  "
            End If

            If mstrCondition.Trim.ToString.Length > 0 Then
                StrQ &= "HAVING " & mstrCondition.ToString.Substring(3, mstrCondition.ToString.Length - 3)
            End If

            StrQ &= " ) AS ETS  WHERE 1 = 1 "


            StrQ &= mstrOrderByQuery


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim mintTotWorkhrs As Integer = 0
            Dim mintOT1 As Integer = 0
            Dim mintOT2 As Integer = 0
            Dim mintOT3 As Integer = 0
            Dim mintOT4 As Integer = 0

            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            Dim mintGroupID As Integer = 0
            Dim mintSubOT1 As Decimal = 0
            Dim mintSubOT2 As Decimal = 0
            Dim mintSubOT3 As Decimal = 0
            Dim mintSubOT4 As Decimal = 0
            'Pinkal (06-Aug-2019) -- End



            rpt_Data = New ArutiReport.Designer.dsArutiReport
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("employeecode")
                rpt_Row.Item("Column2") = dtRow.Item("employee")
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("login_date").ToString()).ToShortDateString
                rpt_Row.Item("Column4") = dtRow.Item("shiftname")

                'Pinkal (13-Dec-2018) -- Start
                'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
                If mblnPolicyManagement Then
                    rpt_Row.Item("Column5") = dtRow.Item("policyname")
                Else
                    rpt_Row.Item("Column5") = ""
                End If
                'Pinkal (13-Dec-2018) -- End

                rpt_Row.Item("Column6") = dtRow.Item("AttenCode")
                rpt_Row.Item("Column7") = dtRow.Item("checkintime")
                rpt_Row.Item("Column8") = dtRow.Item("checkouttime")
                rpt_Row.Item("Column9") = dtRow.Item("workhour")
                rpt_Row.Item("Column10") = dtRow.Item("ot1")
                rpt_Row.Item("Column11") = dtRow.Item("ot2")
                rpt_Row.Item("Column12") = dtRow.Item("ot3")
                rpt_Row.Item("Column13") = dtRow.Item("ot4")

                mintTotWorkhrs = CInt(dsList.Tables("DataTable").Compute("SUM(TotalHrs)", "1=1"))
                mintOT1 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot1)", "1=1"))
                mintOT2 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot2)", "1=1"))
                mintOT3 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot3)", "1=1"))
                mintOT4 = CInt(dsList.Tables("DataTable").Compute("SUM(Totalot4)", "1=1"))

                rpt_Row.Item("Column14") = CalculateTime(True, mintTotWorkhrs).ToString("#00.00")
                rpt_Row.Item("Column15") = CalculateTime(True, mintOT1).ToString("#00.00")
                rpt_Row.Item("Column16") = CalculateTime(True, mintOT2).ToString("#00.00")
                rpt_Row.Item("Column17") = CalculateTime(True, mintOT3).ToString("#00.00")
                rpt_Row.Item("Column18") = CalculateTime(True, mintOT4).ToString("#00.00")


                'Pinkal (06-Aug-2019) -- Start
                'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
                rpt_Row.Item("Column19") = dtRow.Item("GName")
                rpt_Row.Item("Column20") = dtRow.Item("Id")


                If mintViewIndex > 0 Then
                    rpt_Row.Item("Column21") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(TotalHrs)", "Id = " & CInt(dtRow.Item("Id")))).ToString("#00.00")
                    rpt_Row.Item("Column22") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(Totalot1)", "Id = " & CInt(dtRow.Item("Id")))).ToString("#00.00")
                    rpt_Row.Item("Column23") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(Totalot2)", "Id = " & CInt(dtRow.Item("Id")))).ToString("#00.00")
                    rpt_Row.Item("Column24") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(Totalot3)", "Id = " & CInt(dtRow.Item("Id")))).ToString("#00.00")
                    rpt_Row.Item("Column25") = CalculateTime(True, dsList.Tables("Datatable").Compute("SUM(Totalot4)", "Id = " & CInt(dtRow.Item("Id")))).ToString("#00.00")
                End If

                'Pinkal (06-Aug-2019) -- End



                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptHolidayAttendanceReport
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 33, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 34, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 35, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 36, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)



            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If
            'Pinkal (06-Aug-2019) -- End


            Call ReportFunction.TextChange(objRpt, "txtCode", Language.getMessage(mstrModuleName, 1, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 2, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtDate", Language.getMessage(mstrModuleName, 3, "Date"))
            Call ReportFunction.TextChange(objRpt, "txtShift", Language.getMessage(mstrModuleName, 6, "Shift"))

            'Pinkal (13-Dec-2018) -- Start
            'Enhancement - Working on Holiday Attendance Report Changed for AFCons.
            If mblnPolicyManagement Then
                Call ReportFunction.TextChange(objRpt, "txtPolicy", Language.getMessage(mstrModuleName, 7, "Policy"))
            Else
                ReportFunction.EnableSuppress(objRpt, "txtPolicy", True)
                ReportFunction.EnableSuppress(objRpt, "Column51", True)
            End If
            'Pinkal (13-Dec-2018) -- End

            Call ReportFunction.TextChange(objRpt, "txtAttCode", Language.getMessage(mstrModuleName, 40, "Att. Code"))
            Call ReportFunction.TextChange(objRpt, "txtInTime", Language.getMessage(mstrModuleName, 4, "In Time"))
            Call ReportFunction.TextChange(objRpt, "txtOutTime", Language.getMessage(mstrModuleName, 5, "Out Time"))
            Call ReportFunction.TextChange(objRpt, "txtHrsWorked", Language.getMessage(mstrModuleName, 8, "Worked Hrs."))
            Call ReportFunction.TextChange(objRpt, "txtOT1", Language.getMessage(mstrModuleName, 9, "OT1"))
            Call ReportFunction.TextChange(objRpt, "txtOT2", Language.getMessage(mstrModuleName, 10, "OT2"))
            Call ReportFunction.TextChange(objRpt, "txtOT3", Language.getMessage(mstrModuleName, 11, "OT3"))
            Call ReportFunction.TextChange(objRpt, "txtOT4", Language.getMessage(mstrModuleName, 12, "OT4"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 37, "Total :"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 38, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 39, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)


            'Pinkal (06-Aug-2019) -- Start
            'Enhancement [004033 - TRZ] - Working on Analysis by function is needed in the Holiday Attendance Report.
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 41, "Sub Total : "))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            'Pinkal (06-Aug-2019) -- End
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally

        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code")
            Language.setMessage(mstrModuleName, 2, "Employee")
            Language.setMessage(mstrModuleName, 3, "Date")
            Language.setMessage(mstrModuleName, 4, "In Time")
            Language.setMessage(mstrModuleName, 5, "Out Time")
            Language.setMessage(mstrModuleName, 6, "Shift")
            Language.setMessage(mstrModuleName, 7, "Policy")
            Language.setMessage(mstrModuleName, 8, "Worked Hrs.")
            Language.setMessage(mstrModuleName, 9, "OT1")
            Language.setMessage(mstrModuleName, 10, "OT2")
            Language.setMessage(mstrModuleName, 11, "OT3")
            Language.setMessage(mstrModuleName, 12, "OT4")
            Language.setMessage(mstrModuleName, 13, "WK")
            Language.setMessage(mstrModuleName, 14, "HL")
            Language.setMessage(mstrModuleName, 15, "OF")
            Language.setMessage(mstrModuleName, 16, "From Date :")
            Language.setMessage(mstrModuleName, 17, "To")
            Language.setMessage(mstrModuleName, 18, "Employee :")
            Language.setMessage(mstrModuleName, 19, "Policy :")
            Language.setMessage(mstrModuleName, 20, "Shift :")
            Language.setMessage(mstrModuleName, 21, "Day Off Included")
            Language.setMessage(mstrModuleName, 22, "Worked Hours From :")
            Language.setMessage(mstrModuleName, 23, "Worked Hours To :")
            Language.setMessage(mstrModuleName, 24, "OT1 From Hours :")
            Language.setMessage(mstrModuleName, 25, "OT1 To Hours :")
            Language.setMessage(mstrModuleName, 26, "OT2 From Hours :")
            Language.setMessage(mstrModuleName, 27, "OT2 To Hours :")
            Language.setMessage(mstrModuleName, 28, "OT3 From Hours :")
            Language.setMessage(mstrModuleName, 29, "OT3 To Hours :")
            Language.setMessage(mstrModuleName, 30, "OT4 From Hours :")
            Language.setMessage(mstrModuleName, 31, "OT4 To Hours :")
            Language.setMessage(mstrModuleName, 32, " Order By :")
            Language.setMessage(mstrModuleName, 33, "Prepared By :")
            Language.setMessage(mstrModuleName, 34, "Checked By :")
            Language.setMessage(mstrModuleName, 35, "Approved By :")
            Language.setMessage(mstrModuleName, 36, "Received By :")
            Language.setMessage(mstrModuleName, 37, "Total :")
            Language.setMessage(mstrModuleName, 38, "Printed By :")
            Language.setMessage(mstrModuleName, 39, "Printed Date :")
            Language.setMessage(mstrModuleName, 40, "Att. Code")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

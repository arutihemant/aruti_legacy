Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports eZee.Common
Imports ArutiReports


Public Class frmOTRequisition_Report

#Region "Private Variables"

    Private mstrModuleName As String = "frmOTRequisitionDetail_Report"
    Dim objOTRequisitionDetail As clsOTRequisition_Report
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing

#End Region

#Region "Constructor"

    Public Sub New()
        objOTRequisitionDetail = New clsOTRequisition_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objOTRequisitionDetail.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date.Date _
                                                               , "List", True, 0, False)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With
            objPeriod = Nothing


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'cboReportType.Items.Clear()
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "OT Requisition Detail Report"))
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 5, "OT Requisition Summary Report"))
            'cboReportType.SelectedIndex = 0
            'Pinkal (15-Jan-2020) -- End

            Dim objEmp As New clsEmployee_Master

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "EmpCodeName"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With
            objEmp = Nothing


            dsList.Clear()
            dsList = Nothing
            Dim objTranHead As New clsTransactionHead
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", True)

            With cboNormalOTTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboHolidayOTTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy()
                .SelectedValue = 0
            End With
            dsList.Clear()
            dsList = Nothing
            objTranHead = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Dim mstrTranHeadIds As String = ""
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.OTRequisition_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrTranHeadIds = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                If mstrTranHeadIds.Trim.Length > 0 Then
                    Dim ar() As String = mstrTranHeadIds.Trim().Split(",")
                    If ar.Length > 0 Then
                        cboNormalOTTranHead.SelectedValue = CInt(ar(0))
                        cboHolidayOTTranHead.SelectedValue = CInt(ar(1))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'cboReportType.SelectedIndex = 0
            'Pinkal (15-Jan-2020) -- End
            cboPeriod.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objOTRequisitionDetail.SetDefaultValue()

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'objOTRequisitionDetail._ReportId = CInt(cboReportType.SelectedIndex)
            'objOTRequisitionDetail._ReportTypeName = cboReportType.Text
            objOTRequisitionDetail._ReportName = Me.Text
            'Pinkal (15-Jan-2020) -- End

            objOTRequisitionDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objOTRequisitionDetail._Period = cboPeriod.Text
            objOTRequisitionDetail._EmployeeID = CInt(cboEmployee.SelectedValue)
            objOTRequisitionDetail._EmployeeName = cboEmployee.Text
            objOTRequisitionDetail._PeriodStartDate = mdtPeriodStartDate
            objOTRequisitionDetail._PeriodEndDate = mdtPeriodEndDate
            objOTRequisitionDetail._NormalOTTranHeadId = CInt(cboNormalOTTranHead.SelectedValue)
            objOTRequisitionDetail._HolidayOTTranHeadId = CInt(cboHolidayOTTranHead.SelectedValue)
            objOTRequisitionDetail._Advance_Filter = mstrAdvanceFilter
            objOTRequisitionDetail._ViewByIds = mstrViewByIds
            objOTRequisitionDetail._ViewIndex = mintViewIndex
            objOTRequisitionDetail._ViewByName = mstrViewByName
            objOTRequisitionDetail._Analysis_Fields = mstrAnalysis_Fields
            objOTRequisitionDetail._Analysis_Join = mstrAnalysis_Join
            objOTRequisitionDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objOTRequisitionDetail._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objOTRequisitionDetail._Report_GroupName = mstrReport_GroupName
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmOTRequisition_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objOTRequisitionDetail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTRequisition_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOTRequisition_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objOTRequisitionDetail._ReportName
            eZeeHeader.Message = objOTRequisitionDetail._ReportDesc
            Call FillCombo()
            Call ResetValue()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTRequisition_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOTRequisition_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    'Call frmLvForm_ApprovalStatus_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTRequisition_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOTRequisition_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTRequisition_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsOTRequisition_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsOTRequisition_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub

            ElseIf CInt(cboNormalOTTranHead.SelectedValue) <= 0 AndAlso CInt(cboHolidayOTTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Transaction Head mapping is compulsory information.Please map any one transaction head."), enMsgBoxStyle.Information)
                cboNormalOTTranHead.Select()
                Exit Sub
            End If

            objOTRequisitionDetail.Generate_OTRequisitionDetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                         , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                         , True, True, True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            objUserDefRMode._Reportunkid = enArutiReport.OTRequisition_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = IIf(CInt(cboNormalOTTranHead.SelectedValue) > 0, CInt(cboNormalOTTranHead.SelectedValue), 0).ToString() & "," & IIf(CInt(cboHolidayOTTranHead.SelectedValue) > 0, CInt(cboHolidayOTTranHead.SelectedValue), 0).ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.OTRequisition_Report, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            If mblnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Selection Saved Successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(objUserDefRMode._Message)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSettings_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboPeriod
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNormalOTTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNormalOTTranHead.Click, objbtnSearchHolidayOTTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            Dim cbo As ComboBox = Nothing

            If CType(sender, eZeeGradientButton).Name = objbtnSearchNormalOTTranHead.Name Then
                cbo = cboNormalOTTranHead
            ElseIf CType(sender, eZeeGradientButton).Name = objbtnSearchHolidayOTTranHead.Name Then
                cbo = cboHolidayOTTranHead
            End If

            With cbo
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNormalOTTranHead_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrViewByIds = frm._ReportBy_Ids
            mstrViewByName = frm._ReportBy_Name
            mintViewIndex = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = frm._Analysis_OrderBy_GName
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"


    'Pinkal (15-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion Reports for NMB.
    'Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
    '    Try
    '        If CInt(cboReportType.SelectedIndex) = 0 Then    'Detail Report
    '            GetValue()
    '            cboNormalOTTranHead.Enabled = True
    '            objbtnSearchNormalOTTranHead.Enabled = True
    '            objbtnSearchHolidayOTTranHead.Enabled = True
    '            cboHolidayOTTranHead.Enabled = True
    '            btnSaveSettings.Enabled = True

    '        ElseIf CInt(cboReportType.SelectedIndex) = 1 Then   'Summary Report
    '            objbtnSearchNormalOTTranHead.Enabled = False
    '            objbtnSearchHolidayOTTranHead.Enabled = False
    '            cboNormalOTTranHead.SelectedValue = 0
    '            cboNormalOTTranHead.Enabled = False
    '            cboHolidayOTTranHead.SelectedValue = 0
    '            cboHolidayOTTranHead.Enabled = False
    '            btnSaveSettings.Enabled = False
    '        End If
    '        cboPeriod.SelectedIndex = 0
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Pinkal (15-Jan-2020) -- End

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                LblPeriodValue.Text = objPeriod._TnA_StartDate.Date & " - " & objPeriod._TnA_EndDate.Date
                mdtPeriodStartDate = objPeriod._TnA_StartDate.Date
                mdtPeriodEndDate = objPeriod._TnA_EndDate.Date
            Else
                LblPeriodValue.Text = ""
                mdtPeriodStartDate = Nothing
                mdtPeriodEndDate = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveSettings.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveSettings.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.Name, Me.LblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
            Me.LblHolidayOT.Text = Language._Object.getCaption(Me.LblHolidayOT.Name, Me.LblHolidayOT.Text)
            Me.LblNormalOT.Text = Language._Object.getCaption(Me.LblNormalOT.Name, Me.LblNormalOT.Text)
            Me.btnSaveSettings.Text = Language._Object.getCaption(Me.btnSaveSettings.Name, Me.btnSaveSettings.Text)
            Me.LblPeriodValue.Text = Language._Object.getCaption(Me.LblPeriodValue.Name, Me.LblPeriodValue.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Transaction Head mapping is compulsory information.Please map any one transaction head.")
            Language.setMessage(mstrModuleName, 3, "Selection Saved Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

Imports Aruti.Data
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports

'Last Message index= 1

Public Class frmEmpOTRequisition_Report
    Private mstrModuleName As String = "frmEmpOTRequisition_Report"
    Private objEmpOTRequisitionRpt As clsEmpOTRequisition_Report

#Region "Constructor"

    Public Sub New()
        objEmpOTRequisitionRpt = New clsEmpOTRequisition_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpOTRequisitionRpt.SetDefaultValue()
        InitializeComponent()
        _Show_AdvanceFilter = True
    End Sub

#End Region

#Region "Private Variables"

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", "", False, False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid < 4", "", DataViewRowState.CurrentRows).ToTable()
            With cboStatus
                .ValueMember = "statusunkid"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 0
            End With
            objStatus = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            chkShowEachEmpOnNewPage.Checked = True
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAdvanceFilter = ""
            objEmpOTRequisitionRpt.setDefaultOrderBy(0)
            txtOrderBy.Text = objEmpOTRequisitionRpt.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpOTRequisitionRpt.SetDefaultValue()

            objEmpOTRequisitionRpt._EmpId = CInt(cboEmployee.SelectedValue)
            objEmpOTRequisitionRpt._EmpName = cboEmployee.Text
            objEmpOTRequisitionRpt._StatusId = CInt(cboStatus.SelectedValue)
            objEmpOTRequisitionRpt._Status = cboStatus.Text
            objEmpOTRequisitionRpt._FromDate = dtpFromDate.Value.Date
            objEmpOTRequisitionRpt._ToDate = dtpToDate.Value.Date
            objEmpOTRequisitionRpt._ViewByIds = mstrStringIds
            objEmpOTRequisitionRpt._ViewIndex = mintViewIdx
            objEmpOTRequisitionRpt._ViewByName = mstrStringName
            objEmpOTRequisitionRpt._Analysis_Fields = mstrAnalysis_Fields
            objEmpOTRequisitionRpt._Analysis_Join = mstrAnalysis_Join
            objEmpOTRequisitionRpt._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpOTRequisitionRpt._Report_GroupName = mstrReport_GroupName
            objEmpOTRequisitionRpt._AdvanceFilter = mstrAdvanceFilter
            objEmpOTRequisitionRpt._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Events"

    Private Sub frmEmpOTRequisition_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpOTRequisitionRpt = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_FormClosed", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpOTRequisition_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            Me._Title = objEmpOTRequisitionRpt._ReportName
            Me._Message = objEmpOTRequisitionRpt._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpOTRequisition_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmpOTRequisition_Report_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpOTRequisition_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_KeyPress", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Buttons"

    Private Sub frmEmpOTRequisition_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            objEmpOTRequisitionRpt.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   dtpFromDate.Value.Date, _
                                                   dtpToDate.Value.Date, _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, e.Type, Aruti.Data.enExportAction.None)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_Report_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpOTRequisition_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            objEmpOTRequisitionRpt.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   dtpFromDate.Value.Date, _
                                                   dtpToDate.Value.Date, _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                                                   ConfigParameter._Object._ExportReportPath, _
                                                   ConfigParameter._Object._OpenAfterExport, _
                                                   0, enPrintAction.None, e.Type)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpOTRequisition_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_Reset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmpOTRequisition_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpOTRequisition_Report_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub frmEmpOTRequisition_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmpOTRequisition_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsEmpOTRequisition_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmpOTRequisition_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchStatus.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboStatus.DataSource
            frm.ValueMember = cboStatus.ValueMember
            frm.DisplayMember = cboStatus.DisplayMember
            If frm.DisplayDialog Then
                cboStatus.SelectedValue = frm.SelectedValue
                cboStatus.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEmpOTRequisitionRpt.setOrderBy(0)
            txtOrderBy.Text = objEmpOTRequisitionRpt.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.LblStatus.Text = Language._Object.getCaption(Me.LblStatus.Name, Me.LblStatus.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.Name, Me.chkShowEachEmpOnNewPage.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

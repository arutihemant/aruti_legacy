﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeDurationReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeDurationReport))
        Me.objLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblRType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.objPanel2 = New System.Windows.Forms.Panel
        Me.chkCurrentAllocation = New System.Windows.Forms.CheckBox
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvAllocation = New System.Windows.Forms.ListView
        Me.colhAllocations = New System.Windows.Forms.ColumnHeader
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocations = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objPanel1 = New System.Windows.Forms.Panel
        Me.chkFromCurrDate = New System.Windows.Forms.CheckBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.LblJob = New System.Windows.Forms.Label
        Me.lblToYear = New System.Windows.Forms.Label
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbOptionalFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDToYear = New eZee.TextBox.IntegerTextBox
        Me.txtDFromYear = New eZee.TextBox.IntegerTextBox
        Me.lblDTo = New System.Windows.Forms.Label
        Me.lblDFrom = New System.Windows.Forms.Label
        Me.txtMToYear = New eZee.TextBox.IntegerTextBox
        Me.txtMFromYear = New eZee.TextBox.IntegerTextBox
        Me.lblMTo = New System.Windows.Forms.Label
        Me.lblMFrom = New System.Windows.Forms.Label
        Me.txtYToYear = New eZee.TextBox.IntegerTextBox
        Me.txtYFromYear = New eZee.TextBox.IntegerTextBox
        Me.lblYTo = New System.Windows.Forms.Label
        Me.lblYFrom = New System.Windows.Forms.Label
        Me.chkDays = New System.Windows.Forms.CheckBox
        Me.chkMonth = New System.Windows.Forms.CheckBox
        Me.chkYear = New System.Windows.Forms.CheckBox
        Me.objfpnlCtrl = New System.Windows.Forms.FlowLayoutPanel
        Me.dtpAsOnDate = New System.Windows.Forms.DateTimePicker
        Me.lblAsOnDate = New System.Windows.Forms.Label
        Me.gbMandatoryInfo.SuspendLayout()
        Me.objPanel2.SuspendLayout()
        Me.objPanel1.SuspendLayout()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbSortBy.SuspendLayout()
        Me.gbOptionalFilter.SuspendLayout()
        Me.objfpnlCtrl.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 492)
        Me.NavPanel.Size = New System.Drawing.Size(764, 55)
        '
        'objLanguage
        '
        Me.objLanguage.BackColor = System.Drawing.Color.White
        Me.objLanguage.BackgroundImage = CType(resources.GetObject("objLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objLanguage.FlatAppearance.BorderSize = 0
        Me.objLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLanguage.ForeColor = System.Drawing.Color.Black
        Me.objLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objLanguage.Location = New System.Drawing.Point(12, 12)
        Me.objLanguage.Name = "objLanguage"
        Me.objLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objLanguage.TabIndex = 3
        Me.objLanguage.UseVisualStyleBackColor = True
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.lblRType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.objPanel2)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.objPanel1)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.LblEmployee)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployee)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(417, 292)
        Me.gbMandatoryInfo.TabIndex = 17
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Filter Criteria"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRType
        '
        Me.lblRType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRType.Location = New System.Drawing.Point(7, 35)
        Me.lblRType.Name = "lblRType"
        Me.lblRType.Size = New System.Drawing.Size(79, 15)
        Me.lblRType.TabIndex = 95
        Me.lblRType.Text = "Report Type"
        Me.lblRType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 145
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(93, 32)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(290, 21)
        Me.cboReportType.TabIndex = 94
        '
        'objPanel2
        '
        Me.objPanel2.Controls.Add(Me.chkCurrentAllocation)
        Me.objPanel2.Controls.Add(Me.txtSearch)
        Me.objPanel2.Controls.Add(Me.objchkAll)
        Me.objPanel2.Controls.Add(Me.lvAllocation)
        Me.objPanel2.Controls.Add(Me.lblAllocation)
        Me.objPanel2.Controls.Add(Me.cboAllocations)
        Me.objPanel2.Location = New System.Drawing.Point(3, 191)
        Me.objPanel2.Name = "objPanel2"
        Me.objPanel2.Size = New System.Drawing.Size(411, 241)
        Me.objPanel2.TabIndex = 93
        '
        'chkCurrentAllocation
        '
        Me.chkCurrentAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCurrentAllocation.Location = New System.Drawing.Point(90, 30)
        Me.chkCurrentAllocation.Name = "chkCurrentAllocation"
        Me.chkCurrentAllocation.Size = New System.Drawing.Size(291, 16)
        Me.chkCurrentAllocation.TabIndex = 92
        Me.chkCurrentAllocation.Text = "Generate Based On Current Allocation"
        Me.chkCurrentAllocation.UseVisualStyleBackColor = True
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(90, 53)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(291, 21)
        Me.txtSearch.TabIndex = 65
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(357, 86)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 64
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvAllocation
        '
        Me.lvAllocation.CheckBoxes = True
        Me.lvAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhAllocations})
        Me.lvAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvAllocation.FullRowSelect = True
        Me.lvAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAllocation.HideSelection = False
        Me.lvAllocation.Location = New System.Drawing.Point(90, 80)
        Me.lvAllocation.Name = "lvAllocation"
        Me.lvAllocation.Size = New System.Drawing.Size(291, 155)
        Me.lvAllocation.TabIndex = 63
        Me.lvAllocation.UseCompatibleStateImageBehavior = False
        Me.lvAllocation.View = System.Windows.Forms.View.Details
        '
        'colhAllocations
        '
        Me.colhAllocations.Tag = "colhAllocations"
        Me.colhAllocations.Text = ""
        Me.colhAllocations.Width = 285
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(5, 6)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(79, 15)
        Me.lblAllocation.TabIndex = 62
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocations
        '
        Me.cboAllocations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocations.DropDownWidth = 145
        Me.cboAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocations.FormattingEnabled = True
        Me.cboAllocations.Location = New System.Drawing.Point(90, 3)
        Me.cboAllocations.Name = "cboAllocations"
        Me.cboAllocations.Size = New System.Drawing.Size(291, 21)
        Me.cboAllocations.TabIndex = 61
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(318, 5)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 19
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objPanel1
        '
        Me.objPanel1.Controls.Add(Me.dtpAsOnDate)
        Me.objPanel1.Controls.Add(Me.lblAsOnDate)
        Me.objPanel1.Controls.Add(Me.chkFromCurrDate)
        Me.objPanel1.Controls.Add(Me.cboJob)
        Me.objPanel1.Controls.Add(Me.chkInactiveemp)
        Me.objPanel1.Controls.Add(Me.objbtnSearchJob)
        Me.objPanel1.Controls.Add(Me.lblFromYear)
        Me.objPanel1.Controls.Add(Me.LblJob)
        Me.objPanel1.Controls.Add(Me.lblToYear)
        Me.objPanel1.Controls.Add(Me.nudFromYear)
        Me.objPanel1.Controls.Add(Me.nudToYear)
        Me.objPanel1.Location = New System.Drawing.Point(3, 82)
        Me.objPanel1.Name = "objPanel1"
        Me.objPanel1.Size = New System.Drawing.Size(411, 105)
        Me.objPanel1.TabIndex = 92
        '
        'chkFromCurrDate
        '
        Me.chkFromCurrDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFromCurrDate.Location = New System.Drawing.Point(90, 57)
        Me.chkFromCurrDate.Name = "chkFromCurrDate"
        Me.chkFromCurrDate.Size = New System.Drawing.Size(291, 16)
        Me.chkFromCurrDate.TabIndex = 91
        Me.chkFromCurrDate.Text = "Generate from Current Title"
        Me.chkFromCurrDate.UseVisualStyleBackColor = True
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 145
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(90, 3)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(291, 21)
        Me.cboJob.TabIndex = 88
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(245, 32)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(162, 16)
        Me.chkInactiveemp.TabIndex = 81
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(387, 3)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 89
        '
        'lblFromYear
        '
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(5, 33)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(79, 15)
        Me.lblFromYear.TabIndex = 83
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblJob
        '
        Me.LblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJob.Location = New System.Drawing.Point(5, 5)
        Me.LblJob.Name = "LblJob"
        Me.LblJob.Size = New System.Drawing.Size(79, 15)
        Me.LblJob.TabIndex = 90
        Me.LblJob.Text = "Job"
        Me.LblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblToYear
        '
        Me.lblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToYear.Location = New System.Drawing.Point(142, 33)
        Me.lblToYear.Name = "lblToYear"
        Me.lblToYear.Size = New System.Drawing.Size(45, 15)
        Me.lblToYear.TabIndex = 84
        Me.lblToYear.Text = "To Year"
        Me.lblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(90, 30)
        Me.nudFromYear.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(46, 21)
        Me.nudFromYear.TabIndex = 85
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(193, 30)
        Me.nudToYear.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(46, 21)
        Me.nudToYear.TabIndex = 86
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(390, 59)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 60
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(8, 62)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.LblEmployee.TabIndex = 60
        Me.LblEmployee.Text = "Employee"
        Me.LblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 145
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(93, 59)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(291, 21)
        Me.cboEmployee.TabIndex = 59
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(3, 423)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(417, 63)
        Me.gbSortBy.TabIndex = 18
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(389, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(78, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(90, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(293, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbOptionalFilter
        '
        Me.gbOptionalFilter.BorderColor = System.Drawing.Color.Black
        Me.gbOptionalFilter.Checked = False
        Me.gbOptionalFilter.CollapseAllExceptThis = False
        Me.gbOptionalFilter.CollapsedHoverImage = Nothing
        Me.gbOptionalFilter.CollapsedNormalImage = Nothing
        Me.gbOptionalFilter.CollapsedPressedImage = Nothing
        Me.gbOptionalFilter.CollapseOnLoad = False
        Me.gbOptionalFilter.Controls.Add(Me.txtDToYear)
        Me.gbOptionalFilter.Controls.Add(Me.txtDFromYear)
        Me.gbOptionalFilter.Controls.Add(Me.lblDTo)
        Me.gbOptionalFilter.Controls.Add(Me.lblDFrom)
        Me.gbOptionalFilter.Controls.Add(Me.txtMToYear)
        Me.gbOptionalFilter.Controls.Add(Me.txtMFromYear)
        Me.gbOptionalFilter.Controls.Add(Me.lblMTo)
        Me.gbOptionalFilter.Controls.Add(Me.lblMFrom)
        Me.gbOptionalFilter.Controls.Add(Me.txtYToYear)
        Me.gbOptionalFilter.Controls.Add(Me.txtYFromYear)
        Me.gbOptionalFilter.Controls.Add(Me.lblYTo)
        Me.gbOptionalFilter.Controls.Add(Me.lblYFrom)
        Me.gbOptionalFilter.Controls.Add(Me.chkDays)
        Me.gbOptionalFilter.Controls.Add(Me.chkMonth)
        Me.gbOptionalFilter.Controls.Add(Me.chkYear)
        Me.gbOptionalFilter.ExpandedHoverImage = Nothing
        Me.gbOptionalFilter.ExpandedNormalImage = Nothing
        Me.gbOptionalFilter.ExpandedPressedImage = Nothing
        Me.gbOptionalFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOptionalFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOptionalFilter.HeaderHeight = 25
        Me.gbOptionalFilter.HeaderMessage = ""
        Me.gbOptionalFilter.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOptionalFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOptionalFilter.HeightOnCollapse = 0
        Me.gbOptionalFilter.LeftTextSpace = 0
        Me.gbOptionalFilter.Location = New System.Drawing.Point(3, 301)
        Me.gbOptionalFilter.Name = "gbOptionalFilter"
        Me.gbOptionalFilter.OpenHeight = 300
        Me.gbOptionalFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOptionalFilter.ShowBorder = True
        Me.gbOptionalFilter.ShowCheckBox = False
        Me.gbOptionalFilter.ShowCollapseButton = False
        Me.gbOptionalFilter.ShowDefaultBorderColor = True
        Me.gbOptionalFilter.ShowDownButton = False
        Me.gbOptionalFilter.ShowHeader = True
        Me.gbOptionalFilter.Size = New System.Drawing.Size(417, 116)
        Me.gbOptionalFilter.TabIndex = 19
        Me.gbOptionalFilter.Temp = 0
        Me.gbOptionalFilter.Text = "Optional Filter"
        Me.gbOptionalFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDToYear
        '
        Me.txtDToYear.AllowNegative = False
        Me.txtDToYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDToYear.DigitsInGroup = 0
        Me.txtDToYear.Flags = 65536
        Me.txtDToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDToYear.Location = New System.Drawing.Point(253, 86)
        Me.txtDToYear.MaxDecimalPlaces = 0
        Me.txtDToYear.MaxWholeDigits = 9
        Me.txtDToYear.Name = "txtDToYear"
        Me.txtDToYear.Prefix = ""
        Me.txtDToYear.RangeMax = 2147483647
        Me.txtDToYear.RangeMin = -2147483648
        Me.txtDToYear.Size = New System.Drawing.Size(57, 21)
        Me.txtDToYear.TabIndex = 17
        Me.txtDToYear.Text = "0"
        Me.txtDToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDFromYear
        '
        Me.txtDFromYear.AllowNegative = False
        Me.txtDFromYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDFromYear.DigitsInGroup = 0
        Me.txtDFromYear.Flags = 65536
        Me.txtDFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDFromYear.Location = New System.Drawing.Point(150, 86)
        Me.txtDFromYear.MaxDecimalPlaces = 0
        Me.txtDFromYear.MaxWholeDigits = 9
        Me.txtDFromYear.Name = "txtDFromYear"
        Me.txtDFromYear.Prefix = ""
        Me.txtDFromYear.RangeMax = 2147483647
        Me.txtDFromYear.RangeMin = -2147483648
        Me.txtDFromYear.Size = New System.Drawing.Size(57, 21)
        Me.txtDFromYear.TabIndex = 16
        Me.txtDFromYear.Text = "0"
        Me.txtDFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDTo
        '
        Me.lblDTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDTo.Location = New System.Drawing.Point(215, 89)
        Me.lblDTo.Name = "lblDTo"
        Me.lblDTo.Size = New System.Drawing.Size(32, 15)
        Me.lblDTo.TabIndex = 14
        Me.lblDTo.Text = "To"
        Me.lblDTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDFrom
        '
        Me.lblDFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDFrom.Location = New System.Drawing.Point(96, 89)
        Me.lblDFrom.Name = "lblDFrom"
        Me.lblDFrom.Size = New System.Drawing.Size(48, 15)
        Me.lblDFrom.TabIndex = 15
        Me.lblDFrom.Text = "From"
        Me.lblDFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMToYear
        '
        Me.txtMToYear.AllowNegative = False
        Me.txtMToYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMToYear.DigitsInGroup = 0
        Me.txtMToYear.Flags = 65536
        Me.txtMToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMToYear.Location = New System.Drawing.Point(253, 59)
        Me.txtMToYear.MaxDecimalPlaces = 0
        Me.txtMToYear.MaxWholeDigits = 9
        Me.txtMToYear.Name = "txtMToYear"
        Me.txtMToYear.Prefix = ""
        Me.txtMToYear.RangeMax = 2147483647
        Me.txtMToYear.RangeMin = -2147483648
        Me.txtMToYear.Size = New System.Drawing.Size(57, 21)
        Me.txtMToYear.TabIndex = 13
        Me.txtMToYear.Text = "0"
        Me.txtMToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMFromYear
        '
        Me.txtMFromYear.AllowNegative = False
        Me.txtMFromYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMFromYear.DigitsInGroup = 0
        Me.txtMFromYear.Flags = 65536
        Me.txtMFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMFromYear.Location = New System.Drawing.Point(150, 59)
        Me.txtMFromYear.MaxDecimalPlaces = 0
        Me.txtMFromYear.MaxWholeDigits = 9
        Me.txtMFromYear.Name = "txtMFromYear"
        Me.txtMFromYear.Prefix = ""
        Me.txtMFromYear.RangeMax = 2147483647
        Me.txtMFromYear.RangeMin = -2147483648
        Me.txtMFromYear.Size = New System.Drawing.Size(57, 21)
        Me.txtMFromYear.TabIndex = 12
        Me.txtMFromYear.Text = "0"
        Me.txtMFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMTo
        '
        Me.lblMTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMTo.Location = New System.Drawing.Point(215, 62)
        Me.lblMTo.Name = "lblMTo"
        Me.lblMTo.Size = New System.Drawing.Size(32, 15)
        Me.lblMTo.TabIndex = 10
        Me.lblMTo.Text = "To"
        Me.lblMTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMFrom
        '
        Me.lblMFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMFrom.Location = New System.Drawing.Point(96, 62)
        Me.lblMFrom.Name = "lblMFrom"
        Me.lblMFrom.Size = New System.Drawing.Size(48, 15)
        Me.lblMFrom.TabIndex = 11
        Me.lblMFrom.Text = "From"
        Me.lblMFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtYToYear
        '
        Me.txtYToYear.AllowNegative = False
        Me.txtYToYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtYToYear.DigitsInGroup = 0
        Me.txtYToYear.Flags = 65536
        Me.txtYToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYToYear.Location = New System.Drawing.Point(253, 32)
        Me.txtYToYear.MaxDecimalPlaces = 0
        Me.txtYToYear.MaxWholeDigits = 9
        Me.txtYToYear.Name = "txtYToYear"
        Me.txtYToYear.Prefix = ""
        Me.txtYToYear.RangeMax = 2147483647
        Me.txtYToYear.RangeMin = -2147483648
        Me.txtYToYear.Size = New System.Drawing.Size(57, 21)
        Me.txtYToYear.TabIndex = 9
        Me.txtYToYear.Text = "0"
        Me.txtYToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtYFromYear
        '
        Me.txtYFromYear.AllowNegative = False
        Me.txtYFromYear.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtYFromYear.DigitsInGroup = 0
        Me.txtYFromYear.Flags = 65536
        Me.txtYFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYFromYear.Location = New System.Drawing.Point(150, 32)
        Me.txtYFromYear.MaxDecimalPlaces = 0
        Me.txtYFromYear.MaxWholeDigits = 9
        Me.txtYFromYear.Name = "txtYFromYear"
        Me.txtYFromYear.Prefix = ""
        Me.txtYFromYear.RangeMax = 2147483647
        Me.txtYFromYear.RangeMin = -2147483648
        Me.txtYFromYear.Size = New System.Drawing.Size(57, 21)
        Me.txtYFromYear.TabIndex = 8
        Me.txtYFromYear.Text = "0"
        Me.txtYFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblYTo
        '
        Me.lblYTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYTo.Location = New System.Drawing.Point(215, 35)
        Me.lblYTo.Name = "lblYTo"
        Me.lblYTo.Size = New System.Drawing.Size(32, 15)
        Me.lblYTo.TabIndex = 7
        Me.lblYTo.Text = "To"
        Me.lblYTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblYFrom
        '
        Me.lblYFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYFrom.Location = New System.Drawing.Point(96, 35)
        Me.lblYFrom.Name = "lblYFrom"
        Me.lblYFrom.Size = New System.Drawing.Size(48, 15)
        Me.lblYFrom.TabIndex = 7
        Me.lblYFrom.Text = "From"
        Me.lblYFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDays
        '
        Me.chkDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDays.Location = New System.Drawing.Point(10, 89)
        Me.chkDays.Name = "chkDays"
        Me.chkDays.Size = New System.Drawing.Size(77, 16)
        Me.chkDays.TabIndex = 6
        Me.chkDays.Text = "Days"
        Me.chkDays.UseVisualStyleBackColor = True
        '
        'chkMonth
        '
        Me.chkMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMonth.Location = New System.Drawing.Point(10, 62)
        Me.chkMonth.Name = "chkMonth"
        Me.chkMonth.Size = New System.Drawing.Size(77, 15)
        Me.chkMonth.TabIndex = 5
        Me.chkMonth.Text = "Months"
        Me.chkMonth.UseVisualStyleBackColor = True
        '
        'chkYear
        '
        Me.chkYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkYear.Location = New System.Drawing.Point(10, 35)
        Me.chkYear.Name = "chkYear"
        Me.chkYear.Size = New System.Drawing.Size(77, 15)
        Me.chkYear.TabIndex = 4
        Me.chkYear.Text = "Year"
        Me.chkYear.UseVisualStyleBackColor = True
        '
        'objfpnlCtrl
        '
        Me.objfpnlCtrl.AutoSize = True
        Me.objfpnlCtrl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.objfpnlCtrl.Controls.Add(Me.gbMandatoryInfo)
        Me.objfpnlCtrl.Controls.Add(Me.gbOptionalFilter)
        Me.objfpnlCtrl.Controls.Add(Me.gbSortBy)
        Me.objfpnlCtrl.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.objfpnlCtrl.Location = New System.Drawing.Point(12, 66)
        Me.objfpnlCtrl.Name = "objfpnlCtrl"
        Me.objfpnlCtrl.Size = New System.Drawing.Size(423, 489)
        Me.objfpnlCtrl.TabIndex = 20
        '
        'dtpAsOnDate
        '
        Me.dtpAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAsOnDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAsOnDate.Location = New System.Drawing.Point(90, 79)
        Me.dtpAsOnDate.Name = "dtpAsOnDate"
        Me.dtpAsOnDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpAsOnDate.TabIndex = 93
        '
        'lblAsOnDate
        '
        Me.lblAsOnDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsOnDate.Location = New System.Drawing.Point(5, 82)
        Me.lblAsOnDate.Name = "lblAsOnDate"
        Me.lblAsOnDate.Size = New System.Drawing.Size(79, 15)
        Me.lblAsOnDate.TabIndex = 92
        Me.lblAsOnDate.Text = "As On Date"
        Me.lblAsOnDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEmployeeDurationReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 547)
        Me.Controls.Add(Me.objfpnlCtrl)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeDurationReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.objfpnlCtrl, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.objPanel2.ResumeLayout(False)
        Me.objPanel2.PerformLayout()
        Me.objPanel1.ResumeLayout(False)
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbOptionalFilter.ResumeLayout(False)
        Me.gbOptionalFilter.PerformLayout()
        Me.objfpnlCtrl.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblToYear As System.Windows.Forms.Label
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents LblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents objLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objPanel1 As System.Windows.Forms.Panel
    Friend WithEvents objPanel2 As System.Windows.Forms.Panel
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents cboAllocations As System.Windows.Forms.ComboBox
    Friend WithEvents lblRType As System.Windows.Forms.Label
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocation As System.Windows.Forms.ListView
    Friend WithEvents colhAllocations As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbOptionalFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkDays As System.Windows.Forms.CheckBox
    Friend WithEvents chkMonth As System.Windows.Forms.CheckBox
    Friend WithEvents chkYear As System.Windows.Forms.CheckBox
    Friend WithEvents lblYTo As System.Windows.Forms.Label
    Friend WithEvents lblYFrom As System.Windows.Forms.Label
    Friend WithEvents txtDToYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtDFromYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblDTo As System.Windows.Forms.Label
    Friend WithEvents lblDFrom As System.Windows.Forms.Label
    Friend WithEvents txtMToYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtMFromYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents lblMTo As System.Windows.Forms.Label
    Friend WithEvents lblMFrom As System.Windows.Forms.Label
    Friend WithEvents txtYToYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents txtYFromYear As eZee.TextBox.IntegerTextBox
    Friend WithEvents objfpnlCtrl As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents chkFromCurrDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkCurrentAllocation As System.Windows.Forms.CheckBox
    Friend WithEvents dtpAsOnDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAsOnDate As System.Windows.Forms.Label
End Class

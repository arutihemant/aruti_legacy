﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeCVReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkDisplayTrainingInfo = New System.Windows.Forms.CheckBox
        Me.chkDisplayQualificationInfo = New System.Windows.Forms.CheckBox
        Me.chkDisplayHistroyInfo = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objchkDisplayDependantsInfo = New System.Windows.Forms.CheckBox
        Me.objchkDisplayMembershipInfo = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 498)
        Me.NavPanel.Size = New System.Drawing.Size(778, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayTrainingInfo)
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayQualificationInfo)
        Me.gbFilterCriteria.Controls.Add(Me.chkDisplayHistroyInfo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(442, 130)
        Me.gbFilterCriteria.TabIndex = 16
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDisplayTrainingInfo
        '
        Me.chkDisplayTrainingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayTrainingInfo.Location = New System.Drawing.Point(90, 106)
        Me.chkDisplayTrainingInfo.Name = "chkDisplayTrainingInfo"
        Me.chkDisplayTrainingInfo.Size = New System.Drawing.Size(308, 17)
        Me.chkDisplayTrainingInfo.TabIndex = 61
        Me.chkDisplayTrainingInfo.Text = "Display Training Information"
        Me.chkDisplayTrainingInfo.UseVisualStyleBackColor = True
        '
        'chkDisplayQualificationInfo
        '
        Me.chkDisplayQualificationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayQualificationInfo.Location = New System.Drawing.Point(90, 83)
        Me.chkDisplayQualificationInfo.Name = "chkDisplayQualificationInfo"
        Me.chkDisplayQualificationInfo.Size = New System.Drawing.Size(308, 17)
        Me.chkDisplayQualificationInfo.TabIndex = 61
        Me.chkDisplayQualificationInfo.Text = "Display Qualification Information"
        Me.chkDisplayQualificationInfo.UseVisualStyleBackColor = True
        '
        'chkDisplayHistroyInfo
        '
        Me.chkDisplayHistroyInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayHistroyInfo.Location = New System.Drawing.Point(90, 60)
        Me.chkDisplayHistroyInfo.Name = "chkDisplayHistroyInfo"
        Me.chkDisplayHistroyInfo.Size = New System.Drawing.Size(308, 17)
        Me.chkDisplayHistroyInfo.TabIndex = 61
        Me.chkDisplayHistroyInfo.Text = "Display Employee Histroy Information"
        Me.chkDisplayHistroyInfo.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(404, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 59
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(90, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(308, 21)
        Me.cboEmployee.TabIndex = 58
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(78, 15)
        Me.lblEmployee.TabIndex = 57
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkDisplayDependantsInfo
        '
        Me.objchkDisplayDependantsInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkDisplayDependantsInfo.Location = New System.Drawing.Point(12, 274)
        Me.objchkDisplayDependantsInfo.Name = "objchkDisplayDependantsInfo"
        Me.objchkDisplayDependantsInfo.Size = New System.Drawing.Size(308, 17)
        Me.objchkDisplayDependantsInfo.TabIndex = 61
        Me.objchkDisplayDependantsInfo.Text = "Display Dependants Information"
        Me.objchkDisplayDependantsInfo.UseVisualStyleBackColor = True
        Me.objchkDisplayDependantsInfo.Visible = False
        '
        'objchkDisplayMembershipInfo
        '
        Me.objchkDisplayMembershipInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkDisplayMembershipInfo.Location = New System.Drawing.Point(12, 251)
        Me.objchkDisplayMembershipInfo.Name = "objchkDisplayMembershipInfo"
        Me.objchkDisplayMembershipInfo.Size = New System.Drawing.Size(308, 17)
        Me.objchkDisplayMembershipInfo.TabIndex = 61
        Me.objchkDisplayMembershipInfo.Text = "Display Membership Data"
        Me.objchkDisplayMembershipInfo.UseVisualStyleBackColor = True
        Me.objchkDisplayMembershipInfo.Visible = False
        '
        'frmEmployeeCVReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 553)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.objchkDisplayDependantsInfo)
        Me.Controls.Add(Me.objchkDisplayMembershipInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmEmployeeCVReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee CV Report"
        Me.Controls.SetChildIndex(Me.objchkDisplayMembershipInfo, 0)
        Me.Controls.SetChildIndex(Me.objchkDisplayDependantsInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objchkDisplayMembershipInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkDisplayTrainingInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkDisplayQualificationInfo As System.Windows.Forms.CheckBox
    Friend WithEvents objchkDisplayDependantsInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkDisplayHistroyInfo As System.Windows.Forms.CheckBox
End Class

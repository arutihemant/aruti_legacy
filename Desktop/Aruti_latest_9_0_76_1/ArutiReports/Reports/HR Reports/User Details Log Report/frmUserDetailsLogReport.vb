#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmUserDetailsLogReport


#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmUserDetailsLogReport"
    Private objclsUserDetailLog As clsUserDetailsLogReport
    Private blnMultiCompnay As Boolean = False
#End Region

#Region " Contructor "

    Public Sub New()
        objclsUserDetailLog = New clsUserDetailsLogReport
        objclsUserDetailLog.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Page Event"

    Private Sub frmUserDetailsLogReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserDetailsLogReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserDetailsLogReport.SetMessages()
            objfrm._Other_ModuleNames = "clsUserDetailsLogReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim objCompany As New clsCompany_Master
            dsList = objCompany.GetList("Company", True)
            If dsList.Tables(0).Rows.Count > 1 Then blnMultiCompnay = True
            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("companyunkid") = 0
            drRow("code") = Language.getMessage(mstrModuleName, 1, "Select")
            drRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Company")
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 2, "User Log Details"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "User Ability Level"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            If blnMultiCompnay = False Then
                cboCompany.SelectedValue = AppSettings._Object._LastCompanyId
            Else
                cboCompany.SelectedValue = 0
            End If
            cboReportType.SelectedIndex = 0
            dtpFromdate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objclsUserDetailLog.SetDefaultValue()
            If CInt(cboCompany.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Company is mandatory information. Please select Company to continue."), enMsgBoxStyle.Information)
                cboCompany.Focus()
                Return False
            End If
            objclsUserDetailLog._mintCompanyId = cboCompany.SelectedValue
            objclsUserDetailLog._mstrCompanyName = cboCompany.Text
            objclsUserDetailLog._mintReportTypeId = cboReportType.SelectedIndex
            objclsUserDetailLog._mstrReportTypeName = cboReportType.Text
            objclsUserDetailLog._mdtdate1 = dtpFromdate.Value.Date
            objclsUserDetailLog._mdtdate2 = dtpToDate.Value.Date
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Button Event"

    Private Sub objbtnSearchCompany_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCompany.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboCompany.DataSource
            frm.ValueMember = cboCompany.ValueMember
            frm.DisplayMember = cboCompany.DisplayMember
            If frm.DisplayDialog Then
                cboCompany.SelectedValue = frm.SelectedValue
                cboCompany.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCompany_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Select Case cboReportType.SelectedIndex
                Case 0  'User Details
                    Call objclsUserDetailLog.Generate_User_Details()
                Case 1  'User Ability Level
                    Call objclsUserDetailLog.Generate_User_Ability()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Control Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                dtpToDate.Enabled = False
            Else
                dtpToDate.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.ehUserDetailLogReport.GradientColor1 = GUI._HeaderBackColor1 
			Me.ehUserDetailLogReport.GradientColor2 = GUI._HeaderBackColor2 
			Me.ehUserDetailLogReport.BorderColor = GUI._HeaderBorderColor 
			Me.ehUserDetailLogReport.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.ehUserDetailLogReport.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.ehUserDetailLogReport.Title = Language._Object.getCaption(Me.ehUserDetailLogReport.Name & "_Title" , Me.ehUserDetailLogReport.Title)
			Me.ehUserDetailLogReport.Message = Language._Object.getCaption(Me.ehUserDetailLogReport.Name & "_Message" , Me.ehUserDetailLogReport.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "User Log Details")
			Language.setMessage(mstrModuleName, 3, "User Ability Level")
			Language.setMessage(mstrModuleName, 4, "Sorry, Company is mandatory information. Please select Company to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

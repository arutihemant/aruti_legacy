#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMonthlyPayrollReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMonthlyPayrollReport"
    Private objMPayrollReport As clsMonthlyPayrollReport
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrSavedHeadTypeIds As String = String.Empty
    Private mstrSavedLeaveTypeIds As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        objMPayrollReport = New clsMonthlyPayrollReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMPayrollReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objMembership As New clsmembership_master
        Dim objCMaster As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                          True, True, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objMembership.getListForCombo("List", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TERMINATION, True, "List")
            With cboExitReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
            With cboCatReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))                                '0'
                .Items.Add(Language.getMessage(mstrModuleName, 2, "New/Re-hired Staff"))                    '1'
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Exit Staff"))                            '2'
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Staff On Leave"))                        '3'
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Staff Promotion & Recategorization"))    '4'
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objperiod = Nothing : objMembership = Nothing
            objCMaster = Nothing : objMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub FillHeadList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            lvCustomTranHead.BeginUpdate()
            Dim lvItem As ListViewItem
            lvCustomTranHead.Items.Clear()
            ConfigParameter._Object.Refresh()
            mstrSavedHeadTypeIds = ""
            Select Case cboReportType.SelectedIndex
                Case 1
                    mstrSavedHeadTypeIds = ConfigParameter._Object._CheckedMonthlyPayrollReport1HeadIds
                Case 2
                    mstrSavedHeadTypeIds = ConfigParameter._Object._CheckedMonthlyPayrollReport2HeadIds
                Case 3
                    mstrSavedHeadTypeIds = ConfigParameter._Object._CheckedMonthlyPayrollReport3HeadIds
                Case 4
                    mstrSavedHeadTypeIds = ConfigParameter._Object._CheckedMonthlyPayrollReport4HeadIds
            End Select
            Dim lvArray As New List(Of ListViewItem)
            dsList = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, enTranHeadType.EarningForEmployees)
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = CInt(dsRow.Item("tranheadunkid"))
                lvItem.SubItems.Add(dsRow.Item("name").ToString)
                If mstrSavedHeadTypeIds.Trim.Length > 0 Then
                    If Array.IndexOf(mstrSavedHeadTypeIds.Split(","), dsRow.Item("tranheadunkid").ToString) <> -1 Then
                        lvItem.Checked = True
                    End If
                End If
                lvArray.Add(lvItem)
            Next
            lvCustomTranHead.Items.AddRange(lvArray.ToArray)
            lvCustomTranHead.EndUpdate()

            If lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            End If

            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHeadList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillLeaveList()
        Try
            RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            RemoveHandler objchkLCheckAll.CheckedChanged, AddressOf objchkLCheckAll_CheckedChanged
            Dim objLeaveType As New clsleavetype_master
            lvLeaveType.Items.Clear()

            ConfigParameter._Object.Refresh()
            mstrSavedLeaveTypeIds = "" : mstrSavedLeaveTypeIds = ConfigParameter._Object._CheckedMonthlyPayrollReportLeaveIds

            Dim dsList As DataSet = objLeaveType.getListForCombo("Leave")
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dsList.Tables("Leave").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = CInt(dtRow.Item("leavetypeunkid").ToString)
                If mstrSavedLeaveTypeIds.Trim.Length > 0 Then
                    If Array.IndexOf(mstrSavedLeaveTypeIds.Split(","), dtRow.Item("leavetypeunkid").ToString) <> -1 Then
                        lvItem.Checked = True
                    End If
                End If
                lvLeaveType.Items.Add(lvItem)
            Next
            lvLeaveType.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)

            If lvLeaveType.CheckedItems.Count <= 0 Then
                objchkLCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvLeaveType.CheckedItems.Count < lvLeaveType.Items.Count Then
                objchkLCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvLeaveType.CheckedItems.Count = lvLeaveType.Items.Count Then
                objchkLCheckAll.CheckState = CheckState.Checked
            End If

            AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            AddHandler objchkLCheckAll.CheckedChanged, AddressOf objchkLCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLeaveList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub RaiseCommonSearch(ByVal ebtn As eZee.Common.eZeeGradientButton, ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                If cbo.DataSource IsNot Nothing Then
                    .DataSource = cbo.DataSource
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    Select Case ebtn.Name.ToString.ToUpper
                        Case objbtnSearchEmployee.Name.ToUpper
                            .CodeMember = "employeecode"
                    End Select
                End If
                If .DisplayDialog Then
                    cbo.SelectedValue = .SelectedValue
                    cbo.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RaiseCommonSearch", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If cboReportType.SelectedIndex <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Please select at-least one report type to continue."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Please select period to export report."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            objMPayrollReport._ReportTypeId = cboReportType.SelectedIndex
            objMPayrollReport._ReportTypeName = cboReportType.Text
            objMPayrollReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objMPayrollReport._PeriodName = cboPeriod.Text
            Dim oPrd As New clscommom_period_Tran
            oPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objMPayrollReport._StartDate = oPrd._Start_Date
            objMPayrollReport._EndDate = oPrd._End_Date
            oPrd = Nothing
            objMPayrollReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objMPayrollReport._EmployeeName = cboEmployee.Text
            objMPayrollReport._MembershipId = CInt(cboMembership.SelectedValue)
            objMPayrollReport._MembershipName = cboMembership.Text
            objMPayrollReport._ExitReasonId = CInt(cboExitReason.SelectedValue)
            objMPayrollReport._ExitReasonName = cboExitReason.Text
            objMPayrollReport._CheckedLeaveTypeIds = lvLeaveType.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(y) y.SubItems(colhLName.Index).Text)
            objMPayrollReport._CheckedTransHeadIds = lvCustomTranHead.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(y) y.SubItems(colhCName.Index).Text)
            objMPayrollReport._ViewByIds = mstrStringIds
            objMPayrollReport._ViewIndex = mintViewIdx
            objMPayrollReport._ViewByName = mstrStringName
            objMPayrollReport._Analysis_Fields = mstrAnalysis_Fields
            objMPayrollReport._Analysis_Join = mstrAnalysis_Join
            objMPayrollReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMPayrollReport._Report_GroupName = mstrReport_GroupName
            objMPayrollReport._AdvanceFilter = mstrAdvanceFilter
            objMPayrollReport._SkipAbsentFromTnA = chkSkipAbsentTnA.Checked
            objMPayrollReport._IncludeLeavePendingApprovedForms = chkIncludePendingApprovedforms.Checked
            objMPayrollReport._IncludeSystemRetirementMonthlyPayrollReport = chkincludesystemretirement.Checked
            objMPayrollReport._CategorizationReasonId = cboCatReason.SelectedValue
            objMPayrollReport._CategorizationReasonName = cboCatReason.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboExitReason.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            objchkCheckAll.Checked = False
            objchkLCheckAll.Checked = False
            mstrAdvanceFilter = ""
            chkSkipAbsentTnA.Checked = ConfigParameter._Object._SkipAbsentFromTnAMonthlyPayrollReport
            chkIncludePendingApprovedforms.Checked = ConfigParameter._Object._IncludePendingApproverFormsInMonthlyPayrollReport
            chkincludesystemretirement.Checked = ConfigParameter._Object._IncludeSystemRetirementMonthlyPayrollReport
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmMonthlyPayrollReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMPayrollReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyPayrollReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMonthlyPayrollReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Call FillCombo()
            Call FillHeadList()
            Call FillLeaveList()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMonthlyPayrollReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objMPayrollReport.Export_MonthlyPayroll_Report(FinancialYear._Object._DatabaseName, _
                                                           User._Object._Userunkid, _
                                                           FinancialYear._Object._YearUnkid, _
                                                           Company._Object._Companyunkid, _
                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                           ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._OpenAfterExport, _
                                                           ConfigParameter._Object._ExportReportPath, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
            Call cboReportType_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchExitReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchExitReason.Click
        Try
            Call RaiseCommonSearch(objbtnSearchExitReason, cboExitReason)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchExitReason_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Try
            Call RaiseCommonSearch(objbtnSearchMembership, cboMembership)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Call RaiseCommonSearch(objbtnSearchEmployee, cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCatReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCatReason.Click
        Try
            Call RaiseCommonSearch(objbtnSearchCatReason, cboCatReason)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCatReason_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMonthlyPayrollReport.SetMessages()
            objfrm._Other_ModuleNames = "clsMonthlyPayrollReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Listview Event(s) "

    Private Sub lvLeaveType_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLeaveType.ItemChecked
        Try
            RemoveHandler objchkLCheckAll.CheckedChanged, AddressOf objchkLCheckAll_CheckedChanged
            If lvLeaveType.CheckedItems.Count = lvLeaveType.Items.Count Then
                objchkLCheckAll.CheckState = CheckState.Checked
            ElseIf lvLeaveType.CheckedItems.Count < lvLeaveType.Items.Count Then
                objchkLCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvLeaveType.CheckedItems.Count <= 0 Then
                objchkLCheckAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objchkLCheckAll.CheckedChanged, AddressOf objchkLCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveType_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvCustomTranHead_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvCustomTranHead.ItemChecked
        Try
            RemoveHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
            If lvCustomTranHead.CheckedItems.Count = lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Checked
            ElseIf lvCustomTranHead.CheckedItems.Count < lvCustomTranHead.Items.Count Then
                objchkCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvCustomTranHead.CheckedItems.Count <= 0 Then
                objchkCheckAll.CheckState = CheckState.Unchecked
            End If
            AddHandler objchkCheckAll.CheckedChanged, AddressOf objchkCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvCustomTranHead_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchCHeads_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchCHeads.TextChanged
        Try
            If lvCustomTranHead.Items.Count <= 0 Then Exit Sub
            lvCustomTranHead.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvCustomTranHead.FindItemWithText(txtSearchCHeads.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvCustomTranHead.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchCHeads_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtLeaveSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLeaveSearch.TextChanged
        Try
            If lvLeaveType.Items.Count <= 0 Then Exit Sub
            lvLeaveType.SelectedIndices.Clear()
            Dim lvFoundItem As ListViewItem = lvLeaveType.FindItemWithText(txtLeaveSearch.Text, True, 0, True)
            If lvFoundItem IsNot Nothing Then
                lvLeaveType.TopItem = lvFoundItem
                lvFoundItem.Selected = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLeaveSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSaveLeaveSelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveLeaveSelection.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            Dim xCheckedItems As String = String.Empty
            If lvLeaveType.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", (From p In lvLeaveType.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
                objConfig._CheckedMonthlyPayrollReportLeaveIds = xCheckedItems
            End If
            objConfig._SkipAbsentFromTnAMonthlyPayrollReport = chkSkipAbsentTnA.Checked
            objConfig._IncludePendingApproverFormsInMonthlyPayrollReport = chkIncludePendingApprovedforms.Checked
            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Settings saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveLeaveSelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSaveHeadSelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveHeadSelection.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            Dim xCheckedItems As String = String.Empty
            If lvCustomTranHead.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", (From p In lvCustomTranHead.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
                Select Case cboReportType.SelectedIndex
                    Case 1
                        objConfig._CheckedMonthlyPayrollReport1HeadIds = xCheckedItems
                    Case 2
                        objConfig._CheckedMonthlyPayrollReport2HeadIds = xCheckedItems
                    Case 3
                        objConfig._CheckedMonthlyPayrollReport3HeadIds = xCheckedItems
                    Case 4
                        objConfig._CheckedMonthlyPayrollReport4HeadIds = xCheckedItems
                End Select
            End If
            objConfig._IncludeSystemRetirementMonthlyPayrollReport = chkincludesystemretirement.Checked
            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Settings saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveHeadSelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheckAll.CheckedChanged
        Try
            RemoveHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
            For Each item As ListViewItem In lvCustomTranHead.Items
                item.Checked = objchkCheckAll.Checked
            Next
            AddHandler lvCustomTranHead.ItemChecked, AddressOf lvCustomTranHead_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheckAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkLCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkLCheckAll.CheckedChanged
        Try
            RemoveHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
            For Each item As ListViewItem In lvLeaveType.Items
                item.Checked = objchkLCheckAll.Checked
            Next
            AddHandler lvLeaveType.ItemChecked, AddressOf lvLeaveType_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkLCheckAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SELECT
                    Call ResetValue()
                    pnlCutomHeads.Enabled = False : pnlLeaveTypes.Enabled = False : pnlOtherFilter.Enabled = False

                Case 1  'NEW STAFF
                    pnlCutomHeads.Enabled = True : pnlLeaveTypes.Enabled = False : pnlOtherFilter.Enabled = True
                    cboMembership.Enabled = True : objbtnSearchMembership.Enabled = True
                    cboExitReason.Enabled = False : objbtnSearchExitReason.Enabled = False
                    cboCatReason.Enabled = False : objbtnSearchCatReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = False
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = False

                Case 2  'EXIT STAFF
                    pnlCutomHeads.Enabled = True : pnlLeaveTypes.Enabled = False : pnlOtherFilter.Enabled = True
                    cboMembership.Enabled = False : objbtnSearchMembership.Enabled = False
                    cboExitReason.Enabled = True : objbtnSearchExitReason.Enabled = True
                    cboCatReason.Enabled = False : objbtnSearchCatReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = False
                    chkincludesystemretirement.Checked = ConfigParameter._Object._IncludeSystemRetirementMonthlyPayrollReport : chkincludesystemretirement.Enabled = True
                    chkSkipAbsentTnA.Checked = False

                Case 3  'STAFF ON LEAVE
                    pnlCutomHeads.Enabled = True : pnlLeaveTypes.Enabled = True : pnlOtherFilter.Enabled = True
                    cboMembership.Enabled = False : objbtnSearchMembership.Enabled = False
                    cboExitReason.Enabled = False : objbtnSearchExitReason.Enabled = False
                    cboCatReason.Enabled = False : objbtnSearchCatReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = ConfigParameter._Object._IncludePendingApproverFormsInMonthlyPayrollReport
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = ConfigParameter._Object._SkipAbsentFromTnAMonthlyPayrollReport

                Case 4  'STAFF PROMOTION & RECATEGORIZATION
                    pnlCutomHeads.Enabled = True : pnlLeaveTypes.Enabled = False : pnlOtherFilter.Enabled = True
                    cboMembership.Enabled = False : objbtnSearchMembership.Enabled = False
                    cboExitReason.Enabled = False : objbtnSearchExitReason.Enabled = False
                    cboCatReason.Enabled = True : objbtnSearchCatReason.Enabled = True

                    chkIncludePendingApprovedforms.Checked = False
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = False

            End Select
            Call FillHeadList() : If pnlLeaveTypes.Enabled = False Then objchkLCheckAll.Checked = False
            If pnlLeaveTypes.Enabled = True Then Call FillLeaveList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblStaffExitReason.Text = Language._Object.getCaption(Me.lblStaffExitReason.Name, Me.lblStaffExitReason.Text)
            Me.colhCName.Text = Language._Object.getCaption(CStr(Me.colhCName.Tag), Me.colhCName.Text)
            Me.colhCHeadTypeID.Text = Language._Object.getCaption(CStr(Me.colhCHeadTypeID.Tag), Me.colhCHeadTypeID.Text)
            Me.lnkSaveHeadSelection.Text = Language._Object.getCaption(Me.lnkSaveHeadSelection.Name, Me.lnkSaveHeadSelection.Text)
            Me.lnkSaveLeaveSelection.Text = Language._Object.getCaption(Me.lnkSaveLeaveSelection.Name, Me.lnkSaveLeaveSelection.Text)
            Me.colhLName.Text = Language._Object.getCaption(CStr(Me.colhLName.Tag), Me.colhLName.Text)
            Me.chkSkipAbsentTnA.Text = Language._Object.getCaption(Me.chkSkipAbsentTnA.Name, Me.chkSkipAbsentTnA.Text)
            Me.chkIncludePendingApprovedforms.Text = Language._Object.getCaption(Me.chkIncludePendingApprovedforms.Name, Me.chkIncludePendingApprovedforms.Text)
            Me.chkincludesystemretirement.Text = Language._Object.getCaption(Me.chkincludesystemretirement.Name, Me.chkincludesystemretirement.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "New/Re-hired Staff")
            Language.setMessage(mstrModuleName, 3, "Exit Staff")
            Language.setMessage(mstrModuleName, 4, "Staff On Leave")
            Language.setMessage(mstrModuleName, 5, "Staff Promotion & Recategorization")
            Language.setMessage(mstrModuleName, 6, "Settings saved successfully.")
            Language.setMessage(mstrModuleName, 7, "Sorry, Please select at-least one report type to continue.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Please select period to export report.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class

﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports eZeeCommonLib.eZeeDataType
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsFamilyCompetenciesReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsFamilyCompetenciesReport"
    Private mstrReportId As String = enArutiReport.Job_Family_Competencies_Report


#Region " Properties Value "

    Private mintAllocRefId As Integer = 0
    Private mstrAllocRefName As String = ""
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mintCompetenceGroupId As Integer = 0
    Private mstrCompetenceGroup As String = ""
    Private mstrCompetenciesIds As String = ""
    Private mstrCompetenciesNames As String = ""
    Private mstrAllocUnkids As String = ""
    Private mstrAllocName As String = ""

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Properties "

    Public WriteOnly Property _AllocRefId() As Integer
        Set(ByVal value As Integer)
            mintAllocRefId = value
        End Set
    End Property

    Public WriteOnly Property _AllocRefName() As String
        Set(ByVal value As String)
            mstrAllocRefName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroupId() As Integer
        Set(ByVal value As Integer)
            mintCompetenceGroupId = value
        End Set
    End Property

    Public WriteOnly Property _CompetenceGroup() As String
        Set(ByVal value As String)
            mstrCompetenceGroup = value
        End Set
    End Property

    Public WriteOnly Property _CompetenciesIds() As String
        Set(ByVal value As String)
            mstrCompetenciesIds = value
        End Set
    End Property

    Public WriteOnly Property _CompetenciesNames() As String
        Set(ByVal value As String)
            mstrCompetenciesNames = value
        End Set
    End Property

    Public WriteOnly Property _AllocUnkids() As String
        Set(ByVal value As String)
            mstrAllocUnkids = value
        End Set
    End Property

    Public WriteOnly Property _AllocName() As String
        Set(ByVal value As String)
            mstrAllocName = value
        End Set
    End Property


#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintAllocRefId = 0
            mstrAllocRefName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mintCompetenceGroupId = 0
            mstrCompetenceGroup = ""
            mstrCompetenciesIds = ""
            mstrCompetenciesNames = ""
            mstrAllocUnkids = ""
            mstrAllocName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Sub Export_FamilyGroupCompetenciesReport(ByVal strDatabaseName As String, _
                                                    ByVal intUserUnkid As Integer, _
                                                    ByVal intYearUnkid As Integer, _
                                                    ByVal intCompanyUnkid As Integer, _
                                                    ByVal strExportPath As String, _
                                                    ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            StrQ = "IF OBJECT_ID('tempdb..#result') IS NOT NULL " & _
                    "DROP TABLE #result " & _
                    "SELECT DISTINCT " & _
                          "CM.name AS cmpt " & _
                         ",CG.name AS cmcg " & _
                         ",CM.competenciesunkid " & _
                         ",CM.competence_categoryunkid " & _
                         ",AGT.allocationunkid " & _
                         ",CASE WHEN AGM.referenceunkid = 1 THEN hrstation_master.name " & _
                              "WHEN AGM.referenceunkid = 2 THEN hrdepartment_group_master.name " & _
                              "WHEN AGM.referenceunkid = 3 THEN hrdepartment_master.name " & _
                              "WHEN AGM.referenceunkid = 4 THEN hrsectiongroup_master.name " & _
                              "WHEN AGM.referenceunkid = 5 THEN hrsection_master.name " & _
                              "WHEN AGM.referenceunkid = 6 THEN hrunitgroup_master.name " & _
                              "WHEN AGM.referenceunkid = 7 THEN hrunit_master.name " & _
                              "WHEN AGM.referenceunkid = 8 THEN hrteam_master.name " & _
                              "WHEN AGM.referenceunkid = 9 THEN hrjobgroup_master.name " & _
                              "WHEN AGM.referenceunkid = 10 THEN hrjob_master.job_name " & _
                              "WHEN AGM.referenceunkid = 13 THEN hrclassgroup_master.name " & _
                              "WHEN AGM.referenceunkid = 14 THEN hrclasses_master.name " & _
                              "WHEN AGM.referenceunkid = 15 THEN prcostcenter_master.costcentername " & _
                              "WHEN AGM.referenceunkid = 16 THEN hrgrade_master.name END AS Family " & _
                         "INTO #result " & _
                         "FROM hrassess_competence_assign_master AS CAM " & _
                              "JOIN hrassess_group_master AS AGM ON CAM.assessgroupunkid = AGM.assessgroupunkid " & _
                              "JOIN hrassess_group_tran AS AGT ON AGM.assessgroupunkid = AGT.assessgroupunkid " & _
                              "JOIN hrassess_competence_assign_tran AS CAT ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                              "JOIN hrassess_competencies_master AS CM ON CM.competenciesunkid = CAT.competenciesunkid " & _
                              "LEFT JOIN cfcommon_master AS CG ON CG.masterunkid = CM.competence_categoryunkid " & _
                              "LEFT JOIN hrstation_master ON stationunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrdepartment_group_master ON deptgroupunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrdepartment_master ON departmentunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrsectiongroup_master ON sectiongroupunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrsection_master ON sectionunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrunitgroup_master ON unitgroupunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrunit_master ON unitunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrteam_master ON teamunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrjobgroup_master ON jobgroupunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrjob_master ON jobunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrclassgroup_master ON classgroupunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrclasses_master ON classesunkid = AGT.allocationunkid " & _
                              "LEFT JOIN prcostcenter_master ON costcenterunkid = AGT.allocationunkid " & _
                              "LEFT JOIN hrgrade_master ON gradeunkid = AGT.allocationunkid " & _
                    "WHERE CAM.isvoid = 0 AND AGM.isactive = 1 AND AGT.isactive = 1 AND CAT.isvoid = 0 " & _
                         "AND CAM.periodunkid = @periodunkid AND AGM.referenceunkid = @referenceunkid "
            If mintCompetenceGroupId > 0 Then
                StrQ &= " AND CM.competence_categoryunkid = @competence_categoryunkid "
            End If
            If mstrCompetenciesIds.Trim.Length > 0 Then
                StrQ &= " AND CAT.competenciesunkid IN (" & mstrCompetenciesIds & ") "
            End If

            StrQ &= "SELECT " & _
                          "B.Family " & _
                         ",B.cmcg " & _
                         ",STUFF((SELECT '#10;' + cmpt " & _
                                   "FROM #result AS A " & _
                                   "WHERE A.allocationunkid = B.allocationunkid AND A.competence_categoryunkid = B.competence_categoryunkid " & _
                                   "FOR XML PATH ('')),1,4,'') AS cmpts " & _
                    "FROM #result AS B " & _
                    "GROUP BY " & _
                     "B.family " & _
                    ",B.competence_categoryunkid " & _
                    ",B.allocationunkid " & _
                    ",B.cmcg " & _
                    "ORDER BY  B.family " & _
                    "DROP TABLE #result "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceGroupId)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocRefId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList.Tables(0).Columns("Family").Caption = mstrAllocRefName & " " & Language.getMessage(mstrModuleName, 100, "Family")
            dsList.Tables(0).Columns("cmcg").Caption = Language.getMessage(mstrModuleName, 101, "Competence Group")
            dsList.Tables(0).Columns("cmpts").Caption = Language.getMessage(mstrModuleName, 102, "Competence")

            Dim strarrGroupColumns As String() = Nothing
            Dim strGrpCols As String() = {"Family"}
            strarrGroupColumns = strGrpCols

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables(0).Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 1
                        intArrayColumnWidth(i) = 200
                    Case 2
                        intArrayColumnWidth(i) = 400
                    Case Else
                        intArrayColumnWidth(i) = 150
                End Select
            Next

            Dim strFilter As String = ""

            If mintPeriodId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 200, "Period") & " : " & mstrPeriodName & " "
            End If

            If mintCompetenceGroupId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 101, "Competence Group") & " : " & mstrCompetenceGroup & " "
            End If

            If mintAllocRefId > 0 Then
                strFilter &= "," & Language.getMessage(mstrModuleName, 202, "Reference") & " : " & mstrAllocRefName & " "
            End If

            strFilter = Mid(strFilter, 2)

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables(0), intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", strFilter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_FamilyGroupCompetenciesReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Family")
            Language.setMessage(mstrModuleName, 101, "Competence Group")
            Language.setMessage(mstrModuleName, 102, "Competence")
            Language.setMessage(mstrModuleName, 200, "Period")
            Language.setMessage(mstrModuleName, 202, "Reference")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'************************************************************************************************************************************
'Class Name : frmAppraisalSummReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAppraisalSummReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAppraisalSummReport"
    Private objAppraisalReport As clsAppraisalSummReport

#End Region

#Region " Contructor "

    Public Sub New()
        objAppraisalReport = New clsAppraisalSummReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objAppraisalReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objDept As New clsDepartment
            Dim dsList As DataSet = objDept.getComboList("Department", True)
            cboDepartment.DisplayMember = "name"
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DataSource = dsList.Tables("Department")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboDepartment.SelectedValue = 0
            txtClFrom.Text = ""
            txtClto.Text = ""
            txtImpFrom.Text = ""
            txtImpTo.Text = ""
            txtWarningFrom.Text = ""
            txtWarningTo.Text = ""
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkIncludeInactiveEmp.Checked = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAppraisalReport.SetDefaultValue()

            objAppraisalReport._YearId = FinancialYear._Object._YearUnkid
            objAppraisalReport._DepartmentId = CInt(cboDepartment.SelectedValue)
            objAppraisalReport._DepartmentName = cboDepartment.Text
            objAppraisalReport._CLFrom = CInt(txtClFrom.Decimal)
            objAppraisalReport._CLTo = CInt(txtClto.Decimal)
            objAppraisalReport._ImpFrom = CInt(txtImpFrom.Decimal)
            objAppraisalReport._ImpTo = CInt(txtImpTo.Decimal)
            objAppraisalReport._WarningFrom = CInt(txtWarningFrom.Decimal)
            objAppraisalReport._WarningTo = CInt(txtWarningTo.Decimal)

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAppraisalReport._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'S.SANDEEP [ 12 MAY 2012 ] -- END`

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmAppraisalSummReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAppraisalReport = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAppraisalSummReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAppraisalSummReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Me._Title = objAppraisalReport._ReportName
            Me._Message = objAppraisalReport._ReportDesc

            FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppraisalSummReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAppraisalReport.generateReport(0, e.Type, enExportAction.None)
            objAppraisalReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAppraisalReport.generateReport(0, enPrintAction.None, e.Type)
            objAppraisalReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDept.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboDepartment.DataSource
            frm.ValueMember = cboDepartment.ValueMember
            frm.DisplayMember = cboDepartment.DisplayMember
            If frm.DisplayDialog Then
                cboDepartment.SelectedValue = frm.SelectedValue
                cboDepartment.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDept_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAppraisalSummReport.SetMessages()
            objfrm._Other_ModuleNames = "clsAppraisalSummReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblClTo.Text = Language._Object.getCaption(Me.lblClTo.Name, Me.lblClTo.Text)
            Me.lblCommendableletters.Text = Language._Object.getCaption(Me.lblCommendableletters.Name, Me.lblCommendableletters.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblImprovment.Text = Language._Object.getCaption(Me.lblImprovment.Name, Me.lblImprovment.Text)
            Me.lblWarningTo.Text = Language._Object.getCaption(Me.lblWarningTo.Name, Me.lblWarningTo.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblImpTo.Text = Language._Object.getCaption(Me.lblImpTo.Name, Me.lblImpTo.Text)
            Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'************************************************************************************************************************************
'Class Name : clsAssessmentListReport.vb
'Purpose    :
'Date       :10 Feb 2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAppraisalSummReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAppraisalSummReport"
    Private mstrReportId As String = enArutiReport.AppraisalSummaryReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintYearId As Integer = 0
    Private mintDeprtmentId As Integer = 0
    Private mstrDepartment As String = ""
    Private mintCLFrom As Integer = 0
    Private mintCLTo As Integer = 0
    Private mintImpFrom As Integer = 0
    Private mintImpTo As Integer = 0
    Private mintWarningFrom As Integer = 0
    Private mintWarningTo As Integer = 0
    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIncludeInactiveEmp As Boolean = False
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'S.SANDEEP [19 NOV 2015] -- START
    Private intYear As Integer = 0
    Private intMonth As Integer = 0
    Private intDay As Integer = 0
    'S.SANDEEP [19 NOV 2015] -- END

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDeprtmentId = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentName() As String
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Public WriteOnly Property _CLFrom() As Integer
        Set(ByVal value As Integer)
            mintCLFrom = value
        End Set
    End Property

    Public WriteOnly Property _CLTo() As Integer
        Set(ByVal value As Integer)
            mintCLTo = value
        End Set
    End Property

    Public WriteOnly Property _ImpFrom() As Integer
        Set(ByVal value As Integer)
            mintImpFrom = value
        End Set
    End Property

    Public WriteOnly Property _ImpTo() As Integer
        Set(ByVal value As Integer)
            mintImpTo = value
        End Set
    End Property

    Public WriteOnly Property _WarningFrom() As Integer
        Set(ByVal value As Integer)
            mintWarningFrom = value
        End Set
    End Property

    Public WriteOnly Property _WarningTo() As Integer
        Set(ByVal value As Integer)
            mintWarningTo = value
        End Set
    End Property

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'Pinkal (14-Aug-2012) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintYearId = 0
            mintDeprtmentId = 0
            mstrDepartment = ""
            mintCLFrom = 0
            mintCLTo = 0
            mintImpFrom = 0
            mintImpTo = 0
            mintWarningFrom = 0
            mintWarningTo = 0
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mblnIncludeInactiveEmp = False
            'S.SANDEEP [ 12 MAY 2012 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintYearId > 0 Then
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearId)
                Me._FilterQuery &= ""
                Me._FilterTitle &= ""
            End If

            If mintDeprtmentId > 0 Then
                objDataOperation.AddParameter("@deptID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeprtmentId)
                Me._FilterQuery &= " AND hrdepartment_master.departmentunkid = @deptID "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Department :") & " " & mstrDepartment & " "
            End If

            If mintCLFrom > 0 And mintCLTo > 0 Then

                objDataOperation.AddParameter("@ClFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCLFrom)
                Me._FilterQuery &= " AND Op2Emp >= @ClFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Commendable Letters From :") & " " & mintCLFrom & " "

                objDataOperation.AddParameter("@ClTo", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCLTo)
                Me._FilterQuery &= " AND Op2Emp <= @ClTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Commendable Letters To :") & " " & mintCLTo & " "

            End If

            If mintImpFrom > 0 And mintImpTo > 0 Then

                objDataOperation.AddParameter("@ImpFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpFrom)
                Me._FilterQuery &= " AND Op3Emp >= @ImpFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Letters of Improvement From :") & " " & mintImpFrom & " "

                objDataOperation.AddParameter("@ImpTo", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImpTo)
                Me._FilterQuery &= " AND Op3Emp <= @ImpTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Letters of Improvement To :") & " " & mintImpTo & " "

            End If

            If mintWarningFrom > 0 And mintWarningTo > 0 Then

                objDataOperation.AddParameter("@warnFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWarningFrom)
                Me._FilterQuery &= " AND Op4Emp >= @warnFrom "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Warning From :") & " " & mintWarningFrom & " "

                objDataOperation.AddParameter("@warnTo", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWarningTo)
                Me._FilterQuery &= " AND Op4Emp <= @warnTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Warning To :") & " " & mintWarningTo & " "

            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

            'S.SANDEEP [30 JAN 2016] -- START
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("employeecode", Language.getMessage(mstrModuleName, 3, "Employee Code")))
            iColumn_DetailReport.Add(New IColumn("firstname + ' ' + ISNULL(othername,'')+ ' ' + surname", Language.getMessage(mstrModuleName, 3, "Employee")))
            iColumn_DetailReport.Add(New IColumn("gm.name", Language.getMessage(mstrModuleName, 3, "Grade")))
            iColumn_DetailReport.Add(New IColumn("DM.name", Language.getMessage(mstrModuleName, 3, "Department")))
            iColumn_DetailReport.Add(New IColumn("JM.job_name", Language.getMessage(mstrModuleName, 3, "Job")))
            'S.SANDEEP [30 JAN 2016] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = intCompanyUnkid
            mDecRoundingFactor = objConfig._PAScoringRoudingFactor
            objConfig = Nothing
            'S.SANDEEP |24-DEC-2019| -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '           " hrdepartment_master.departmentunkid " & _
            '           ",name " & _
            '           ",CAST(ISNULL(TotEmp,0) AS INT) AS TOTAL_EMPLOYEE " & _
            '           ",CAST(ISNULL(SEmp,0) AS INT) AS FORMS_RECEIVED " & _
            '           ",CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((ISNULL(SEmp,0)*100)/ISNULL(TotEmp,0) AS DECIMAL(10,2)) END AS RECEIVED_PERCENT " & _
            '           ",CAST(ISNULL(TotEmp,0) AS INT) - CAST(ISNULL(SEmp,0) AS INT) AS NOT_SUBMITTED " & _
            '           ",CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((CAST(ISNULL(TotEmp,0) AS INT) - CAST(ISNULL(SEmp,0) AS INT)) * 100/ISNULL(TotEmp,0) AS DECIMAL(10,2)) END AS NOT_PERCENT " & _
            '           ",CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((ISNULL(Op1Emp,0)*100)/ISNULL(TotEmp,0) AS DECIMAL (10,2)) END AS SALARY_PERCENT " & _
            '           ",ISNULL(CAST(Op2Emp AS NVARCHAR(50)),'') AS COMM_LETTERS " & _
            '           ",ISNULL(CAST(Op3Emp AS NVARCHAR(50)),'') AS IMPROV_LETTERS " & _
            '           ",ISNULL(CAST(Op4Emp AS NVARCHAR(50)),'') AS WARNINGS " & _
            '       "FROM hrdepartment_master " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '             "SELECT " & _
            '                   "hremployee_master.departmentunkid " & _
            '                  ",CAST(COUNT(hremployee_master.employeeunkid) AS DECIMAL(10,2)) AS Op1Emp " & _
            '             "FROM hrapps_finalemployee " & _
            '             "JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid " & _
            '             "WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.SALARY_INCREMENT & "' "

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master .departmentunkid " & _
            '        ") AS C ON dbo.hrdepartment_master.departmentunkid = C.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "hremployee_master.departmentunkid " & _
            '                  ",COUNT(hremployee_master.employeeunkid) AS Op2Emp " & _
            '             "FROM hrapps_finalemployee " & _
            '             "JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid " & _
            '             "WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.COMMENDABLE_LETTER & "' "
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master .departmentunkid " & _
            '        ") AS D ON hrdepartment_master.departmentunkid = D.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "hremployee_master.departmentunkid " & _
            '                  ",COUNT(hremployee_master.employeeunkid) AS Op3Emp " & _
            '             "FROM hrapps_finalemployee " & _
            '             "JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid " & _
            '             "WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.IMPROVEMENT_LETTER & "' "
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master .departmentunkid " & _
            '        ") AS E ON hrdepartment_master.departmentunkid = E.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "hremployee_master.departmentunkid " & _
            '                  ",COUNT(hremployee_master.employeeunkid) AS Op4Emp " & _
            '             "FROM hrapps_finalemployee " & _
            '             "JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid " & _
            '             "WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.WARNING_LETTER & "' "
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master .departmentunkid " & _
            '        ") AS F ON hrdepartment_master.departmentunkid = F.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                " departmentunkid " & _
            '                ",ISNULL(SUM(SEmp),0) AS SEmp " & _
            '              "FROM " & _
            '              "( " & _
            '                     "SELECT " & _
            '                           "hremployee_master.departmentunkid " & _
            '                          ",hremployee_master.employeeunkid " & _
            '                          ",CAST(COUNT(hremployee_master.employeeunkid) AS DECIMAL(10,2)) AS SEmp " & _
            '                     "FROM hrevaluation_analysis_master " & _
            '                          "JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid " & _
            '                     "WHERE selfemployeeunkid > 0 AND iscommitted = 1 "

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master.departmentunkid,hremployee_master.employeeunkid " & _
            '              ") AS Self " & _
            '              "JOIN " & _
            '              "( " & _
            '                     "SELECT " & _
            '                           "hremployee_master.departmentunkid AS deptId " & _
            '                          ",hremployee_master.employeeunkid AS AempId " & _
            '                          ",CAST(COUNT(hremployee_master.employeeunkid)/COUNT(hrevaluation_analysis_master.assessedemployeeunkid)AS DECIMAL(10,2)) AS ASEmp " & _
            '                     "FROM hrevaluation_analysis_master " & _
            '                          "JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid " & _
            '                     "WHERE assessedemployeeunkid > 0 AND iscommitted = 1 "

            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY hremployee_master.departmentunkid,hremployee_master.employeeunkid " & _
            '              ") AS Assessed ON Assessed.deptId = Self.departmentunkid AND Assessed.AempId = Self.employeeunkid GROUP BY departmentunkid " & _
            '        ") AS B ON dbo.hrdepartment_master.departmentunkid = B.departmentunkid " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '             "SELECT " & _
            '                   "departmentunkid " & _
            '                  ",CAST(COUNT(employeeunkid)AS DECIMAL(10,2)) AS TotEmp " & _
            '             "FROM hremployee_master WHERE YEAR(GETDATE()) - YEAR(appointeddate) > 1 "
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'StrQ &= "GROUP BY departmentunkid " & _
            '        ") AS A ON hrdepartment_master.departmentunkid = A.departmentunkid " & _
            '        "WHERE isactive = 1 "

            StrQ = "SELECT " & _
                   "     hrdepartment_master.departmentunkid " & _
                   "    ,name " & _
                   "    ,CAST(ISNULL(TotEmp,0) AS INT) AS TOTAL_EMPLOYEE " & _
                   "    ,CAST(ISNULL(SEmp,0) AS INT) AS FORMS_RECEIVED " & _
                   "    ,CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((ISNULL(SEmp,0)*100)/ISNULL(TotEmp,0) AS DECIMAL(10,2)) END AS RECEIVED_PERCENT " & _
                   "    ,CAST(ISNULL(TotEmp,0) AS INT) - CAST(ISNULL(SEmp,0) AS INT) AS NOT_SUBMITTED " & _
                   "    ,CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((CAST(ISNULL(TotEmp,0) AS INT) - CAST(ISNULL(SEmp,0) AS INT)) * 100/ISNULL(TotEmp,0) AS DECIMAL(10,2)) END AS NOT_PERCENT " & _
                   "    ,CASE WHEN ISNULL(TotEmp,0) = 0 THEN 0 ELSE CAST((ISNULL(Op1Emp,0)*100)/ISNULL(TotEmp,0) AS DECIMAL (10,2)) END AS SALARY_PERCENT " & _
                   "    ,ISNULL(CAST(Op2Emp AS NVARCHAR(50)),'') AS COMM_LETTERS " & _
                   "    ,ISNULL(CAST(Op3Emp AS NVARCHAR(50)),'') AS IMPROV_LETTERS " & _
                   "    ,ISNULL(CAST(Op4Emp AS NVARCHAR(50)),'') AS WARNINGS " & _
                   "FROM hrdepartment_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         DEPT.departmentunkid " & _
                   "        ,CAST(COUNT(hremployee_master.employeeunkid) AS DECIMAL(10,2)) AS Op1Emp " & _
                   "    FROM hrapps_finalemployee " & _
                   "        JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "        JOIN " & _
                   "        (" & _
                   "        SELECT " & _
                   "        	EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                   "        FROM" & _
                   "        (" & _
                   "            SELECT " & _
                   "                 hremployee_transfer_tran.employeeunkid " & _
                   "                ,hremployee_transfer_tran.departmentunkid " & _
                   "                ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                   "            FROM hremployee_transfer_tran " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                   "        ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                   ") AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.SALARY_INCREMENT & "' "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid " & _
                    ") AS C ON dbo.hrdepartment_master.departmentunkid = C.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        DEPT.departmentunkid " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS Op2Emp " & _
                    "   FROM hrapps_finalemployee " & _
                    "   JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "   JOIN " & _
                    "   (" & _
                    "        SELECT " & _
                    "        	EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "        FROM" & _
                    "        (" & _
                    "            SELECT " & _
                    "                 hremployee_transfer_tran.employeeunkid " & _
                    "                ,hremployee_transfer_tran.departmentunkid " & _
                    "                ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "            FROM hremployee_transfer_tran " & _
                    "            WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "        ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "   ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.COMMENDABLE_LETTER & "' "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid " & _
                    ") AS D ON hrdepartment_master.departmentunkid = D.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        DEPT.departmentunkid " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS Op3Emp " & _
                    "   FROM hrapps_finalemployee " & _
                    "       JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "       JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "        	    EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "           FROM " & _
                    "           (" & _
                    "                SELECT " & _
                    "                    hremployee_transfer_tran.employeeunkid " & _
                    "                   ,hremployee_transfer_tran.departmentunkid " & _
                    "                   ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "               FROM hremployee_transfer_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "       ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.IMPROVEMENT_LETTER & "' "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid " & _
                    ") AS E ON hrdepartment_master.departmentunkid = E.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        DEPT.departmentunkid " & _
                    "       ,COUNT(hremployee_master.employeeunkid) AS Op4Emp " & _
                    "   FROM hrapps_finalemployee " & _
                    "       JOIN hremployee_master ON hrapps_finalemployee.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "       JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "        	    EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "           FROM " & _
                    "           (" & _
                    "                SELECT " & _
                    "                    hremployee_transfer_tran.employeeunkid " & _
                    "                   ,hremployee_transfer_tran.departmentunkid " & _
                    "                   ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "               FROM hremployee_transfer_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "       ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE isfinalshortlisted = 1 AND operationmodeid  = '" & enAppraisal_Modes.WARNING_LETTER & "' "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid " & _
                    ") AS F ON hrdepartment_master.departmentunkid = F.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        departmentunkid " & _
                    "       ,ISNULL(SUM(SEmp),0) AS SEmp " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            DEPT.departmentunkid " & _
                    "           ,hremployee_master.employeeunkid " & _
                    "           ,CAST(COUNT(hremployee_master.employeeunkid) AS DECIMAL(10,2)) AS SEmp " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "       JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.selfemployeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "       JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "        	    EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "           FROM " & _
                    "           (" & _
                    "                SELECT " & _
                    "                    hremployee_transfer_tran.employeeunkid " & _
                    "                   ,hremployee_transfer_tran.departmentunkid " & _
                    "                   ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "               FROM hremployee_transfer_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "       ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            

            StrQ &= " WHERE selfemployeeunkid > 0 AND iscommitted = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid,hremployee_master.employeeunkid " & _
                    ") AS Self " & _
                    "JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        DEPT.departmentunkid AS deptId " & _
                    "       ,hremployee_master.employeeunkid AS AempId " & _
                    "       ,CAST(COUNT(hremployee_master.employeeunkid)/COUNT(hrevaluation_analysis_master.assessedemployeeunkid)AS DECIMAL(10,2)) AS ASEmp " & _
                    "   FROM hrevaluation_analysis_master " & _
                    "       JOIN hremployee_master ON hremployee_master.employeeunkid = hrevaluation_analysis_master.assessedemployeeunkid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "       JOIN " & _
                    "       (" & _
                    "           SELECT " & _
                    "        	    EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "           FROM " & _
                    "           (" & _
                    "                SELECT " & _
                    "                    hremployee_transfer_tran.employeeunkid " & _
                    "                   ,hremployee_transfer_tran.departmentunkid " & _
                    "                   ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "               FROM hremployee_transfer_tran " & _
                    "               WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "           ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "       ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            

            StrQ &= " WHERE assessedemployeeunkid > 0 AND iscommitted = 1 "


            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            StrQ &= "GROUP BY DEPT.departmentunkid,hremployee_master.employeeunkid " & _
                    ") AS Assessed ON Assessed.deptId = Self.departmentunkid AND Assessed.AempId = Self.employeeunkid GROUP BY departmentunkid " & _
                    ") AS B ON dbo.hrdepartment_master.departmentunkid = B.departmentunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        DEPT.departmentunkid " & _
                    "       ,CAST(COUNT(hremployee_master.employeeunkid)AS DECIMAL(10,2)) AS TotEmp " & _
                    "   FROM hremployee_master "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "   JOIN " & _
                    "   (" & _
                    "        SELECT " & _
                    "      	    EDEPT.employeeunkid,EDEPT.departmentunkid " & _
                    "        FROM " & _
                    "        (" & _
                    "             SELECT " & _
                    "                hremployee_transfer_tran.employeeunkid " & _
                    "               ,hremployee_transfer_tran.departmentunkid " & _
                    "               ,ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC ) AS Rno " & _
                    "             FROM hremployee_transfer_tran " & _
                    "             WHERE isvoid = 0 AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "         ) AS EDEPT WHERE EDEPT.Rno = 1 " & _
                    "   ) AS DEPT ON DEPT.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE YEAR(GETDATE()) - YEAR(appointeddate) > 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            StrQ &= "GROUP BY DEPT.departmentunkid " & _
                    ") AS A ON hrdepartment_master.departmentunkid = A.departmentunkid " & _
                    "WHERE isactive = 1 "
            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("name")
                rpt_Rows.Item("Column2") = dtRow.Item("TOTAL_EMPLOYEE")
                rpt_Rows.Item("Column3") = dtRow.Item("FORMS_RECEIVED")
                rpt_Rows.Item("Column4") = dtRow.Item("RECEIVED_PERCENT")
                rpt_Rows.Item("Column5") = dtRow.Item("NOT_SUBMITTED")
                rpt_Rows.Item("Column6") = dtRow.Item("NOT_PERCENT")
                rpt_Rows.Item("Column7") = dtRow.Item("SALARY_PERCENT")
                rpt_Rows.Item("Column8") = dtRow.Item("COMM_LETTERS")
                rpt_Rows.Item("Column9") = dtRow.Item("IMPROV_LETTERS")
                rpt_Rows.Item("Column10") = dtRow.Item("WARNINGS")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptAppraisalSummReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            Dim objImg(0) As Object

            If Company._Object._Image IsNot Nothing Then
                objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
            Else
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)
            End If

            arrImageRow.Item("arutiLogo") = objImg(0)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 14, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 15, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 16, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 17, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 1, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtNoOfEmp", Language.getMessage(mstrModuleName, 2, "Total Employee Above 1 Year of Service"))
            Call ReportFunction.TextChange(objRpt, "txtTotRcvd", Language.getMessage(mstrModuleName, 3, "Total Received"))
            Call ReportFunction.TextChange(objRpt, "txtRcvdPer", Language.getMessage(mstrModuleName, 4, "%"))
            Call ReportFunction.TextChange(objRpt, "txtNotSubmitted", Language.getMessage(mstrModuleName, 5, "Not Submitted"))
            Call ReportFunction.TextChange(objRpt, "txtNotPer", Language.getMessage(mstrModuleName, 4, "%"))
            Call ReportFunction.TextChange(objRpt, "txtSalPer", Language.getMessage(mstrModuleName, 6, "Salary Increase By %"))
            Call ReportFunction.TextChange(objRpt, "txtCommLetter", Language.getMessage(mstrModuleName, 7, "Commendable Letters"))
            Call ReportFunction.TextChange(objRpt, "txtImproveList", Language.getMessage(mstrModuleName, 8, "Letter of Improvement"))
            Call ReportFunction.TextChange(objRpt, "txtWarning", Language.getMessage(mstrModuleName, 9, "Warning"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 12, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call ReportFunction.TextChange(objRpt, "txtHeading", Language.getMessage(mstrModuleName, 13, "SUMMARY FOR APPRAISAL RESULT AWARDS FOR THE YEAR : " & FinancialYear._Object._FinancialYear_Name))
            Call ReportFunction.TextChange(objRpt, "txtHeading", Language.getMessage(mstrModuleName, 13, "SUMMARY FOR APPRAISAL RESULT AWARDS FOR THE YEAR : ") & FinancialYear._Object._FinancialYear_Name)
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'S.SANDEEP [19 NOV 2015] -- START
    Public Function Generate_Appraisal_Summary_Report(ByVal mintViewIdx As Integer, _
                                                      ByVal mstrAnalysis_Fields As String, _
                                                      ByVal mstrAnalysis_Join As String, _
                                                      ByVal mstrAnalysis_OrderBy As String, _
                                                      ByVal mstrReport_GroupName As String, _
                                                      ByVal mstrAdvanceFilter As String, _
                                                      ByVal intAssessmentPeriodId As Integer, _
                                                      ByVal intSelectedEmployeeId As Integer, _
                                                      ByVal intTnAPeriodId As Integer, _
                                                      ByVal strExportReportPath As String, _
                                                      ByVal blnOpenAfterExport As Boolean, _
                                                      ByVal dtTnAPeriodStartDate As Date, _
                                                      ByVal dtTnAPeriodEndDate As Date, _
                                                      ByVal dtPYPeriodStartDate As Date, _
                                                      ByVal dtPYPeriodEndDate As Date, _
                                                      ByVal strUserAccessFilterString As String, _
                                                      ByVal blnIncludeInactiveEmployee As Boolean, _
                                                      ByVal strSelectedEmployeeName As String, _
                                                      ByVal strAssessmentPeriodName As String, _
                                                      ByVal strTnAPeriodName As String, _
                                                      ByVal strFmtCurrency As String, _
                                                      ByVal blnIsSelfAssignCompetencies As Boolean, _
                                                      ByVal blnExludeZeroRatingValues As Boolean, _
                                                      ByVal strExcludeInactiveEmployeeCaption As String, _
                                                      ByVal strExludeZeroRatingCaption As String, _
                                                      ByVal strDatabaseName As String, _
                                                      ByVal intUserUnkid As Integer, _
                                                      ByVal intYearUnkid As Integer, _
                                                      ByVal intCompanyUnkid As Integer, _
                                                      ByVal strUserModeSetting As String, _
                                                      ByVal blnOnlyApproved As Boolean) As Boolean  'OMAN REPORT
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim dtExcelTable As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        Try
            'S.SANDEEP [13-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : 
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid
            'S.SANDEEP [13-JUL-2017] -- END

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            mDecRoundingFactor = ConfigParameter._Object._PAScoringRoudingFactor
            'S.SANDEEP |24-DEC-2019| -- END

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtTnAPeriodStartDate, dtTnAPeriodEndDate, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtTnAPeriodEndDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtTnAPeriodEndDate, strDatabaseName)

            dtExcelTable = New DataTable("OmanSummary")
            Dim dCol As DataColumn = Nothing

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 100, "Sr.No.")
                .ColumnName = "SrNo"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 101, "Emp.No.")
                .ColumnName = "EmpNo"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 102, "Name")
                .ColumnName = "Name"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 103, "Department")
                .ColumnName = "Department"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 104, "Designation")
                .ColumnName = "Designation"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 105, "Category")
                .ColumnName = "Category"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 106, "DOB")
                .ColumnName = "DOB"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 107, "DOJ")
                .ColumnName = "DOJ"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 108, "Years Worked")
                .ColumnName = "YrsWorked"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 109, "Grade")
                .ColumnName = "Grade"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)


            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 110, "Appraisal Ratings %")
                .ColumnName = "apprrating"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)


            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 111, "Appraisal Grade")
                .ColumnName = "apprgrade"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)


            'Pinkal (24-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 120, "Appraiser Name")
                .ColumnName = "Appraiser"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 121, "Appraiser Comments")
                .ColumnName = "Appraiser_comment"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            'Pinkal (24-Nov-2015) -- End

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 113, "Basic Salary")
                .ColumnName = "BasicSal"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Decimal")
            End With
            dtExcelTable.Columns.Add(dCol)

            Dim objTranHeadMst As New clsTransactionHead

            dsList = objTranHeadMst.GetList("Heads", , , , , , , "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others & " AND prtranhead_master.tranheadunkid IN(39,40,41,42,43,44)")

            objTranHeadMst = Nothing
            If dsList.Tables("Heads").Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables("Heads").Rows
                    dCol = New DataColumn
                    With dCol
                        .Caption = dr("trnheadname")
                        .ColumnName = dr("tranheadunkid")
                        .DefaultValue = 0
                        .DataType = System.Type.GetType("System.Decimal")
                    End With
                    dtExcelTable.Columns.Add(dCol)
                Next

            End If

            dCol = New DataColumn
            With dCol
                .Caption = Language.getMessage(mstrModuleName, 120, "Total")
                .ColumnName = "total"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Decimal")
            End With
            dtExcelTable.Columns.Add(dCol)


            dCol = New DataColumn
            With dCol
                .ColumnName = "Id"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtExcelTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GName"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtExcelTable.Columns.Add(dCol)

            StrQ = "SELECT " & _
                   "     ROW_NUMBER()OVER(ORDER BY hremployee_master.employeeunkid) AS SrNo " & _
                   "    ,employeecode AS EmpNo " & _
                   "    ,firstname + ' ' + ISNULL(othername,'')+ ' ' + surname AS Name " & _
                   "    ,DM.name AS Department " & _
                   "    ,JM.job_name AS Designation " & _
                   "    ,ISNULL(TM.name,'') AS Category " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),birthdate,112),'') AS DOB " & _
                   "    ,ISNULL(CONVERT(NVARCHAR(8),appointeddate,112),'') AS DOJ " & _
                   "    ,'' AS YrsWorked " & _
                   "    ,gm.name AS Grade " & _
                   "    ,'' AS RatePoint " & _
                   "    ,'' AS RateScore " & _
                   "    ,hremployee_master.employeeunkid AS EmpId "

            If mintViewIdx > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_transfer_tran.employeeunkid AS EmpId " & _
                    "       ,hremployee_transfer_tran.departmentunkid " & _
                    "       ,hremployee_transfer_tran.teamunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE hremployee_transfer_tran.isvoid = 0 " & _
                    "   AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtTnAPeriodEndDate) & "' " & _
                    ") AS EDM ON EDM.EmpId = hremployee_master.employeeunkid AND EDM.rno = 1 " & _
                    "JOIN hrdepartment_master AS DM ON DM.departmentunkid = EDM.departmentunkid " & _
                    "LEFT JOIN hrteam_master AS TM ON EDM.teamunkid = TM.teamunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hremployee_categorization_tran.employeeunkid AS EmpId " & _
                    "       ,hremployee_categorization_tran.jobunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE hremployee_categorization_tran.isvoid = 0 " & _
                    "       AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= '" & eZeeDate.convertDate(dtTnAPeriodEndDate) & "' " & _
                    ") AS EJM ON EJM.EmpId = hremployee_master.employeeunkid AND EJM.rno = 1 " & _
                    "JOIN hrjob_master AS JM ON JM.jobunkid = EJM.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        prsalaryincrement_tran.employeeunkid AS EmpId " & _
                    "       ,prsalaryincrement_tran.gradeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC,prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran " & _
                    "   WHERE prsalaryincrement_tran.isvoid = 0 " & _
                    "       AND prsalaryincrement_tran.isapproved = 1 " & _
                    "       AND CONVERT(NVARCHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(dtTnAPeriodEndDate) & "' " & _
                    ") AS EGM ON EGM.EmpId = hremployee_master.employeeunkid AND EGM.rno = 1 " & _
                    "JOIN hrgrade_master gm ON EGM.gradeunkid = gm.gradeunkid "

            If mintViewIdx > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= " WHERE hremployee_master.isapproved = 1 "

            If intSelectedEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & intSelectedEmployeeId & "' "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If mintViewIdx > 0 Then
                StrQ &= " Order By " & mstrAnalysis_OrderBy
                If Me.OrderByQuery <> "" Then
                    StrQ &= "," & Me.OrderByQuery
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "  ORDER BY " & Me.OrderByQuery
                End If
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim intRecordNumber As Integer = 1
            Dim objED As New clsEarningDeduction
            For Each dr As DataRow In dsList.Tables("DataTable").Rows
                dtExcelTable.ImportRow(dr)

                'dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("SrNo") = intRecordNumber
                'intRecordNumber = intRecordNumber + 1

                If dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOB").ToString.Trim.Length > 0 Then dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOB") = eZeeDate.convertDate(dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOB").ToString).ToShortDateString
                If dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOJ").ToString.Trim.Length > 0 Then dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOJ") = eZeeDate.convertDate(dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOJ").ToString).ToShortDateString
                'If dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("apprrating") = 0 Then dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("apprrating") = ""
                PerformDateOperation(CDate(dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("DOJ")).Date, ConfigParameter._Object._CurrentDateAndTime.Date)
                dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("YrsWorked") = Language.getMessage(mstrModuleName, 121, "Yrs. worked") & " : " & intYear & Language.getMessage(mstrModuleName, 122, "Yrs") & ", " & intMonth & " " & Language.getMessage(mstrModuleName, 123, "mon")

                '*********************************** BASIC SALARY PART | START
                Dim dsBasicPay As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPYPeriodStartDate, dtPYPeriodEndDate, strUserModeSetting, blnOnlyApproved, mblnIncludeInactiveEmp, "List", , dr("EmpId").ToString(), , , , , , dtPYPeriodEndDate, , , , " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others)


                '57.2
                'Dim dsBasicPay As DataSet = objED.GetList("List", dr("EmpId").ToString(), 0, 0, 0, 0, 0, 0 _
                '                                                                  , 0, 0, 0, 0, "", 0, 3, 0, True, "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others _
                '                                                                  , dtPYPeriodEndDate.Date, -1, "", "", Nothing)

                If dsBasicPay IsNot Nothing AndAlso dsBasicPay.Tables(0).Rows.Count > 0 Then
                    dtExcelTable.Rows(dtExcelTable.Rows.Count - 1).Item("BasicSal") = Format(CDec(dsBasicPay.Tables(0).Rows(0)("amount")), strFmtCurrency)
                End If
                dsBasicPay.Clear()
                dsBasicPay = Nothing
                '*********************************** BASIC SALARY PART | END


                '*********************************** ALLOWANCE PART | START
                'Dim dsOtherAllowance As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPYPeriodStartDate, dtPYPeriodEndDate, strUserModeSetting, blnOnlyApproved, mblnIncludeInactiveEmp, "List", , dr("EmpId").ToString(), , , , , "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others, dtPYPeriodEndDate)

                Dim dsOtherAllowance As DataSet = objED.GetList(strDatabaseName, intUserUnkid, intYearUnkid, intCompanyUnkid, dtPYPeriodStartDate, dtPYPeriodEndDate, strUserModeSetting, blnOnlyApproved, mblnIncludeInactiveEmp, "List", , dr("EmpId").ToString(), , , , , , dtPYPeriodEndDate, , , , " AND prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others)

                '57.2
                'Dim dsOtherAllowance As DataSet = objED.GetList("List", dr("EmpId").ToString(), 0, 0, 0, 0, 0, 0 _
                '                                                                  , 0, 0, 0, 0, "", 0, 3, 0, True, "prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " AND prtranhead_master.typeof_id = " & enTypeOf.Informational & " AND prtranhead_master.calctype_id = " & enCalcType.FlatRate_Others _
                '                                                                  , dtPYPeriodEndDate.Date, -1, "", "", Nothing)

                If dsOtherAllowance IsNot Nothing AndAlso dsOtherAllowance.Tables(0).Rows.Count > 0 Then
                    Dim xRowtotal As Decimal = 0
                    For Each drow As DataRow In dsOtherAllowance.Tables("List").Rows
                        If dtExcelTable.Columns.Contains(drow("tranheadunkid").ToString) Then
                            xRowtotal += CDec(drow("amount"))
                            dtExcelTable.Rows(dtExcelTable.Rows.Count - 1).Item(drow("tranheadunkid").ToString) = Format(CDec(drow("amount")), strFmtCurrency)
                        End If
                    Next

                    'Pinkal (24-Nov-2015) -- Start
                    'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
                    'dtExcelTable.Rows(dtExcelTable.Rows.Count - 1).Item("total") = Format(CDec(xRowtotal), strFmtCurrency)
                    dtExcelTable.Rows(dtExcelTable.Rows.Count - 1).Item("total") = Format(CDec(xRowtotal) + CDec(dtExcelTable.Rows(dtExcelTable.Rows.Count - 1).Item("BasicSal")), strFmtCurrency)
                    'Pinkal (24-Nov-2015) -- End


                End If
                dsOtherAllowance.Clear()
                dsOtherAllowance = Nothing
                '*********************************** ALLOWANCE PART | END

                '*********************************** SCORING PART | START
                Dim dsDetails As New DataSet
                Dim iStrGroupIds As String = String.Empty
                Dim objAssessGrp As New clsassess_group_master
                'S.SANDEEP [19 DEC 2016] -- START
                'iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(dr("EmpId"), dtTnAPeriodEndDate)
                iStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(dr("EmpId"), dtTnAPeriodEndDate, intTnAPeriodId)
                'S.SANDEEP [19 DEC 2016] -- END
                If iStrGroupIds.Trim.Length <= 0 Then iStrGroupIds = "0"
                dsDetails = GetCompetenciesData(iStrGroupIds, blnIsSelfAssignCompetencies, dr("EmpId"))
                Dim totalwgt As Decimal = 0 : Dim strGrpValue As String = "" : Dim total As Decimal

                For Each xrow As DataRow In dsDetails.Tables(0).Rows
                    If strGrpValue <> xrow("assessgroupunkid").ToString Then
                        totalwgt += CDec(xrow("Col_Group_Weight_Value"))
                        strGrpValue = xrow("assessgroupunkid").ToString
                    End If
                Next

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                'AND isfound = 1
                'dsDetails = New DataSet
                'Dim iEAssessorId, iEReviewerId As Integer
                'iEAssessorId = 0 : iEReviewerId = 0
                'dsDetails = GetEmployee_AssessorReviewerDetails(dr("EmpId"), intAssessmentPeriodId, strDatabaseName)
                'If dsDetails.Tables(0).Rows.Count > 0 Then
                '    Dim dtmp() As DataRow = Nothing
                '    If iEAssessorId <= 0 Then
                '        dtmp = dsDetails.Tables(0).Select("isreviewer = 0")
                '        If dtmp.Length > 0 Then
                '            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                '            'Pinkal (24-Nov-2015) -- Start
                '            'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
                '            dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("Appraiser") = dtmp(0)("arname")
                '            'Pinkal (24-Nov-2015) -- End

                '        End If
                '    End If
                '    If iEReviewerId <= 0 Then
                '        dtmp = dsDetails.Tables(0).Select("isreviewer = 1")
                '        If dtmp.Length > 0 Then
                '            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                '        End If
                '    End If
                'End If
                dsDetails = New DataSet
                Dim iEAssessorId, iEReviewerId As Integer
                iEAssessorId = 0 : iEReviewerId = 0

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
                'dsDetails = GetEmployee_AssessorReviewerDetails(dr("EmpId"), intAssessmentPeriodId, strDatabaseName)
                dsDetails = GetEmployee_AssessorReviewerDetails(dr("EmpId"), intAssessmentPeriodId, strDatabaseName, dtTnAPeriodEndDate)
                'S.SANDEEP [04-AUG-2017] -- END
                If dsDetails.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = Nothing
                    If iEAssessorId <= 0 Then
                        dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                        If dtmp.Length > 0 Then
                            iEAssessorId = dtmp(0).Item("assessormasterunkid")
                            dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("Appraiser") = dtmp(0)("arname")
                        Else
                            'S.SANDEEP [06-NOV-2017] -- START
                            'dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                            'iEAssessorId = dtmp(0).Item("assessormasterunkid")
                            'dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("Appraiser") = dtmp(0)("arname")

                            dtmp = dsDetails.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                            If dtmp.Length > 0 Then
                                iEAssessorId = dtmp(0).Item("assessormasterunkid")
                                dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("Appraiser") = dtmp(0)("arname")
                            End If
                            'S.SANDEEP [06-NOV-2017] -- END
                        End If
                    End If
                    If iEReviewerId <= 0 Then
                        dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 1")
                        If dtmp.Length > 0 Then
                            iEReviewerId = dtmp(0).Item("assessormasterunkid")
                        Else
                            'S.SANDEEP [06-NOV-2017] -- START
                            'dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
                            'iEReviewerId = dtmp(0).Item("assessormasterunkid")
                            dtmp = dsDetails.Tables(0).Select("isreviewer = 1 AND isfound = 0 AND visibletypeid = 1")
                            If dtmp.Length > 0 Then
                                iEReviewerId = dtmp(0).Item("assessormasterunkid")
                            End If
                            'S.SANDEEP [06-NOV-2017] -- END
                        End If
                    End If
                End If
                'S.SANDEEP [27 DEC 2016] -- END




                dsDetails = New DataSet
                dsDetails = GetEmployeeCompetencyResult(dr("EmpId"), intAssessmentPeriodId, iStrGroupIds, iEAssessorId, iEReviewerId, "False")
                If dsDetails.Tables(0).Rows.Count > 0 Then
                    total = dsDetails.Tables(0).Compute("SUM(result)", "")
                    'S.SANDEEP |24-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                    total = Rounding.BRound(total, mDecRoundingFactor)
                    'S.SANDEEP |24-DEC-2019| -- END

                    Dim strGrd As String = String.Empty
                    'Pinkal (24-Nov-2015) -- Start
                    'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
                    'total = CDbl(((total * 100) / totalwgt)).ToString("####.#0")
                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                    total = CDbl(((total * 100) / totalwgt)).ToString("###################0.#")
                    'S.SANDEEP |21-AUG-2019| -- END
                    'Pinkal (24-Nov-2015) -- End
                    strGrd = GetAppraisalGrades(total)

                    dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("apprrating") = total
                    dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("apprgrade") = strGrd
                End If
                '*********************************** SCORING PART | START


                'S.SANDEEP [26 NOV 2015] -- START
                '************************************ COMMENTS PART | START
                dtExcelTable.Rows(dtExcelTable.Rows.Count - 1)("Appraiser_comment") = GetAppraisarComments(intAssessmentPeriodId, dr("EmpId"))
                '************************************ COMMENTS PART | END
                'S.SANDEEP [26 NOV 2015] -- END

            Next
            objED = Nothing

            If blnExludeZeroRatingValues = True Then
                'Pinkal (24-Nov-2015) -- Start
                'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
                'dtExcelTable = New DataView(dtExcelTable, "apprrating > 0", "", DataViewRowState.CurrentRows).ToTable
                dtExcelTable = New DataView(dtExcelTable, "apprrating <> ''", "", DataViewRowState.CurrentRows).ToTable
                'Pinkal (24-Nov-2015) -- End
            End If

            'Pinkal (24-Nov-2015) -- Start
            'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.
            Dim rowsToUpdate = dtExcelTable.AsEnumerable()
            intRecordNumber = 1
            For Each xrow As DataRow In rowsToUpdate
                xrow.SetField("SrNo", intRecordNumber)
                intRecordNumber = intRecordNumber + 1
            Next
            'Pinkal (24-Nov-2015) -- End

            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            row = New WorksheetRow()

            Dim strFilterStringTitle As String = ""

            If intAssessmentPeriodId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 114, "Assessment Period :") & " " & strAssessmentPeriodName
            End If

            If intSelectedEmployeeId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 115, "Employee :") & " " & strSelectedEmployeeName
            End If

            If intTnAPeriodId > 0 Then
                strFilterStringTitle &= "," & Language.getMessage(mstrModuleName, 125, "TnA Period :") & " " & strTnAPeriodName
            End If

            If blnIncludeInactiveEmployee = True Then
                strFilterStringTitle &= "," & strExcludeInactiveEmployeeCaption
            End If

            If blnExludeZeroRatingValues = True Then
                strFilterStringTitle &= "," & strExludeZeroRatingCaption
            End If

            strFilterStringTitle = Mid(strFilterStringTitle, 2)



            If mintViewIdx > 0 Then
                dtExcelTable.Columns.Remove("Id")
                dtExcelTable.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                dtExcelTable.Columns.Remove("Id")
                dtExcelTable.Columns.Remove("GName")
            End If

            wcell = New WorksheetCell(strFilterStringTitle.ToString, "s10bw")
            If mintViewIdx > 0 Then
                wcell.MergeAcross = dtExcelTable.Columns.Count - 2
            Else
                wcell.MergeAcross = dtExcelTable.Columns.Count - 1
            End If
            row.Cells.Add(wcell)

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 116, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If



            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 117, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 118, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 119, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            Dim intArrayColumnWidth As Integer() = Nothing
            If mintViewIdx > 0 Then
                ReDim intArrayColumnWidth(dtExcelTable.Columns.Count - 2)
            Else
                ReDim intArrayColumnWidth(dtExcelTable.Columns.Count - 1)
            End If


            'For i As Integer = 0 To intArrayColumnWidth.Length - 1
            '    If i = 0 Then
            '        intArrayColumnWidth(i) = 30
            '    ElseIf i = 1 Then
            '        intArrayColumnWidth(i) = 50
            '    ElseIf i = 2 Then
            '        intArrayColumnWidth(i) = 160
            '    ElseIf i > 2 And i <= 5 Then
            '        intArrayColumnWidth(i) = 125
            '    ElseIf i > 5 And i <= 7 Then
            '        intArrayColumnWidth(i) = 90
            '    ElseIf i > 7 Then
            '        intArrayColumnWidth(i) = 90
            '    End If
            'Next

            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 100
            Next

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportReportPath, blnOpenAfterExport, dtExcelTable, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", strFilterStringTitle, , "", True, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Appraisal_Summary_Report; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return True
    End Function

    Private Function GetCompetenciesData(ByVal strGroupIds As String, ByVal blnIsSlefAssignCompetencies As Boolean, ByVal intEmployeeId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT DISTINCT " & _
                       "     hrassess_group_master.weight AS Col_Group_Weight_Value " & _
                       "    ,hrassess_group_master.assessgroupunkid " & _
                       "FROM hrassess_competence_assign_tran " & _
                       "    JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                       "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_master.competence_categoryunkid " & _
                       "    JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                       "    JOIN hrassess_group_master ON hrassess_competence_assign_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                       "WHERE hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.isvoid = 0 " & _
                       "    AND hrassess_group_master.assessgroupunkid IN(" & strGroupIds & ") "

                If blnIsSlefAssignCompetencies = True Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & intEmployeeId & "' "
                End If

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCompetenciesData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function GetEmployeeCompetencyResult(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strGroupIds As String, ByVal intAssessorId As Integer, ByVal intReviewerId As Integer, ByVal strWeightAsNumber As String) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation
            StrQ = "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.result END AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " ,hrassess_competencies_master.competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid AND hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.selfemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                   "UNION ALL " & _
                   "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.result END AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " ,hrassess_competencies_master.competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid AND hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = '" & intAssessorId & "' " & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                   "  AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "UNION ALL " & _
                   "SELECT DISTINCT " & _
                   "  hrevaluation_analysis_master.assessmodeid " & _
                   " ,CASE WHEN @WNum = 1 THEN hrassess_competence_assign_tran.weight * hrcompetency_analysis_tran.result ELSE hrcompetency_analysis_tran.result END AS result " & _
                   " ,hrcompetency_analysis_tran.remark " & _
                   " ,hrcompetency_analysis_tran.competenciesunkid " & _
                   " ,hrassess_competencies_master.competence_categoryunkid " & _
                   " ,hrcompetency_analysis_tran.assessgroupunkid " & _
                   "FROM hrcompetency_analysis_tran " & _
                   " JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                   " JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid AND hrassess_competencies_master.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                   " JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.analysisunkid = hrcompetency_analysis_tran.analysisunkid " & _
                   "WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 AND hrevaluation_analysis_master.assessormasterunkid = " & intReviewerId & " AND hrassess_competence_assign_master.assessgroupunkid IN(" & strGroupIds & ")" & _
                   "  AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' AND hrevaluation_analysis_master.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

            objDo.AddParameter("@WNum", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(strWeightAsNumber))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetAppraisalGrades(ByVal decScore As Decimal) As String
        Dim strGrd As String = String.Empty
        Try
            Dim strQ As String = "SELECT @grd = ISNULL(grade_award,'') FROM hrapps_ratings WHERE @total BETWEEN score_from AND score_to AND isvoid = 0 "
            Using objDo As New clsDataOperation
                objDo.AddParameter("@total", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, decScore)
                objDo.AddParameter("@grd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strGrd, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(strQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                strGrd = objDo.GetParameterValue("@grd")

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAppraisalGrades; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strGrd
    End Function

    'S.SANDEEP [04-AUG-2017] -- START
    'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
    Private Function GetEmployee_AssessorReviewerDetails(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strDataBaseName As String, ByVal dtEmpAsOnDate As Date) As DataSet
        Dim dsList As New DataSet
        Dim objEvalMaster As New clsevaluation_analysis_master
        Try
            dsList = objEvalMaster.GetEmployee_AssessorReviewerDetails(intPeriodId, strDataBaseName, dtEmpAsOnDate, intEmployeeId)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_AssessorReviewerDetails; Module Name: " & mstrModuleName)
        Finally
            objEvalMaster = Nothing
        End Try
        Return dsList
    End Function
    'Private Function GetEmployee_AssessorReviewerDetails(ByVal intEmployeeId As Integer, ByVal intPeriodId As Integer, ByVal strDataBaseName As String) As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty
    '    'Shani(18-Feb-2016) -- Start
    '    Dim StrQFinalQry As String = String.Empty
    '    'Dim strActiveEmp As String = String.Empty
    '    'Shani(18-Feb-2016) -- End
    '    Try
    '        Using objDo As New clsDataOperation

    '            'Shani(18-Feb-2016) -- Start
    '            '
    '            'StrQ = "SELECT " & _
    '            '       "     hrassessor_master.assessormasterunkid " & _
    '            '       "    ,hrassessor_master.employeeunkid " & _
    '            '       "    ,hrassessor_master.isreviewer " & _
    '            '       "    ,hremployee_master.firstname+' '+hremployee_master.othername+' '+hremployee_master.surname AS arName " & _
    '            '       "FROM hrassessor_tran " & _
    '            '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
    '            '       "    JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
    '            '       "WHERE hrassessor_tran.employeeunkid = " & intEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 " & _
    '            '       " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            'Dim objAPeriod As New clscommom_period_Tran
    '            'objAPeriod._Periodunkid = intPeriodId

    '            'objDo.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objAPeriod._Start_Date))
    '            'objDo.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objAPeriod._End_Date))
    '            'objAPeriod = Nothing

    '            'dsList = objDo.ExecQuery(StrQ, "List")

    '            'If objDo.ErrorMessage <> "" Then
    '            '    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            'End If

    '            StrQ = "SELECT #DIS# " & _
    '                   "     CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN ASR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
    '                   "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN RVR.assessormasterunkid ELSE hrassessor_master.assessormasterunkid END " & _
    '                   "     END AS assessormasterunkid " & _
    '                   "    ,CASE WHEN hrassessor_master.isreviewer = 0 THEN CASE WHEN ASR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
    '                   "          WHEN hrassessor_master.isreviewer = 1 THEN CASE WHEN RVR.assessormasterunkid > 0 THEN 1 ELSE 0 END " & _
    '                   "     END AS isfound " & _
    '       "    ,hrassessor_master.employeeunkid " & _
    '       "    ,hrassessor_master.isreviewer " & _
    '                   "    ,#EMP_NAME# AS arName " & _
    '                   "    ,#EMP_CODE# AS EmpCode " & _
    '                   "    ,#JOB# AS JOb " & _
    '                   "    ,hrassessor_tran.visibletypeid " & _
    '       "FROM hrassessor_tran " & _
    '       "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
    '                   "    LEFT JOIN hrevaluation_analysis_master ASR ON ASR.assessormasterunkid = hrassessor_master.assessormasterunkid AND ASR.periodunkid = '" & intPeriodId & "' AND ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
    '                   "    LEFT JOIN hrevaluation_analysis_master RVR ON RVR.assessormasterunkid = hrassessor_master.assessormasterunkid AND RVR.periodunkid = '" & intPeriodId & "' AND RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid " & _
    '                   "    #EMP_JOIN# " & _
    '       "WHERE hrassessor_tran.employeeunkid = " & intEmployeeId & " AND hrassessor_tran.isvoid = 0 and hrassessor_master.isvoid = 0 " & _
    '                   "    #CND#  " 'S.SANDEEP [27 DEC 2016] -- START {visibletypeid,ASR.assessedemployeeunkid = hrassessor_tran.employeeunkid,RVR.assessedemployeeunkid = hrassessor_tran.employeeunkid}-- END


    '            StrQFinalQry = StrQ
    '            StrQFinalQry = StrQFinalQry.Replace("#EMP_NAME#", "hremployee_master.firstname+' '+hremployee_master.othername+' '+hremployee_master.surname")
    '            StrQFinalQry = StrQFinalQry.Replace("#EMP_JOIN#", " JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
    '                                                              " JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid ")
    '            StrQFinalQry = StrQFinalQry.Replace("#CND#", " AND hrassessor_master.isexternalapprover = 0 ")
    '            StrQFinalQry = StrQFinalQry.Replace("#EMP_TBL#", "hremployee_master")
    '            StrQFinalQry = StrQFinalQry.Replace("#DIS#", " ")
    '            StrQFinalQry = StrQFinalQry.Replace("#EMP_CODE#", "hremployee_master.employeecode")
    '            StrQFinalQry = StrQFinalQry.Replace("#JOB#", "hrjob_master.job_name")

    '            dsList = objDo.ExecQuery(StrQFinalQry, "List")

    '            If objDo.ErrorMessage <> "" Then
    '    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            End If

    '            'objDo.ClearParameters()

    '            'StrQFinalQry = "SELECT DISTINCT " & _
    '            '               "     cfuser_master.companyunkid " & _
    '            '               "    ,cfuser_master.employeeunkid " & _
    '            '               "    ,cffinancial_year_tran.database_name AS DName " & _
    '            '               "    ,hrassessor_master.assessormasterunkid " & _
    '            '               "FROM hrassessor_master " & _
    '            '               "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = hrassessor_master.employeeunkid AND isactive = 1 " & _
    '            '               "    JOIN hrevaluation_analysis_master ON hrevaluation_analysis_master.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '            '               "    JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND isclosed = 0 " & _
    '            '               "WHERE hrassessor_master.isexternalapprover = 1 " & _
    '            '               "    AND hrevaluation_analysis_master.assessedemployeeunkid = '" & intEmployeeId & "' " & _
    '            '               "    AND hrevaluation_analysis_master.periodunkid = '" & intPeriodId & "' "

    '            'Dim dsExternal As DataSet = objDo.ExecQuery(StrQFinalQry, "List")

    '            'If objDo.ErrorMessage <> "" Then
    '            '    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            'End If
    '            Dim dsExternal As DataSet = (New clsAssessor).GetExternalAssessorReviewerList(objDo, "List", , , intEmployeeId, , clsAssessor.enAssessorType.ASSESSOR)

    '            Dim dsTemp As DataSet
    '            If dsExternal.Tables(0).Rows.Count > 0 Then
    '                For Each dRow As DataRow In dsExternal.Tables(0).Rows
    '                    StrQFinalQry = StrQ

    '                    If dRow("DBName").ToString.Trim.Length <= 0 AndAlso dRow("companyunkid") <= 0 Then
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_NAME#", "ISNULL(UEmp.username,'') ")
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid AND UEmp.isactive = 1")
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_CODE#", "''")
    '                        StrQFinalQry = StrQFinalQry.Replace("#JOB#", "''")
    '                    Else
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_NAME#", "ISNULL(#DName#hremployee_master.firstname,'')+' '+ISNULL(#DName#hremployee_master.surname,'') ")
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
    '                                                                          " JOIN #DName#hremployee_master ON #DName#hremployee_master.employeeunkid = UEmp.employeeunkid " & _
    '                                                                          " JOIN #DName#hrjob_master ON #DName#hrjob_master.jobunkid = #DName#hremployee_master.jobunkid ")
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_TBL#", "#DName#hremployee_master")
    '                        StrQFinalQry = StrQFinalQry.Replace("#EMP_CODE#", "#DName#hremployee_master.employeecode")
    '                        StrQFinalQry = StrQFinalQry.Replace("#JOB#", "#DName#hrjob_master.job_name")
    '                        StrQFinalQry = StrQFinalQry.Replace("#DName#", dRow("DBName") & "..")
    '                    End If
    '                    StrQFinalQry = StrQFinalQry.Replace("#DIS#", "DISTINCT")
    '                    StrQFinalQry = StrQFinalQry.Replace("#CND#", "")
    '                    StrQFinalQry &= " AND UEmp.companyunkid = '" & dRow("companyunkid") & "' "

    '                    dsTemp = objDo.ExecQuery(StrQFinalQry, "List")

    '                    If objDo.ErrorMessage <> "" Then
    '                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '                    End If
    '                    If dsList.Tables.Count <= 0 Then
    '                        dsList.Tables.Add(dsTemp.Tables(0).Copy)
    '                    Else
    '                        dsList.Tables(0).Merge(dsTemp.Tables(0), True)
    '                    End If
    '                Next
    '            End If

    '            If objDo.ErrorMessage <> "" Then
    '    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
    '            End If
    '            'Shani(18-Feb-2016) -- End
    '        End Using
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_AssessorReviewerDetails; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    '    Return dsList
    'End Function
    'S.SANDEEP [04-AUG-2017] -- END

    Public Sub PerformDateOperation(ByVal date1 As DateTime, ByVal date2 As DateTime)
        Try
            Dim monthDay As Integer() = New Integer(11) {31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}


            Dim increment As Integer
            Dim mdtFromDate As DateTime
            Dim mdtToDate As DateTime


            If date1 > date2 Then
                mdtFromDate = date2
                mdtToDate = date1
            Else
                mdtFromDate = date1
                mdtToDate = date2
            End If
            increment = 0
            If mdtFromDate.Day > mdtToDate.Day Then
                increment = monthDay(mdtFromDate.Month - 1)
            End If

            If increment = -1 Then  'FEB MONTH
                If DateTime.IsLeapYear(mdtFromDate.Year) Then
                    ' leap year february contain 29 days
                    increment = 29
                Else
                    increment = 28
                End If
            End If

            '''' DAY CALCULATION '''
            If increment <> 0 Then
                intDay = (mdtToDate.Day + increment) - mdtFromDate.Day
                increment = 1
            Else
                intDay = mdtToDate.Day - mdtFromDate.Day
                'If intDay = 0 Then
                '    intDay = 1
                'End If
            End If

            '''' MONTH CALCULATION '''
            If (mdtFromDate.Month + increment) > mdtToDate.Month Then
                intMonth = (mdtToDate.Month + 12) - (mdtFromDate.Month + increment)
                increment = 1
            Else
                intMonth = (mdtToDate.Month) - (mdtFromDate.Month + increment)
                increment = 0
            End If

            '''' YEAR CALCULATION '''
            intYear = mdtToDate.Year - (mdtFromDate.Year + increment)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformDateOperation; Module Name: " & "clsGetDateDifference")
        Finally
        End Try
    End Sub

    Private Function GetCustomItemDataList(ByVal intPeriodId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation
            StrQ = "SELECT " & _
                   "     @CustomSectionTitles AS Col_CustomItemCategory_Caption " & _
                   "    ,name AS Col_CustomItemCategory_Value " & _
                   "    ,custom_item " & _
                   "    ,customitemunkid " & _
                   "    ,hrassess_custom_items.customheaderunkid " & _
                   "    ,itemtypeid " & _
                   "    ,selectionmodeid " & _
                   "    ,ROW_NUMBER()OVER(PARTITION BY hrassess_custom_items.customheaderunkid ORDER BY hrassess_custom_headers.customheaderunkid) AS xFc " & _
                   "FROM hrassess_custom_items " & _
                   "    JOIN hrassess_custom_headers ON hrassess_custom_headers.customheaderunkid = hrassess_custom_items.customheaderunkid " & _
                   "WHERE hrassess_custom_headers.isactive = 1 AND hrassess_custom_items.isactive = 1 " & _
                   "AND hrassess_custom_items.periodunkid = '" & intPeriodId & "' "

            objDo.AddParameter("@CustomSectionTitles", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "Custom Section Titles"))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetCustomItemResult(ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Using objDo As New clsDataOperation

            StrQ = "SELECT " & _
                   "     customitemunkid AS Cid " & _
                   "    ,custom_value AS cvalue " & _
                   "    ,hrcompetency_customitem_tran.analysisunkid " & _
                   "    ,assessmodeid AS ModeId " & _
                   "    ,SUM(1)OVER(PARTITION BY customitemunkid) AS xCnt " & _
                   "FROM hrcompetency_customitem_tran " & _
                   "    JOIN hrevaluation_analysis_master ON hrcompetency_customitem_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                   "WHERE hrcompetency_customitem_tran.isvoid = 0 AND employeeunkid = '" & intEmployeeId & "' AND hrcompetency_customitem_tran.periodunkid = '" & intPeriodId & "' "

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If

        End Using
        Return dsList
    End Function

    Private Function GetAppraisarComments(ByVal intPeridId As Integer, ByVal intEmployeeId As Integer) As String
        Dim dsCusHeaders As New DataSet : Dim dsDetails As New DataSet
        Dim strCommentsGiven As String = String.Empty
        Try
            dsCusHeaders = GetCustomItemDataList(intPeridId)
            dsDetails = GetCustomItemResult(intPeridId, intEmployeeId)
            Dim iCustomId, iNewCustomId As String : iCustomId = "" : iNewCustomId = ""

            For Each xRow As DataRow In dsCusHeaders.Tables(0).Rows
                iCustomId = xRow.Item("Col_CustomItemCategory_Value")
                If iCustomId <> iNewCustomId Then
                    iNewCustomId = xRow.Item("Col_CustomItemCategory_Value")
                End If
                Dim dval() As DataRow = dsDetails.Tables(0).Select("Cid = '" & xRow("customitemunkid") & "'")
                If dval.Length > 0 Then
                    For intRowIndex As Integer = 0 To dval.Length - 1
                        Select Case CInt(xRow.Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                If dval(intRowIndex).Item("xCnt") > 1 Then
                                    Select Case dval(intRowIndex).Item("ModeId")
                                        Case enAssessmentMode.SELF_ASSESSMENT
                                            '    strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                                            strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & dval(intRowIndex).Item("cvalue") & "<br>"
                                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                                            '    strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & dval(intRowIndex).Item("cvalue") & vbCrLf
                                    End Select
                                Else
                                    strCommentsGiven &= dval(intRowIndex).Item("cvalue") & "<br>"
                                End If
                            Case clsassess_custom_items.enCustomType.SELECTION
                                If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                    Select Case CInt(xRow.Item("selectionmodeid"))
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                            If dval(intRowIndex).Item("xCnt") > 1 Then
                                                Select Case dval(intRowIndex).Item("ModeId")
                                                    Case enAssessmentMode.SELF_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & objCMaster._Name & vbCrLf
                                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                        strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & objCMaster._Name & "<br>"
                                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & objCMaster._Name & vbCrLf
                                                End Select
                                            Else
                                                strCommentsGiven &= objCMaster._Name & "<br>"
                                            End If
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                            If dval(intRowIndex).Item("xCnt") > 1 Then
                                                Select Case dval(intRowIndex).Item("ModeId")
                                                    Case enAssessmentMode.SELF_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & objCMaster._Name & vbCrLf
                                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                        strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & objCMaster._Name & "<br>"
                                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & objCMaster._Name & vbCrLf
                                                End Select
                                            Else
                                                strCommentsGiven &= objCMaster._Name & "<br>"
                                            End If
                                            objCMaster = Nothing
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            Dim objEmpField1 As New clsassess_empfield1_master
                                            objEmpField1._Empfield1unkid = CInt(dval(intRowIndex).Item("cvalue"))
                                            If dval(intRowIndex).Item("xCnt") > 1 Then
                                                Select Case dval(intRowIndex).Item("ModeId")
                                                    Case enAssessmentMode.SELF_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & objEmpField1._Field_Data & vbCrLf
                                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                        strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & objEmpField1._Field_Data & "<br>"
                                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                        'strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & objEmpField1._Field_Data & vbCrLf
                                                End Select
                                            Else
                                                strCommentsGiven &= objEmpField1._Field_Data & "<br>"
                                            End If
                                            objEmpField1 = Nothing

                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dval(intRowIndex).Item("cvalue"))
                                            If dval(intRowIndex).Item("xCnt") > 1 Then
                                                Select Case dval(intRowIndex).Item("ModeId")
                                                    Case enAssessmentMode.SELF_ASSESSMENT
                                                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                        strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & objCMaster._Name & "<br>"
                                                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                End Select
                                            Else
                                                strCommentsGiven &= objCMaster._Name & "<br>"
                                            End If
                                            objCMaster = Nothing
                                            'S.SANDEEP |16-AUG-2019| -- END

                                    End Select
                                End If
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                    If dval(intRowIndex).Item("xCnt") > 1 Then
                                        Select Case dval(intRowIndex).Item("ModeId")
                                            Case enAssessmentMode.SELF_ASSESSMENT
                                                'strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & "<br>"
                                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                'strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & vbCrLf
                                        End Select
                                    Else
                                        strCommentsGiven &= eZeeDate.convertDate(dval(intRowIndex).Item("cvalue").ToString).ToShortDateString & "<br>"
                                    End If
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                If dval(intRowIndex).Item("cvalue").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dval(intRowIndex).Item("cvalue")) Then
                                        If dval(intRowIndex).Item("xCnt") > 1 Then
                                            Select Case dval(intRowIndex).Item("ModeId")
                                                Case enAssessmentMode.SELF_ASSESSMENT
                                                    'strCommentsGiven &= Language.getMessage(mstrModuleName, 24, "Self") & " : " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                                Case enAssessmentMode.APPRAISER_ASSESSMENT
                                                    strCommentsGiven &= Language.getMessage(mstrModuleName, 25, "Assessor") & " : " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & "<br>"
                                                Case enAssessmentMode.REVIEWER_ASSESSMENT
                                                    'strCommentsGiven &= Language.getMessage(mstrModuleName, 26, "Reviewer") & " : " & CDbl(dval(intRowIndex).Item("cvalue")).ToString & vbCrLf
                                            End Select
                                        Else
                                            strCommentsGiven &= CDbl(dval(intRowIndex).Item("cvalue")).ToString & "<br>"
                                        End If
                                    End If
                                End If
                        End Select
                    Next
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAppraisarComments; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strCommentsGiven.Replace("<br>", "")
    End Function
    'S.SANDEEP [19 NOV 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Department")
            Language.setMessage(mstrModuleName, 2, "Total Employee Above 1 Year of Service")
            Language.setMessage(mstrModuleName, 3, "Total Received")
            Language.setMessage(mstrModuleName, 4, "%")
            Language.setMessage(mstrModuleName, 5, "Not Submitted")
            Language.setMessage(mstrModuleName, 6, "Salary Increase By %")
            Language.setMessage(mstrModuleName, 7, "Commendable Letters")
            Language.setMessage(mstrModuleName, 8, "Letter of Improvement")
            Language.setMessage(mstrModuleName, 9, "Warning")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")
            Language.setMessage(mstrModuleName, 12, "Page :")
            Language.setMessage(mstrModuleName, 13, "SUMMARY FOR APPRAISAL RESULT AWARDS FOR THE YEAR :")
            Language.setMessage(mstrModuleName, 14, "Prepared By :")
            Language.setMessage(mstrModuleName, 15, "Checked By :")
            Language.setMessage(mstrModuleName, 16, "Approved By :")
            Language.setMessage(mstrModuleName, 17, "Received By :")
            Language.setMessage(mstrModuleName, 18, "Department :")
            Language.setMessage(mstrModuleName, 19, "Commendable Letters From :")
            Language.setMessage(mstrModuleName, 20, "Commendable Letters To :")
            Language.setMessage(mstrModuleName, 21, "Letters of Improvement From :")
            Language.setMessage(mstrModuleName, 22, "Letters of Improvement To :")
            Language.setMessage(mstrModuleName, 23, "Warning From :")
            Language.setMessage(mstrModuleName, 24, "Warning To :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Drawing.Printing

#End Region

Public Class frmPerformanceAnalyis_Chart

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmPerformanceEvaluationReport"   'PLEASE DO NOT CHANGE THIS NAME (ITS THE NAME OF PARENT FORM)
    Private dsDataSet As New DataSet
    Private mstrChartTitle As String = String.Empty
    Private mCheckedItems As ListView.CheckedListViewItemCollection
    Private mColHdr As String = String.Empty

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private mblnIsAnalysisChart As Boolean = False
    'S.SANDEEP |27-JUL-2019| -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _DataSet() As DataSet
        Set(ByVal value As DataSet)
            dsDataSet = value
        End Set
    End Property

    Public WriteOnly Property _ChartTitle() As String
        Set(ByVal value As String)
            mstrChartTitle = value
        End Set
    End Property

    Public WriteOnly Property _CheckedItems() As ListView.CheckedListViewItemCollection
        Set(ByVal value As ListView.CheckedListViewItemCollection)
            mCheckedItems = value
        End Set
    End Property

    Public WriteOnly Property _ColHdr() As String
        Set(ByVal value As String)
            mColHdr = value
        End Set
    End Property

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Public WriteOnly Property _IsAnalysisChart() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAnalysisChart = value
        End Set
    End Property
    'S.SANDEEP |27-JUL-2019| -- END

#End Region

#Region " Form's Events "

    Private Sub frmPerformanceAnalyis_Chart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Call SetMessages()
            Call OtherSettings()
            'S.SANDEEP |26-AUG-2019| -- END

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If mblnIsAnalysisChart Then
                pnlMain.Visible = True
                objspcChart2.Visible = False
                pnlMain.Dock = DockStyle.Fill
            Else
                pnlMain.Visible = False
                objspcChart2.Visible = True
                objspcChart2.Dock = DockStyle.Fill
            End If
            'S.SANDEEP |27-JUL-2019| -- END
            Call Fill_List()
            Call Fill_Chart()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPerformanceAnalyis_Chart_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_List()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'colhAllocation.Text = mColHdr
            'For Each mItem As ListViewItem In mCheckedItems
            '    Dim lvItem As New ListViewItem
            '    lvItem.Text = mItem.Text
            '    lvAllocation.Items.Add(lvItem)
            'Next
            If mblnIsAnalysisChart Then
            colhAllocation.Text = mColHdr
            For Each mItem As ListViewItem In mCheckedItems
                Dim lvItem As New ListViewItem
                lvItem.Text = mItem.Text
                lvAllocation.Items.Add(lvItem)
            Next
            Else
                dgvData.DataSource = dsDataSet.Tables("List")
                dgvData.ColumnHeadersVisible = False
                If dgvData.ColumnCount > 0 Then
                    dgvData.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    'S.SANDEEP |26-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    dgvData.Columns("objColRowType").Visible = False
                    'S.SANDEEP |26-AUG-2019| -- END
                End If
                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                If dgvData.RowCount > 0 Then
                    Dim gvr As DataGridViewRow = Nothing
                    gvr = dgvData.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells("objColRowType").Value.ToString() = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.DefaultCellStyle.ForeColor = Color.Blue
                    End If

                    gvr = dgvData.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) x.Cells("objColRowType").Value.ToString() = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.DefaultCellStyle.ForeColor = Color.Tomato
                End If
            End If
                'S.SANDEEP |26-AUG-2019| -- END
            End If
            'S.SANDEEP |27-JUL-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Chart()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'chAnalysis_Chart.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis1")
            'chAnalysis_Chart.Series(1).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis2")

            'chAnalysis_Chart.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 7, "Scores")
            'chAnalysis_Chart.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 8, "Number Of Staff Assessed")
            'chAnalysis_Chart.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            'chAnalysis_Chart.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            'chAnalysis_Chart.ChartAreas(0).AxisY2.LabelStyle.Format = "{0.##}%"
            'chAnalysis_Chart.Titles(0).Text = mstrChartTitle
            'chAnalysis_Chart.Titles(0).Font = New Font(Me.Font.Name, 11, FontStyle.Bold)
            'chAnalysis_Chart.Series(0).IsValueShownAsLabel = True
            'chAnalysis_Chart.Series(1).IsValueShownAsLabel = True
            'chAnalysis_Chart.Series(1).LabelFormat = "{0.##}%"

            'chAnalysis_Chart.Series(0).IsVisibleInLegend = False
            'chAnalysis_Chart.Series(1).IsVisibleInLegend = False

            'chAnalysis_Chart.Series(0).Color = Color.SeaGreen
            'chAnalysis_Chart.Series(1).Color = Color.Tomato


            'Dim legendItem1 As New LegendItem()
            'legendItem1.Name = Language.getMessage(mstrModuleName, 5, "# Of Emplyoee")
            'legendItem1.ImageStyle = LegendImageStyle.Rectangle
            'legendItem1.ShadowOffset = 1
            'legendItem1.Color = chAnalysis_Chart.Series(0).Color
            'chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem1)

            'Dim legendItem2 As New LegendItem()
            'legendItem2.Name = Language.getMessage(mstrModuleName, 6, "%")
            'legendItem2.ImageStyle = LegendImageStyle.Rectangle
            'legendItem2.ShadowOffset = 1
            'legendItem2.Color = chAnalysis_Chart.Series(1).Color
            'chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem2)

            'chAnalysis_Chart.Series(0).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(0).Name & " : is #VAL "
            'chAnalysis_Chart.Series(1).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(1).Name & " : is #VAL "


            'For i As Integer = 0 To chAnalysis_Chart.Series.Count - 1
            '    For Each dp As DataPoint In chAnalysis_Chart.Series(i).Points
            '        If dp.YValues(0) = 0 Then
            '            dp.Label = " "
            '        End If
            '    Next
            'Next

            If mblnIsAnalysisChart Then
            chAnalysis_Chart.Series(0).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis1")
            chAnalysis_Chart.Series(1).Points.DataBindXY(dsDataSet.Tables(0).DefaultView, "xAxis", dsDataSet.Tables(0).DefaultView, "yAxis2")

            chAnalysis_Chart.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 7, "Scores")
            chAnalysis_Chart.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 8, "Number Of Staff Assessed")
            chAnalysis_Chart.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            chAnalysis_Chart.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
            chAnalysis_Chart.ChartAreas(0).AxisY2.LabelStyle.Format = "{0.##}%"
            chAnalysis_Chart.Titles(0).Text = mstrChartTitle
            chAnalysis_Chart.Titles(0).Font = New Font(Me.Font.Name, 11, FontStyle.Bold)
            chAnalysis_Chart.Series(0).IsValueShownAsLabel = True
            chAnalysis_Chart.Series(1).IsValueShownAsLabel = True
            chAnalysis_Chart.Series(1).LabelFormat = "{0.##}%"

            chAnalysis_Chart.Series(0).IsVisibleInLegend = False
            chAnalysis_Chart.Series(1).IsVisibleInLegend = False

            chAnalysis_Chart.Series(0).Color = Color.SeaGreen
            chAnalysis_Chart.Series(1).Color = Color.Tomato


            Dim legendItem1 As New LegendItem()
                legendItem1.Name = Language.getMessage(mstrModuleName, 901, "# Of Emplyoee")
            legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
            legendItem1.Color = chAnalysis_Chart.Series(0).Color
            chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem1)

            Dim legendItem2 As New LegendItem()
            legendItem2.Name = Language.getMessage(mstrModuleName, 6, "%")
            legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
            legendItem2.Color = chAnalysis_Chart.Series(1).Color
            chAnalysis_Chart.Legends(0).CustomItems.Add(legendItem2)

            chAnalysis_Chart.Series(0).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(0).Name & " : is #VAL "
            chAnalysis_Chart.Series(1).ToolTip = chAnalysis_Chart.Legends(0).CustomItems(1).Name & " : is #VAL "


            For i As Integer = 0 To chAnalysis_Chart.Series.Count - 1
                For Each dp As DataPoint In chAnalysis_Chart.Series(i).Points
                    If dp.YValues(0) = 0 Then
                        dp.Label = " "                        
                    End If
                Next
            Next
            Else
                chHpoCurve.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 902, "Number Of Employees (%)")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = mstrChartTitle

                chHpoCurve.Titles(0).Font = New Font(Me.Font.Name, 11, FontStyle.Bold)
                'Chart1.Series(0).IsValueShownAsLabel = True
                'Chart1.Series(1).IsValueShownAsLabel = True
                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Language.getMessage(mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next
            End If
            'S.SANDEEP |27-JUL-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Chart", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
            Me.chAnalysis_Chart.Text = Language._Object.getCaption(Me.chAnalysis_Chart.Name, Me.chAnalysis_Chart.Text)
            Me.chHpoCurve.Text = Language._Object.getCaption(Me.chHpoCurve.Name, Me.chHpoCurve.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 6, "%")
            Language.setMessage(mstrModuleName, 7, "Scores")
            Language.setMessage(mstrModuleName, 8, "Number Of Staff Assessed")
            Language.setMessage(mstrModuleName, 100, "Ratings")
            Language.setMessage(mstrModuleName, 102, "Normal Distribution")
            Language.setMessage(mstrModuleName, 103, "Actual Performance")
            Language.setMessage(mstrModuleName, 901, "# Of Emplyoee")
            Language.setMessage(mstrModuleName, 902, "Number Of Employees (%)")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'************************************************************************************************************************************
'Class Name : clsAssetDeclarationCategoryWise.vb
'Purpose    :
'Date       :08/02/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsAssetDeclarationCategoryWise
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssetDeclarationCategoryWise"
    Private mstrReportId As String = enArutiReport.Asset_Declaration_Category_Wise
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
        Call Create_OnDetailReport_WithSalary()
    End Sub
#End Region

#Region " Private variables "
    Private mdtAsOnDate As DateTime = Nothing
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mblnIncluderInactiveEmp As Boolean = False
    Private mintAppointmentTypeId As Integer = 0
    Private mstrAppointmentTypeName As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mintDeclareOperatorId As Integer
    Private mstrDeclareOperatorName As String
    Private mdecDeclareAmtFrom As Decimal = 0
    Private mdecDeclareAmtTo As Decimal = 0
    Private mintLiabOperatorId As Integer
    Private mstrLiabOperatorName As String
    Private mdecLiabAmtFrom As Decimal = 0
    Private mdecLiabAmtTo As Decimal = 0
    Private mintAssetCategoryId As Integer
    Private mstrAssetCategoryName As String
    Private mintADCatOperatorId As Integer
    Private mstrADCatOperatorName As String
    Private mdecADCatAmtFrom As Decimal
    Private mdecADCatAmtTo As Decimal
    Private mblnShowSalary As Boolean = False

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    Private mstrDeclareValueQuery As String = ""

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END


#End Region

#Region " Properties "
    Public WriteOnly Property _AsOnDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAsOnDate = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _IncluderInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncluderInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AppointmentTypeName() As String
        Set(ByVal value As String)
            mstrAppointmentTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _DeclareOperatorId() As Integer
        Set(ByVal value As Integer)
            mintDeclareOperatorId = value
        End Set
    End Property

    Public WriteOnly Property _DeclareOperatorName() As String
        Set(ByVal value As String)
            mstrDeclareOperatorName = value
        End Set
    End Property

    Public WriteOnly Property _DeclareAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecDeclareAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _DeclareAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecDeclareAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _LiabOperatorId() As Integer
        Set(ByVal value As Integer)
            mintLiabOperatorId = value
        End Set
    End Property

    Public WriteOnly Property _LiabOperatorName() As String
        Set(ByVal value As String)
            mstrLiabOperatorName = value
        End Set
    End Property

    Public WriteOnly Property _LiabAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecLiabAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _LiabAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecLiabAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _AssetCategoryId() As Integer
        Set(ByVal value As Integer)
            mintAssetCategoryId = value
        End Set
    End Property

    Public WriteOnly Property _AssetCategoryName() As String
        Set(ByVal value As String)
            mstrAssetCategoryName = value
        End Set
    End Property

    Public WriteOnly Property _ADCatOperatorId() As Integer
        Set(ByVal value As Integer)
            mintADCatOperatorId = value
        End Set
    End Property

    Public WriteOnly Property _ADCatOperatorName() As String
        Set(ByVal value As String)
            mstrADCatOperatorName = value
        End Set
    End Property

    Public WriteOnly Property _ADCatAmtFrom() As Decimal
        Set(ByVal value As Decimal)
            mdecADCatAmtFrom = value
        End Set
    End Property

    Public WriteOnly Property _ADCatAmtTo() As Decimal
        Set(ByVal value As Decimal)
            mdecADCatAmtTo = value
        End Set
    End Property

    Public WriteOnly Property _ShowSalary() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSalary = value
        End Set
    End Property


    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region



#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtAsOnDate = Nothing
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mblnIncluderInactiveEmp = False
            mintAppointmentTypeId = 0
            mstrAppointmentTypeName = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mintDeclareOperatorId = -1
            mstrDeclareOperatorName = ""
            mdecDeclareAmtFrom = -1
            mdecDeclareAmtTo = -1
            mintLiabOperatorId = -1
            mstrLiabOperatorName = ""
            mdecLiabAmtFrom = -1
            mdecLiabAmtTo = -1
            mintADCatOperatorId = -1
            mstrADCatOperatorName = ""
            mdecADCatAmtFrom = -1
            mdecADCatAmtTo = -1
            mblnShowSalary = False
            mstrUserAccessFilter = ""

            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GroupName = ""
            mstrReport_GroupName = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Dim strDeclareValueQuery As String = ""
        Dim strLiabValueQuery As String = ""
        Dim strADCatQuery As String = ""
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'objDataOperation.AddParameter("@SalDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime).ToString)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrAdvance_Filter.Trim.Length > 0 Then
                Me._FilterQuery &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [ 13 FEB 2013 ] -- END
            objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsOnDate).ToString)
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "As On Date : ") & " " & mdtAsOnDate.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "Employee : ") & " " & mstrEmployeeName & " "
            End If


            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN @Date1 AND @Date2 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 35, "Appointed Date From : ") & " " & mdtDate1.ToShortDateString & " " & _
                                           Language.getMessage(mstrModuleName, 36, "To : ") & " " & mdtDate2.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < @Date1 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 37, "Appointed Before Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate1).ToString)
                        Me._FilterQuery &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > @Date1 "
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 38, "Appointed After Date : ") & " " & mdtDate1.ToShortDateString & " "
                    End If
            End Select


            '****   DECLARED AMOUNT   *****
            'If mintDeclareOperatorId > 0 Then
            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.CURRENT_ASSETS Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.banktotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.SHARE_AND_DIVIDENDS Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.sharedividendtotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.HOUSES_AND_BUILDINGS Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.housetotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.PARK_FARMS_AND_MINES Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.parkfarmtotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.TRANSPORT_EQUIPMENTS Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.vehicletotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.MACHINERIES_AND_INDUSTRIES Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.machinerytotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.OTHER_BUSINESSES Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.otherbusinesstotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.OTHER_COMMERCIAL_INTERESTS Then
                strDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.otherresourcetotal, 0) "
            End If

            If mintDeclareOperatorId > 0 Then
                Me._FilterQuery &= " AND " & strDeclareValueQuery.Substring(2) & " " & mstrDeclareOperatorName & " " & " @DeclareAmtFrom "
                objDataOperation.AddParameter("@DeclareAmtFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDeclareAmtFrom)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 40, "Estimated Declared Value") & " " & mstrDeclareOperatorName & " " & mdecDeclareAmtFrom & " "

                If mintDeclareOperatorId = enComparison_Operator.BETWEEN Then
                    Me._FilterQuery &= " AND @DeclareAmtTo "
                    objDataOperation.AddParameter("@DeclareAmtTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDeclareAmtTo)
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "And") & " " & mdecDeclareAmtTo & " "
                End If
            End If
            'End If


            '****   LIABILITY AMOUNT   *****
            If mintLiabOperatorId > 0 Then
                'If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS Then
                strLiabValueQuery &= " ISNULL(hrassetdeclaration_master.debttotal, 0) "
                'End If
                Me._FilterQuery &= " AND " & strLiabValueQuery & " " & mstrLiabOperatorName & " " & " @LiabAmtFrom "
                objDataOperation.AddParameter("@LiabAmtFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLiabAmtFrom)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 41, "Estimated Debt Value") & " " & mstrLiabOperatorName & " " & mdecLiabAmtFrom & " "

                If mintLiabOperatorId = enComparison_Operator.BETWEEN Then
                    Me._FilterQuery &= " AND @LiabAmtTo "
                    objDataOperation.AddParameter("@LiabAmtTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLiabAmtTo)
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "And") & " " & mdecLiabAmtTo & " "
                End If
            End If


            '****   ASSET CATEGORY AMOUNT   *****
            If mintAssetCategoryId > 0 Then

                If mintAssetCategoryId = enAD_Categories.CURRENT_ASSETS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.banktotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.SHARE_AND_DIVIDENDS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.sharedividendtotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.HOUSES_AND_BUILDINGS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.housetotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.PARK_FARMS_AND_MINES Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.parkfarmtotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.TRANSPORT_EQUIPMENTS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.vehicletotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.MACHINERIES_AND_INDUSTRIES Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.machinerytotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.OTHER_BUSINESSES Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.otherbusinesstotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.OTHER_COMMERCIAL_INTERESTS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.otherresourcetotal, 0) "
                End If

                If mintAssetCategoryId = enAD_Categories.DEBTS Then
                    strADCatQuery = " AND ISNULL(hrassetdeclaration_master.debttotal, 0) "
                End If

                If mintADCatOperatorId <= 0 Then
                    Me._FilterTitle &= Language.getMessage(mstrModuleName, 53, " Asset Category : ") & mstrAssetCategoryName
                ElseIf mintADCatOperatorId > 0 Then
                    Me._FilterQuery &= strADCatQuery & " " & mstrADCatOperatorName & " @ADCatAmtFrom "
                    objDataOperation.AddParameter("@ADCatAmtFrom", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecADCatAmtFrom)
                    Me._FilterTitle &= mstrAssetCategoryName & " " & Language.getMessage(mstrModuleName, 54, "Value") & " " & mstrADCatOperatorName & " " & mdecADCatAmtFrom & " "

                    If mintADCatOperatorId = enComparison_Operator.BETWEEN Then
                        Me._FilterQuery &= " AND @ADCatAmtTo "
                        objDataOperation.AddParameter("@ADCatAmtTo", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecADCatAmtTo)
                        Me._FilterTitle &= Language.getMessage(mstrModuleName, 39, "And") & " " & mdecADCatAmtTo & " "
                    End If
                End If
            Else
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 55, " Asset Category : ALL")
            End If

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 42, "Order By :") & " " & Me.OrderByDisplay & " "
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= "ORDER BY " & mstrAnalysis_OrderBy_GroupName & "," & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            If intReportType = 0 Then
                OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
            Else
                OrderByDisplay = iColumn_DetailReport_WithSalary.ColumnItem(0).DisplayName
                OrderByQuery = iColumn_DetailReport_WithSalary.ColumnItem(0).Name
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            If intReportType = 0 Then
                Call OrderByExecute(iColumn_DetailReport)
            Else
                Call OrderByExecute(iColumn_DetailReport_WithSalary)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection
    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END
            iColumn_DetailReport.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 45, "Appointed Date")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            iColumn_DetailReport.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                                                "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                                                "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                                                "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 50, "Declared Amount")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hrassetdeclaration_master.debttotal, 0)", Language.getMessage(mstrModuleName, 51, "Liability Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Dim iColumn_DetailReport_WithSalary As New IColumnCollection
    Public Property Field_OnDetailReport_WithSalary() As IColumnCollection
        Get
            Return iColumn_DetailReport_WithSalary
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport_WithSalary = value
        End Set
    End Property

    Private Sub Create_OnDetailReport_WithSalary()
        Try
            iColumn_DetailReport_WithSalary.Clear()
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 43, "Employee Code")))
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            'iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(hremployee_master.surname,'') + ' '+ ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            Else
                iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(hremployee_master.firstname,'') + ' '+ ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 44, "Employee Name")))
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_DetailReport_WithSalary.Add(New IColumn("hremployee_master.appointeddate", Language.getMessage(mstrModuleName, 45, "Appointed Date")))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(Dept.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            'iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(JBM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            'iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(ClsG.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            'iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(Cls.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(DM.name,'')", Language.getMessage(mstrModuleName, 46, "Department")))
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(JM.job_name,'')", Language.getMessage(mstrModuleName, 47, "Job Title")))
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(CGM.name,'')", Language.getMessage(mstrModuleName, 48, "Region")))
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(CM.name,'')", Language.getMessage(mstrModuleName, 49, "WorkStation")))

            'Shani(24-Aug-2015) -- End

            iColumn_DetailReport_WithSalary.Add(New IColumn("(ISNULL(banktotal,0)+ISNULL(sharedividendtotal,0)+ " & _
                                                "  ISNULL(housetotal,0)+ISNULL(parkfarmtotal,0)+ " & _
                                                "  ISNULL(vehicletotal,0)+ISNULL(machinerytotal,0)+ " & _
                                                "  ISNULL(otherbusinesstotal,0)+ISNULL(otherresourcetotal,0))", Language.getMessage(mstrModuleName, 50, "Declared Amount")))
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(hrassetdeclaration_master.debttotal, 0)", Language.getMessage(mstrModuleName, 51, "Liability Amount")))
            iColumn_DetailReport_WithSalary.Add(New IColumn("ISNULL(Salary,0)", Language.getMessage(mstrModuleName, 52, "Salary")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport_WithSalary; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Shani(24-Aug-2015) -- End
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            mstrDeclareValueQuery = ""
            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.CURRENT_ASSETS Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.banktotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.SHARE_AND_DIVIDENDS Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.sharedividendtotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.HOUSES_AND_BUILDINGS Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.housetotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.PARK_FARMS_AND_MINES Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.parkfarmtotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.TRANSPORT_EQUIPMENTS Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.vehicletotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.MACHINERIES_AND_INDUSTRIES Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.machinerytotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.OTHER_BUSINESSES Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.otherbusinesstotal, 0) "
            End If

            If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS OrElse mintAssetCategoryId = enAD_Categories.OTHER_COMMERCIAL_INTERESTS Then
                mstrDeclareValueQuery &= " + ISNULL(hrassetdeclaration_master.otherresourcetotal, 0) "
            End If

            mstrDeclareValueQuery = mstrDeclareValueQuery.Substring(2)

            StrQ = "SELECT hrassetdeclaration_master.assetdeclarationunkid " & _
                          ",hrassetdeclaration_master.employeeunkid " & _
                          ", hremployee_master.employeecode "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS employeename "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS employeename "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= ", ISNULL(CONVERT(CHAR(8), appointeddate, 112), '') AS App_Date " & _
            '              ", ISNULL(Dept.name,'') AS Department " & _
            '              ", ISNULL(JBM.job_name,'') AS Job_Title " & _
            '              ", ISNULL(ClsG.name,'') AS Region " & _
            '              ", ISNULL(Cls.name,'') AS WorkStation "

            StrQ &= ", ISNULL(CONVERT(CHAR(8), appointeddate, 112), '') AS App_Date " & _
                          ", ISNULL(DM.name,'') AS Department " & _
                          ", ISNULL(JM.job_name,'') AS Job_Title " & _
                          ", ISNULL(CGM.name,'') AS Region " & _
                          ", ISNULL(CM.name,'') AS WorkStation "
            'Shani(24-Aug-2015) -- End

            If mblnShowSalary = True Then
                StrQ &= ",ISNULL(Salary,0) AS Salary "
            Else
                StrQ &= ",0 AS Salary "
            End If

            StrQ &= "     , hrassetdeclaration_master.cash " & _
                          ", hrassetdeclaration_master.cashexistingbank " & _
                          ", hrassetdeclaration_master.finyear_start " & _
                          ", hrassetdeclaration_master.finyear_end " & _
                          ", hrassetdeclaration_master.resources " & _
                          ", hrassetdeclaration_master.debt " & _
                          ", hrassetdeclaration_master.isfinalsaved " & _
                          ", hrassetdeclaration_master.userunkid " & _
                          ", hrassetdeclaration_master.isvoid " & _
                          ", hrassetdeclaration_master.voiduserunkid " & _
                          ", hrassetdeclaration_master.voiddatetime " & _
                          ", hrassetdeclaration_master.voidreason " & _
                          ", hrassetdeclaration_master.loginemployeeunkid " & _
                          ", hrassetdeclaration_master.voidloginemployeeunkid " & _
                          ", hrassetdeclaration_master.transactiondate " & _
                          ", hrassetdeclaration_master.savedate " & _
                          ", hrassetdeclaration_master.finalsavedate " & _
                          ", hrassetdeclaration_master.unlockfinalsavedate " & _
                          ", hrassetdeclaration_master.banktotal " & _
                          ", hrassetdeclaration_master.sharedividendtotal " & _
                          ", hrassetdeclaration_master.housetotal " & _
                          ", hrassetdeclaration_master.parkfarmtotal " & _
                          ", hrassetdeclaration_master.vehicletotal " & _
                          ", hrassetdeclaration_master.machinerytotal " & _
                          ", hrassetdeclaration_master.otherbusinesstotal " & _
                          ", hrassetdeclaration_master.otherresourcetotal " & _
                          ", hrassetdeclaration_master.debttotal " & _
                          ", hrassetdeclaration_master.debttotal AS EmpTotalLiabilities "

            If mstrDeclareValueQuery.Trim <> "" Then
                StrQ &= ", " & mstrDeclareValueQuery & " AS TotalDeclaredValue "
            End If

            StrQ &= ", ISNULL(hrassetdeclaration_master.debttotal, 0) AS TotalDebtValue "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ &= "       FROM   hrassetdeclaration_master " & _
            '                "JOIN hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid " & _
            '                "LEFT JOIN hrjob_master AS JBM ON hremployee_master.jobunkid = JBM.jobunkid " & _
            '                "LEFT JOIN hrdepartment_master AS Dept ON Dept.departmentunkid = hremployee_master.departmentunkid " & _
            '                "LEFT JOIN hrclasses_master AS Cls ON hremployee_master.classunkid = Cls.classesunkid " & _
            '                "LEFT JOIN hrclassgroup_master AS ClsG ON hremployee_master.classgroupunkid = ClsG.classgroupunkid "

            'StrQ &= mstrAnalysis_Join

            StrQ &= "       FROM   hrassetdeclaration_master " & _
                            "JOIN hremployee_master ON hrassetdeclaration_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "      ( " & _
                    "             SELECT " & _
                    "              departmentunkid " & _
                    "              ,classgroupunkid " & _
                    "              ,classunkid " & _
                    "              ,employeeunkid " & _
                    "              ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "          FROM hremployee_transfer_tran " & _
                    "          WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "      ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "   JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                    "   LEFT JOIN hrclassgroup_master AS CGM ON Alloc.classgroupunkid = CGM.classgroupunkid " & _
                    "   LEFT JOIN hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                    "   LEFT JOIN " & _
                    "      ( " & _
                    "          SELECT " & _
                    "              jobunkid " & _
                    "              ,jobgroupunkid " & _
                    "              ,employeeunkid " & _
                    "              ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "          FROM hremployee_categorization_tran " & _
                    "          WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "      ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "   JOIN hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

            'Shani(24-Aug-2015) -- End

            If mblnShowSalary = True Then
                StrQ &= "   LEFT JOIN ( SELECT  Eid " & _
                                              ", Salary " & _
                                        "FROM    ( SELECT    employeeunkid AS Eid " & _
                                                          ", ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                                          ", newscale AS Salary " & _
                                                  "FROM      prsalaryincrement_tran " & _
                                                  "WHERE     incrementdate < = @AsOnDate " & _
                                                            "AND isapproved = 1 " & _
                                                            "AND isvoid = 0 " & _
                                                ") AS A " & _
                                        "WHERE   A.Rno = 1 " & _
                                      ") AS B ON B.Eid = hremployee_master.employeeunkid "
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Shani(24-Aug-2015) -- End

            StrQ &= "WHERE   ISNULL(hrassetdeclaration_master.isvoid, 0) = 0 " & _
                    "AND hrassetdeclaration_master.isfinalsaved = 1 "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mblnIncluderInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncluderInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If


            'Shani(24-Aug-2015) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then

                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim iCnt As Integer = 1
            Dim decSalGrpTotal As Decimal = 0
            Dim decDeclareGrpTotal As Decimal = 0
            Dim decDebtGrpTotal As Decimal = 0
            Dim decSalTotal As Decimal = 0
            Dim decDeclareTotal As Decimal = 0
            Dim decDebtTotal As Decimal = 0
            Dim intPrevId As Integer = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("employeename")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("App_Date").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = dtRow.Item("Job_Title")
                rpt_Rows.Item("Column5") = dtRow.Item("Department")
                rpt_Rows.Item("Column6") = dtRow.Item("Region")
                rpt_Rows.Item("Column7") = dtRow.Item("WorkStation")
                rpt_Rows.Item("Column8") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency)
                rpt_Rows.Item("Column10") = Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency)
                rpt_Rows.Item("Column11") = iCnt.ToString
                rpt_Rows.Item("Column12") = dtRow.Item("GName")
                rpt_Rows.Item("Column13") = Format(CDec(dtRow.Item("banktotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column14") = Format(CDec(dtRow.Item("sharedividendtotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column15") = Format(CDec(dtRow.Item("housetotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column16") = Format(CDec(dtRow.Item("parkfarmtotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column17") = Format(CDec(dtRow.Item("vehicletotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column18") = Format(CDec(dtRow.Item("machinerytotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column19") = Format(CDec(dtRow.Item("otherbusinesstotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column20") = Format(CDec(dtRow.Item("otherresourcetotal")), GUI.fmtCurrency)
                rpt_Rows.Item("Column21") = Format(CDec(dtRow.Item("debttotal")), GUI.fmtCurrency)

                If intPrevId <> CInt(dtRow.Item("Id")) Then
                    decSalGrpTotal = CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                    decDeclareGrpTotal = CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                    decDebtGrpTotal = CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))

                Else
                    decSalGrpTotal += CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                    decDeclareGrpTotal += CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                    decDebtGrpTotal += CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))
                End If

                decSalTotal += CDec(Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency))
                decDeclareTotal += CDec(Format(CDec(dtRow.Item("TotalDeclaredValue")), GUI.fmtCurrency))
                decDebtTotal += CDec(Format(CDec(dtRow.Item("TotalDebtValue")), GUI.fmtCurrency))

                rpt_Rows.Item("Column82") = Format(decSalGrpTotal, GUI.fmtCurrency)
                rpt_Rows.Item("Column83") = Format(decDeclareGrpTotal, GUI.fmtCurrency)
                rpt_Rows.Item("Column84") = Format(decDebtGrpTotal, GUI.fmtCurrency)

                iCnt += 1
                intPrevId = CInt(dtRow.Item("Id"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

            Next

            Select Case mintReportTypeId
                Case 0 'Employee Declared Value Analysis
                    objRpt = New ArutiReport.Designer.rptDeclaredValueAnalysis
                Case 1 'Employee Wise Asset Declaration Analysis
                    objRpt = New ArutiReport.Designer.rptADCategoryWise
            End Select


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 1, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 2, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 3, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 4, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            If mblnShowSalary = False Then
                ReportFunction.EnableSuppress(objRpt, "txtSalAmt", True)
                ReportFunction.EnableSuppress(objRpt, "Column81", True)
                ReportFunction.EnableSuppress(objRpt, "Column821", True)
                ReportFunction.EnableSuppress(objRpt, "txtTotalAmt1", True)


                objRpt.ReportDefinition.ReportObjects("txtDecAmt").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                objRpt.ReportDefinition.ReportObjects("Column831").Left = objRpt.ReportDefinition.ReportObjects("Column821").Left
                objRpt.ReportDefinition.ReportObjects("txtTotalAmt2").Left = objRpt.ReportDefinition.ReportObjects("txtTotalAmt1").Left

                Select Case mintReportTypeId
                    Case 0 'Employee Declared Value Analysis
                        objRpt.ReportDefinition.ReportObjects("Column91").Left = objRpt.ReportDefinition.ReportObjects("Column81").Left

                        Dim iPosition As Integer = objRpt.ReportDefinition.ReportObjects("txtDecAmt").Left

                        iPosition = iPosition + objRpt.ReportDefinition.ReportObjects("txtDecAmt").Width + 30
                        objRpt.ReportDefinition.ReportObjects("txtLiability").Left = iPosition
                        objRpt.ReportDefinition.ReportObjects("Column101").Left = iPosition
                        objRpt.ReportDefinition.ReportObjects("Column841").Left = iPosition
                        objRpt.ReportDefinition.ReportObjects("txtTotalAmt3").Left = iPosition

                    Case 1 'Employee Wise Asset Declaration Analysis
                        Dim salarywidth As Integer = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Width

                        objRpt.ReportDefinition.ReportObjects("txtDecAmt").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtCurrAsset").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtCurrAsset").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtShareDividend").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtShareDividend").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtHouse").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtHouse").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtParkFarm").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtParkFarm").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtVehicle").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtVehicle").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtMachine").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtMachine").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtBusiness").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtBusiness").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtResource").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtResource").Width += salarywidth

                        objRpt.ReportDefinition.ReportObjects("txtLiabValue").Left = objRpt.ReportDefinition.ReportObjects("txtSalAmt").Left
                        objRpt.ReportDefinition.ReportObjects("txtLiabValue").Width += salarywidth
                End Select


            End If

            ReportFunction.TextChange(objRpt, "txtSN", Language.getMessage(mstrModuleName, 5, "Sno."))
            ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 6, "Employee Code"))
            ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 7, "Employee Name"))
            ReportFunction.TextChange(objRpt, "txtAdate", Language.getMessage(mstrModuleName, 8, "Appointed Date"))
            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 9, "Department"))
            ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 10, "Job Title"))
            ReportFunction.TextChange(objRpt, "txtClassGrp", Language.getMessage(mstrModuleName, 11, "Region"))
            ReportFunction.TextChange(objRpt, "txtClass", Language.getMessage(mstrModuleName, 12, "WorkStation"))
            ReportFunction.TextChange(objRpt, "txtSalAmt", Language.getMessage(mstrModuleName, 13, "Salary"))
            ReportFunction.TextChange(objRpt, "txtLiability", Language.getMessage(mstrModuleName, 14, "Liability Amount"))

            ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 15, "Group Count : "))
            ReportFunction.TextChange(objRpt, "txtGrandCount", Language.getMessage(mstrModuleName, 16, "Grand Count : "))
            ReportFunction.TextChange(objRpt, "txtGrpTotal", Language.getMessage(mstrModuleName, 17, "Group Total : "))

            ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Select Case mintReportTypeId
                Case 0 'Employee Declared Value Analysis
                    ReportFunction.TextChange(objRpt, "txtDecAmt", Language.getMessage(mstrModuleName, 18, "Declared Amount"))
                    ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 19, "Grand Total :"))

                Case 1 'Employee Wise Asset Declaration Analysis
                    ReportFunction.TextChange(objRpt, "txtDecAmt", Language.getMessage(mstrModuleName, 20, "ASSET CATEGORY"))
                    ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 21, "TOTAL"))

                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.CURRENT_ASSETS Then
                        ReportFunction.TextChange(objRpt, "txtCurrAsset", Language.getMessage(mstrModuleName, 22, "CURRENT ASSETS"))
                    Else
                        ReportFunction.TextChange(objRpt, "txtCurrAsset", "")
                        ReportFunction.EnableSuppress(objRpt, "Column131", True)
                        ReportFunction.EnableUnderlaySection(objRpt, "DetailCurrAsset", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.SHARE_AND_DIVIDENDS Then
                        ReportFunction.TextChange(objRpt, "txtShareDividend", Language.getMessage(mstrModuleName, 23, "SHARE AND DIVIDENDS"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailShareDividend", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailShareDividend", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.HOUSES_AND_BUILDINGS Then
                        ReportFunction.TextChange(objRpt, "txtHouse", Language.getMessage(mstrModuleName, 24, "HOUSES AND BUILDINGS"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailHouse", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailHouse", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.PARK_FARMS_AND_MINES Then
                        ReportFunction.TextChange(objRpt, "txtParkFarm", Language.getMessage(mstrModuleName, 25, "PARK, FARMS AND MINES"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailParkFarm", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailParkFarm", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.TRANSPORT_EQUIPMENTS Then
                        ReportFunction.TextChange(objRpt, "txtVehicle", Language.getMessage(mstrModuleName, 26, "TRANSPORT EQUIPMENTS"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailVehicle", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailVehicle", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.MACHINERIES_AND_INDUSTRIES Then
                        ReportFunction.TextChange(objRpt, "txtMachine", Language.getMessage(mstrModuleName, 27, "MACHINERIES AND INDUSTRIES"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailMachine", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailMachine", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.OTHER_BUSINESSES Then
                        ReportFunction.TextChange(objRpt, "txtBusiness", Language.getMessage(mstrModuleName, 28, "OTHER BUSINESSES"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailBusiness", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailBusiness", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.OTHER_COMMERCIAL_INTERESTS Then
                        ReportFunction.TextChange(objRpt, "txtResource", Language.getMessage(mstrModuleName, 29, "OTHER COMMERCIAL INTERESTS"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailResource", False)
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailResource", True)
                    End If
                    If mintAssetCategoryId = 0 OrElse mintAssetCategoryId = enAD_Categories.DEBTS Then
                        ReportFunction.TextChange(objRpt, "txtLiabValue", Language.getMessage(mstrModuleName, 30, "DEBTS"))
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailLiabValue", False)
                        If mintAssetCategoryId = enAD_Categories.DEBTS Then
                            Call ReportFunction.EnableSuppressSection(objRpt, "DetailDeclaredValue", True)
                        End If
                    Else
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailLiabValue", True)
                        Call ReportFunction.EnableSuppressSection(objRpt, "DetailDeclaredValue", False)
                    End If
            End Select


            If rpt_Data.Tables("ArutiTable").Rows.Count > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtTotalAmt1", Format(decSalTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotalAmt2", Format(decDeclareTotal, GUI.fmtCurrency))
                Call ReportFunction.TextChange(objRpt, "txtTotalAmt3", Format(decDebtTotal, GUI.fmtCurrency))
            End If

            ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 31, "Printed By :"))
            ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 32, "Printed Date :"))

            ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Prepared By :")
            Language.setMessage(mstrModuleName, 2, "Checked By :")
            Language.setMessage(mstrModuleName, 3, "Approved By :")
            Language.setMessage(mstrModuleName, 4, "Received By :")
            Language.setMessage(mstrModuleName, 5, "Sno.")
            Language.setMessage(mstrModuleName, 6, "Employee Code")
            Language.setMessage(mstrModuleName, 7, "Employee Name")
            Language.setMessage(mstrModuleName, 8, "Appointed Date")
            Language.setMessage(mstrModuleName, 9, "Department")
            Language.setMessage(mstrModuleName, 10, "Job Title")
            Language.setMessage(mstrModuleName, 11, "Region")
            Language.setMessage(mstrModuleName, 12, "WorkStation")
            Language.setMessage(mstrModuleName, 13, "Salary")
            Language.setMessage(mstrModuleName, 14, "Liability Amount")
            Language.setMessage(mstrModuleName, 15, "Group Count :")
            Language.setMessage(mstrModuleName, 16, "Grand Count :")
            Language.setMessage(mstrModuleName, 17, "Group Total :")
            Language.setMessage(mstrModuleName, 18, "Declared Amount")
            Language.setMessage(mstrModuleName, 19, "Grand Total :")
            Language.setMessage(mstrModuleName, 20, "ASSET CATEGORY")
            Language.setMessage(mstrModuleName, 21, "TOTAL")
            Language.setMessage(mstrModuleName, 22, "CURRENT ASSETS")
            Language.setMessage(mstrModuleName, 23, "SHARE AND DIVIDENDS")
            Language.setMessage(mstrModuleName, 24, "HOUSES AND BUILDINGS")
            Language.setMessage(mstrModuleName, 25, "PARK, FARMS AND MINES")
            Language.setMessage(mstrModuleName, 26, "TRANSPORT EQUIPMENTS")
            Language.setMessage(mstrModuleName, 27, "MACHINERIES AND INDUSTRIES")
            Language.setMessage(mstrModuleName, 28, "OTHER BUSINESSES")
            Language.setMessage(mstrModuleName, 29, "OTHER COMMERCIAL INTERESTS")
            Language.setMessage(mstrModuleName, 30, "DEBTS")
            Language.setMessage(mstrModuleName, 31, "Printed By :")
            Language.setMessage(mstrModuleName, 32, "Printed Date :")
            Language.setMessage(mstrModuleName, 33, "As On Date :")
            Language.setMessage(mstrModuleName, 34, "Employee :")
            Language.setMessage(mstrModuleName, 35, "Appointed Date From :")
            Language.setMessage(mstrModuleName, 36, "To :")
            Language.setMessage(mstrModuleName, 37, "Appointed Before Date :")
            Language.setMessage(mstrModuleName, 38, "Appointed After Date :")
            Language.setMessage(mstrModuleName, 39, "And")
            Language.setMessage(mstrModuleName, 40, "Estimated Declared Value")
            Language.setMessage(mstrModuleName, 41, "Estimated Debt Value")
            Language.setMessage(mstrModuleName, 42, "Order By :")
            Language.setMessage(mstrModuleName, 43, "Employee Code")
            Language.setMessage(mstrModuleName, 44, "Employee Name")
            Language.setMessage(mstrModuleName, 45, "Appointed Date")
            Language.setMessage(mstrModuleName, 46, "Department")
            Language.setMessage(mstrModuleName, 47, "Job Title")
            Language.setMessage(mstrModuleName, 48, "Region")
            Language.setMessage(mstrModuleName, 49, "WorkStation")
            Language.setMessage(mstrModuleName, 50, "Declared Amount")
            Language.setMessage(mstrModuleName, 51, "Liability Amount")
            Language.setMessage(mstrModuleName, 52, "Salary")
            Language.setMessage(mstrModuleName, 53, " Asset Category :")
            Language.setMessage(mstrModuleName, 54, "Value")
            Language.setMessage(mstrModuleName, 55, " Asset Category : ALL")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class

﻿'************************************************************************************************************************************
'Class Name : clsAssessment_Progress_Update.vb
'Purpose    :
'Date       :01-Jul-2021
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>
''' 
Public Class clsProgressUpdateReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsProgressUpdateReport"
    Private mstrReportId As String = enArutiReport.Assessment_Progress_Update '270
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Enum "

    Public Enum enUpdateStatus
        Updated = 1
        NotUpdated = 2
        NotPlanned = 3
    End Enum

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintCompanyUnkid As Integer = 0
    Private mdtEmployeeAsOnDate As DateTime
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mstrSelectedCols As String = String.Empty
    Private mstrSelectedJoin As String = String.Empty
    Private mstrDisplayCols As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnFirstNamethenSurname As Boolean = True
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintStatusId As Integer = 0
    Private mstrStatusName As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _SelectedCols() As String
        Set(ByVal value As String)
            mstrSelectedCols = value
        End Set
    End Property

    Public WriteOnly Property _SelectedJoin() As String
        Set(ByVal value As String)
            mstrSelectedJoin = value
        End Set
    End Property

    Public WriteOnly Property _DisplayCols() As String
        Set(ByVal value As String)
            mstrDisplayCols = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    Public WriteOnly Property _StatusName() As String
        Set(ByVal value As String)
            mstrStatusName = value
        End Set
    End Property

#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = -1
            mstrEmployeeName = String.Empty
            mintPeriodId = -1
            mstrPeriodName = String.Empty
            mintCompanyUnkid = 0
            mdtEmployeeAsOnDate = Nothing
            Rpt = Nothing
            mstrSelectedCols = String.Empty
            mstrSelectedJoin = String.Empty
            mstrDisplayCols = String.Empty
            mstrAdvanceFilter = String.Empty
            mblnFirstNamethenSurname = True
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function GetProgressStatus(ByVal strList As String, Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim ds As New DataSet
        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT " & CInt(enUpdateStatus.Updated) & " AS Id, @Updated AS Name UNION " & _
                    "SELECT " & CInt(enUpdateStatus.NotUpdated) & " AS Id, @NotUpdated AS Name UNION " & _
                    "SELECT " & CInt(enUpdateStatus.NotPlanned) & " AS Id, @NotPlanned AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Select"))
            objDataOperation.AddParameter("@Updated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Updated"))
            objDataOperation.AddParameter("@NotUpdated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Not Updated"))
            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Not Planned"))

            ds = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetProgressStatus; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return ds
    End Function

    Public Sub ExportProgressUpdate(ByVal strDatabaseName As String, _
                                    ByVal intUserUnkid As Integer, _
                                    ByVal intYearUnkid As Integer, _
                                    ByVal intCompanyUnkid As Integer, _
                                    ByVal dtPeriodStart As Date, _
                                    ByVal dtPeriodEnd As Date, _
                                    ByVal strUserModeSetting As String, _
                                    ByVal blnOnlyApproved As Boolean, _
                                    ByVal strExportPath As String, _
                                    ByVal blnOpenAfterExport As Boolean)
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            If intCompanyUnkid <= 0 Then
                intCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = intCompanyUnkid
            ConfigParameter._Object._Companyunkid = intCompanyUnkid

            If intUserUnkid <= 0 Then
                intUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = intUserUnkid

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "SELECT " & _
                   "     employeecode AS ecode "

            If mblnFirstNamethenSurname Then
                StrQ &= "    ,firstname + ' ' + surname AS ename "
            Else
                StrQ &= "    ,surname + ' ' + firstname AS ename "
            End If

            StrQ &= "    ,ISNULL(ejbm.job_name, '') AS job "

            StrQ &= mstrSelectedCols & " "

            StrQ &= "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN @NotPlanned " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN @NotUpdated " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN @Updated " & _
                    "     END AS gstatus " & _
                    "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN 0 " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN 0 " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN CAST(ISNULL(c.avgpct, 0) AS DECIMAL(36, 2)) " & _
                    "     END AS avgpct " & _
                    "    ,CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN '' " & _
                    "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN '' " & _
                    "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN ISNULL(c.adate, '') " & _
                    "     END AS adate "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields & " "
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS teffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS reffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ISNULL(CONVERT(CHAR(8),effectivedate,112),'') AS ceffdate " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_cctranhead_tran " & _
                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        pt.gradegroupunkid " & _
                    "       ,pt.gradeunkid " & _
                    "       ,pt.gradelevelunkid " & _
                    "       ,pt.employeeunkid " & _
                    "       ,ROW_NUMBER()OVER(PARTITION BY pt.employeeunkid ORDER BY pt.incrementdate DESC, pt.salaryincrementtranunkid DESC) AS rno " & _
                    "   FROM prsalaryincrement_tran pt " & _
                    "   WHERE pt.isvoid = 0 AND pt.isapproved = 1 AND CONVERT(CHAR(8),pt.incrementdate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    ") AS grds ON hremployee_master.employeeunkid = grds.employeeunkid AND grds.rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= mstrSelectedJoin & " "

            StrQ &= mstrAnalysis_Join & " "

            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,jobunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY employeeunkid) AS rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd).ToString & "' " & _
                    ") AS t ON t.employeeunkid = hremployee_master.employeeunkid AND t.rno = 1 " & _
                    "LEFT JOIN hrjob_master as ejbm ON t.jobunkid = ejbm.jobunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        employeeunkid " & _
                    "       ,COUNT(1) AS tgoal " & _
                    "   FROM hrassess_empfield1_master " & _
                    "   WHERE periodunkid = @periodunkid AND isvoid = 0 " & _
                    "   GROUP BY employeeunkid " & _
                    ") AS a ON a.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hrassess_empupdate_tran.employeeunkid " & _
                    "       ,COUNT(DISTINCT empfieldunkid) AS ugoal " & _
                    "   FROM hrassess_empupdate_tran " & _
                    "       JOIN hrassess_empfield1_master ON empfield1unkid = empfieldunkid " & _
                    "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
                    "       AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND hrassess_empfield1_master.periodunkid = hrassess_empupdate_tran.periodunkid " & _
                    "       AND approvalstatusunkid = 2 AND hrassess_empfield1_master.periodunkid = @periodunkid " & _
                    "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
                    ") AS b ON b.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        hrassess_empupdate_tran.employeeunkid " & _
                    "       ,SUM(hrassess_empupdate_tran.pct_completed) / COUNT(empfieldunkid) AS avgpct " & _
                    "       ,CONVERT(CHAR(8), MAX(updatedate), 112) AS adate " & _
                    "   FROM hrassess_empupdate_tran " & _
                    "       JOIN hrassess_empfield1_master ON empfield1unkid = empfieldunkid " & _
                    "   WHERE hrassess_empupdate_tran.periodunkid = @periodunkid " & _
                    "   AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empupdate_tran.isvoid = 0 AND hrassess_empfield1_master.periodunkid = hrassess_empupdate_tran.periodunkid " & _
                    "   AND approvalstatusunkid = 2 AND hrassess_empfield1_master.periodunkid = @periodunkid " & _
                    "   GROUP BY hrassess_empupdate_tran.employeeunkid " & _
                    ") AS c ON c.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "


            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If


            Dim iExFliter As String = ""
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeid "
                iExFliter &= "," & Language.getMessage(mstrModuleName, 300, "Employee :") & " " & mstrEmployeeName
            End If

            If mintStatusId > 0 Then
                StrQ &= " AND CASE WHEN ISNULL(a.tgoal, 0) <= 0 THEN " & CInt(enUpdateStatus.NotPlanned) & " " & _
                        "          WHEN ISNULL(a.tgoal, 0) <> ISNULL(b.ugoal, 0) THEN " & CInt(enUpdateStatus.NotUpdated) & " " & _
                        "          WHEN ISNULL(a.tgoal, 0) = ISNULL(b.ugoal, 0) THEN " & CInt(enUpdateStatus.Updated) & " " & _
                        "     END = @statusid "

                iExFliter &= "," & Language.getMessage(mstrModuleName, 301, "Status :") & " " & mstrStatusName
            End If


            objDataOperation.AddParameter("@NotPlanned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Not Planned"))
            objDataOperation.AddParameter("@Updated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Updated"))
            objDataOperation.AddParameter("@NotUpdated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Not Updated"))
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusId)
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim strarrGroupColumns As String() = Nothing
            'If mintViewIndex <= 0 Then
            '    dsList.Tables(0).Columns.Remove("Id") : dsList.Tables(0).Columns.Remove("GName")
            'Else
            '    dsList.Tables(0).Columns.Remove("Id")
            '    dsList.Tables(0).Columns("GName").Caption = mstrReport_GroupName
            '    Dim strGrpCols As String() = {"GName"}
            '    strarrGroupColumns = strGrpCols
            'End If
            dsList.Tables(0).Columns.Remove("Id") : dsList.Tables(0).Columns.Remove("GName")

            dsList.Tables(0).Columns("ecode").Caption = Language.getMessage(mstrModuleName, 400, "Employee Code")
            dsList.Tables(0).Columns("ename").Caption = Language.getMessage(mstrModuleName, 401, "Employee Name")
            dsList.Tables(0).Columns("job").Caption = Language.getMessage(mstrModuleName, 402, "Job")
            dsList.Tables(0).Columns("gstatus").Caption = Language.getMessage(mstrModuleName, 403, "Progress Update Status")
            dsList.Tables(0).Columns("avgpct").Caption = Language.getMessage(mstrModuleName, 404, "Average% updated to date")

            For Each iRow As DataRow In dsList.Tables(0).Rows
                If iRow("adate").ToString().Length > 0 Then iRow("adate") = eZeeDate.convertDate(iRow("adate").ToString).ToShortDateString()
            Next
            dsList.Tables(0).Columns("adate").Caption = Language.getMessage(mstrModuleName, 405, "As on Date")

            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(dsList.Tables("List").Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case 0
                        intArrayColumnWidth(i) = 90
                    Case 1
                        intArrayColumnWidth(i) = 140
                    Case 2
                        intArrayColumnWidth(i) = 160
                    Case Else
                        intArrayColumnWidth(i) = 100
                End Select
            Next

            If iExFliter.Trim.Length > 0 Then iExFliter = Mid(iExFliter, 2)
            Dim iExTitle As String = Language.getMessage(mstrModuleName, 18, "Period : ")

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, strExportPath, blnOpenAfterExport, dsList.Tables("List"), intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, iExTitle & mstrPeriodName, iExFliter, Nothing, "", True, Nothing, Nothing, Nothing, Nothing, False, False, "", False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExportProgressUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 18, "Period :")
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 101, "Updated")
            Language.setMessage(mstrModuleName, 102, "Not Updated")
            Language.setMessage(mstrModuleName, 103, "Not Planned")
            Language.setMessage(mstrModuleName, 300, "Employee :")
            Language.setMessage(mstrModuleName, 301, "Status :")
            Language.setMessage(mstrModuleName, 400, "Employee Code")
            Language.setMessage(mstrModuleName, 401, "Employee Name")
            Language.setMessage(mstrModuleName, 402, "Job")
            Language.setMessage(mstrModuleName, 403, "Progress Update Status")
            Language.setMessage(mstrModuleName, 404, "Average% updated to date")
            Language.setMessage(mstrModuleName, 405, "As on Date")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

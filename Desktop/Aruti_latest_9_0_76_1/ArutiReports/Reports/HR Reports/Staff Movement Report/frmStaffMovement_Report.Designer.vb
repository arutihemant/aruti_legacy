﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffMovement_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStaffMovement_Report))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.objbtnSearchSickLeave = New eZee.Common.eZeeGradientButton
        Me.cboSickLeave = New System.Windows.Forms.ComboBox
        Me.LblSickLeave = New System.Windows.Forms.Label
        Me.objbtnSearchAnnualLeave = New eZee.Common.eZeeGradientButton
        Me.cboAnnualLeave = New System.Windows.Forms.ComboBox
        Me.LblAnnualLeave = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.objbtnSearchAbsent = New eZee.Common.eZeeGradientButton
        Me.cboAbsent = New System.Windows.Forms.ComboBox
        Me.LblAbsent = New System.Windows.Forms.Label
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.LblMembership = New System.Windows.Forms.Label
        Me.objbtnSearchChild = New eZee.Common.eZeeGradientButton
        Me.cboChild = New System.Windows.Forms.ComboBox
        Me.LblChild = New System.Windows.Forms.Label
        Me.objbtnSearchSpouse = New eZee.Common.eZeeGradientButton
        Me.CboSpouse = New System.Windows.Forms.ComboBox
        Me.LblSpouse = New System.Windows.Forms.Label
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.LblLeaveType = New System.Windows.Forms.Label
        Me.lvLeaveType = New System.Windows.Forms.ListView
        Me.colhLeaveType = New System.Windows.Forms.ColumnHeader
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchPeriod = New eZee.Common.eZeeGradientButton
        Me.LblPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchLocation = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchjob = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblLocation = New System.Windows.Forms.Label
        Me.cboLocation = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.LblJob = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(751, 58)
        Me.eZeeHeader.TabIndex = 21
        Me.eZeeHeader.Title = ""
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 608)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(751, 55)
        Me.EZeeFooter1.TabIndex = 22
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(355, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(458, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 1
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(554, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 2
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(650, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(9, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 0
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSickLeave)
        Me.gbFilterCriteria.Controls.Add(Me.cboSickLeave)
        Me.gbFilterCriteria.Controls.Add(Me.LblSickLeave)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAnnualLeave)
        Me.gbFilterCriteria.Controls.Add(Me.cboAnnualLeave)
        Me.gbFilterCriteria.Controls.Add(Me.LblAnnualLeave)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeLine1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAbsent)
        Me.gbFilterCriteria.Controls.Add(Me.cboAbsent)
        Me.gbFilterCriteria.Controls.Add(Me.LblAbsent)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMembership)
        Me.gbFilterCriteria.Controls.Add(Me.cboMembership)
        Me.gbFilterCriteria.Controls.Add(Me.LblMembership)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchChild)
        Me.gbFilterCriteria.Controls.Add(Me.cboChild)
        Me.gbFilterCriteria.Controls.Add(Me.LblChild)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.CboSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.LblSpouse)
        Me.gbFilterCriteria.Controls.Add(Me.chkSelectAll)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.lvLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.LblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchjob)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblLocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboLocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(6, 59)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(388, 544)
        Me.gbFilterCriteria.TabIndex = 23
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(6, 442)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(371, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 246
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(366, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(338, 6)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(94, 6)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(238, 21)
        Me.cboOtherEarning.TabIndex = 56
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSickLeave
        '
        Me.objbtnSearchSickLeave.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSickLeave.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSickLeave.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSickLeave.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSickLeave.BorderSelected = False
        Me.objbtnSearchSickLeave.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSickLeave.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSickLeave.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSickLeave.Location = New System.Drawing.Point(346, 298)
        Me.objbtnSearchSickLeave.Name = "objbtnSearchSickLeave"
        Me.objbtnSearchSickLeave.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSickLeave.TabIndex = 244
        '
        'cboSickLeave
        '
        Me.cboSickLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSickLeave.DropDownWidth = 120
        Me.cboSickLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSickLeave.FormattingEnabled = True
        Me.cboSickLeave.Location = New System.Drawing.Point(96, 298)
        Me.cboSickLeave.Name = "cboSickLeave"
        Me.cboSickLeave.Size = New System.Drawing.Size(244, 21)
        Me.cboSickLeave.TabIndex = 243
        '
        'LblSickLeave
        '
        Me.LblSickLeave.BackColor = System.Drawing.Color.Transparent
        Me.LblSickLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSickLeave.Location = New System.Drawing.Point(8, 300)
        Me.LblSickLeave.Name = "LblSickLeave"
        Me.LblSickLeave.Size = New System.Drawing.Size(84, 16)
        Me.LblSickLeave.TabIndex = 242
        Me.LblSickLeave.Text = "Sick Leave"
        '
        'objbtnSearchAnnualLeave
        '
        Me.objbtnSearchAnnualLeave.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAnnualLeave.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAnnualLeave.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAnnualLeave.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAnnualLeave.BorderSelected = False
        Me.objbtnSearchAnnualLeave.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAnnualLeave.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAnnualLeave.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAnnualLeave.Location = New System.Drawing.Point(346, 271)
        Me.objbtnSearchAnnualLeave.Name = "objbtnSearchAnnualLeave"
        Me.objbtnSearchAnnualLeave.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAnnualLeave.TabIndex = 241
        '
        'cboAnnualLeave
        '
        Me.cboAnnualLeave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnnualLeave.DropDownWidth = 120
        Me.cboAnnualLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAnnualLeave.FormattingEnabled = True
        Me.cboAnnualLeave.Location = New System.Drawing.Point(96, 271)
        Me.cboAnnualLeave.Name = "cboAnnualLeave"
        Me.cboAnnualLeave.Size = New System.Drawing.Size(244, 21)
        Me.cboAnnualLeave.TabIndex = 240
        '
        'LblAnnualLeave
        '
        Me.LblAnnualLeave.BackColor = System.Drawing.Color.Transparent
        Me.LblAnnualLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAnnualLeave.Location = New System.Drawing.Point(8, 273)
        Me.LblAnnualLeave.Name = "LblAnnualLeave"
        Me.LblAnnualLeave.Size = New System.Drawing.Size(84, 16)
        Me.LblAnnualLeave.TabIndex = 239
        Me.LblAnnualLeave.Text = "Annual Leave"
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(96, 135)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(188, 16)
        Me.chkInactiveemp.TabIndex = 237
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(272, 508)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(105, 30)
        Me.btnSaveSelection.TabIndex = 7
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = False
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(10, 155)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(367, 4)
        Me.EZeeLine1.TabIndex = 235
        '
        'objbtnSearchAbsent
        '
        Me.objbtnSearchAbsent.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAbsent.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAbsent.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAbsent.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAbsent.BorderSelected = False
        Me.objbtnSearchAbsent.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAbsent.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAbsent.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAbsent.Location = New System.Drawing.Point(346, 244)
        Me.objbtnSearchAbsent.Name = "objbtnSearchAbsent"
        Me.objbtnSearchAbsent.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAbsent.TabIndex = 233
        '
        'cboAbsent
        '
        Me.cboAbsent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAbsent.DropDownWidth = 120
        Me.cboAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAbsent.FormattingEnabled = True
        Me.cboAbsent.Location = New System.Drawing.Point(96, 244)
        Me.cboAbsent.Name = "cboAbsent"
        Me.cboAbsent.Size = New System.Drawing.Size(244, 21)
        Me.cboAbsent.TabIndex = 232
        '
        'LblAbsent
        '
        Me.LblAbsent.BackColor = System.Drawing.Color.Transparent
        Me.LblAbsent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAbsent.Location = New System.Drawing.Point(8, 246)
        Me.LblAbsent.Name = "LblAbsent"
        Me.LblAbsent.Size = New System.Drawing.Size(84, 16)
        Me.LblAbsent.TabIndex = 231
        Me.LblAbsent.Text = "Absent"
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(346, 217)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembership.TabIndex = 229
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 120
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(96, 217)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(244, 21)
        Me.cboMembership.TabIndex = 228
        '
        'LblMembership
        '
        Me.LblMembership.BackColor = System.Drawing.Color.Transparent
        Me.LblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMembership.Location = New System.Drawing.Point(8, 219)
        Me.LblMembership.Name = "LblMembership"
        Me.LblMembership.Size = New System.Drawing.Size(84, 16)
        Me.LblMembership.TabIndex = 227
        Me.LblMembership.Text = "Membership"
        '
        'objbtnSearchChild
        '
        Me.objbtnSearchChild.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchChild.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchChild.BorderSelected = False
        Me.objbtnSearchChild.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchChild.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchChild.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchChild.Location = New System.Drawing.Point(346, 191)
        Me.objbtnSearchChild.Name = "objbtnSearchChild"
        Me.objbtnSearchChild.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchChild.TabIndex = 226
        '
        'cboChild
        '
        Me.cboChild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChild.DropDownWidth = 120
        Me.cboChild.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChild.FormattingEnabled = True
        Me.cboChild.Location = New System.Drawing.Point(96, 191)
        Me.cboChild.Name = "cboChild"
        Me.cboChild.Size = New System.Drawing.Size(244, 21)
        Me.cboChild.TabIndex = 225
        '
        'LblChild
        '
        Me.LblChild.BackColor = System.Drawing.Color.Transparent
        Me.LblChild.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblChild.Location = New System.Drawing.Point(8, 193)
        Me.LblChild.Name = "LblChild"
        Me.LblChild.Size = New System.Drawing.Size(84, 16)
        Me.LblChild.TabIndex = 224
        Me.LblChild.Text = "Child"
        '
        'objbtnSearchSpouse
        '
        Me.objbtnSearchSpouse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSpouse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSpouse.BorderSelected = False
        Me.objbtnSearchSpouse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSpouse.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSpouse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSpouse.Location = New System.Drawing.Point(346, 164)
        Me.objbtnSearchSpouse.Name = "objbtnSearchSpouse"
        Me.objbtnSearchSpouse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSpouse.TabIndex = 223
        '
        'CboSpouse
        '
        Me.CboSpouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboSpouse.DropDownWidth = 120
        Me.CboSpouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboSpouse.FormattingEnabled = True
        Me.CboSpouse.Location = New System.Drawing.Point(96, 164)
        Me.CboSpouse.Name = "CboSpouse"
        Me.CboSpouse.Size = New System.Drawing.Size(244, 21)
        Me.CboSpouse.TabIndex = 222
        '
        'LblSpouse
        '
        Me.LblSpouse.BackColor = System.Drawing.Color.Transparent
        Me.LblSpouse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSpouse.Location = New System.Drawing.Point(8, 166)
        Me.LblSpouse.Name = "LblSpouse"
        Me.LblSpouse.Size = New System.Drawing.Size(84, 16)
        Me.LblSpouse.TabIndex = 221
        Me.LblSpouse.Text = "Spouse"
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(104, 322)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(71, 16)
        Me.chkSelectAll.TabIndex = 219
        Me.chkSelectAll.Text = "Select All"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'LblLeaveType
        '
        Me.LblLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.LblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveType.Location = New System.Drawing.Point(8, 344)
        Me.LblLeaveType.Name = "LblLeaveType"
        Me.LblLeaveType.Size = New System.Drawing.Size(84, 16)
        Me.LblLeaveType.TabIndex = 217
        Me.LblLeaveType.Text = "Leave Type"
        '
        'lvLeaveType
        '
        Me.lvLeaveType.CheckBoxes = True
        Me.lvLeaveType.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLeaveType})
        Me.lvLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeaveType.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvLeaveType.Location = New System.Drawing.Point(96, 343)
        Me.lvLeaveType.Name = "lvLeaveType"
        Me.lvLeaveType.ShowItemToolTips = True
        Me.lvLeaveType.Size = New System.Drawing.Size(271, 93)
        Me.lvLeaveType.TabIndex = 218
        Me.lvLeaveType.UseCompatibleStateImageBehavior = False
        Me.lvLeaveType.View = System.Windows.Forms.View.Details
        '
        'colhLeaveType
        '
        Me.colhLeaveType.Width = 200
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 120
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(96, 30)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(244, 21)
        Me.cboPeriod.TabIndex = 216
        '
        'objbtnSearchPeriod
        '
        Me.objbtnSearchPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPeriod.BorderSelected = False
        Me.objbtnSearchPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPeriod.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPeriod.Location = New System.Drawing.Point(346, 30)
        Me.objbtnSearchPeriod.Name = "objbtnSearchPeriod"
        Me.objbtnSearchPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPeriod.TabIndex = 215
        '
        'LblPeriod
        '
        Me.LblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.LblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPeriod.Location = New System.Drawing.Point(8, 32)
        Me.LblPeriod.Name = "LblPeriod"
        Me.LblPeriod.Size = New System.Drawing.Size(84, 16)
        Me.LblPeriod.TabIndex = 210
        Me.LblPeriod.Text = "Period"
        '
        'objbtnSearchLocation
        '
        Me.objbtnSearchLocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLocation.BorderSelected = False
        Me.objbtnSearchLocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLocation.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLocation.Location = New System.Drawing.Point(346, 111)
        Me.objbtnSearchLocation.Name = "objbtnSearchLocation"
        Me.objbtnSearchLocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLocation.TabIndex = 203
        '
        'objbtnSearchjob
        '
        Me.objbtnSearchjob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchjob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchjob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchjob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchjob.BorderSelected = False
        Me.objbtnSearchjob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchjob.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchjob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchjob.Location = New System.Drawing.Point(346, 84)
        Me.objbtnSearchjob.Name = "objbtnSearchjob"
        Me.objbtnSearchjob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchjob.TabIndex = 104
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(346, 57)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 60
        '
        'LblLocation
        '
        Me.LblLocation.BackColor = System.Drawing.Color.Transparent
        Me.LblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLocation.Location = New System.Drawing.Point(8, 113)
        Me.LblLocation.Name = "LblLocation"
        Me.LblLocation.Size = New System.Drawing.Size(84, 16)
        Me.LblLocation.TabIndex = 199
        Me.LblLocation.Text = "Location"
        '
        'cboLocation
        '
        Me.cboLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLocation.DropDownWidth = 120
        Me.cboLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLocation.FormattingEnabled = True
        Me.cboLocation.Location = New System.Drawing.Point(96, 111)
        Me.cboLocation.Name = "cboLocation"
        Me.cboLocation.Size = New System.Drawing.Size(244, 21)
        Me.cboLocation.TabIndex = 198
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 16)
        Me.lblEmployee.TabIndex = 197
        Me.lblEmployee.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(96, 57)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(244, 21)
        Me.cboEmployee.TabIndex = 196
        '
        'LblJob
        '
        Me.LblJob.BackColor = System.Drawing.Color.Transparent
        Me.LblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJob.Location = New System.Drawing.Point(8, 86)
        Me.LblJob.Name = "LblJob"
        Me.LblJob.Size = New System.Drawing.Size(84, 16)
        Me.LblJob.TabIndex = 190
        Me.LblJob.Text = "Job"
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 120
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(96, 84)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(244, 21)
        Me.cboJob.TabIndex = 5
        '
        'frmStaffMovement_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(751, 663)
        Me.ControlBox = False
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmStaffMovement_Report"
        Me.Text = "frmStaffMovement_Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents LblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLocation As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchjob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Private WithEvents LblLocation As System.Windows.Forms.Label
    Public WithEvents cboLocation As System.Windows.Forms.ComboBox
    Private WithEvents lblEmployee As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Private WithEvents LblJob As System.Windows.Forms.Label
    Public WithEvents cboJob As System.Windows.Forms.ComboBox
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Private WithEvents LblLeaveType As System.Windows.Forms.Label
    Friend WithEvents lvLeaveType As System.Windows.Forms.ListView
    Friend WithEvents colhLeaveType As System.Windows.Forms.ColumnHeader
    Private WithEvents LblSpouse As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Public WithEvents cboMembership As System.Windows.Forms.ComboBox
    Private WithEvents LblMembership As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchChild As eZee.Common.eZeeGradientButton
    Public WithEvents cboChild As System.Windows.Forms.ComboBox
    Private WithEvents LblChild As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSpouse As eZee.Common.eZeeGradientButton
    Public WithEvents CboSpouse As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchAbsent As eZee.Common.eZeeGradientButton
    Public WithEvents cboAbsent As System.Windows.Forms.ComboBox
    Private WithEvents LblAbsent As System.Windows.Forms.Label
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchSickLeave As eZee.Common.eZeeGradientButton
    Public WithEvents cboSickLeave As System.Windows.Forms.ComboBox
    Private WithEvents LblSickLeave As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAnnualLeave As eZee.Common.eZeeGradientButton
    Public WithEvents cboAnnualLeave As System.Windows.Forms.ComboBox
    Private WithEvents LblAnnualLeave As System.Windows.Forms.Label
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
End Class

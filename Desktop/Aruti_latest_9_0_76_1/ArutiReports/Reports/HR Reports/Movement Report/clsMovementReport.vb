'************************************************************************************************************************************
'Class Name : clsMovementReport.vb
'Purpose    :
'Date       : 26-Dec-2013
'Written By : Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports System

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsMovementReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsMovementReport"
    Private mstrReportId As String = enArutiReport.Movement_Report '141
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmplyoeeName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintAllocationTypeId As Integer = 1
    Private mstrSelectedAllocationIds As String = String.Empty
    Private mstrAllocationTypeName As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    Private mstrSelectedAllocNames As String = String.Empty
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mblnIncludeInactiveEmp = False

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmplyoeeName() As String
        Set(ByVal value As String)
            mstrEmplyoeeName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeId() As Integer
        Set(ByVal value As Integer)
            mintAllocationTypeId = value
        End Set
    End Property

    Public WriteOnly Property _SelectedAllocationIds() As String
        Set(ByVal value As String)
            mstrSelectedAllocationIds = value
        End Set
    End Property

    Public WriteOnly Property _AllocationTypeName() As String
        Set(ByVal value As String)
            mstrAllocationTypeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property

    Public WriteOnly Property _SelectedAllocNames() As String
        Set(ByVal value As String)
            mstrSelectedAllocNames = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END
#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportTypeId = 0
            mstrReportTypeName = String.Empty
            mintEmployeeId = 0
            mstrEmplyoeeName = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mintAllocationTypeId = 1
            mstrSelectedAllocationIds = String.Empty
            mstrAdvance_Filter = String.Empty
            mdtDate1 = Nothing
            mdtDate2 = Nothing
            mblnIncludeInactiveEmp = False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "From Date :") & " " & mdtDate1.ToShortDateString & " " & _
                               Language.getMessage(mstrModuleName, 16, "To") & " " & mdtDate2.ToShortDateString & " "
            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 17, "Employee :") & " " & mstrEmplyoeeName & " "
            End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 18, "Allocation Type :") & " " & mstrAllocationTypeName & " "


            'If mstrSelectedAllocNames.Trim.Length > 0 Then
            '    Me._FilterTitle &= mstrAllocationTypeName & " : " & mstrSelectedAllocNames & " "
            'End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    Select Case pintReportType
        '        Case 0
        '            objRpt = Generate_AllocationMovement()
        '        Case 1
        '            objRpt = Generate_EmployeeMovement()
        '    End Select

        '    Rpt = objRpt

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            Select Case pintReportType
                Case 0
                    objRpt = Generate_AllocationMovement(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
                Case 1
                    objRpt = Generate_EmployeeMovement(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)
            End Select

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_AllocationMovement(ByVal strDatabaseName As String, _
                                                ByVal intUserUnkid As Integer, _
                                                ByVal intYearUnkid As Integer, _
                                                ByVal intCompanyUnkid As Integer, _
                                                ByVal dtPeriodStart As Date, _
                                                ByVal dtPeriodEnd As Date, _
                                                ByVal strUserModeSetting As String, _
                                                ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Public Function Generate_AllocationMovement() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsAlloc As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As New ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END

            Dim iColumnName As String = String.Empty
            Dim iTableName As String = String.Empty

            Select Case mintAllocationTypeId
                Case enAllocation.BRANCH
                    iColumnName = "stationunkid"
                    iTableName = "hrstation_master"
                Case enAllocation.DEPARTMENT_GROUP
                    iColumnName = "deptgroupunkid"
                    iTableName = "hrdepartment_group_master"
                Case enAllocation.DEPARTMENT
                    iColumnName = "departmentunkid"
                    iTableName = "hrdepartment_master"
                Case enAllocation.SECTION_GROUP
                    iColumnName = "sectiongroupunkid"
                    iTableName = "hrsectiongroup_master"
                Case enAllocation.SECTION
                    iColumnName = "sectionunkid"
                    iTableName = "hrsection_master"
                Case enAllocation.UNIT_GROUP
                    iColumnName = "unitgroupunkid"
                    iTableName = "hrunitgroup_master"
                Case enAllocation.UNIT
                    iColumnName = "unitunkid"
                    iTableName = "hrunit_master"
                Case enAllocation.TEAM
                    iColumnName = "teamunkid"
                    iTableName = "hrteam_master"
                Case enAllocation.JOB_GROUP
                    iColumnName = "jobgroupunkid"
                    iTableName = "hrjobgroup_master"
                Case enAllocation.JOBS
                    iColumnName = "jobunkid"
                    iTableName = "hrjob_master"
                Case enAllocation.CLASS_GROUP
                    iColumnName = "classgroupunkid"
                    iTableName = "hrclassgroup_master"
                Case enAllocation.CLASSES
                    iColumnName = "classunkid"
                    iTableName = "hrclasses_master"
            End Select

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
            '       "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT " & _
            '       "SET @tallocid = 0 " & _
            '       "SET @Id = 0 " & _
            '       "DECLARE EM CURSOR FOR SELECT employeeunkid, " & iColumnName & " ,auditdatetime FROM athremployee_master WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' ORDER BY employeeunkid,auditdatetime ASC " & _
            '       "OPEN EM " & _
            '       "FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
            '       "WHILE @@FETCH_STATUS = 0 " & _
            '       "BEGIN " & _
            '                 "IF @fallocid <> @tallocid " & _
            '                 "BEGIN " & _
            '                      "INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
            '                      "SET @Id = @@IDENTITY; " & _
            '                      "UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
            '                     "SET @tallocid = @fallocid " & _
            '                 "END " & _
            '            "FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
            '       "END " & _
            '       "CLOSE EM " & _
            '       "DEALLOCATE EM " & _
            '       "SELECT " & _
            '       " employeecode+' - '+ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename " & _
            '       ",JB.job_name AS job " & _
            '       ",CONVERT(CHAR(8),adate,112) AS aDate "

            Dim StrCursorQuery As String
            StrCursorQuery = ""

            StrCursorQuery = GenerateCursorQry(iTableName, iColumnName)


            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
            '       "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT " & _
            '       "SET @tallocid = 0 " & _
            '       "SET @Id = 0 " & _
            '       "DECLARE EM CURSOR FOR " & StrCursorQuery & " " & _
            '       "    OPEN EM " & _
            '       "        FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
            '       "        WHILE @@FETCH_STATUS = 0 " & _
            '       "        BEGIN " & _
            '       "            IF @fallocid <> @tallocid " & _
            '       "            BEGIN " & _
            '       "                INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
            '       "                SET @Id = @@IDENTITY; " & _
            '       "                UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
            '       "                SET @tallocid = @fallocid " & _
            '       "            END " & _
            '       "            FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
            '       "        END " & _
            '       "    CLOSE EM " & _
            '       "DEALLOCATE EM " & _
            '       "SELECT "
            StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
                   "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT, @fempid INT " & _
                   "SET @tallocid = 0 " & _
                   "SET @Id = 0 " & _
                   "DECLARE EM CURSOR FOR " & StrCursorQuery & " " & _
                   "    OPEN EM " & _
                   "        FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
                   "        WHILE @@FETCH_STATUS = 0 " & _
                   "        BEGIN " & _
                   "            IF (@fallocid <> @tallocid OR @fempid <> @empid) " & _
                   "            BEGIN " & _
                   "                INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
                   "                SET @Id = @@IDENTITY; " & _
                   "                UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
                   "                SET @tallocid = @fallocid " & _
                   "                SET @fempid = @empid " & _
                   "            END " & _
                   "            FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
                   "        END " & _
                   "    CLOSE EM " & _
                   "DEALLOCATE EM " & _
                   "SELECT "
            'S.SANDEEP [20 MAY 2016] -- END



            If mblnFirstNamethenSurname = False Then
                StrQ &= " employeecode+' - '+ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS ename "
            Else
                StrQ &= " employeecode+' - '+ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename "
            End If
            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ &= "    ,JB.job_name AS job " & _
            '        "    ,CONVERT(CHAR(8),adate,112) AS aDate "
            StrQ &= "    ,ISNULL(JB.job_name,'') AS job " & _
                    "    ,CONVERT(CHAR(8),adate,112) AS aDate "
            'S.SANDEEP [20 MAY 2016] -- END

            'S.SANDEEP [04 JUN 2015] -- END


            If iTableName = "hrjob_master" Then
                StrQ &= ",'('+ fdept.job_code +') '+ fdept.job_name AS frm_dept " & _
                        ",'('+ tdept.job_code +') '+ tdept.job_name AS to_dept "
            Else
                StrQ &= ",'('+ fdept.code +') '+ fdept.name AS frm_dept " & _
                        ",'('+ tdept.code +') '+ tdept.name AS to_dept "
            End If

            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ &= ",fallocid " & _
            '        ",tallocid " & _
            '        "FROM @EM " & _
            '        "   LEFT JOIN " & iTableName & " fdept ON fdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].fallocid " & _
            '        "   LEFT JOIN " & iTableName & " tdept ON tdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].tallocid " & _
            '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = [@EM].empid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            employeeunkid " & _
            '        "           ,jobunkid " & _
            '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate ASC) AS rno " & _
            '        "       FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
            '        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        "   ) AS CR ON CR.employeeunkid  = hremployee_master.employeeunkid AND CR.rno = 1 " & _
            '        "   JOIN hrjob_master AS JB ON CR.jobunkid = JB.jobunkid "

            StrQ &= ",fallocid " & _
                    ",tallocid " & _
                    "FROM @EM " & _
                    "   LEFT JOIN " & iTableName & " fdept ON fdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].fallocid " & _
                    "   LEFT JOIN " & iTableName & " tdept ON tdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].tallocid " & _
                    "   JOIN hremployee_master ON hremployee_master.employeeunkid = [@EM].empid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,jobunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
                    "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS CR ON CR.employeeunkid  = hremployee_master.employeeunkid AND CR.rno = 1 " & _
                    "   LEFT JOIN hrjob_master AS JB ON CR.jobunkid = JB.jobunkid "
            'S.SANDEEP [20 MAY 2016] -- END

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "WHERE 1 = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [20 MAY 2016] -- START
            StrQ &= " ORDER BY [@EM].adate "
            'S.SANDEEP [20 MAY 2016] -- END

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

            Dim dsTranData As New DataSet

            dsTranData = objDataOperation.ExecQuery(StrQ, "iList")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT * FROM " & iTableName & " WHERE isactive = 1 "
            If mstrSelectedAllocationIds.Trim.Length > 0 Then
                StrQ &= " AND " & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " IN (" & mstrSelectedAllocationIds & ")"
            End If

            dsAlloc = objDataOperation.ExecQuery(StrQ, "iList")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsAlloc.Tables("iList").Rows.Count > 0 Then
                Dim iRow As DataRow = Nothing

                For Each dRow As DataRow In dsAlloc.Tables(0).Rows
                    '***************************************** TRANSFER IN BLOCK START *****************************************
                    Dim dtIN As DataRow() = dsTranData.Tables(0).Select("tallocid = '" & dRow.Item(IIf(iColumnName = "classunkid", "classesunkid", iColumnName)) & "' AND fallocid > 0")
                    Dim iFlag1 As Boolean = False
                    For i As Integer = 0 To dtIN.Length - 1
                        If iFlag1 = False Then
                            iRow = rpt_Data.Tables("ArutiTable").NewRow
                            If iTableName = "hrjob_master" Then
                                iRow.Item("Column1") = mstrAllocationTypeName & " : (" & dRow.Item("job_code") & ") - " & dRow.Item("job_name")
                            Else
                                iRow.Item("Column1") = mstrAllocationTypeName & " : (" & dRow.Item("code") & ") - " & dRow.Item("name")
                            End If
                            iRow.Item("Column20") = 1
                            rpt_Data.Tables("ArutiTable").Rows.Add(iRow)

                            iRow = rpt_Data.Tables("ArutiTable").NewRow
                            iRow.Item("Column1") = Language.getMessage(mstrModuleName, 5, "Transfer IN")
                            iRow.Item("Column20") = 1
                            iRow.Item("Column4") = Language.getMessage(mstrModuleName, 6, "Transfer From")
                            rpt_Data.Tables("ArutiTable").Rows.Add(iRow)
                            iFlag1 = True
                        End If
                        iRow = rpt_Data.Tables("ArutiTable").NewRow

                        iRow.Item("Column1") = Space(5) & dtIN(i).Item("ename")
                        iRow.Item("Column2") = dtIN(i).Item("job")
                        iRow.Item("Column3") = eZeeDate.convertDate(dtIN(i).Item("aDate")).ToShortDateString
                        iRow.Item("Column4") = mstrAllocationTypeName & " : " & dtIN(i).Item("frm_dept")
                        iRow.Item("Column20") = 0

                        rpt_Data.Tables("ArutiTable").Rows.Add(iRow)
                    Next
                    '***************************************** TRANSFER IN BLOCK ENDS  *****************************************

                    '***************************************** TRANSFER OUT BLOCK START *****************************************
                    Dim dtOUT As DataRow() = dsTranData.Tables(0).Select("fallocid = '" & dRow.Item(IIf(iColumnName = "classunkid", "classesunkid", iColumnName)) & "' AND tallocid > 0")
                    Dim iFlag2 As Boolean = False
                    For i As Integer = 0 To dtOUT.Length - 1
                        If iFlag1 = False Then
                            iRow = rpt_Data.Tables("ArutiTable").NewRow
                            If iTableName = "hrjob_master" Then
                                iRow.Item("Column1") = mstrAllocationTypeName & " : (" & dRow.Item("job_code") & ") - " & dRow.Item("job_name")
                            Else
                                iRow.Item("Column1") = mstrAllocationTypeName & " : (" & dRow.Item("code") & ") - " & dRow.Item("name")
                            End If
                            iRow.Item("Column20") = 1
                            rpt_Data.Tables("ArutiTable").Rows.Add(iRow)

                            iRow = rpt_Data.Tables("ArutiTable").NewRow
                            iRow.Item("Column1") = Language.getMessage(mstrModuleName, 7, "Transfer OUT")
                            iRow.Item("Column20") = 1
                            iRow.Item("Column4") = Language.getMessage(mstrModuleName, 8, "Transfer To")
                            rpt_Data.Tables("ArutiTable").Rows.Add(iRow)
                            iFlag1 = True : iFlag2 = True
                        End If
                        If iFlag1 = True AndAlso iFlag2 = False Then
                            iRow = rpt_Data.Tables("ArutiTable").NewRow
                            iRow.Item("Column1") = Language.getMessage(mstrModuleName, 7, "Transfer OUT")
                            iRow.Item("Column20") = 1
                            iRow.Item("Column4") = Language.getMessage(mstrModuleName, 8, "Transfer To")
                            rpt_Data.Tables("ArutiTable").Rows.Add(iRow)
                            iFlag1 = True : iFlag2 = True
                        End If

                        iRow = rpt_Data.Tables("ArutiTable").NewRow

                        iRow.Item("Column1") = Space(5) & dtOUT(i).Item("ename")
                        iRow.Item("Column2") = dtOUT(i).Item("job")
                        iRow.Item("Column3") = eZeeDate.convertDate(dtOUT(i).Item("aDate")).ToShortDateString
                        iRow.Item("Column4") = mstrAllocationTypeName & " : " & dtOUT(i).Item("to_dept")
                        iRow.Item("Column20") = 0

                        rpt_Data.Tables("ArutiTable").Rows.Add(iRow)

                    Next
                    '***************************************** TRANSFER OUT BLOCK ENDS  *****************************************
                Next
            End If

            Call FilterTitleAndFilterQuery()
            objRpt = New ArutiReport.Designer.rptAllocationMovementReport
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 9, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 10, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 11, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 12, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 1, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 2, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtTranferDate", Language.getMessage(mstrModuleName, 3, "Tranfer Date"))
            Call ReportFunction.TextChange(objRpt, "txtTransferOperation", Language.getMessage(mstrModuleName, 4, "Transfer Operation"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " - [" & mstrReportTypeName & "]")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)


            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_AllocationMovement; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Generate_EmployeeMovement(ByVal strDatabaseName As String, _
                                              ByVal intUserUnkid As Integer, _
                                              ByVal intYearUnkid As Integer, _
                                              ByVal intCompanyUnkid As Integer, _
                                              ByVal dtPeriodStart As Date, _
                                              ByVal dtPeriodEnd As Date, _
                                              ByVal strUserModeSetting As String, _
                                              ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Public Function Generate_EmployeeMovement() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END

            Dim iColumnName As String = String.Empty
            Dim iTableName As String = String.Empty

            Select Case mintAllocationTypeId
                Case enAllocation.BRANCH
                    iColumnName = "stationunkid"
                    iTableName = "hrstation_master"
                Case enAllocation.DEPARTMENT_GROUP
                    iColumnName = "deptgroupunkid"
                    iTableName = "hrdepartment_group_master"
                Case enAllocation.DEPARTMENT
                    iColumnName = "departmentunkid"
                    iTableName = "hrdepartment_master"
                Case enAllocation.SECTION_GROUP
                    iColumnName = "sectiongroupunkid"
                    iTableName = "hrsectiongroup_master"
                Case enAllocation.SECTION
                    iColumnName = "sectionunkid"
                    iTableName = "hrsection_master"
                Case enAllocation.UNIT_GROUP
                    iColumnName = "unitgroupunkid"
                    iTableName = "hrunitgroup_master"
                Case enAllocation.UNIT
                    iColumnName = "unitunkid"
                    iTableName = "hrunit_master"
                Case enAllocation.TEAM
                    iColumnName = "teamunkid"
                    iTableName = "hrteam_master"
                Case enAllocation.JOB_GROUP
                    iColumnName = "jobgroupunkid"
                    iTableName = "hrjobgroup_master"
                Case enAllocation.JOBS
                    iColumnName = "jobunkid"
                    iTableName = "hrjob_master"
                Case enAllocation.CLASS_GROUP
                    iColumnName = "classgroupunkid"
                    iTableName = "hrclassgroup_master"
                Case enAllocation.CLASSES
                    iColumnName = "classunkid"
                    iTableName = "hrclasses_master"
            End Select


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
            '       "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT " & _
            '       "SET @tallocid = 0 " & _
            '       "SET @Id = 0 " & _
            '       "DECLARE EM CURSOR FOR SELECT employeeunkid, " & iColumnName & " ,auditdatetime FROM athremployee_master WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' ORDER BY employeeunkid,auditdatetime ASC " & _
            '       "OPEN EM " & _
            '       "FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
            '       "WHILE @@FETCH_STATUS = 0 " & _
            '       "BEGIN " & _
            '                 "IF @fallocid <> @tallocid " & _
            '                 "BEGIN " & _
            '                      "INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
            '                      "SET @Id = @@IDENTITY; " & _
            '                      "UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
            '                     "SET @tallocid = @fallocid " & _
            '                 "END " & _
            '            "FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
            '       "END " & _
            '       "CLOSE EM " & _
            '       "DEALLOCATE EM " & _
            '       "SELECT "

            Dim StrCursorQuery As String
            StrCursorQuery = ""

            StrCursorQuery = GenerateCursorQry(iTableName, iColumnName)


            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
            '       "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT " & _
            '       "SET @tallocid = 0 " & _
            '       "SET @Id = 0 " & _
            '       "DECLARE EM CURSOR FOR " & StrCursorQuery & " " & _
            '       "    OPEN EM " & _
            '       "        FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
            '       "            WHILE @@FETCH_STATUS = 0 " & _
            '       "                BEGIN " & _
            '       "                    IF @fallocid <> @tallocid " & _
            '       "                        BEGIN " & _
            '       "                            INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
            '       "                            SET @Id = @@IDENTITY; " & _
            '       "                            UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
            '       "                            SET @tallocid = @fallocid " & _
            '       "                        END " & _
            '       "                    FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
            '       "                END " & _
            '       "    CLOSE EM " & _
            '       "DEALLOCATE EM " & _
            '       "SELECT "

            StrQ = "DECLARE @EM AS TABLE(id INT IDENTITY,empid INT,fallocid INT,tallocid INT DEFAULT 0,adate DATETIME) " & _
                   "DECLARE @fallocid INT,@tallocid INT,@empid INT,@date DATETIME,@Id INT, @fempid INT " & _
                   "SET @tallocid = 0 " & _
                   "SET @Id = 0 " & _
                   "DECLARE EM CURSOR FOR " & StrCursorQuery & " " & _
                   "    OPEN EM " & _
                   "        FETCH NEXT FROM EM INTO @empid, @fallocid, @date " & _
                   "            WHILE @@FETCH_STATUS = 0 " & _
                   "                BEGIN " & _
                   "                    IF (@fallocid <> @tallocid OR @fempid <> @empid) " & _
                   "                        BEGIN " & _
                   "                            INSERT INTO @EM(empid,fallocid,tallocid,adate)SELECT @empid, @fallocid, 0,@date " & _
                   "                            SET @Id = @@IDENTITY; " & _
                   "                            UPDATE @EM SET tallocid = @fallocid,adate = @date WHERE id = @Id - 1 AND empid = @empid " & _
                   "                            SET @tallocid = @fallocid " & _
                   "                            SET @fempid = @empid " & _
                   "                        END " & _
                   "                    FETCH NEXT FROM EM into @empid, @fallocid, @date " & _
                   "                END " & _
                   "    CLOSE EM " & _
                   "DEALLOCATE EM " & _
                   "SELECT "
            'S.SANDEEP [20 MAY 2016] -- END



            'S.SANDEEP [04 JUN 2015] -- END


            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("hremployee_master.surname +' '+ hremployee_master.firstname", Language.getMessage(mstrModuleName, 10, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                StrQ &= " employeecode+' - '+ISNULL(surname,'')+' '+ISNULL(firstname,'')+' '+ISNULL(othername,'') AS ename "
            Else
                StrQ &= " employeecode+' - '+ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ename "
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ &= ",JB.job_name AS job " & _
            '        ",CONVERT(CHAR(8),adate,112) AS aDate "

            StrQ &= ",ISNULL(JB.job_name,'') AS job " & _
                    ",CONVERT(CHAR(8),adate,112) AS aDate "
            'S.SANDEEP [20 MAY 2016] -- END

            If iTableName = "hrjob_master" Then
                StrQ &= ",'('+ fdept.job_code +') '+ fdept.job_name AS frm_dept " & _
                        ",'('+ tdept.job_code +') '+ tdept.job_name AS to_dept "
            Else
                StrQ &= ",'('+ fdept.code +') '+ fdept.name AS frm_dept " & _
                        ",'('+ tdept.code +') '+ tdept.name AS to_dept "
            End If

            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ &= "FROM @EM " & _
            '        "   LEFT JOIN " & iTableName & " fdept ON fdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].fallocid " & _
            '        "   LEFT JOIN " & iTableName & " tdept ON tdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].tallocid " & _
            '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = [@EM].empid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            employeeunkid " & _
            '        "           ,jobunkid " & _
            '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "       FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
            '        "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '        "   ) AS CR ON CR.employeeunkid  = hremployee_master.employeeunkid AND CR.rno = 1 " & _
            '        "   JOIN hrjob_master AS JB ON CR.jobunkid = JB.jobunkid "

            StrQ &= "FROM @EM " & _
                        "LEFT JOIN " & iTableName & " fdept ON fdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].fallocid " & _
                        "LEFT JOIN " & iTableName & " tdept ON tdept." & IIf(iColumnName = "classunkid", "classesunkid", iColumnName) & " =  [@EM].tallocid " & _
                        "JOIN hremployee_master ON hremployee_master.employeeunkid = [@EM].empid "
            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            StrQ &= "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            employeeunkid " & _
                        "           ,jobunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "       FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
                    "       AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' "
            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_categorization_tran.employeeunkid = '" & mintEmployeeId & "' "
            End If
            StrQ &= "   ) AS CR ON CR.employeeunkid  = hremployee_master.employeeunkid AND CR.rno = 1 " & _
                    "   LEFT JOIN hrjob_master AS JB ON CR.jobunkid = JB.jobunkid "
            'S.SANDEEP [20 MAY 2016] -- END

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
            End If




            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    StrQ &= " AND " & mstrAdvance_Filter
            'End If
            'If mblnIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
            'End If


            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END
            

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [20 MAY 2016] -- START
            'StrQ &= "WHERE [@EM].tallocid > 0 "
            StrQ &= "WHERE 1 = 1 AND [@EM].tallocid > 0 "
            'S.SANDEEP [20 MAY 2016] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP [23 MAR 2016] -- START
            'LEAVE OPTIMIZATION
            StrQ &= " ORDER BY [@EM].adate "
            'S.SANDEEP [23 MAR 2016] -- END


            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

            Call FilterTitleAndFilterQuery()

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dRow As DataRow In dsList.Tables("List").Rows
                Dim iRow As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                iRow.Item("Column1") = dRow.Item("ename")
                iRow.Item("Column2") = dRow.Item("job")
                iRow.Item("Column3") = eZeeDate.convertDate(dRow.Item("aDate").ToString).ToShortDateString
                iRow.Item("Column4") = dRow.Item("frm_dept")
                iRow.Item("Column5") = dRow.Item("to_dept")

                rpt_Data.Tables("ArutiTable").Rows.Add(iRow)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeMovementReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 9, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 10, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 11, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 12, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 1, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 2, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtTransferDate", Language.getMessage(mstrModuleName, 3, "Tranfer Date"))
            Call ReportFunction.TextChange(objRpt, "txtFromAllocation", Language.getMessage(mstrModuleName, 6, "Transfer From"))
            Call ReportFunction.TextChange(objRpt, "txtToAllocation", Language.getMessage(mstrModuleName, 8, "Transfer To"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " - [" & mstrReportTypeName & "]")
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_EmployeeMovement; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function GenerateCursorQry(ByVal strTableName As String, ByVal strColumnName As String) As String
        Dim StrQry As String = String.Empty
        Try
            'S.SANDEEP [20 MAY 2016] -- START
            'If strTableName <> "hrjob_master" Then
            '    StrQry = "SELECT " & _
            '             "	 employeeunkid " & _
            '             "	," & strColumnName & " " & _
            '             "	,effectivedate " & _
            '             "FROM " & _
            '             "( " & _
            '             "	SELECT " & _
            '             "		 employeeunkid " & _
            '             "		," & strColumnName & " " & _
            '             "		,effectivedate " & _
            '             "		,ROW_NUMBER()OVER(PARTITION BY employeeunkid," & strColumnName & " ORDER BY effectivedate ASC) AS rno " & _
            '             "	FROM hremployee_transfer_tran WHERE isvoid = 0 " & _
            '             "	AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2) & "' " & _
            '             ")AS AM WHERE AM.rno = 1 " & _
            '             "AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
            '             "ORDER BY employeeunkid,effectivedate DESC "
            'Else
            '    StrQry = "SELECT " & _
            '             "	 employeeunkid " & _
            '             "	," & strColumnName & " " & _
            '             "	,effectivedate " & _
            '             "FROM " & _
            '             "( " & _
            '             "	SELECT " & _
            '             "		 employeeunkid " & _
            '             "		," & strColumnName & " " & _
            '             "		,effectivedate " & _
            '             "		,ROW_NUMBER()OVER(PARTITION BY employeeunkid," & strColumnName & " ORDER BY effectivedate ASC) AS rno " & _
            '             "    FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
            '             "	  AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2) & "' " & _
            '             ") AS CM WHERE CM.rno = 1 " & _
            '             "AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
            '             "ORDER BY employeeunkid,effectivedate DESC "
            'End If

            If strTableName <> "hrjob_master" Then
                StrQry = "SELECT " & _
                         "	 employeeunkid " & _
                         "	," & strColumnName & " " & _
                         "	,effectivedate " & _
                         "FROM " & _
                         "( " & _
                         "	SELECT " & _
                         "		 employeeunkid " & _
                         "		," & strColumnName & " " & _
                         "		,effectivedate " & _
                         "	FROM hremployee_transfer_tran WHERE isvoid = 0 " & _
                         "	AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2) & "' "
                If mintEmployeeId > 0 Then
                    StrQry &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
                End If
                StrQry &= ")AS AM WHERE 1 = 1 " & _
                         "AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
                          "ORDER BY employeeunkid,effectivedate "
            Else
                StrQry = "SELECT " & _
                         "	 employeeunkid " & _
                         "	," & strColumnName & " " & _
                         "	,effectivedate " & _
                         "FROM " & _
                         "( " & _
                         "	SELECT " & _
                         "		 employeeunkid " & _
                         "		," & strColumnName & " " & _
                         "		,effectivedate " & _
                         "    FROM hremployee_categorization_tran WHERE isvoid = 0 " & _
                         "	  AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2) & "' "
                If mintEmployeeId > 0 Then
                    StrQry &= " AND hremployee_transfer_tran.employeeunkid = '" & mintEmployeeId & "' "
                End If
                StrQry &= ") AS CM WHERE 1 = 1 " & _
                         "AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' " & _
                         "ORDER BY employeeunkid,effectivedate "
            End If
            'S.SANDEEP [20 MAY 2016] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrQry
    End Function
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee")
            Language.setMessage(mstrModuleName, 2, "Job")
            Language.setMessage(mstrModuleName, 3, "Tranfer Date")
            Language.setMessage(mstrModuleName, 4, "Transfer Operation")
            Language.setMessage(mstrModuleName, 5, "Transfer IN")
            Language.setMessage(mstrModuleName, 6, "Transfer From")
            Language.setMessage(mstrModuleName, 7, "Transfer OUT")
            Language.setMessage(mstrModuleName, 8, "Transfer To")
            Language.setMessage(mstrModuleName, 9, "Prepared By :")
            Language.setMessage(mstrModuleName, 10, "Checked By :")
            Language.setMessage(mstrModuleName, 11, "Approved By :")
            Language.setMessage(mstrModuleName, 12, "Received By :")
            Language.setMessage(mstrModuleName, 13, "Printed By :")
            Language.setMessage(mstrModuleName, 14, "Printed Date :")
            Language.setMessage(mstrModuleName, 15, "From Date :")
            Language.setMessage(mstrModuleName, 16, "To")
            Language.setMessage(mstrModuleName, 17, "Employee :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeIdentitiesReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.LblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.dtpIssueDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblIssueDateTo = New System.Windows.Forms.Label
        Me.dtpIssueDateFrom = New System.Windows.Forms.DateTimePicker
        Me.LblIssueDateFrom = New System.Windows.Forms.Label
        Me.dtpExpiryDateTo = New System.Windows.Forms.DateTimePicker
        Me.LblExpiryDateTo = New System.Windows.Forms.Label
        Me.LblExpiryDateFrom = New System.Windows.Forms.Label
        Me.dtpExpiryDateFrom = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 512)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.LblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.dtpIssueDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.LblIssueDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpIssueDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.LblIssueDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpExpiryDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpiryDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.LblExpiryDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpExpiryDateFrom)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(399, 136)
        Me.gbFilterCriteria.TabIndex = 13
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(302, 5)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 104
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(371, 31)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 55
        '
        'LblEmployee
        '
        Me.LblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployee.Location = New System.Drawing.Point(5, 34)
        Me.LblEmployee.Name = "LblEmployee"
        Me.LblEmployee.Size = New System.Drawing.Size(104, 15)
        Me.LblEmployee.TabIndex = 102
        Me.LblEmployee.Text = "Employee"
        Me.LblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(114, 31)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(251, 21)
        Me.cboEmployee.TabIndex = 54
        '
        'dtpIssueDateTo
        '
        Me.dtpIssueDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDateTo.Checked = False
        Me.dtpIssueDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIssueDateTo.Location = New System.Drawing.Point(263, 86)
        Me.dtpIssueDateTo.Name = "dtpIssueDateTo"
        Me.dtpIssueDateTo.ShowCheckBox = True
        Me.dtpIssueDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpIssueDateTo.TabIndex = 101
        '
        'LblIssueDateTo
        '
        Me.LblIssueDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIssueDateTo.Location = New System.Drawing.Point(222, 89)
        Me.LblIssueDateTo.Name = "LblIssueDateTo"
        Me.LblIssueDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblIssueDateTo.TabIndex = 100
        Me.LblIssueDateTo.Text = "To"
        Me.LblIssueDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpIssueDateFrom
        '
        Me.dtpIssueDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDateFrom.Checked = False
        Me.dtpIssueDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIssueDateFrom.Location = New System.Drawing.Point(114, 86)
        Me.dtpIssueDateFrom.Name = "dtpIssueDateFrom"
        Me.dtpIssueDateFrom.ShowCheckBox = True
        Me.dtpIssueDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpIssueDateFrom.TabIndex = 99
        '
        'LblIssueDateFrom
        '
        Me.LblIssueDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblIssueDateFrom.Location = New System.Drawing.Point(5, 89)
        Me.LblIssueDateFrom.Name = "LblIssueDateFrom"
        Me.LblIssueDateFrom.Size = New System.Drawing.Size(104, 15)
        Me.LblIssueDateFrom.TabIndex = 98
        Me.LblIssueDateFrom.Text = "Issue Date From"
        Me.LblIssueDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpExpiryDateTo
        '
        Me.dtpExpiryDateTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDateTo.Checked = False
        Me.dtpExpiryDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExpiryDateTo.Location = New System.Drawing.Point(263, 58)
        Me.dtpExpiryDateTo.Name = "dtpExpiryDateTo"
        Me.dtpExpiryDateTo.ShowCheckBox = True
        Me.dtpExpiryDateTo.Size = New System.Drawing.Size(102, 21)
        Me.dtpExpiryDateTo.TabIndex = 97
        '
        'LblExpiryDateTo
        '
        Me.LblExpiryDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpiryDateTo.Location = New System.Drawing.Point(222, 61)
        Me.LblExpiryDateTo.Name = "LblExpiryDateTo"
        Me.LblExpiryDateTo.Size = New System.Drawing.Size(35, 15)
        Me.LblExpiryDateTo.TabIndex = 96
        Me.LblExpiryDateTo.Text = "To"
        Me.LblExpiryDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblExpiryDateFrom
        '
        Me.LblExpiryDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExpiryDateFrom.Location = New System.Drawing.Point(5, 61)
        Me.LblExpiryDateFrom.Name = "LblExpiryDateFrom"
        Me.LblExpiryDateFrom.Size = New System.Drawing.Size(104, 15)
        Me.LblExpiryDateFrom.TabIndex = 95
        Me.LblExpiryDateFrom.Text = "Expiry Date From"
        Me.LblExpiryDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpExpiryDateFrom
        '
        Me.dtpExpiryDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDateFrom.Checked = False
        Me.dtpExpiryDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExpiryDateFrom.Location = New System.Drawing.Point(114, 58)
        Me.dtpExpiryDateFrom.Name = "dtpExpiryDateFrom"
        Me.dtpExpiryDateFrom.ShowCheckBox = True
        Me.dtpExpiryDateFrom.Size = New System.Drawing.Size(102, 21)
        Me.dtpExpiryDateFrom.TabIndex = 94
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 210)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(399, 63)
        Me.gbSortBy.TabIndex = 103
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(372, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(66, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(90, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(278, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(116, 113)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(249, 16)
        Me.chkInactiveemp.TabIndex = 106
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'frmEmployeeIdentitiesReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 567)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeIdentitiesReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = ""
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dtpIssueDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblIssueDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpIssueDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblIssueDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpExpiryDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblExpiryDateTo As System.Windows.Forms.Label
    Friend WithEvents LblExpiryDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpExpiryDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
End Class

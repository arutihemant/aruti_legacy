'************************************************************************************************************************************
'Class Name : clsAssessmentListReport.vb
'Purpose    :
'Date       :01/08/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsAssessmentListReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsAssessmentListReport"
    Private mstrReportId As String = enArutiReport.EmployeeAssessmentListReport '31
    Dim objDataOperation As clsDataOperation
    Dim dtFinalTable As DataTable

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintAssessorId As Integer = 0
    Private mstrAssessorName As String = String.Empty
    Private mintBranchId As Integer = 0
    Private mstrBranchName As String = String.Empty
    Private mintJobId As Integer = 0
    Private msteJobName As String = String.Empty
    Private mintResultId As Integer = 0
    Private mstrResultName As String = String.Empty
    Private mintAssessItemId As Integer = 0
    Private mstrAssessItemName As String = String.Empty
    Private mintAssessGrpId As Integer = 0
    Private mstrAssessGrpName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    'Sandeep [ 10 FEB 2011 ] -- Start
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = String.Empty
    'Sandeep [ 10 FEB 2011 ] -- End 


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End


    'S.SANDEEP [ 22 JULY 2011 ] -- START
    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
    Private mblnIncludeUncommited As Boolean = False
    'S.SANDEEP [ 22 JULY 2011 ] -- END 
    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _AssessorId() As Integer
        Set(ByVal value As Integer)
            mintAssessorId = value
        End Set
    End Property

    Public WriteOnly Property _AssessorName() As String
        Set(ByVal value As String)
            mstrAssessorName = value
        End Set
    End Property

    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _BranchName() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property

    Public WriteOnly Property _JobId() As Integer
        Set(ByVal value As Integer)
            mintJobId = value
        End Set
    End Property

    Public WriteOnly Property _JobName() As String
        Set(ByVal value As String)
            msteJobName = value
        End Set
    End Property

    Public WriteOnly Property _ResultId() As Integer
        Set(ByVal value As Integer)
            mintResultId = value
        End Set
    End Property

    Public WriteOnly Property _ResultName() As String
        Set(ByVal value As String)
            mstrResultName = value
        End Set
    End Property

    Public WriteOnly Property _AssessItemId() As Integer
        Set(ByVal value As Integer)
            mintAssessItemId = value
        End Set
    End Property

    Public WriteOnly Property _AssessItemName() As String
        Set(ByVal value As String)
            mstrAssessItemName = value
        End Set
    End Property

    Public WriteOnly Property _AssessGrpId() As Integer
        Set(ByVal value As Integer)
            mintAssessGrpId = value
        End Set
    End Property

    Public WriteOnly Property _AssessGrpName() As String
        Set(ByVal value As String)
            mstrAssessGrpName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End


    'S.SANDEEP [ 22 JULY 2011 ] -- START
    'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
    Public WriteOnly Property _IncludeUncommited() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeUncommited = value
        End Set
    End Property
    'S.SANDEEP [ 22 JULY 2011 ] -- END 

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintAssessorId = 0
            mstrAssessorName = ""
            mintBranchId = 0
            mstrBranchName = ""
            mintJobId = 0
            msteJobName = ""
            mintResultId = 0
            mstrResultName = ""
            mintAssessItemId = 0
            mstrAssessItemName = ""
            mintAssessGrpId = 0
            mstrAssessGrpName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            mblnIncludeUncommited = False
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@SelfAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Self Assessment"))
            objDataOperation.AddParameter("@AssessorAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Assessor Assessment"))
            objDataOperation.AddParameter("@Level", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 28, "Level"))

            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            objDataOperation.AddParameter("@Commited", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Commited"))
            objDataOperation.AddParameter("@UCommit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "Uncommited"))
            'S.SANDEEP [ 22 JULY 2011 ] -- END 


            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Employee :") & " " & mstrEmployeeName & " "
            End If
           
            If mintAssessorId > 0 Then
                objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorId)
                Me._FilterQuery &= " AND AssessorId = @AssessorId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Assessor :") & " " & mstrAssessorName & " "
            End If
            
            If mintBranchId > 0 Then
                objDataOperation.AddParameter("@BranchId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchId)
                Me._FilterQuery &= " AND BranchId = @BranchId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Branch :") & " " & mstrBranchName & " "
            End If
            
            If mintJobId > 0 Then
                objDataOperation.AddParameter("@JobId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobId)
                Me._FilterQuery &= " AND JobId = @JobId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Job :") & " " & msteJobName & " "
            End If
           
            If mintResultId > 0 Then
                objDataOperation.AddParameter("@ResultId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultId)
                Me._FilterQuery &= " AND ResultId = @ResultId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Score :") & " " & mstrResultName & " "
            End If
            
            If mintAssessItemId > 0 Then
                objDataOperation.AddParameter("@AssessItemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessItemId)
                Me._FilterQuery &= " AND ItemId = @AssessItemId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, "Assessment Item :") & " " & mstrAssessItemName & " "
            End If
           
            If mintAssessGrpId > 0 Then
                objDataOperation.AddParameter("@AssessGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessGrpId)
                Me._FilterQuery &= " AND AssessGrpId = @AssessGrpId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 21, "Assessment Group :") & " " & mstrAssessGrpName & " "
            End If

            If mintPeriodId > 0 Then
                objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                Me._FilterQuery &= " AND PeriodId = @PeriodId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, "Period :") & " " & mstrPeriodName & " "
            End If
            
            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FilterTitleAndFilterQuery", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setDefaultOrderBy", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "setOrderBy", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 10 FEB 2011 ] -- Start
    Public Function Get_ReportType(Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id,@List AS Name " & _
                   "UNION SELECT 1 AS Id,@ItemWise AS Name " & _
                   "UNION SELECT 2 AS Id,@GroupWise AS Name "

            objDataOperation.AddParameter("@List", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "List"))
            objDataOperation.AddParameter("@ItemWise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Item Wise"))
            objDataOperation.AddParameter("@GroupWise", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Group Wise"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_ReportType", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sandeep [ 10 FEB 2011 ] -- End

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = Value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 24, "Employee")))
            iColumn_DetailReport.Add(New IColumn("Branch", Language.getMessage(mstrModuleName, 25, "Branch")))
            iColumn_DetailReport.Add(New IColumn("Job", Language.getMessage(mstrModuleName, 26, "Job")))
            iColumn_DetailReport.Add(New IColumn("Period", Language.getMessage(mstrModuleName, 8, "Period")))
            iColumn_DetailReport.Add(New IColumn("AssessGroup", Language.getMessage(mstrModuleName, 9, "Assessment Group")))
            iColumn_DetailReport.Add(New IColumn("AssessItem", Language.getMessage(mstrModuleName, 10, "Assessment Item")))
            iColumn_DetailReport.Add(New IColumn("Assessor", Language.getMessage(mstrModuleName, 27, "Assessor")))
            iColumn_DetailReport.Add(New IColumn("Score", Language.getMessage(mstrModuleName, 11, "Score")))

        Catch ex As Exception
            DisplayError.Show(-1, ex.message, "Create_OnDetailReport", mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataoperation


            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            'StrQ = "SELECT " & _
            '        "	 EmpId AS EmpId  " & _
            '        "	,SelfId AS SelfId  " & _
            '        "	,EName AS EName  " & _
            '        "	,Branch AS Branch  " & _
            '        "	,Job AS Job  " & _
            '        "	,AssessDate AS AssessDate  " & _
            '        "	,Period AS Period  " & _
            '        "	,AssessGroup AS AssessGroup  " & _
            '        "	,AssessItem AS AssessItem  " & _
            '        "	,Score AS Score  " & _
            '        "	,Strength AS Strength  " & _
            '        "	,Improvement AS Improvement  " & _
            '        "	,TrainingNeed AS TrainingNeed  " & _
            '        "	,Remark AS Remark  " & _
            '        "	,Assessment AS Assessment  " & _
            '        "	,Assessor AS Assessor  " & _
            '        "FROM   " & _
            '        "(  " & _
            '        "		SELECT  " & _
            '        "			 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName  " & _
            '        "			,ISNULL(hrstation_master.name,'') AS Branch  " & _
            '        "			,ISNULL(hrjob_master.job_name,'')+' '+ @Level+' '+ CAST(hrjob_master.job_level AS NVARCHAR(10)) AS Job  " & _
            '        "			,CONVERT(CHAR(8),assessmentdate,112) AS AssessDate  " & _
            '        "			,ISNULL(cfcommon_period_tran.period_name,'') AS Period  " & _
            '        "			,ISNULL(hrassess_group_master.assessgroup_name,'') AS AssessGroup  " & _
            '        "			,ISNULL(hrassess_item_master.name,'') AS AssessItem  " & _
            '        "			,ISNULL(hrresult_master.resultname,'') AS Score  " & _
            '        "			,ISNULL(hrassess_analysis_master.strength,'') AS Strength  " & _
            '        "			,ISNULL(hrassess_analysis_master.improvement,'') AS Improvement  " & _
            '        "			,ISNULL(hrassess_analysis_master.trainingneed,'') AS TrainingNeed  " & _
            '        "			,ISNULL(hrassess_analysis_master.remark,'') AS Remark  " & _
            '        "			,hremployee_master.employeeunkid AS EmpId  " & _
            '        "			,1 AS SelfId  " & _
            '        "			,@SelfAssessment AS Assessment  " & _
            '        "			,hrassess_analysis_master.assessormasterunkid AS AssessorId  " & _
            '        "			,hrassess_analysis_master.assessoremployeeunkid AS AssessorEmpId  " & _
            '        "			,'' AS Assessor  " & _
            '        "			,hrjob_master.jobunkid AS JobId  " & _
            '        "			,hrassess_group_master.assessgroupunkid AS AssessGrpId  " & _
            '        "			,hrassess_item_master.assessitemunkid AS ItemId  " & _
            '        "			,hrstation_master.stationunkid AS BranchId  " & _
            '        "			,hrresult_master.resultunkid AS ResultId  " & _
            '        "           ,cfcommon_period_tran.periodunkid AS PeriodId " & _
            '        "		FROM hrassess_analysis_master  " & _
            '        "			JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid  " & _
            '        "			JOIN hrassess_item_master ON hrassess_analysis_master.assessitemunkid = hrassess_item_master.assessitemunkid  " & _
            '        "			JOIN hrresult_master ON hrassess_analysis_master.resultcodeunkid = hrresult_master.resultunkid  " & _
            '        "			JOIN cfcommon_period_tran ON hrassess_analysis_master.periodunkid = cfcommon_period_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Assessment & " " & _
            '        "			JOIN hremployee_master ON hrassess_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid  " & _
            '        "			JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid  " & _
            '        "			LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  " & _
            '        "		    WHERE selfemployeeunkid > -1 AND ISNULL(isselfassessed ,0) = 1  "

            'If mblnIsActive = False Then
            '    StrQ &= " AND hremployee_master.isactive  = 1 "
            'End If

            'StrQ &= "UNION  " & _
            '        "	SELECT  " & _
            '        "		 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName  " & _
            '        "		,ISNULL(hrstation_master.name,'') AS Branch  " & _
            '        "		,ISNULL(hrjob_master.job_name,'')+' '+ @Level+' '+ CAST(hrjob_master.job_level AS NVARCHAR(10)) AS Job  " & _
            '        "		,CONVERT(CHAR(8),assessmentdate,112) AS AssessDate  " & _
            '        "		,ISNULL(cfcommon_period_tran.period_name,'') AS Period  " & _
            '        "		,ISNULL(hrassess_group_master.assessgroup_name,'') AS AssessGroup  " & _
            '        "		,ISNULL(hrassess_item_master.name,'') AS AssessItem  " & _
            '        "		,ISNULL(hrresult_master.resultname,'') AS Score  " & _
            '        "		,ISNULL(hrassess_analysis_master.strength,'') AS Strength  " & _
            '        "		,ISNULL(hrassess_analysis_master.improvement,'') AS Improvement  " & _
            '        "		,ISNULL(hrassess_analysis_master.trainingneed,'') AS TrainingNeed  " & _
            '        "		,ISNULL(hrassess_analysis_master.remark,'') AS Remark  " & _
            '        "		,hremployee_master.employeeunkid AS EmpId  " & _
            '        "		,2 AS SelfId  " & _
            '        "		,@AssessorAssessment AS Assessment  " & _
            '        "		,hrassess_analysis_master.assessormasterunkid AS AssessorId  " & _
            '        "		,hrassess_analysis_master.assessoremployeeunkid AS AssessorEmpId  " & _
            '        "		,ISNULL(AEmp.firstname,'')+' '+ISNULL(AEmp.othername,'')+' '+ISNULL(AEmp.surname,'') AS Assessor  " & _
            '        "		,hrjob_master.jobunkid AS JobId  " & _
            '        "		,hrassess_group_master.assessgroupunkid AS AssessGrpId  " & _
            '        "		,hrassess_item_master.assessitemunkid AS ItemId  " & _
            '        "		,hrstation_master.stationunkid AS BranchId  " & _
            '        "		,hrresult_master.resultunkid AS ResultId  " & _
            '        "       ,cfcommon_period_tran.periodunkid AS PeriodId " & _
            '        "	FROM hrassess_analysis_master  " & _
            '        "		JOIN hremployee_master AS AEmp ON dbo.hrassess_analysis_master.assessoremployeeunkid = AEmp.employeeunkid  " & _
            '        "		JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid  " & _
            '        "		JOIN hrassess_item_master ON hrassess_analysis_master.assessitemunkid = hrassess_item_master.assessitemunkid  " & _
            '        "		JOIN hrresult_master ON hrassess_analysis_master.resultcodeunkid = hrresult_master.resultunkid  " & _
            '        "		JOIN cfcommon_period_tran ON hrassess_analysis_master.periodunkid = cfcommon_period_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Assessment & " " & _
            '        "		JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid  " & _
            '        "		JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid  " & _
            '        "		LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  " & _
            '        "	WHERE assessedemployeeunkid > -1 AND isselfassessed  = 0  "

            'If mblnIsActive = False Then
            '    StrQ &= " AND hremployee_master.isactive = 1 "
            'End If

            'StrQ &= ") AS AssessList  " & _
            '        "WHERE 1=1   "

            StrQ = "SELECT " & _
                    "	 EmpId AS EmpId  " & _
                    "	,SelfId AS SelfId  " & _
                    "	,EName AS EName  " & _
                    "	,Branch AS Branch  " & _
                    "	,Job AS Job  " & _
                    "	,AssessDate AS AssessDate  " & _
                    "	,Period AS Period  " & _
                    "	,AssessGroup AS AssessGroup  " & _
                    "	,AssessItem AS AssessItem  " & _
                    "	,Score AS Score  " & _
                    "	,Strength AS Strength  " & _
                    "	,Improvement AS Improvement  " & _
                    "	,TrainingNeed AS TrainingNeed  " & _
                    "	,Remark AS Remark  " & _
                    "	,Assessment AS Assessment  " & _
                    "	,Assessor AS Assessor  " & _
                    "   ,Status AS Status " & _
                    "FROM   " & _
                    "(  " & _
                    "		SELECT  "

            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '" ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName  " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END


            StrQ &= "			,ISNULL(hrstation_master.name,'') AS Branch  " & _
                    "			,ISNULL(hrjob_master.job_name,'')+' '+ @Level+' '+ CAST(hrjob_master.job_level AS NVARCHAR(10)) AS Job  " & _
                    "			,CONVERT(CHAR(8),assessmentdate,112) AS AssessDate  " & _
                    "			,ISNULL(cfcommon_period_tran.period_name,'') AS Period  " & _
                    "			,ISNULL(hrassess_group_master.assessgroup_name,'') AS AssessGroup  " & _
                    "			,ISNULL(hrassess_item_master.name,'') AS AssessItem  " & _
                    "			,ISNULL(hrresult_master.resultname,'') AS Score  " & _
                    "			,ISNULL(hrassess_analysis_master.strength,'') AS Strength  " & _
                    "			,ISNULL(hrassess_analysis_master.improvement,'') AS Improvement  " & _
                    "			,ISNULL(hrassess_analysis_master.trainingneed,'') AS TrainingNeed  " & _
                    "			,ISNULL(hrassess_analysis_master.remark,'') AS Remark  " & _
                    "			,hremployee_master.employeeunkid AS EmpId  " & _
                    "			,1 AS SelfId  " & _
                    "			,@SelfAssessment AS Assessment  " & _
                    "			,hrassess_analysis_master.assessormasterunkid AS AssessorId  " & _
                    "			,hrassess_analysis_master.assessoremployeeunkid AS AssessorEmpId  " & _
                    "			,'' AS Assessor  " & _
                    "			,hrjob_master.jobunkid AS JobId  " & _
                    "			,hrassess_group_master.assessgroupunkid AS AssessGrpId  " & _
                    "			,hrassess_item_master.assessitemunkid AS ItemId  " & _
                    "			,hrstation_master.stationunkid AS BranchId  " & _
                    "			,hrresult_master.resultunkid AS ResultId  " & _
                    "           ,cfcommon_period_tran.periodunkid AS PeriodId " & _
                    "           ,hrassess_analysis_master.iscommitted AS iscommitted " & _
                    "           ,CASE WHEN iscommitted = 1 THEN @Commited " & _
                    "                 WHEN iscommitted = 0 THEN @UCommit END AS Status " & _
                    "		FROM hrassess_analysis_master  " & _
                    "			JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid  " & _
                    "			JOIN hrassess_item_master ON hrassess_analysis_master.assessitemunkid = hrassess_item_master.assessitemunkid  " & _
                    "			JOIN hrresult_master ON hrassess_analysis_master.resultcodeunkid = hrresult_master.resultunkid  " & _
                    "			JOIN cfcommon_period_tran ON hrassess_analysis_master.periodunkid = cfcommon_period_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Assessment & " " & _
                    "			JOIN hremployee_master ON hrassess_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid  " & _
                    "			JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid  " & _
                    "			LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  " & _
                    "		    WHERE selfemployeeunkid > -1 AND ISNULL(isselfassessed ,0) = 1  "

            If mblnIsActive = False Then
                StrQ &= " AND hremployee_master.isactive  = 1 "
            End If

            StrQ &= "UNION  " & _
                    "	SELECT  "
            'S.SANDEEP [ 26 MAR 2014 ] -- START
            '"		 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName  " & _
            If mblnFirstNamethenSurname = False Then
                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') AS EName "
            Else
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                            "+ ISNULL(hremployee_master.surname, '') AS EName "
            End If
            'S.SANDEEP [ 26 MAR 2014 ] -- END

            StrQ &= "		,ISNULL(hrstation_master.name,'') AS Branch  " & _
                    "		,ISNULL(hrjob_master.job_name,'') AS Job  " & _
                    "		,CONVERT(CHAR(8),assessmentdate,112) AS AssessDate  " & _
                    "		,ISNULL(cfcommon_period_tran.period_name,'') AS Period  " & _
                    "		,ISNULL(hrassess_group_master.assessgroup_name,'') AS AssessGroup  " & _
                    "		,ISNULL(hrassess_item_master.name,'') AS AssessItem  " & _
                    "		,ISNULL(hrresult_master.resultname,'') AS Score  " & _
                    "		,ISNULL(hrassess_analysis_master.strength,'') AS Strength  " & _
                    "		,ISNULL(hrassess_analysis_master.improvement,'') AS Improvement  " & _
                    "		,ISNULL(hrassess_analysis_master.trainingneed,'') AS TrainingNeed  " & _
                    "		,ISNULL(hrassess_analysis_master.remark,'') AS Remark  " & _
                    "		,hremployee_master.employeeunkid AS EmpId  " & _
                    "		,2 AS SelfId  " & _
                    "		,@AssessorAssessment AS Assessment  " & _
                    "		,hrassess_analysis_master.assessormasterunkid AS AssessorId  " & _
                    "		,hrassess_analysis_master.assessoremployeeunkid AS AssessorEmpId  " & _
                    "		,ISNULL(AEmp.firstname,'')+' '+ISNULL(AEmp.othername,'')+' '+ISNULL(AEmp.surname,'') AS Assessor  " & _
                    "		,hrjob_master.jobunkid AS JobId  " & _
                    "		,hrassess_group_master.assessgroupunkid AS AssessGrpId  " & _
                    "		,hrassess_item_master.assessitemunkid AS ItemId  " & _
                    "		,hrstation_master.stationunkid AS BranchId  " & _
                    "		,hrresult_master.resultunkid AS ResultId  " & _
                    "       ,cfcommon_period_tran.periodunkid AS PeriodId " & _
                    "       ,hrassess_analysis_master.iscommitted AS iscommitted " & _
                    "       ,CASE WHEN iscommitted = 1 THEN @Commited " & _
                    "             WHEN iscommitted = 0 THEN @UCommit END AS Status " & _
                    "	FROM hrassess_analysis_master  " & _
                    "		JOIN hremployee_master AS AEmp ON dbo.hrassess_analysis_master.assessoremployeeunkid = AEmp.employeeunkid  " & _
                    "		JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid  " & _
                    "		JOIN hrassess_item_master ON hrassess_analysis_master.assessitemunkid = hrassess_item_master.assessitemunkid  " & _
                    "		JOIN hrresult_master ON hrassess_analysis_master.resultcodeunkid = hrresult_master.resultunkid  " & _
                    "		JOIN cfcommon_period_tran ON hrassess_analysis_master.periodunkid = cfcommon_period_tran.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Assessment & " " & _
                    "		JOIN hremployee_master ON hrassess_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid  " & _
                    "		JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid  " & _
                    "		LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid  " & _
                    "	WHERE assessedemployeeunkid > -1 AND isselfassessed  = 0  "

            If mblnIsActive = False Then
                StrQ &= " AND hremployee_master.isactive = 1 "
            End If

            StrQ &= ") AS AssessList  " & _
                    "WHERE 1=1   "
            If mblnIncludeUncommited = False Then
                StrQ &= " AND iscommitted = 1 "
            End If
            StrQ &= ""
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmpId")
                rpt_Row.Item("Column2") = dtRow.Item("SelfId")
                rpt_Row.Item("Column3") = dtRow.Item("EName")
                rpt_Row.Item("Column4") = dtRow.Item("Branch")
                rpt_Row.Item("Column5") = dtRow.Item("Job")
                rpt_Row.Item("Column6") = eZeeDate.convertDate(dtRow.Item("AssessDate").ToString).ToShortDateString
                rpt_Row.Item("Column7") = dtRow.Item("Period")

                'Sandeep [ 10 FEB 2011 ] -- Start
                'rpt_Row.Item("Column8") = dtRow.Item("AssessGroup")
                'rpt_Row.Item("Column9") = dtRow.Item("AssessItem")
                Select Case mintReportTypeId
                    Case 0, 1
                rpt_Row.Item("Column8") = dtRow.Item("AssessGroup")
                rpt_Row.Item("Column9") = dtRow.Item("AssessItem")
                    Case 2
                        rpt_Row.Item("Column8") = dtRow.Item("AssessItem")
                        rpt_Row.Item("Column9") = dtRow.Item("AssessGroup")
                End Select
                'Sandeep [ 10 FEB 2011 ] -- End 
                rpt_Row.Item("Column10") = dtRow.Item("Score")
                rpt_Row.Item("Column11") = dtRow.Item("Strength")
                rpt_Row.Item("Column12") = dtRow.Item("Improvement")
                rpt_Row.Item("Column13") = dtRow.Item("TrainingNeed")
                rpt_Row.Item("Column14") = dtRow.Item("Remark")
                rpt_Row.Item("Column15") = dtRow.Item("Assessment")
                rpt_Row.Item("Column16") = dtRow.Item("Assessor")

                'S.SANDEEP [ 22 JULY 2011 ] -- START
                'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
                rpt_Row.Item("Column17") = dtRow.Item("Status")
                'S.SANDEEP [ 22 JULY 2011 ] -- END 

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            'Sandeep [ 10 FEB 2011 ] -- Start
            'objRpt = New ArutiReport.Designer.rptAssessmentListReport
            Select Case mintReportTypeId
                Case 0  'List
            objRpt = New ArutiReport.Designer.rptAssessmentListReport
                Case 1, 2  'Item Wise & Group Wise
                    objRpt = New ArutiReport.Designer.rptAssessmentByReport
            End Select
            'Sandeep [ 10 FEB 2011 ] -- End 



            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                         ConfigParameter._Object._GetLeftMargin, _
                                         ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 34, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 35, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 36, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 37, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If
            'Sandeep [ 10 FEB 2011 ] -- End 


            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtAssessor", Language.getMessage(mstrModuleName, 3, "Assessor :"))
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 4, "Employee :"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 5, "Branch :"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 6, "Job :"))
            Call ReportFunction.TextChange(objRpt, "txtAssessDate", Language.getMessage(mstrModuleName, 7, "Assess Date"))
            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 8, "Period"))

            'Sandeep [ 10 FEB 2011 ] -- Start
            'Call ReportFunction.TextChange(objRpt, "txtAssessGroup", Language.getMessage(mstrModuleName, 9, "Assessment Group"))
            'Call ReportFunction.TextChange(objRpt, "txtAssessItem", Language.getMessage(mstrModuleName, 10, "Assessment Item"))
            Select Case mintReportTypeId
                Case 0
            Call ReportFunction.TextChange(objRpt, "txtAssessGroup", Language.getMessage(mstrModuleName, 9, "Assessment Group"))
            Call ReportFunction.TextChange(objRpt, "txtAssessItem", Language.getMessage(mstrModuleName, 10, "Assessment Item"))
                Case 1
                    Call ReportFunction.TextChange(objRpt, "txtAssessGroup", Language.getMessage(mstrModuleName, 9, "Assessment Group"))
                    Call ReportFunction.TextChange(objRpt, "txtAssessItem", Language.getMessage(mstrModuleName, 32, "Assessment Item :"))
                Case 2
                    Call ReportFunction.TextChange(objRpt, "txtAssessGroup", Language.getMessage(mstrModuleName, 10, "Assessment Item"))
                    Call ReportFunction.TextChange(objRpt, "txtAssessItem", Language.getMessage(mstrModuleName, 33, "Assessment Group :"))
            End Select
            'Sandeep [ 10 FEB 2011 ] -- End 
            Call ReportFunction.TextChange(objRpt, "txtScore", Language.getMessage(mstrModuleName, 11, "Score"))
            Call ReportFunction.TextChange(objRpt, "txtStrength", Language.getMessage(mstrModuleName, 12, "Strength"))
            Call ReportFunction.TextChange(objRpt, "txtImprovement", Language.getMessage(mstrModuleName, 13, "Improvement"))
            Call ReportFunction.TextChange(objRpt, "txtTraining", Language.getMessage(mstrModuleName, 14, "Training"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 15, "Remark"))
            
            'S.SANDEEP [ 22 JULY 2011 ] -- START
            'ENHANCEMENT : WEB & DESKTOP COMMIT OPTION TO BE INCLUDED
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 40, "Status"))
            'S.SANDEEP [ 22 JULY 2011 ] -- END 

            
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 16, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 17, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 18, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            'Sandeep [ 10 FEB 2011 ] -- Start
            'Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " -[ " & mstrReportTypeName & " ]")
            'Sandeep [ 10 FEB 2011 ] -- End 
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            DisplayError.Show(-1, ex.message, "Generate_DetailReport", mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Self Assessment")
			Language.setMessage(mstrModuleName, 2, "Assessor Assessment")
			Language.setMessage(mstrModuleName, 3, "Assessor :")
			Language.setMessage(mstrModuleName, 4, "Employee :")
			Language.setMessage(mstrModuleName, 5, "Branch :")
			Language.setMessage(mstrModuleName, 6, "Job :")
			Language.setMessage(mstrModuleName, 7, "Assess Date")
			Language.setMessage(mstrModuleName, 8, "Period")
			Language.setMessage(mstrModuleName, 9, "Assessment Group")
			Language.setMessage(mstrModuleName, 10, "Assessment Item")
			Language.setMessage(mstrModuleName, 11, "Score")
			Language.setMessage(mstrModuleName, 12, "Strength")
			Language.setMessage(mstrModuleName, 13, "Improvement")
			Language.setMessage(mstrModuleName, 14, "Training")
			Language.setMessage(mstrModuleName, 15, "Remark")
			Language.setMessage(mstrModuleName, 16, "Printed By :")
			Language.setMessage(mstrModuleName, 17, "Printed Date :")
			Language.setMessage(mstrModuleName, 18, "Page :")
			Language.setMessage(mstrModuleName, 19, "Score :")
			Language.setMessage(mstrModuleName, 20, "Assessment Item :")
			Language.setMessage(mstrModuleName, 21, "Assessment Group :")
			Language.setMessage(mstrModuleName, 22, "Period :")
			Language.setMessage(mstrModuleName, 23, "Order By :")
			Language.setMessage(mstrModuleName, 24, "Employee")
			Language.setMessage(mstrModuleName, 25, "Branch")
			Language.setMessage(mstrModuleName, 26, "Job")
			Language.setMessage(mstrModuleName, 27, "Assessor")
			Language.setMessage(mstrModuleName, 28, "Level")
			Language.setMessage(mstrModuleName, 29, "List")
			Language.setMessage(mstrModuleName, 30, "Item Wise")
			Language.setMessage(mstrModuleName, 31, "Group Wise")
			Language.setMessage(mstrModuleName, 32, "Assessment Item :")
			Language.setMessage(mstrModuleName, 33, "Assessment Group :")
			Language.setMessage(mstrModuleName, 34, "Prepared By :")
			Language.setMessage(mstrModuleName, 35, "Checked By :")
			Language.setMessage(mstrModuleName, 36, "Approved By :")
			Language.setMessage(mstrModuleName, 37, "Received By :")
			Language.setMessage(mstrModuleName, 38, "Commited")
			Language.setMessage(mstrModuleName, 39, "Uncommited")
			Language.setMessage(mstrModuleName, 40, "Status")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

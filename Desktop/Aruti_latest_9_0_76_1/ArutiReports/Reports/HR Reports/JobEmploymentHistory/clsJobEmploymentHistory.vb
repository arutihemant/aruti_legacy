'************************************************************************************************************************************
'Class Name : clsEmployeeHistory.vb
'Purpose    :
'Date       :29 March 2011
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Anjan
''' </summary>
Public Class clsJobEmploymentHistory
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeHistory"
    Private mstrReportID As String = CStr(enArutiReport.JobEmploymentHistory)
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportID), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "
    Private mdtStartDateFrom As DateTime = Nothing
    Private mdtStartDateTo As DateTime = Nothing
    Private mdtEndDateFrom As DateTime = Nothing
    Private mdtEndDateTo As DateTime = Nothing
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintEmployerId As Integer = 0
    Private mstrEmployerName As String = String.Empty
    Private mstrJob As String = String.Empty 'Sohail (12 Apr 2012)

    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    'Pinkal (14-Aug-2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrUserAccessFilter As String = String.Empty
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrAdvance_Filter As String = String.Empty
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnShowEmpScale As Boolean = False
    'Pinkal (24-Apr-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    'ENHANCEMENT : Requested by Rutta
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Private mblnIncludeAccessFilterQry As Boolean = True
    'Shani(15-Feb-2016) -- End
#End Region

#Region " Properties "
    Public WriteOnly Property _StartDateFrom() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _StartDateTo() As DateTime
        Set(ByVal value As DateTime)
            mdtStartDateTo = value
        End Set
    End Property

    Public WriteOnly Property _EndDateFrom() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _EndDateTo() As DateTime
        Set(ByVal value As DateTime)
            mdtEndDateTo = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployerId() As Integer
        Set(ByVal value As Integer)
            mintEmployerId = value
        End Set
    End Property

    Public WriteOnly Property _EmployerName() As String
        Set(ByVal value As String)
            mstrEmployerName = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Sohail (12 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Job() As String
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property
    'Sohail (12 Apr 2012) -- End


    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    'Pinkal (14-Aug-2012) -- End
    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ShowEmployeeScale() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmpScale = value
        End Set
    End Property

    'Pinkal (24-Apr-2013) -- End

    'S.SANDEEP [ 26 MAR 2014 ] -- START
    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property
    'S.SANDEEP [ 26 MAR 2014 ] -- END

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Public WriteOnly Property _IncludeAccessFilterQry() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property
    'Shani(15-Feb-2016) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mdtStartDateFrom = Nothing
            mdtStartDateTo = Nothing
            mdtEndDateFrom = Nothing
            mdtEndDateTo = Nothing
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mintEmployerId = 0
            mstrEmployerName = ""
            mstrJob = "" 'Sohail (12 Apr 2012)

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblIsActive = True
            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [ 13 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrAdvance_Filter = ""
            'S.SANDEEP [ 13 FEB 2013 ] -- END


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            mblnShowEmpScale = False
            'Pinkal (24-Apr-2013) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            mblnIncludeAccessFilterQry = True
            'Shani(15-Feb-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "No"))


            'objDataOperation.AddParameter("@StartDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateFrom))
            'objDataOperation.AddParameter("@StartDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateTo))
            'Me._FilterQuery &= ""
            'Me._FilterTitle &= ""

            'objDataOperation.AddParameter("@EndDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDateFrom))
            'objDataOperation.AddParameter("@EndDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDateTo))
            'Me._FilterQuery &= ""
            'Me._FilterTitle &= ""

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            If mblIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND hremp_experience_tran.employeeunkid = @employeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Employee name :") & " " & mstrEmployeeName & " "
            End If

            If mstrEmployerName.Trim <> "" Then
                objDataOperation.AddParameter("@Employername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployerName.ToString)
                Me._FilterQuery &= " AND hremp_experience_tran.company like '" & mstrEmployerName & "' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "Employer Name :") & " " & mstrEmployerName & " "
            End If

            If mdtStartDateFrom <> Nothing And mdtStartDateTo <> Nothing Then
                objDataOperation.AddParameter("@StartFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateFrom).ToString)
                objDataOperation.AddParameter("@StartToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartDateTo).ToString)
                Me._FilterQuery &= " AND CONVERT(CHAR(8),hremp_experience_tran.start_date,112) BETWEEN @StartFromDate AND @StartToDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Start Date From : ") & eZeeDate.convertDate(mdtStartDateFrom).ToString & Language.getMessage(mstrModuleName, 4, "To") & " " & eZeeDate.convertDate(mdtStartDateTo).ToString & " "
            End If

            If mdtEndDateFrom <> Nothing And mdtEndDateTo <> Nothing Then
                objDataOperation.AddParameter("@EndFromDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDateFrom).ToString)
                objDataOperation.AddParameter("@EndToDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndDateTo).ToString)
                Me._FilterQuery &= " AND CONVERT(CHAR(8),hremp_experience_tran.end_date,112) BETWEEN @EndFromDate AND @EndToDate "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "End Date From : ") & eZeeDate.convertDate(mdtEndDateFrom).ToString & Language.getMessage(mstrModuleName, 4, "To") & " " & eZeeDate.convertDate(mdtEndDateTo).ToString & " "
            End If

            'Sohail (12 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mstrJob.Trim <> "" Then
                objDataOperation.AddParameter("@job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob.ToString)
                Me._FilterQuery &= " AND hremp_experience_tran.old_job LIKE '%" & mstrJob & "%' "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 30, "Job Like :") & " " & mstrJob & " "
            End If
            'Sohail (12 Apr 2012) -- End


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try
        '    'Pinkal (14-Aug-2012) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If
        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid


        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid


        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (14-Aug-2012) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try


    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, _
                                           xUserUnkid, _
                                           xYearUnkid, _
                                           xCompanyUnkid, _
                                           xPeriodStart, _
                                           xPeriodEnd, _
                                           xUserModeSetting, _
                                           xOnlyApproved)
            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""

        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

        Try
            Call OrderByExecute(iColumn_DetailReport)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Public Sub Create_OnDetailReport()

        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("hremployee_master.employeecode", Language.getMessage(mstrModuleName, 9, "Employee Code")))
            'SANDEEP [ 26 MAR 2014 ] -- START
            'ENHANCEMENT : Requested by Rutta
            'iColumn_DetailReport.Add(New IColumn("hremployee_master.surname +' '+ hremployee_master.firstname", Language.getMessage(mstrModuleName, 10, "Employee Name")))
            If mblnFirstNamethenSurname = False Then
                iColumn_DetailReport.Add(New IColumn("hremployee_master.surname +' '+ hremployee_master.firstname", Language.getMessage(mstrModuleName, 10, "Employee Name")))
            Else
                iColumn_DetailReport.Add(New IColumn("hremployee_master.firstname +' '+ hremployee_master.surname", Language.getMessage(mstrModuleName, 10, "Employee Name")))
            End If
            'SANDEEP [ 26 MAR 2014 ] -- END

            iColumn_DetailReport.Add(New IColumn("hremp_experience_tran.start_date", Language.getMessage(mstrModuleName, 11, "Start Date")))
            iColumn_DetailReport.Add(New IColumn("hremp_experience_tran.end_date", Language.getMessage(mstrModuleName, 12, "End Date")))
            iColumn_DetailReport.Add(New IColumn("DATEDIFF(YEAR, hremp_experience_tran.start_date,hremp_experience_tran.end_date)", Language.getMessage(mstrModuleName, 13, "Years")))
            iColumn_DetailReport.Add(New IColumn("hremp_experience_tran.company", Language.getMessage(mstrModuleName, 14, "Employer")))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iColumn_DetailReport.Add(New IColumn("hrjob_master.job_name", Language.getMessage(mstrModuleName, 15, "Job")))
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremp_experience_tran.old_job, '')", Language.getMessage(mstrModuleName, 15, "Job")))
            'S.SANDEEP [04 JUN 2015] -- END


            If mblnShowEmpScale Then
                iColumn_DetailReport.Add(New IColumn("hremp_experience_tran.last_pay", Language.getMessage(mstrModuleName, 16, "Income")))
            End If

            'iColumn_DetailReport.Add(New IColumn("CASE WHEN iscontact_previous =1 THEN @No ELSE @Yes END", Language.getMessage(mstrModuleName, 208, "Objection")))
            'iColumn_DetailReport.Add(New IColumn("", ""))
            'iColumn_DetailReport.Add(New IColumn("", ""))
            'iColumn_DetailReport.Add(New IColumn("", ""))


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Apr-2013) -- End


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting) 'Shani(15-Feb-2016)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'S.SANDEEP [04 JUN 2015] -- END


            ''Sohail (12 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            ''strQ = "SELECT " & _
            ''        " hremployee_master.employeecode AS code " & _
            ''        ", hremployee_master.surname +' '+ hremployee_master.firstname AS empname " & _
            ''        ", CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS startdate " & _
            ''        ", CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS enddate " & _
            ''        ", DATEDIFF(YEAR, hremp_experience_tran.start_date,hremp_experience_tran.end_date) AS years " & _
            ''        ", hremp_experience_tran.company AS employer " & _
            ''        ", hrjob_master.job_name AS job " & _
            ''        ", hremp_experience_tran.last_pay AS income " & _
            ''        ", CASE WHEN iscontact_previous =1 THEN @No ELSE @Yes END AS objection  " & _
            ''        ", hremp_experience_tran.contact_person AS contactperson " & _
            ''        ", hremp_experience_tran.contact_no AS contactaddress " & _
            ''        ", hremp_experience_tran.leave_reason AS leavingreason " & _
            ''        ", hremp_experience_tran.remark AS remarks " & _
            ''    "FROM hremp_experience_tran " & _
            ''    "JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
            ''    "LEFT JOIN hrjob_master ON hrjob_master.jobunkid= hremp_experience_tran.jobunkid " & _
            ''    "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "
            'strQ = "SELECT  hremployee_master.employeecode AS code "

            ''SANDEEP [ 26 MAR 2014 ] -- START
            ''ENHANCEMENT : Requested by Rutta
            ''", hremployee_master.surname +' '+ hremployee_master.firstname AS empname "
            'If mblnFirstNamethenSurname = False Then
            '    strQ &= ", hremployee_master.surname +' '+ hremployee_master.firstname AS empname "
            'Else
            '    strQ &= ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname "
            'End If
            ''SANDEEP [ 26 MAR 2014 ] -- END

            'strQ &= ", CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS startdate " & _
            '        ", CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS enddate " & _
            '        ", DATEDIFF(YEAR, hremp_experience_tran.start_date,hremp_experience_tran.end_date) AS years " & _
            '        ", hremp_experience_tran.company AS employer " & _
            '              ", ISNULL(hremp_experience_tran.old_job, '') AS job " & _
            '        ", hremp_experience_tran.last_pay AS income " & _
            '        ", CASE WHEN iscontact_previous =1 THEN @No ELSE @Yes END AS objection  " & _
            '        ", hremp_experience_tran.contact_person AS contactperson " & _
            '        ", hremp_experience_tran.contact_no AS contactaddress " & _
            '        ", hremp_experience_tran.leave_reason AS leavingreason " & _
            '        ", hremp_experience_tran.remark AS remarks " & _
            '    "FROM hremp_experience_tran " & _
            '    "JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "
            ''Sohail (12 Apr 2012) -- End

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

            'If mblIsActive = False Then
            '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1  "
            '    'S.SANDEEP [ 12 MAY 2012 ] -- START
            '    'ISSUE : TRA ENHANCEMENTS

            '    strQ &= " AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            'End If

            ''S.SANDEEP [ 13 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'If mstrAdvance_Filter.Trim.Length > 0 Then
            '    strQ &= " AND " & mstrAdvance_Filter
            'End If
            ''S.SANDEEP [ 13 FEB 2013 ] -- END


            ''S.SANDEEP [ 12 NOV 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        strQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    strQ &= mstrUserAccessFilter
            'End If
            ''S.SANDEEP [ 12 NOV 2012 ] -- END

            ''Pinkal (24-Jun-2011) -- End

            'Sohail (12 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '        " hremployee_master.employeecode AS code " & _
            '        ", hremployee_master.surname +' '+ hremployee_master.firstname AS empname " & _
            '        ", CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS startdate " & _
            '        ", CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS enddate " & _
            '        ", DATEDIFF(YEAR, hremp_experience_tran.start_date,hremp_experience_tran.end_date) AS years " & _
            '        ", hremp_experience_tran.company AS employer " & _
            '        ", hrjob_master.job_name AS job " & _
            '        ", hremp_experience_tran.last_pay AS income " & _
            '        ", CASE WHEN iscontact_previous =1 THEN @No ELSE @Yes END AS objection  " & _
            '        ", hremp_experience_tran.contact_person AS contactperson " & _
            '        ", hremp_experience_tran.contact_no AS contactaddress " & _
            '        ", hremp_experience_tran.leave_reason AS leavingreason " & _
            '        ", hremp_experience_tran.remark AS remarks " & _
            '    "FROM hremp_experience_tran " & _
            '    "JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '    "LEFT JOIN hrjob_master ON hrjob_master.jobunkid= hremp_experience_tran.jobunkid " & _
            '    "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "

            StrQ = "SELECT  hremployee_master.employeecode AS code "

            If mblnFirstNamethenSurname = False Then
                StrQ &= ", hremployee_master.surname +' '+ hremployee_master.firstname AS empname "
            Else
                StrQ &= ", hremployee_master.firstname +' '+ hremployee_master.surname AS empname "
            End If
            StrQ &= ", CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS startdate " & _
                    ", CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS enddate " & _
                    ", DATEDIFF(YEAR, hremp_experience_tran.start_date,hremp_experience_tran.end_date) AS years " & _
                    ", hremp_experience_tran.company AS employer " & _
                    ", ISNULL(hremp_experience_tran.old_job, '') AS job " & _
                    ", hremp_experience_tran.last_pay AS income " & _
                    ", CASE WHEN iscontact_previous =1 THEN @No ELSE @Yes END AS objection  " & _
                    ", hremp_experience_tran.contact_person AS contactperson " & _
                    ", hremp_experience_tran.contact_no AS contactaddress " & _
                    ", hremp_experience_tran.leave_reason AS leavingreason " & _
                    ", hremp_experience_tran.remark AS remarks " & _
                    "FROM hremp_experience_tran " & _
                    "   JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP |28-OCT-2021| -- START
            'ISSUE : REPORT OPTIMZATION
            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP |28-OCT-2021| -- END

            StrQ &= "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If mblIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("code")
                rpt_Row.Item("Column2") = dtRow.Item("empname")
                rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("enddate").ToString).ToShortDateString
                If dtRow.Item("enddate").ToString.Trim.Length > 0 Then
                    rpt_Row.Item("Column4") = eZeeDate.convertDate(dtRow.Item("enddate").ToString).ToShortDateString
                Else
                    rpt_Row.Item("Column4") = ""
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                rpt_Row.Item("Column5") = dtRow.Item("years")
                rpt_Row.Item("Column6") = dtRow.Item("employer")
                rpt_Row.Item("Column7") = dtRow.Item("job")

                If mblnShowEmpScale Then
                    rpt_Row.Item("Column8") = Format(dtRow.Item("income"), GUI.fmtCurrency)
                End If
                rpt_Row.Item("Column9") = dtRow.Item("objection")
                rpt_Row.Item("Column10") = dtRow.Item("contactperson")
                rpt_Row.Item("Column11") = dtRow.Item("contactaddress")
                rpt_Row.Item("Column12") = dtRow.Item("leavingreason")
                rpt_Row.Item("Column13") = dtRow.Item("remarks")

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next

            objRpt = New ArutiReport.Designer.rptJobEmploymentHistory

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportID))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiImage").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiImage").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If mblnShowEmpScale Then
                Call ReportFunction.TextChange(objRpt, "txtIncome", Language.getMessage(mstrModuleName, 16, "Income"))
            Else
                Call ReportFunction.EnableSuppress(objRpt, "txtIncome", True)
            End If

            objRpt.SetDataSource(rpt_Data)




            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 9, "Employee Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 10, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtStartDate", Language.getMessage(mstrModuleName, 11, "Start Date"))
            Call ReportFunction.TextChange(objRpt, "txtEndDate", Language.getMessage(mstrModuleName, 12, "End Date"))
            Call ReportFunction.TextChange(objRpt, "txtYears", Language.getMessage(mstrModuleName, 13, "Years"))
            Call ReportFunction.TextChange(objRpt, "txtJob", Language.getMessage(mstrModuleName, 15, "Job"))
            Call ReportFunction.TextChange(objRpt, "txtIncome", Language.getMessage(mstrModuleName, 16, "Income"))
            Call ReportFunction.TextChange(objRpt, "txtObjection", Language.getMessage(mstrModuleName, 17, "Objection"))
            Call ReportFunction.TextChange(objRpt, "txtContactPerson", Language.getMessage(mstrModuleName, 18, "Contact Person"))
            Call ReportFunction.TextChange(objRpt, "txtContactAddress", Language.getMessage(mstrModuleName, 19, "Contact Address"))
            Call ReportFunction.TextChange(objRpt, "txtRemark", Language.getMessage(mstrModuleName, 20, "Remark"))
            Call ReportFunction.TextChange(objRpt, "txtEmployer", Language.getMessage(mstrModuleName, 14, "Employer"))
            Call ReportFunction.TextChange(objRpt, "txtLeavingReason", Language.getMessage(mstrModuleName, 21, "Leaving Reason"))
            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 22, "Total"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 27, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 28, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 29, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try

    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee name :")
            Language.setMessage(mstrModuleName, 2, "Employer Name :")
            Language.setMessage(mstrModuleName, 3, "Start Date From :")
            Language.setMessage(mstrModuleName, 4, "To")
            Language.setMessage(mstrModuleName, 5, "End Date From :")
            Language.setMessage(mstrModuleName, 6, "Order By :")
            Language.setMessage(mstrModuleName, 7, "Yes")
            Language.setMessage(mstrModuleName, 8, "No")
            Language.setMessage(mstrModuleName, 9, "Employee Code")
            Language.setMessage(mstrModuleName, 10, "Employee Name")
            Language.setMessage(mstrModuleName, 11, "Start Date")
            Language.setMessage(mstrModuleName, 12, "End Date")
            Language.setMessage(mstrModuleName, 13, "Years")
            Language.setMessage(mstrModuleName, 14, "Employer")
            Language.setMessage(mstrModuleName, 15, "Job")
            Language.setMessage(mstrModuleName, 16, "Income")
            Language.setMessage(mstrModuleName, 17, "Objection")
            Language.setMessage(mstrModuleName, 18, "Contact Person")
            Language.setMessage(mstrModuleName, 19, "Contact Address")
            Language.setMessage(mstrModuleName, 20, "Remark")
            Language.setMessage(mstrModuleName, 21, "Leaving Reason")
            Language.setMessage(mstrModuleName, 22, "Total")
            Language.setMessage(mstrModuleName, 23, "Prepared By :")
            Language.setMessage(mstrModuleName, 24, "Checked By :")
            Language.setMessage(mstrModuleName, 25, "Approved By :")
            Language.setMessage(mstrModuleName, 26, "Received By :")
            Language.setMessage(mstrModuleName, 27, "Printed By :")
            Language.setMessage(mstrModuleName, 28, "Printed Date :")
            Language.setMessage(mstrModuleName, 29, "Page :")
            Language.setMessage(mstrModuleName, 30, "Job Like :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

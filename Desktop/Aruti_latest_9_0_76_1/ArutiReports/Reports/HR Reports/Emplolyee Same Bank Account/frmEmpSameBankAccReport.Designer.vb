﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpSameBankAccReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchAccountType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchBankBranch = New eZee.Common.eZeeGradientButton
        Me.cboBankBranch = New System.Windows.Forms.ComboBox
        Me.cboAccountType = New System.Windows.Forms.ComboBox
        Me.lblAccountType = New System.Windows.Forms.Label
        Me.lblBankBranch = New System.Windows.Forms.Label
        Me.objbtnSearchBank = New eZee.Common.eZeeGradientButton
        Me.cboBank = New System.Windows.Forms.ComboBox
        Me.lblBank = New System.Windows.Forms.Label
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 410)
        Me.NavPanel.Size = New System.Drawing.Size(608, 55)
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 236)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(401, 63)
        Me.gbSortBy.TabIndex = 23
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(373, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(81, 15)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(95, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(272, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAccountType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchBankBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBankBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboAccountType)
        Me.gbFilterCriteria.Controls.Add(Me.lblAccountType)
        Me.gbFilterCriteria.Controls.Add(Me.lblBankBranch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchBank)
        Me.gbFilterCriteria.Controls.Add(Me.cboBank)
        Me.gbFilterCriteria.Controls.Add(Me.lblBank)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(401, 163)
        Me.gbFilterCriteria.TabIndex = 22
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAccountType
        '
        Me.objbtnSearchAccountType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAccountType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccountType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAccountType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAccountType.BorderSelected = False
        Me.objbtnSearchAccountType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAccountType.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAccountType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAccountType.Location = New System.Drawing.Point(372, 86)
        Me.objbtnSearchAccountType.Name = "objbtnSearchAccountType"
        Me.objbtnSearchAccountType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAccountType.TabIndex = 32
        '
        'objbtnSearchBankBranch
        '
        Me.objbtnSearchBankBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBankBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBankBranch.BorderSelected = False
        Me.objbtnSearchBankBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBankBranch.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBankBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBankBranch.Location = New System.Drawing.Point(372, 59)
        Me.objbtnSearchBankBranch.Name = "objbtnSearchBankBranch"
        Me.objbtnSearchBankBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBankBranch.TabIndex = 29
        '
        'cboBankBranch
        '
        Me.cboBankBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBankBranch.DropDownWidth = 230
        Me.cboBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBankBranch.FormattingEnabled = True
        Me.cboBankBranch.Location = New System.Drawing.Point(94, 59)
        Me.cboBankBranch.Name = "cboBankBranch"
        Me.cboBankBranch.Size = New System.Drawing.Size(272, 21)
        Me.cboBankBranch.TabIndex = 27
        '
        'cboAccountType
        '
        Me.cboAccountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAccountType.DropDownWidth = 230
        Me.cboAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAccountType.FormattingEnabled = True
        Me.cboAccountType.Location = New System.Drawing.Point(94, 86)
        Me.cboAccountType.Name = "cboAccountType"
        Me.cboAccountType.Size = New System.Drawing.Size(272, 21)
        Me.cboAccountType.TabIndex = 30
        '
        'lblAccountType
        '
        Me.lblAccountType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccountType.Location = New System.Drawing.Point(7, 89)
        Me.lblAccountType.Name = "lblAccountType"
        Me.lblAccountType.Size = New System.Drawing.Size(81, 15)
        Me.lblAccountType.TabIndex = 31
        Me.lblAccountType.Text = "Account Type"
        Me.lblAccountType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBankBranch
        '
        Me.lblBankBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBankBranch.Location = New System.Drawing.Point(7, 62)
        Me.lblBankBranch.Name = "lblBankBranch"
        Me.lblBankBranch.Size = New System.Drawing.Size(81, 15)
        Me.lblBankBranch.TabIndex = 28
        Me.lblBankBranch.Text = "Bank Branch"
        Me.lblBankBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchBank
        '
        Me.objbtnSearchBank.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBank.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBank.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBank.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBank.BorderSelected = False
        Me.objbtnSearchBank.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBank.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBank.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBank.Location = New System.Drawing.Point(372, 32)
        Me.objbtnSearchBank.Name = "objbtnSearchBank"
        Me.objbtnSearchBank.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBank.TabIndex = 26
        '
        'cboBank
        '
        Me.cboBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBank.DropDownWidth = 230
        Me.cboBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBank.FormattingEnabled = True
        Me.cboBank.Location = New System.Drawing.Point(94, 32)
        Me.cboBank.Name = "cboBank"
        Me.cboBank.Size = New System.Drawing.Size(272, 21)
        Me.cboBank.TabIndex = 24
        '
        'lblBank
        '
        Me.lblBank.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBank.Location = New System.Drawing.Point(7, 35)
        Me.lblBank.Name = "lblBank"
        Me.lblBank.Size = New System.Drawing.Size(81, 15)
        Me.lblBank.TabIndex = 25
        Me.lblBank.Text = "Bank"
        Me.lblBank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(95, 142)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(192, 16)
        Me.chkInactiveemp.TabIndex = 4
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(303, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 22
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(373, 113)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 5
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(95, 113)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(272, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 116)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(81, 15)
        Me.lblEmployee.TabIndex = 3
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEmpSameBankAccReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(608, 465)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpSameBankAccReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = ""
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchBankBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBankBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBankBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchBank As eZee.Common.eZeeGradientButton
    Friend WithEvents cboBank As System.Windows.Forms.ComboBox
    Friend WithEvents lblBank As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAccountType As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAccountType As System.Windows.Forms.ComboBox
    Friend WithEvents lblAccountType As System.Windows.Forms.Label
End Class

'************************************************************************************************************************************
'Class Name : clsTrainingImpact.vb
'Purpose    :
'Date       :02/04/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal Jariwala
''' </summary>
''' 

Public Class clsTrainingImpact
    Inherits IReportData

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingImpact"
    Private mintImpactunkid As Integer
    Dim objDataOperation As clsDataOperation

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set impactunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Impactunkid() As Integer
        Get
            Return mintImpactunkid
        End Get
        Set(ByVal value As Integer)
            mintImpactunkid = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport()
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "

    Public Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_Development As ArutiReport.Designer.dsArutiReport
        Dim rpt_objective As ArutiReport.Designer.dsArutiReport
        Dim rpt_selfAssessment As ArutiReport.Designer.dsArutiReport
        Dim rpt_FeedBack As ArutiReport.Designer.dsArutiReport
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()


            StrQ = "SELECT " & _
                        "hrtnatraining_impact_master.courseunkid " & _
                        ",cfcommon_master.name " & _
                        ",cfcommon_master.coursetypeid " & _
                        ",CONVERT(CHAR(8),hrtraining_scheduling.start_date,112) start_date " & _
                        ",CONVERT(CHAR(8),hrtraining_scheduling.end_date,112) end_date " & _
                        ",hrtnatraining_impact_master.employeeunkid " & _
                        ", ISNULL(emp.firstname,'') + ' ' + ISNULL(emp.surname,'') employee " & _
                        ",hrtnatraining_impact_master.managerunkid " & _
                        ", ISNULL(mgr.firstname,'') + ' ' + ISNULL(mgr.surname,'') Manager " & _
                        "FROM    hrtnatraining_impact_master " & _
                        "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrtnatraining_impact_master.courseunkid " & _
                                                "AND mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & _
                        "JOIN hremployee_master emp ON hrtnatraining_impact_master.employeeunkid = emp.employeeunkid " & _
                        "JOIN hremployee_master mgr ON hrtnatraining_impact_master.managerunkid = mgr.employeeunkid " & _
                        "JOIN hrtraining_scheduling ON hrtnatraining_impact_master.trainingschedulingunkid = hrtraining_scheduling.trainingschedulingunkid " & _
                        "WHERE   hrtnatraining_impact_master.impactunkid = " & mintImpactunkid


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim rpt_Row As DataRow
                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_Row.Item("Column1") = dtRow("name").ToString()
                    rpt_Row.Item("Column2") = eZeeDate.convertDate(dtRow("start_date").ToString()).ToShortDateString
                    rpt_Row.Item("Column3") = eZeeDate.convertDate(dtRow("end_date").ToString()).ToShortDateString
                    rpt_Row.Item("Column4") = dtRow("employee").ToString()
                    rpt_Row.Item("Column5") = dtRow("Manager").ToString()
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
                Next
            End If


            rpt_Development = New ArutiReport.Designer.dsArutiReport
            rpt_objective = New ArutiReport.Designer.dsArutiReport
            rpt_selfAssessment = New ArutiReport.Designer.dsArutiReport
            rpt_FeedBack = New ArutiReport.Designer.dsArutiReport

            Dim dtData As DataTable = GetImpactData()
            Dim i As Integer = 1
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Development.Tables("ArutiTable").NewRow

                If dtRow.Item("kra").ToString() <> "" Then
                    rpt_Row.Item("Column1") = dtRow.Item("kra")
                    rpt_Row.Item("Column3") = i & "."
                    i += 1
                End If
                rpt_Row.Item("Column2") = dtRow.Item("development")
                rpt_Development.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            i = 1
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_objective.Tables("ArutiTable").NewRow

                If dtRow.Item("kra").ToString() <> "" Then
                    rpt_Row.Item("Column1") = dtRow.Item("kra")
                    rpt_Row.Item("Column3") = i & "."
                    i += 1
                End If
                rpt_Row.Item("Column2") = dtRow.Item("objective")

                rpt_objective.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            Dim mstrRemark As String = ""
            i = 1
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_selfAssessment.Tables("ArutiTable").NewRow

                If dtRow.Item("kra").ToString() <> "" Then
                    rpt_Row.Item("Column1") = dtRow.Item("kra")
                    rpt_Row.Item("Column3") = i & "."
                    i += 1
                End If

                If mstrRemark <> dtRow.Item("self_remark") Then
                    rpt_Row.Item("Column2") = dtRow.Item("self_remark")
                    mstrRemark = dtRow.Item("self_remark").ToString()
                End If

                rpt_selfAssessment.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_FeedBack.Tables("ArutiTable").NewRow

                If dtRow.Item("question").ToString() = "" Then Continue For

                rpt_Row.Item("Column1") = dtRow.Item("question")
                rpt_Row.Item("Column2") = dtRow.Item("answer")
                rpt_Row.Item("Column3") = dtRow.Item("manager_remark")
                rpt_FeedBack.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptTrainingImpact
            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptPartAJobCapability1(a)").SetDataSource(rpt_Development)
            objRpt.Subreports("rptPartAJobCapability1(b)").SetDataSource(rpt_objective)
            objRpt.Subreports("rptPartBSelfAssessment").SetDataSource(rpt_selfAssessment)
            objRpt.Subreports("rptPartCFeedback").SetDataSource(rpt_FeedBack)

            Call ReportFunction.TextChange(objRpt, "txtLevel3RptHeading", Language.getMessage(mstrModuleName, 1, "LEVEL THREE TRAINING IMPACT EVALUATION FORM"))
            Call ReportFunction.TextChange(objRpt, "txtLevel3Evaluation", Language.getMessage(mstrModuleName, 2, "LEVEL 3 EVALUATION"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 3, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtSupervisor", Language.getMessage(mstrModuleName, 4, "Line Manager/Supervisor"))
            Call ReportFunction.TextChange(objRpt, "txtProgrammeAttended", Language.getMessage(mstrModuleName, 5, "NAME OF TRAINING/PROGRAMME ATTENDED"))
            Call ReportFunction.TextChange(objRpt, "txtStartEndDate", Language.getMessage(mstrModuleName, 6, "STARTING & ENDING DATE"))
            Call ReportFunction.TextChange(objRpt, "txtPartAHeading", Language.getMessage(mstrModuleName, 7, "PART A :"))
            Call ReportFunction.TextChange(objRpt, "txtPartAMainDesc", Language.getMessage(mstrModuleName, 8, "TO BE COMPLETED BY A LINE MANAGER/SUPERVISOR AFTER DISCUSSING WITH AN EMPLOYEE PRIOR ATTENDANCE TO TRAINING"))
            Call ReportFunction.TextChange(objRpt, "txtPartADesc", Language.getMessage(mstrModuleName, 9, "Your staff has been successfully placed on the above programme. You are advised that before he/she attends you spend at least fifteen (15) minutes to discuss and agree on the following items:-"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeSign", Language.getMessage(mstrModuleName, 10, "Employee's Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtEmpDate", vbCrLf & Language.getMessage(mstrModuleName, 11, "Date :"))
            Call ReportFunction.TextChange(objRpt, "txtsupervisorDate", vbCrLf & Language.getMessage(mstrModuleName, 11, "Date :"))
            Call ReportFunction.TextChange(objRpt, "txtSupervisorSign", Language.getMessage(mstrModuleName, 12, "Supervisor’s Signature:"))
            Call ReportFunction.TextChange(objRpt, "txtPartBHeading", Language.getMessage(mstrModuleName, 13, "PART B :"))
            Call ReportFunction.TextChange(objRpt, "txtPartBMainDesc", Language.getMessage(mstrModuleName, 14, "SELF EVALUATION/ASSESSMENT (TO BE COMPLETED BY AN EMPLOYEE)"))
            Call ReportFunction.TextChange(objRpt, "txtParBtDesc", Language.getMessage(mstrModuleName, 15, "Six (6) Months ago you attended the above mentioned training/programme. As part of level three training impact evaluation process you are requested to provide Self Assessment on Competence enhancement/Improvement in relation to the explanations provided on Part A item1(b)."))
            Call ReportFunction.TextChange(objRpt, "txtPartCHeading", Language.getMessage(mstrModuleName, 16, "PART C :"))
            Call ReportFunction.TextChange(objRpt, "txtPartCMainDesc", Language.getMessage(mstrModuleName, 17, "TO BE COMPLETED BY A LINE MANAGER / SUPERVISOR AFTER DISCUSSION WITH AN EMPLOYEE."))
            Call ReportFunction.TextChange(objRpt, "txtPartCDesc", Language.getMessage(mstrModuleName, 18, "Six (6) Months ago a member of your staff attended the above training programme. It is expected that following his/her return you have been giving him/her support, coaching and guidance to facilitate the transfer of the learning into the workplace for enhanced competences in the prior identified Job capability/training related performance gap and career Development Needs. To that end as part of Level Three Training Impact Evaluation Process you are kindly requested to briefly answer the following questions and return the form to us."))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 19, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 20, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 21, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)


            If dsList.Tables(0).Rows.Count > 0 Then

                If CInt(dsList.Tables(0).Rows(0)("coursetypeid")) = enCourseType.Job_Capability Then

                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtItem1A", Language.getMessage(mstrModuleName, 22, "Item 1(a):"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1Adesc", Language.getMessage(mstrModuleName, 23, "Job Capability or Training Related Performance Gap identified during Continuous Performance Assessment or Appraisal Process."))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1AKPI", Language.getMessage(mstrModuleName, 24, "Key Performance Indicator or Key Results Area"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1AIdenitfyJob", Language.getMessage(mstrModuleName, 25, "Identified Job Capability or Training Related Performance Deficiency / Gap"))

                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtItem1b", Language.getMessage(mstrModuleName, 26, "Item 1(b):"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1Bdesc", Language.getMessage(mstrModuleName, 27, "Training / Programme Objective(s) or Expected Outcomes after attending the training: This should address Job Capability or Training Related Performance Gap Identified on Item 1(a). (Your expectation on what the staff will be able to do at workplace after attending the training / programme)."))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1BKPI", Language.getMessage(mstrModuleName, 28, "Key Performance Indicator or Key Results Area"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1BTrainingObjective", Language.getMessage(mstrModuleName, 29, "Training Objective"))

                ElseIf CInt(dsList.Tables(0).Rows(0)("coursetypeid")) = enCourseType.Career_Development Then

                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtItem1A", Language.getMessage(mstrModuleName, 30, "Item 2(a):"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1Adesc", Language.getMessage(mstrModuleName, 31, "Career Development Need Identified during Continuous Performance Assessment or Appraisal Process."))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1AKPI", Language.getMessage(mstrModuleName, 32, "Key Performance Indicator or Key Results Area for senior position."))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(a)"), "txtPartA1AIdenitfyJob", Language.getMessage(mstrModuleName, 33, "Identified Career Development Gap"))


                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtItem1b", Language.getMessage(mstrModuleName, 34, "Item 2(b):"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1Bdesc", Language.getMessage(mstrModuleName, 35, "Training / Programme Objective(s) or Expected Outcome after attending the training: This should address the identified Career Development Gap Identified on Item 2(a). (Your expectation on what the staff needs to have for preparation for immediate higher responsibilities or senior position)."))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1BKPI", Language.getMessage(mstrModuleName, 24, "Key Performance Indicator or Key Results Area"))
                    Call ReportFunction.TextChange(objRpt.Subreports("rptPartAJobCapability1(b)"), "txtPartA1BTrainingObjective", Language.getMessage(mstrModuleName, 36, "Training / Programme Objective"))

                End If

            End If

            Call ReportFunction.TextChange(objRpt.Subreports("rptPartBSelfAssessment"), "txtPartBDesc", Language.getMessage(mstrModuleName, 37, "Job capabilities enhanced / Improved after attending the training / programme."))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPartBSelfAssessment"), "txtParBKPI", Language.getMessage(mstrModuleName, 38, "Key Results Area"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPartBSelfAssessment"), "txtPartBName", Language.getMessage(mstrModuleName, 39, "Name or Describe tasks that you are able to perform competently to the required standards after attending the Training in relation to Items 1(a)."))


            Call ReportFunction.TextChange(objRpt.Subreports("rptPartCFeedback"), "txtAnswer", Language.getMessage(mstrModuleName, 40, "Answer :"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptPartCFeedback"), "txtComment", Language.getMessage(mstrModuleName, 41, "Comment :"))


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function GetImpactData() As DataTable
        Dim dtData As New DataTable
        Try
            dtData.Columns.Add("impactunkid", Type.GetType("System.Int32"))
            dtData.Columns.Add("kraunkid", Type.GetType("System.Int32"))
            dtData.Columns.Add("kra", Type.GetType("System.String"))
            dtData.Columns.Add("developmentunkid", Type.GetType("System.String"))
            dtData.Columns.Add("development", Type.GetType("System.String"))
            dtData.Columns.Add("objectiveunkid", Type.GetType("System.String"))
            dtData.Columns.Add("objective", Type.GetType("System.String"))
            dtData.Columns.Add("questionunkid", Type.GetType("System.Int32"))
            dtData.Columns.Add("question", Type.GetType("System.String"))
            dtData.Columns.Add("answerunkid", Type.GetType("System.Int32"))
            dtData.Columns.Add("answer", Type.GetType("System.String"))
            dtData.Columns.Add("manager_remark", Type.GetType("System.String"))
            dtData.Columns.Add("self_remark", Type.GetType("System.String"))

            Dim objImpact As New clshrtnatraining_impact_master
            objImpact._Impactunkid = mintImpactunkid

            Dim dTemp As DataRow = Nothing

            If objImpact._dsKRAList.Tables(0).Rows.Count >= objImpact._dsDevelopmentList.Tables(0).Rows.Count AndAlso objImpact._dsKRAList.Tables(0).Rows.Count >= objImpact._dsObjectiveList.Tables(0).Rows.Count AndAlso _
                objImpact._dsKRAList.Tables(0).Rows.Count >= objImpact._dsFeedBackList.Tables(0).Rows.Count Then

                For Each dRow As DataRow In objImpact._dsKRAList.Tables(0).Rows
                    dTemp = dtData.NewRow

                    dTemp.Item("kraunkid") = dRow.Item("kraunkid")
                    dTemp.Item("kra") = dRow.Item("kra")
                    dTemp.Item("impactunkid") = mintImpactunkid

                    If objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsDevelopmentList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("development") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("development_area")
                        dTemp.Item("developmentunkid") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("developmentunkid")
                    Else
                        dTemp.Item("development") = ""
                    End If

                    If objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsObjectiveList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("objective") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("training_objective")
                        dTemp.Item("objectiveunkid") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("objectiveunkid")
                    Else
                        dTemp.Item("objective") = ""
                    End If

                    If objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsFeedBackList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("questionunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("questionunkid")
                        dTemp.Item("question") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("question")
                        dTemp.Item("answerunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("answerunkid")
                        dTemp.Item("answer") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("answer")
                        dTemp.Item("manager_remark") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsKRAList.Tables(0).Rows.IndexOf(dRow))("manager_remark")
                    Else
                        dTemp.Item("question") = ""
                        dTemp.Item("answer") = ""
                        dTemp.Item("manager_remark") = ""
                    End If

                    dTemp.Item("self_remark") = objImpact._Self_Remark

                    dtData.Rows.Add(dTemp)
                Next


            ElseIf objImpact._dsDevelopmentList.Tables(0).Rows.Count >= objImpact._dsKRAList.Tables(0).Rows.Count AndAlso objImpact._dsDevelopmentList.Tables(0).Rows.Count >= objImpact._dsObjectiveList.Tables(0).Rows.Count AndAlso _
                     objImpact._dsDevelopmentList.Tables(0).Rows.Count >= objImpact._dsFeedBackList.Tables(0).Rows.Count Then


                For Each dRow As DataRow In objImpact._dsDevelopmentList.Tables(0).Rows
                    dTemp = dtData.NewRow

                    dTemp.Item("developmentunkid") = dRow.Item("developmentunkid")
                    dTemp.Item("development") = dRow.Item("development_area")
                    dTemp.Item("impactunkid") = mintImpactunkid

                    If objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsObjectiveList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("objective") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("training_objective")
                        dTemp.Item("objectiveunkid") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("objectiveunkid")
                    Else
                        dTemp.Item("objective") = ""
                    End If

                    If objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsKRAList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("kra") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("kra")
                        dTemp.Item("kraunkid") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("kraunkid")
                    Else
                        dTemp.Item("kra") = ""
                    End If

                    If objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsFeedBackList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("questionunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("questionunkid")
                        dTemp.Item("question") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("question")
                        dTemp.Item("answerunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("answerunkid")
                        dTemp.Item("answer") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("answer")
                        dTemp.Item("manager_remark") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsDevelopmentList.Tables(0).Rows.IndexOf(dRow))("manager_remark")
                    Else
                        dTemp.Item("question") = ""
                        dTemp.Item("answer") = ""
                        dTemp.Item("manager_remark") = ""
                    End If

                    dTemp.Item("self_remark") = objImpact._Self_Remark
                    dtData.Rows.Add(dTemp)
                Next

            ElseIf objImpact._dsObjectiveList.Tables(0).Rows.Count >= objImpact._dsKRAList.Tables(0).Rows.Count AndAlso objImpact._dsObjectiveList.Tables(0).Rows.Count >= objImpact._dsDevelopmentList.Tables(0).Rows.Count AndAlso _
                     objImpact._dsObjectiveList.Tables(0).Rows.Count >= objImpact._dsFeedBackList.Tables(0).Rows.Count Then

                For Each dRow As DataRow In objImpact._dsObjectiveList.Tables(0).Rows
                    dTemp = dtData.NewRow

                    dTemp.Item("objectiveunkid") = dRow.Item("training_objective")
                    dTemp.Item("objective") = dRow.Item("objectiveunkid")
                    dTemp.Item("impactunkid") = mintImpactunkid

                    If objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsKRAList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("kra") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("kra")
                        dTemp.Item("kraunkid") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("kraunkid")
                    Else
                        dTemp.Item("kra") = ""
                    End If

                    If objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsDevelopmentList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("development") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("development_area")
                        dTemp.Item("developmentunkid") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("developmentunkid")
                    Else
                        dTemp.Item("development") = ""
                    End If

                    If objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsFeedBackList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("questionunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("questionunkid")
                        dTemp.Item("question") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("question")
                        dTemp.Item("answerunkid") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("answerunkid")
                        dTemp.Item("answer") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("answer")
                        dTemp.Item("manager_remark") = objImpact._dsFeedBackList.Tables(0).Rows(objImpact._dsObjectiveList.Tables(0).Rows.IndexOf(dRow))("manager_remark")
                    Else
                        dTemp.Item("question") = ""
                        dTemp.Item("answer") = ""
                        dTemp.Item("manager_remark") = ""
                    End If


                    dTemp.Item("self_remark") = objImpact._Self_Remark
                    dtData.Rows.Add(dTemp)
                Next


            ElseIf objImpact._dsFeedBackList.Tables(0).Rows.Count >= objImpact._dsKRAList.Tables(0).Rows.Count AndAlso objImpact._dsFeedBackList.Tables(0).Rows.Count >= objImpact._dsDevelopmentList.Tables(0).Rows.Count AndAlso _
                objImpact._dsFeedBackList.Tables(0).Rows.Count >= objImpact._dsObjectiveList.Tables(0).Rows.Count Then


                For Each dRow As DataRow In objImpact._dsFeedBackList.Tables(0).Rows
                    dTemp = dtData.NewRow

                    dTemp.Item("questionunkid") = dRow.Item("questionunkid")
                    dTemp.Item("question") = dRow.Item("question")
                    dTemp.Item("answerunkid") = dRow.Item("answerunkid")
                    dTemp.Item("answer") = dRow.Item("answer")
                    dTemp.Item("manager_remark") = dRow.Item("manager_remark")
                    dTemp.Item("impactunkid") = mintImpactunkid

                    If objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsKRAList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("kra") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("kra")
                        dTemp.Item("kraunkid") = objImpact._dsKRAList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("kraunkid")
                    Else
                        dTemp.Item("kra") = ""
                    End If

                    If objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsDevelopmentList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("development") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("development_area")
                        dTemp.Item("developmentunkid") = objImpact._dsDevelopmentList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("developmentunkid")
                    Else
                        dTemp.Item("development") = ""
                    End If

                    If objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow) <= objImpact._dsObjectiveList.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("objective") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("training_objective")
                        dTemp.Item("objectiveunkid") = objImpact._dsObjectiveList.Tables(0).Rows(objImpact._dsFeedBackList.Tables(0).Rows.IndexOf(dRow))("objectiveunkid")
                    Else
                        dTemp.Item("objective") = ""
                    End If

                    dTemp.Item("self_remark") = objImpact._Self_Remark
                    dtData.Rows.Add(dTemp)
                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetImpactData; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
        Return dtData
    End Function

#End Region

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub
End Class

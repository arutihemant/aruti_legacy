'************************************************************************************************************************************
'Class Name : frmAssessmentFormReport.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmAssessmentFormReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentFormReport"
    Private ObjAssesss_Form As clsAssessment_Form
    Private mDicPeriods As Dictionary(Of Integer, String)

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
        ObjAssesss_Form = New clsAssessment_Form(User._Object._Languageunkid,Company._Object._Companyunkid)
        ObjAssesss_Form.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData 'S.SANDEEP [05 OCT 2015]
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = ObjEmp.GetEmployeeList("List", True, False)
            dsCombos = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("List")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True)
            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            'S.SANDEEP [09 OCT 2015] -- START

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsCombos = objYear.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboFromYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Year").Copy
                .SelectedValue = FinancialYear._Object._YearUnkid
            End With

            With cboToYear
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Year").Copy
                .SelectedValue = FinancialYear._Object._YearUnkid
            End With
            'S.SANDEEP [09 OCT 2015] -- END


            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjEmp = Nothing
            ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            ObjAssesss_Form.SetDefaultValue()

            If ConfigParameter._Object._AssessmentReportTemplateId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Default Assessment Form template is not set in Aruti Configuration. Please go to following path to set default template." & vbCrLf & _
                                                    "Aruti Configuration -> Option -> Performance Management Tab"), enMsgBoxStyle.Information)
                Return False
            End If

            If cboEmployee.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            'S.SANDEEP [09 OCT 2015] -- START
            'If cboPeriod.SelectedValue <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Return False
            'End If

            If cboPeriod.Visible = True AndAlso cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            ''Shani(14-FEB-2017) -- Start
            ''Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'If ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_13 AndAlso chkProbationTemplate.Checked = True Then
            '    Dim objEmp_Mst As New clsEmployee_Master
            '    objEmp_Mst._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            'End If
            ''Shani(14-FEB-2017) -- End


            If gbMultiperiod.Visible = True Then
                If cboFromPeriod.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "From Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                    cboFromPeriod.Focus()
                    Return False
                End If

                If cboToPeriod.SelectedValue <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "To Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                    cboToPeriod.Focus()
                    Return False
                End If

                If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                    cboToPeriod.Focus()
                    Exit Function
                End If

                mDicPeriods = New Dictionary(Of Integer, String)
                Dim objPrd As New clscommom_period_Tran : Dim dsList As New DataSet
                'S.SANDEEP [06-NOV-2017] -- START
                'dsList = objPrd.GetList("List", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, 1, , True)
                dsList = objPrd.GetList("List", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, , , True)
                'S.SANDEEP [06-NOV-2017] -- END
                Dim dview As DataView = Nothing
                If cboFromPeriod.SelectedIndex = cboToPeriod.SelectedIndex Then
                    dview = New DataView(dsList.Tables(0), "periodunkid = '" & CType(cboFromPeriod.SelectedItem, DataRowView).Item("periodunkid") & "'", "end_date", DataViewRowState.CurrentRows)
                Else
                    dview = New DataView(dsList.Tables(0), "start_date >= '" & CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date") & "' AND end_date <= '" & CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date") & "'", "end_date", DataViewRowState.CurrentRows)
                End If
                mDicPeriods = dview.ToTable.AsEnumerable().ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("periodunkid"), Function(row) row.Field(Of String)("period_name"))
                objPrd = Nothing : dsList.Dispose()

                'Pinkal (24-Nov-2015) -- Start
                'Enhancement - WORKING ON Changes Required by Voltamp for Assessment Changes.

                ObjAssesss_Form._PeriodId = cboFromPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboFromPeriod.Text
            Else
                ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboPeriod.Text

                'Pinkal (24-Nov-2015) -- End
            End If

            ObjAssesss_Form._IsSelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
            'S.SANDEEP [09 OCT 2015] -- END



            ObjAssesss_Form._EmployeeId = cboEmployee.SelectedValue
            ObjAssesss_Form._EmployeeName = cboEmployee.Text


            ObjAssesss_Form._IsPreviewDisplay = False
            ObjAssesss_Form._SelectedTemplateId = ConfigParameter._Object._AssessmentReportTemplateId

            'S.SANDEEP [19 FEB 2015] -- START
            ObjAssesss_Form._ScoringOptionId = ConfigParameter._Object._ScoringOptionId
            'S.SANDEEP [19 FEB 2015] -- END

            ObjAssesss_Form._EmployeeAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)


            'S.SANDEEP [29 DEC 2015] -- START

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'ObjAssesss_Form._IncludeCustomItemInPlanning = ConfigParameter._Object._IncludeCustomItemInPlanning
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [29 DEC 2015] -- END

            'Shani (09-May-2016) -- Start
            ObjAssesss_Form._IsProbationTemplate = chkProbationTemplate.Checked
            'Shani (09-May-2016) -- End


            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            ObjAssesss_Form._IsUsedAgreedScore = ConfigParameter._Object._IsUseAgreedScore
            'Shani (23-Nov123-2016-2016) -- End

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            ObjAssesss_Form._IsCalibrationSettingActive = ConfigParameter._Object._IsCalibrationSettingActive
            ObjAssesss_Form._DisplayScoreName = cboScoreOption.Text
            ObjAssesss_Form._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms Events "

    Private Sub frmAssessmentFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            ObjAssesss_Form = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmAssessmentFormReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = ObjAssesss_Form._ReportName
            Me._Message = ObjAssesss_Form._ReportDesc

            Call FillCombo()
            Call ResetValue()

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            If ConfigParameter._Object._IsCalibrationSettingActive Then
                gbDisplayScore.Visible = True
            Else
                gbDisplayScore.Visible = False
            End If
            'S.SANDEEP |27-MAY-2019| -- END

            'S.SANDEEP [09 OCT 2015] -- START
            If ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
                gbMultiperiod.Visible = True
                cboPeriod.Visible = False : lblPeriod.Visible = False
                gbFilterCriteria.Size = New Size(379, 62)
            Else
                cboPeriod.Visible = True : lblPeriod.Visible = True
                gbMultiperiod.Visible = False

                'Shani (09-May-2016) -- Start
                'gbFilterCriteria.Size = New Size(379, 93)
                If Company._Object._Code = "CCK" AndAlso ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_13 Then
                    chkProbationTemplate.Visible = True
                    chkProbationTemplate.Checked = True
                    gbFilterCriteria.Size = New Size(379, 111)
                Else
                    chkProbationTemplate.Visible = False
                    chkProbationTemplate.Checked = False
                gbFilterCriteria.Size = New Size(379, 93)
            End If
                'Shani (09-May-2016) -- End

            End If
            'S.SANDEEP [09 OCT 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentFormReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub
    Private Sub frmAssessmentFormReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessment_Form.SetMessages()
            'S.SANDEEP |11-APR-2019| -- START
            'objfrm._Other_ModuleNames = "clsAssessment_Form"
            objfrm._Other_ModuleNames = "clsAssessmentListReport"
            'S.SANDEEP |11-APR-2019| -- END
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentFormReport_Language_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'ObjAssesss_Form.generateReport(0, e.Type, enExportAction.None)
            If ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
                Dim strBuilder As System.Text.StringBuilder
                If mDicPeriods.Keys.Count > 0 Then
                    strBuilder = ObjAssesss_Form.Generate_MultiPeriod_DetailReport(cboEmployee.SelectedValue, mDicPeriods, cboFromYear.Text, ConfigParameter._Object._ConsiderItemWeightAsNumber.ToString, ConfigParameter._Object._Self_Assign_Competencies, False, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    Dim frm As New frmHTMLReportView
                    frm.displayDialog(strBuilder.ToString, False)
                End If
            Else
                ObjAssesss_Form.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            End If
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'ObjAssesss_Form.generateReport(0, enPrintAction.None, e.Type)
            If ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
                Dim strBuilder As System.Text.StringBuilder
                If mDicPeriods.Keys.Count > 0 Then
                    strBuilder = ObjAssesss_Form.Generate_MultiPeriod_DetailReport(cboEmployee.SelectedValue, mDicPeriods, cboFromYear.Text, ConfigParameter._Object._ConsiderItemWeightAsNumber.ToString, ConfigParameter._Object._Self_Assign_Competencies, False, FinancialYear._Object._DatabaseName, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                    Dim frm As New frmHTMLReportView
                    frm.displayDialog(strBuilder.ToString, False)
                End If
            Else
                ObjAssesss_Form.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                              ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            End If

            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Controls "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    'S.SANDEEP [09 OCT 2015] -- START
#Region " ComboBox Event "

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromYear.SelectedIndexChanged
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [09 OCT 2015] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Period is compulsory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Default Assessment Form template is not set in Aruti Configuration. Please go to following path to set default template." & vbCrLf & _
                                                             "Aruti Configuration -> Option -> Performance Management Tab")
            Language.setMessage(mstrModuleName, 4, "Periods are compulsory information. Please check atleast one period to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot skip periods in between. Please check period to continue")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmAssessmentFormReport1

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmAssessmentFormReport"
'    Private ObjAssesss_Form As clsAssessment_Form

'#End Region

'#Region " Contructor "

'    Public Sub New()
'        ObjAssesss_Form = New clsAssessment_Form(User._Object._Languageunkid,Company._Object._Companyunkid)
'        ObjAssesss_Form.SetDefaultValue()
'        InitializeComponent()
'    End Sub

'#End Region

'#Region " Private Function "

'    Private Sub FillCombo()
'        Dim ObjEmp As New clsEmployee_Master
'        Dim ObjPeriod As New clscommom_period_Tran
'        Dim dsCombos As New DataSet
'        Try
'            'Pinkal (24-Jun-2011) -- Start
'            ' dsCombos = ObjEmp.GetEmployeeList("List", True, True)
'            dsCombos = ObjEmp.GetEmployeeList("List", True, False)
'            'Pinkal (24-Jun-2011) -- End
'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsCombos.Tables("List")
'            End With

'            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True)
'            With cboPeriod
'                .ValueMember = "periodunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("List")
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            dsCombos.Dispose()
'            ObjEmp = Nothing
'            ObjPeriod = Nothing
'        End Try
'    End Sub

'    Private Sub ResetValue()
'        Try
'            cboEmployee.SelectedValue = 0
'            cboPeriod.SelectedValue = 0
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Function SetFilter() As Boolean
'        Try
'            ObjAssesss_Form.SetDefaultValue()

'            If cboEmployee.SelectedValue <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select employee to continue."), enMsgBoxStyle.Information)
'                cboEmployee.Focus()
'                Return False
'            End If

'            If cboPeriod.SelectedValue <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
'                cboPeriod.Focus()
'                Return False
'            End If


'            ObjAssesss_Form._EmployeeId = cboEmployee.SelectedValue
'            ObjAssesss_Form._EmployeeName = cboEmployee.Text

'            ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
'            ObjAssesss_Form._PeriodName = cboPeriod.Text

'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Forms "

'    Private Sub frmAssessmentFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        Try
'            ObjAssesss_Form = Nothing
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "frmAssessmentFormReport_FormClosed", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmAssessmentFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            Call Language.setLanguage(Me.Name)
'            Me._Title = ObjAssesss_Form._ReportName
'            Me._Message = ObjAssesss_Form._ReportDesc

'            Call FillCombo()
'            Call ResetValue()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmAssessmentFormReport_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control Then
'                If e.KeyCode = Windows.Forms.Keys.R Then
'                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            Select Case e.KeyChar
'                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
'                    Windows.Forms.SendKeys.Send("{TAB}")
'                    e.Handled = True
'                    Exit Select
'            End Select
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Buttons "

'    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
'        Try

'            If Not SetFilter() Then Exit Sub
'            ObjAssesss_Form.generateReport(0, e.Type, enExportAction.None)
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
'        Try

'            If Not SetFilter() Then Exit Sub

'            ObjAssesss_Form.generateReport(0, enPrintAction.None, e.Type)
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
'        Try
'            Call ResetValue()
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
'        End Try
'    End Sub


'#End Region

'#Region " Controls "

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            frm.DataSource = cboEmployee.DataSource
'            frm.ValueMember = cboEmployee.ValueMember
'            frm.DisplayMember = cboEmployee.DisplayMember
'            frm.CodeMember = "employeecode"
'            If frm.DisplayDialog Then
'                cboEmployee.SelectedValue = frm.SelectedValue
'                cboEmployee.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'End Class

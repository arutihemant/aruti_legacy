﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTrainingStatusSummaryReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowTrainingStatusChart = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.cboTrainingStatus = New System.Windows.Forms.ComboBox
        Me.lblTrainingStatus = New System.Windows.Forms.Label
        Me.objbtnSearchTraining = New eZee.Common.eZeeGradientButton
        Me.cboTrainingName = New System.Windows.Forms.ComboBox
        Me.lblTrainingName = New System.Windows.Forms.Label
        Me.cboTrainingCalendar = New System.Windows.Forms.ComboBox
        Me.lblTrainingCalendar = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkShowTrainingStatusChart)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTraining)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingName)
        Me.gbFilterCriteria.Controls.Add(Me.cboTrainingCalendar)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingCalendar)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 67)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(459, 164)
        Me.gbFilterCriteria.TabIndex = 62
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowTrainingStatusChart
        '
        Me.chkShowTrainingStatusChart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowTrainingStatusChart.Location = New System.Drawing.Point(127, 133)
        Me.chkShowTrainingStatusChart.Name = "chkShowTrainingStatusChart"
        Me.chkShowTrainingStatusChart.Size = New System.Drawing.Size(287, 18)
        Me.chkShowTrainingStatusChart.TabIndex = 294
        Me.chkShowTrainingStatusChart.Text = "Show Training Status Chart"
        Me.chkShowTrainingStatusChart.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(127, 111)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(287, 16)
        Me.chkInactiveemp.TabIndex = 293
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        '
        'cboTrainingStatus
        '
        Me.cboTrainingStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingStatus.FormattingEnabled = True
        Me.cboTrainingStatus.Location = New System.Drawing.Point(127, 84)
        Me.cboTrainingStatus.Name = "cboTrainingStatus"
        Me.cboTrainingStatus.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingStatus.TabIndex = 106
        '
        'lblTrainingStatus
        '
        Me.lblTrainingStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingStatus.Location = New System.Drawing.Point(14, 87)
        Me.lblTrainingStatus.Name = "lblTrainingStatus"
        Me.lblTrainingStatus.Size = New System.Drawing.Size(108, 15)
        Me.lblTrainingStatus.TabIndex = 105
        Me.lblTrainingStatus.Text = "Training Status"
        Me.lblTrainingStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTraining
        '
        Me.objbtnSearchTraining.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTraining.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTraining.BorderSelected = False
        Me.objbtnSearchTraining.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTraining.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTraining.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTraining.Location = New System.Drawing.Point(425, 59)
        Me.objbtnSearchTraining.Name = "objbtnSearchTraining"
        Me.objbtnSearchTraining.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTraining.TabIndex = 98
        '
        'cboTrainingName
        '
        Me.cboTrainingName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingName.FormattingEnabled = True
        Me.cboTrainingName.Location = New System.Drawing.Point(127, 57)
        Me.cboTrainingName.Name = "cboTrainingName"
        Me.cboTrainingName.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingName.TabIndex = 24
        '
        'lblTrainingName
        '
        Me.lblTrainingName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingName.Location = New System.Drawing.Point(14, 60)
        Me.lblTrainingName.Name = "lblTrainingName"
        Me.lblTrainingName.Size = New System.Drawing.Size(84, 15)
        Me.lblTrainingName.TabIndex = 23
        Me.lblTrainingName.Text = "Training Name"
        Me.lblTrainingName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrainingCalendar
        '
        Me.cboTrainingCalendar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrainingCalendar.FormattingEnabled = True
        Me.cboTrainingCalendar.Location = New System.Drawing.Point(127, 30)
        Me.cboTrainingCalendar.Name = "cboTrainingCalendar"
        Me.cboTrainingCalendar.Size = New System.Drawing.Size(287, 21)
        Me.cboTrainingCalendar.TabIndex = 4
        '
        'lblTrainingCalendar
        '
        Me.lblTrainingCalendar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingCalendar.Location = New System.Drawing.Point(14, 33)
        Me.lblTrainingCalendar.Name = "lblTrainingCalendar"
        Me.lblTrainingCalendar.Size = New System.Drawing.Size(108, 15)
        Me.lblTrainingCalendar.TabIndex = 3
        Me.lblTrainingCalendar.Text = "Training Calendar"
        Me.lblTrainingCalendar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmTrainingStatusSummaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 452)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTrainingStatusSummaryReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Training Status Summary Report"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboTrainingStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingStatus As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTraining As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTrainingName As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingName As System.Windows.Forms.Label
    Friend WithEvents cboTrainingCalendar As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingCalendar As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowTrainingStatusChart As System.Windows.Forms.CheckBox
End Class

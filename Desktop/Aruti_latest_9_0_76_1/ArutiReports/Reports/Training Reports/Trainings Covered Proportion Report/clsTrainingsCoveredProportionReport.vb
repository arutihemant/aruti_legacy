﻿'Class Name : clsTrainingCalendarReport.vb
'Purpose    :
'Date       : 20-May-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsTrainingsCoveredProportionReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsTrainingsCoveredProportionReport"
    Private mstrReportId As String = enArutiReport.Trainings_Covered_Proportion_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintAllocationid As Integer = 0
    Private mstrAllocationName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mlstIds As List(Of String)
    'Hemant (28 Oct 2021) -- Start
    'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
    Private mblnIsActive As Boolean = True
    'Hemant (28 Oct 2021) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationId() As Integer
        Set(ByVal value As Integer)
            mintAllocationid = value
        End Set
    End Property

    Public WriteOnly Property _AllocationName() As String
        Set(ByVal value As String)
            mstrAllocationName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _Ids() As List(Of String)
        Set(ByVal value As List(Of String))
            mlstIds = value
        End Set
    End Property

    'Hemant (28 Oct 2021) -- Start
    'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Hemant (28 Oct 2021) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintAllocationid = 0
            mstrAllocationName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Calendar :") & " " & mstrCalendarName & " "
            End If

            If mintAllocationid > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Allocation :") & " " & mstrAllocationName & " "
            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Training :") & " " & mstrTrainingName & " "
            End If

            Me._FilterQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "startdate"
            OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim dsCompletedTrainingList As New DataSet
        Dim dsCompanyAllocationList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception

        Dim dsCombo As DataSet = Nothing
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDept As New clsDepartment
        Dim objSectionGrp As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCC As New clscostcenter_master
        Dim strColumnName As String = String.Empty
        Try

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Hemant (28 Oct 2021) -- End

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Training Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            Select Case mintAllocationid
                Case enAllocation.BRANCH
                    dsCombo = objStation.getComboList("Station")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "stationunkid"
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombo = objDeptGrp.getComboList("DeptGrp")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "deptgroupunkid"
                Case enAllocation.DEPARTMENT
                    dsCombo = objDept.getComboList("Department")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "departmentunkid"
                Case enAllocation.SECTION_GROUP
                    dsCombo = objSectionGrp.getComboList("List")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "sectiongroupunkid"
                Case enAllocation.SECTION
                    dsCombo = objSection.getComboList("Section")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "sectionunkid"
                Case enAllocation.UNIT_GROUP
                    dsCombo = objUnitGroup.getComboList("List")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "unitgroupunkid"
                Case enAllocation.UNIT
                    dsCombo = objUnit.getComboList("Unit")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "unitunkid"
                Case enAllocation.TEAM
                    dsCombo = objTeam.getComboList("List")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "teamunkid"
                Case enAllocation.JOB_GROUP
                    dsCombo = objJobGrp.getComboList("JobGrp")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "jobgroupunkid"
                Case enAllocation.JOBS
                    dsCombo = objJob.getComboList("Job", True)
                    dsCombo.Tables(0).Columns("jobunkid").ColumnName = "Id"
                    strColumnName = "jobunkid"
                Case enAllocation.CLASS_GROUP
                    dsCombo = objClassGrp.getComboList("ClassGrp")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "classgroupunkid"
                Case enAllocation.CLASSES
                    dsCombo = objClass.getComboList("Class")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    strColumnName = "classunkid"
                Case enAllocation.COST_CENTER
                    dsCombo = objCC.getComboList("List")
                    dsCombo.Tables(0).Columns(0).ColumnName = "Id"
                    dsCombo.Tables(0).Columns("costcentername").ColumnName = "name"
                    strColumnName = "costcenterunkid"
                Case Else
                    dsCombo = Nothing
                    strColumnName = ""
            End Select

            If dsCombo.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsCombo.Tables(0).Select("Id > 0 AND ID IN ( " & String.Join(",", mlstIds.ToArray) & ")")
                    dtCol = New DataColumn("ColumnAllocation" & dtRow.Item("Id"))
                    dtCol.DataType = System.Type.GetType("System.String")
                    dtCol.Caption = dtRow.Item("Name")
                    dtCol.DefaultValue = "0%"
                    dtFinalTable.Columns.Add(dtCol)
                Next
            End If


            StrQ &= "SELECT trdepartmentaltrainingneed_master.departmentaltrainingneedunkid " & _
                     ", trdepartmentaltrainingneed_master.periodunkid " & _
                     ", ISNULL(trtraining_calendar_master.calendar_name, '') AS periodname " & _
                     ", trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                     ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                     ", trdepartmentaltrainingneed_master.startdate AS startdate " & _
                     ", trdepartmentaltrainingneed_master.enddate AS enddate "

            StrQ &= "FROM trdepartmentaltrainingneed_master " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 "

            StrQ &= "WHERE trdepartmentaltrainingneed_master.isvoid = 0 " & _
                      "AND trtraining_calendar_master.isactive = 1 " & _
                      "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "IF OBJECT_ID('tempdb..#Completed') IS NOT NULL DROP TABLE #Completed " & _
                    "SELECT " & _
                        "* INTO #Completed " & _
                    "FROM (SELECT " & _
                            "B.employeeunkid " & _
                           ",B.stationunkid " & _
                           ",B.deptgroupunkid " & _
                           ",B.departmentunkid " & _
                           ",B.sectiongroupunkid " & _
                           ",B.sectionunkid " & _
                           ",B.unitgroupunkid " & _
                           ",B.unitunkid " & _
                           ",B.teamunkid " & _
                           ",B.classgroupunkid " & _
                           ",B.classunkid " & _
                           ",B.jobgroupunkid " & _
                           ",B.jobunkid " & _
                           ",B.costcenterunkid " & _
                           ",B.gradegroupunkid " & _
                           ",B.gradeunkid " & _
                           ",B.gradelevelunkid " & _
                           ",A.coursemasterunkid " & _
                        "FROM (SELECT " & _
                                "* " & _
                            "FROM trtraining_request_master " & _
                            "WHERE isvoid = 0 " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND departmentaltrainingneedunkid > 0 " & _
                            "AND iscompleted_submit_approval = 1 " & _
                            "AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED & ") AS A " & _
                        " JOIN hremployee_master ON hremployee_master.employeeunkid = A.employeeunkid " & _
                        "LEFT JOIN (SELECT " & _
                                "Trf_AS.employeeunkid " & _
                               ",stationunkid " & _
                               ",deptgroupunkid " & _
                               ",departmentunkid " & _
                               ",sectiongroupunkid " & _
                               ",sectionunkid " & _
                               ",unitgroupunkid " & _
                               ",unitunkid " & _
                               ",teamunkid " & _
                               ",classgroupunkid " & _
                               ",classunkid " & _
                               ",RUCat.jobgroupunkid " & _
                               ",RUCat.jobunkid " & _
                               ",CCT.costcenterunkid " & _
                               ",GRD.gradegroupunkid " & _
                               ",GRD.gradeunkid " & _
                               ",GRD.gradelevelunkid " & _
                            "FROM (SELECT " & _
                                    "ETT.employeeunkid AS TrfEmpId " & _
                                   ",ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                   ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                   ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                   ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                   ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                   ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                   ",ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                   ",ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                   ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                   ",ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                   ",CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                                   ",ETT.employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_transfer_tran AS ETT " & _
                                "WHERE isvoid = 0 " & _
                                "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsonDate) AS Trf_AS " & _
                            "JOIN (SELECT " & _
                                    "ECT.employeeunkid AS CatEmpId " & _
                                   ",ECT.jobgroupunkid " & _
                                   ",ECT.jobunkid " & _
                                   ",CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                   ",ECT.employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                "WHERE isvoid = 0 " & _
                                "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsonDate) AS RUCat " & _
                                "ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
                            "JOIN (SELECT " & _
                                    "CCT.employeeunkid AS CCTEmpId " & _
                                   ",CCT.cctranheadvalueid AS costcenterunkid " & _
                                   ",CONVERT(CHAR(8), CCT.effectivedate, 112) AS CTEfDt " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_cctranhead_tran AS CCT " & _
                                "WHERE CCT.isvoid = 0 " & _
                                "AND CCT.istransactionhead = 0 " & _
                                "AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= @EmployeeAsonDate) AS CCT " & _
                                "ON Trf_AS.employeeunkid = CCT.CCTEmpId " & _
                            "JOIN (SELECT " & _
                                    "gradegroupunkid " & _
                                   ",gradeunkid " & _
                                   ",gradelevelunkid " & _
                                   ",employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..prsalaryincrement_tran " & _
                                "WHERE isvoid = 0 " & _
                                "AND isapproved = 1 " & _
                                "AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsonDate) AS GRD " & _
                                "ON Trf_AS.employeeunkid = GRD.employeeunkid " & _
                            "WHERE Trf_AS.Rno = 1 " & _
                            "AND RUCat.Rno = 1 " & _
                            "AND CCT.Rno = 1 " & _
                            "AND GRD.Rno = 1) AS B " & _
                            "ON B.employeeunkid = A.employeeunkid "

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (28 Oct 2021) -- End

            StrQ &= ") AS C " & _
                    "WHERE 1 = 1 "

            StrQ &= "SELECT " & _
                        " " & strColumnName & " AS Aid " & _
                       ",coursemasterunkid " & _
                       ",COUNT(*) AS TotalCount " & _
                    "FROM #Completed " & _
                    " WHERE " & strColumnName & " IN ( " & String.Join(",", mlstIds.ToArray) & ")" & _
                    "GROUP BY " & strColumnName & " " & _
                            ",coursemasterunkid "

            objDataOperation.ClearParameters()
            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
            End If
            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))

            dsCompletedTrainingList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "IF OBJECT_ID('tempdb..#Allocation') IS NOT NULL DROP TABLE #Allocation " & _
                        "SELECT * INTO #Allocation from ( " & _
                         "SELECT " & _
                                "Trf_AS.employeeunkid " & _
                               ",Trf_AS.stationunkid " & _
                               ",Trf_AS.deptgroupunkid " & _
                               ",Trf_AS.departmentunkid " & _
                               ",Trf_AS.sectiongroupunkid " & _
                               ",Trf_AS.sectionunkid " & _
                               ",Trf_AS.unitgroupunkid " & _
                               ",Trf_AS.unitunkid " & _
                               ",Trf_AS.teamunkid " & _
                               ",Trf_AS.classgroupunkid " & _
                               ",Trf_AS.classunkid " & _
                               ",RUCat.jobgroupunkid " & _
                               ",RUCat.jobunkid " & _
                               ",CCT.costcenterunkid " & _
                               ",GRD.gradegroupunkid " & _
                               ",GRD.gradeunkid " & _
                               ",GRD.gradelevelunkid " & _
                            "FROM (SELECT " & _
                                    "ETT.employeeunkid AS TrfEmpId " & _
                                   ",ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                   ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                   ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                   ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                   ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                   ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                   ",ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                   ",ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                   ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                   ",ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                   ",CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                                   ",ETT.employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_transfer_tran AS ETT " & _
                                "WHERE isvoid = 0 " & _
                                "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsonDate) AS Trf_AS " & _
                            "JOIN hremployee_master	ON hremployee_master.employeeunkid = Trf_AS.employeeunkid " & _
                            "JOIN (SELECT " & _
                                    "ECT.employeeunkid AS CatEmpId " & _
                                   ",ECT.jobgroupunkid " & _
                                   ",ECT.jobunkid " & _
                                   ",CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                   ",ECT.employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                "WHERE isvoid = 0 " & _
                                "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsonDate) AS RUCat " & _
                                "ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
                            "JOIN (SELECT " & _
                                    "CCT.employeeunkid AS CCTEmpId " & _
                                   ",CCT.cctranheadvalueid AS costcenterunkid " & _
                                   ",CONVERT(CHAR(8), CCT.effectivedate, 112) AS CTEfDt " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..hremployee_cctranhead_tran AS CCT " & _
                                "WHERE CCT.isvoid = 0 " & _
                                "AND CCT.istransactionhead = 0 " & _
                                "AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= @EmployeeAsonDate) AS CCT " & _
                                "ON Trf_AS.employeeunkid = CCT.CCTEmpId " & _
                            "JOIN (SELECT " & _
                                    "gradegroupunkid " & _
                                   ",gradeunkid " & _
                                   ",gradelevelunkid " & _
                                   ",employeeunkid " & _
                                   ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                                "FROM " & xDatabaseName & "..prsalaryincrement_tran " & _
                                "WHERE isvoid = 0 " & _
                                "AND isapproved = 1 " & _
                                "AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsonDate) AS GRD " & _
                                "ON Trf_AS.employeeunkid = GRD.employeeunkid "

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'Hemant (28 Oct 2021) -- End

            StrQ &= "WHERE Trf_AS.Rno = 1 " & _
                            "AND RUCat.Rno = 1 " & _
                            "AND CCT.Rno = 1 " & _
                            "AND GRD.Rno = 1 "

            'Hemant (28 Oct 2021) -- Start
            'ENHANCEMENT : OLD-497 - Training Covered Proportion Report considers inactive employee also when computing the percentage. Consider only the active staff count by default.
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (28 Oct 2021) -- End

            StrQ &= ") as A "

            StrQ &= " SELECT " & strColumnName & " AS Aid, count(*) AS TotalCount from #Allocation " & _
                    " WHERE " & strColumnName & " IN ( " & String.Join(",", mlstIds.ToArray) & ")" & _
                    " group by " & strColumnName & " "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))

            dsCompanyAllocationList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing

            For Each drRow As DataRow In dsList.Tables(0).Rows

                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("TrainingName") = drRow.Item("trainingcoursename")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString

                For Each drComplete As DataRow In dsCompletedTrainingList.Tables(0).Select("coursemasterunkid = " & CInt(drRow.Item("trainingcourseunkid")))
                    Dim CompletedTotalCount As Decimal
                    Dim AllocationTotalCount As Decimal

                    CompletedTotalCount = CDec(drComplete.Item("totalcount"))
                    For Each drCompany As DataRow In dsCompanyAllocationList.Tables(0).Select("Aid =" & CInt(drComplete.Item("Aid")))

                        AllocationTotalCount = CDec(drCompany.Item("totalcount"))
                        If AllocationTotalCount > 0 Then
                            rpt_Row.Item("ColumnAllocation" & CInt(drComplete.Item("Aid"))) = (CDec((CompletedTotalCount * 100) / AllocationTotalCount)).ToString("#0.00") & "%"
                        End If

                    Next

                Next
                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Approved By"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)

        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDept = Nothing
            objSectionGrp = Nothing
            objSection = Nothing
            objUnitGroup = Nothing
            objUnit = Nothing
            objTeam = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
        End Try

    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Training Name")
			Language.setMessage(mstrModuleName, 3, "Start Date")
			Language.setMessage(mstrModuleName, 4, "End Date")
			Language.setMessage(mstrModuleName, 5, "Allocation :")
			Language.setMessage(mstrModuleName, 6, "Training :")
			Language.setMessage(mstrModuleName, 15, "Prepared By :")
			Language.setMessage(mstrModuleName, 16, "Checked By :")
			Language.setMessage(mstrModuleName, 17, "Approved By")
			Language.setMessage(mstrModuleName, 18, "Received By :")
			Language.setMessage(mstrModuleName, 19, "Calendar :")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

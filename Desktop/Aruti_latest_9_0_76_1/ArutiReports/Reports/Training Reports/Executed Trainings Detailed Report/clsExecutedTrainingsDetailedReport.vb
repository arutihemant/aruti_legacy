﻿'Class Name : clsExecutedTrainingsDetailedReport.vb
'Purpose    :
'Date       : 19-May-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsExecutedTrainingsDetailedReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsExecutedTrainingsDetailedReport"
    Private mstrReportId As String = enArutiReport.Executed_Trainings_Detailed_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mintCalendarUnkid As Integer = 0
    Private mstrCalendarName As String = ""
    Private mintTrainingUnkid As Integer = 0
    Private mstrTrainingName As String = ""
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    Private mstrOrderByQuery As String = ""
    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Private mblnIsActive As Boolean = True
    'Hemant (25 Oct 2021) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _CalendarUnkid() As Integer
        Set(ByVal value As Integer)
            mintCalendarUnkid = value
        End Set
    End Property

    Public WriteOnly Property _CalendarName() As String
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    Public WriteOnly Property _TrainingUnkid() As Integer
        Set(ByVal value As Integer)
            mintTrainingUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TrainingName() As String
        Set(ByVal value As String)
            mstrTrainingName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Hemant (25 Oct 2021) -- Start
    'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Hemant (25 Oct 2021) -- End

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""
            mintCalendarUnkid = 0
            mstrCalendarName = ""
            mintTrainingUnkid = 0
            mstrTrainingName = ""
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mstrAdvance_Filter = ""
            mstrOrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintCalendarUnkid > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.periodunkid = @periodunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Calendar :") & " " & mstrCalendarName & " "
            End If

            If mintTrainingUnkid > 0 Then
                objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingUnkid)
                Me._FilterQuery &= " AND trdepartmentaltrainingneed_master.trainingcourseunkid = @trainingcourseunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Training :") & " " & mstrTrainingName & " "
            End If

            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                If mintReportId = 0 Then
                    Me._FilterQuery &= " AND trdepttrainingneed_employee_tran.employeeunkid = @employeeunkid "
                ElseIf mintReportId = 1 Then
                    Me._FilterQuery &= " AND trtraining_request_master.employeeunkid = @employeeunkid "
                End If
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 16, "Employee :") & " " & mstrEmployeeName & " "
            End If

            mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = "startdate"
            OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub
#End Region

#Region " Report Generation "
    Public Sub Generate_PlannedDetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("EmpCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Emp Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeNames", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Names")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Company", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Company")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Function", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Function")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Grade", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Grade")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("JobTitle", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Job Title")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Gender")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Training Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 13, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)


            StrQ &= "SELECT  " & _
                       "  trdepttrainingneed_employee_tran.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ", ISNULL(hs.name, '') AS Branch " & _
                       ", ISNULL(hd.name, '') AS Department " & _
                       ", ISNULL(hsg.name, '') AS SectionGrp " & _
                       ", ISNULL(hcg.name, '') AS ClassGrp " & _
                       ", ISNULL(hc.name, '') AS Class " & _
                       ", ISNULL(hjb.job_name, '') AS Job " & _
                       ", ISNULL(hgd.name, '') AS GradeGrp " & _
                       ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                       ", CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       ", trdepartmentaltrainingneed_master.startdate AS startdate " & _
                       ", trdepartmentaltrainingneed_master.enddate AS enddate "
            StrQ &= "FROM trdepttrainingneed_employee_tran " & _
                    "LEFT JOIN trdepartmentaltrainingneed_master on trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trdepttrainingneed_employee_tran.departmentaltrainingneedunkid " & _
                           " AND trdepartmentaltrainingneed_master.isvoid = 0 " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 " & _
                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trdepttrainingneed_employee_tran.employeeunkid " & _
                         " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrstation_master hs ON t.stationunkid = hs.stationunkid " & _
                          " LEFT JOIN hrdepartment_master hd ON t.departmentunkid = hd.departmentunkid " & _
                          " LEFT JOIN hrsectiongroup_master hsg ON t.sectiongroupunkid = hsg.sectiongroupunkid " & _
                          " LEFT JOIN hrclassgroup_master hcg ON t.classgroupunkid = hcg.classgroupunkid " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "           gradegroupunkid " & _
                          "          ,gradeunkid " & _
                          "          ,gradelevelunkid " & _
                          "          ,employeeunkid " & _
                          "          ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                          "        FROM prsalaryincrement_tran " & _
                          "        WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS G ON G.employeeunkid = hremployee_master.employeeunkid	AND G.Rno = 1 " & _
                          " LEFT JOIN hrgradegroup_master hgd ON hgd.gradegroupunkid = G.gradegroupunkid "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'Hemant (25 Oct 2021) -- End

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE trdepttrainingneed_employee_tran.isvoid = 0 " & _
                      "AND trtraining_calendar_master.isactive = 1 " & _
                      "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' "


            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (25 Oct 2021) -- End

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmpCode") = drRow.Item("employeecode")
                rpt_Row.Item("EmployeeNames") = drRow.Item("employee")
                rpt_Row.Item("Company") = drRow.Item("Branch")
                rpt_Row.Item("Function") = drRow.Item("Department")
                rpt_Row.Item("Department") = drRow.Item("SectionGrp")
                rpt_Row.Item("Zone") = drRow.Item("ClassGrp")
                rpt_Row.Item("Branch") = drRow.Item("Class")
                rpt_Row.Item("Grade") = drRow.Item("GradeGrp")
                rpt_Row.Item("JobTitle") = drRow.Item("Job")
                rpt_Row.Item("Gender") = drRow.Item("Gender")
                rpt_Row.Item("TrainingName") = drRow.Item("trainingcoursename")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString

                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub

    Public Sub Generate_ExecutedDetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Training")

            dtCol = New DataColumn("EmpCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Emp Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeNames", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Employee Names")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Company", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Company")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Function", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "Function")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Department", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "Department")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Zone", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "Zone")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Branch", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "Branch")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Grade", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "Grade")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("JobTitle", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "Job Title")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("Gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "Gender")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("TrainingName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "Training Name")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("StartDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "Start Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EndDate", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 13, "End Date")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)


            StrQ &= "SELECT  " & _
                       "  trtraining_request_master.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode, '') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                       ", ISNULL(hs.name, '') AS Branch " & _
                       ", ISNULL(hd.name, '') AS Department " & _
                       ", ISNULL(hsg.name, '') AS SectionGrp " & _
                       ", ISNULL(hcg.name, '') AS ClassGrp " & _
                       ", ISNULL(hc.name, '') AS Class " & _
                       ", ISNULL(hjb.job_name, '') AS Job " & _
                       ", ISNULL(hgd.name, '') AS GradeGrp " & _
                       ", CASE WHEN trdepartmentaltrainingneed_master.trainingcourseunkid > 0 THEN ISNULL(tcourse.name, '') ELSE ISNULL(other_trainingcourse, '') END AS trainingcoursename " & _
                       ", CASE WHEN gender = 1 THEN @Male WHEN gender = 2 THEN @Female ELSE '' END AS Gender " & _
                       ", trdepartmentaltrainingneed_master.startdate AS startdate " & _
                       ", trdepartmentaltrainingneed_master.enddate AS enddate "
            StrQ &= "FROM trtraining_request_master " & _
                    "LEFT JOIN trdepartmentaltrainingneed_master on trdepartmentaltrainingneed_master.departmentaltrainingneedunkid = trtraining_request_master.departmentaltrainingneedunkid " & _
                           " AND trdepartmentaltrainingneed_master.isvoid = 0 " & _
                    "LEFT JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                    "LEFT JOIN cfcommon_master AS tcourse ON tcourse.masterunkid = trdepartmentaltrainingneed_master.trainingcourseunkid " & _
                           "AND tcourse.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & " " & _
                           "AND tcourse.isactive = 1 " & _
                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = trtraining_request_master.employeeunkid " & _
                         " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             stationunkid " & _
                          "            ,deptgroupunkid " & _
                          "            ,departmentunkid " & _
                          "            ,sectiongroupunkid " & _
                          "            ,sectionunkid " & _
                          "            ,unitgroupunkid " & _
                          "            ,unitunkid " & _
                          "            ,teamunkid " & _
                          "            ,classgroupunkid " & _
                          "             ,classunkid " & _
                          "            ,employeeunkid " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "       FROM hremployee_transfer_tran " & _
                          "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                          " LEFT JOIN hrstation_master hs ON t.stationunkid = hs.stationunkid " & _
                          " LEFT JOIN hrdepartment_master hd ON t.departmentunkid = hd.departmentunkid " & _
                          " LEFT JOIN hrsectiongroup_master hsg ON t.sectiongroupunkid = hsg.sectiongroupunkid " & _
                          " LEFT JOIN hrclassgroup_master hcg ON t.classgroupunkid = hcg.classgroupunkid " & _
                          " LEFT JOIN hrclasses_master hc ON t.classunkid = hc.classesunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "             SELECT " & _
                          "                 jobgroupunkid " & _
                          "                ,jobunkid " & _
                          "                ,employeeunkid " & _
                          "                ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                          "             FROM hremployee_categorization_tran " & _
                          "             WHERE isvoid = 0	AND CONVERT(CHAR(8), effectivedate, 112) <= @EmployeeAsOnDate  " & _
                          " ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                          " LEFT JOIN hrjob_master hjb ON hjb.jobunkid = j.jobunkid " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "           gradegroupunkid " & _
                          "          ,gradeunkid " & _
                          "          ,gradelevelunkid " & _
                          "          ,employeeunkid " & _
                          "          ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                          "        FROM prsalaryincrement_tran " & _
                          "        WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= @EmployeeAsOnDate " & _
                          " ) AS G ON G.employeeunkid = hremployee_master.employeeunkid	AND G.Rno = 1 " & _
                          " LEFT JOIN hrgradegroup_master hgd ON hgd.gradegroupunkid = G.gradegroupunkid "

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            'Hemant (25 Oct 2021) -- End

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE trtraining_request_master.isvoid = 0 " & _
                      "AND trtraining_calendar_master.isactive = 1 " & _
                      "AND trdepartmentaltrainingneed_master.statusunkid = '" & CInt(clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved) & "' " & _
                      "AND trtraining_request_master.departmentaltrainingneedunkid > 0 " & _
                      "AND trtraining_request_master.iscompleted_submit_approval = 1 " & _
                      "AND trtraining_request_master.completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Hemant (25 Oct 2021) -- End

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart.Date))
            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("EmpCode") = drRow.Item("employeecode")
                rpt_Row.Item("EmployeeNames") = drRow.Item("employee")
                rpt_Row.Item("Company") = drRow.Item("Branch")
                rpt_Row.Item("Function") = drRow.Item("Department")
                rpt_Row.Item("Department") = drRow.Item("SectionGrp")
                rpt_Row.Item("Zone") = drRow.Item("ClassGrp")
                rpt_Row.Item("Branch") = drRow.Item("Class")
                rpt_Row.Item("Grade") = drRow.Item("GradeGrp")
                rpt_Row.Item("JobTitle") = drRow.Item("Job")
                rpt_Row.Item("Gender") = drRow.Item("Gender")
                rpt_Row.Item("TrainingName") = drRow.Item("trainingcoursename")
                rpt_Row.Item("StartDate") = CDate(drRow.Item("startdate")).ToShortDateString
                rpt_Row.Item("EndDate") = CDate(drRow.Item("enddate")).ToShortDateString

                dtFinalTable.Rows.Add(rpt_Row)

            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(i) = 125
            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try

    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Emp Code")
            Language.setMessage(mstrModuleName, 2, "Employee Names")
            Language.setMessage(mstrModuleName, 3, "Company")
            Language.setMessage(mstrModuleName, 4, "Function")
            Language.setMessage(mstrModuleName, 5, "Department")
            Language.setMessage(mstrModuleName, 6, "Zone")
            Language.setMessage(mstrModuleName, 7, "Branch")
            Language.setMessage(mstrModuleName, 8, "Grade")
            Language.setMessage(mstrModuleName, 9, "Job Title")
            Language.setMessage(mstrModuleName, 10, "Gender")
            Language.setMessage(mstrModuleName, 11, "Training Name")
            Language.setMessage(mstrModuleName, 12, "Start Date")
            Language.setMessage(mstrModuleName, 13, "End Date")
            Language.setMessage(mstrModuleName, 14, "Calendar :")
            Language.setMessage(mstrModuleName, 15, "Training :")
            Language.setMessage(mstrModuleName, 16, "Employee :")
            Language.setMessage(mstrModuleName, 17, "Prepared By :")
            Language.setMessage(mstrModuleName, 18, "Checked By :")
            Language.setMessage(mstrModuleName, 19, "Approved By :")
            Language.setMessage(mstrModuleName, 20, "Received By :")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

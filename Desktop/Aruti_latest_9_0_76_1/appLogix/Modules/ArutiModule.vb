﻿Public Module ArutiModule
    Public Enum ArutiModule
        'eZee FrontDesk NextGen Modules ( Module Range : 1 to 14 )

        'eZee Burrp NextGen Modules ( Module Range : 15 to 28 )

        'eZee NextGen General Modules ( Module Range : 29 to 50 )

        'Aruti Lic (Module Range : 51 to 80 )
        'Payroll = 51
        'BasicHRMS = 52
        'LeaveTnA = 53
        'AdvancedHRRecruitment = 54
        'AdvancedHRAssessment = 55
        'AdvancedHRTraining = 56
        'AdvancedHRMedical = 57
        'AdvancedHRDiscipline = 58
        'CustomizationsAccountingSystemERPs = 59
        'CustomizationsEDI = 60
        'CustomizationsTnADevices = 61
        'CustomizationsstandardpredefinedpreprintedForms = 62


        'Anjan [ 09 Mar 2013 ] -- Start
        'ENHANCEMENT : New License Changes
        Payroll_Management = 0
        Loan_and_Savings_Management = 1
        Employee_BioData_Management = 2
        Leave_Management = 3
        Time_and_Attendance_Management = 4
        Employee_Performance_Appraisal_Management = 5
        Trainings_Needs_Analysis = 6
        On_Job_Training_Management = 7
        Recruitment_Management = 8
        Online_Job_Applications_Management = 9
        Employee_Disciplinary_Cases_Management = 10
        Medical_Bills_and_Claims_Management = 11
        Employee_Self_Service = 12
        Manager_Self_Service = 13
        Employee_Assets_Declarations = 14
        Custom_Report_Engine = 15
        Integration_Accounting_System_ERPs = 16
        Integration_Electronic_Banking_Interface = 17
        Integration_Time_Attendance_Devices = 18
        'Anjan [ 09 Mar 2013 ] -- End



    End Enum

End Module

﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmEmpSelection

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmEmpSelection"
    Private mblnCancel As Boolean = True
    Private dtEmployee As DataView = Nothing
    Private dtSelectedEmployee As DataView = Nothing

    Private menAction As enAction = enAction.ADD_ONE
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    Private mdtEmployeeAsOnStartDate As Date = FinancialYear._Object._Database_Start_Date
    Private mdtEmployeeAsOnEndDate As Date = FinancialYear._Object._Database_End_Date
    'Private mdtSearchEmployee As DataTable = Nothing
    'Private mdtLeaveTypeMapping As DataTable = Nothing

    Private mstrReportBy_Ids As String = String.Empty
    Private mstrReportBy_Name As String = String.Empty
    Private mintViewIndex As Integer
    Private mstrAnalysis_Fields As String = String.Empty
    Private mstrAnalysis_Join As String = String.Empty
    Private mstrAnalysis_OrderBy As String = String.Empty
    Private mstrAnalysis_OrderBy_GroupName As String = String.Empty
    Private mstrHr_EmployeeTable_Alias As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    Private mstrAnalysis_TableName As String = String.Empty
    'Sohail (01 Mar 2017) -- Start
    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
    Private mstrAnalysis_CodeField As String = String.Empty
    'Sohail (01 Mar 2017) -- End

    Private mblnCloseForm As Boolean = False
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef strEmpUnkIDs As String _
                                  , Optional ByVal strHr_EmployeeTable_Alias As String = "hremployee_master" _
                                  , Optional ByVal blnCloseForm As Boolean = False _
                                  ) As Boolean
        Try
            mstrEmployeeIDs = strEmpUnkIDs
            mstrHr_EmployeeTable_Alias = strHr_EmployeeTable_Alias
            mblnCloseForm = blnCloseForm
            'menAction = eAction

            Me.ShowDialog()

            strEmpUnkIDs = mstrEmployeeIDs

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Properties "

    Public Property _ReportBy_Ids() As String
        Get
            Return mstrReportBy_Ids
        End Get
        Set(ByVal value As String)
            mstrReportBy_Ids = value
        End Set
    End Property

    Public Property _ReportBy_Name() As String
        Get
            Return mstrReportBy_Name
        End Get
        Set(ByVal value As String)
            mstrReportBy_Name = value
        End Set
    End Property

    Public ReadOnly Property _ViewIndex() As Integer
        Get
            Return mintViewIndex
        End Get
    End Property

    Public ReadOnly Property _Analysis_Fields() As String
        Get
            Return mstrAnalysis_Fields
        End Get
    End Property

    Public ReadOnly Property _Analysis_Join() As String
        Get
            Return mstrAnalysis_Join
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy() As String
        Get
            Return mstrAnalysis_OrderBy
        End Get
    End Property

    Public ReadOnly Property _Analysis_OrderBy_GName() As String
        Get
            Return mstrAnalysis_OrderBy_GroupName
        End Get
    End Property

    Public ReadOnly Property _Report_GroupName() As String
        Get
            Return mstrReport_GroupName
        End Get
    End Property

    Public ReadOnly Property _Analysis_TableName() As String
        Get
            Return mstrAnalysis_TableName
        End Get
    End Property

    Public WriteOnly Property _EmployeeAsOnStartDate() As Date
        Set(ByVal value As Date)
            mdtEmployeeAsOnStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeAsOnEndDate() As Date
        Set(ByVal value As Date)
            mdtEmployeeAsOnEndDate = value
        End Set
    End Property

    'Sohail (01 Mar 2017) -- Start
    'Enhancement #24 -  65.1 - Export and Import option on Budget Codes.
    Public ReadOnly Property _Analysis_CodeField() As String
        Get
            Return mstrAnalysis_CodeField
        End Get
    End Property
    'Sohail (01 Mar 2017) -- End
#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            'cboApproveLevel.BackColor = GUI.ColorComp
            'txtName.BackColor = GUI.ColorComp
            'cboUser.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'cboApproveLevel.SelectedValue = objLeaveApprover._Levelunkid

            Dim objEmployee As New clsEmployee_Master


            'If objLeaveApprover._Isexternalapprover = True Then
            '    Dim objUser As New clsUserAddEdit
            '    objUser._Userunkid = objLeaveApprover._leaveapproverunkid
            '    Dim sName As String = objUser._Firstname + " " + objUser._Lastname
            '    txtName.Text = CStr(IIf(sName.Trim = "", objUser._Username, sName))
            '    txtName.Tag = objUser._Userunkid
            '    objUser = Nothing
            'Else
            '    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeaveApprover._leaveapproverunkid
            '    txtName.Text = objEmployee._Firstname + " " + objEmployee._Surname
            '    txtName.Tag = objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If


            'mdtTran = objLeaveApproverTran._DataList


            'If menAction = enAction.EDIT_ONE Then

            '    mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), objLeaveApprover._Isexternalapprover)

            '    FillEmployeeList()
            '    cboUser.SelectedValue = objLeaveApprover._MapUserId
            '    Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
            '    mdtLeaveTypeMapping = objLeaveTypeMapping.GetLeaveTypeForMapping(mintApproverUnkid)
            '    objLeaveTypeMapping = Nothing
            'Else
            '    cboUser.SelectedValue = 0
            'End If
            If mstrEmployeeIDs.Trim <> "" Then

                Dim dtTable As DataTable = New DataView(dtEmployee.ToTable, "", "", DataViewRowState.CurrentRows).ToTable

                If dtSelectedEmployee Is Nothing Then
                    dtSelectedEmployee.Table = dtEmployee.ToTable.Clone
                End If

                If dtSelectedEmployee.ToTable.Columns.Contains("IsChecked") = False Then
                    dtSelectedEmployee.Table = dtEmployee.ToTable.Clone
                End If

            End If

            FillSelectedEmplyeeList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'objLeaveApprover._Levelunkid = CInt(cboApproveLevel.SelectedValue)
            'objLeaveApprover._leaveapproverunkid = CInt(txtName.Tag)
            'objLeaveApprover._Departmentunkid = -1
            'objLeaveApprover._Sectionunkid = -1
            'objLeaveApprover._Jobunkid = -1
            'If chkExternalApprover.Checked = True Then
            '    objLeaveApprover._MapUserId = CInt(txtName.Tag)
            'Else
            '    objLeaveApprover._MapUserId = CInt(cboUser.SelectedValue)
            'End If
            'objLeaveApprover._dtLeaveTypeMapping = mdtLeaveTypeMapping
            'objLeaveApprover._Userumkid = User._Object._Userunkid

            'objLeaveApprover._Isexternalapprover = chkExternalApprover.Checked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        Dim strSearch As String = String.Empty
        Try
            Dim objEmployee As New clsEmployee_Master

            Cursor.Current = Cursors.WaitCursor

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter & " "
            End If

            'If menAction <> enAction.EDIT_ONE Then
            '    If mstrEmployeeIDs.Trim.Length > 0 Then
            '        strSearch &= "AND hremployee_master.employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            '    End If
            'End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            Dim blnInActiveEmp As Boolean = False
            'If menAction <> enAction.EDIT_ONE Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If

            dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             mdtEmployeeAsOnStartDate, _
                                             mdtEmployeeAsOnEndDate, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             blnInActiveEmp, _
                                             "Employee", _
                                            ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearch, False, False)


            Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            dtCol.DefaultValue = False
            dtCol.AllowDBNull = False
            dsEmployee.Tables(0).Columns.Add(dtCol)

            dtEmployee = New DataView(dsEmployee.Tables(0))

            If mstrEmployeeIDs.Trim.Length > 0 Then
                Dim dRow() As DataRow = dtEmployee.Table.Select("employeeunkid IN(" & mstrEmployeeIDs & ")")
                If dRow.Length > 0 Then
                    For Each row As DataRow In dRow
                        row.Item("IsChecked") = True
                    Next
                    dtEmployee.Table.AcceptChanges()
                End If
            End If

            dgvEmployee.AutoGenerateColumns = False

            objcolhIsCheck.DataPropertyName = "IsChecked"
            objcolhEmpId.DataPropertyName = "employeeunkid"
            colhEmpCode.DataPropertyName = "employeecode"
            colhEmpName.DataPropertyName = "name"
            objcolhDeptId.DataPropertyName = "departmentunkid"
            objcolhDept.DataPropertyName = "DeptName"
            objcolhSectionId.DataPropertyName = "sectionunkid"
            objcolhSection.DataPropertyName = "section"
            objcolhJobId.DataPropertyName = "jobunkid"
            objcolhJob.DataPropertyName = "job_name"

            dgvEmployee.DataSource = dtEmployee

            lblCount.Text = "( " & dgvEmployee.RowCount.ToString & " )"

            'RemoveHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

            'mdtSearchEmployee = dtEmployee

            'lvEmployee.Items.Clear()
            'If Not dtEmployee Is Nothing Then
            '    Dim lvItem As ListViewItem

            '    For Each drRow As DataRow In dtEmployee.Rows
            '        lvItem = New ListViewItem
            '        'lvItem.Text = drRow("name").ToString
            '        lvItem.Tag = drRow("employeeunkid")
            '        lvItem.Text = drRow("employeecode").ToString
            '        lvItem.SubItems.Add(drRow("name").ToString)
            '        lvItem.SubItems.Add(drRow("DeptName").ToString)
            '        lvItem.SubItems.Add(drRow("section").ToString)
            '        lvItem.SubItems.Add(drRow("job_name").ToString)
            '        lvItem.SubItems.Add(drRow("departmentunkid").ToString)
            '        lvItem.SubItems.Add(drRow("sectionunkid").ToString)
            '        lvItem.SubItems.Add(drRow("jobunkid").ToString)
            '        lvEmployee.Items.Add(lvItem)
            '    Next

            '    If lvEmployee.Items.Count > 20 Then
            '        ColhEmp.Width = 210 - 18
            '    Else
            '        ColhEmp.Width = 210
            '    End If

            'End If



            'If lvEmployee.Items.Count > 0 Then
            '    chkEmployeeAll.Enabled = True
            'Else
            '    chkEmployeeAll.Checked = False
            '    chkEmployeeAll.Enabled = False
            'End If

            'AddHandler lvEmployee.ItemChecked, AddressOf lvEmployee_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub FillSelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Try
            If dtSelectedEmployee Is Nothing Then Exit Try

            If dtSelectedEmployee.Table.Columns.Contains("IsChecked") = False Then
                dtSelectedEmployee.Table = dtEmployee.Table.Clone
            End If

            dgvSelectedEmployee.AutoGenerateColumns = False

            objcolhSelectedIsCheck.DataPropertyName = "IsChecked"
            objcolhSelectedEmpId.DataPropertyName = "employeeunkid"
            colhSelectedEmpCode.DataPropertyName = "employeecode"
            colhSelectedEmpName.DataPropertyName = "name"
            objcolhSelectedDeptId.DataPropertyName = "departmentunkid"
            colhSelectedDept.DataPropertyName = "DeptName"
            objcolhSelectedSectionId.DataPropertyName = "sectionunkid"
            colhSelectedSection.DataPropertyName = "section"
            objcolhSelectedJobId.DataPropertyName = "jobunkid"
            colhSelectedJob.DataPropertyName = "job_name"

            dgvSelectedEmployee.DataSource = dtSelectedEmployee
            dgvSelectedEmployee.Refresh()

            lblSelectedCount.Text = "( " & dgvSelectedEmployee.RowCount.ToString & " )"

            RemoveHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
            Dim dtRow() As DataRow = dtSelectedEmployee.Table.Select("IsChecked = True", "")
            If dtRow.Length > 0 Then
                If dtSelectedEmployee.Table.Rows.Count = dtRow.Length Then
                    chkAllSelectedEmployee.CheckState = CheckState.Checked
                Else
                    chkAllSelectedEmployee.CheckState = CheckState.Indeterminate
                End If
            Else
                chkAllSelectedEmployee.CheckState = CheckState.Unchecked
            End If
            AddHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged

            'If Not dtEmployee Is Nothing Then
            '    Dim lvItem As ListViewItem
            '    lvSelectedEmployee.Items.Clear()
            '    For Each drRow As DataRow In dtEmployee.Rows
            '        lvItem = New ListViewItem
            '        lvItem.Text = ""
            '        lvItem.Tag = CInt(drRow("leaveapprovertranunkid"))
            '        lvItem.SubItems.Add(drRow("approvername").ToString)
            '        lvItem.SubItems.Add(drRow("departmentname").ToString)
            '        lvItem.SubItems.Add(drRow("sectionname").ToString)
            '        lvItem.SubItems.Add(drRow("jobname").ToString)
            '        lvItem.SubItems.Add(drRow("name").ToString)
            '        lvItem.SubItems.Add(drRow("GUID").ToString)
            '        lvItem.SubItems.Add(drRow("employeeunkid").ToString)
            '        lvSelectedEmployee.Items.Add(lvItem)
            '    Next
            'End If
            'If lvSelectedEmployee.Items.Count > 0 Then
            '    chkAllSelectedEmployee.Enabled = True
            'Else
            '    chkAllSelectedEmployee.Checked = False
            '    chkAllSelectedEmployee.Enabled = False
            'End If

            'AddHandler lvSelectedEmployee.ItemChecked, AddressOf lvSelectedEmployee_ItemChecked

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSelectedEmplyeeList", mstrModuleName)
        End Try

    End Sub

    Private Function Validation() As Boolean
        Try
            If dtEmployee.ToTable.Select("IsChecked = 1").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee."), enMsgBoxStyle.Information)
                dgvEmployee.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetVisibility()
        Try
            'objbtnAddLevel.Enabled = User._Object.Privilege._AddLeaveApproverLevel
            'cboUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser
            'objbtnSearchUser.Enabled = User._Object.Privilege._AllowMapApproverWithUser
            'lnkMapLeaveType.Enabled = User._Object.Privilege._AllowtoMapLeaveType
            'lnkMapLeaveType.Visible = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetOperation(ByVal dtView As DataView, ByVal blnOperation As Boolean)
        Try
            For Each dtRow As DataRowView In dtEmployee
                dtRow.Item("IsChecked") = blnOperation
                dtRow.EndEdit()
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetOperation", mstrModuleName)
        End Try
    End Sub

    Private Sub SetSelectedEmployeeOperation(ByVal blnOperation As Boolean)
        Try
            For Each dtRow As DataRowView In dtSelectedEmployee
                dtRow.Item("IsChecked") = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetSelectedEmployeeOperation", mstrModuleName)
        End Try
    End Sub

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Public Shared Function GetAnalysisByDetails(ByVal strEmpSelectionModuleName As String _
                                                , ByVal strReportBy_Ids As String _
                                                , Optional ByVal strHr_EmployeeTable_Alias As String = "" _
                                                , Optional ByRef stroutAnalysis_Fields As String = "" _
                                                , Optional ByRef stroutAnalysis_Join As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy As String = "" _
                                                , Optional ByRef stroutAnalysis_OrderBy_GroupName As String = "" _
                                                , Optional ByRef stroutReport_GroupName As String = "" _
                                                , Optional ByRef stroutAnalysis_TableName As String = "" _
                                                , Optional ByRef stroutAnalysis_CodeField As String = "" _
                                                ) As Boolean
        'Sohail (01 Mar 2017) - [stroutAnalysis_CodeField]

        Try
            'mstrReportBy_Name = String.Join(",", (From p In dtSelectedEmployee.ToTable Select (p.Item("name").ToString)).ToArray)
            If strHr_EmployeeTable_Alias.Trim = "" Then strHr_EmployeeTable_Alias = "hremployee_master"

            'mintViewIndex = 0
            stroutAnalysis_Fields = ", " & strHr_EmployeeTable_Alias & ".employeeunkid AS Id, " & strHr_EmployeeTable_Alias & ".firstname + ' ' + ISNULL(" & strHr_EmployeeTable_Alias & ".othername, '') + ' ' + " & strHr_EmployeeTable_Alias & ".surname AS GName "
            stroutAnalysis_Join = "AND " & strHr_EmployeeTable_Alias & ".employeeunkid IN ( " & strReportBy_Ids & " ) "
            stroutAnalysis_OrderBy = " " & strHr_EmployeeTable_Alias & ".employeeunkid "
            stroutAnalysis_OrderBy_GroupName = " " & strHr_EmployeeTable_Alias & ".firstname + ' ' + ISNULL(" & strHr_EmployeeTable_Alias & ".othername, '') + ' ' + " & strHr_EmployeeTable_Alias & ".surname "
            stroutReport_GroupName = Language.getMessage(strEmpSelectionModuleName, 1, "Employee :")
            stroutAnalysis_TableName = " " & strHr_EmployeeTable_Alias & " "
            stroutAnalysis_CodeField = ", " & strHr_EmployeeTable_Alias & ".employeecode AS GCode " 'Sohail (01 Mar 2017)

        Catch ex As Exception
            Throw New Exception("Procedure name : GetAnalysisByDetails ; ModuleName : " & strEmpSelectionModuleName & ";" & ex.Message)
        End Try
    End Function
    'Sohail (02 Sep 2016) -- End

#End Region

#Region "Form's Event"

    Private Sub frmEmpSelection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'objLeaveApprover = New clsleaveapprover_master
        'objLeaveApproverTran = New clsleaveapprover_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            setColor()
            If mstrEmployeeIDs.Trim.Length > 0 Then
                Call FillEmployeeList()
                Call btnAdd.PerformClick()
            End If
            'mdtTran = objLeaveApproverTran._DataList
            Call SetVisibility()
            Call FillCombo()
            Call GetValue()

            If mblnCloseForm = True Then
                btnSave.PerformClick()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpSelection_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpSelection_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpSelection_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpSelection_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpSelection_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpSelection_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'objLeaveApprover = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveapprover_master.SetMessages()
            clsleaveapprover_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveapprover_master,clsleaveapprover_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If dtEmployee Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Fill employee list to Add employees."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If Validation() = False Then Exit Sub

            Cursor.Current = Cursors.WaitCursor

            'If dtSelectedEmployee Is Nothing Then 

            If dtSelectedEmployee Is Nothing OrElse dtSelectedEmployee.ToTable.Columns.Contains("IsChecked") = False Then
                dtSelectedEmployee = New DataView
                dtSelectedEmployee.Table = dtEmployee.ToTable.Clone
            End If

            Dim dtTable As DataTable = New DataView(dtEmployee.ToTable, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable.Copy
            Dim dtOld As DataTable = dtSelectedEmployee.ToTable.Copy
            Dim dtCol() As DataColumn = {dtTable.Columns("employeeunkid")}
            dtTable.PrimaryKey = dtCol
            dtOld.Merge(dtTable, True)
            Dim dtDiff As DataTable = dtOld.GetChanges()
            For Each dRow As DataRow In dtDiff.Select("IsChecked=1")
                dRow.Item("IsChecked") = False
            Next
            dtDiff.AcceptChanges()
            dtSelectedEmployee.Table = dtDiff.Copy

            Call FillSelectedEmplyeeList()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dtSelectedEmployee Is Nothing Then Exit Try

            If dtSelectedEmployee.ToTable.Select("IsChecked = 1").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), enMsgBoxStyle.Information)
                dgvSelectedEmployee.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employees?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                dtSelectedEmployee.Table.AcceptChanges()
                For Each dtRow As DataRow In dtSelectedEmployee.Table.Select("IsChecked = 1")
                    dtSelectedEmployee.Table.Rows.Remove(dtRow)
                Next
            End If
            dtSelectedEmployee.Table.AcceptChanges()
            FillSelectedEmplyeeList()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If dtSelectedEmployee Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Add atleast one employee."), enMsgBoxStyle.Information)
                Exit Try
            End If

            'If dtSelectedEmployee.Table.Select("IsChecked = 1").Length <= 0 Then
            If dtSelectedEmployee.ToTable.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information. Please Add atleast One Employee."), enMsgBoxStyle.Information)
                dgvSelectedEmployee.Focus()
                Exit Try
            End If

            'mstrEmployeeIDs = String.Join(",", (From p In dtSelectedEmployee.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToArray)
            mstrEmployeeIDs = String.Join(",", (From p In dtSelectedEmployee.ToTable Select (p.Item("employeeunkid").ToString)).ToArray)
            mstrReportBy_Ids = mstrEmployeeIDs
            mstrReportBy_Name = String.Join(",", (From p In dtSelectedEmployee.ToTable Select (p.Item("name").ToString)).ToArray)
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            'If mstrHr_EmployeeTable_Alias.Trim = "" Then mstrHr_EmployeeTable_Alias = "hremployee_master"

            'mintViewIndex = 0
            'mstrAnalysis_Fields = ", " & mstrHr_EmployeeTable_Alias & ".employeeunkid AS Id, " & mstrHr_EmployeeTable_Alias & ".firstname + ' ' + ISNULL(" & mstrHr_EmployeeTable_Alias & ".othername, '') + ' ' + " & mstrHr_EmployeeTable_Alias & ".surname AS GName "
            'mstrAnalysis_Join = "AND " & mstrHr_EmployeeTable_Alias & ".employeeunkid IN ( " & mstrReportBy_Ids & " ) "
            'mstrAnalysis_OrderBy = " " & mstrHr_EmployeeTable_Alias & ".employeeunkid "
            'mstrAnalysis_OrderBy_GroupName = " " & mstrHr_EmployeeTable_Alias & ".firstname + ' ' + ISNULL(" & mstrHr_EmployeeTable_Alias & ".othername, '') + ' ' + " & mstrHr_EmployeeTable_Alias & ".surname "
            'mstrReport_GroupName = Language.getMessage(mstrModuleName, 1, "Employee :")
            'mstrAnalysis_TableName = " " & mstrHr_EmployeeTable_Alias & " "
            Call GetAnalysisByDetails(mstrModuleName _
                                      , mstrReportBy_Ids _
                                      , mstrHr_EmployeeTable_Alias _
                                      , mstrAnalysis_Fields _
                                      , mstrAnalysis_Join _
                                      , mstrAnalysis_OrderBy _
                                      , mstrAnalysis_OrderBy_GroupName _
                                      , mstrReport_GroupName _
                                      , mstrAnalysis_TableName _
                                      , mstrAnalysis_CodeField _
                                      )
            'Sohail (01 Mar 2017) - [mstrAnalysis_CodeField]
            'Sohail (02 Sep 2016) -- End

            mblnCancel = False

            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillSelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            chkAllSelectedEmployee.Checked = False
            FillSelectedEmplyeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub objbtnAddLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLevel.Click
    '    Dim frm As New frmEmpSelection
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnAddLevel_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
    '    Dim objfrm As New frmCommonSearch
    '    Dim objEmployee As New clsEmployee_Master
    '    Dim dsList As DataSet
    '    Try

    '        'S.SANDEEP [30 JAN 2016] -- START
    '        ''Sohail (06 Jan 2012) -- Start
    '        ''TRA - ENHANCEMENT
    '        ''dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
    '        ''                                     CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)))

    '        ''S.SANDEEP [04 JUN 2015] -- START
    '        ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''If menAction = enAction.EDIT_ONE Then
    '        ''    dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
    '        ''                                         CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)), , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
    '        ''Else
    '        ''    dsList = objEmployee.GetEmployeeList("Employee", False, True, , CInt(IIf(CInt(cboDepartment.SelectedValue) > 0, CInt(cboDepartment.SelectedValue), 0)), _
    '        ''                                         CInt(IIf(CInt(cboSection.SelectedValue) > 0, CInt(cboSection.SelectedValue), 0)), , , , , , , CInt(IIf(CInt(cboJob.SelectedValue) > 0, CInt(cboJob.SelectedValue), 0)), , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
    '        ''End If
    '        'dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '        '                                     User._Object._Userunkid, _
    '        '                                     FinancialYear._Object._YearUnkid, _
    '        '                                     Company._Object._Companyunkid, _
    '        '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '        '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '        '                                     ConfigParameter._Object._UserAccessModeSetting, _
    '        '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, _
    '        '                                     0, CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue))
    '        ''S.SANDEEP [04 JUN 2015] -- END
    '        If chkExternalApprover.Checked = False Then
    '            Dim blnInActiveEmp As Boolean = False
    '            If menAction <> enAction.EDIT_ONE Then
    '                blnInActiveEmp = False
    '            Else
    '                blnInActiveEmp = True
    '            End If

    '            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                                                 User._Object._Userunkid, _
    '                                                 FinancialYear._Object._YearUnkid, _
    '                                                 Company._Object._Companyunkid, _
    '                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                 ConfigParameter._Object._UserAccessModeSetting, _
    '                                               True, blnInActiveEmp, "Employee", False, _
    '                                                 0, CInt(cboDepartment.SelectedValue), CInt(cboSection.SelectedValue), CInt(cboJob.SelectedValue))

    '            'Pinkal (06-Jan-2016) -- End

    '            'S.SANDEEP [04 JUN 2015] -- END


    '            objfrm.DataSource = dsList.Tables("Employee")
    '            objfrm.ValueMember = "employeeunkid"
    '            objfrm.DisplayMember = "employeename"
    '            objfrm.CodeMember = "employeecode"
    '            If objfrm.DisplayDialog() Then
    '                txtName.Text = objfrm.SelectedText
    '                txtName.Tag = objfrm.SelectedValue

    '                'S.SANDEEP [30 JAN 2016] -- START
    '                'mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag))
    '                mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)
    '                'S.SANDEEP [30 JAN 2016] -- END

    '                FillEmployeeList()

    '                Dim objOption As New clsPassowdOptions
    '                If objOption._IsEmployeeAsUser Then
    '                    Dim objUser As New clsUserAddEdit
    '                    Dim mintUserID As Integer = objUser.Return_UserId(CInt(objfrm.SelectedValue), Company._Object._Companyunkid)
    '                    Dim drRow() As DataRow = CType(cboUser.DataSource, DataTable).Select("userunkid = " & mintUserID)
    '                    If drRow.Length > 0 Then
    '                        cboUser.SelectedValue = mintUserID
    '                    Else
    '                        cboUser.SelectedValue = 0
    '                    End If

    '                End If
    '            End If
    '        Else
    '            Dim objUser As New clsUserAddEdit

    '            dsList = objUser.GetExternalApproverList("List", _
    '                                                     Company._Object._Companyunkid, _
    '                                                     FinancialYear._Object._YearUnkid, _
    '                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), "264")
    '            objUser = Nothing

    '            objfrm.DataSource = dsList.Tables("List")
    '            objfrm.ValueMember = "userunkid"
    '            objfrm.DisplayMember = "Name"
    '            objfrm.CodeMember = "username"
    '            If objfrm.DisplayDialog() Then
    '                txtName.Text = objfrm.SelectedText
    '                txtName.Tag = objfrm.SelectedValue
    '                'S.SANDEEP [30 JAN 2016] -- START
    '                'mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag))
    '                mstrEmployeeIDs = objLeaveApprover.GetApproverEmployeeId(CInt(txtName.Tag), chkExternalApprover.Checked)
    '                'S.SANDEEP [30 JAN 2016] -- END
    '                FillEmployeeList()
    '            End If
    '            objUser = Nothing
    '        End If
    '        'S.SANDEEP [30 JAN 2016] -- END

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'mstrEmployeeIDs = ""
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchedEmployee.Text = ""
            FillEmployeeList()
            'lvEmployee_ItemChecked(sender, New ItemCheckedEventArgs(Nothing))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkEmployeeAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmployeeAll.CheckedChanged
        Try
            If dgvEmployee.RowCount <= 0 Then Exit Sub
            SetOperation(dtEmployee, chkEmployeeAll.Checked)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEmployeeAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkAllSelectedEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllSelectedEmployee.CheckedChanged
        Try
            If dgvSelectedEmployee.RowCount <= 0 Then Exit Sub
            SetSelectedEmployeeOperation(chkAllSelectedEmployee.Checked)
            dgvSelectedEmployee.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAllSelectedEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Try
            'RemoveHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
            'If lvEmployee.CheckedItems.Count <= 0 Then
            '    chkEmployeeAll.CheckState = CheckState.Unchecked

            'ElseIf lvEmployee.CheckedItems.Count < lvEmployee.Items.Count Then
            '    chkEmployeeAll.CheckState = CheckState.Indeterminate

            'ElseIf lvEmployee.CheckedItems.Count = lvEmployee.Items.Count Then
            '    chkEmployeeAll.CheckState = CheckState.Checked

            'End If
            'AddHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasters_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvSelectedEmployee_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        Try
            'RemoveHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
            'If lvSelectedEmployee.CheckedItems.Count <= 0 Then
            '    chkAllSelectedEmployee.CheckState = CheckState.Unchecked

            'ElseIf lvSelectedEmployee.CheckedItems.Count < lvSelectedEmployee.Items.Count Then
            '    chkAllSelectedEmployee.CheckState = CheckState.Indeterminate

            'ElseIf lvSelectedEmployee.CheckedItems.Count = lvSelectedEmployee.Items.Count Then
            '    chkAllSelectedEmployee.CheckState = CheckState.Checked

            'End If
            'AddHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSelectedEmployee_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub



#End Region

#Region " Textbox's Events "
    Private Sub txtSearchedEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchedEmployee.TextChanged
        Try
            If txtSearchedEmployee.Text.Trim <> "" Then
                dtEmployee.RowFilter = "employeecode LIKE '%" & txtSearchedEmployee.Text & "%' OR name LIKE '%" & txtSearchedEmployee.Text & "%' "
            Else
                dtEmployee.RowFilter = ""
            End If

            dgvEmployee.DataSource = dtEmployee

            lblCount.Text = "( " & dgvEmployee.RowCount.ToString & " )"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchedEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchedSelectedEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchedSelectedEmployee.TextChanged
        Try
            If txtSearchedSelectedEmployee.Text.Trim <> "" Then
                dtSelectedEmployee.RowFilter = "employeecode LIKE '%" & txtSearchedSelectedEmployee.Text & "%' OR name LIKE '%" & txtSearchedSelectedEmployee.Text & "%' "
            Else
                dtSelectedEmployee.RowFilter = ""
            End If

            dgvSelectedEmployee.DataSource = dtSelectedEmployee

            lblSelectedCount.Text = "( " & dgvSelectedEmployee.RowCount.ToString & " )"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchedSelectedEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Checkbox Event "



#End Region

#Region " DataGrid's Events "
    Private Sub dgvEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            RemoveHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged

            If e.ColumnIndex = objcolhIsCheck.Index Then

                If Me.dgvEmployee.IsCurrentCellDirty Then
                    Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dtEmployee.Table.AcceptChanges()

                Dim dtRow() As DataRow = dtEmployee.Table.Select("IsChecked = True", "")
                If dtRow.Length > 0 Then
                    If dtEmployee.Table.Rows.Count = dtRow.Length Then
                        chkEmployeeAll.CheckState = CheckState.Checked
                    Else
                        chkEmployeeAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkEmployeeAll.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler chkEmployeeAll.CheckedChanged, AddressOf chkEmployeeAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvSelectedEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSelectedEmployee.CellContentClick
        Try
            RemoveHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged

            If e.ColumnIndex = objcolhIsCheck.Index Then

                If Me.dgvSelectedEmployee.IsCurrentCellDirty Then
                    Me.dgvSelectedEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dtSelectedEmployee.Table.AcceptChanges()

                Dim dtRow() As DataRow = dtSelectedEmployee.Table.Select("IsChecked = True", "")
                If dtRow.Length > 0 Then
                    If dtSelectedEmployee.Table.Rows.Count = dtRow.Length Then
                        chkAllSelectedEmployee.CheckState = CheckState.Checked
                    Else
                        chkAllSelectedEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    chkAllSelectedEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler chkAllSelectedEmployee.CheckedChanged, AddressOf chkAllSelectedEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvSelectedEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbApproversInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApproversInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSelectedEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSelectedEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbApproversInfo.Text = Language._Object.getCaption(Me.gbApproversInfo.Name, Me.gbApproversInfo.Text)
            Me.gbSelectedEmployee.Text = Language._Object.getCaption(Me.gbSelectedEmployee.Name, Me.gbSelectedEmployee.Text)
            Me.chkAllSelectedEmployee.Text = Language._Object.getCaption(Me.chkAllSelectedEmployee.Name, Me.chkAllSelectedEmployee.Text)
            Me.lblFilterjob.Text = Language._Object.getCaption(Me.lblFilterjob.Name, Me.lblFilterjob.Text)
            Me.lblFilterSection.Text = Language._Object.getCaption(Me.lblFilterSection.Name, Me.lblFilterSection.Text)
            Me.lblFilterDepartment.Text = Language._Object.getCaption(Me.lblFilterDepartment.Name, Me.lblFilterDepartment.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.chkEmployeeAll.Text = Language._Object.getCaption(Me.chkEmployeeAll.Name, Me.chkEmployeeAll.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.lblApprovalTo.Text = Language._Object.getCaption(Me.lblApprovalTo.Name, Me.lblApprovalTo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblCount.Text = Language._Object.getCaption(Me.lblCount.Name, Me.lblCount.Text)
            Me.lblSelectedCount.Text = Language._Object.getCaption(Me.lblSelectedCount.Name, Me.lblSelectedCount.Text)
            Me.colhEmpCode.HeaderText = Language._Object.getCaption(Me.colhEmpCode.Name, Me.colhEmpCode.HeaderText)
            Me.colhEmpName.HeaderText = Language._Object.getCaption(Me.colhEmpName.Name, Me.colhEmpName.HeaderText)
            Me.colhSelectedEmpCode.HeaderText = Language._Object.getCaption(Me.colhSelectedEmpCode.Name, Me.colhSelectedEmpCode.HeaderText)
            Me.colhSelectedEmpName.HeaderText = Language._Object.getCaption(Me.colhSelectedEmpName.Name, Me.colhSelectedEmpName.HeaderText)
            Me.colhSelectedDept.HeaderText = Language._Object.getCaption(Me.colhSelectedDept.Name, Me.colhSelectedDept.HeaderText)
            Me.colhSelectedSection.HeaderText = Language._Object.getCaption(Me.colhSelectedSection.Name, Me.colhSelectedSection.HeaderText)
            Me.colhSelectedJob.HeaderText = Language._Object.getCaption(Me.colhSelectedJob.Name, Me.colhSelectedJob.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee.")
            Language.setMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete Selected Employees?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpSelection
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpSelection))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbApproversInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbSelectedEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblSelectedCount = New System.Windows.Forms.Label
        Me.txtSearchedSelectedEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.pnlSelectedEmployee = New System.Windows.Forms.Panel
        Me.chkAllSelectedEmployee = New System.Windows.Forms.CheckBox
        Me.dgvSelectedEmployee = New System.Windows.Forms.DataGridView
        Me.objcolhSelectedIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cboFilterJob = New System.Windows.Forms.ComboBox
        Me.lblFilterjob = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.cboFilterSection = New System.Windows.Forms.ComboBox
        Me.lblFilterSection = New System.Windows.Forms.Label
        Me.cboeFilterDept = New System.Windows.Forms.ComboBox
        Me.lblFilterDepartment = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.gbEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCount = New System.Windows.Forms.Label
        Me.chkEmployeeAll = New System.Windows.Forms.CheckBox
        Me.dgvEmployee = New System.Windows.Forms.DataGridView
        Me.objcolhIsCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.txtSearchedEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblApprovalTo = New System.Windows.Forms.Label
        Me.cboApprovalTo = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblSection = New System.Windows.Forms.Label
        Me.objcolhSelectedEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSelectedEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSelectedEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSelectedDeptId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSelectedDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSelectedSectionId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSelectedSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSelectedJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSelectedJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDeptId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSectionId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbApproversInfo.SuspendLayout()
        Me.gbSelectedEmployee.SuspendLayout()
        Me.pnlSelectedEmployee.SuspendLayout()
        CType(Me.dgvSelectedEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployee.SuspendLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbApproversInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.lblJob)
        Me.pnlMainInfo.Controls.Add(Me.lblDepartment)
        Me.pnlMainInfo.Controls.Add(Me.lblSection)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(927, 533)
        Me.pnlMainInfo.TabIndex = 1
        '
        'gbApproversInfo
        '
        Me.gbApproversInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApproversInfo.Checked = False
        Me.gbApproversInfo.CollapseAllExceptThis = False
        Me.gbApproversInfo.CollapsedHoverImage = Nothing
        Me.gbApproversInfo.CollapsedNormalImage = Nothing
        Me.gbApproversInfo.CollapsedPressedImage = Nothing
        Me.gbApproversInfo.CollapseOnLoad = False
        Me.gbApproversInfo.Controls.Add(Me.gbSelectedEmployee)
        Me.gbApproversInfo.Controls.Add(Me.gbEmployee)
        Me.gbApproversInfo.Controls.Add(Me.lblApprovalTo)
        Me.gbApproversInfo.Controls.Add(Me.cboApprovalTo)
        Me.gbApproversInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproversInfo.ExpandedHoverImage = Nothing
        Me.gbApproversInfo.ExpandedNormalImage = Nothing
        Me.gbApproversInfo.ExpandedPressedImage = Nothing
        Me.gbApproversInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproversInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproversInfo.HeaderHeight = 25
        Me.gbApproversInfo.HeaderMessage = ""
        Me.gbApproversInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApproversInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproversInfo.HeightOnCollapse = 0
        Me.gbApproversInfo.LeftTextSpace = 0
        Me.gbApproversInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbApproversInfo.Name = "gbApproversInfo"
        Me.gbApproversInfo.OpenHeight = 300
        Me.gbApproversInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproversInfo.ShowBorder = True
        Me.gbApproversInfo.ShowCheckBox = False
        Me.gbApproversInfo.ShowCollapseButton = False
        Me.gbApproversInfo.ShowDefaultBorderColor = True
        Me.gbApproversInfo.ShowDownButton = False
        Me.gbApproversInfo.ShowHeader = True
        Me.gbApproversInfo.Size = New System.Drawing.Size(927, 478)
        Me.gbApproversInfo.TabIndex = 0
        Me.gbApproversInfo.Temp = 0
        Me.gbApproversInfo.Text = "Employee Info"
        Me.gbApproversInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbSelectedEmployee
        '
        Me.gbSelectedEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.Checked = True
        Me.gbSelectedEmployee.CollapseAllExceptThis = False
        Me.gbSelectedEmployee.CollapsedHoverImage = Nothing
        Me.gbSelectedEmployee.CollapsedNormalImage = Nothing
        Me.gbSelectedEmployee.CollapsedPressedImage = Nothing
        Me.gbSelectedEmployee.CollapseOnLoad = False
        Me.gbSelectedEmployee.Controls.Add(Me.lblSelectedCount)
        Me.gbSelectedEmployee.Controls.Add(Me.txtSearchedSelectedEmployee)
        Me.gbSelectedEmployee.Controls.Add(Me.pnlSelectedEmployee)
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterJob)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterjob)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine3)
        Me.gbSelectedEmployee.Controls.Add(Me.cboFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterSection)
        Me.gbSelectedEmployee.Controls.Add(Me.cboeFilterDept)
        Me.gbSelectedEmployee.Controls.Add(Me.lblFilterDepartment)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnReset)
        Me.gbSelectedEmployee.Controls.Add(Me.objbtnSearch)
        Me.gbSelectedEmployee.Controls.Add(Me.btnDelete)
        Me.gbSelectedEmployee.Controls.Add(Me.objelLine2)
        Me.gbSelectedEmployee.ExpandedHoverImage = Nothing
        Me.gbSelectedEmployee.ExpandedNormalImage = Nothing
        Me.gbSelectedEmployee.ExpandedPressedImage = Nothing
        Me.gbSelectedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSelectedEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSelectedEmployee.HeaderHeight = 25
        Me.gbSelectedEmployee.HeaderMessage = ""
        Me.gbSelectedEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSelectedEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSelectedEmployee.HeightOnCollapse = 0
        Me.gbSelectedEmployee.LeftTextSpace = 0
        Me.gbSelectedEmployee.Location = New System.Drawing.Point(352, 33)
        Me.gbSelectedEmployee.Name = "gbSelectedEmployee"
        Me.gbSelectedEmployee.OpenHeight = 300
        Me.gbSelectedEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSelectedEmployee.ShowBorder = True
        Me.gbSelectedEmployee.ShowCheckBox = False
        Me.gbSelectedEmployee.ShowCollapseButton = False
        Me.gbSelectedEmployee.ShowDefaultBorderColor = True
        Me.gbSelectedEmployee.ShowDownButton = False
        Me.gbSelectedEmployee.ShowHeader = True
        Me.gbSelectedEmployee.Size = New System.Drawing.Size(561, 439)
        Me.gbSelectedEmployee.TabIndex = 1
        Me.gbSelectedEmployee.Temp = 0
        Me.gbSelectedEmployee.Text = "Selected Employee"
        Me.gbSelectedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSelectedCount
        '
        Me.lblSelectedCount.Location = New System.Drawing.Point(120, 7)
        Me.lblSelectedCount.Name = "lblSelectedCount"
        Me.lblSelectedCount.Size = New System.Drawing.Size(59, 13)
        Me.lblSelectedCount.TabIndex = 220
        Me.lblSelectedCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtSearchedSelectedEmployee
        '
        Me.txtSearchedSelectedEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchedSelectedEmployee.Flags = 0
        Me.txtSearchedSelectedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchedSelectedEmployee.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchedSelectedEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchedSelectedEmployee.Location = New System.Drawing.Point(3, 404)
        Me.txtSearchedSelectedEmployee.Name = "txtSearchedSelectedEmployee"
        Me.txtSearchedSelectedEmployee.Size = New System.Drawing.Size(455, 21)
        Me.txtSearchedSelectedEmployee.TabIndex = 219
        '
        'pnlSelectedEmployee
        '
        Me.pnlSelectedEmployee.AutoSize = True
        Me.pnlSelectedEmployee.Controls.Add(Me.chkAllSelectedEmployee)
        Me.pnlSelectedEmployee.Controls.Add(Me.dgvSelectedEmployee)
        Me.pnlSelectedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSelectedEmployee.Location = New System.Drawing.Point(2, 26)
        Me.pnlSelectedEmployee.Name = "pnlSelectedEmployee"
        Me.pnlSelectedEmployee.Size = New System.Drawing.Size(562, 357)
        Me.pnlSelectedEmployee.TabIndex = 2
        '
        'chkAllSelectedEmployee
        '
        Me.chkAllSelectedEmployee.Location = New System.Drawing.Point(8, 5)
        Me.chkAllSelectedEmployee.Name = "chkAllSelectedEmployee"
        Me.chkAllSelectedEmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkAllSelectedEmployee.TabIndex = 17
        Me.chkAllSelectedEmployee.UseVisualStyleBackColor = True
        '
        'dgvSelectedEmployee
        '
        Me.dgvSelectedEmployee.AllowUserToAddRows = False
        Me.dgvSelectedEmployee.AllowUserToDeleteRows = False
        Me.dgvSelectedEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgvSelectedEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSelectedEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhSelectedIsCheck, Me.objcolhSelectedEmpId, Me.colhSelectedEmpCode, Me.colhSelectedEmpName, Me.objcolhSelectedDeptId, Me.colhSelectedDept, Me.objcolhSelectedSectionId, Me.colhSelectedSection, Me.objcolhSelectedJobId, Me.colhSelectedJob})
        Me.dgvSelectedEmployee.Location = New System.Drawing.Point(-2, 0)
        Me.dgvSelectedEmployee.Name = "dgvSelectedEmployee"
        Me.dgvSelectedEmployee.RowHeadersVisible = False
        Me.dgvSelectedEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSelectedEmployee.Size = New System.Drawing.Size(561, 354)
        Me.dgvSelectedEmployee.TabIndex = 242
        '
        'objcolhSelectedIsCheck
        '
        Me.objcolhSelectedIsCheck.HeaderText = ""
        Me.objcolhSelectedIsCheck.Name = "objcolhSelectedIsCheck"
        Me.objcolhSelectedIsCheck.Width = 30
        '
        'cboFilterJob
        '
        Me.cboFilterJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterJob.DropDownWidth = 300
        Me.cboFilterJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterJob.FormattingEnabled = True
        Me.cboFilterJob.Location = New System.Drawing.Point(432, 33)
        Me.cboFilterJob.Name = "cboFilterJob"
        Me.cboFilterJob.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterJob.TabIndex = 108
        Me.cboFilterJob.Visible = False
        '
        'lblFilterjob
        '
        Me.lblFilterjob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterjob.Location = New System.Drawing.Point(381, 36)
        Me.lblFilterjob.Name = "lblFilterjob"
        Me.lblFilterjob.Size = New System.Drawing.Size(48, 15)
        Me.lblFilterjob.TabIndex = 107
        Me.lblFilterjob.Text = "Job"
        Me.lblFilterjob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterjob.Visible = False
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(11, 58)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(542, 11)
        Me.objelLine3.TabIndex = 105
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFilterSection
        '
        Me.cboFilterSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilterSection.DropDownWidth = 300
        Me.cboFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilterSection.FormattingEnabled = True
        Me.cboFilterSection.Location = New System.Drawing.Point(266, 33)
        Me.cboFilterSection.Name = "cboFilterSection"
        Me.cboFilterSection.Size = New System.Drawing.Size(104, 21)
        Me.cboFilterSection.TabIndex = 104
        Me.cboFilterSection.Visible = False
        '
        'lblFilterSection
        '
        Me.lblFilterSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterSection.Location = New System.Drawing.Point(202, 36)
        Me.lblFilterSection.Name = "lblFilterSection"
        Me.lblFilterSection.Size = New System.Drawing.Size(58, 15)
        Me.lblFilterSection.TabIndex = 103
        Me.lblFilterSection.Text = "Section"
        Me.lblFilterSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterSection.Visible = False
        '
        'cboeFilterDept
        '
        Me.cboeFilterDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboeFilterDept.DropDownWidth = 300
        Me.cboeFilterDept.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboeFilterDept.FormattingEnabled = True
        Me.cboeFilterDept.Location = New System.Drawing.Point(89, 33)
        Me.cboeFilterDept.Name = "cboeFilterDept"
        Me.cboeFilterDept.Size = New System.Drawing.Size(104, 21)
        Me.cboeFilterDept.TabIndex = 102
        Me.cboeFilterDept.Visible = False
        '
        'lblFilterDepartment
        '
        Me.lblFilterDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilterDepartment.Location = New System.Drawing.Point(8, 36)
        Me.lblFilterDepartment.Name = "lblFilterDepartment"
        Me.lblFilterDepartment.Size = New System.Drawing.Size(75, 15)
        Me.lblFilterDepartment.TabIndex = 101
        Me.lblFilterDepartment.Text = "Department"
        Me.lblFilterDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblFilterDepartment.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(534, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 71
        Me.objbtnReset.TabStop = False
        Me.objbtnReset.Visible = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(510, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 70
        Me.objbtnSearch.TabStop = False
        Me.objbtnSearch.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(464, 400)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(89, 29)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(6, 391)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(547, 6)
        Me.objelLine2.TabIndex = 11
        '
        'gbEmployee
        '
        Me.gbEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbEmployee.Checked = False
        Me.gbEmployee.CollapseAllExceptThis = False
        Me.gbEmployee.CollapsedHoverImage = Nothing
        Me.gbEmployee.CollapsedNormalImage = Nothing
        Me.gbEmployee.CollapsedPressedImage = Nothing
        Me.gbEmployee.CollapseOnLoad = False
        Me.gbEmployee.Controls.Add(Me.lblCount)
        Me.gbEmployee.Controls.Add(Me.chkEmployeeAll)
        Me.gbEmployee.Controls.Add(Me.dgvEmployee)
        Me.gbEmployee.Controls.Add(Me.lnkAllocation)
        Me.gbEmployee.Controls.Add(Me.txtSearchedEmployee)
        Me.gbEmployee.Controls.Add(Me.objAlloacationReset)
        Me.gbEmployee.Controls.Add(Me.objelLine1)
        Me.gbEmployee.Controls.Add(Me.btnAdd)
        Me.gbEmployee.ExpandedHoverImage = Nothing
        Me.gbEmployee.ExpandedNormalImage = Nothing
        Me.gbEmployee.ExpandedPressedImage = Nothing
        Me.gbEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployee.HeaderHeight = 25
        Me.gbEmployee.HeaderMessage = ""
        Me.gbEmployee.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployee.HeightOnCollapse = 0
        Me.gbEmployee.LeftTextSpace = 0
        Me.gbEmployee.Location = New System.Drawing.Point(11, 33)
        Me.gbEmployee.Name = "gbEmployee"
        Me.gbEmployee.OpenHeight = 300
        Me.gbEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployee.ShowBorder = True
        Me.gbEmployee.ShowCheckBox = False
        Me.gbEmployee.ShowCollapseButton = False
        Me.gbEmployee.ShowDefaultBorderColor = True
        Me.gbEmployee.ShowDownButton = False
        Me.gbEmployee.ShowHeader = True
        Me.gbEmployee.Size = New System.Drawing.Size(335, 439)
        Me.gbEmployee.TabIndex = 0
        Me.gbEmployee.Temp = 0
        Me.gbEmployee.Text = "Employee"
        Me.gbEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCount
        '
        Me.lblCount.Location = New System.Drawing.Point(73, 7)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(59, 13)
        Me.lblCount.TabIndex = 2
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'chkEmployeeAll
        '
        Me.chkEmployeeAll.AutoSize = True
        Me.chkEmployeeAll.BackColor = System.Drawing.Color.Transparent
        Me.chkEmployeeAll.Location = New System.Drawing.Point(17, 31)
        Me.chkEmployeeAll.Name = "chkEmployeeAll"
        Me.chkEmployeeAll.Size = New System.Drawing.Size(15, 14)
        Me.chkEmployeeAll.TabIndex = 16
        Me.chkEmployeeAll.UseVisualStyleBackColor = False
        '
        'dgvEmployee
        '
        Me.dgvEmployee.AllowUserToAddRows = False
        Me.dgvEmployee.AllowUserToDeleteRows = False
        Me.dgvEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhIsCheck, Me.objcolhEmpId, Me.colhEmpCode, Me.colhEmpName, Me.objcolhDeptId, Me.objcolhDept, Me.objcolhSectionId, Me.objcolhSection, Me.objcolhJobId, Me.objcolhJob})
        Me.dgvEmployee.Location = New System.Drawing.Point(9, 26)
        Me.dgvEmployee.Name = "dgvEmployee"
        Me.dgvEmployee.RowHeadersVisible = False
        Me.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmployee.Size = New System.Drawing.Size(326, 354)
        Me.dgvEmployee.TabIndex = 241
        '
        'objcolhIsCheck
        '
        Me.objcolhIsCheck.HeaderText = ""
        Me.objcolhIsCheck.Name = "objcolhIsCheck"
        Me.objcolhIsCheck.Width = 30
        '
        'lnkAllocation
        '
        Me.lnkAllocation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(213, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 15)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtSearchedEmployee
        '
        Me.txtSearchedEmployee.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchedEmployee.Flags = 0
        Me.txtSearchedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchedEmployee.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchedEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSearchedEmployee.Location = New System.Drawing.Point(9, 404)
        Me.txtSearchedEmployee.Name = "txtSearchedEmployee"
        Me.txtSearchedEmployee.Size = New System.Drawing.Size(217, 21)
        Me.txtSearchedEmployee.TabIndex = 218
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(311, 2)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 237
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(3, 391)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(315, 6)
        Me.objelLine1.TabIndex = 10
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(232, 400)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(89, 29)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lblApprovalTo
        '
        Me.lblApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovalTo.Location = New System.Drawing.Point(8, 206)
        Me.lblApprovalTo.Name = "lblApprovalTo"
        Me.lblApprovalTo.Size = New System.Drawing.Size(67, 15)
        Me.lblApprovalTo.TabIndex = 98
        Me.lblApprovalTo.Text = "Approval To"
        Me.lblApprovalTo.Visible = False
        '
        'cboApprovalTo
        '
        Me.cboApprovalTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovalTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovalTo.FormattingEnabled = True
        Me.cboApprovalTo.Location = New System.Drawing.Point(81, 203)
        Me.cboApprovalTo.Name = "cboApprovalTo"
        Me.cboApprovalTo.Size = New System.Drawing.Size(165, 21)
        Me.cboApprovalTo.TabIndex = 6
        Me.cboApprovalTo.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 478)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(927, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(715, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(818, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(20, 123)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(67, 15)
        Me.lblJob.TabIndex = 223
        Me.lblJob.Text = "Job"
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(20, 69)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(67, 15)
        Me.lblDepartment.TabIndex = 219
        Me.lblDepartment.Text = "Department"
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(20, 96)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(67, 15)
        Me.lblSection.TabIndex = 221
        Me.lblSection.Text = "Section"
        '
        'objcolhSelectedEmpId
        '
        Me.objcolhSelectedEmpId.HeaderText = "Emp Id"
        Me.objcolhSelectedEmpId.Name = "objcolhSelectedEmpId"
        Me.objcolhSelectedEmpId.Visible = False
        '
        'colhSelectedEmpCode
        '
        Me.colhSelectedEmpCode.HeaderText = "Emp. Code"
        Me.colhSelectedEmpCode.Name = "colhSelectedEmpCode"
        '
        'colhSelectedEmpName
        '
        Me.colhSelectedEmpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhSelectedEmpName.HeaderText = "Employee Name"
        Me.colhSelectedEmpName.Name = "colhSelectedEmpName"
        '
        'objcolhSelectedDeptId
        '
        Me.objcolhSelectedDeptId.HeaderText = "Dept"
        Me.objcolhSelectedDeptId.Name = "objcolhSelectedDeptId"
        Me.objcolhSelectedDeptId.Visible = False
        '
        'colhSelectedDept
        '
        Me.colhSelectedDept.HeaderText = "Department"
        Me.colhSelectedDept.Name = "colhSelectedDept"
        '
        'objcolhSelectedSectionId
        '
        Me.objcolhSelectedSectionId.HeaderText = "Section"
        Me.objcolhSelectedSectionId.Name = "objcolhSelectedSectionId"
        Me.objcolhSelectedSectionId.Visible = False
        '
        'colhSelectedSection
        '
        Me.colhSelectedSection.HeaderText = "Section"
        Me.colhSelectedSection.Name = "colhSelectedSection"
        '
        'objcolhSelectedJobId
        '
        Me.objcolhSelectedJobId.HeaderText = "Job"
        Me.objcolhSelectedJobId.Name = "objcolhSelectedJobId"
        Me.objcolhSelectedJobId.Visible = False
        '
        'colhSelectedJob
        '
        Me.colhSelectedJob.HeaderText = "Job"
        Me.colhSelectedJob.Name = "colhSelectedJob"
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.HeaderText = "Emp Id"
        Me.objcolhEmpId.Name = "objcolhEmpId"
        Me.objcolhEmpId.Visible = False
        '
        'colhEmpCode
        '
        Me.colhEmpCode.HeaderText = "Emp. Code"
        Me.colhEmpCode.Name = "colhEmpCode"
        '
        'colhEmpName
        '
        Me.colhEmpName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmpName.HeaderText = "Employee Name"
        Me.colhEmpName.Name = "colhEmpName"
        '
        'objcolhDeptId
        '
        Me.objcolhDeptId.HeaderText = "Dept"
        Me.objcolhDeptId.Name = "objcolhDeptId"
        Me.objcolhDeptId.Visible = False
        '
        'objcolhDept
        '
        Me.objcolhDept.HeaderText = "Department"
        Me.objcolhDept.Name = "objcolhDept"
        Me.objcolhDept.Visible = False
        '
        'objcolhSectionId
        '
        Me.objcolhSectionId.HeaderText = "Section"
        Me.objcolhSectionId.Name = "objcolhSectionId"
        Me.objcolhSectionId.Visible = False
        '
        'objcolhSection
        '
        Me.objcolhSection.HeaderText = "Section"
        Me.objcolhSection.Name = "objcolhSection"
        Me.objcolhSection.Visible = False
        '
        'objcolhJobId
        '
        Me.objcolhJobId.HeaderText = "Job"
        Me.objcolhJobId.Name = "objcolhJobId"
        Me.objcolhJobId.Visible = False
        '
        'objcolhJob
        '
        Me.objcolhJob.HeaderText = "Job"
        Me.objcolhJob.Name = "objcolhJob"
        Me.objcolhJob.Visible = False
        '
        'frmEmpSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(927, 533)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpSelection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Employee Selection"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbApproversInfo.ResumeLayout(False)
        Me.gbSelectedEmployee.ResumeLayout(False)
        Me.gbSelectedEmployee.PerformLayout()
        Me.pnlSelectedEmployee.ResumeLayout(False)
        CType(Me.dgvSelectedEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployee.ResumeLayout(False)
        Me.gbEmployee.PerformLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbApproversInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbSelectedEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlSelectedEmployee As System.Windows.Forms.Panel
    Friend WithEvents chkAllSelectedEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents cboFilterJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterjob As System.Windows.Forms.Label
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents cboFilterSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterSection As System.Windows.Forms.Label
    Friend WithEvents cboeFilterDept As System.Windows.Forms.ComboBox
    Friend WithEvents lblFilterDepartment As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents gbEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents txtSearchedEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents chkEmployeeAll As System.Windows.Forms.CheckBox
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lblApprovalTo As System.Windows.Forms.Label
    Friend WithEvents cboApprovalTo As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents dgvEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSelectedEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchedSelectedEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents lblSelectedCount As System.Windows.Forms.Label
    Friend WithEvents objcolhIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDeptId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSectionId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSelectedIsCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhSelectedEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSelectedEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSelectedEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSelectedDeptId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSelectedDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSelectedSectionId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSelectedSection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhSelectedJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSelectedJob As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

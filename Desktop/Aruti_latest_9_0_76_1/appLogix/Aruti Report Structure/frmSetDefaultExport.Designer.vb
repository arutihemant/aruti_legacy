<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetDefaultExport
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetDefaultExport))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.rabPDF = New System.Windows.Forms.RadioButton
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.rabHTML = New System.Windows.Forms.RadioButton
        Me.rabExcel = New System.Windows.Forms.RadioButton
        Me.rabRichText = New System.Windows.Forms.RadioButton
        Me.rabWord = New System.Windows.Forms.RadioButton
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.SystemColors.Control
        Me.pnlMain.Controls.Add(Me.rabPDF)
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.rabHTML)
        Me.pnlMain.Controls.Add(Me.rabExcel)
        Me.pnlMain.Controls.Add(Me.rabRichText)
        Me.pnlMain.Controls.Add(Me.rabWord)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(471, 216)
        Me.pnlMain.TabIndex = 0
        '
        'rabPDF
        '
        Me.rabPDF.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabPDF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabPDF.Image = Global.Aruti.Data.My.Resources.Resources.PDF_48
        Me.rabPDF.Location = New System.Drawing.Point(367, 25)
        Me.rabPDF.Name = "rabPDF"
        Me.rabPDF.Size = New System.Drawing.Size(80, 110)
        Me.rabPDF.TabIndex = 4
        Me.rabPDF.Tag = ""
        Me.rabPDF.Text = "&PDF"
        Me.rabPDF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabPDF.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabPDF.UseVisualStyleBackColor = True
        '
        'objefFooter
        '
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnOK)
        Me.objefFooter.Controls.Add(Me.btnCancel)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.Color.Empty
        Me.objefFooter.GradientColor2 = System.Drawing.Color.Empty
        Me.objefFooter.Location = New System.Drawing.Point(0, 161)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(471, 55)
        Me.objefFooter.TabIndex = 5
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(275, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(90, 30)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "&Set"
        Me.btnOK.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(371, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'rabHTML
        '
        Me.rabHTML.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabHTML.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabHTML.Image = Global.Aruti.Data.My.Resources.Resources.HTML_48
        Me.rabHTML.Location = New System.Drawing.Point(281, 25)
        Me.rabHTML.Name = "rabHTML"
        Me.rabHTML.Size = New System.Drawing.Size(80, 110)
        Me.rabHTML.TabIndex = 3
        Me.rabHTML.Text = "&HTML"
        Me.rabHTML.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabHTML.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabHTML.UseVisualStyleBackColor = True
        '
        'rabExcel
        '
        Me.rabExcel.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabExcel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabExcel.Image = Global.Aruti.Data.My.Resources.Resources.Excel_48
        Me.rabExcel.Location = New System.Drawing.Point(195, 25)
        Me.rabExcel.Name = "rabExcel"
        Me.rabExcel.Size = New System.Drawing.Size(80, 110)
        Me.rabExcel.TabIndex = 2
        Me.rabExcel.Tag = "rabPDF"
        Me.rabExcel.Text = "MS &Excel"
        Me.rabExcel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabExcel.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabExcel.UseVisualStyleBackColor = True
        '
        'rabRichText
        '
        Me.rabRichText.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabRichText.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabRichText.Image = Global.Aruti.Data.My.Resources.Resources.RichText1_48
        Me.rabRichText.Location = New System.Drawing.Point(23, 25)
        Me.rabRichText.Name = "rabRichText"
        Me.rabRichText.Size = New System.Drawing.Size(80, 110)
        Me.rabRichText.TabIndex = 0
        Me.rabRichText.Text = "&Rich Text"
        Me.rabRichText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabRichText.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabRichText.UseVisualStyleBackColor = True
        '
        'rabWord
        '
        Me.rabWord.Appearance = System.Windows.Forms.Appearance.Button
        Me.rabWord.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rabWord.Image = Global.Aruti.Data.My.Resources.Resources.Word_48
        Me.rabWord.Location = New System.Drawing.Point(109, 25)
        Me.rabWord.Name = "rabWord"
        Me.rabWord.Size = New System.Drawing.Size(80, 110)
        Me.rabWord.TabIndex = 1
        Me.rabWord.Text = "MS &Word"
        Me.rabWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rabWord.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.rabWord.UseVisualStyleBackColor = True
        '
        'frmSetDefaultExport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 216)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSetDefaultExport"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Set Export Default"
        Me.pnlMain.ResumeLayout(False)
        Me.objefFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents rabRichText As System.Windows.Forms.RadioButton
    Friend WithEvents rabHTML As System.Windows.Forms.RadioButton
    Friend WithEvents rabExcel As System.Windows.Forms.RadioButton
    Friend WithEvents rabWord As System.Windows.Forms.RadioButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents rabPDF As System.Windows.Forms.RadioButton
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
End Class

Imports eZeeCommonLib

Public Class ReportFunction
    Private Shared ReadOnly mstrModuleName As String = "modCommon"


#Region "Report Enum"
    Public Enum enPrintAction
        None = 0
        Preview = 1
        Print = 2
    End Enum

    Public Enum enExportAction
        None = 0
        RichText = 2
        Word = 3
        Excel = 4
        PDF = 5
        HTML = 6
    End Enum
#End Region

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mblnIsDisplayLogo As Boolean = False
    Private mblnIsShowPreparedBy As Boolean = False
    Private mblnIsShowApprovedBy As Boolean = False
    Private mblnIsShowCheckedBy As Boolean = False
    Private mblnIsShowReceivedBy As Boolean = False
    Private mdblLeftMargin As Double = 0.25
    Private mdblRightMargin As Double = 0.25
    Private mdblPageMargin As Double = 0.0
    Private mblnShowLogoRightSide As Boolean = False
    Private mblnShowPropertyInfo As Boolean = False

    Public ReadOnly Property _DisplayLogo() As Boolean
        Get
            Return mblnIsDisplayLogo
        End Get
    End Property

    Public ReadOnly Property _IsShowPreparedBy() As Boolean
        Get
            Return mblnIsShowPreparedBy
        End Get
    End Property

    Public ReadOnly Property _IsShowApprovedBy() As Boolean
        Get
            Return mblnIsShowApprovedBy
        End Get
    End Property

    Public ReadOnly Property _IsShowCheckedBy() As Boolean
        Get
            Return mblnIsShowCheckedBy
        End Get
    End Property

    Public ReadOnly Property _IsShowReceivedBy() As Boolean
        Get
            Return mblnIsShowReceivedBy
        End Get
    End Property

    Public ReadOnly Property _LeftMargin() As Double
        Get
            Return mdblLeftMargin
        End Get
    End Property

    Public ReadOnly Property _RightMargin() As Double
        Get
            Return mdblRightMargin
        End Get
    End Property

    Public ReadOnly Property _PageMargin() As Double
        Get
            Return mdblPageMargin
        End Get
    End Property

    Public ReadOnly Property _ShowLogoRightSide() As Boolean
        Get
            Return mblnShowLogoRightSide
        End Get
    End Property

    Public ReadOnly Property _ShowPropertyInfo() As Boolean
        Get
            Return mblnShowPropertyInfo
        End Get
    End Property

    Public Sub GetReportSetting(ByVal intReportId As Integer, ByVal intCompanyId As Integer)
        Dim StrQ As String = String.Empty
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     key_name AS iKey " & _
                       "    ,key_value AS iKeyValue " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE UPPER(key_name) IN ('DISPLAYLOGO','PREPAREDBY','APPROVEDBY','CHECKEDBY','RECEIVEDBY','SHOWLOGORIGHTSIDE','LEFTMARGIN','RIGHTMARGIN','PAGEMARGIN','SHOWPROPERTYINFO') " & _
                       "    AND companyunkid = '" & intCompanyId & "' " & _
                       "ORDER BY key_name "

                Dim dsKeyList As New DataSet : dsKeyList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                For Each row As DataRow In dsKeyList.Tables(0).Rows
                    Select Case row("iKey").ToString.ToUpper()

                        Case "DISPLAYLOGO"

                            StrQ = "DECLARE @words VARCHAR (MAX)  " & _
                                   " SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "    SELECT " & _
                                   "        @end   = CHARINDEX(',',@words,@start) " & _
                                   "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "      , @start = @end + 1 " & _
                                   "    INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word FROM @split WHERE CAST([@split].word AS BIGINT) = '" & intReportId & "' ORDER BY word"
                            Dim iCnt As Integer = 0
                            iCnt = objDo.RecordCount(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If iCnt > 0 Then
                                mblnIsDisplayLogo = True
                            End If

                        Case "PREPAREDBY"

                            StrQ = "DECLARE @words VARCHAR (MAX)  " & _
                                   " SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "    SELECT " & _
                                   "        @end   = CHARINDEX(',',@words,@start) " & _
                                   "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "      , @start = @end + 1 " & _
                                   "    INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word FROM @split WHERE CAST([@split].word AS BIGINT) = '" & intReportId & "' ORDER BY word"
                            Dim iCnt As Integer = 0
                            iCnt = objDo.RecordCount(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If iCnt > 0 Then
                                mblnIsShowPreparedBy = True
                            End If

                        Case "APPROVEDBY"

                            StrQ = "DECLARE @words VARCHAR (MAX)  " & _
                                   " SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "    SELECT " & _
                                   "        @end   = CHARINDEX(',',@words,@start) " & _
                                   "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "      , @start = @end + 1 " & _
                                   "    INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word FROM @split WHERE CAST([@split].word AS BIGINT) = '" & intReportId & "' ORDER BY word"
                            Dim iCnt As Integer = 0
                            iCnt = objDo.RecordCount(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If iCnt > 0 Then
                                mblnIsShowApprovedBy = True
                            End If

                        Case "CHECKEDBY"

                            StrQ = "DECLARE @words VARCHAR (MAX)  " & _
                                   " SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "    SELECT " & _
                                   "        @end   = CHARINDEX(',',@words,@start) " & _
                                   "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "      , @start = @end + 1 " & _
                                   "    INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word FROM @split WHERE CAST([@split].word AS BIGINT) = '" & intReportId & "' ORDER BY word"
                            Dim iCnt As Integer = 0
                            iCnt = objDo.RecordCount(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If iCnt > 0 Then
                                mblnIsShowCheckedBy = True
                            End If

                        Case "RECEIVEDBY"

                            StrQ = "DECLARE @words VARCHAR (MAX)  " & _
                                   " SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "    SELECT " & _
                                   "        @end   = CHARINDEX(',',@words,@start) " & _
                                   "      , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "      , @start = @end + 1 " & _
                                   "    INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word FROM @split WHERE CAST([@split].word AS BIGINT) = '" & intReportId & "' ORDER BY word"
                            Dim iCnt As Integer = 0
                            iCnt = objDo.RecordCount(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            If iCnt > 0 Then
                                mblnIsShowReceivedBy = True
                            End If

                        Case "SHOWLOGORIGHTSIDE"

                            mblnShowLogoRightSide = CBool(row("iKeyValue"))

                        Case "LEFTMARGIN"

                            StrQ = "DECLARE @words VARCHAR (MAX) " & _
                                   "SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "SELECT " & _
                                   "      @end   = CHARINDEX(',',@words,@start) " & _
                                   "    , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "    , @start = @end+1 " & _
                                   "INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT @lmargin = SUBSTRING(word, CHARINDEX('|', word) + 1,LEN(word)) " & _
                                   "FROM @split WHERE CAST(SUBSTRING(word, 1, CHARINDEX('|', word)-1) AS BIGINT) = '" & intReportId & "' " & _
                                   "ORDER BY word "

                            '"SELECT word,SUBSTRING(word, 1, CHARINDEX('|', word)-1) AS rid ,SUBSTRING(word, CHARINDEX('|', word) + 1,LEN(word)) as rvalue " & _

                            objDo.AddParameter("@lmargin", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblLeftMargin, ParameterDirection.InputOutput)

                            objDo.ExecNonQuery(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            mdblLeftMargin = objDo.GetParameterValue("@lmargin")

                            If mdblLeftMargin <= 0 Then mdblLeftMargin = 0.25

                        Case "RIGHTMARGIN"

                            StrQ = "DECLARE @words VARCHAR (MAX) " & _
                                   "SET @words = '" & row("iKeyValue").ToString & "' " & _
                                   "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                                   "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                                   "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                                   "WHILE @start < @stop begin " & _
                                   "SELECT " & _
                                   "      @end   = CHARINDEX(',',@words,@start) " & _
                                   "    , @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                   "    , @start = @end+1 " & _
                                   "INSERT @split VALUES (@word) " & _
                                   "END " & _
                                   "SELECT word,SUBSTRING(word, 1, CHARINDEX('|', word)-1) AS rid ,SUBSTRING(word, CHARINDEX('|', word) + 1,LEN(word)) AS rvalue " & _
                                   "FROM @split WHERE CAST(SUBSTRING(word, 1, CHARINDEX('|', word)-1) AS BIGINT) = '" & intReportId & "' " & _
                                   "ORDER BY word "

                            objDo.AddParameter("@rmargin", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRightMargin, ParameterDirection.InputOutput)

                            objDo.ExecNonQuery(StrQ)

                            If objDo.ErrorMessage <> "" Then
                                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

                            mdblRightMargin = objDo.GetParameterValue("@lmargin")

                            If mdblRightMargin <= 0 Then mdblRightMargin = 0.25


                        Case "PAGEMARGIN"

                            mdblPageMargin = CDbl(row("iKeyValue"))

                        Case "SHOWPROPERTYINFO"

                            mblnShowLogoRightSide = CBool(row("iKeyValue"))

                    End Select
                Next


            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReportSetting; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    ''' <summary>
    ''' Set a Value of given CrystalReports Text Field (Object).
    ''' </summary>
    ''' <param name="rpt">A Object of CrystalDecisions.CrystalReports.Engine.ReportClass 
    ''' or CrystalDecisions.CrystalReports.Engine.ReportDocument</param>
    ''' <param name="strObjectName">A Text Field (Object) Name.</param>
    ''' <param name="strValue">A Value of Text Field.</param>
    ''' <remarks></remarks>
    ''' 
    Public Shared Sub TextChange(ByVal rpt As Object, _
                                ByVal strObjectName As String, _
                                ByVal strValue As String)

        Dim txt As CrystalDecisions.CrystalReports.Engine.TextObject = Nothing
        Try
            txt = DirectCast(rpt.ReportDefinition.ReportObjects(strObjectName), CrystalDecisions.CrystalReports.Engine.TextObject)
            txt.Text = strValue

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "TextChange", mstrModuleName)
        Finally
            If txt IsNot Nothing Then
                txt.Dispose()
                txt = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Set a Decimal point to CrystalReports Formula Field from database setting.
    ''' </summary>
    ''' <param name="rpt">A Object of CrystalDecisions.CrystalReports.Engine.ReportClass 
    ''' or CrystalDecisions.CrystalReports.Engine.ReportDocument</param>
    ''' <param name="strObjectName">A Formula Field Name.</param>
    ''' <param name="intDecimalType">0= Currency, 1= Store Qty, 3=Outlet Qty</param>
    ''' <remarks></remarks>
    Public Shared Sub SetRptDecimal(ByVal rpt As Object, _
                                ByVal strObjectName As String, _
                                Optional ByVal intDecimalType As Integer = 0)

        Dim objFld As CrystalDecisions.CrystalReports.Engine.FieldObject = Nothing
        Try
            objFld = DirectCast(rpt.ReportDefinition.ReportObjects(strObjectName), CrystalDecisions.CrystalReports.Engine.FieldObject)

            objFld.ObjectFormat.EnableCanGrow = True


            'Sandeep [ 01 MARCH 2011 ] -- Start
            'Dim objExRate As New clsExchangeRate
            'objExRate._ExchangeRateunkid = 1
            'objFld.FieldFormat.NumericFormat.DecimalPlaces = objExRate._Digits_After_Decimal
            'objExRate = Nothing

            Dim objExRate As New clsExchangeRate
            objExRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objFld.FieldFormat.NumericFormat.DecimalPlaces = objExRate._Digits_After_Decimal
            objExRate = Nothing
            'Sandeep [ 01 MARCH 2011 ] -- End 

            'Select Case intDecimalType
            '    Case 1
            '        objFld.FieldFormat.NumericFormat.DecimalPlaces = Parameter._Object.POS_StoreQty_DecimalPoint
            '    Case 2
            '        objFld.FieldFormat.NumericFormat.DecimalPlaces = Parameter._Object.POS_OutletQty_DecimalPoint
            '    Case Else
            '        objFld.FieldFormat.NumericFormat.DecimalPlaces = Parameter._Object._NoofDigitAfterDecimalPoint
            'End Select

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetRptDecimal", mstrModuleName)
        Finally
            If objFld IsNot Nothing Then
                objFld.Dispose()
                objFld = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Set a EnableSuppress Value of given CrystalReports Object.
    ''' </summary>
    ''' <param name="rpt">A Object of CrystalDecisions.CrystalReports.Engine.ReportClass 
    ''' or CrystalDecisions.CrystalReports.Engine.ReportDocument</param>
    ''' <param name="strObjectName">A Object Name.</param>
    ''' <param name="strValue">A Value of EnableSuppress.</param>
    ''' <remarks></remarks>
    Public Shared Sub EnableSuppress(ByVal rpt As Object, _
                                ByVal strObjectName As String, _
                                ByVal strValue As Boolean)

        Try
            rpt.ReportDefinition.ReportObjects(strObjectName).ObjectFormat.EnableSuppress = strValue
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "EnableSuppress", mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Set a EnableSuppress Value of given CrystalReports Object.
    ''' </summary>
    ''' <param name="rpt">A Object of CrystalDecisions.CrystalReports.Engine.ReportClass 
    ''' or CrystalDecisions.CrystalReports.Engine.ReportDocument</param>
    ''' <param name="strSectionName">A Section Name.</param>
    ''' <param name="strValue">A Value of EnableSuppress.</param>
    ''' <remarks></remarks>
    Public Shared Sub EnableSuppressSection(ByVal rpt As Object, _
                                ByVal strSectionName As String, _
                                ByVal strValue As Boolean)

        Try
            rpt.ReportDefinition.Sections(strSectionName).SectionFormat.EnableSuppress = strValue
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "EnableSuppressSection", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (07 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Set a EnableUnderlaySection Value of given CrystalReports Object.
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="strSectionName"></param>
    ''' <param name="strValue"></param>
    ''' <remarks></remarks>
    Public Shared Sub EnableUnderlaySection(ByVal rpt As Object, _
                               ByVal strSectionName As String, _
                               ByVal strValue As Boolean)

        Try
            rpt.ReportDefinition.Sections(strSectionName).SectionFormat.EnableUnderlaySection = strValue

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "EnableUnderlaySection", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (07 Feb 2013) -- End

    'Sohail (25 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Modify by Sohail : Set a EnableNewPageBefore Value of given CrystalReports Object.
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="strSectionName"></param>
    ''' <param name="strValue"></param>
    ''' <remarks></remarks>
    Public Shared Sub EnableNewPageBefore(ByVal rpt As Object, _
                               ByVal strSectionName As String, _
                               ByVal strValue As Boolean)

        Try
            rpt.ReportDefinition.Sections(strSectionName).SectionFormat.EnableNewPageBefore = strValue

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "EnableNewPageBefore", mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify by Sohail : Set a EnableNewPageAfter Value of given CrystalReports Object.
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="strSectionName"></param>
    ''' <param name="strValue"></param>
    ''' <remarks></remarks>
    Public Shared Sub EnableNewPageAfter(ByVal rpt As Object, _
                               ByVal strSectionName As String, _
                               ByVal strValue As Boolean)

        Try
            rpt.ReportDefinition.Sections(strSectionName).SectionFormat.EnableNewPageAfter = strValue

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "EnableNewPageAfter", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (25 Feb 2013) -- End

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="ObjectName"></param>
    ''' <param name="intLeft"></param>
    ''' <param name="intWidth">-1 = Nothing</param>
    ''' <remarks></remarks>
    Public Shared Sub SetColumnAt(ByVal rpt As Object, _
                            ByVal ObjectName As String, _
                            ByVal intLeft As Integer, _
                            Optional ByVal intWidth As Integer = -1)

        rpt.ReportDefinition.ReportObjects(ObjectName).Left = intLeft
        Select Case intWidth
            Case -1
            Case Else
                rpt.ReportDefinition.ReportObjects(ObjectName).Width = intWidth
        End Select

    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="ObjectName"></param>
    ''' <param name="FromObjectName"></param>
    ''' <param name="ColSpace"></param>
    ''' <param name="intWidth">-1 = Nothing, -2 = Same Width as FromObject </param>
    ''' <remarks></remarks>
    Public Shared Sub SetColumnAfter(ByVal rpt As Object, _
                           ByVal ObjectName As String, _
                           ByVal FromObjectName As String, _
                           ByVal ColSpace As Integer, _
                           Optional ByVal intWidth As Integer = -1)

        rpt.ReportDefinition.ReportObjects(ObjectName).Left = _
            rpt.ReportDefinition.ReportObjects(FromObjectName).Left + _
                rpt.ReportDefinition.ReportObjects(FromObjectName).Width + ColSpace

        Select Case intWidth
            Case -1
                rpt.ReportDefinition.ReportObjects(ObjectName).Width = rpt.ReportDefinition.ReportObjects(FromObjectName).Width
            Case Else
                rpt.ReportDefinition.ReportObjects(ObjectName).Width = intWidth
        End Select
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="rpt"></param>
    ''' <param name="ObjectName"></param>
    ''' <param name="FromObjectName"></param>
    ''' <param name="intWidth">-1 = Nothing, -2 = Same Width as FromObject </param>
    ''' <remarks></remarks>
    Public Shared Sub SetColumnOn(ByVal rpt As Object, _
                          ByVal ObjectName As String, _
                          ByVal FromObjectName As String, _
                          Optional ByVal intWidth As Integer = -1)

        rpt.ReportDefinition.ReportObjects(ObjectName).Left = _
            rpt.ReportDefinition.ReportObjects(FromObjectName).Left

        Select Case intWidth
            Case -1
                rpt.ReportDefinition.ReportObjects(ObjectName).Width = rpt.ReportDefinition.ReportObjects(FromObjectName).Width
            Case Else
                rpt.ReportDefinition.ReportObjects(ObjectName).Width = intWidth
        End Select


    End Sub

    'Sandeep (13 Apr 2009) -- Start
    Public Shared Sub ReportExecute(ByVal objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass, _
                            ByVal PrintAction As enPrintAction, _
                            ByVal ExportAction As enExportAction, _
                            ByVal strExportPath As String, _
                            ByVal blnOpenExport As Boolean, _
                            ByVal strReportName As String, _
                            ByVal strParameter As CrystalDecisions.Shared.ParameterFields)

        Dim objReportViewer As ArutiReportViewer
        Dim strReportExportFileExtension As String = ""
        Dim strReportExportFile As String = ""
        Try

            If IsNothing(objRpt) Then Exit Sub
            If PrintAction = enPrintAction.None And ExportAction = enExportAction.None Then Exit Sub

            For i As Integer = 0 To strParameter.Count - 1
                objRpt.SetParameterValue(strParameter.Item(i).Name.ToString, strParameter.Item(i).CurrentValues)
            Next

            'Printing
            Select Case PrintAction
                Case enPrintAction.Preview

                    objReportViewer = New ArutiReportViewer

                    objReportViewer._Heading = strReportName
                    objReportViewer.crwArutiReportViewer.ParameterFieldInfo = strParameter
                    objReportViewer.crwArutiReportViewer.ReportSource = objRpt
                    objReportViewer.Show()

                    Exit Sub
                Case enPrintAction.Print
                    Dim prd As New System.Drawing.Printing.PrintDocument

                    objRpt.PrintOptions.PrinterName = prd.PrinterSettings.PrinterName
                    objRpt.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
                    objRpt.PrintToPrinter(1, False, 0, 0)

                    Exit Sub
            End Select


            'Exporting
            If System.IO.Directory.Exists(strExportPath) = False Then
                Dim dig As New Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            End If

            'Sandeep (27 July 2009) -- Start
            If strReportName.Contains("/") = True Then
                strReportName = strReportName.Replace("/", "_")
            End If

            If strReportName.Contains("\") Then
                strReportName = strReportName.Replace("\", "_")
            End If
            'Sandeep (27 July 2009) -- End

            strReportExportFile = strExportPath & "\" & _
                                    strReportName.Replace(" ", "_") & "_" & _
                                    Now.Date.ToString("yyyyMMdd")

            Select Case ExportAction
                Case enExportAction.Excel
                    strReportExportFileExtension = ".xls"
                Case enExportAction.HTML
                    strReportExportFileExtension = ".html"
                Case enExportAction.PDF
                    strReportExportFileExtension = ".pdf"
                Case enExportAction.RichText
                    strReportExportFileExtension = ".rtf"
                Case enExportAction.Word
                    strReportExportFileExtension = ".doc"
                Case Else
                    Exit Sub
            End Select

            Dim strName As String = strReportExportFile & strReportExportFileExtension
            objRpt.ExportToDisk(ExportAction, strName)


            If blnOpenExport Then
                Call Shell("explorer " & strReportExportFile & strReportExportFileExtension, vbMaximizedFocus)
            Else

            End If

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "ReportExecute", mstrModuleName)
        End Try
    End Sub
    'Sandeep (13 Apr 2009) -- End

    Public Shared Sub SetAligment(ByVal rpt As Object, _
                                ByVal strObjectName As String, _
                                ByVal Alignment As CrystalDecisions.Shared.Alignment)
        Try
            rpt.ReportDefinition.ReportObjects(strObjectName).ObjectFormat.HorizontalAlignment = Alignment
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetAligment", mstrModuleName)
        End Try
    End Sub

    Public Shared ReadOnly Property PrintDate() As String
        Get
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'Return DateTime.Now.ToString(GUI.fmtDate) & " " & DateTime.Now.ToString(GUI.fmtTimeDisplay)
            Return DateTime.Now.Date & " " & DateTime.Now.ToShortTimeString()
            'Pinkal (16-Apr-2016) -- End
        End Get
    End Property

    'Sandeep [ 10 FEB 2011 ] -- Start
    Public Shared Function CreateBlankImage(ByVal iWidth As Integer, ByVal iHeight As Integer, ByVal bCenter As Boolean, ByVal bWidthCenter As Boolean, ByVal bHeightCenter As Boolean) As Bitmap

        Dim bmpBitmap As Bitmap
        bmpBitmap = New Bitmap(iWidth, iHeight)

        Dim iBitmapWidth As Integer = bmpBitmap.Width - 1
        Dim iBitmapHeight As Integer = bmpBitmap.Height - 1

        For y As Integer = 0 To iBitmapWidth
            For x As Integer = 0 To iBitmapHeight
                bmpBitmap.SetPixel(x, y, Color.FromArgb(255, 255, 255, 255))
            Next
        Next
        Return bmpBitmap
    End Function
    'Sandeep [ 10 FEB 2011 ] -- End 


    'Sandeep [ 01 MARCH 2011 ] -- Start
    Public Shared Function Logo_Display(ByVal rpt As Object, _
                                        ByVal blnIsDisplayLogo As Boolean, _
                                        ByVal blnIsDisplayRightSide As Boolean, _
                                        ByVal StrLogo1 As String, _
                                        ByVal StrLogo2 As String, _
                                        ByRef dtRow As DataRow, _
                                        Optional ByVal StrCompanyName As String = "", _
                                        Optional ByVal StrReportName As String = "", _
                                        Optional ByVal StrFilterDescription As String = "", _
                                        Optional ByVal dblLeftMargin As Double = 0.25, _
                                        Optional ByVal dblRightMargin As Double = 0.25, _
                                        Optional ByVal dblTopMargin As Double = 0.0)

        Try
            Dim intWidth As Integer = 0
            If blnIsDisplayLogo = True Then
                If blnIsDisplayRightSide = True Then
                    rpt.ReportDefinition.ReportObjects(StrLogo1).ObjectFormat.EnableSuppress = True
                    intWidth = rpt.ReportDefinition.ReportObjects(StrLogo1).Width
                    If StrCompanyName.Length > 0 Then
                        rpt.ReportDefinition.ReportObjects(StrCompanyName).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                    End If
                    If StrReportName.Length > 0 Then
                        rpt.ReportDefinition.ReportObjects(StrReportName).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                    End If
                    If StrFilterDescription.Length > 0 Then
                        rpt.ReportDefinition.ReportObjects(StrFilterDescription).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                    End If
                Else
                    rpt.ReportDefinition.ReportObjects(StrLogo2).ObjectFormat.EnableSuppress = True
                    intWidth = rpt.ReportDefinition.ReportObjects(StrLogo2).Width
                End If

                Dim objImg(0) As Object
                If Company._Object._Image IsNot Nothing Then
                    objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
                Else
                    Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    objImg(0) = eZeeDataType.image2Data(imgBlank)
                End If
                dtRow.Item("arutiLogo") = objImg(0)

                If StrCompanyName.Length > 0 Then
                    If blnIsDisplayRightSide = False Then
                        rpt.ReportDefinition.ReportObjects(StrCompanyName).Width += (intWidth * 2) - 1000
                    Else
                        rpt.ReportDefinition.ReportObjects(StrCompanyName).Width += intWidth
                    End If
                End If

                If StrReportName.Length > 0 Then
                    If blnIsDisplayRightSide = False Then
                        rpt.ReportDefinition.ReportObjects(StrReportName).Width += (intWidth * 2) - 1000
                    Else
                        rpt.ReportDefinition.ReportObjects(StrReportName).Width += intWidth
                    End If
                End If

                If StrFilterDescription.Length > 0 Then
                    If blnIsDisplayRightSide = False Then
                        If rpt.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape Then
                            rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width += intWidth + 200
                        Else
                            rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width += intWidth
                        End If
                    Else
                        rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width += intWidth
                    End If
                End If
            Else
                rpt.ReportDefinition.ReportObjects(StrLogo1).ObjectFormat.EnableSuppress = True
                rpt.ReportDefinition.ReportObjects(StrLogo2).ObjectFormat.EnableSuppress = True
                Dim intWidth1 As Integer = rpt.ReportDefinition.ReportObjects(StrLogo1).Width

                If StrCompanyName.Length > 0 Then
                    rpt.ReportDefinition.ReportObjects(StrCompanyName).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                End If

                If StrReportName.Length > 0 Then
                    rpt.ReportDefinition.ReportObjects(StrReportName).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                End If

                If StrFilterDescription.Length > 0 Then
                    rpt.ReportDefinition.ReportObjects(StrFilterDescription).Left = rpt.ReportDefinition.ReportObjects(StrLogo1).Left
                End If

                Dim intWidth2 As Integer = rpt.ReportDefinition.ReportObjects(StrLogo2).Width

                If StrCompanyName.Length > 0 Then
                    'rpt.ReportDefinition.ReportObjects(StrCompanyName).Width = rpt.ReportDefinition.ReportObjects(StrCompanyName).Width + intWidth1 + intWidth2 + (intWidth2 / 2) - 350
                    rpt.ReportDefinition.ReportObjects(StrCompanyName).Width = rpt.ReportDefinition.ReportObjects(StrCompanyName).Width + intWidth1 + intWidth2
                End If

                If StrReportName.Length > 0 Then
                    'rpt.ReportDefinition.ReportObjects(StrReportName).Width = rpt.ReportDefinition.ReportObjects(StrReportName).Width + intWidth1 + intWidth2 + (intWidth2 / 2) - 350
                    rpt.ReportDefinition.ReportObjects(StrReportName).Width = rpt.ReportDefinition.ReportObjects(StrReportName).Width + intWidth1 + intWidth2
                End If

                If StrFilterDescription.Length > 0 Then
                    'rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width = rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width + intWidth1 + intWidth2 + (intWidth2 / 2) - 350
                    rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width = rpt.ReportDefinition.ReportObjects(StrFilterDescription).Width + intWidth1 + intWidth2
                End If

                Dim objImg(0) As Object
                Dim imgBlank As Image = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                objImg(0) = eZeeDataType.image2Data(imgBlank)

                If Company._Object._Image IsNot Nothing Then
                    objImg(0) = eZeeDataType.image2Data(Company._Object._Image)
                Else
                    objImg(0) = eZeeDataType.image2Data(imgBlank)
                End If
                dtRow.Item("arutiLogo") = objImg(0)
            End If

            Dim margins As CrystalDecisions.Shared.PageMargins

            margins.leftMargin = dblLeftMargin * 1440
            margins.rightMargin = dblRightMargin * 1440
            margins.topMargin = dblTopMargin * 1440
            rpt.PrintOptions.ApplyPageMargins(margins)


            Return dtRow

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Logo_Display", mstrModuleName)
            Return dtRow
        End Try
    End Function
    'Sandeep [ 01 MARCH 2011 ] -- End 



    'Sandeep [ 09 MARCH 2011 ] -- Start
    Public Shared Sub Open_ExportedFile(ByVal blnOpen As Boolean, ByVal filePath As String)
        Try
            If blnOpen Then
                Call Shell("explorer " & filePath, vbMaximizedFocus)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Open_ExportedFile", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 09 MARCH 2011 ] -- End 


End Class

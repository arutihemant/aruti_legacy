Imports eZeeCommonLib

Public Class frmLanguageUtility
    Inherits eZee.Common.eZeeForm

    Private mstrModuleName As String = "frmLanguageUtility"

    Private m_blnIsImport As Boolean = False
    Private mblnCancel As Boolean = True

#Region " Display Dialog "

    Public Function displayDialog(ByVal blnIsImport As Boolean) As Boolean
        Try
            m_blnIsImport = blnIsImport
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "displayDialog", mstrModuleName)
            Return True
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmLanguageUtility_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            If Not m_blnIsImport Then
                mstrModuleName = "frmExport"
                Me.Name = "frmExport"
                Me.Text = "Export Language"
                Me.eZeeHeader.Name = "eZeeHeaderExport"
                Me.eZeeHeader.Title = "Export Language"
                Me.eZeeHeader.Message = "User can Export language."
            Else
                mstrModuleName = "frmImoprt"
                Me.Name = "frmImoprt"
                Me.Text = "Import Language"
                Me.eZeeHeader.Name = "eZeeHeaderImport"
                Me.eZeeHeader.Title = "Import Language"
                Me.eZeeHeader.Message = "User can Import language from earlier created language pack."
            End If

            Call Language.setLanguage(Me.Name)
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            Call OtherSettings()

            If m_blnIsImport Then
                tabcLanguageUtility.TabPages.Remove(tabpExport)
                btnExport.Visible = False
            Else
                tabcLanguageUtility.TabPages.Remove(tabpImport)
                tabcLanguageUtility.TabPages.Remove(tabpImportLookUp)
                btnImport.Visible = False
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmLanguageUtility_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Try
            ofdFile.Filter = "Xml Files|*.xml|All Files (*.*)|*.*"
            If ofdFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtImportFileName.Text = ofdFile.FileName
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnLookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLookup.Click
        Try
            ofdFile.Filter = "Xml Files|*.xml|All Files (*.*)|*.*"
            If ofdFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtLookupFile.Text = ofdFile.FileName
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnLookup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOpenDirectory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenDirectory.Click
        Try
            If fdbPath.ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtExportPath.Text = fdbPath.SelectedPath
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnOpenDirectory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim strFile As String = ""
        Try
            If tabcLanguageUtility.SelectedTab.Equals(tabpImportLookUp) Then
                If System.IO.File.Exists(txtLookupFile.Text) Then
                    strFile = txtLookupFile.Text
                Else
                    'Anjan (21 May 2010)-Start
                    'eZeeMsgBox.Show("Import file not found.")
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Import file not found."))
                    'Anjan (21 May 2010)-End
                    Exit Sub
                End If

                'Dim objLookup As New clsCityLookup
                'Call objLookup.ImportData(strFile)
                'objLookup = Nothing
            Else
                If System.IO.File.Exists(txtImportFileName.Text) Then
                    strFile = txtImportFileName.Text
                Else
                    'Anjan (21 May 2010)-Start
                    'eZeeMsgBox.Show("Import file not found.")
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Import file not found."))
                    'Anjan (21 May 2010)-End
                    Exit Sub
                End If

                Dim objLan As New clsLanguage
                Call objLan.ImportLanguage(strFile)
                objLan = Nothing
            End If


            'Anjan (21 May 2010)-Start
            'eZeeMsgBox.Show( "Import data successfully")
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Import data successfully"))
            'Anjan (21 May 2010)-End

            mblnCancel = False

            Me.Close()

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim dsList As New DataSet
        Dim objLanguage As New clsLanguage
        Dim objMessage As New clsMessages
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = ""

        Dim strFile As String = ""
        Try

            If System.IO.Directory.Exists(txtExportPath.Text) Then
                strFile = txtExportPath.Text
            Else
                'Anjan (21 May 2010)-Start
                'eZeeMsgBox.Show("Export path not found")
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Export path not found"))
                'Anjan (21 May 2010)-End
                Exit Sub
            End If

            If txtExportFile.Text.ToString.Trim = "" Then
                'Anjan (21 May 2010)-Start
                'eZeeMsgBox.Show("Please enter valid file name.")
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please enter valid file name."))
                'Anjan (21 May 2010)-End
                Exit Sub
            Else
                If strFile.Trim.Substring(strFile.Trim.Length - 1, 1) = "\" Then
                    strFile &= txtExportFile.Text & ".xml"
                ElseIf strFile.Trim.Substring(strFile.Trim.Length - 1, 1) = "/" Then
                    strFile &= txtExportFile.Text & ".xml"
                Else
                    strFile &= "\" & txtExportFile.Text & ".xml"
                End If
            End If

            dsList.Tables.Add(objLanguage.getList("Language").Tables(0).Copy)
            dsList.Tables.Add(objMessage.getList("Message").Tables(0).Copy)

            'ForntDesk Default Table
            'strQ = "SELECT privilegegroupunkid AS Code " & _
            '            ", Name " & _
            '        "FROM cfdeskuserprivilegegroup_master "

            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "PrivilegeGroup").Tables(0).Copy)

            'strQ = "SELECT privilegeunkid AS Code " & _
            '            ", privilege_name AS Name " & _
            '        "FROM cfdeskprivilege_master "

            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "Privilege").Tables(0).Copy)

            'strQ = "SELECT deskuserabilitylevelunkid AS Code " & _
            '            ", Name " & _
            '        "FROM cfdeskuserabilitylevel_master "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "AbilityLevel").Tables(0).Copy)

            'strQ = "SELECT planunkid AS Code " & _
            '            ", Name " & _
            '        "FROM cfplan_master "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "Plan").Tables(0).Copy)

            'strQ = "SELECT vehicleunkid AS Code " & _
            '            ", vehicle_make AS Name " & _
            '             ", vehicle_model AS description " & _
            '        "FROM cfvehicle_master "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "Vehicle").Tables(0).Copy)

            'strQ = "SELECT reportcategoryunkid AS Code " & _
            '            ", Name " & _
            '        "FROM cfreport_category "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "ReportCategory").Tables(0).Copy)

            'strQ = "SELECT reportunkid AS Code " & _
            '            ", Name " & _
            '        "FROM cfreport_master "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "Report").Tables(0).Copy)

            ''Common
            'strQ = "SELECT countryunkid AS Code " & _
            '            ", country_name AS Name " & _
            '            ", country_name1 AS Name1 " & _
            '            ", country_name2 AS Name2 " & _
            '        "FROM hrmsConfiguration..cfcountry_master"
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "Country").Tables(0).Copy)

            ''POS 
            ' ''for Form PrivilegeGroup 
            'strQ = "SELECT privilegeheaderunkid AS Code " & _
            '            ", headername AS Name " & _
            '            ", headername1 AS Name1 " & _
            '            ", headername2 AS Name2 " & _
            '        "FROM posdeskprivilege_header "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "POS_PrivilegeGroup").Tables(0).Copy)

            ' ''For From Privilege 
            'strQ = "SELECT privilegeunkid AS Code " & _
            '            ", privilege_name AS Name " & _
            '            ", privilege_name1 AS Name1 " & _
            '            ", privilege_name2 AS Name2 " & _
            '        "FROM posdeskprivilege_list "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "POS_Privilege").Tables(0).Copy)

            ' ''For From Report Category
            'strQ = "SELECT reportcategoryunkid AS Code " & _
            '            ", Name " & _
            '            ", Name1 " & _
            '            ", Name2 " & _
            '        "FROM posreport_category "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "POS_ReportCategory").Tables(0).Copy)

            ' ''For From Report Master 
            'strQ = "SELECT reportunkid AS Code " & _
            '            ", Name " & _
            '            ", Name1 " & _
            '            ", Name2 " & _
            '            ", description " & _
            '            ", description1 " & _
            '            ", description2 " & _
            '        "FROM posreport_master "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "POS_Report").Tables(0).Copy)

            ' ''For From Report Master 
            'strQ = "SELECT fieldunkid AS Code " & _
            '            ", Name " & _
            '            ", Name1 " & _
            '            ", Name2 " & _
            '        "FROM postemplate_field "
            'dsList.Tables.Add(objDataOperation.ExecQuery(strQ, "POS_Template_Field").Tables(0).Copy)

            dsList.WriteXml(strFile, XmlWriteMode.WriteSchema)

            'Anjan (21 May 2010)-Start
            'eZeeMsgBox.Show("Export file successfully. Please chack file """ & strFile & """")
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Export file successfully. Please check file") & " " & strFile)
            'Anjan (21 May 2010)-End

            mblnCancel = False
            Me.Close()

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnExport_Click", mstrModuleName)

        Finally
            objLanguage = Nothing
            objMessage = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing

            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            mblnCancel = True

            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region





	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbImport.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImport.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbExport.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExport.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbImportLookup.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbImportLookup.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objbtnOpenDirectory.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnOpenDirectory.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnLookup.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLookup.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.tabpExport.Text = Language._Object.getCaption(Me.tabpExport.Name, Me.tabpExport.Text)
			Me.tabpImport.Text = Language._Object.getCaption(Me.tabpImport.Name, Me.tabpImport.Text)
			Me.lblExportFileName.Text = Language._Object.getCaption(Me.lblExportFileName.Name, Me.lblExportFileName.Text)
			Me.lblExportPath.Text = Language._Object.getCaption(Me.lblExportPath.Name, Me.lblExportPath.Text)
			Me.lblImportFilePath.Text = Language._Object.getCaption(Me.lblImportFilePath.Name, Me.lblImportFilePath.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbImport.Text = Language._Object.getCaption(Me.gbImport.Name, Me.gbImport.Text)
			Me.gbExport.Text = Language._Object.getCaption(Me.gbExport.Name, Me.gbExport.Text)
			Me.tabpImportLookUp.Text = Language._Object.getCaption(Me.tabpImportLookUp.Name, Me.tabpImportLookUp.Text)
			Me.gbImportLookup.Text = Language._Object.getCaption(Me.gbImportLookup.Name, Me.gbImportLookup.Text)
			Me.btnLookup.Text = Language._Object.getCaption(Me.btnLookup.Name, Me.btnLookup.Text)
			Me.lblLookupFile.Text = Language._Object.getCaption(Me.lblLookupFile.Name, Me.lblLookupFile.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Import file not found.")
			Language.setMessage(mstrModuleName, 2, "Import data successfully")
			Language.setMessage(mstrModuleName, 3, "Export file successfully. Please check file")
			Language.setMessage(mstrModuleName, 4, "Export path not found")
			Language.setMessage(mstrModuleName, 5, "Please enter valid file name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
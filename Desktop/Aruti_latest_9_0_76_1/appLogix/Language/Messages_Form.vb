'************************************************************************************************************************************
'Class Name : frmMessages.vb
'Purpose    : show, edit system Messages.
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Imports System.Text
Imports System.Text.RegularExpressions

Imports eZeeCommonLib

Public Class frmMessages
    Private ReadOnly mstrModuleName As String = "frmMessages"

    Private daMessages As SqlDataAdapter
    Friend WithEvents dtMessages As DataTable
    Private cbMessages As SqlCommandBuilder

    Private blnIsChanage As Boolean = False
    Private WithEvents objFindReplaceLanguage As New frmFindReplaceLanguage
    Private mFormControl As Control = Nothing

#Region " Public Method "
    Public Sub displayDialog()
        Me.ShowDialog()
    End Sub
#End Region

    Private Sub createDataAdapter()
        Dim strQ As String = ""
        Dim objDataopration As clsDataOperation = Nothing
        Try

            objDataopration = New clsDataOperation
            ''For From Messages 
            strQ = "SELECT * FROM cfmessages " & _
                    "WHERE application = 0 " & _
                    "AND module_name not like 'frm%'"

            dtMessages = New DataTable
            dtMessages.TableName = "Messages"

            daMessages = New SqlDataAdapter
            daMessages.SelectCommand = objDataopration.SQL_Command(strQ)
            daMessages.ContinueUpdateOnError = True
            daMessages.Fill(dtMessages)

            cbMessages = New SqlCommandBuilder(daMessages)

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "createDataAdapter", mstrModuleName)
        Finally
            If objDataopration IsNot Nothing Then objDataopration.Dispose()
            objDataopration = Nothing
        End Try
    End Sub

    Private Sub fillDatagridViews()
        Try

            dgMessage.AutoGenerateColumns = False
            dgMessage.DataSource = dtMessages

            dgcolMsgCode.DataPropertyName = "messagecode"
            dgcolMsgModuleName.DataPropertyName = "module_name"
            dgcolMessage.DataPropertyName = "message"
            dgcolMessage1.DataPropertyName = "message1"
            dgcolMessage2.DataPropertyName = "message2"
            dgcolMsgApplication.DataPropertyName = "application"

            dgcolMessage.HeaderText = Language.getMessage("Language", 1, "Default English")
            dgcolMessage1.HeaderText = Language.getMessage("Language", 2, "Custom 1")
            dgcolMessage2.HeaderText = Language.getMessage("Language", 3, "Custom 2")

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "fillDatagridViews", mstrModuleName)
        End Try
    End Sub

    Private Sub SaveValue()
        Try
            daMessages.Update(dtMessages)

            cbMessages.RefreshSchema()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "SaveValue", mstrModuleName)
        Finally

        End Try
    End Sub

#Region " Forms "

    Private Sub frmLanguage_FormClosing(ByVal sender As System.Object, _
                ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

        Try
            objFindReplaceLanguage.Close()
            mFormControl = Nothing
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLanguage_FormClosing", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLanguage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave.PerformClick()
            End If
            If e.Control = True And e.KeyCode = Keys.F Then
                btnFindReplace.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLanguage_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLanguage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strQ As String = ""

        Try
            Call Set_Logo(Me, gApplicationType)
            Call OtherSettings()

            Call createDataAdapter()
            Call fillDatagridViews()

            dgMessage.Refresh()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmLanguage_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Call SaveValue()
            blnIsChanage = False
        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "btnSave_Click", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If blnIsChanage Then
                If eZeeMsgBox.Show("Do you want save changes you made?", enMsgBoxStyle.YesNo + enMsgBoxStyle.Question) = Windows.Forms.DialogResult.Yes Then
                    Call SaveValue()
                End If
            End If

            Me.Close()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "For Find Replace"
    ' make the regex and match class level variables 
    ' to make happen find next 
    Private regex As Regex
    Private match As Match
    Private eSearchEvent As New SearchEvent

    Private mintMatchIndex As Integer = 0
    Private mintRowIndex As Integer = 0
    Private mintCntFind As Integer = 0

    Private Sub btnFindReplace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindReplace.Click
        objFindReplaceLanguage.TopMost = True
        objFindReplaceLanguage.Show()
    End Sub

    ''' <summary> 
    ''' This function makes and returns a RegEx object 
    ''' depending on user input 
    ''' </summary> 
    ''' <returns></returns> 
    Private Function GetRegExpression(ByVal searchTextBox As String) As Regex
        Dim result As Regex
        Dim regExString As String

        ' Get what the user entered 
        regExString = searchTextBox

        ' If regular expressions checkbox is selected, 
        ' our job is easy. Just do nothing 
        'If useRegulatExpressionCheckBox.Checked Then
        If eSearchEvent._UseWildcards Then
            ' wild cards checkbox checked 
            regExString = regExString.Replace("*", "\w*")
            ' multiple characters wildcard (*) 
            regExString = regExString.Replace("?", "\w")
            ' single character wildcard (?) 
            ' if wild cards selected, find whole words only 
            regExString = [String].Format("{0}{1}{0}", "\b", regExString)
        Else
            ' replace escape characters 
            'regExString = regex.Escape(regExString)
            regExString = RegularExpressions.Regex.Escape(regExString)
        End If

        ' Is whole word check box checked? 
        If eSearchEvent._MatchWholeWord Then
            regExString = [String].Format("{0}{1}{0}", "\b", regExString)
        End If

        ' Is match case checkbox checked? 
        If eSearchEvent._MatchCase Then
            result = New Regex(regExString)
        Else
            result = New Regex(regExString, RegexOptions.IgnoreCase)
        End If

        Return result
    End Function

    ''' <summary> 
    ''' finds the text in searchTextBox in contentTextBox 
    ''' </summary> 
    Private Function FindText(ByVal contentTextBox As String, _
                        ByVal searchTextBox As String, _
                        ByVal NextIndex As Integer) As Boolean

        If NextIndex = 0 Then
            match = regex.Match(contentTextBox)
        Else
            match = regex.Match(contentTextBox, NextIndex)
        End If

        If match.Success Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Property _CurrentText(ByVal intCnt As Integer) As String
        Get
            If eSearchEvent._LookIn = 2 Then
                Return dgMessage.Rows(intCnt).Cells(dgcolMessage2.Index).Value
            Else
                Return dgMessage.Rows(intCnt).Cells(dgcolMessage1.Index).Value
            End If
        End Get
        Set(ByVal value As String)
            If eSearchEvent._LookIn = 2 Then
                dgMessage.Rows(intCnt).Cells(dgcolMessage2.Index).Value = value
            Else
                dgMessage.Rows(intCnt).Cells(dgcolMessage1.Index).Value = value
            End If
        End Set
    End Property

    Private WriteOnly Property _SelectedRow(ByVal intCnt As Integer) As Boolean
        Set(ByVal value As Boolean)
            If eSearchEvent._LookIn = 2 Then
                dgMessage.Rows(intCnt).Cells(dgcolMessage2.Index).Selected = value
            Else
                dgMessage.Rows(intCnt).Cells(dgcolMessage1.Index).Selected = value
            End If
        End Set
    End Property

    Private ReadOnly Property _RowCount() As Integer
        Get
            Return dgMessage.Rows.Count
        End Get
    End Property

    Private Sub objFindReplaceLanguage_FindNext(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.FindNext
        Dim intTempRowIndex As Integer = -1
        Dim intCnt As Integer
        Dim input As String = ""

        Try
            eSearchEvent = e

            regex = GetRegExpression(eSearchEvent._FindWhat)

            If mintRowIndex = _RowCount - 1 Then
                mintRowIndex = 0
            End If

            For intCnt = mintRowIndex To _RowCount - 1
                input = _CurrentText(intCnt)

                If FindText(input, e._FindWhat, mintMatchIndex) Then
                    mintMatchIndex = match.Index + 1
                    intTempRowIndex = intCnt
                    Exit For
                Else
                    mintMatchIndex = 0
                End If
            Next

            mintRowIndex = intTempRowIndex

            If mintRowIndex > -1 Then
                _SelectedRow(intCnt) = True

            Else
                MessageBox.Show([String].Format("Cannot find '{0}'. ", e._FindWhat), Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                mintRowIndex = 0
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "FindString", mstrModuleName)
        End Try
    End Sub

    Private Sub objFindReplaceLanguage_Replace(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.Replace
        eSearchEvent = e

        Dim input As String = ""
        Dim replaceRegex As Regex = GetRegExpression(e._FindWhat)
        Dim replacedString As String
        Try

            If mintRowIndex > -1 Then
                input = _CurrentText(mintRowIndex)

                If mintMatchIndex <> 0 Then
                    replacedString = replaceRegex.Replace(input, e._ReplaceWith, 1, mintMatchIndex - 1)

                    If input <> replacedString Then
                        _CurrentText(mintRowIndex) = replacedString
                    End If
                End If

            End If

            Call objFindReplaceLanguage_FindNext(sender, e)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objFindReplaceLanguage_Replace", mstrModuleName)
        End Try
    End Sub

    Private Sub objFindReplaceLanguage_ReplaceAll(ByVal sender As Object, ByVal e As SearchEvent) Handles objFindReplaceLanguage.ReplaceAll
        eSearchEvent = e

        Dim input As String = ""
        Dim replaceRegex As Regex = GetRegExpression(e._FindWhat)
        Dim replacedString As String
        Dim CountReplace As Integer = 0

        Try

            For iCnt As Integer = 0 To _RowCount - 1
                input = _CurrentText(iCnt)

                replacedString = replaceRegex.Replace(input, e._ReplaceWith)

                If input <> replacedString Then
                    _CurrentText(iCnt) = replacedString
                    CountReplace += 1
                End If
            Next

            MessageBox.Show(CountReplace & " Replacements are made. ", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objFindReplaceLanguage_ReplaceAll", mstrModuleName)
        End Try
    End Sub
#End Region

    Private Sub dtMessages_RowChanged(ByVal sender As Object, ByVal e As System.Data.DataRowChangeEventArgs) Handles dtMessages.RowChanged
        If e.Row.RowState = DataRowState.Modified Then
            blnIsChanage = True
        End If
    End Sub

#Region "UI Settings"

    Private Sub dg_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgMessage.CellPainting
        Try
            If e.RowIndex = -1 Then
                Dim br As System.Drawing.Drawing2D.LinearGradientBrush
                br = New System.Drawing.Drawing2D.LinearGradientBrush(e.CellBounds, System.Drawing.Color.White, System.Drawing.Color.FromArgb(255, 5, 80, 150), System.Drawing.Drawing2D.LinearGradientMode.Vertical)
                e.Graphics.FillRectangle(br, e.CellBounds)
                e.PaintContent(e.ClipBounds)
                e.Handled = True
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnFindReplace.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFindReplace.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.dgcolMsgCode.HeaderText = Language._Object.getCaption(Me.dgcolMsgCode.Name, Me.dgcolMsgCode.HeaderText)
			Me.dgcolMsgModuleName.HeaderText = Language._Object.getCaption(Me.dgcolMsgModuleName.Name, Me.dgcolMsgModuleName.HeaderText)
			Me.dgcolMessage.HeaderText = Language._Object.getCaption(Me.dgcolMessage.Name, Me.dgcolMessage.HeaderText)
			Me.dgcolMessage1.HeaderText = Language._Object.getCaption(Me.dgcolMessage1.Name, Me.dgcolMessage1.HeaderText)
			Me.dgcolMessage2.HeaderText = Language._Object.getCaption(Me.dgcolMessage2.Name, Me.dgcolMessage2.HeaderText)
			Me.dgcolMsgApplication.HeaderText = Language._Object.getCaption(Me.dgcolMsgApplication.Name, Me.dgcolMsgApplication.HeaderText)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnFindReplace.Text = Language._Object.getCaption(Me.btnFindReplace.Name, Me.btnFindReplace.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage("Language", 1, "Default English")
			Language.setMessage("Language", 2, "Custom 1")
			Language.setMessage("Language", 3, "Custom 2")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class


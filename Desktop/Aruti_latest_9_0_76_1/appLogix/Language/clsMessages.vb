'************************************************************************************************************************************
'Class Name : clsMessages.vb
'Purpose    : All owner level opration like getlist, getComboList, insert, update, delete, checkduplicate.
'Date       : 2/18/2008
'Written By : Naimish
'Modified   : 05/06/2009
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Developer: Naimish
''' </summary>
Public Class clsMessages
    Private ReadOnly mstrModuleName As String = "clsMessages"

#Region " Private variables "
    Private mintMessageUnkId As Integer = -1
    Private mstrMessagecode As String = ""
    Private mintApplication As Integer = -1
    Private mstrModule_Name As String = ""
    Private mstrMessage As String = ""
    Private mstrMessage1 As String = ""
    Private mstrMessage2 As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _MessageUnkId() As Integer
        Get
            Return mintMessageUnkId
        End Get
        Set(ByVal value As Integer)
            mintMessageUnkId = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Messagecode() As String
        Get
            Return mstrMessagecode
        End Get
        Set(ByVal value As String)
            mstrMessagecode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Application() As Integer
        Get
            Return mintApplication
        End Get
        Set(ByVal value As Integer)
            mintApplication = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Module_Name() As String
        Get
            Return mstrModule_Name
        End Get
        Set(ByVal value As String)
            mstrModule_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Message1() As String
        Get
            Return mstrMessage1
        End Get
        Set(ByVal value As String)
            mstrMessage1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Naimish
    ''' </summary>
    Public Property _Message2() As String
        Get
            Return mstrMessage2
        End Get
        Set(ByVal value As String)
            mstrMessage2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' For insert given property value in database table (cfmessages).
    ''' Modify By: Naimish
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function Insert() As Boolean

        Dim strQ As String = ""

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.AddParameter("@messagecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMessagecode.ToString)
            objDataOperation.AddParameter("@application", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, mintApplication.ToString)
            objDataOperation.AddParameter("@module_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrModule_Name.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage.ToString)
            objDataOperation.AddParameter("@message1", SqlDbType.nvarchar, eZeeDataType.DESC_SIZE, mstrMessage1.ToString)
            objDataOperation.AddParameter("@message2", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrMessage2.ToString)

            strQ = "INSERT INTO cfmessages ( " & _
                        "messagecode, " & _
                        "application, " & _
                        "module_name, " & _
                        "message, " & _
                        "message1, " & _
                        "message2" & _
                    ") VALUES (" & _
                        "@messagecode, " & _
                        "@application, " & _
                        "@module_name, " & _
                        "@message, " & _
                        "@message1, " & _
                        "@message2)"

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception

            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Naimish
    ''' </summary>
    Public Sub Truncate()

        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "TRUNCATE TABLE cfmessages "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            objDataOperation = Nothing
        End Try

    End Sub


#Region "List"
    ''' <summary>
    ''' Get list of all fields from database table (cfmessages).
    ''' Modify By: Naimish
    ''' </summary>
    ''' <param name="strListName">Optional Para, DataTable name.</param>
    ''' <returns>DataSet</returns>
    Public Function getList(Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            strQ = "SELECT " & _
                        "messageunkid, " & _
                        "messagecode, " & _
                        "application, " & _
                        "module_name, " & _
                        "message, " & _
                        "message1, " & _
                        "message2 " & _
                    "FROM cfmessages "

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try

        Return dsList

    End Function

#End Region
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFindReplaceLanguage
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFindReplaceLanguage))
        Me.lblReplace = New System.Windows.Forms.Label
        Me.txtReplace = New eZee.TextBox.AlphanumericTextBox
        Me.lblSearch = New System.Windows.Forms.Label
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.gbFindOption = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkUseWildCards = New System.Windows.Forms.CheckBox
        Me.chkMatchCase = New System.Windows.Forms.CheckBox
        Me.chkMatchWholeWord = New System.Windows.Forms.CheckBox
        Me.cboLanguage = New System.Windows.Forms.ComboBox
        Me.btnFindNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReplace = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReplaceAll = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.gbFindOption.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblReplace
        '
        Me.lblReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReplace.Location = New System.Drawing.Point(10, 50)
        Me.lblReplace.Name = "lblReplace"
        Me.lblReplace.Size = New System.Drawing.Size(77, 13)
        Me.lblReplace.TabIndex = 2
        Me.lblReplace.Text = "Replace with"
        '
        'txtReplace
        '
        Me.txtReplace.Flags = 0
        Me.txtReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReplace.InvalidChars = New Char(-1) {}
        Me.txtReplace.Location = New System.Drawing.Point(12, 66)
        Me.txtReplace.Name = "txtReplace"
        Me.txtReplace.Size = New System.Drawing.Size(302, 21)
        Me.txtReplace.TabIndex = 3
        '
        'lblSearch
        '
        Me.lblSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearch.Location = New System.Drawing.Point(10, 10)
        Me.lblSearch.Name = "lblSearch"
        Me.lblSearch.Size = New System.Drawing.Size(77, 13)
        Me.lblSearch.TabIndex = 0
        Me.lblSearch.Text = "Find what"
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char(-1) {}
        Me.txtSearch.Location = New System.Drawing.Point(12, 26)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(302, 21)
        Me.txtSearch.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Look in"
        '
        'gbFindOption
        '
        Me.gbFindOption.BorderColor = System.Drawing.Color.Black
        Me.gbFindOption.Checked = False
        Me.gbFindOption.CollapseAllExceptThis = False
        Me.gbFindOption.CollapsedHoverImage = Nothing
        Me.gbFindOption.CollapsedNormalImage = Nothing
        Me.gbFindOption.CollapsedPressedImage = Nothing
        Me.gbFindOption.CollapseOnLoad = False
        Me.gbFindOption.Controls.Add(Me.chkUseWildCards)
        Me.gbFindOption.Controls.Add(Me.chkMatchCase)
        Me.gbFindOption.Controls.Add(Me.chkMatchWholeWord)
        Me.gbFindOption.ExpandedHoverImage = Nothing
        Me.gbFindOption.ExpandedNormalImage = Nothing
        Me.gbFindOption.ExpandedPressedImage = Nothing
        Me.gbFindOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFindOption.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFindOption.HeaderHeight = 25
        Me.gbFindOption.HeaderMessage = ""
        Me.gbFindOption.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFindOption.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFindOption.HeightOnCollapse = 0
        Me.gbFindOption.LeftTextSpace = 0
        Me.gbFindOption.Location = New System.Drawing.Point(12, 132)
        Me.gbFindOption.Name = "gbFindOption"
        Me.gbFindOption.OpenHeight = 100
        Me.gbFindOption.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFindOption.ShowBorder = True
        Me.gbFindOption.ShowCheckBox = False
        Me.gbFindOption.ShowCollapseButton = False
        Me.gbFindOption.ShowDefaultBorderColor = True
        Me.gbFindOption.ShowDownButton = False
        Me.gbFindOption.ShowHeader = True
        Me.gbFindOption.Size = New System.Drawing.Size(302, 100)
        Me.gbFindOption.TabIndex = 6
        Me.gbFindOption.Temp = 0
        Me.gbFindOption.Text = "Find Option"
        Me.gbFindOption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkUseWildCards
        '
        Me.chkUseWildCards.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkUseWildCards.Location = New System.Drawing.Point(9, 77)
        Me.chkUseWildCards.Margin = New System.Windows.Forms.Padding(6)
        Me.chkUseWildCards.Name = "chkUseWildCards"
        Me.chkUseWildCards.Size = New System.Drawing.Size(287, 17)
        Me.chkUseWildCards.TabIndex = 2
        Me.chkUseWildCards.Text = "Use wildcards "
        Me.chkUseWildCards.UseVisualStyleBackColor = True
        '
        'chkMatchCase
        '
        Me.chkMatchCase.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkMatchCase.Location = New System.Drawing.Point(9, 55)
        Me.chkMatchCase.Margin = New System.Windows.Forms.Padding(6)
        Me.chkMatchCase.Name = "chkMatchCase"
        Me.chkMatchCase.Size = New System.Drawing.Size(287, 17)
        Me.chkMatchCase.TabIndex = 1
        Me.chkMatchCase.Text = "Match case"
        Me.chkMatchCase.UseVisualStyleBackColor = True
        '
        'chkMatchWholeWord
        '
        Me.chkMatchWholeWord.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkMatchWholeWord.Location = New System.Drawing.Point(9, 32)
        Me.chkMatchWholeWord.Margin = New System.Windows.Forms.Padding(6)
        Me.chkMatchWholeWord.Name = "chkMatchWholeWord"
        Me.chkMatchWholeWord.Size = New System.Drawing.Size(287, 17)
        Me.chkMatchWholeWord.TabIndex = 0
        Me.chkMatchWholeWord.Text = "Match whole word"
        Me.chkMatchWholeWord.UseVisualStyleBackColor = True
        '
        'cboLanguage
        '
        Me.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage.FormattingEnabled = True
        Me.cboLanguage.Location = New System.Drawing.Point(12, 105)
        Me.cboLanguage.Name = "cboLanguage"
        Me.cboLanguage.Size = New System.Drawing.Size(302, 21)
        Me.cboLanguage.TabIndex = 5
        '
        'btnFindNext
        '
        Me.btnFindNext.BackColor = System.Drawing.Color.White
        Me.btnFindNext.BackgroundImage = CType(resources.GetObject("btnFindNext.BackgroundImage"), System.Drawing.Image)
        Me.btnFindNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFindNext.BorderColor = System.Drawing.Color.Empty
        Me.btnFindNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnFindNext.FlatAppearance.BorderSize = 0
        Me.btnFindNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindNext.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFindNext.ForeColor = System.Drawing.Color.Black
        Me.btnFindNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFindNext.GradientForeColor = System.Drawing.Color.Black
        Me.btnFindNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFindNext.Location = New System.Drawing.Point(38, 14)
        Me.btnFindNext.Name = "btnFindNext"
        Me.btnFindNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFindNext.Size = New System.Drawing.Size(88, 29)
        Me.btnFindNext.TabIndex = 7
        Me.btnFindNext.Text = "&Find Next"
        Me.btnFindNext.UseVisualStyleBackColor = False
        '
        'btnReplace
        '
        Me.btnReplace.BackColor = System.Drawing.Color.White
        Me.btnReplace.BackgroundImage = CType(resources.GetObject("btnReplace.BackgroundImage"), System.Drawing.Image)
        Me.btnReplace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReplace.BorderColor = System.Drawing.Color.Empty
        Me.btnReplace.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReplace.FlatAppearance.BorderSize = 0
        Me.btnReplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplace.ForeColor = System.Drawing.Color.Black
        Me.btnReplace.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReplace.GradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Location = New System.Drawing.Point(132, 14)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplace.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReplace.Size = New System.Drawing.Size(88, 29)
        Me.btnReplace.TabIndex = 8
        Me.btnReplace.Text = "&Replace"
        Me.btnReplace.UseVisualStyleBackColor = False
        '
        'btnReplaceAll
        '
        Me.btnReplaceAll.BackColor = System.Drawing.Color.White
        Me.btnReplaceAll.BackgroundImage = CType(resources.GetObject("btnReplaceAll.BackgroundImage"), System.Drawing.Image)
        Me.btnReplaceAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReplaceAll.BorderColor = System.Drawing.Color.Empty
        Me.btnReplaceAll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReplaceAll.FlatAppearance.BorderSize = 0
        Me.btnReplaceAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReplaceAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReplaceAll.ForeColor = System.Drawing.Color.Black
        Me.btnReplaceAll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReplaceAll.GradientForeColor = System.Drawing.Color.Black
        Me.btnReplaceAll.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplaceAll.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReplaceAll.Location = New System.Drawing.Point(226, 14)
        Me.btnReplaceAll.Name = "btnReplaceAll"
        Me.btnReplaceAll.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReplaceAll.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReplaceAll.Size = New System.Drawing.Size(88, 29)
        Me.btnReplaceAll.TabIndex = 9
        Me.btnReplaceAll.Text = "Replace &All"
        Me.btnReplaceAll.UseVisualStyleBackColor = False
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.lblSearch)
        Me.pnlMain.Controls.Add(Me.txtSearch)
        Me.pnlMain.Controls.Add(Me.txtReplace)
        Me.pnlMain.Controls.Add(Me.lblReplace)
        Me.pnlMain.Controls.Add(Me.cboLanguage)
        Me.pnlMain.Controls.Add(Me.Label1)
        Me.pnlMain.Controls.Add(Me.gbFindOption)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(325, 296)
        Me.pnlMain.TabIndex = 10
        '
        'objefFooter
        '
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnReplaceAll)
        Me.objefFooter.Controls.Add(Me.btnReplace)
        Me.objefFooter.Controls.Add(Me.btnFindNext)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefFooter.Location = New System.Drawing.Point(0, 241)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(325, 55)
        Me.objefFooter.TabIndex = 10
        '
        'frmFindReplaceLanguage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(325, 296)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFindReplaceLanguage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Find and Replace"
        Me.gbFindOption.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objefFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblReplace As System.Windows.Forms.Label
    Friend WithEvents txtReplace As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSearch As System.Windows.Forms.Label
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbFindOption As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents chkUseWildCards As System.Windows.Forms.CheckBox
    Private WithEvents chkMatchCase As System.Windows.Forms.CheckBox
    Private WithEvents chkMatchWholeWord As System.Windows.Forms.CheckBox
    Friend WithEvents cboLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents btnFindNext As eZee.Common.eZeeLightButton
    Friend WithEvents btnReplace As eZee.Common.eZeeLightButton
    Friend WithEvents btnReplaceAll As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMessages
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMessages))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnFindReplace = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgMessage = New System.Windows.Forms.DataGridView
        Me.dgcolMsgCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMsgModuleName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMessage1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMessage2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolMsgApplication = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMain.SuspendLayout()
        Me.objefFooter.SuspendLayout()
        CType(Me.dgMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFooter)
        Me.pnlMain.Controls.Add(Me.dgMessage)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(688, 451)
        Me.pnlMain.TabIndex = 0
        '
        'objefFooter
        '
        Me.objefFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFooter.Controls.Add(Me.btnCancel)
        Me.objefFooter.Controls.Add(Me.btnSave)
        Me.objefFooter.Controls.Add(Me.btnFindReplace)
        Me.objefFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefFooter.Location = New System.Drawing.Point(0, 396)
        Me.objefFooter.Name = "objefFooter"
        Me.objefFooter.Size = New System.Drawing.Size(688, 55)
        Me.objefFooter.TabIndex = 14
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(588, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Selected = False
        Me.btnCancel.ShowDefaultBorderColor = True
        Me.btnCancel.Size = New System.Drawing.Size(91, 30)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(491, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Selected = False
        Me.btnSave.ShowDefaultBorderColor = True
        Me.btnSave.Size = New System.Drawing.Size(91, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnFindReplace
        '
        Me.btnFindReplace.BackColor = System.Drawing.Color.White
        Me.btnFindReplace.BackgroundImage = CType(resources.GetObject("btnFindReplace.BackgroundImage"), System.Drawing.Image)
        Me.btnFindReplace.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnFindReplace.BorderColor = System.Drawing.Color.Empty
        Me.btnFindReplace.FlatAppearance.BorderSize = 0
        Me.btnFindReplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnFindReplace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFindReplace.ForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFindReplace.GradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindReplace.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.Location = New System.Drawing.Point(9, 13)
        Me.btnFindReplace.Name = "btnFindReplace"
        Me.btnFindReplace.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnFindReplace.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnFindReplace.Selected = False
        Me.btnFindReplace.ShowDefaultBorderColor = True
        Me.btnFindReplace.Size = New System.Drawing.Size(108, 30)
        Me.btnFindReplace.TabIndex = 13
        Me.btnFindReplace.Text = "&Find && Replace"
        Me.btnFindReplace.UseVisualStyleBackColor = False
        '
        'dgMessage
        '
        Me.dgMessage.AllowUserToAddRows = False
        Me.dgMessage.AllowUserToDeleteRows = False
        Me.dgMessage.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgMessage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgMessage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolMsgCode, Me.dgcolMsgModuleName, Me.dgcolMessage, Me.dgcolMessage1, Me.dgcolMessage2, Me.dgcolMsgApplication})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgMessage.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgMessage.Location = New System.Drawing.Point(9, 66)
        Me.dgMessage.Name = "dgMessage"
        Me.dgMessage.RowHeadersVisible = False
        Me.dgMessage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgMessage.ShowCellErrors = False
        Me.dgMessage.ShowRowErrors = False
        Me.dgMessage.Size = New System.Drawing.Size(670, 324)
        Me.dgMessage.TabIndex = 3
        '
        'dgcolMsgCode
        '
        Me.dgcolMsgCode.Frozen = True
        Me.dgcolMsgCode.HeaderText = "ID"
        Me.dgcolMsgCode.Name = "dgcolMsgCode"
        Me.dgcolMsgCode.ReadOnly = True
        Me.dgcolMsgCode.Visible = False
        Me.dgcolMsgCode.Width = 5
        '
        'dgcolMsgModuleName
        '
        Me.dgcolMsgModuleName.Frozen = True
        Me.dgcolMsgModuleName.HeaderText = "Module Name"
        Me.dgcolMsgModuleName.Name = "dgcolMsgModuleName"
        Me.dgcolMsgModuleName.ReadOnly = True
        Me.dgcolMsgModuleName.Visible = False
        Me.dgcolMsgModuleName.Width = 150
        '
        'dgcolMessage
        '
        Me.dgcolMessage.Frozen = True
        Me.dgcolMessage.HeaderText = "Message"
        Me.dgcolMessage.Name = "dgcolMessage"
        Me.dgcolMessage.ReadOnly = True
        Me.dgcolMessage.Width = 223
        '
        'dgcolMessage1
        '
        Me.dgcolMessage1.HeaderText = "Message1"
        Me.dgcolMessage1.Name = "dgcolMessage1"
        Me.dgcolMessage1.Width = 222
        '
        'dgcolMessage2
        '
        Me.dgcolMessage2.HeaderText = "Message2"
        Me.dgcolMessage2.Name = "dgcolMessage2"
        Me.dgcolMessage2.Width = 222
        '
        'dgcolMsgApplication
        '
        Me.dgcolMsgApplication.HeaderText = "Application"
        Me.dgcolMsgApplication.Name = "dgcolMsgApplication"
        Me.dgcolMsgApplication.ReadOnly = True
        Me.dgcolMsgApplication.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = "Message"
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(688, 60)
        Me.eZeeHeader.TabIndex = 11
        Me.eZeeHeader.Title = "Title"
        '
        'frmMessages
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(688, 451)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMessages"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Messages"
        Me.pnlMain.ResumeLayout(False)
        Me.objefFooter.ResumeLayout(False)
        CType(Me.dgMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents dgMessage As System.Windows.Forms.DataGridView
    Friend WithEvents dgcolMsgCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMsgModuleName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMessage2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolMsgApplication As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents btnFindReplace As eZee.Common.eZeeLightButton
    Friend WithEvents objefFooter As eZee.Common.eZeeFooter
End Class

﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

'Last Message Index = 3

Public Class frmBankBranch_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmBankBranch_AddEdit"
    Private mblnCancel As Boolean = True
    Private objBankbranchmaster As clsbankbranch_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintBranchMasterUnkid As Integer = -1

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintBranchMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintBranchMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmBankBranch_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBankbranchmaster = New clsbankbranch_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objBankbranchmaster._Branchunkid = mintBranchMasterUnkid
                'Sohail (11 Sep 2010) -- Start
                cboGroup.Enabled = False
                objbtnAddGroup.Enabled = False
                'Sohail (11 Sep 2010) -- End
            End If
            FillBankGroup()
            FillCountry()
            GetValue()
            'Sohail (24 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If menAction <> enAction.EDIT_ONE Then
                chkClearing.Checked = True
            End If
            'Sohail (24 Jul 2013) -- End
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranch_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankBranch_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranch_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankBranch_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranch_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankBranch_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Sohail (11 Sep 2010) -- Start
        Try
        objBankbranchmaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankBranch_AddEdit_FormClosed", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- end
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsbankbranch_master.SetMessages()
            objfrm._Other_ModuleNames = "clsbankbranch_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        Try
            FillZipcode()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCity_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Bank Group is compulsory information.Please Select Bank Group."), enMsgBoxStyle.Information)
                cboGroup.Focus()
                Exit Sub
            ElseIf Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Branch Code cannot be blank. Branch Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtname.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Branch Name cannot be blank. Branch Name is required information."), enMsgBoxStyle.Information)
                txtname.Focus()
                Exit Sub
            End If

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                    'Anjan [19 January 2016] -- Start
                    'dicNotification.Add(objUsr._Email, StrMessage)
                    If dicNotification.ContainsKey(objUsr._Email) = False Then
                        dicNotification.Add(objUsr._Email, StrMessage)
                    End If
                    'Anjan [19 January 2016] -- End

                Next
                objUsr = Nothing
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBankbranchmaster.Update()
            Else
                blnFlag = objBankbranchmaster.Insert()
            End If

            If blnFlag = False And objBankbranchmaster._Message <> "" Then
                eZeeMsgBox.Show(objBankbranchmaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBankbranchmaster = Nothing
                    objBankbranchmaster = New clsbankbranch_master
                    Call GetValue()
                    cboGroup.Select()
                Else
                    mintBranchMasterUnkid = objBankbranchmaster._Branchunkid
                    Me.Close()
                End If
            End If

            'S.SANDEEP [ 03 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 03 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Try
            Dim ObjPayrollGroups_AddEdit As New frmPayrollGroups_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                ObjPayrollGroups_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                ObjPayrollGroups_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(ObjPayrollGroups_AddEdit)
            End If
            'Anjan (02 Sep 2011)-End 

            ObjPayrollGroups_AddEdit.lblwebsite.Visible = True
            ObjPayrollGroups_AddEdit.txtwebsite.Visible = True
            ObjPayrollGroups_AddEdit.gbGroupInfo.Size = New Size(374, 208)
            ObjPayrollGroups_AddEdit.Size = New Size(403, 309)
            If ObjPayrollGroups_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, PayrollGroupType.Bank) Then
                FillBankGroup()
                cboGroup.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Sohail (11 Sep 2010) -- Start
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
        'Sohail (11 Sep 2010) -- End
    End Sub

    'Shani(25-Sep-2015) -- Start
    'ENHANCEMENT : Add search Option on Add edit bank Branch  Bank Group(Issue No : 477)
    Private Sub objbtnSearchBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankGroup.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboGroup.DataSource, DataTable)
            objfrm.ValueMember = "groupmasterunkid"
            objfrm.DisplayMember = "name"
            objfrm.CodeMember = "Code"
            If objfrm.DisplayDialog Then
                cboGroup.SelectedValue = objfrm.SelectedValue
            End If
            cboGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankGroup_Click", mstrModuleName)
        End Try
    End Sub
    'Shani(25-Sep-2015) -- End

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboGroup.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtaddress1.BackColor = GUI.ColorOptional
            txtaddress2.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            cboZipcode.BackColor = GUI.ColorOptional
            txtcontactperson.BackColor = GUI.ColorOptional
            txtcontactno.BackColor = GUI.ColorOptional

            'Pinkal (12-Oct-2011) -- Start
            txtSortCode.BackColor = GUI.ColorOptional
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboGroup.SelectedValue = objBankbranchmaster._Bankgroupunkid
            txtCode.Text = objBankbranchmaster._Branchcode
            txtname.Text = objBankbranchmaster._Branchname
            txtaddress1.Text = objBankbranchmaster._Address1
            txtaddress2.Text = objBankbranchmaster._Address2
            cboCountry.SelectedValue = objBankbranchmaster._Countryunkid
            cboState.SelectedValue = objBankbranchmaster._Stateunkid
            cboCity.SelectedValue = objBankbranchmaster._Cityunkid
            cboZipcode.SelectedValue = objBankbranchmaster._Pincodeunkid
            txtcontactno.Text = objBankbranchmaster._Contactno
            txtcontactperson.Text = objBankbranchmaster._Contactperson

            'Pinkal (12-Oct-2011) -- Start
            txtSortCode.Text = objBankbranchmaster._Sortcode
            'Pinkal (12-Oct-2011) -- End
            chkClearing.Checked = objBankbranchmaster._Isclearing 'Sohail (24 Jul 2013)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBankbranchmaster._Bankgroupunkid = CInt(cboGroup.SelectedValue)
            objBankbranchmaster._Branchcode = txtCode.Text.Trim
            objBankbranchmaster._Branchname = txtname.Text.Trim
            objBankbranchmaster._Address1 = txtaddress1.Text.Trim
            objBankbranchmaster._Address2 = txtaddress2.Text.Trim
            objBankbranchmaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objBankbranchmaster._Stateunkid = CInt(cboState.SelectedValue)
            objBankbranchmaster._Cityunkid = CInt(cboCity.SelectedValue)
            objBankbranchmaster._Pincodeunkid = CInt(cboZipcode.SelectedValue)
            objBankbranchmaster._Contactno = txtcontactno.Text.Trim
            objBankbranchmaster._Contactperson = txtcontactperson.Text.Trim

            'Pinkal (12-Oct-2011) -- Start
            objBankbranchmaster._Sortcode = txtSortCode.Text.Trim
            'Pinkal (12-Oct-2011) -- End
            objBankbranchmaster._Isclearing = chkClearing.Checked 'Sohail (24 Jul 2013)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillBankGroup()
        Try
            Dim objGroupMaster As New clspayrollgroup_master
            Dim dsGroup As DataSet = objGroupMaster.getListForCombo(CInt(PayrollGroupType.Bank), "Bank Group", True)
            cboGroup.ValueMember = "groupmasterunkid"
            cboGroup.DisplayMember = "name"
            cboGroup.DataSource = dsGroup.Tables("Bank Group")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCountry()
        Try
            Dim objMasterdata As New clsMasterData
            Dim dsCountry As DataSet = objMasterdata.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCountry", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    Private Sub FillZipcode()
        Dim dsZipcode As DataSet = Nothing
        Try
            Dim objZipcodemaster As New clszipcode_master
            If CInt(cboCity.SelectedValue) > 0 Then
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, False, CInt(cboCity.SelectedValue))
            Else
                dsZipcode = objZipcodemaster.GetList("ZipCode", True, True)
            End If
            cboZipcode.ValueMember = "zipcodeunkid"
            cboZipcode.DisplayMember = "zipcode_no"
            cboZipcode.DataSource = dsZipcode.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillZipcode", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 03 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_Bank_Users.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Dear") & " <b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for bank branch. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 9, "This is to inform you that changes have been made for bank branch. Following information has been changed by user :") & " <b>" & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 10, "from Machine :") & " <b>" & getHostName.ToString & "</b> " & Language.getMessage(mstrModuleName, 11, "and IPAddress :") & " <b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '30%'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '30%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 4, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 5, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 6, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_Bank_Users.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enNotificationBank.BANK_GROUP
                            If objBankbranchmaster._Branchcode <> txtCode.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblCode.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objBankbranchmaster._Branchcode & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtCode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objBankbranchmaster._Branchname <> txtname.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblName.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objBankbranchmaster._Branchname & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtname.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objBankbranchmaster._Sortcode <> txtSortCode.Text Then
                                StrMessage.Append("<TR WIDTH = '30%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & lblSortCode.Text & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objBankbranchmaster._Sortcode & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtSortCode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                    End Select
                Next
            End If

            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 7, "Notification of Changes in Bank Branch.")
                            objSendMail._Message = dicNotification(sKey)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End

                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 OCT 2012 ] -- END

#End Region

    
   

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBankInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBankInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbBankInformation.Text = Language._Object.getCaption(Me.gbBankInformation.Name, Me.gbBankInformation.Text)
			Me.lblcontactperson.Text = Language._Object.getCaption(Me.lblcontactperson.Name, Me.lblcontactperson.Text)
			Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.Name, Me.lblContactNo.Text)
			Me.lblPincode.Text = Language._Object.getCaption(Me.lblPincode.Name, Me.lblPincode.Text)
			Me.lblstate.Text = Language._Object.getCaption(Me.lblstate.Name, Me.lblstate.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblGroup.Text = Language._Object.getCaption(Me.lblGroup.Name, Me.lblGroup.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblSortCode.Text = Language._Object.getCaption(Me.lblSortCode.Name, Me.lblSortCode.Text)
			Me.chkClearing.Text = Language._Object.getCaption(Me.chkClearing.Name, Me.chkClearing.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Bank Group is compulsory information.Please Select Bank Group.")
			Language.setMessage(mstrModuleName, 2, "Branch Code cannot be blank. Branch Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Branch Name cannot be blank. Branch Name is required information.")
			Language.setMessage(mstrModuleName, 4, "Field")
			Language.setMessage(mstrModuleName, 5, "Old Value")
			Language.setMessage(mstrModuleName, 6, "New Value")
			Language.setMessage(mstrModuleName, 7, "Notification of Changes in Bank Branch.")
			Language.setMessage(mstrModuleName, 8, "Dear")
			Language.setMessage(mstrModuleName, 9, "This is to inform you that changes have been made for bank branch. Following information has been changed by user :")
			Language.setMessage(mstrModuleName, 10, "from Machine :")
			Language.setMessage(mstrModuleName, 11, "and IPAddress :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
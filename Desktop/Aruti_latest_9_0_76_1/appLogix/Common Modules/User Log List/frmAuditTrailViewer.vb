﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAuditTrailViewer

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAuditTrailViewer"
    Private objAT_View As clsView_AT_Logs

#End Region

#Region " Private Methods "

    Private Sub Fill_Combo()
        Try
            With cboViewType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Application Event Log"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "User Authentication Log"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "User Attempts Log"))
                'S.SANDEEP [ 28 JAN 2014 ] -- START
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Email Notification Log"))
                'S.SANDEEP [ 28 JAN 2014 ] -- END

                'S.SANDEEP [ 17 OCT 2014 ] -- START
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Removed Pay Per Activity Log"))
                'S.SANDEEP [ 17 OCT 2014 ] -- END

                .SelectedIndex = 0
            End With


            With cboOperation
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Search"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Export"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Delete"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objAT_View.GetList("List", cboViewType.SelectedIndex + 1)
            dsList = objAT_View.GetList("List", cboViewType.SelectedIndex + 1, User._Object._Languageunkid)
            'Shani(24-Aug-2015) -- End


            If dsList IsNot Nothing Then
                If cboOperation.SelectedIndex > 0 Then
                    StrSearch &= "AND viewoperationid = " & cboOperation.SelectedIndex
                End If

                If StrSearch.Length > 0 Then
                    StrSearch = StrSearch.Substring(3)
                    dTable = New DataView(dsList.Tables(0), StrSearch, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If

                dTable.Columns.Remove("viewoperationid")

                dgvData.DataSource = dTable
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAuditTrailViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            objAT_View = New clsView_AT_Logs
            Call Fill_Combo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAuditTrailViewer_Load", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- Start
    'Enhancement - Language Settings

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (10-Jul-2014) -- End

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboOperation.SelectedIndex = 0
            cboViewType.SelectedIndex = 0
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SHARMA [ 22 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If dgvData.RowCount > 0 Then
                Dim dTable As DataTable = CType(dgvData.DataSource, DataTable)
                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='6'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 7, "Audit Trail Viewer") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                If cboViewType.SelectedIndex >= 0 Then
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & lblAttemptsType.Text.Trim & " : " & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '80%' COLSPAN='5'><FONT SIZE=2><B>" & cboViewType.Text.Trim & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If

                If cboOperation.SelectedIndex > 0 Then
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & lblOperation.Text.Trim & " : " & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '80%' COLSPAN='5'><FONT SIZE=2><B>" & cboOperation.Text.Trim & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If
                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR/> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For i As Integer = 0 To dTable.Columns.Count - 1
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dTable.Columns(i).ColumnName.ToString & "</B></FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For j As Integer = 0 To dTable.Rows.Count - 1
                    For i As Integer = 0 To dTable.Columns.Count - 1
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dTable.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next
                strBuilder.Append(" </TABLE> " & vbCrLf)


                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim svDialog As New SaveFileDialog
                svDialog.Filter = "Excel files (*.xlsx)|*.xlsx"
                If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim strWriter As New IO.StreamWriter(fsFile)
                    With strWriter
                        .BaseStream.Seek(0, IO.SeekOrigin.End)
                        .WriteLine(strBuilder)
                        .Close()
                    End With
                    Diagnostics.Process.Start(svDialog.FileName)
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub
    'S.SHARMA [ 22 JAN 2013 ] -- END

#End Region
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		

			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAttemptsType.Text = Language._Object.getCaption(Me.lblAttemptsType.Name, Me.lblAttemptsType.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Application Event Log")
			Language.setMessage(mstrModuleName, 2, "User Authentication Log")
			Language.setMessage(mstrModuleName, 3, "User Attempts Log")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Search")
			Language.setMessage(mstrModuleName, 6, "Export")
			Language.setMessage(mstrModuleName, 7, "Audit Trail Viewer")
			Language.setMessage(mstrModuleName, 8, "Email Notification Log")
			Language.setMessage(mstrModuleName, 9, "Delete")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
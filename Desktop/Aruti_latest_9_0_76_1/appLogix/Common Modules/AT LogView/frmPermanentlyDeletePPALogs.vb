﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPermanentlyDeletePPALogs

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPermanentlyDeletePPALogs"

#End Region

#Region " Private Methods "

    Public Sub FillCombo()
        Try
            With cboRemoveFrom
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Activity Postings"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Activiy Rates"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Store_User_Log() As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.ACTIVITY_DELETE_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.DELETE
            Dim StrXML_Value As String = ""
            StrXML_Value &= "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf
            StrXML_Value &= "<" & cboRemoveFrom.Name & ">" & CInt(cboRemoveFrom.SelectedIndex) & "</" & cboRemoveFrom.Name & ">" & vbCrLf
            StrXML_Value &= "<" & dtpFromDate.Name & ">" & eZeeDate.convertDate(dtpFromDate.Value) & "</" & dtpFromDate.Name & ">" & vbCrLf
            StrXML_Value &= "<" & dtpTodate.Name & ">" & eZeeDate.convertDate(dtpTodate.Value) & "</" & dtpTodate.Name & ">" & vbCrLf
            StrXML_Value &= "<IP>" & getIP() & "</IP>" & vbCrLf
            StrXML_Value &= "<Host>" & getHostName() & "</Host>" & vbCrLf
            StrXML_Value &= "</" & Me.Name & ">"
            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmPermanentlyDeleteLogs_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call OtherSettings()
            Call FillCombo()
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeletePayPerActivityAuditTrails
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPermanentlyDeleteLogs_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPermanentlyDeleteLogs_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Alert! You are about to delete Pay Per Activity log between the selected date range." & vbCrLf & _
                                    "This will PERMANENTLY DELETE the logs and the deleted log cannot be recovered in any circumstances." & vbCrLf & _
                                   "Do you wish to continue?"), CType(enMsgBoxStyle.Exclamation + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim objPayActivity As New clsPayActivity_Tran
                If objPayActivity.RemoveDataFrom_AuditTrails(CInt(cboRemoveFrom.SelectedIndex), dtpFromDate.Value.Date, dtpTodate.Value.Date) = True Then
                    If Store_User_Log() = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Pay Per Activity log(s) deleted permanently from system successfully."), enMsgBoxStyle.Information)
                    End If
                End If
                objPayActivity = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
            Me.lblRemoveFrom.Text = Language._Object.getCaption(Me.lblRemoveFrom.Name, Me.lblRemoveFrom.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Activity Postings")
            Language.setMessage(mstrModuleName, 2, "Activiy Rates")
            Language.setMessage(mstrModuleName, 3, "Alert! You are about to delete Pay Per Activity log between the selected date range." & vbCrLf & _
                                             "This will PERMANENTLY DELETE the logs and the deleted log cannot be recovered in any circumstances." & vbCrLf & _
                                            "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 4, "Pay Per Activity log(s) deleted permanently from system successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
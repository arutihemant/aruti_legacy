Option Strict On

Imports eZeeCommonLib

Public Class frmProductSupport
    Private mstrModuleName As String = "frmProductSupport"

#Region " Form "

    Private Sub frmProductSupport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmProductSupport_KeyPress", "frmProductSupport")
        End Try
    End Sub

    'Private Sub frmProductSupport_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._RightToLeft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "frmProductSupport_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub


    Private Sub frmProductSupport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me)
            'Call OtherSettings()

            'Select Case eZeeApplication._AppType
            '    Case enApplication.eZeePOS
            '        Me.Icon = ecore32.core.BurrpIcon
            '        pnlBoxImage.BackgroundImage = ecore32.core.POSProductImage
            '    Case enApplication.eZeePOSConfigartion
            '        Me.Icon = ecore32.core.BackOfficeIcon
            '        pnlBoxImage.BackgroundImage = ecore32.core.POSProductImage
            '    Case enApplication.eZeeFD
            '        Me.Icon = ecore32.core.FDIcon
            '        pnlBoxImage.BackgroundImage = ecore32.core.FDProductImage
            '    Case Else
            '        Me.Icon = ecore32.core.FDCIcon
            '        pnlBoxImage.BackgroundImage = ecore32.core.FDProductImage
            'End Select

            'Dim objReseller As New clsReseller
            'Dim strOtherInfo As String = ""
            'Dim strAddress1 As String = ""
            'Dim strAddress2 As String = ""

            'objlblOEMCompany.Text = objReseller._OEMCompany_Name
            'strOtherInfo += IIf(objReseller._OEMContact_person <> "", objReseller._OEMContact_person & vbCrLf, "").ToString
            'strAddress1 += IIf(objReseller._OEMAddress1 <> "", objReseller._OEMAddress1 & "," & vbCrLf, "").ToString
            'strAddress2 += IIf(objReseller._OEMAddress2 <> "", objReseller._OEMAddress2 & ",", "").ToString
            'strAddress2 += IIf(objReseller._OEMCity <> "", " " & objReseller._OEMCity & " - ", "").ToString
            'strAddress2 += IIf(objReseller._OEMZipcode <> "", " " & objReseller._OEMZipcode & ",", "").ToString
            'strAddress2 += IIf(objReseller._OEMState <> "", " " & objReseller._OEMState & ",", "").ToString
            'strAddress2 += IIf(objReseller._OEMCountry <> "", " " & objReseller._OEMCountry & ".", "").ToString
            'objlblOEMOtherInfo.Text = strOtherInfo & strAddress1 & strAddress2
            'objlblOEMOtherInfo.Text += IIf(objReseller._Website <> "", vbCrLf & objReseller._Website, "").ToString

            'objlblOEmSalesPhone.Text = objReseller._OEMSales_phone
            'objlblOEMSalesEmail.Text = objReseller._OEMSales_email

            'objlblOEMSupportPhone.Text = objReseller._OEMSupport_phone
            'objlblOEMSupportEmail.Text = objReseller._OEMSupport_email

            'If objReseller._HasReseller = False Then
            '    pnlResellerInfo.Visible = False
            'Else
            '    pnlResellerInfo.Visible = True
            '    objlblCompany.Text = objReseller._Company_Name
            '    strOtherInfo += IIf(objReseller._Contact_person <> "", objReseller._Contact_person & vbCrLf, "").ToString
            '    strAddress1 += IIf(objReseller._Address1 <> "", objReseller._Address1 & "," & vbCrLf, "").ToString
            '    strAddress2 += IIf(objReseller._Address2 <> "", objReseller._Address2 & ",", "").ToString
            '    strAddress2 += IIf(objReseller._City <> "", " " & objReseller._City & " - ", "").ToString
            '    strAddress2 += IIf(objReseller._Zipcode <> "", " " & objReseller._Zipcode & ",", "").ToString
            '    strAddress2 += IIf(objReseller._State <> "", " " & objReseller._State & ",", "").ToString
            '    strAddress2 += IIf(objReseller._Country <> "", " " & objReseller._Country & ".", "").ToString
            '    objlblOtherInfo.Text = strOtherInfo & strAddress1 & strAddress2
            '    objlblOtherInfo.Text += IIf(objReseller._Website <> "", vbCrLf & objReseller._Website, "").ToString

            '    objlblSalesPhone.Text = objReseller._Sales_phone
            '    objlblSalesEmail.Text = objReseller._Sales_email

            '    objlblSupportPhone.Text = objReseller._Support_phone
            '    objlblSupportEmail.Text = objReseller._Support_email
            'End If

        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region " Buttons "
    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.Close()
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOk.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbServiceAndSupport.Text = Language._Object.getCaption(Me.gbServiceAndSupport.Name, Me.gbServiceAndSupport.Text)
			Me.Label6.Text = Language._Object.getCaption(Me.Label6.Name, Me.Label6.Text)
			Me.Label5.Text = Language._Object.getCaption(Me.Label5.Name, Me.Label5.Text)
			Me.Label4.Text = Language._Object.getCaption(Me.Label4.Name, Me.Label4.Text)
			Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.lblMsg1.Text = Language._Object.getCaption(Me.lblMsg1.Name, Me.lblMsg1.Text)
			Me.LinkLabel1.Text = Language._Object.getCaption(Me.LinkLabel1.Name, Me.LinkLabel1.Text)
			Me.Label7.Text = Language._Object.getCaption(Me.Label7.Name, Me.Label7.Text)
			Me.Label8.Text = Language._Object.getCaption(Me.Label8.Name, Me.Label8.Text)
			Me.Label9.Text = Language._Object.getCaption(Me.Label9.Name, Me.Label9.Text)
			Me.Label11.Text = Language._Object.getCaption(Me.Label11.Name, Me.Label11.Text)
			Me.Label10.Text = Language._Object.getCaption(Me.Label10.Name, Me.Label10.Text)
			Me.LinkLabel2.Text = Language._Object.getCaption(Me.LinkLabel2.Name, Me.LinkLabel2.Text)
			Me.Label16.Text = Language._Object.getCaption(Me.Label16.Name, Me.Label16.Text)
			Me.Label15.Text = Language._Object.getCaption(Me.Label15.Name, Me.Label15.Text)
			Me.Label14.Text = Language._Object.getCaption(Me.Label14.Name, Me.Label14.Text)
			Me.Label13.Text = Language._Object.getCaption(Me.Label13.Name, Me.Label13.Text)
			Me.Label12.Text = Language._Object.getCaption(Me.Label12.Name, Me.Label12.Text)
			Me.lblMainBranch.Text = Language._Object.getCaption(Me.lblMainBranch.Name, Me.lblMainBranch.Text)
			Me.lblOEMSalesEmailTitle.Text = Language._Object.getCaption(Me.lblOEMSalesEmailTitle.Name, Me.lblOEMSalesEmailTitle.Text)
			Me.lblOEMSalesPhoneTitle.Text = Language._Object.getCaption(Me.lblOEMSalesPhoneTitle.Name, Me.lblOEMSalesPhoneTitle.Text)
			Me.lblOEMSalesInfo.Text = Language._Object.getCaption(Me.lblOEMSalesInfo.Name, Me.lblOEMSalesInfo.Text)
			Me.lblOEMSupportInfo.Text = Language._Object.getCaption(Me.lblOEMSupportInfo.Name, Me.lblOEMSupportInfo.Text)
			Me.lblEmailTitle.Text = Language._Object.getCaption(Me.lblEmailTitle.Name, Me.lblEmailTitle.Text)
			Me.lblPhoneTitle.Text = Language._Object.getCaption(Me.lblPhoneTitle.Name, Me.lblPhoneTitle.Text)
			Me.lblSupportInfo.Text = Language._Object.getCaption(Me.lblSupportInfo.Name, Me.lblSupportInfo.Text)
			Me.lblSalesInfo.Text = Language._Object.getCaption(Me.lblSalesInfo.Name, Me.lblSalesInfo.Text)
			Me.lblLocalSalesANDSupport.Text = Language._Object.getCaption(Me.lblLocalSalesANDSupport.Name, Me.lblLocalSalesANDSupport.Text)
			Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
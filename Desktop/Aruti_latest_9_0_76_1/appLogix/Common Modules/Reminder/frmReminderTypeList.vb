﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmReminderTypeList

#Region " Private Variables "

    Private objRemiderType As clsReminderType
    Private ReadOnly mstrModuleName As String = "frmReminderTypeList"

#End Region

#Region " Private Functions "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsList = objRemiderType.GetList("RemiderType")

            lvRemiderType.Items.Clear()

            For Each dtRow As DataRow In dsList.Tables("RemiderType").Rows

                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("alias").ToString
                lvItem.SubItems.Add(dtRow.Item("code").ToString)
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("remindertypeunkid")

                lvRemiderType.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvRemiderType.Items.Count > 15 Then
                colhReminderType.Width = colhReminderType.Width - 20
            Else
                colhReminderType.Width = colhReminderType.Width
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddReminderType
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditReminderType
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteReminderType
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 29 OCT 2012 ] -- END

#End Region

#Region " Forms's Events "

    Private Sub frmReminderTypeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvRemiderType.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderTypeList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderTypeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderTypeList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderTypeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objRemiderType = New clsReminderType
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call FillList()

            If lvRemiderType.Items.Count > 0 Then lvRemiderType.Items(0).Selected = True
            lvRemiderType.Select()

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 29 OCT 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderTypeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderTypeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRemiderType = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderTypeList_FormClosed", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsReminderType.SetMessages()
            objfrm._Other_ModuleNames = "clsReminderType"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvRemiderType.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Reminder Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvRemiderType.Select()
            'S.SANDEEP [ 30 May 2011 ] -- START
            Exit Sub
            'S.SANDEEP [ 30 May 2011 ] -- END 
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvRemiderType.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Reminder Type?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objRemiderType.Delete(CInt(lvRemiderType.SelectedItems(0).Tag))
                lvRemiderType.SelectedItems(0).Remove()

                If lvRemiderType.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvRemiderType.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvRemiderType.Items.Count - 1
                    lvRemiderType.Items(intSelectedIndex).Selected = True
                    lvRemiderType.EnsureVisible(intSelectedIndex)
                ElseIf lvRemiderType.Items.Count <> 0 Then
                    lvRemiderType.Items(intSelectedIndex).Selected = True
                    lvRemiderType.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvRemiderType.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvRemiderType.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Discipline Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvRemiderType.Select()
            Exit Sub
        End If

        Dim frm As New frmReminderType_AddEdit

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvRemiderType.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 



            If frm.displayDialog(CInt(lvRemiderType.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If

            frm = Nothing

            lvRemiderType.Items(intSelectedIndex).Selected = True
            lvRemiderType.EnsureVisible(intSelectedIndex)
            lvRemiderType.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmReminderType_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

        
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhAlias.Text = Language._Object.getCaption(CStr(Me.colhAlias.Tag), Me.colhAlias.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhReminderType.Text = Language._Object.getCaption(CStr(Me.colhReminderType.Tag), Me.colhReminderType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Reminder Type from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Reminder Type?")
            Language.setMessage(mstrModuleName, 3, "Please select Discipline Type from the list to perform further operation.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
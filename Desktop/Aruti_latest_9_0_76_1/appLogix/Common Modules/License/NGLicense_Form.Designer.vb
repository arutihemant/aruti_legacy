<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNGLicense
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNGLicense))
        Me.lnkEmail = New System.Windows.Forms.LinkLabel
        Me.lblMsg = New System.Windows.Forms.Label
        Me.ln1 = New eZee.Common.eZeeLine
        Me.lblPropertyName = New System.Windows.Forms.Label
        Me.lblMachineID = New System.Windows.Forms.Label
        Me.lblSessionId = New System.Windows.Forms.Label
        Me.txtPropertyName = New eZee.TextBox.AlphanumericTextBox
        Me.txtMachineID = New eZee.TextBox.AlphanumericTextBox
        Me.txtSessionID = New eZee.TextBox.AlphanumericTextBox
        Me.lblMsg2 = New System.Windows.Forms.Label
        Me.ln2 = New eZee.Common.eZeeLine
        Me.lblNoofRoomKey = New System.Windows.Forms.Label
        Me.lblLicSerialNo = New System.Windows.Forms.Label
        Me.txtNoofRoomKey = New eZee.TextBox.AlphanumericTextBox
        Me.txtLicSerialNo = New eZee.TextBox.AlphanumericTextBox
        Me.gbLicInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkCopyToClipBoard = New System.Windows.Forms.LinkLabel
        Me.gbRegistration = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnExtend = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnActivate = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbLicInfo.SuspendLayout()
        Me.gbRegistration.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'lnkEmail
        '
        Me.lnkEmail.AutoSize = True
        Me.lnkEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lnkEmail.Location = New System.Drawing.Point(227, 168)
        Me.lnkEmail.Name = "lnkEmail"
        Me.lnkEmail.Size = New System.Drawing.Size(46, 13)
        Me.lnkEmail.TabIndex = 10
        Me.lnkEmail.TabStop = True
        Me.lnkEmail.Text = "Email Us"
        Me.lnkEmail.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lnkEmail.Visible = False
        '
        'lblMsg
        '
        Me.lblMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblMsg.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblMsg.Location = New System.Drawing.Point(8, 127)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(269, 41)
        Me.lblMsg.TabIndex = 9
        Me.lblMsg.Text = "Note : Please copy and paste the above information in an email and submit it to u" & _
            "s."
        '
        'ln1
        '
        Me.ln1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.ln1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.ln1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.ln1.Location = New System.Drawing.Point(7, 110)
        Me.ln1.Name = "ln1"
        Me.ln1.Size = New System.Drawing.Size(266, 17)
        Me.ln1.TabIndex = 8
        Me.ln1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPropertyName
        '
        Me.lblPropertyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblPropertyName.Location = New System.Drawing.Point(9, 90)
        Me.lblPropertyName.Name = "lblPropertyName"
        Me.lblPropertyName.Size = New System.Drawing.Size(97, 13)
        Me.lblPropertyName.TabIndex = 6
        Me.lblPropertyName.Text = "Property Name"
        '
        'lblMachineID
        '
        Me.lblMachineID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblMachineID.Location = New System.Drawing.Point(8, 63)
        Me.lblMachineID.Name = "lblMachineID"
        Me.lblMachineID.Size = New System.Drawing.Size(97, 13)
        Me.lblMachineID.TabIndex = 2
        Me.lblMachineID.Text = "Machine ID"
        '
        'lblSessionId
        '
        Me.lblSessionId.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblSessionId.Location = New System.Drawing.Point(8, 36)
        Me.lblSessionId.Name = "lblSessionId"
        Me.lblSessionId.Size = New System.Drawing.Size(97, 13)
        Me.lblSessionId.TabIndex = 0
        Me.lblSessionId.Text = "Session ID"
        '
        'txtPropertyName
        '
        Me.txtPropertyName.Flags = 0
        Me.txtPropertyName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtPropertyName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPropertyName.Location = New System.Drawing.Point(109, 86)
        Me.txtPropertyName.Name = "txtPropertyName"
        Me.txtPropertyName.ReadOnly = True
        Me.txtPropertyName.Size = New System.Drawing.Size(164, 21)
        Me.txtPropertyName.TabIndex = 7
        Me.txtPropertyName.Tag = "name"
        '
        'txtMachineID
        '
        Me.txtMachineID.Flags = 0
        Me.txtMachineID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtMachineID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMachineID.Location = New System.Drawing.Point(109, 59)
        Me.txtMachineID.Name = "txtMachineID"
        Me.txtMachineID.ReadOnly = True
        Me.txtMachineID.Size = New System.Drawing.Size(164, 21)
        Me.txtMachineID.TabIndex = 3
        Me.txtMachineID.Tag = "name"
        '
        'txtSessionID
        '
        Me.txtSessionID.Flags = 0
        Me.txtSessionID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtSessionID.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSessionID.Location = New System.Drawing.Point(109, 32)
        Me.txtSessionID.Name = "txtSessionID"
        Me.txtSessionID.ReadOnly = True
        Me.txtSessionID.Size = New System.Drawing.Size(164, 21)
        Me.txtSessionID.TabIndex = 1
        Me.txtSessionID.Tag = "name"
        '
        'lblMsg2
        '
        Me.lblMsg2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblMsg2.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblMsg2.Location = New System.Drawing.Point(9, 98)
        Me.lblMsg2.Name = "lblMsg2"
        Me.lblMsg2.Size = New System.Drawing.Size(269, 42)
        Me.lblMsg2.TabIndex = 5
        Me.lblMsg2.Text = "Note : Please make sure to type the license key exactly as provided. Upon unsucce" & _
            "ssful attempt Session ID will change."
        '
        'ln2
        '
        Me.ln2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.ln2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.ln2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.ln2.Location = New System.Drawing.Point(9, 81)
        Me.ln2.Name = "ln2"
        Me.ln2.Size = New System.Drawing.Size(266, 17)
        Me.ln2.TabIndex = 4
        Me.ln2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNoofRoomKey
        '
        Me.lblNoofRoomKey.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblNoofRoomKey.Location = New System.Drawing.Point(8, 62)
        Me.lblNoofRoomKey.Name = "lblNoofRoomKey"
        Me.lblNoofRoomKey.Size = New System.Drawing.Size(97, 13)
        Me.lblNoofRoomKey.TabIndex = 2
        Me.lblNoofRoomKey.Text = "Activation Code"
        '
        'lblLicSerialNo
        '
        Me.lblLicSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblLicSerialNo.Location = New System.Drawing.Point(8, 36)
        Me.lblLicSerialNo.Name = "lblLicSerialNo"
        Me.lblLicSerialNo.Size = New System.Drawing.Size(97, 13)
        Me.lblLicSerialNo.TabIndex = 0
        Me.lblLicSerialNo.Text = "License Code"
        '
        'txtNoofRoomKey
        '
        Me.txtNoofRoomKey.Flags = 0
        Me.txtNoofRoomKey.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtNoofRoomKey.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtNoofRoomKey.Location = New System.Drawing.Point(111, 58)
        Me.txtNoofRoomKey.Name = "txtNoofRoomKey"
        Me.txtNoofRoomKey.Size = New System.Drawing.Size(164, 21)
        Me.txtNoofRoomKey.TabIndex = 3
        Me.txtNoofRoomKey.Tag = "name"
        '
        'txtLicSerialNo
        '
        Me.txtLicSerialNo.Flags = 0
        Me.txtLicSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.txtLicSerialNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLicSerialNo.Location = New System.Drawing.Point(111, 32)
        Me.txtLicSerialNo.Name = "txtLicSerialNo"
        Me.txtLicSerialNo.Size = New System.Drawing.Size(164, 21)
        Me.txtLicSerialNo.TabIndex = 1
        Me.txtLicSerialNo.Tag = "name"
        '
        'gbLicInfo
        '
        Me.gbLicInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLicInfo.Checked = True
        Me.gbLicInfo.CollapseAllExceptThis = False
        Me.gbLicInfo.CollapsedHoverImage = Nothing
        Me.gbLicInfo.CollapsedNormalImage = Nothing
        Me.gbLicInfo.CollapsedPressedImage = Nothing
        Me.gbLicInfo.CollapseOnLoad = False
        Me.gbLicInfo.Controls.Add(Me.lnkCopyToClipBoard)
        Me.gbLicInfo.Controls.Add(Me.lnkEmail)
        Me.gbLicInfo.Controls.Add(Me.lblMsg)
        Me.gbLicInfo.Controls.Add(Me.lblSessionId)
        Me.gbLicInfo.Controls.Add(Me.ln1)
        Me.gbLicInfo.Controls.Add(Me.txtSessionID)
        Me.gbLicInfo.Controls.Add(Me.lblPropertyName)
        Me.gbLicInfo.Controls.Add(Me.txtMachineID)
        Me.gbLicInfo.Controls.Add(Me.txtPropertyName)
        Me.gbLicInfo.Controls.Add(Me.lblMachineID)
        Me.gbLicInfo.ExpandedHoverImage = Nothing
        Me.gbLicInfo.ExpandedNormalImage = Nothing
        Me.gbLicInfo.ExpandedPressedImage = Nothing
        Me.gbLicInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLicInfo.GradientColor = System.Drawing.Color.Transparent
        Me.gbLicInfo.HeaderHeight = 25
        Me.gbLicInfo.HeaderMessage = ""
        Me.gbLicInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLicInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLicInfo.HeightOnCollapse = 0
        Me.gbLicInfo.LeftTextSpace = 0
        Me.gbLicInfo.Location = New System.Drawing.Point(9, 9)
        Me.gbLicInfo.Name = "gbLicInfo"
        Me.gbLicInfo.OpenHeight = 218
        Me.gbLicInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbLicInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLicInfo.ShowBorder = True
        Me.gbLicInfo.ShowCheckBox = False
        Me.gbLicInfo.ShowCollapseButton = False
        Me.gbLicInfo.ShowDefaultBorderColor = True
        Me.gbLicInfo.ShowDownButton = False
        Me.gbLicInfo.ShowHeader = True
        Me.gbLicInfo.Size = New System.Drawing.Size(284, 188)
        Me.gbLicInfo.TabIndex = 0
        Me.gbLicInfo.Temp = 0
        Me.gbLicInfo.Text = "License information"
        Me.gbLicInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkCopyToClipBoard
        '
        Me.lnkCopyToClipBoard.BackColor = System.Drawing.Color.Transparent
        Me.lnkCopyToClipBoard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lnkCopyToClipBoard.Location = New System.Drawing.Point(129, 5)
        Me.lnkCopyToClipBoard.Name = "lnkCopyToClipBoard"
        Me.lnkCopyToClipBoard.Size = New System.Drawing.Size(149, 13)
        Me.lnkCopyToClipBoard.TabIndex = 12
        Me.lnkCopyToClipBoard.TabStop = True
        Me.lnkCopyToClipBoard.Text = "Copy to Clip board"
        Me.lnkCopyToClipBoard.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'gbRegistration
        '
        Me.gbRegistration.BorderColor = System.Drawing.Color.Black
        Me.gbRegistration.Checked = False
        Me.gbRegistration.CollapseAllExceptThis = False
        Me.gbRegistration.CollapsedHoverImage = Nothing
        Me.gbRegistration.CollapsedNormalImage = Nothing
        Me.gbRegistration.CollapsedPressedImage = Nothing
        Me.gbRegistration.CollapseOnLoad = False
        Me.gbRegistration.Controls.Add(Me.lblMsg2)
        Me.gbRegistration.Controls.Add(Me.ln2)
        Me.gbRegistration.Controls.Add(Me.lblLicSerialNo)
        Me.gbRegistration.Controls.Add(Me.lblNoofRoomKey)
        Me.gbRegistration.Controls.Add(Me.txtLicSerialNo)
        Me.gbRegistration.Controls.Add(Me.txtNoofRoomKey)
        Me.gbRegistration.ExpandedHoverImage = Nothing
        Me.gbRegistration.ExpandedNormalImage = Nothing
        Me.gbRegistration.ExpandedPressedImage = Nothing
        Me.gbRegistration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRegistration.GradientColor = System.Drawing.Color.Transparent
        Me.gbRegistration.HeaderHeight = 25
        Me.gbRegistration.HeaderMessage = ""
        Me.gbRegistration.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRegistration.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRegistration.HeightOnCollapse = 0
        Me.gbRegistration.LeftTextSpace = 0
        Me.gbRegistration.Location = New System.Drawing.Point(9, 203)
        Me.gbRegistration.Name = "gbRegistration"
        Me.gbRegistration.OpenHeight = 149
        Me.gbRegistration.Padding = New System.Windows.Forms.Padding(3)
        Me.gbRegistration.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRegistration.ShowBorder = True
        Me.gbRegistration.ShowCheckBox = False
        Me.gbRegistration.ShowCollapseButton = False
        Me.gbRegistration.ShowDefaultBorderColor = True
        Me.gbRegistration.ShowDownButton = False
        Me.gbRegistration.ShowHeader = True
        Me.gbRegistration.Size = New System.Drawing.Size(284, 149)
        Me.gbRegistration.TabIndex = 1
        Me.gbRegistration.Temp = 0
        Me.gbRegistration.Text = "Registration"
        Me.gbRegistration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnExtend)
        Me.objefFormFooter.Controls.Add(Me.btnActivate)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 362)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(301, 55)
        Me.objefFormFooter.TabIndex = 2
        '
        'btnExtend
        '
        Me.btnExtend.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExtend.BackColor = System.Drawing.Color.White
        Me.btnExtend.BackgroundImage = CType(resources.GetObject("btnExtend.BackgroundImage"), System.Drawing.Image)
        Me.btnExtend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExtend.BorderColor = System.Drawing.Color.Empty
        Me.btnExtend.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExtend.FlatAppearance.BorderSize = 0
        Me.btnExtend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExtend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExtend.ForeColor = System.Drawing.Color.Black
        Me.btnExtend.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExtend.GradientForeColor = System.Drawing.Color.Black
        Me.btnExtend.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExtend.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExtend.Location = New System.Drawing.Point(6, 12)
        Me.btnExtend.Name = "btnExtend"
        Me.btnExtend.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExtend.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExtend.Size = New System.Drawing.Size(90, 30)
        Me.btnExtend.TabIndex = 0
        Me.btnExtend.Text = "&Extend"
        Me.btnExtend.UseVisualStyleBackColor = False
        Me.btnExtend.Visible = False
        '
        'btnActivate
        '
        Me.btnActivate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActivate.BackColor = System.Drawing.Color.White
        Me.btnActivate.BackgroundImage = CType(resources.GetObject("btnActivate.BackgroundImage"), System.Drawing.Image)
        Me.btnActivate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActivate.BorderColor = System.Drawing.Color.Empty
        Me.btnActivate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActivate.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.btnActivate.FlatAppearance.BorderSize = 0
        Me.btnActivate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActivate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActivate.ForeColor = System.Drawing.Color.Black
        Me.btnActivate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActivate.GradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Location = New System.Drawing.Point(103, 12)
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Size = New System.Drawing.Size(90, 30)
        Me.btnActivate.TabIndex = 1
        Me.btnActivate.Text = "&Activate"
        Me.btnActivate.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(200, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'frmNGLicense
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(301, 417)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.gbRegistration)
        Me.Controls.Add(Me.gbLicInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNGLicense"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registration"
        Me.gbLicInfo.ResumeLayout(False)
        Me.gbLicInfo.PerformLayout()
        Me.gbRegistration.ResumeLayout(False)
        Me.gbRegistration.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtMachineID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSessionID As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPropertyName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMachineID As System.Windows.Forms.Label
    Friend WithEvents lblSessionId As System.Windows.Forms.Label
    Friend WithEvents lblPropertyName As System.Windows.Forms.Label
    Friend WithEvents ln1 As eZee.Common.eZeeLine
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents lblMsg2 As System.Windows.Forms.Label
    Friend WithEvents ln2 As eZee.Common.eZeeLine
    Friend WithEvents lblNoofRoomKey As System.Windows.Forms.Label
    Friend WithEvents lblLicSerialNo As System.Windows.Forms.Label
    Friend WithEvents txtNoofRoomKey As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtLicSerialNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkEmail As System.Windows.Forms.LinkLabel
    Friend WithEvents gbLicInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbRegistration As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnActivate As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnExtend As eZee.Common.eZeeLightButton
    Friend WithEvents lnkCopyToClipBoard As System.Windows.Forms.LinkLabel
End Class

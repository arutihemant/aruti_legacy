﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmScanOrAttachmentInfo
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmScanOrAttachmentInfo))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.dtpUploadDate = New System.Windows.Forms.DateTimePicker
        Me.lblUploadDate = New System.Windows.Forms.Label
        Me.lblSelection = New System.Windows.Forms.Label
        Me.cboAssocatedValue = New System.Windows.Forms.ComboBox
        Me.lvScanAttachment = New eZee.Common.eZeeListView(Me.components)
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhDocument = New System.Windows.Forms.ColumnHeader
        Me.colhFileName = New System.Windows.Forms.ColumnHeader
        Me.colhValue = New System.Windows.Forms.ColumnHeader
        Me.objcolhDocType = New System.Windows.Forms.ColumnHeader
        Me.objcolhGuid = New System.Windows.Forms.ColumnHeader
        Me.objcolhEmpId = New System.Windows.Forms.ColumnHeader
        Me.objcolhAppId = New System.Windows.Forms.ColumnHeader
        Me.objcolhFullPath = New System.Windows.Forms.ColumnHeader
        Me.objcolhTransId = New System.Windows.Forms.ColumnHeader
        Me.objcolhDocumentId = New System.Windows.Forms.ColumnHeader
        Me.btnRemove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboDocument = New System.Windows.Forms.ComboBox
        Me.btnScan = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblDocType = New System.Windows.Forms.Label
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.lblDocument = New System.Windows.Forms.Label
        Me.objelSep = New eZee.Common.eZeeLine
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnPreview = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.pnlMainInfo.SuspendLayout()
        Me.pnlEmployee.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.pnlEmployee)
        Me.pnlMainInfo.Controls.Add(Me.dtpUploadDate)
        Me.pnlMainInfo.Controls.Add(Me.lblUploadDate)
        Me.pnlMainInfo.Controls.Add(Me.lblSelection)
        Me.pnlMainInfo.Controls.Add(Me.cboAssocatedValue)
        Me.pnlMainInfo.Controls.Add(Me.lvScanAttachment)
        Me.pnlMainInfo.Controls.Add(Me.btnRemove)
        Me.pnlMainInfo.Controls.Add(Me.btnAdd)
        Me.pnlMainInfo.Controls.Add(Me.cboDocument)
        Me.pnlMainInfo.Controls.Add(Me.btnScan)
        Me.pnlMainInfo.Controls.Add(Me.lblDocType)
        Me.pnlMainInfo.Controls.Add(Me.cboDocumentType)
        Me.pnlMainInfo.Controls.Add(Me.lblDocument)
        Me.pnlMainInfo.Controls.Add(Me.objelSep)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.btnEdit)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(765, 442)
        Me.pnlMainInfo.TabIndex = 0
        '
        'pnlEmployee
        '
        Me.pnlEmployee.Controls.Add(Me.objlblCaption)
        Me.pnlEmployee.Controls.Add(Me.cboEmployee)
        Me.pnlEmployee.Controls.Add(Me.objbtnSearch)
        Me.pnlEmployee.Location = New System.Drawing.Point(6, 60)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(319, 27)
        Me.pnlEmployee.TabIndex = 152
        '
        'objlblCaption
        '
        Me.objlblCaption.Location = New System.Drawing.Point(2, 6)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(87, 15)
        Me.objlblCaption.TabIndex = 2
        Me.objlblCaption.Text = "#Value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 225
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(99, 3)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(195, 21)
        Me.cboEmployee.TabIndex = 131
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(296, 3)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 136
        '
        'dtpUploadDate
        '
        Me.dtpUploadDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpUploadDate.Location = New System.Drawing.Point(650, 64)
        Me.dtpUploadDate.Name = "dtpUploadDate"
        Me.dtpUploadDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpUploadDate.TabIndex = 150
        '
        'lblUploadDate
        '
        Me.lblUploadDate.Location = New System.Drawing.Point(599, 67)
        Me.lblUploadDate.Name = "lblUploadDate"
        Me.lblUploadDate.Size = New System.Drawing.Size(47, 15)
        Me.lblUploadDate.TabIndex = 149
        Me.lblUploadDate.Text = "Date"
        Me.lblUploadDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSelection
        '
        Me.lblSelection.Location = New System.Drawing.Point(333, 94)
        Me.lblSelection.Name = "lblSelection"
        Me.lblSelection.Size = New System.Drawing.Size(108, 15)
        Me.lblSelection.TabIndex = 148
        Me.lblSelection.Text = "#Value"
        Me.lblSelection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAssocatedValue
        '
        Me.cboAssocatedValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssocatedValue.DropDownWidth = 320
        Me.cboAssocatedValue.FormattingEnabled = True
        Me.cboAssocatedValue.Location = New System.Drawing.Point(447, 91)
        Me.cboAssocatedValue.Name = "cboAssocatedValue"
        Me.cboAssocatedValue.Size = New System.Drawing.Size(310, 21)
        Me.cboAssocatedValue.TabIndex = 147
        '
        'lvScanAttachment
        '
        Me.lvScanAttachment.BackColorOnChecked = False
        Me.lvScanAttachment.ColumnHeaders = Nothing
        Me.lvScanAttachment.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName, Me.colhDocument, Me.colhFileName, Me.colhValue, Me.objcolhDocType, Me.objcolhGuid, Me.objcolhEmpId, Me.objcolhAppId, Me.objcolhFullPath, Me.objcolhTransId, Me.objcolhDocumentId})
        Me.lvScanAttachment.CompulsoryColumns = ""
        Me.lvScanAttachment.FullRowSelect = True
        Me.lvScanAttachment.GridLines = True
        Me.lvScanAttachment.GroupingColumn = Nothing
        Me.lvScanAttachment.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvScanAttachment.HideSelection = False
        Me.lvScanAttachment.Location = New System.Drawing.Point(11, 163)
        Me.lvScanAttachment.MinColumnWidth = 50
        Me.lvScanAttachment.MultiSelect = False
        Me.lvScanAttachment.Name = "lvScanAttachment"
        Me.lvScanAttachment.OptionalColumns = ""
        Me.lvScanAttachment.ShowMoreItem = False
        Me.lvScanAttachment.ShowSaveItem = False
        Me.lvScanAttachment.ShowSelectAll = True
        Me.lvScanAttachment.ShowSizeAllColumnsToFit = True
        Me.lvScanAttachment.Size = New System.Drawing.Size(746, 220)
        Me.lvScanAttachment.Sortable = True
        Me.lvScanAttachment.TabIndex = 146
        Me.lvScanAttachment.UseCompatibleStateImageBehavior = False
        Me.lvScanAttachment.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 100
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Employee/Applicant"
        Me.colhName.Width = 180
        '
        'colhDocument
        '
        Me.colhDocument.Tag = "colhDocument"
        Me.colhDocument.Text = "Document"
        Me.colhDocument.Width = 160
        '
        'colhFileName
        '
        Me.colhFileName.Tag = "colhFileName"
        Me.colhFileName.Text = "Filename"
        Me.colhFileName.Width = 180
        '
        'colhValue
        '
        Me.colhValue.Tag = "colhValue"
        Me.colhValue.Text = "Associated Value"
        Me.colhValue.Width = 115
        '
        'objcolhDocType
        '
        Me.objcolhDocType.Tag = "objcolhDocType"
        Me.objcolhDocType.Text = ""
        Me.objcolhDocType.Width = 0
        '
        'objcolhGuid
        '
        Me.objcolhGuid.Tag = "objcolhGuid"
        Me.objcolhGuid.Text = ""
        Me.objcolhGuid.Width = 0
        '
        'objcolhEmpId
        '
        Me.objcolhEmpId.Tag = "objcolhEmpId"
        Me.objcolhEmpId.Text = ""
        Me.objcolhEmpId.Width = 0
        '
        'objcolhAppId
        '
        Me.objcolhAppId.Tag = "objcolhAppId"
        Me.objcolhAppId.Text = ""
        Me.objcolhAppId.Width = 0
        '
        'objcolhFullPath
        '
        Me.objcolhFullPath.Tag = "objcolhFullPath"
        Me.objcolhFullPath.Text = ""
        Me.objcolhFullPath.Width = 0
        '
        'objcolhTransId
        '
        Me.objcolhTransId.Tag = "objcolhTransId"
        Me.objcolhTransId.Text = ""
        Me.objcolhTransId.Width = 0
        '
        'objcolhDocumentId
        '
        Me.objcolhDocumentId.Tag = "objcolhDocumentId"
        Me.objcolhDocumentId.Text = ""
        Me.objcolhDocumentId.Width = 0
        '
        'btnRemove
        '
        Me.btnRemove.BackColor = System.Drawing.Color.White
        Me.btnRemove.BackgroundImage = CType(resources.GetObject("btnRemove.BackgroundImage"), System.Drawing.Image)
        Me.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRemove.BorderColor = System.Drawing.Color.Empty
        Me.btnRemove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRemove.FlatAppearance.BorderSize = 0
        Me.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemove.ForeColor = System.Drawing.Color.Black
        Me.btnRemove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRemove.GradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Location = New System.Drawing.Point(660, 127)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRemove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRemove.Size = New System.Drawing.Size(97, 30)
        Me.btnRemove.TabIndex = 138
        Me.btnRemove.Text = "&Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(558, 127)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 137
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'cboDocument
        '
        Me.cboDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocument.DropDownWidth = 225
        Me.cboDocument.FormattingEnabled = True
        Me.cboDocument.Location = New System.Drawing.Point(105, 91)
        Me.cboDocument.Name = "cboDocument"
        Me.cboDocument.Size = New System.Drawing.Size(195, 21)
        Me.cboDocument.TabIndex = 135
        '
        'btnScan
        '
        Me.btnScan.BackColor = System.Drawing.Color.White
        Me.btnScan.BackgroundImage = CType(resources.GetObject("btnScan.BackgroundImage"), System.Drawing.Image)
        Me.btnScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnScan.BorderColor = System.Drawing.Color.Empty
        Me.btnScan.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnScan.FlatAppearance.BorderSize = 0
        Me.btnScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScan.ForeColor = System.Drawing.Color.Black
        Me.btnScan.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnScan.GradientForeColor = System.Drawing.Color.Black
        Me.btnScan.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScan.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnScan.Location = New System.Drawing.Point(12, 127)
        Me.btnScan.Name = "btnScan"
        Me.btnScan.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnScan.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnScan.Size = New System.Drawing.Size(97, 30)
        Me.btnScan.TabIndex = 125
        Me.btnScan.Text = "Sca&n"
        Me.btnScan.UseVisualStyleBackColor = True
        '
        'lblDocType
        '
        Me.lblDocType.Location = New System.Drawing.Point(333, 67)
        Me.lblDocType.Name = "lblDocType"
        Me.lblDocType.Size = New System.Drawing.Size(108, 15)
        Me.lblDocType.TabIndex = 134
        Me.lblDocType.Text = "Document Type"
        Me.lblDocType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.DropDownWidth = 225
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(447, 64)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(148, 21)
        Me.cboDocumentType.TabIndex = 133
        '
        'lblDocument
        '
        Me.lblDocument.Location = New System.Drawing.Point(8, 94)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(93, 15)
        Me.lblDocument.TabIndex = 132
        Me.lblDocument.Text = "Document"
        Me.lblDocument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelSep
        '
        Me.objelSep.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelSep.Location = New System.Drawing.Point(12, 115)
        Me.objelSep.Name = "objelSep"
        Me.objelSep.Size = New System.Drawing.Size(741, 9)
        Me.objelSep.TabIndex = 130
        Me.objelSep.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnPreview)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 387)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(765, 55)
        Me.objFooter.TabIndex = 129
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.BackColor = System.Drawing.Color.White
        Me.btnPreview.BackgroundImage = CType(resources.GetObject("btnPreview.BackgroundImage"), System.Drawing.Image)
        Me.btnPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPreview.BorderColor = System.Drawing.Color.Empty
        Me.btnPreview.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPreview.FlatAppearance.BorderSize = 0
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.ForeColor = System.Drawing.Color.Black
        Me.btnPreview.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPreview.GradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Location = New System.Drawing.Point(15, 13)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPreview.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPreview.Size = New System.Drawing.Size(97, 30)
        Me.btnPreview.TabIndex = 127
        Me.btnPreview.Text = "&Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(557, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 126
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(660, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(660, 127)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 151
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(765, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Scan/Attachment"
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'frmScanOrAttachmentInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 442)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmScanOrAttachmentInfo"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Scan / Attachment Info"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlEmployee.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnScan As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objelSep As eZee.Common.eZeeLine
    Friend WithEvents cboDocument As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocType As System.Windows.Forms.Label
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocument As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents btnRemove As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents lvScanAttachment As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDocument As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhFileName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDocType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhEmpId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAppId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhFullPath As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGuid As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpUploadDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblUploadDate As System.Windows.Forms.Label
    Friend WithEvents lblSelection As System.Windows.Forms.Label
    Friend WithEvents cboAssocatedValue As System.Windows.Forms.ComboBox
    Friend WithEvents colhValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhTransId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhDocumentId As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnPreview As eZee.Common.eZeeLightButton
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
End Class

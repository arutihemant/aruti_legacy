﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmADMapHierarchyLevel

#Region " Private Variables "
    Private mblnCancel As Boolean = True
    Private ReadOnly mstrModuleName As String = "frmADMapHierarchyLevel"
    Private mintAllocationId As Integer = 0
    Private mintHierarchyLevel As Integer = 0
    Private mintCompanyId As Integer = 0
    Private objAdEntry As clsadentry_hierarchy
    Private mblnIsSuccess As Boolean = False

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal _CompanyId As Integer, ByVal _AllocationId As Integer, ByVal _HierarchyLevel As Integer, ByRef _Issuccess As Boolean) As Boolean
        Try
            mintAllocationId = _AllocationId
            mintHierarchyLevel = _HierarchyLevel
            mintCompanyId = _CompanyId
            Me.ShowDialog()
            _Issuccess = mblnIsSuccess
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " From's Events "

    Private Sub frmADMapHierarchyLevel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objAdEntry = New clsadentry_hierarchy
            FillGrid()
            GetValue()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmADMapHierarchyLevel_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub GetValue()
        Try
            If dgData.RowCount <= 0 Then Exit Sub

            Dim dcRow = dgData.Columns.Cast(Of DataGridViewColumn)().Where(Function(x) x.GetType().FullName = "System.Windows.Forms.DataGridViewComboBoxColumn")

            For i As Integer = 0 To dgData.RowCount - 1

                Dim dsList As DataSet = objAdEntry.GetList("List", mintCompanyId, True, mintAllocationId, CInt(dgData.Rows(i).Cells(objdgcolhParentAllocationId.Index).Value))

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    dgData.Rows(i).Cells(objdgcolhHierarchyId.Index).Value = CInt(dsList.Tables(0).Rows(0)("hierarchyunkid"))

                    For j As Integer = 0 To dcRow.Count - 1

                        If dcRow(j).Name = "Child1" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid1"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid1"))

                        ElseIf dcRow(j).Name = "Child2" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid2"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid2"))

                        ElseIf dcRow(j).Name = "Child3" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid3"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid3"))

                        ElseIf dcRow(j).Name = "Child4" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid4"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid4"))

                        ElseIf dcRow(j).Name = "Child5" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid5"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid5"))

                        ElseIf dcRow(j).Name = "Child6" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid6"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid6"))

                        ElseIf dcRow(j).Name = "Child7" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid7"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid7"))

                        ElseIf dcRow(j).Name = "Child8" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid8"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid8"))

                        ElseIf dcRow(j).Name = "Child9" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid9"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid9"))

                        ElseIf dcRow(j).Name = "Child10" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid10"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid10"))

                        ElseIf dcRow(j).Name = "Child11" Then

                            dgData.Rows(i).Cells(dcRow(j).Name).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid11"))
                            dgData.Rows(i).Cells(dcRow(j).Index + 1).Value = CInt(dsList.Tables(0).Rows(0)("allocationrefid11"))

                        End If

                    Next

                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = objMaster.GetCompanyCurrentYearInfo("List", mintCompanyId)


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim objAdAttribute As New clsADAttribute_mapping
                Dim dtHierarchyLevel As DataTable = objAdAttribute.GetEntryPointAllocation(mintAllocationId, dsList.Tables(0).Rows(0)("database_name").ToString())
                objAdAttribute = Nothing

                For i As Integer = 1 To mintHierarchyLevel - 1
                    Dim dcColumn As New DataColumn("oChild" & i, Type.GetType("System.Int16"))
                    dcColumn.DefaultValue = 0
                    dtHierarchyLevel.Columns.Add(dcColumn)
                Next

                dgData.AutoGenerateColumns = False
                dgcolhParentAllocation.ReadOnly = True
                dgcolhParentAllocation.DataPropertyName = "AllocationName"
                objdgcolhParentAllocationId.DataPropertyName = "AllocationId"

                Dim dsChilAllocation As DataSet = objMaster.GetEAllocation_Notification("List", "", False, False)
                Dim dtAllocation As DataTable = New DataView(dsChilAllocation.Tables(0), "Id <> " & mintAllocationId & " AND Id <> " & enAllocation.COST_CENTER, "", DataViewRowState.CurrentRows).ToTable()
                Dim dRow As DataRow = dtAllocation.NewRow()
                dRow("Id") = 0
                dRow("Name") = Language.getMessage(mstrModuleName, 4, "Select")
                dtAllocation.Rows.InsertAt(dRow, 0)

                Dim dsAllocation As DataSet = objMaster.GetEAllocation_Notification("List", mintAllocationId.ToString(), False, False)
                If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then
                    dgcolhParentAllocation.HeaderText = dsAllocation.Tables(0).Rows(0)("Name").ToString()
                End If


                For i As Integer = 1 To mintHierarchyLevel - 1
                    Dim dcchildCombo As New DataGridViewComboBoxColumn
                    dcchildCombo.HeaderText = "Child" & i
                    dcchildCombo.Name = "Child" & i
                    dcchildCombo.Width = 200
                    dgData.Columns.Add(dcchildCombo)

                    dcchildCombo.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                    dcchildCombo.ValueMember = "Id"
                    dcchildCombo.DisplayMember = "Name"
                    dcchildCombo.DataPropertyName = "Id"
                    dcchildCombo.DataSource = dtAllocation.Copy()

                    Dim dcchildtxt As New DataGridViewTextBoxColumn
                    dcchildtxt.Name = "oChild" & i
                    dcchildtxt.HeaderText = ""
                    dcchildtxt.Width = 25
                    dcchildtxt.Tag = "oChild" & i
                    dcchildtxt.Visible = False
                    dgData.Columns.Add(dcchildtxt)
                Next

                dgData.DataSource = dtHierarchyLevel

            End If

            objMaster = Nothing


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If dgData.Rows.Count <= 0 Then
                Return False
            End If

            Dim dcRow = dgData.Columns.Cast(Of DataGridViewColumn)().Where(Function(x) x.GetType().FullName = "System.Windows.Forms.DataGridViewTextBoxColumn" And x.Visible = False And x.Name <> objdgcolhParentAllocationId.Name And x.Name <> objdgcolhHierarchyId.Name)

            For Each oRow As DataGridViewRow In dgData.Rows
                Dim iCells = From oCell As DataGridViewCell In oRow.Cells Where TypeOf oCell Is DataGridViewTextBoxCell AndAlso oCell.Visible = False AndAlso oCell.ColumnIndex <> objdgcolhHierarchyId.Index AndAlso oCell.ColumnIndex <> objdgcolhParentAllocationId.Index AndAlso oCell.Value IsNot Nothing AndAlso CInt(oCell.Value) > 0 Select oCell
                If iCells IsNot Nothing AndAlso iCells.Count() > 0 Then

                    'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    'Dim xCount = (From oCell As DataGridViewCell In oRow.Cells Where TypeOf oCell Is DataGridViewTextBoxCell AndAlso oCell.Visible = False AndAlso oCell.ColumnIndex <> objdgcolhHierarchyId.Index AndAlso oCell.ColumnIndex <> objdgcolhParentAllocationId.Index Select oCell).Count
                    'If iCells.Count() <> xCount Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Hierarchy Mapping is compulsory information.Please map hierarchy."))
                    '    mblnFlag = False
                    '    Exit Try
                    'End If
                    'Pinkal (04-Apr-2020) -- End

                   
                    For Each iCol As DataGridViewColumn In dcRow
                        Dim oCol As DataGridViewColumn = iCol
                        Dim iRow As DataGridViewRow = oRow
                        Dim oCells = From oCell As DataGridViewCell In oRow.Cells Where TypeOf oCell Is DataGridViewTextBoxCell AndAlso oCell.ColumnIndex <> oCol.Index AndAlso oCell.Value.ToString.ToUpper() = iRow.Cells(oCol.Index).Value.ToString.ToUpper() AndAlso oCell.ColumnIndex <> objdgcolhHierarchyId.Index AndAlso oCell.ColumnIndex <> objdgcolhParentAllocationId.Index Select oCell
                        If oCells IsNot Nothing Then
                            If oCells.Count() > 0 Then
                                For Each iCell In oCells
                                    If iCell.ColumnIndex = iCol.Index Then
                                        Continue For
                                    ElseIf oRow.Cells(iCell.ColumnIndex).Value = oRow.Cells(iCol.Index).Value Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot assign same value in different level.Please assign different value in different level."))
                                        mblnFlag = False
                                        Exit Try
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            Next




            'Dim dcRow = dgData.Columns.Cast(Of DataGridViewColumn)().Where(Function(x) x.GetType().FullName = "System.Windows.Forms.DataGridViewTextBoxColumn" And x.Visible = False And x.Name <> objdgcolhParentAllocationId.Name And x.Name <> objdgcolhHierarchyId.Name)

            'If dcRow IsNot Nothing AndAlso dcRow.Count > 0 Then

            '    Dim mblnIsExit As Boolean = False
            '    Dim mblnIsNotValue As Boolean = False
            '    For i As Integer = 0 To dgData.Rows.Count - 1

            '        Dim xCurrentValue As Integer = 0
            '        Dim index As Integer = 0

            '        For j As Integer = 0 To dcRow.Count - 1

            '            If index <= 0 Then
            '                index = dcRow(j).Index
            '            End If

            '            If xCurrentValue <= 0 Then
            '                xCurrentValue = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)
            '                If index = dcRow(j).Index Then Continue For
            '                'Else
            '                '    index = dcRow(j).Index
            '            End If

            '            If (xCurrentValue = 0 AndAlso CInt(dgData.Rows(i).Cells(j).Value) = 0) Then
                        '    Continue For
            '            End If

            '            If (xCurrentValue = 0 OrElse CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value) = 0) AndAlso (xCurrentValue > 0 OrElse CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value) > 0) Then
                        '    mblnIsNotValue = True
                        '    Exit For
            '            End If

            '            If (xCurrentValue > 0 AndAlso CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value) > 0) AndAlso xCurrentValue = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value) Then
                        '    mblnIsExit = True
                        '    Exit For
            '            End If

            '            'If (xCurrentValue = 0 AndAlso CInt(dgData.Rows(i).Cells(index).Value) = 0) Then
            '            '    Continue For
            '            'End If

            '            'If (xCurrentValue = 0 OrElse CInt(dgData.Rows(i).Cells(index).Value) = 0) AndAlso (xCurrentValue > 0 OrElse CInt(dgData.Rows(i).Cells(index).Value) > 0) Then
            '            '    mblnIsNotValue = True
            '            '    Exit For
            '            'End If

            '            'If (xCurrentValue > 0 AndAlso CInt(dgData.Rows(i).Cells(index).Value) > 0) AndAlso xCurrentValue = CInt(dgData.Rows(i).Cells(index).Value) Then
            '            '    mblnIsExit = True
            '            '    Exit For
            '            'End If

            '            If j = dcRow.Count - 1 Then
            '                xCurrentValue = 0
            '                index = dcRow(j).Index
            '            End If

            '        Next

            '        If mblnIsNotValue Then
            '            mblnFlag = False
            '            Exit For
            '        End If

            '        If mblnIsExit Then
            '            mblnFlag = False
            '            Exit For
            '        End If

            '    Next

            '    If mblnIsNotValue Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Hierarchy Mapping is compulsory information.Please map hierarchy."))
            '    End If

            '    If mblnIsExit Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot assign same value in different level.Please assign different value in different level."))
            '    End If

            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function


#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mblnFlag As Boolean = False
        Try

            If Validation() = False Then Exit Sub

            Dim dcRow = dgData.Columns.Cast(Of DataGridViewColumn)().Where(Function(x) x.GetType().FullName = "System.Windows.Forms.DataGridViewTextBoxColumn" And x.Visible = False And x.Name <> objdgcolhParentAllocationId.Name And x.Name <> objdgcolhHierarchyId.Name)

            If dcRow IsNot Nothing AndAlso dcRow.Count > 0 Then

                objAdEntry._Companyunkid = mintCompanyId
                objAdEntry._Userunkid = User._Object._Userunkid
                objAdEntry._Isvoid = False
                objAdEntry._AuditUserId = User._Object._Userunkid
                objAdEntry._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objAdEntry._FormName = Me.Name
                objAdEntry._ClientIP = getIP()
                objAdEntry._FromWeb = False
                objAdEntry._HostName = getHostName()

                For i As Integer = 0 To dgData.Rows.Count - 1

                    objAdEntry._Hierarchyunkid = CInt(dgData.Rows(i).Cells(objdgcolhHierarchyId.Index).Value)
                    objAdEntry._Allocationrefid = mintAllocationId
                    objAdEntry._Allocationtranunkid = CInt(dgData.Rows(i).Cells(objdgcolhParentAllocationId.Index).Value)

                    For j As Integer = 0 To dcRow.Count - 1

                        If dgData.Columns(dcRow(j).Index).Name = "oChild1" Then

                            objAdEntry._Allocationrefid1 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild2" Then

                            objAdEntry._Allocationrefid2 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild3" Then

                            objAdEntry._Allocationrefid3 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild4" Then

                            objAdEntry._Allocationrefid4 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild5" Then

                            objAdEntry._Allocationrefid5 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild6" Then

                            objAdEntry._Allocationrefid6 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild7" Then

                            objAdEntry._Allocationrefid7 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild8" Then

                            objAdEntry._Allocationrefid8 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild9" Then

                            objAdEntry._Allocationrefid9 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild10" Then

                            objAdEntry._Allocationrefid10 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        ElseIf dgData.Columns(dcRow(j).Index).Name = "oChild11" Then

                            objAdEntry._Allocationrefid11 = CInt(dgData.Rows(i).Cells(dcRow(j).Index).Value)

                        End If

                    Next

                    'If objAdEntry._Allocationrefid1 = 0 AndAlso objAdEntry._Allocationrefid2 = 0 AndAlso objAdEntry._Allocationrefid3 = 0 AndAlso objAdEntry._Allocationrefid4 = 0 AndAlso objAdEntry._Allocationrefid5 = 0 _
                    'AndAlso objAdEntry._Allocationrefid6 = 0 AndAlso objAdEntry._Allocationrefid7 = 0 AndAlso objAdEntry._Allocationrefid8 = 0 AndAlso objAdEntry._Allocationrefid9 = 0 AndAlso objAdEntry._Allocationrefid10 = 0 _
                    'AndAlso objAdEntry._Allocationrefid11 = 0 Then
                    '    Continue For
                    'End If

                    mblnFlag = objAdEntry.Insert()

                    If mblnFlag = False Then
                        eZeeMsgBox.Show(objAdEntry._Message)
                        Exit For
                    End If

                Next


                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Active Directory hierarchy level saved successfully."))
                    mblnIsSuccess = True
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnIsSuccess = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgData_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgData.CellEnter
        SendKeys.Send("{F4}")
    End Sub

    Private Sub dgData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgData.EditingControlShowing
        Try
            Dim cmb As ComboBox = CType(e.Control, ComboBox)
            If cmb IsNot Nothing Then
                RemoveHandler cmb.SelectedValueChanged, AddressOf cmb_SelectedValueChanged
                AddHandler cmb.SelectedValueChanged, AddressOf cmb_SelectedValueChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgData_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgData.DataError

    End Sub

#End Region

#Region "ComboBox Events"

    Private Sub cmb_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb IsNot Nothing AndAlso cmb.SelectedValue IsNot Nothing Then
                RemoveHandler cmb.SelectedValueChanged, AddressOf cmb_SelectedValueChanged
                dgData.Rows(dgData.CurrentRow.Index).Cells(dgData.CurrentCell.ColumnIndex + 1).Value = cmb.SelectedValue
                AddHandler cmb.SelectedValueChanged, AddressOf cmb_SelectedValueChanged

                If dgData.Rows(dgData.CurrentRow.Index).Cells(objdgcolhHierarchyId.Index).Value Is Nothing Then
                    dgData.Rows(dgData.CurrentRow.Index).Cells(objdgcolhHierarchyId.Index).Value = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cmb_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Me.pnlMain.BackColor = GUI.FormColor
			Call SetLanguage()
			
			Me.objgbMapAttributeHierarchyLevel.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.objgbMapAttributeHierarchyLevel.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.dgcolhParentAllocation.HeaderText = Language._Object.getCaption(Me.dgcolhParentAllocation.Name, Me.dgcolhParentAllocation.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Active Directory hierarchy level saved successfully.")
			Language.setMessage(mstrModuleName, 2, "Hierarchy Mapping is compulsory information.Please map hierarchy.")
			Language.setMessage(mstrModuleName, 3, "You cannot assign same value in different level.Please assign different value in different level.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
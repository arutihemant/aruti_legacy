﻿'************************************************************************************************************************************
'Class Name : clsDependant_Benefice_Status_tran.vb
'Purpose    :
'Date       :20-05-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsDependant_Benefice_Status_tran
    Private Const mstrModuleName = "clsDependant_Benefice_Status_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintDpndtbeneficestatustranunkid As Integer
    Private mintDpndtbeneficetranunkid As Integer
    Private mdtEffective_Date As Date
    Private mblnIsactive As Boolean = True
    Private mintReasonunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficestatustranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Dpndtbeneficestatustranunkid() As Integer
        Get
            Return mintDpndtbeneficestatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficestatustranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficetranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Dpndtbeneficetranunkid() As Integer
        Get
            Return mintDpndtbeneficetranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficetranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reasonunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Reasonunkid() As Integer
        Get
            Return mintReasonunkid
        End Get
        Set(ByVal value As Integer)
            mintReasonunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property



    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  dpndtbeneficestatustranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrdependant_beneficiaries_status_tran " & _
             "WHERE dpndtbeneficestatustranunkid = @dpndtbeneficestatustranunkid "

            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDpndtbeneficestatusTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdpndtbeneficestatustranunkid = CInt(dtRow.Item("dpndtbeneficestatustranunkid"))
                mintdpndtbeneficetranunkid = CInt(dtRow.Item("dpndtbeneficetranunkid"))
                mdteffective_date = dtRow.Item("effective_date")
                mblnisactive = CBool(dtRow.Item("isactive"))
                mintreasonunkid = CInt(dtRow.Item("reasonunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intDpndtbeneficetranunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid " & _
              ", hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid " & _
              ", hrdependant_beneficiaries_status_tran.effective_date " & _
              ", hrdependant_beneficiaries_status_tran.isactive " & _
              ", hrdependant_beneficiaries_status_tran.reasonunkid " & _
              ", hrdependant_beneficiaries_status_tran.userunkid " & _
              ", hrdependant_beneficiaries_status_tran.loginemployeeunkid " & _
              ", hrdependant_beneficiaries_status_tran.isweb " & _
              ", hrdependant_beneficiaries_status_tran.isvoid " & _
              ", hrdependant_beneficiaries_status_tran.voiduserunkid " & _
              ", hrdependant_beneficiaries_status_tran.voidloginemployeeunkid " & _
              ", hrdependant_beneficiaries_status_tran.voiddatetime " & _
              ", hrdependant_beneficiaries_status_tran.voidreason " & _
             "FROM hrdependant_beneficiaries_status_tran " & _
             "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If intDpndtbeneficetranunkid > 0 Then
                strQ &= " AND hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDpndtbeneficetranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdependant_beneficiaries_status_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdteffective_date.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreasonunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            StrQ = "INSERT INTO hrdependant_beneficiaries_status_tran ( " & _
              "  dpndtbeneficetranunkid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @dpndtbeneficetranunkid " & _
              ", @effective_date " & _
              ", @isactive " & _
              ", @reasonunkid " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDpndtbeneficestatusTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(ByVal dtTable As DataTable) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                mintDpndtbeneficetranunkid = CInt(dtRow.Item("DpndtTranId"))
                Me._xDataOp = objDataOperation

                If Insert() = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdependant_beneficiaries_status_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintDpndtbeneficestatustranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdpndtbeneficestatustranunkid.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdteffective_date.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreasonunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            StrQ = "UPDATE hrdependant_beneficiaries_status_tran SET " & _
              "  dpndtbeneficetranunkid = @dpndtbeneficetranunkid" & _
              ", effective_date = @effective_date" & _
              ", isactive = @isactive" & _
              ", reasonunkid = @reasonunkid" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE dpndtbeneficestatustranunkid = @dpndtbeneficestatustranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdependant_beneficiaries_status_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrdependant_beneficiaries_status_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE dpndtbeneficestatustranunkid = @dpndtbeneficestatustranunkid "

            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficestatustranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Dpndtbeneficestatustranunkid = mintDpndtbeneficestatustranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteByDependantunkId(ByVal intDependantUnkId As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrdependant_beneficiaries_status_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid "

            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficestatustranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Dpndtbeneficestatustranunkid = mintDpndtbeneficestatustranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  dpndtbeneficestatustranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrdependant_beneficiaries_status_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND dpndtbeneficestatustranunkid <> @dpndtbeneficestatustranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (06 Sep 2021) -- Start
    'Enhancement :  : Data optimization on dependent status screen.
    Public Function IsTableDataChanged(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " dpndtbeneficetranunkid,  CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) AS effective_date, isactive, reasonunkid "

            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM hrdependant_beneficiaries_status_tran " & _
                            "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 " & _
                            "AND hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
                            "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @effective_date " & _
                            "ORDER BY hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT * FROM cte " & _
                    "EXCEPT " & _
                    "SELECT " & mintDpndtbeneficetranunkid & ", '" & eZeeDate.convertDate(mdtEffective_Date) & "', " & CInt(Int(mblnIsactive)) & ", " & mintReasonunkid & " "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@effective_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEffective_Date))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataChanged; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Sohail (06 Sep 2021) -- End

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dpndtbeneficestatustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficestatustranunkid.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO athrdependant_beneficiaries_status_tran ( " & _
              "  dpndtbeneficestatustranunkid " & _
              ", dpndtbeneficetranunkid " & _
              ", effective_date " & _
              ", isactive " & _
              ", reasonunkid " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  @dpndtbeneficestatustranunkid " & _
              ", @dpndtbeneficetranunkid " & _
              ", @effective_date " & _
              ", @isactive " & _
              ", @reasonunkid " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class